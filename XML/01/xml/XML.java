package xml;
import java.io.*;
import java.util.*;
class P{
	String nombre;
	String valor;
	public P(String nombre){
		this.nombre=nombre;
	}
	public P(String nombre,String valor){
		this.nombre=nombre;
		this.valor=valor;
	}
	public String toString(){
		if(valor==null){
			return this.nombre;
		}
		return this.nombre+"=\""+this.valor+"\"";
	}
}
interface X{
}
class E implements X{
	String n;
	ArrayList<P>p;
	ArrayList<X>e;
	E s;
	int l;
	public E(String n){
		p=new ArrayList<>();
		e=new ArrayList<>();
		this.n=n;
	}
	public void add(X x){
		this.e.add(x);
	}
	public void ap(String n){
		this.p.add(new P(n));
	}
	public void ap(String n,String v){
		this.p.add(new P(n,v));
	}
	public String nr(int l,String s){
		return new String(new char[l]).replace("\0",s);
	}
	public String nr(int l){
		return nr(l,XML.sep);
	}
	public String toString(){
		StringBuilder r=new StringBuilder();
		r.append(nr(l));
		r.append("<");
		r.append(this.n);
		for(int i=0;i<p.size();i++){
			r.append(" ");
			r.append(this.p.get(i)+"");
		}
		if(this.e.size()==0){
			r.append("/>\n");
		}
		if(this.e.size()>0){
			r.append(">");
			if(this.e.size()==1&&this.e.get(0)instanceof V){
				r.append(this.e.get(0)+"");
			}else{
				r.append("\n");
				for(int i=0;i<this.e.size();i++){
					r.append(this.e.get(i)+"");
				}
				r.append(nr(this.l));
			}
			r.append("<");
			r.append("/");
			r.append(this.n);
			r.append(">\n");
		}
		return r+"";
	}
}
class V implements X{
	String v;
	public V(String v){
		this.v=v;
	}
	public String toString(){
		return this.v;
	}
}
public class XML{
	public ArrayList<X>x;
	public E a;
	public static String sep="\t";
	public XML(){
		x=new ArrayList<>();
	}
	public void abrir(String nombre){
		E e=new E(nombre);
		e.s=a;
		if(a==null){
			x.add(e);
		}else{
			e.l=a.l+1;
			a.add(e);
		}
		a=e;
	}
	public void propiedad(String nombre){
		a.ap(nombre);
	}
	public void propiedad(String nombre,String valor){
		a.ap(nombre,valor);
	}
	public void contenido(String c){
		a.add(new V(c));
	}
	public void cerrar(){
		a=a.s;
	}
	public String toString(){
		StringBuilder r=new StringBuilder();
		r.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		for(int i=0;i<x.size();i++){
			r.append(x.get(i)+"");
		}
		return r+"";
	}
	public void escribir(File f)throws Exception{
		PrintStream ps=new PrintStream(new FileOutputStream(f));
		ps.print(this+"");
	}
	public static XML leer(String s)throws Exception{
		XML r=new XML();
		int e=0;
		StringBuilder n=new StringBuilder();
		ArrayList<ArrayList<String>>propiedades=new ArrayList<>();
		int contador=0;
		for(int i=s.indexOf("<",s.indexOf("?>")+2);i<s.length();i++){
			/*if(contador>10000000){
				c.U.imp("Muchos ciclos");
				System.exit(0);
			}*/
			contador++;
			char b=s.charAt(i);
			if(b=='"'||b=='\r'||b=='\n'){
				continue;
			}
			//c.U.imp(i+":"+b+" "+e);
			if(Character.isLetterOrDigit(b)||b=='_'){
				n.append(s.charAt(i));
				if(e==3){
					e=4;
				}
				if(e==0){
					e=6;
				}
			}else{
				if(e==2){
					if(s.charAt(i)==' '){
						if(n.length()>0){
							propiedades.add(new ArrayList<>());
							propiedades.get(propiedades.size()-1).add(n+"");
							n=new StringBuilder();
						}
					}
					if(s.charAt(i)=='='){
						if(n.length()>0){
							propiedades.add(new ArrayList<>());
							propiedades.get(propiedades.size()-1).add(n+"");
							n=new StringBuilder();
						}
						e=3;
						continue;
					}
					if(s.charAt(i)=='>'){
						for(int j=0;j<propiedades.size();j++){
							if(propiedades.get(j).size()==2){
								r.propiedad(propiedades.get(j).get(0),propiedades.get(j).get(1));
							}else{
								r.propiedad(propiedades.get(j).get(0));
							}
						}
						propiedades=new ArrayList<>();
						e=0;
						continue;
					}
					if(s.charAt(i)=='/'){
						for(int j=0;j<propiedades.size();j++){
							if(propiedades.get(j).size()==2){
								r.propiedad(propiedades.get(j).get(0),propiedades.get(j).get(1));
							}else{
								r.propiedad(propiedades.get(j).get(0));
							}
						}
						propiedades=new ArrayList<>();
						r.cerrar();
						e=0;
						i++;
						continue;
					}
				}
				if(e==4){
					if(s.charAt(i)==' '||s.charAt(i)=='>'||s.charAt(i)=='/'){
						e=2;
						propiedades.get(propiedades.size()-1).add(n+"");
						n=new StringBuilder();
						if(s.charAt(i)=='>'||s.charAt(i)=='/'){
							i--;
						}
						continue;
					}
				}
				if(e==6){
					if(s.charAt(i)=='<'){
						r.contenido(n+"");
						n=new StringBuilder();
						e=0;
						if(s.charAt(i+1)!='/'){
							c.U.imp("Error fatal");
							System.exit(0);
						}
						r.cerrar();
						i=s.indexOf(">",i+2);
						continue;
					}
				}
				if(e==1){
					if(s.charAt(i)==' '||s.charAt(i)=='>'||s.charAt(i)=='/'){
						r.abrir(n+"");
						n=new StringBuilder();
					}
					if(s.charAt(i)==' '){
						e=2;
					}
					if(s.charAt(i)=='>'){
						e=0;
					}
					if(s.charAt(i)=='/'){
						e=0;
						i++;
					}
				}
				n=new StringBuilder();
				if(s.charAt(i)=='<'){
					if(s.charAt(i+1)=='/'){
						r.cerrar();
						i=s.indexOf(">",i+2);
						continue;
					}
					e=1;
				}
				if(s.charAt(i)=='='){
					e=3;
				}
				if(s.charAt(i)=='>'){
					e=0;
				}
			}
		}
		return r;
	}
}