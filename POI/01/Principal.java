import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xssf.streaming.*;
import java.util.*;
import java.io.*;
public class Principal{
	public static SXSSFWorkbook unirArchivos(List<String>rutas)throws Exception{
		SXSSFWorkbook wb=new SXSSFWorkbook(5);
		for(int i=0;i<rutas.size();i++){
			XSSFWorkbook owb=new XSSFWorkbook(new FileInputStream(new File(rutas.get(i))));
			SXSSFSheet hoja=new SXSSFSheet(wb,owb.getSheetAt(0));
			System.out.println(hoja.getSheetName());
			wb=(SXSSFWorkbook)hoja.getWorkbook();
			System.out.println(wb.getSheet(hoja.getSheetName()));
			//wb.createSheet();
		}
		return wb;
	}
	public static void main(String[]argumentos){
		try{
			ArrayList<String>lista=new ArrayList<String>();
			lista.add("A.xlsx");
			lista.add("B.xlsx");
			lista.add("C.xlsx");
			lista.add("D.xlsx");
			unirArchivos(lista).write(new FileOutputStream(new File("output.xlsx")));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}