/*
 * An XML document type.
 * Localname: SolicitudDespachoMateriales
 * Namespace: http://EPM.Materiales.BizTalk.Schemas.Solicitud
 * Java type: solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument
 *
 * Automatically generated - do not modify.
 */
package solicitud.schemas.biztalk.materiales.epm;


/**
 * A document containing one SolicitudDespachoMateriales(@http://EPM.Materiales.BizTalk.Schemas.Solicitud) element.
 *
 * This is a complex type.
 */
public interface SolicitudDespachoMaterialesDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(SolicitudDespachoMaterialesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s3CCBD10005EAD96CF7579BB81801A7F6").resolveHandle("solicituddespachomateriales8a85doctype");
    
    /**
     * Gets the "SolicitudDespachoMateriales" element
     */
    solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales getSolicitudDespachoMateriales();
    
    /**
     * Sets the "SolicitudDespachoMateriales" element
     */
    void setSolicitudDespachoMateriales(solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales solicitudDespachoMateriales);
    
    /**
     * Appends and returns a new empty "SolicitudDespachoMateriales" element
     */
    solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales addNewSolicitudDespachoMateriales();
    
    /**
     * An XML SolicitudDespachoMateriales(@http://EPM.Materiales.BizTalk.Schemas.Solicitud).
     *
     * This is a complex type.
     */
    public interface SolicitudDespachoMateriales extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(SolicitudDespachoMateriales.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s3CCBD10005EAD96CF7579BB81801A7F6").resolveHandle("solicituddespachomateriales3b51elemtype");
        
        /**
         * Gets the "AplicacionDestino" element
         */
        java.lang.String getAplicacionDestino();
        
        /**
         * Gets (as xml) the "AplicacionDestino" element
         */
        org.apache.xmlbeans.XmlString xgetAplicacionDestino();
        
        /**
         * Sets the "AplicacionDestino" element
         */
        void setAplicacionDestino(java.lang.String aplicacionDestino);
        
        /**
         * Sets (as xml) the "AplicacionDestino" element
         */
        void xsetAplicacionDestino(org.apache.xmlbeans.XmlString aplicacionDestino);
        
        /**
         * Gets the "SistemaOrigen" element
         */
        java.lang.String getSistemaOrigen();
        
        /**
         * Gets (as xml) the "SistemaOrigen" element
         */
        org.apache.xmlbeans.XmlString xgetSistemaOrigen();
        
        /**
         * Sets the "SistemaOrigen" element
         */
        void setSistemaOrigen(java.lang.String sistemaOrigen);
        
        /**
         * Sets (as xml) the "SistemaOrigen" element
         */
        void xsetSistemaOrigen(org.apache.xmlbeans.XmlString sistemaOrigen);
        
        /**
         * Gets the "Materiales" element
         */
        solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales getMateriales();
        
        /**
         * Sets the "Materiales" element
         */
        void setMateriales(solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales materiales);
        
        /**
         * Appends and returns a new empty "Materiales" element
         */
        solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales addNewMateriales();
        
        /**
         * An XML Materiales(@).
         *
         * This is a complex type.
         */
        public interface Materiales extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Materiales.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s3CCBD10005EAD96CF7579BB81801A7F6").resolveHandle("materiales7692elemtype");
            
            /**
             * Gets array of all "Material" elements
             */
            solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material[] getMaterialArray();
            
            /**
             * Gets ith "Material" element
             */
            solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material getMaterialArray(int i);
            
            /**
             * Returns number of "Material" element
             */
            int sizeOfMaterialArray();
            
            /**
             * Sets array of all "Material" element
             */
            void setMaterialArray(solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material[] materialArray);
            
            /**
             * Sets ith "Material" element
             */
            void setMaterialArray(int i, solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material material);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "Material" element
             */
            solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material insertNewMaterial(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "Material" element
             */
            solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material addNewMaterial();
            
            /**
             * Removes the ith "Material" element
             */
            void removeMaterial(int i);
            
            /**
             * An XML Material(@).
             *
             * This is a complex type.
             */
            public interface Material extends org.apache.xmlbeans.XmlObject
            {
                public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                    org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Material.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s3CCBD10005EAD96CF7579BB81801A7F6").resolveHandle("material321felemtype");
                
                /**
                 * Gets the "Almacen" element
                 */
                java.lang.String getAlmacen();
                
                /**
                 * Gets (as xml) the "Almacen" element
                 */
                org.apache.xmlbeans.XmlString xgetAlmacen();
                
                /**
                 * Sets the "Almacen" element
                 */
                void setAlmacen(java.lang.String almacen);
                
                /**
                 * Sets (as xml) the "Almacen" element
                 */
                void xsetAlmacen(org.apache.xmlbeans.XmlString almacen);
                
                /**
                 * Gets the "CantidadEntregada" element
                 */
                java.math.BigDecimal getCantidadEntregada();
                
                /**
                 * Gets (as xml) the "CantidadEntregada" element
                 */
                org.apache.xmlbeans.XmlDecimal xgetCantidadEntregada();
                
                /**
                 * Sets the "CantidadEntregada" element
                 */
                void setCantidadEntregada(java.math.BigDecimal cantidadEntregada);
                
                /**
                 * Sets (as xml) the "CantidadEntregada" element
                 */
                void xsetCantidadEntregada(org.apache.xmlbeans.XmlDecimal cantidadEntregada);
                
                /**
                 * Gets the "CodigoMaterial" element
                 */
                java.lang.String getCodigoMaterial();
                
                /**
                 * Gets (as xml) the "CodigoMaterial" element
                 */
                org.apache.xmlbeans.XmlString xgetCodigoMaterial();
                
                /**
                 * Sets the "CodigoMaterial" element
                 */
                void setCodigoMaterial(java.lang.String codigoMaterial);
                
                /**
                 * Sets (as xml) the "CodigoMaterial" element
                 */
                void xsetCodigoMaterial(org.apache.xmlbeans.XmlString codigoMaterial);
                
                /**
                 * Gets the "CostoTransaccion" element
                 */
                java.math.BigDecimal getCostoTransaccion();
                
                /**
                 * Gets (as xml) the "CostoTransaccion" element
                 */
                org.apache.xmlbeans.XmlDecimal xgetCostoTransaccion();
                
                /**
                 * Sets the "CostoTransaccion" element
                 */
                void setCostoTransaccion(java.math.BigDecimal costoTransaccion);
                
                /**
                 * Sets (as xml) the "CostoTransaccion" element
                 */
                void xsetCostoTransaccion(org.apache.xmlbeans.XmlDecimal costoTransaccion);
                
                /**
                 * Gets the "DescripcionMaterial" element
                 */
                java.lang.String getDescripcionMaterial();
                
                /**
                 * Gets (as xml) the "DescripcionMaterial" element
                 */
                org.apache.xmlbeans.XmlString xgetDescripcionMaterial();
                
                /**
                 * True if has "DescripcionMaterial" element
                 */
                boolean isSetDescripcionMaterial();
                
                /**
                 * Sets the "DescripcionMaterial" element
                 */
                void setDescripcionMaterial(java.lang.String descripcionMaterial);
                
                /**
                 * Sets (as xml) the "DescripcionMaterial" element
                 */
                void xsetDescripcionMaterial(org.apache.xmlbeans.XmlString descripcionMaterial);
                
                /**
                 * Unsets the "DescripcionMaterial" element
                 */
                void unsetDescripcionMaterial();
                
                /**
                 * Gets the "Negocio" element
                 */
                java.lang.String getNegocio();
                
                /**
                 * Gets (as xml) the "Negocio" element
                 */
                org.apache.xmlbeans.XmlString xgetNegocio();
                
                /**
                 * Sets the "Negocio" element
                 */
                void setNegocio(java.lang.String negocio);
                
                /**
                 * Sets (as xml) the "Negocio" element
                 */
                void xsetNegocio(org.apache.xmlbeans.XmlString negocio);
                
                /**
                 * Gets the "FechaHoraTransaccion" element
                 */
                java.util.Calendar getFechaHoraTransaccion();
                
                /**
                 * Gets (as xml) the "FechaHoraTransaccion" element
                 */
                org.apache.xmlbeans.XmlDateTime xgetFechaHoraTransaccion();
                
                /**
                 * Sets the "FechaHoraTransaccion" element
                 */
                void setFechaHoraTransaccion(java.util.Calendar fechaHoraTransaccion);
                
                /**
                 * Sets (as xml) the "FechaHoraTransaccion" element
                 */
                void xsetFechaHoraTransaccion(org.apache.xmlbeans.XmlDateTime fechaHoraTransaccion);
                
                /**
                 * Gets the "PlantaDespacho" element
                 */
                java.lang.String getPlantaDespacho();
                
                /**
                 * Gets (as xml) the "PlantaDespacho" element
                 */
                org.apache.xmlbeans.XmlString xgetPlantaDespacho();
                
                /**
                 * Sets the "PlantaDespacho" element
                 */
                void setPlantaDespacho(java.lang.String plantaDespacho);
                
                /**
                 * Sets (as xml) the "PlantaDespacho" element
                 */
                void xsetPlantaDespacho(org.apache.xmlbeans.XmlString plantaDespacho);
                
                /**
                 * Gets the "NoReserva" element
                 */
                java.lang.String getNoReserva();
                
                /**
                 * Gets (as xml) the "NoReserva" element
                 */
                org.apache.xmlbeans.XmlString xgetNoReserva();
                
                /**
                 * Sets the "NoReserva" element
                 */
                void setNoReserva(java.lang.String noReserva);
                
                /**
                 * Sets (as xml) the "NoReserva" element
                 */
                void xsetNoReserva(org.apache.xmlbeans.XmlString noReserva);
                
                /**
                 * Gets the "NumeroDeSerie" element
                 */
                java.lang.String getNumeroDeSerie();
                
                /**
                 * Gets (as xml) the "NumeroDeSerie" element
                 */
                org.apache.xmlbeans.XmlString xgetNumeroDeSerie();
                
                /**
                 * True if has "NumeroDeSerie" element
                 */
                boolean isSetNumeroDeSerie();
                
                /**
                 * Sets the "NumeroDeSerie" element
                 */
                void setNumeroDeSerie(java.lang.String numeroDeSerie);
                
                /**
                 * Sets (as xml) the "NumeroDeSerie" element
                 */
                void xsetNumeroDeSerie(org.apache.xmlbeans.XmlString numeroDeSerie);
                
                /**
                 * Unsets the "NumeroDeSerie" element
                 */
                void unsetNumeroDeSerie();
                
                /**
                 * Gets the "OT_ERP" element
                 */
                java.lang.String getOTERP();
                
                /**
                 * Gets (as xml) the "OT_ERP" element
                 */
                org.apache.xmlbeans.XmlString xgetOTERP();
                
                /**
                 * Sets the "OT_ERP" element
                 */
                void setOTERP(java.lang.String oterp);
                
                /**
                 * Sets (as xml) the "OT_ERP" element
                 */
                void xsetOTERP(org.apache.xmlbeans.XmlString oterp);
                
                /**
                 * Gets the "OT_Origen" element
                 */
                java.lang.String getOTOrigen();
                
                /**
                 * Gets (as xml) the "OT_Origen" element
                 */
                org.apache.xmlbeans.XmlString xgetOTOrigen();
                
                /**
                 * True if has "OT_Origen" element
                 */
                boolean isSetOTOrigen();
                
                /**
                 * Sets the "OT_Origen" element
                 */
                void setOTOrigen(java.lang.String otOrigen);
                
                /**
                 * Sets (as xml) the "OT_Origen" element
                 */
                void xsetOTOrigen(org.apache.xmlbeans.XmlString otOrigen);
                
                /**
                 * Unsets the "OT_Origen" element
                 */
                void unsetOTOrigen();
                
                /**
                 * Gets the "Secuencia" element
                 */
                long getSecuencia();
                
                /**
                 * Gets (as xml) the "Secuencia" element
                 */
                org.apache.xmlbeans.XmlLong xgetSecuencia();
                
                /**
                 * Sets the "Secuencia" element
                 */
                void setSecuencia(long secuencia);
                
                /**
                 * Sets (as xml) the "Secuencia" element
                 */
                void xsetSecuencia(org.apache.xmlbeans.XmlLong secuencia);
                
                /**
                 * Gets the "TipoDespacho" element
                 */
                java.lang.String getTipoDespacho();
                
                /**
                 * Gets (as xml) the "TipoDespacho" element
                 */
                org.apache.xmlbeans.XmlString xgetTipoDespacho();
                
                /**
                 * Sets the "TipoDespacho" element
                 */
                void setTipoDespacho(java.lang.String tipoDespacho);
                
                /**
                 * Sets (as xml) the "TipoDespacho" element
                 */
                void xsetTipoDespacho(org.apache.xmlbeans.XmlString tipoDespacho);
                
                /**
                 * Gets the "IdentificadorItem" element
                 */
                long getIdentificadorItem();
                
                /**
                 * Gets (as xml) the "IdentificadorItem" element
                 */
                org.apache.xmlbeans.XmlLong xgetIdentificadorItem();
                
                /**
                 * Sets the "IdentificadorItem" element
                 */
                void setIdentificadorItem(long identificadorItem);
                
                /**
                 * Sets (as xml) the "IdentificadorItem" element
                 */
                void xsetIdentificadorItem(org.apache.xmlbeans.XmlLong identificadorItem);
                
                /**
                 * A factory class with static methods for creating instances
                 * of this type.
                 */
                
                public static final class Factory
                {
                    public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material newInstance() {
                      return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                    
                    public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material newInstance(org.apache.xmlbeans.XmlOptions options) {
                      return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                    
                    private Factory() { } // No instance of this class allowed
                }
            }
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales newInstance() {
                  return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales newInstance() {
              return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument newInstance() {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
