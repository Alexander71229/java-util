/*
 * An XML document type.
 * Localname: SolicitudDespachoMateriales
 * Namespace: http://EPM.Materiales.BizTalk.Schemas.Solicitud
 * Java type: solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument
 *
 * Automatically generated - do not modify.
 */
package solicitud.schemas.biztalk.materiales.epm.impl;
/**
 * A document containing one SolicitudDespachoMateriales(@http://EPM.Materiales.BizTalk.Schemas.Solicitud) element.
 *
 * This is a complex type.
 */
public class SolicitudDespachoMaterialesDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument
{
    private static final long serialVersionUID = 1L;
    
    public SolicitudDespachoMaterialesDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SOLICITUDDESPACHOMATERIALES$0 = 
        new javax.xml.namespace.QName("http://EPM.Materiales.BizTalk.Schemas.Solicitud", "SolicitudDespachoMateriales");
    
    
    /**
     * Gets the "SolicitudDespachoMateriales" element
     */
    public solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales getSolicitudDespachoMateriales()
    {
        synchronized (monitor())
        {
            check_orphaned();
            solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales target = null;
            target = (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales)get_store().find_element_user(SOLICITUDDESPACHOMATERIALES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "SolicitudDespachoMateriales" element
     */
    public void setSolicitudDespachoMateriales(solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales solicitudDespachoMateriales)
    {
        generatedSetterHelperImpl(solicitudDespachoMateriales, SOLICITUDDESPACHOMATERIALES$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "SolicitudDespachoMateriales" element
     */
    public solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales addNewSolicitudDespachoMateriales()
    {
        synchronized (monitor())
        {
            check_orphaned();
            solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales target = null;
            target = (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales)get_store().add_element_user(SOLICITUDDESPACHOMATERIALES$0);
            return target;
        }
    }
    /**
     * An XML SolicitudDespachoMateriales(@http://EPM.Materiales.BizTalk.Schemas.Solicitud).
     *
     * This is a complex type.
     */
    public static class SolicitudDespachoMaterialesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales
    {
        private static final long serialVersionUID = 1L;
        
        public SolicitudDespachoMaterialesImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName APLICACIONDESTINO$0 = 
            new javax.xml.namespace.QName("", "AplicacionDestino");
        private static final javax.xml.namespace.QName SISTEMAORIGEN$2 = 
            new javax.xml.namespace.QName("", "SistemaOrigen");
        private static final javax.xml.namespace.QName MATERIALES$4 = 
            new javax.xml.namespace.QName("", "Materiales");
        
        
        /**
         * Gets the "AplicacionDestino" element
         */
        public java.lang.String getAplicacionDestino()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APLICACIONDESTINO$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AplicacionDestino" element
         */
        public org.apache.xmlbeans.XmlString xgetAplicacionDestino()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(APLICACIONDESTINO$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AplicacionDestino" element
         */
        public void setAplicacionDestino(java.lang.String aplicacionDestino)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APLICACIONDESTINO$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APLICACIONDESTINO$0);
                }
                target.setStringValue(aplicacionDestino);
            }
        }
        
        /**
         * Sets (as xml) the "AplicacionDestino" element
         */
        public void xsetAplicacionDestino(org.apache.xmlbeans.XmlString aplicacionDestino)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(APLICACIONDESTINO$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(APLICACIONDESTINO$0);
                }
                target.set(aplicacionDestino);
            }
        }
        
        /**
         * Gets the "SistemaOrigen" element
         */
        public java.lang.String getSistemaOrigen()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SISTEMAORIGEN$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "SistemaOrigen" element
         */
        public org.apache.xmlbeans.XmlString xgetSistemaOrigen()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SISTEMAORIGEN$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "SistemaOrigen" element
         */
        public void setSistemaOrigen(java.lang.String sistemaOrigen)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SISTEMAORIGEN$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SISTEMAORIGEN$2);
                }
                target.setStringValue(sistemaOrigen);
            }
        }
        
        /**
         * Sets (as xml) the "SistemaOrigen" element
         */
        public void xsetSistemaOrigen(org.apache.xmlbeans.XmlString sistemaOrigen)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SISTEMAORIGEN$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SISTEMAORIGEN$2);
                }
                target.set(sistemaOrigen);
            }
        }
        
        /**
         * Gets the "Materiales" element
         */
        public solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales getMateriales()
        {
            synchronized (monitor())
            {
                check_orphaned();
                solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales target = null;
                target = (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales)get_store().find_element_user(MATERIALES$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Materiales" element
         */
        public void setMateriales(solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales materiales)
        {
            generatedSetterHelperImpl(materiales, MATERIALES$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Materiales" element
         */
        public solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales addNewMateriales()
        {
            synchronized (monitor())
            {
                check_orphaned();
                solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales target = null;
                target = (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales)get_store().add_element_user(MATERIALES$4);
                return target;
            }
        }
        /**
         * An XML Materiales(@).
         *
         * This is a complex type.
         */
        public static class MaterialesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales
        {
            private static final long serialVersionUID = 1L;
            
            public MaterialesImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName MATERIAL$0 = 
                new javax.xml.namespace.QName("", "Material");
            
            
            /**
             * Gets array of all "Material" elements
             */
            public solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material[] getMaterialArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(MATERIAL$0, targetList);
                    solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material[] result = new solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets ith "Material" element
             */
            public solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material getMaterialArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material target = null;
                    target = (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material)get_store().find_element_user(MATERIAL$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target;
                }
            }
            
            /**
             * Returns number of "Material" element
             */
            public int sizeOfMaterialArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(MATERIAL$0);
                }
            }
            
            /**
             * Sets array of all "Material" element  WARNING: This method is not atomicaly synchronized.
             */
            public void setMaterialArray(solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material[] materialArray)
            {
                check_orphaned();
                arraySetterHelper(materialArray, MATERIAL$0);
            }
            
            /**
             * Sets ith "Material" element
             */
            public void setMaterialArray(int i, solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material material)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material target = null;
                    target = (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material)get_store().find_element_user(MATERIAL$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(material);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "Material" element
             */
            public solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material insertNewMaterial(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material target = null;
                    target = (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material)get_store().insert_element_user(MATERIAL$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "Material" element
             */
            public solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material addNewMaterial()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material target = null;
                    target = (solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material)get_store().add_element_user(MATERIAL$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "Material" element
             */
            public void removeMaterial(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(MATERIAL$0, i);
                }
            }
            /**
             * An XML Material(@).
             *
             * This is a complex type.
             */
            public static class MaterialImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements solicitud.schemas.biztalk.materiales.epm.SolicitudDespachoMaterialesDocument.SolicitudDespachoMateriales.Materiales.Material
            {
                private static final long serialVersionUID = 1L;
                
                public MaterialImpl(org.apache.xmlbeans.SchemaType sType)
                {
                    super(sType);
                }
                
                private static final javax.xml.namespace.QName ALMACEN$0 = 
                    new javax.xml.namespace.QName("", "Almacen");
                private static final javax.xml.namespace.QName CANTIDADENTREGADA$2 = 
                    new javax.xml.namespace.QName("", "CantidadEntregada");
                private static final javax.xml.namespace.QName CODIGOMATERIAL$4 = 
                    new javax.xml.namespace.QName("", "CodigoMaterial");
                private static final javax.xml.namespace.QName COSTOTRANSACCION$6 = 
                    new javax.xml.namespace.QName("", "CostoTransaccion");
                private static final javax.xml.namespace.QName DESCRIPCIONMATERIAL$8 = 
                    new javax.xml.namespace.QName("", "DescripcionMaterial");
                private static final javax.xml.namespace.QName NEGOCIO$10 = 
                    new javax.xml.namespace.QName("", "Negocio");
                private static final javax.xml.namespace.QName FECHAHORATRANSACCION$12 = 
                    new javax.xml.namespace.QName("", "FechaHoraTransaccion");
                private static final javax.xml.namespace.QName PLANTADESPACHO$14 = 
                    new javax.xml.namespace.QName("", "PlantaDespacho");
                private static final javax.xml.namespace.QName NORESERVA$16 = 
                    new javax.xml.namespace.QName("", "NoReserva");
                private static final javax.xml.namespace.QName NUMERODESERIE$18 = 
                    new javax.xml.namespace.QName("", "NumeroDeSerie");
                private static final javax.xml.namespace.QName OTERP$20 = 
                    new javax.xml.namespace.QName("", "OT_ERP");
                private static final javax.xml.namespace.QName OTORIGEN$22 = 
                    new javax.xml.namespace.QName("", "OT_Origen");
                private static final javax.xml.namespace.QName SECUENCIA$24 = 
                    new javax.xml.namespace.QName("", "Secuencia");
                private static final javax.xml.namespace.QName TIPODESPACHO$26 = 
                    new javax.xml.namespace.QName("", "TipoDespacho");
                private static final javax.xml.namespace.QName IDENTIFICADORITEM$28 = 
                    new javax.xml.namespace.QName("", "IdentificadorItem");
                
                
                /**
                 * Gets the "Almacen" element
                 */
                public java.lang.String getAlmacen()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ALMACEN$0, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "Almacen" element
                 */
                public org.apache.xmlbeans.XmlString xgetAlmacen()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ALMACEN$0, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "Almacen" element
                 */
                public void setAlmacen(java.lang.String almacen)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ALMACEN$0, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ALMACEN$0);
                      }
                      target.setStringValue(almacen);
                    }
                }
                
                /**
                 * Sets (as xml) the "Almacen" element
                 */
                public void xsetAlmacen(org.apache.xmlbeans.XmlString almacen)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ALMACEN$0, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ALMACEN$0);
                      }
                      target.set(almacen);
                    }
                }
                
                /**
                 * Gets the "CantidadEntregada" element
                 */
                public java.math.BigDecimal getCantidadEntregada()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CANTIDADENTREGADA$2, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getBigDecimalValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "CantidadEntregada" element
                 */
                public org.apache.xmlbeans.XmlDecimal xgetCantidadEntregada()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlDecimal target = null;
                      target = (org.apache.xmlbeans.XmlDecimal)get_store().find_element_user(CANTIDADENTREGADA$2, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "CantidadEntregada" element
                 */
                public void setCantidadEntregada(java.math.BigDecimal cantidadEntregada)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CANTIDADENTREGADA$2, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CANTIDADENTREGADA$2);
                      }
                      target.setBigDecimalValue(cantidadEntregada);
                    }
                }
                
                /**
                 * Sets (as xml) the "CantidadEntregada" element
                 */
                public void xsetCantidadEntregada(org.apache.xmlbeans.XmlDecimal cantidadEntregada)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlDecimal target = null;
                      target = (org.apache.xmlbeans.XmlDecimal)get_store().find_element_user(CANTIDADENTREGADA$2, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlDecimal)get_store().add_element_user(CANTIDADENTREGADA$2);
                      }
                      target.set(cantidadEntregada);
                    }
                }
                
                /**
                 * Gets the "CodigoMaterial" element
                 */
                public java.lang.String getCodigoMaterial()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODIGOMATERIAL$4, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "CodigoMaterial" element
                 */
                public org.apache.xmlbeans.XmlString xgetCodigoMaterial()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODIGOMATERIAL$4, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "CodigoMaterial" element
                 */
                public void setCodigoMaterial(java.lang.String codigoMaterial)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODIGOMATERIAL$4, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CODIGOMATERIAL$4);
                      }
                      target.setStringValue(codigoMaterial);
                    }
                }
                
                /**
                 * Sets (as xml) the "CodigoMaterial" element
                 */
                public void xsetCodigoMaterial(org.apache.xmlbeans.XmlString codigoMaterial)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODIGOMATERIAL$4, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CODIGOMATERIAL$4);
                      }
                      target.set(codigoMaterial);
                    }
                }
                
                /**
                 * Gets the "CostoTransaccion" element
                 */
                public java.math.BigDecimal getCostoTransaccion()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COSTOTRANSACCION$6, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getBigDecimalValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "CostoTransaccion" element
                 */
                public org.apache.xmlbeans.XmlDecimal xgetCostoTransaccion()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlDecimal target = null;
                      target = (org.apache.xmlbeans.XmlDecimal)get_store().find_element_user(COSTOTRANSACCION$6, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "CostoTransaccion" element
                 */
                public void setCostoTransaccion(java.math.BigDecimal costoTransaccion)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COSTOTRANSACCION$6, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COSTOTRANSACCION$6);
                      }
                      target.setBigDecimalValue(costoTransaccion);
                    }
                }
                
                /**
                 * Sets (as xml) the "CostoTransaccion" element
                 */
                public void xsetCostoTransaccion(org.apache.xmlbeans.XmlDecimal costoTransaccion)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlDecimal target = null;
                      target = (org.apache.xmlbeans.XmlDecimal)get_store().find_element_user(COSTOTRANSACCION$6, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlDecimal)get_store().add_element_user(COSTOTRANSACCION$6);
                      }
                      target.set(costoTransaccion);
                    }
                }
                
                /**
                 * Gets the "DescripcionMaterial" element
                 */
                public java.lang.String getDescripcionMaterial()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPCIONMATERIAL$8, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "DescripcionMaterial" element
                 */
                public org.apache.xmlbeans.XmlString xgetDescripcionMaterial()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONMATERIAL$8, 0);
                      return target;
                    }
                }
                
                /**
                 * True if has "DescripcionMaterial" element
                 */
                public boolean isSetDescripcionMaterial()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      return get_store().count_elements(DESCRIPCIONMATERIAL$8) != 0;
                    }
                }
                
                /**
                 * Sets the "DescripcionMaterial" element
                 */
                public void setDescripcionMaterial(java.lang.String descripcionMaterial)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPCIONMATERIAL$8, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPCIONMATERIAL$8);
                      }
                      target.setStringValue(descripcionMaterial);
                    }
                }
                
                /**
                 * Sets (as xml) the "DescripcionMaterial" element
                 */
                public void xsetDescripcionMaterial(org.apache.xmlbeans.XmlString descripcionMaterial)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPCIONMATERIAL$8, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPCIONMATERIAL$8);
                      }
                      target.set(descripcionMaterial);
                    }
                }
                
                /**
                 * Unsets the "DescripcionMaterial" element
                 */
                public void unsetDescripcionMaterial()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      get_store().remove_element(DESCRIPCIONMATERIAL$8, 0);
                    }
                }
                
                /**
                 * Gets the "Negocio" element
                 */
                public java.lang.String getNegocio()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NEGOCIO$10, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "Negocio" element
                 */
                public org.apache.xmlbeans.XmlString xgetNegocio()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NEGOCIO$10, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "Negocio" element
                 */
                public void setNegocio(java.lang.String negocio)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NEGOCIO$10, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NEGOCIO$10);
                      }
                      target.setStringValue(negocio);
                    }
                }
                
                /**
                 * Sets (as xml) the "Negocio" element
                 */
                public void xsetNegocio(org.apache.xmlbeans.XmlString negocio)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NEGOCIO$10, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NEGOCIO$10);
                      }
                      target.set(negocio);
                    }
                }
                
                /**
                 * Gets the "FechaHoraTransaccion" element
                 */
                public java.util.Calendar getFechaHoraTransaccion()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FECHAHORATRANSACCION$12, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getCalendarValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "FechaHoraTransaccion" element
                 */
                public org.apache.xmlbeans.XmlDateTime xgetFechaHoraTransaccion()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlDateTime target = null;
                      target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(FECHAHORATRANSACCION$12, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "FechaHoraTransaccion" element
                 */
                public void setFechaHoraTransaccion(java.util.Calendar fechaHoraTransaccion)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FECHAHORATRANSACCION$12, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FECHAHORATRANSACCION$12);
                      }
                      target.setCalendarValue(fechaHoraTransaccion);
                    }
                }
                
                /**
                 * Sets (as xml) the "FechaHoraTransaccion" element
                 */
                public void xsetFechaHoraTransaccion(org.apache.xmlbeans.XmlDateTime fechaHoraTransaccion)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlDateTime target = null;
                      target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(FECHAHORATRANSACCION$12, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(FECHAHORATRANSACCION$12);
                      }
                      target.set(fechaHoraTransaccion);
                    }
                }
                
                /**
                 * Gets the "PlantaDespacho" element
                 */
                public java.lang.String getPlantaDespacho()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PLANTADESPACHO$14, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "PlantaDespacho" element
                 */
                public org.apache.xmlbeans.XmlString xgetPlantaDespacho()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PLANTADESPACHO$14, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "PlantaDespacho" element
                 */
                public void setPlantaDespacho(java.lang.String plantaDespacho)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PLANTADESPACHO$14, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PLANTADESPACHO$14);
                      }
                      target.setStringValue(plantaDespacho);
                    }
                }
                
                /**
                 * Sets (as xml) the "PlantaDespacho" element
                 */
                public void xsetPlantaDespacho(org.apache.xmlbeans.XmlString plantaDespacho)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PLANTADESPACHO$14, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PLANTADESPACHO$14);
                      }
                      target.set(plantaDespacho);
                    }
                }
                
                /**
                 * Gets the "NoReserva" element
                 */
                public java.lang.String getNoReserva()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NORESERVA$16, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "NoReserva" element
                 */
                public org.apache.xmlbeans.XmlString xgetNoReserva()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NORESERVA$16, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "NoReserva" element
                 */
                public void setNoReserva(java.lang.String noReserva)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NORESERVA$16, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NORESERVA$16);
                      }
                      target.setStringValue(noReserva);
                    }
                }
                
                /**
                 * Sets (as xml) the "NoReserva" element
                 */
                public void xsetNoReserva(org.apache.xmlbeans.XmlString noReserva)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NORESERVA$16, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NORESERVA$16);
                      }
                      target.set(noReserva);
                    }
                }
                
                /**
                 * Gets the "NumeroDeSerie" element
                 */
                public java.lang.String getNumeroDeSerie()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NUMERODESERIE$18, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "NumeroDeSerie" element
                 */
                public org.apache.xmlbeans.XmlString xgetNumeroDeSerie()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NUMERODESERIE$18, 0);
                      return target;
                    }
                }
                
                /**
                 * True if has "NumeroDeSerie" element
                 */
                public boolean isSetNumeroDeSerie()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      return get_store().count_elements(NUMERODESERIE$18) != 0;
                    }
                }
                
                /**
                 * Sets the "NumeroDeSerie" element
                 */
                public void setNumeroDeSerie(java.lang.String numeroDeSerie)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NUMERODESERIE$18, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NUMERODESERIE$18);
                      }
                      target.setStringValue(numeroDeSerie);
                    }
                }
                
                /**
                 * Sets (as xml) the "NumeroDeSerie" element
                 */
                public void xsetNumeroDeSerie(org.apache.xmlbeans.XmlString numeroDeSerie)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(NUMERODESERIE$18, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(NUMERODESERIE$18);
                      }
                      target.set(numeroDeSerie);
                    }
                }
                
                /**
                 * Unsets the "NumeroDeSerie" element
                 */
                public void unsetNumeroDeSerie()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      get_store().remove_element(NUMERODESERIE$18, 0);
                    }
                }
                
                /**
                 * Gets the "OT_ERP" element
                 */
                public java.lang.String getOTERP()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OTERP$20, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "OT_ERP" element
                 */
                public org.apache.xmlbeans.XmlString xgetOTERP()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(OTERP$20, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "OT_ERP" element
                 */
                public void setOTERP(java.lang.String oterp)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OTERP$20, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OTERP$20);
                      }
                      target.setStringValue(oterp);
                    }
                }
                
                /**
                 * Sets (as xml) the "OT_ERP" element
                 */
                public void xsetOTERP(org.apache.xmlbeans.XmlString oterp)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(OTERP$20, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(OTERP$20);
                      }
                      target.set(oterp);
                    }
                }
                
                /**
                 * Gets the "OT_Origen" element
                 */
                public java.lang.String getOTOrigen()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OTORIGEN$22, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "OT_Origen" element
                 */
                public org.apache.xmlbeans.XmlString xgetOTOrigen()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(OTORIGEN$22, 0);
                      return target;
                    }
                }
                
                /**
                 * True if has "OT_Origen" element
                 */
                public boolean isSetOTOrigen()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      return get_store().count_elements(OTORIGEN$22) != 0;
                    }
                }
                
                /**
                 * Sets the "OT_Origen" element
                 */
                public void setOTOrigen(java.lang.String otOrigen)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OTORIGEN$22, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OTORIGEN$22);
                      }
                      target.setStringValue(otOrigen);
                    }
                }
                
                /**
                 * Sets (as xml) the "OT_Origen" element
                 */
                public void xsetOTOrigen(org.apache.xmlbeans.XmlString otOrigen)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(OTORIGEN$22, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(OTORIGEN$22);
                      }
                      target.set(otOrigen);
                    }
                }
                
                /**
                 * Unsets the "OT_Origen" element
                 */
                public void unsetOTOrigen()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      get_store().remove_element(OTORIGEN$22, 0);
                    }
                }
                
                /**
                 * Gets the "Secuencia" element
                 */
                public long getSecuencia()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECUENCIA$24, 0);
                      if (target == null)
                      {
                        return 0L;
                      }
                      return target.getLongValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "Secuencia" element
                 */
                public org.apache.xmlbeans.XmlLong xgetSecuencia()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlLong target = null;
                      target = (org.apache.xmlbeans.XmlLong)get_store().find_element_user(SECUENCIA$24, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "Secuencia" element
                 */
                public void setSecuencia(long secuencia)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECUENCIA$24, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECUENCIA$24);
                      }
                      target.setLongValue(secuencia);
                    }
                }
                
                /**
                 * Sets (as xml) the "Secuencia" element
                 */
                public void xsetSecuencia(org.apache.xmlbeans.XmlLong secuencia)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlLong target = null;
                      target = (org.apache.xmlbeans.XmlLong)get_store().find_element_user(SECUENCIA$24, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlLong)get_store().add_element_user(SECUENCIA$24);
                      }
                      target.set(secuencia);
                    }
                }
                
                /**
                 * Gets the "TipoDespacho" element
                 */
                public java.lang.String getTipoDespacho()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPODESPACHO$26, 0);
                      if (target == null)
                      {
                        return null;
                      }
                      return target.getStringValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "TipoDespacho" element
                 */
                public org.apache.xmlbeans.XmlString xgetTipoDespacho()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPODESPACHO$26, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "TipoDespacho" element
                 */
                public void setTipoDespacho(java.lang.String tipoDespacho)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIPODESPACHO$26, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIPODESPACHO$26);
                      }
                      target.setStringValue(tipoDespacho);
                    }
                }
                
                /**
                 * Sets (as xml) the "TipoDespacho" element
                 */
                public void xsetTipoDespacho(org.apache.xmlbeans.XmlString tipoDespacho)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlString target = null;
                      target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIPODESPACHO$26, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIPODESPACHO$26);
                      }
                      target.set(tipoDespacho);
                    }
                }
                
                /**
                 * Gets the "IdentificadorItem" element
                 */
                public long getIdentificadorItem()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDENTIFICADORITEM$28, 0);
                      if (target == null)
                      {
                        return 0L;
                      }
                      return target.getLongValue();
                    }
                }
                
                /**
                 * Gets (as xml) the "IdentificadorItem" element
                 */
                public org.apache.xmlbeans.XmlLong xgetIdentificadorItem()
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlLong target = null;
                      target = (org.apache.xmlbeans.XmlLong)get_store().find_element_user(IDENTIFICADORITEM$28, 0);
                      return target;
                    }
                }
                
                /**
                 * Sets the "IdentificadorItem" element
                 */
                public void setIdentificadorItem(long identificadorItem)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.SimpleValue target = null;
                      target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDENTIFICADORITEM$28, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDENTIFICADORITEM$28);
                      }
                      target.setLongValue(identificadorItem);
                    }
                }
                
                /**
                 * Sets (as xml) the "IdentificadorItem" element
                 */
                public void xsetIdentificadorItem(org.apache.xmlbeans.XmlLong identificadorItem)
                {
                    synchronized (monitor())
                    {
                      check_orphaned();
                      org.apache.xmlbeans.XmlLong target = null;
                      target = (org.apache.xmlbeans.XmlLong)get_store().find_element_user(IDENTIFICADORITEM$28, 0);
                      if (target == null)
                      {
                        target = (org.apache.xmlbeans.XmlLong)get_store().add_element_user(IDENTIFICADORITEM$28);
                      }
                      target.set(identificadorItem);
                    }
                }
            }
        }
    }
}
