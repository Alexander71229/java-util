/*
 * An XML document type.
 * Localname: RespuestaDespachoMateriales
 * Namespace: http://EPM.Materiales.BizTalk.Schemas.Respuesta
 * Java type: respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument
 *
 * Automatically generated - do not modify.
 */
package respuesta.schemas.biztalk.materiales.epm;


/**
 * A document containing one RespuestaDespachoMateriales(@http://EPM.Materiales.BizTalk.Schemas.Respuesta) element.
 *
 * This is a complex type.
 */
public interface RespuestaDespachoMaterialesDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(RespuestaDespachoMaterialesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s3CCBD10005EAD96CF7579BB81801A7F6").resolveHandle("respuestadespachomateriales2dbddoctype");
    
    /**
     * Gets the "RespuestaDespachoMateriales" element
     */
    respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales getRespuestaDespachoMateriales();
    
    /**
     * Sets the "RespuestaDespachoMateriales" element
     */
    void setRespuestaDespachoMateriales(respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales respuestaDespachoMateriales);
    
    /**
     * Appends and returns a new empty "RespuestaDespachoMateriales" element
     */
    respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales addNewRespuestaDespachoMateriales();
    
    /**
     * An XML RespuestaDespachoMateriales(@http://EPM.Materiales.BizTalk.Schemas.Respuesta).
     *
     * This is a complex type.
     */
    public interface RespuestaDespachoMateriales extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(RespuestaDespachoMateriales.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s3CCBD10005EAD96CF7579BB81801A7F6").resolveHandle("respuestadespachomaterialesbba5elemtype");
        
        /**
         * Gets the "creationDateTime" element
         */
        java.util.Calendar getCreationDateTime();
        
        /**
         * Gets (as xml) the "creationDateTime" element
         */
        org.apache.xmlbeans.XmlDateTime xgetCreationDateTime();
        
        /**
         * Sets the "creationDateTime" element
         */
        void setCreationDateTime(java.util.Calendar creationDateTime);
        
        /**
         * Sets (as xml) the "creationDateTime" element
         */
        void xsetCreationDateTime(org.apache.xmlbeans.XmlDateTime creationDateTime);
        
        /**
         * Gets the "baseLanguage" element
         */
        java.lang.String getBaseLanguage();
        
        /**
         * Gets (as xml) the "baseLanguage" element
         */
        org.apache.xmlbeans.XmlString xgetBaseLanguage();
        
        /**
         * Sets the "baseLanguage" element
         */
        void setBaseLanguage(java.lang.String baseLanguage);
        
        /**
         * Sets (as xml) the "baseLanguage" element
         */
        void xsetBaseLanguage(org.apache.xmlbeans.XmlString baseLanguage);
        
        /**
         * Gets the "transLanguage" element
         */
        java.lang.String getTransLanguage();
        
        /**
         * Gets (as xml) the "transLanguage" element
         */
        org.apache.xmlbeans.XmlString xgetTransLanguage();
        
        /**
         * Sets the "transLanguage" element
         */
        void setTransLanguage(java.lang.String transLanguage);
        
        /**
         * Sets (as xml) the "transLanguage" element
         */
        void xsetTransLanguage(org.apache.xmlbeans.XmlString transLanguage);
        
        /**
         * Gets the "messageID" element
         */
        java.lang.String getMessageID();
        
        /**
         * Gets (as xml) the "messageID" element
         */
        org.apache.xmlbeans.XmlString xgetMessageID();
        
        /**
         * Sets the "messageID" element
         */
        void setMessageID(java.lang.String messageID);
        
        /**
         * Sets (as xml) the "messageID" element
         */
        void xsetMessageID(org.apache.xmlbeans.XmlString messageID);
        
        /**
         * Gets the "maximoVersion" element
         */
        java.lang.String getMaximoVersion();
        
        /**
         * Gets (as xml) the "maximoVersion" element
         */
        org.apache.xmlbeans.XmlString xgetMaximoVersion();
        
        /**
         * Sets the "maximoVersion" element
         */
        void setMaximoVersion(java.lang.String maximoVersion);
        
        /**
         * Sets (as xml) the "maximoVersion" element
         */
        void xsetMaximoVersion(org.apache.xmlbeans.XmlString maximoVersion);
        
        /**
         * Gets the "Description" element
         */
        java.lang.String getDescription();
        
        /**
         * Gets (as xml) the "Description" element
         */
        org.apache.xmlbeans.XmlString xgetDescription();
        
        /**
         * Sets the "Description" element
         */
        void setDescription(java.lang.String description);
        
        /**
         * Sets (as xml) the "Description" element
         */
        void xsetDescription(org.apache.xmlbeans.XmlString description);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales newInstance() {
              return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument newInstance() {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
