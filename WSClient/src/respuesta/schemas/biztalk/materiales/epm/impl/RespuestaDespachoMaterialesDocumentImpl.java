/*
 * An XML document type.
 * Localname: RespuestaDespachoMateriales
 * Namespace: http://EPM.Materiales.BizTalk.Schemas.Respuesta
 * Java type: respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument
 *
 * Automatically generated - do not modify.
 */
package respuesta.schemas.biztalk.materiales.epm.impl;
/**
 * A document containing one RespuestaDespachoMateriales(@http://EPM.Materiales.BizTalk.Schemas.Respuesta) element.
 *
 * This is a complex type.
 */
public class RespuestaDespachoMaterialesDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument
{
    private static final long serialVersionUID = 1L;
    
    public RespuestaDespachoMaterialesDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RESPUESTADESPACHOMATERIALES$0 = 
        new javax.xml.namespace.QName("http://EPM.Materiales.BizTalk.Schemas.Respuesta", "RespuestaDespachoMateriales");
    
    
    /**
     * Gets the "RespuestaDespachoMateriales" element
     */
    public respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales getRespuestaDespachoMateriales()
    {
        synchronized (monitor())
        {
            check_orphaned();
            respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales target = null;
            target = (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales)get_store().find_element_user(RESPUESTADESPACHOMATERIALES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "RespuestaDespachoMateriales" element
     */
    public void setRespuestaDespachoMateriales(respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales respuestaDespachoMateriales)
    {
        generatedSetterHelperImpl(respuestaDespachoMateriales, RESPUESTADESPACHOMATERIALES$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "RespuestaDespachoMateriales" element
     */
    public respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales addNewRespuestaDespachoMateriales()
    {
        synchronized (monitor())
        {
            check_orphaned();
            respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales target = null;
            target = (respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales)get_store().add_element_user(RESPUESTADESPACHOMATERIALES$0);
            return target;
        }
    }
    /**
     * An XML RespuestaDespachoMateriales(@http://EPM.Materiales.BizTalk.Schemas.Respuesta).
     *
     * This is a complex type.
     */
    public static class RespuestaDespachoMaterialesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements respuesta.schemas.biztalk.materiales.epm.RespuestaDespachoMaterialesDocument.RespuestaDespachoMateriales
    {
        private static final long serialVersionUID = 1L;
        
        public RespuestaDespachoMaterialesImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CREATIONDATETIME$0 = 
            new javax.xml.namespace.QName("", "creationDateTime");
        private static final javax.xml.namespace.QName BASELANGUAGE$2 = 
            new javax.xml.namespace.QName("", "baseLanguage");
        private static final javax.xml.namespace.QName TRANSLANGUAGE$4 = 
            new javax.xml.namespace.QName("", "transLanguage");
        private static final javax.xml.namespace.QName MESSAGEID$6 = 
            new javax.xml.namespace.QName("", "messageID");
        private static final javax.xml.namespace.QName MAXIMOVERSION$8 = 
            new javax.xml.namespace.QName("", "maximoVersion");
        private static final javax.xml.namespace.QName DESCRIPTION$10 = 
            new javax.xml.namespace.QName("", "Description");
        
        
        /**
         * Gets the "creationDateTime" element
         */
        public java.util.Calendar getCreationDateTime()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREATIONDATETIME$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "creationDateTime" element
         */
        public org.apache.xmlbeans.XmlDateTime xgetCreationDateTime()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(CREATIONDATETIME$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "creationDateTime" element
         */
        public void setCreationDateTime(java.util.Calendar creationDateTime)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREATIONDATETIME$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CREATIONDATETIME$0);
                }
                target.setCalendarValue(creationDateTime);
            }
        }
        
        /**
         * Sets (as xml) the "creationDateTime" element
         */
        public void xsetCreationDateTime(org.apache.xmlbeans.XmlDateTime creationDateTime)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(CREATIONDATETIME$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(CREATIONDATETIME$0);
                }
                target.set(creationDateTime);
            }
        }
        
        /**
         * Gets the "baseLanguage" element
         */
        public java.lang.String getBaseLanguage()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(BASELANGUAGE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "baseLanguage" element
         */
        public org.apache.xmlbeans.XmlString xgetBaseLanguage()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(BASELANGUAGE$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "baseLanguage" element
         */
        public void setBaseLanguage(java.lang.String baseLanguage)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(BASELANGUAGE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(BASELANGUAGE$2);
                }
                target.setStringValue(baseLanguage);
            }
        }
        
        /**
         * Sets (as xml) the "baseLanguage" element
         */
        public void xsetBaseLanguage(org.apache.xmlbeans.XmlString baseLanguage)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(BASELANGUAGE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(BASELANGUAGE$2);
                }
                target.set(baseLanguage);
            }
        }
        
        /**
         * Gets the "transLanguage" element
         */
        public java.lang.String getTransLanguage()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRANSLANGUAGE$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "transLanguage" element
         */
        public org.apache.xmlbeans.XmlString xgetTransLanguage()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TRANSLANGUAGE$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "transLanguage" element
         */
        public void setTransLanguage(java.lang.String transLanguage)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRANSLANGUAGE$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TRANSLANGUAGE$4);
                }
                target.setStringValue(transLanguage);
            }
        }
        
        /**
         * Sets (as xml) the "transLanguage" element
         */
        public void xsetTransLanguage(org.apache.xmlbeans.XmlString transLanguage)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TRANSLANGUAGE$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TRANSLANGUAGE$4);
                }
                target.set(transLanguage);
            }
        }
        
        /**
         * Gets the "messageID" element
         */
        public java.lang.String getMessageID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGEID$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "messageID" element
         */
        public org.apache.xmlbeans.XmlString xgetMessageID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MESSAGEID$6, 0);
                return target;
            }
        }
        
        /**
         * Sets the "messageID" element
         */
        public void setMessageID(java.lang.String messageID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGEID$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MESSAGEID$6);
                }
                target.setStringValue(messageID);
            }
        }
        
        /**
         * Sets (as xml) the "messageID" element
         */
        public void xsetMessageID(org.apache.xmlbeans.XmlString messageID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MESSAGEID$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MESSAGEID$6);
                }
                target.set(messageID);
            }
        }
        
        /**
         * Gets the "maximoVersion" element
         */
        public java.lang.String getMaximoVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAXIMOVERSION$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "maximoVersion" element
         */
        public org.apache.xmlbeans.XmlString xgetMaximoVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAXIMOVERSION$8, 0);
                return target;
            }
        }
        
        /**
         * Sets the "maximoVersion" element
         */
        public void setMaximoVersion(java.lang.String maximoVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAXIMOVERSION$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MAXIMOVERSION$8);
                }
                target.setStringValue(maximoVersion);
            }
        }
        
        /**
         * Sets (as xml) the "maximoVersion" element
         */
        public void xsetMaximoVersion(org.apache.xmlbeans.XmlString maximoVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MAXIMOVERSION$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MAXIMOVERSION$8);
                }
                target.set(maximoVersion);
            }
        }
        
        /**
         * Gets the "Description" element
         */
        public java.lang.String getDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Description" element
         */
        public org.apache.xmlbeans.XmlString xgetDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$10, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Description" element
         */
        public void setDescription(java.lang.String description)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPTION$10);
                }
                target.setStringValue(description);
            }
        }
        
        /**
         * Sets (as xml) the "Description" element
         */
        public void xsetDescription(org.apache.xmlbeans.XmlString description)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPTION$10);
                }
                target.set(description);
            }
        }
    }
}
