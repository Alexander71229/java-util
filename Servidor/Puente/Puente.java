import java.net.*;
import java.io.*;
import java.util.*;
public class Puente extends Thread{
	public static PrintStream ps;
	public static FileOutputStream fos;
	//public String servidor="10.1.62.163"; //localhost
	//public String servidor="localhost"; //localhost
	public String servidor="127.0.0.1"; //localhost
	//public String servidor="127.000.000.001";
	public int puerto=5101;
	//public String servidorDestino="216.58.222.196";//www.google.com
	//public String servidorDestino="10.1.20.141";//test-wap001.test.epm.com.co
	//public int puertoDestino=80;
	//public String servidorDestino="localhost";//Servidor de prueba
	//public int puertoDestino=8282;
	//public String servidorDestino="10.1.20.139";//test-wap039.test.epm.com.co
	//public String servidorDestino="10.1.16.24";//epmapd09-03 Biztalk
	//public String servidorDestino="010.001.016.024";//epmapd09-03 Biztalk
	//public String servidorDestino="epm-ait56";
	//public String servidorDestino="epm-po19";
	public String servidorDestino="epm-ait56";
	//public String servidorDestino="10.1.20.139"; //"test-wap039.test.epm.com.co";
	//public String servidorDestino="epm-ait56";
	//public String servidorDestino="190.248.37.152"; //procinal
	//public String servidorDestino="test-wap040.test.epm.com.co";
	public int puertoDestino=5101;
	public void run(){
		try{
			ServerSocket serverSocket=new ServerSocket(puerto);
			while(true){
				Socket socket=serverSocket.accept();
				atenderCliente(socket);
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
	public byte[]leer2(InputStream is)throws Exception{
		//StringBuilder resultado=new StringBuilder(50000);
		ArrayList<Byte>resultado=new ArrayList<Byte>(50000);
		int dato=0;
		int l=0;
		int e=0;
		String propiedad="";
		String valor="";
		HashMap<String,String>h=new HashMap<String,String>();
		int len=0;
		while((dato=is.read())>=0){
			//resultado.append((char)dato);
			resultado.add((byte)dato);
			Puente.imprimir(((char)dato)+"");
			if(e==0){
				if(dato==10){
					if(propiedad.equals("")){
						l=0;
						if(h.get("Content-Length")!=null){
							len=Integer.parseInt(h.get("Content-Length"));
							if(len==0){
								break;
							}
						}else{
							if(h.get("Transfer-Encoding")==null){
								break;
							}
						}
						e=3;
					}
					propiedad="";
					continue;
				}
				if(dato==':'){
					e=1;
					continue;
				}else{
					if(dato!=10&&dato!=13){
						propiedad+=(char)dato;
						continue;
					}
				}
			}
			if(e==1){
				if(dato==10){
					h.put(propiedad,valor.trim());
					propiedad="";
					valor="";
					e=0;
					continue;
				}else{
					if(dato!=13){
						valor+=(char)dato;
						continue;
					}
				}
			}
			if(e==3){
				l++;
				if(len>0){
					if(l==len){
						break;
					}
				}else{
					if(dato==13){
						e=4;
						continue;
					}
				}
			}
			if(e==4){
				if(dato=='0'){
					e=5;
					continue;
				}
				if(dato!=13&&dato!=10){
					e=3;
					continue;
				}
			}
			if(e==5){
				if(dato==13){
					dato=is.read();
					//resultado.append((char)dato);
					resultado.add((byte)dato);
					dato=is.read();
					//resultado.append((char)dato);
					resultado.add((byte)dato);
					dato=is.read();
					//resultado.append((char)dato);
					resultado.add((byte)dato);
					break;
				}
				if(dato!=10){
					e=3;
					continue;
				}
			}
		}
		//return resultado.toString();
		return convert(resultado);
	}
	public byte[]convert(ArrayList<Byte>datos)throws Exception{
		byte[]resultado=new byte[datos.size()];
		for(int i=0;i<datos.size();i++){
			resultado[i]=(byte)datos.get(i);
		}
		return resultado;
	}
	public String convert2(byte[]datos)throws Exception{
		return new String(datos);
	}
	public String leer1(InputStream is)throws Exception{
		BufferedReader entrada=new BufferedReader(new InputStreamReader(is));
		String l=".";
		String m="";
		while(l.length()>0){
			l=entrada.readLine();
			m+=l;
		}
		return m;
	}
	public String leer(InputStream is,int l)throws Exception{
		String r="";
		int dato=0;
		while(r.length()<l&&(dato=is.read())>=0){
			r+=(char)dato;
		}
		return r;
	}
	public String leer(InputStream is)throws Exception{
		String m="";
		Scanner s=new Scanner(is);
		int len=-1;
		while(s.hasNextLine()){
			String l=s.nextLine();
			Puente.imprimir(l+"\n");
			m+=l+"\n";
			String[]d=l.split(": ");
			if(d!=null&&d.length>1&&d[0].equals("Content-Length")){
				len=Integer.parseInt(d[1]);
				return m+leer(is,len);
			}
			if(l.isEmpty()&&len<=0){
				break;
			}
		}
		return m;
	}
	public void escribir(OutputStream os,String mensaje)throws Exception{
		PrintStream ps=new PrintStream(os);
		ps.println(mensaje);
	}
	public void escribir(OutputStream os,byte[]datos)throws Exception{
		os.write(datos);
	}
	public void atenderCliente(Socket socket){
		try{
			System.out.println("atenderCliente");
			Puente.imprimir("\n[Request]\n");
			Puente.imprimir("\n[Request]\n".getBytes());
			Temporizador.agregar(0,10000,"T 0",true);
			byte[]m=leer2(socket.getInputStream());
			Temporizador.detener(0);
			//String m2=convert2(m).replace("epmapd09-03.corp.epm.com.co:80","localhost:00000000000000008383");
			//String m2=convert2(m);
			Puente.imprimir(m);
			Socket sd=new Socket(this.servidorDestino,this.puertoDestino);
			escribir(sd.getOutputStream(),m);
			Puente.imprimir("\n[Response]\n".getBytes());
			Temporizador.agregar(1,10000,"T 1",true);
			m=leer2(sd.getInputStream());
			Temporizador.detener(1);
			//m2=convert2(m).replace("epmapd09-03.corp.epm.com.co:80","localhost:00000000000000008383");
			Puente.imprimir(m);
			escribir(socket.getOutputStream(),m);
			socket.close();
			sd.close();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
	public static void imprimir(String m)throws Exception{
		/*if(ps==null){
			ps=new PrintStream(new FileOutputStream(new File("Log.txt")));
		}
		ps.print(m);*/
	}
	public static void imprimir(byte[]datos)throws Exception{
		if(fos==null){
			fos=new FileOutputStream(new File("Binario.txt"));
		}
		fos.write(datos);
	}
	public String toString(){
		return this.servidor+":"+this.puerto+"->"+this.servidorDestino+":"+this.puertoDestino;
	}
	public static void main(String[]argumentos){
		try{
			Puente puente=new Puente();
			puente.start();
			System.out.println("Esperando\n"+puente);
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}