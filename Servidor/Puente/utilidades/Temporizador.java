package utilidades;
import java.util.*;
public class Temporizador extends Thread{
	public int tiempo;
	public String mensaje;
	public boolean detenido=false;
	public String traza;
	public static HashMap<String,Temporizador>hash;
	public boolean conTraza=true;
	public Temporizador(int tiempo,String mensaje){
		this.tiempo=tiempo;
		this.mensaje=mensaje;
		this.traza=utilidades.General.traza(1);
		start();
	}
	public Temporizador(int tiempo,String mensaje,boolean conTraza){
		this.tiempo=tiempo;
		this.mensaje=mensaje;
		this.traza=utilidades.General.traza(1);
		this.conTraza=conTraza;
		start();
	}
	public void run(){
		try{
			sleep(this.tiempo);
			if(!detenido){
				if(this.conTraza){
					utilidades.General.imprimir("Timeout:"+this.tiempo+"\n"+this.traza+"\n"+this.mensaje);
				}else{
					utilidades.General.imprimir("Timeout:"+this.tiempo+"\n"+this.mensaje);
				}
			}
		}catch(Throwable t){
		}
	}
	public void detener(){
		detenido=true;
	}
	public static void agregar(String id,int tiempo,String mensaje,boolean conTraza){
		try{
			if(hash==null){
				hash=new HashMap<String,Temporizador>();
			}
			hash.put(id,new Temporizador(tiempo,mensaje,conTraza));
		}catch(Throwable t){
		}
	}
	public static void detener(String id){
		try{
			hash.get(id).detener();
			hash.remove(id);
		}catch(Throwable t){
		}
	}
	public static void agregar(int id,int tiempo,String mensaje,boolean conTraza){
		agregar(id+"",tiempo,mensaje,conTraza);
	}
	public static void detener(int id){
		detener(id+"");
	}
	public static void agregar(String id,int tiempo,String mensaje){
		agregar(id,tiempo,mensaje,true);
	}
	public static void agregar(int id,int tiempo,String mensaje){
		agregar(id+"",tiempo,mensaje,true);
	}
}