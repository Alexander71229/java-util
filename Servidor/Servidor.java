import java.util.*;
import java.io.*;
import java.net.*;
public class Servidor extends Thread{
	public int puerto;
	public Servidor(int puerto)throws Exception{
		this.puerto=puerto;
	}
	public void run(){
		BufferedReader entrada;
		PrintStream salida;
		try{
			ServerSocket serverSocket=new ServerSocket(puerto);
			while(true){
				Socket socket=serverSocket.accept();
				entrada=new BufferedReader(new InputStreamReader(socket.getInputStream()));
				salida=new PrintStream(socket.getOutputStream());
				String texto="";
				String leer="";
				while(true){
					leer=entrada.readLine();
					if(leer.length()<=0){
						break;
					}
					texto+=leer;
				}
				System.out.println("Le�do:"+texto);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void main(String[]args){
		try{
			Servidor servidor=new Servidor(8282);
			servidor.start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}