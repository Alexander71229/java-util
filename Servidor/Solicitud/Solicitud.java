import java.util.*;
import java.io.*;
import java.net.*;
public class Solicitud{
	/*public byte[]leerArchivo(String archivo)throws Exception{
		return Files.readAllBytes(FileSystems.getDefault().getPath(archivo));
	}*/
	public static byte[]leerArchivo(String archivo)throws Exception{
		FileInputStream fis=new FileInputStream(new File(archivo));
		byte[]r=new byte[fis.available()];
		fis.read(r);
		return r;
	}
	public static void enviarMensaje(Socket s,String mensaje)throws Exception{
		s.getOutputStream().write(mensaje.getBytes());
	}
	public static void enviarMensaje(Socket s,byte[]mensaje)throws Exception{
		s.getOutputStream().write(mensaje);
	}
	public static byte[]leer2(InputStream is,PrintStream ps)throws Exception{
		ArrayList<Byte>resultado=new ArrayList<Byte>(50000);
		int dato=0;
		int l=0;
		int e=0;
		String propiedad="";
		String valor="";
		HashMap<String,String>h=new HashMap<String,String>();
		int len=0;
		while((dato=is.read())>=0){
			resultado.add((byte)dato);
			//ps.println((char)dato);
			if(e==0){
				if(dato==10){
					if(propiedad.equals("")){
						l=0;
						if(h.get("Content-Length")!=null){
							len=Integer.parseInt(h.get("Content-Length"));
							if(len==0){
								break;
							}
						}else{
							if(h.get("Transfer-Encoding")==null){
								break;
							}
						}
						e=3;
					}
					propiedad="";
					continue;
				}
				if(dato==':'){
					e=1;
					continue;
				}else{
					if(dato!=10&&dato!=13){
						propiedad+=(char)dato;
						continue;
					}
				}
			}
			if(e==1){
				if(dato==10){
					h.put(propiedad,valor.trim());
					propiedad="";
					valor="";
					e=0;
					continue;
				}else{
					if(dato!=13){
						valor+=(char)dato;
						continue;
					}
				}
			}
			if(e==3){
				l++;
				if(len>0){
					if(l==len){
						break;
					}
				}else{
					if(dato==13){
						e=4;
						continue;
					}
				}
			}
			if(e==4){
				if(dato=='0'){
					e=5;
					continue;
				}
				if(dato!=13&&dato!=10){
					e=3;
					continue;
				}
			}
			if(e==5){
				if(dato==13){
					dato=is.read();
					resultado.add((byte)dato);
					dato=is.read();
					resultado.add((byte)dato);
					dato=is.read();
					resultado.add((byte)dato);
					break;
				}
				if(dato!=10){
					e=3;
					continue;
				}
			}
		}
		return convert(resultado);
	}
	public static byte[]convert(ArrayList<Byte>datos)throws Exception{
		byte[]resultado=new byte[datos.size()];
		for(int i=0;i<datos.size();i++){
			resultado[i]=(byte)datos.get(i);
		}
		return resultado;
	}
	public static String convert2(byte[]datos)throws Exception{
		return new String(datos);
	}
	public static void escribir(OutputStream os,byte[]datos)throws Exception{
		os.write(datos);
	}
	public static void main(String[]argumentos){
		try{
			PrintStream ps=new PrintStream(new FileOutputStream(new File("respuesta.txt")));
			//Socket s=new Socket("test-wap039.test.epm.com.co",80);
			Socket s=new Socket("10.1.20.139",80);//039
			//Socket s=new Socket("10.1.8.61",80);//040
			//Socket s=new Socket("10.1.20.139",8484);
			//Socket s=new Socket("localhost",8383);
			//byte[]solicitud=leerArchivo("SolicitudNavegadorError.txt");
			byte[]solicitud=leerArchivo("SolicitudNavegadorCorrecta.txt");
			//byte[]solicitud=leerArchivo("solicitudConsulta.txt");
			ps.println(convert2(solicitud));
			//enviarMensaje(s,solicitud);
			escribir(s.getOutputStream(),solicitud);
			System.out.println("Esperando respuesta");
			byte[]respuesta=leer2(s.getInputStream(),ps);
			ps.println(convert2(respuesta));
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}