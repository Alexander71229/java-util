import java.net.*;
import java.io.*;
public class Cliente extends Thread{
	public String servidor="localhost";
	public int puerto=8282;
	public Cliente(){
	}
	public Cliente(String servidor,int puerto){
		this.servidor=servidor;
		this.puerto=puerto;
	}
	public void run(){
		BufferedReader entrada;
		PrintStream salida;
		try{
			Socket socket=new Socket(this.servidor,this.puerto);
			entrada=new BufferedReader(new InputStreamReader(socket.getInputStream()));
			salida=new PrintStream(socket.getOutputStream());
			while(true){
				salida.println("Cliente enviando mensaje");
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
	public static void main(String[]argumentos){
		try{
			Cliente cliente=new Cliente();
			cliente.start();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}