import java.io.*;
import java.net.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.util.*;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
public class CargaArchivo extends HttpServlet{
	public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
		ServletContext contexto=request.getSession().getServletContext();
		String ruta=request.getParameter("ruta");
		File origen=new File(ruta);
		DiskFileUpload fileUpload=new DiskFileUpload();
		List<FileItem>fileItems=fileUpload.parseRequest(request);
		for(int i=0;i<fileItems.size();i++){
			FileItem fi=fileItems.get(i);
			fi.write(origen);
		}
	}
}