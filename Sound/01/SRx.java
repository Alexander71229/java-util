import javax.sound.sampled.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;

class Masa {
  byte n;
  double c=1;
  double f;
  double x;
  double v;
  double w;
  double m;
  double tm;
  double coe;
  public void res()
  {
    x=0;
    v=0;
    m=0;
  }
  Masa(double f,double tm)
  {
    this.coe=1;
    this.tm=tm;
    n=0;
    res();
    setF(f);
  }
  public void setF(double f)
  {
    this.f=f;
    this.w=4*Math.PI*Math.PI*this.f*this.f;
  }
  public void act(double d)
  {
    v=(d*c/f-x)*w/tm+v;
    x=(v/tm+x)*coe;
    if(m<Math.abs(x))m=Math.abs(x);
  }
  public void nor(int tn)
  {
    double d;
    int k;
    c=1;
    res();
    for(k=0;k<tn;k++)
    {
      d=32767*Math.sin(2*Math.PI*f*k/tm);
      act(d);
    }
    res();
    c=32767./m;
  }
  public double e()
  {
    return x*x+v*v;
  }
};


class Rcx extends Thread
{
  private TargetDataLine linea;
  public AudioFormat formatoAudio;
  private DataLine.Info dataLineInfo;
  public byte datos[][];
  public int ind;
  public int did;
  public int can;
  public int estado;
  public int tamano;
  public int numeroMuestras;
  private int audioData[];
  private int MSB,LSB;
  public Rcx(AudioFormat formatoAudio, int tamano)
  {
    try {
      this.formatoAudio = formatoAudio;
      this.dataLineInfo = new DataLine.Info(TargetDataLine.class,formatoAudio);
      linea = (TargetDataLine)AudioSystem.getLine(dataLineInfo);
      linea.open(formatoAudio);
      this.tamano = tamano;
      estado=0;
      ind=0;
      did=0;
      can=0;
      datos = new byte[2][tamano];
    } catch(Exception e) {
      System.out.println("No se pudo abrir la l�nea");
			e.printStackTrace();
    }
  }
  public void start()
  {
    linea.start();
    super.start();
  }
  public void detenerGrabacion()
  {
    linea.stop();
    linea.close();
  }
  public void run()
  {
    int tind = ind;
    try
    {
//System.out.println("inicia Captura");
      while(linea.read(datos[tind],0,tamano)>0)
      {
//System.out.println("finaliza Captura");
        ind=tind;
        estado=1;
        can++;
        did++;
        tind = (ind+1)%2;
//System.out.println("inicia Captura");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}

class Nalis extends Thread {
  public JFrame jf;
  private boolean gstd=true;
  public Rcx rec;
  public int h=600,w=800;
  public int audioData[];
  public int can;
  Masa masas[];
  double frec=440*Math.pow(2,(57-69)/12.);
  double max;
  int nf=50;
  double df=1.001;
  //double df=1.0594630943592952645618252949461;
  public Nalis(Rcx rec,int nota) {
    frec=440*Math.pow(2,(nota-69)/12.);
    jf = new JFrame() {
      public void paint(Graphics g) {
        double dw=w/(2*nf+1);
        if(true) {
          gstd=false;
          g.setColor(Color.white);
          g.fillRect(0,0,w,h);
        }
        if(df!=1.0594630943592952645618252949461){
          for(int i=-nf;i<=nf;i++) {
            if(i==0) {
              g.setColor(Color.red);
            } else {
              g.setColor(Color.blue);
            }
            g.fillRect((int)((i+nf)*dw),0,(int)dw,(int)(masas[i+nf].m/max*h));
          }
        }else{
          for(int i=-nf;i<=nf;i++) {
            boolean alg=false;
            int vlx=i+nf;
            vlx=vlx%12;
            if(vlx<=4&&vlx%2==0||vlx>4&&vlx%2==1){
              alg=true;
            }
            if(alg) {
              g.setColor(Color.red);
            } else {
              g.setColor(Color.blue);
            }
            g.fillRect((int)((i+nf)*dw),0,(int)dw,(int)(masas[i+nf].m/max*h));
          }
        }
      }
    };
    jf.setSize(w,h);
    jf.setVisible(true);
    this.rec = rec;
    masas = new Masa[2*nf+1];
    for (int i=-nf;i<=nf;i++)
    {
      //masas[i+dt]=new Masa(440*Math.pow(2,(i-69)/12.+i/(2.*dt+1.)),44100.);
      masas[i+nf]=new Masa(frec*Math.pow(df,i),44100);
    }
  }
  public void start() {
System.out.println("start");
    can=0;
    super.start();
  }
  public void run() {
    int MSB,LSB;
    int did;
System.out.println("run");
    while(rec.estado==0);
    while(rec.estado>0) {
//System.out.println("inicio an�lisis");
      did=rec.did;
      if (rec.formatoAudio.getSampleSizeInBits()==16)
      {
        rec.numeroMuestras = rec.tamano/2;
        audioData = new int[rec.numeroMuestras];

        if (rec.formatoAudio.isBigEndian())
        {
          for (int i = 0;i<rec.numeroMuestras;i++)
          {
            MSB = (int)rec.datos[rec.ind][2*i];
            LSB = (int)rec.datos[rec.ind][2*i+1];
            audioData[i] = MSB<<8|(255&LSB);
//System.out.println(audioData[i]);
          }
        }
        else
        {
          for (int i=0;i<rec.numeroMuestras;i++)
          {
            LSB = (int)rec.datos[rec.ind][2*i];
            MSB = (int)rec.datos[rec.ind][2*i+1];
            audioData[i] = MSB<<8|(255&LSB);
            //audioData[i] = (int)Math.round(30000*Math.sin(i*Math.PI*440/22050.));
//System.out.println(audioData[i]);
          }
        }
      }
      else if (rec.formatoAudio.getSampleSizeInBits()==8)
      {
        rec.numeroMuestras = rec.tamano;
        audioData = new int[rec.numeroMuestras];
        if (rec.formatoAudio.getEncoding().toString().startsWith("PCM_SIGN"))
        {
          for (int i=0;i<rec.numeroMuestras;i++)
          {
            audioData[i] = rec.datos[rec.ind][i];
          }
        }
        else
        {
          for (int i=0;i<rec.numeroMuestras;i++)
          {
            audioData[i] = rec.datos[rec.ind][i]-128;
          }
        }
      }
      for (int i=-nf;i<=nf;i++) {
        masas[i+nf].res();
      }
      for(int t=0;t<rec.numeroMuestras;t++) {
        for (int i=-nf;i<=nf;i++) {
          masas[i+nf].act(audioData[t]);
        }
      }
      //max=0;
      for (int i=-nf;i<=nf;i++) {
        if(max<masas[i+nf].m)max=masas[i+nf].m;
      }
//System.out.println(max);
      jf.repaint();
      can++;
//System.out.println("fin an�lisis");
      while(did==rec.did);
    }
  }
}


public class SRx {
  public static void main(String[] args) {
		c.U.traza();
    int nota=57;
    AudioFormat  formatoAudio = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0F, 16, 2, 4, 44100.0F, false);
    Rcx  rec = new Rcx(formatoAudio, 22048*2);
    try{
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader (isr);
      String texto = br.readLine();
      nota=Integer.parseInt(texto);
    }catch(Exception e){
    }
    Nalis nali = new Nalis(rec,nota);
    try
    {
      //System.in.read();
    }
    catch (Exception e)
    {
			c.U.imp(e);
      e.printStackTrace();
    }
    out("Inicia.");
    rec.start();
    nali.start();
    try
    {
      System.in.read();
      System.in.read();
      System.in.read();
      System.in.read();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    rec.detenerGrabacion();
    out("Termina.");
    out("Capturados:"+rec.can);
    out("Analizados:"+nali.can);
  }
  private static void out(String strMessage)
  {
    System.out.println(strMessage);
  }
}