package prueba.o;
import java.util.*;
import reactor.core.publisher.*;
import java.util.function.Consumer;
//import com.citrix.sharefile.api.SFApiClient;
import com.citrix.sharefile.api.models.*;
import prueba.s.SFApiClient;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.net.URI;
public class R{
	public SFApiClient apiClient;
    //Un llamado al api para obtener el id de un folder en SF segun su ruta descrita en un String(Ej del String: LABO/ASESOR/POLIZA/OPERACION)
    public Mono<SFItem> obtenerIdCarpeta(String path) {
        return Mono.fromCallable(() -> apiClient.items().byPath(path).execute());
    }
    public Mono<SFItem> crearCarpetaReactiva(String path) {
        LocalDate fechaActual = LocalDate.now();
        String mes = fechaActual.format(DateTimeFormatter.ofPattern("MMMM")).toUpperCase();
        return obtenerIdCarpeta(path)        //Poner un filtro y buscar cada path hasta encontrar uno que no exista
                .onErrorResume(throwable -> Mono.just(new SFItem()))
                .flatMap(sfItem -> {
                            return (sfItem.getId() == null) ?
                                    this.idCarpetaNoPresente(path) :
                                    Mono.just(sfItem);
                        }
                ).flatMap(parent -> {



                    SFFolder newFolder = new SFFolder();
                    newFolder.setName(path.split("/")[path.split("/").length - 1]);

                    //Controla fecha de caducidad
                    if (newFolder.getName().toString().equals(mes)) {

                        System.out.println("La carpeta se llama: " + newFolder.getName().toString());
                        newFolder.setExpirationDate(obtenerFechaExpiracion(LocalDateTime.now()));
                        System.out.println("A la carpeta se le asigno fecha de caducidad: " + newFolder.getExpirationDate());

                    }



                    return crearCarpetaApi(parent.geturl() != null ? parent.geturl().toString() : null, newFolder);
                });
    }
    private Mono<? extends SFItem> idCarpetaNoPresente(String path) {
        return getParentFolder(path)
                .flatMap(parent ->
                        !parent.isEmpty() ?
                                crearCarpetaReactiva(parent) :
                                Mono.just(new SFItem())
                );
    }
    public static Date obtenerFechaExpiracion(LocalDateTime fechaEntrada) {

        Date fechaDespues = Date.from(fechaEntrada.plusDays(120).atZone(ZoneId.systemDefault()).toInstant());
        return fechaDespues;
    }
    public Mono<SFFolder> crearCarpetaApi(String parentFolder, SFFolder folder) {
        /*return Mono.just(folder)
                .flatMap(sfFolder -> {
                    if (Optional.ofNullable(parentFolder).isPresent()) {
                        return crearUriCarpetaPadre(parentFolder);
                    }
                    return getUriMono();
                })
                .flatMap(uri -> Mono.fromCallable(() -> apiClient.items().createFolder(uri, folder).execute())
                        .onErrorResume(throwable -> {
                            // Manejar la excepción y retornar un resultado alternativo
                            return obtenerIdCarpeta(folder.getName()).map(sfItem -> {
                                return (SFFolder) sfItem;
                            });
                        })
                );*/
        return (Optional.ofNullable(parentFolder).isPresent() ?
                crearUriCarpetaPadre(parentFolder) :
                getUriMono())
                .flatMap(parentUri -> Mono.fromCallable(() -> apiClient.items().createFolder(parentUri, folder).execute()))
                .onErrorResume(throwable -> {
                    return obtenerIdCarpeta(folder.getName()).map(sfItem -> {
                        return (SFFolder) sfItem;
                    });
                });

    }
    private Mono<String> getParentFolder(String path) {
        return Mono.just(path)
                .map(s -> {
                    if (path.lastIndexOf('/') == -1) {
                        return "";
                    }
                    return path.substring(0, path.lastIndexOf('/'));
                });
    }
    private Mono<URI> crearUriCarpetaPadre(String parentFolder) {
        return Mono.just(parentFolder)
                .map(pf -> URI.create(pf))
                .onErrorResume(throwable -> getUriMono());
    }
    private Mono<URI> getUriMono() {
        return Mono.fromCallable(() -> apiClient.items().get().execute().getParent().geturl()); //Devuelve uri de la raiz
    }
}