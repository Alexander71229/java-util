package prueba;
import java.util.*;
import reactor.core.publisher.*;
import java.util.function.Consumer;
class Req01{
	int i;
	String v;
	public Req01(){
	}
	public Req01(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Res01{
	int i;
	String v;
	public Res01(){
	}
	public Res01(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Req02{
	int i;
	String v;
	public Req02(){
	}
	public Req02(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Res02{
	int i;
	String v;
	public Res02(){
	}
	public Res02(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Req03{
	int i;
	String v;
	public Req03(){
	}
	public Req03(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Res03{
	int i;
	String v;
	public Res03(){
	}
	public Res03(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Req04{
	int i;
	String v;
	public Req04(){
	}
	public Req04(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Res04{
	int i;
	String v;
	public Res04(){
	}
	public Res04(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Req05{
	int i;
	String v;
	public Req05(){
	}
	public Req05(int i,String v){
		this.i=i;
		this.v=v;
	}
}
class Res05{
	int i;
	String v;
	public Res05(){
	}
	public Res05(int i,String v){
		this.i=i;
		this.v=v;
	}
}
public class Main{
	public static void t01(){
		List<String> elements=new ArrayList<>();
		Flux.just(6,8,3,5).log().map(x->"x"+x).doOnSubscribe(x->c.U.imp(x+"")).subscribe(elements::add);
		Mono.just(7).log().doOnNext(x->c.U.imp((x*2)+"")).map(x->"x"+x).doOnSubscribe(x->c.U.imp(x+"")).subscribe(elements::add);
		c.U.imp(elements+"");
	}
	public static void main(String[]argumentos){
		c.U.ruta="Log.txt";
		c.U.imp("Inicio");
		//r01(new Req01()).doOnNext(Main::c01).subscribe();
		//m01(r01(new Req01()),new Res05());
		Res05 d=new Res05();
		r01(new Req01()).doOnNext(x->{
			if(x.i==1){
				l02(d);
			}
		}).subscribe();
	}
	public static Mono<Res01>r01(Req01 h){
		return Mono.just(new Res01(1,"1"));
	}
	public static Mono<Res02>r02(Req02 h){
		return Mono.just(new Res02(2,"2"));
	}
	public static Mono<Res03>r03(Req03 h){
		return Mono.just(new Res03(3,"3"));
	}
	public static Mono<Res04>r04(Req04 h){
		return Mono.just(new Res04(4,"4"));
	}
	public static Mono<Res05>r05(Req05 h){
		return Mono.just(new Res05(5,"5"));
	}
	public static void c01(Res01 r){
		r02(new Req02()).doOnNext(Main::c02).subscribe();
	}
	public static void c02(Res02 r){
		c.U.traza();
	}
	public static void m01(Mono<Res01>m,Res05 d){
		c.U.imp("m01");
		m.doOnNext(x->{
			if(x.i==1){
				m02(r02(new Req02()),d);
			}
		}).subscribe();
	}
	public static void m02(Mono<Res02>m,Res05 d){
		c.U.imp("m02");
		//m.doOnNext(f).subscribe();
	}
}