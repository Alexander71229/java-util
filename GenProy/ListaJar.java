import java.util.*;
import java.io.*;
public class ListaJar{
	public static String gen(String ruta,String separador,final String terminacion)throws Exception{
		File d=new File(ruta);
		File[]lista=d.listFiles(new FileFilter(){
			public boolean accept(File ruta){
				if(ruta.isDirectory()){
					return true;
				}else{
					String nombre=ruta.toString().toLowerCase();
					if(nombre.endsWith(terminacion)){
						return true;
					}
				}
				return false;
			}
		});
		String lj="";
		for(int i=0;i<lista.length;i++){
			if(lista[i].isDirectory()){
				lj+=gen(lista[i]+"",separador,terminacion);
			}else{
				lj+=separador+"\""+lista[i]+"\"";
			}
		}
		return lj;
	}
	public static List<File>gen(File d,String terminacion){
		File[]lista=d.listFiles(new FileFilter(){
			public boolean accept(File ruta){
				if(ruta.isDirectory()){
					return true;
				}else{
					String nombre=ruta.toString().toLowerCase();
					if(nombre.endsWith(terminacion)){
						return true;
					}
				}
				return false;
			}
		});
		ArrayList<File>r=new ArrayList<>();
		for(int i=0;i<lista.length;i++){
			if(lista[i].isDirectory()){
				r.addAll(gen(lista[i],terminacion));
			}else{
				r.add(lista[i]);
			}
		}
		return r;
	}
	public static List<File>gen(File d){
		File[]lista=d.listFiles(new FileFilter(){
			public boolean accept(File ruta){
				if(ruta.isDirectory()){
					return true;
				}
				return false;
			}
		});
		ArrayList<File>r=new ArrayList<>();
		r.add(d);
		for(int i=0;i<lista.length;i++){
			if(lista[i].isDirectory()){
				r.addAll(gen(lista[i]));
			}
		}
		return r;
	}
	public static void main(String[]argumentos){
		try{
			//String ruta="C:\\Desarrollo\\Proyectos\\SURA\\EGV Movilidad\\movil-star2-ml\\G\\x\\star2\\target\\star2\\WEB-INF\\lib\\";
			//String ruta="C:\\Desarrollo\\Proyectos\\Backup WLS WorkSpace Jrockit6\\wls\\librerias\\";
			String ruta="C:\\Desarrollo\\Proyectos\\SURA\\EGV Movilidad\\movil-asistencia-ml\\Datos\\EAR\\mod\\war\\WEB-INF\\lib";
			String nombre="Jars.txt";
			PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre)));
			ps.print(gen(ruta,";",".jar"));
			PrintStream ps2=new PrintStream(new FileOutputStream(new File("classpath.bat")));
			List<File>l=gen(new File(ruta));
			for(int i=0;i<l.size();i++){
				ps2.println("SET CLASSPATH=%CLASSPATH%;"+l.get(i)+"\\*");
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}