import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
public class GenProy{
	public static boolean limpiar=true;
	public static String destino="D:\\Desarrollo\\EPM\\FE\\CEC\\GEN\\web\\1Compilar.bat";
	public static String fuentes="D:\\Desarrollo\\EPM\\FE\\CEC\\GEN\\web\\f";
	public static String clases="D:\\Desarrollo\\EPM\\FE\\CEC\\GEN\\web\\war\\WEB-INF\\classes";
	public static String clasesDestino=null;
	public static String libs="D:\\Desarrollo\\EPM\\FE\\CEC\\GEN\\web\\war\\WEB-INF\\lib";
	public static String jvm="C:\\PROGRA~1\\Java\\jdk1.8.0_51\\bin\\javac.exe";
	public static String xjar="";
	public static String jvmopts="";
	public static int n=9999999;
	public static ArrayList<String>zjar;
	public static ArrayList<String>folders;
	public static void main(String[]argumentos){
		try{
			//addLib("D:\\Desarrollo\\EPM\\FE\\CEC\\GEN\\lib");
			//gen();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
	public static void gen()throws Exception{
		/*PrintStream ps=new PrintStream(new FileOutputStream(new File(destino+"\\"+"1Compilar.bat")));
		PrintStream ps2=new PrintStream(new FileOutputStream(new File(destino+"\\"+"argumentos")));
		String jars=gen(libs,";",".jar");
		String c=gen(fuentes," ",".java");
		xjar+=gen(zjar,";",".jar");
		String s="d:\n";
		s+="cd \""+clases+"\"\n";
		s+="del /S *.class\n";
		s+="cd \""+destino+"\"\n";
		s+="\""+jvm+"\" -Xstdout comx.txt -d "+clases+" -classpath "+fuentes+";"+jars+xjar+c+"\n";
		//s+="pause\n";
		ps.println(s);*/
		String jars=gen(libs,";",".jar");
		String c=gen(fuentes," ",".java");
		xjar+=gen(zjar,";",".jar");
		PrintStream ps=new PrintStream(new FileOutputStream(new File(destino+"\\"+"1Compilar.bat")));
		String s="";
		if(limpiar){
			s+="cd \""+clases+"\"\n";
			s+="del /S *.class\n";
		}
		ps.print(s);
		ArrayList<String>lis=cxs(c,n," ");
		for(int i=0;i<lis.size();i++){
			s="\""+jvm+"\" @argumentos"+i+"\n";
			ps.print(s);
			PrintStream ps2=new PrintStream(new FileOutputStream(new File(destino+"\\"+"argumentos"+i)));
			if(clasesDestino==null){
				clasesDestino=clases;
			}
			s=jvmopts+"-Xstdout comx"+i+".txt -d "+clasesDestino+" -classpath \""+(folders.stream().collect(Collectors.joining(";"))+";"+clases+";"+fuentes+";"+jars+xjar).replace("\\","\\\\")+"\""+lis.get(i)+"\n";
			ps2.println(s);
		}
	}
	public static ArrayList<String>cxs(String entrada,int cantidad,String separador){
		String[]e=entrada.split(separador);
		ArrayList<String>resultado=new ArrayList<String>();
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<e.length;i++){
			if(i>0){
				if(i%cantidad==0){
					resultado.add(sb+"");
					sb=new StringBuffer();
				}
				sb.append(" ");
			}
			sb.append(e[i]);
		}
		if(sb.length()>0){
			resultado.add(sb+"");
		}
		return resultado;
	}
	public static void addLib(String ruta){
		if(zjar==null){
			zjar=new ArrayList<String>();
		}
		zjar.add(ruta);
	}
	public static void addFolder(String ruta){
		if(folders==null){
			folders=new ArrayList<String>();
		}
		folders.add(ruta);
	}
	public static String gen(ArrayList<String>rutas,String separador,final String terminacion)throws Exception{
		String r="";
		if(rutas==null){
			return "";
		}
		for(int i=0;i<rutas.size();i++){
			r+=gen(rutas.get(i),separador,terminacion);
		}
		return r;
	}
	public static String gen(String ruta,String separador,final String terminacion)throws Exception{
		File d=new File(ruta);
		File[]lista=d.listFiles(new FileFilter(){
			public boolean accept(File ruta){
				if(ruta.isDirectory()){
					return true;
				}else{
					String nombre=ruta.toString().toLowerCase();
					if(nombre.endsWith(terminacion)){
						return true;
					}
				}
				return false;
			}
		});
		String lj="";
		for(int i=0;lista!=null&&i<lista.length;i++){
			if(lista[i].isDirectory()){
				lj+=gen(lista[i]+"",separador,terminacion);
			}else{
				lj+=separador+lista[i];
			}
		}
		return lj;
	}
}