import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;
import java.sql.*;
import java.util.concurrent.atomic.AtomicInteger;
public class Implementar{
	public static String cap(String cadena){
		return cadena.substring(0,1).toUpperCase()+cadena.substring(1);
	}
	public static String pr(Class[]a){
		AtomicInteger id=new AtomicInteger();
		return Arrays.stream(a).map(x->x.getSimpleName()+" p"+id.getAndIncrement()).collect(Collectors.joining(","));
	}
	public static String ps(Class[]a){
		AtomicInteger id=new AtomicInteger();
		return Arrays.stream(a).map(x->"p"+id.getAndIncrement()).collect(Collectors.joining(","));
	}
	public static String li(Class[]a){
		return Arrays.stream(a).map(x->x.getSimpleName()).collect(Collectors.joining(","));
	}
	public static String li(String s,Class[]a){
		String c=li(a);
		if(c.isEmpty()){
			return "";
		}
		return s+c;
	}
	public static void main(String[]argumentos){
		try{
			String paquete="dbutil.driver";
			String nombreClase="DepuracionPreparedStatement";
			String nombreObjeto="ops";
			Class clase=PreparedStatement.class;
			HashMap<String,Integer>imps=new HashMap<String,Integer>();
			imps.put(clase.getName(),1);
			String cadena=Arrays.stream(clase.getDeclaredMethods()).filter(m->m.getModifiers()==1025).map(m->{
				Class[]clases=new Class[1+m.getParameterTypes().length+m.getExceptionTypes().length];
				System.arraycopy(m.getParameterTypes(),0,clases,1,m.getParameterTypes().length);
				System.arraycopy(m.getExceptionTypes(),0,clases,1+m.getParameterTypes().length,m.getExceptionTypes().length);
				clases[0]=m.getReturnType();
				Arrays.stream(clases).forEach(c->{
					if(c.getPackage()!=null&&!c.getPackage().getName().equals("java.lang")){
						imps.put(c.getName(),1);
					}
					if(m.getReturnType().getComponentType()!=null){
						imps.put(m.getReturnType().getComponentType().getName(),1);
					}
				});
				StringBuffer sb=new StringBuffer();
				sb.append("\tpublic "+m.getReturnType().getSimpleName()+" "+m.getName()+"("+pr(m.getParameterTypes())+")"+li("throws ",m.getExceptionTypes())+"{\n");
				if(m.getReturnType().getSimpleName().equals("void")){
					sb.append("\t\tthis.");
				}else{
					sb.append("\t\treturn this.");
				}
				sb.append(nombreObjeto+"."+m.getName()+"("+ps(m.getParameterTypes()));
				sb.append(");\n");
				sb.append("\t}");
				return sb+"";
			}).collect(Collectors.joining("\n"));
			if(!paquete.isEmpty()){
				U.imp("package "+paquete+";");
			}
			imps.keySet().forEach(x->U.imp("import "+x+";"));
			U.imp("public class "+nombreClase+" implements "+clase.getSimpleName()+"{");
			U.imp("\tprivate "+clase.getSimpleName()+" "+nombreObjeto+";");
			U.imp("\tpublic "+nombreClase+"(){");
			U.imp("\t}");
			U.imp("\tpublic "+nombreClase+"("+clase.getSimpleName()+" "+nombreObjeto+"){");
			U.imp("\t\tthis."+nombreObjeto+"="+nombreObjeto+";");
			U.imp("\t}");
			U.imp("\tpublic void set"+cap(nombreObjeto)+"("+clase.getSimpleName()+" "+nombreObjeto+"){");
			U.imp("\t\tthis."+nombreObjeto+"="+nombreObjeto+";");
			U.imp("\t}");
			U.imp("\tpublic "+clase.getSimpleName()+" get"+cap(nombreObjeto)+"(){");
			U.imp("\t\treturn this."+nombreObjeto+";");
			U.imp("\t}");
			U.imp(cadena);
			U.imp("}");
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}