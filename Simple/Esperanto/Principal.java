import java.io.*;
import java.util.*;
public class Principal{
	public static HashMap<String,String>h;
	public static void dic(File f)throws Exception{
		h=new HashMap<>();
		Scanner s=new Scanner(f,"UTF-8");
		while(s.hasNext()){
			String p=s.next().toLowerCase();
			if(p.endsWith("--")){
				h.put(p.substring(0,p.length()-2),s.next());
			}
		}
	}
	public static void leer(File f)throws Exception{
		StringBuffer r=new StringBuffer();
		Scanner s=new Scanner(f,"UTF-8");
		while(s.hasNext()){
			String p=s.next();
			for(int i=0;i<p.length();i++){
				boolean e=false;
				for(int j=0;j<p.length();j++){
					if(i<p.length()-j){
						String x=p.substring(i,p.length()-j);
						if(h.get(x.toLowerCase())!=null){
							e=true;
							i=p.length()-j-1;
							//r.append("["+x+"]");
							r.append(h.get(x.toLowerCase()));
						}
					}
				}
				if(!e){
					r.append(p);
					break;
					//r.append(p.charAt(i));
				}
			}
			r.append(" ");
		}
		c.U.imp((r+"").trim());
	}
	public static void main(String[]argumentos){
		try{
			c.U.traza();
			dic(new File("DicEs.txt"));
			leer(new File("Entrada01.txt"));
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}