Tie ĉi mi havas materialon en diversaj lingvoj, plejmulte kun rilato al Esperanto.
Viro, kiu ankoraŭ legas, estas leganta viro.
Viro, kiu antaŭe legis, estas leginta viro.
Viro, kiu poste legos, estas legonta viro.
Libro, kiun oni ankoraŭ legas, estas legata libro.
Libro, kiun oni antaŭe legis, estas legita libro.
Libro, kiun oni poste legos, estas legota libro.