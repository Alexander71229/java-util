import java.util.*;
import java.math.*;
import java.util.stream.*;
public class Crear02{
	public static String gp(){
		return g3(3)+g2(3);
	}
	public static String fc(int v){
		if(v<10){
			return "0"+v;
		}
		return v+"";
	}
	public static String g1(int n){
		return new Random().ints(n,1,9).mapToObj(x->""+x).collect(Collectors.joining(""));
	}
	public static String g2(int n){
		return new Random().ints(n,0,9).mapToObj(x->""+x).collect(Collectors.joining(""));
	}
	public static String g3(int n){
		return new Random().ints(n,0,26).mapToObj(x->""+((char)(x+65))).collect(Collectors.joining(""));
	}
	public static int gv(int a,int b){
		return new Random().nextInt(b-a)+a;
	}
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log02.txt";
			Random r=new Random(0);
			int f=100000;
			int id1=1;
			int id2=1;
			int min=400;
			int max=1500;
			c.U.imp("begin");
			for(int i=0;i<5;i++){
				String x=g1(6);
				int v=gv(min,max);
				for(int j=0;j<5;j++){
					int m=2014+j;
					int v2=(int)Math.round(j*10*r.nextDouble())+5+v;
					for(int l=0;l<5;l++){
						for(int k=1;k<=12;k++){
							if(r.nextInt(4)==1){
								int v3=(int)Math.round(v2-(4+2*(r.nextDouble()))*(l*12+k-1));
								c.U.imp("\tinsert into valor_vehiculo values("+id1+++","+x+","+m+",to_date('"+(2015+l)+fc(k)+"01','yyyymmdd'),"+(v3*f)+");");
							}
						}
					}
					for(int k=0;k<5;k++){
						c.U.imp("\tinsert into vehiculos values("+id2+++","+x+","+m+",'"+gp()+"');");
					}
				}
			}
			c.U.imp("\tcommit;");
			c.U.imp("end;");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}