import java.util.*;
import java.util.stream.Collectors;
import java.lang.reflect.*;
class A{
	public Date d;
	public int i;
	public String s;
	public void setD(Date d){
		this.d=d;
	}
	public Date getD(){
		return this.d;
	}
	public void setI(int i){
		this.i=i;
	}
	public int getI(){
		return this.i;
	}
	public void setS(String s){
		this.s=s;
	}
	public String getS(){
		return this.s;
	}
	public String toString(){
		return d.getTime()+":"+i+":"+s;
	}
	public void setS(String s,String o){
		this.s=s;
	}
}
public class M{
	public static String capitalize(String c){
		return c.substring(0,1).toUpperCase()+c.substring(1);
	}
	public static<D>List<D>convertirAListaObjeto(List<ArrayList<Object>>datos,Class<D>clase,String[]nombres){
		return datos.stream().map(x->map(nombres,x,clase)).collect(Collectors.toList());
	}
	public static<D>List<D>convertirAListaObjeto(List<ArrayList<Object>>datos,Class<D>clase){
		return convertirAListaObjeto(datos,clase,Arrays.stream(clase.getDeclaredFields()).map(Field::getName).toArray(String[]::new));
	}
	public static<D>List<D>convertirListaMapaAListaObjeto(List<Map<String,Object>>datos,Class<D>clase,Map<String,String>mapaNombres){
		return datos.stream().map(x->map(mapaNombres,x,clase)).collect(Collectors.toList());
	}
	public static<D>List<D>convertirListaMapaAListaObjeto(List<Map<String,Object>>datos,Class<D>clase){
		ArrayList<String>nombres=new ArrayList<String>();
		nombres.addAll(datos.get(0).keySet());
		Map<String,String>mapaNombres=new HashMap<String,String>();
		for(int i=0;i<nombres.size();i++){
			mapaNombres.put(nombres.get(i),clase.getDeclaredFields()[i].getName());
		}
		U.imp("Mapeo de nombres:"+mapaNombres);
		return convertirListaMapaAListaObjeto(datos,clase,mapaNombres);
	}
	/*public static<D>List<D>convertirListaMapaAListaObjeto(List<Map<String,Object>>datos,Class<D>clase){
		return convertirListaMapaAListaObjeto(datos,clase,Arrays.stream(clase.getDeclaredFields()).map(Field::getName).toArray(String[]::new));
	}*/
	/*public static<D>List<D>convertirAListaObjeto(List<Map<String,Object>>datos,Class<D>clase,String[]nombres){
		return datos.stream().map(x->map(nombres,x,clase)).collect(Collectors.toList());
	}*/
	public static Map<String,Method>obtenerMapaMetodosDeclarados(Class clase,int cantidadParametros){
		//Map<String,Method>mapaMetodos=Arrays.stream(clase.getDeclaredMethods()).collect(Collectors.toMap(Method::getName,x->x));
		Map<String,Method>mapaMetodos=new HashMap<String,Method>();
		Arrays.stream(clase.getDeclaredMethods()).forEach(m->{
			if(m.getParameterCount()==cantidadParametros||cantidadParametros==-1){
				mapaMetodos.put(m.getName(),m);
			}
		});
		return mapaMetodos;
	}
	/*public static<D>D map(String[]nombres,Object objeto,Class<D>clase){
		U.imp("Sin implementar");
		return(D)objeto;
	}*/
	public static<D>D map(String[]nombres,List<Object>lista,Class<D>clase){
		try{
			D objeto=clase.newInstance();
			Map<String,Method>mapaMetodos=obtenerMapaMetodosDeclarados(clase,1);
			for(int i=0;i<nombres.length;i++){
				mapaMetodos.get("set"+capitalize(nombres[i])).invoke(objeto,lista.get(i));
			}
			return objeto;
		}catch(Throwable t){
			U.imp(t);
			return null;
		}
	}
	public static<D>D map(Map<String,String>mapaNombres,Map<String,Object>mapa,Class<D>clase){
		try{
			D objeto=clase.newInstance();
			Map<String,Method>mapaMetodos=obtenerMapaMetodosDeclarados(clase,1);
			U.imp("MapaX:"+mapa);
			U.imp("MapaX:"+mapaNombres);
			mapa.forEach((clave,valor)->{
				try{
					mapaMetodos.get("set"+capitalize(mapaNombres.get(clave))).invoke(objeto,valor);
				}catch(Exception e){
					U.imp(mapaMetodos.get("set"+capitalize(mapaNombres.get(clave)))+"");
					U.imp(clave+":"+valor);
				}
			});
			/*Arrays.stream(nombres).forEach(nombre->{
				try{
					mapaMetodos.get("set"+capitalize(nombre)).invoke(objeto,mapa.get(nombre));
				}catch(Throwable t){
				}
			});*/
			return objeto;
		}catch(Throwable t){
			U.imp(t);
			return null;
		}
	}
	public static void metodo(List a){
		U.imp(a+"-->1");
	}
	public static void metodo(Map a){
		U.imp(a+"-->2");
	}
	public static void metodo(Object a){
		if(a instanceof List){
			metodo((List)a);
		}
		if(a instanceof Map){
			metodo((Map)a);
		}
	}
	public static<D>D casteo(Object o,Class<D>c){
		return c.cast(o);
	}
	public static void main(String[]argumentos){
		try{
			Object objeto=new ArrayList();
			metodo(objeto);
			U.imp("Inicia:"+new Date());
			ArrayList<ArrayList<Object>>o=new ArrayList<ArrayList<Object>>();
			o.add(new ArrayList<Object>());
			o.get(o.size()-1).add(new Date());
			o.get(o.size()-1).add(1);
			o.get(o.size()-1).add("Uclyamunti");
			o.add(new ArrayList<Object>());
			o.get(o.size()-1).add(new Date());
			o.get(o.size()-1).add(2);
			o.get(o.size()-1).add("Certina");
			o.add(new ArrayList<Object>());
			o.get(o.size()-1).add(new Date());
			o.get(o.size()-1).add(3);
			o.get(o.size()-1).add("Urzinel");
			o.add(new ArrayList<Object>());
			o.get(o.size()-1).add(new Date());
			o.get(o.size()-1).add(4);
			o.get(o.size()-1).add("Clerulmi");
			o.add(new ArrayList<Object>());
			o.get(o.size()-1).add(new Date());
			o.get(o.size()-1).add(5);
			o.get(o.size()-1).add("Zurtichi");
			ArrayList<Map<String,Object>>d01=new ArrayList<Map<String,Object>>();
			d01.add(new LinkedHashMap<String,Object>());
			d01.get(d01.size()-1).put("fecha",new Date());
			d01.get(d01.size()-1).put("identificador",1);
			d01.get(d01.size()-1).put("nombre","Dektiyime");
			d01.add(new LinkedHashMap<String,Object>());
			d01.get(d01.size()-1).put("fecha",new Date());
			d01.get(d01.size()-1).put("identificador",2);
			d01.get(d01.size()-1).put("nombre","Kurnirek");
			U.imp(convertirAListaObjeto(o,A.class,new String[]{"d","i","s"})+"");
			U.imp(convertirAListaObjeto(o,A.class)+"");
			U.imp(convertirAListaObjeto(o,A.class,new String[]{"d","i"})+"");
			Map<String,String>mapaNombres=new HashMap<String,String>();
			mapaNombres.put("fecha","d");
			mapaNombres.put("identificador","i");
			mapaNombres.put("nombre","s");
			U.imp(convertirListaMapaAListaObjeto(d01,A.class,mapaNombres)+"");
			U.imp("Lista de Mapa sin especificar nombres:"+convertirListaMapaAListaObjeto(d01,A.class));
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}