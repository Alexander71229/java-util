import java.util.*;
public class Informacion{
	private List<Map<String,List<Object>>>registros;
	public Informacion(){
		registros=new ArrayList<Map<String,List<Object>>>();
	}
	public void nuevoRegistro(){
		registros.add(new HashMap<String,List<Object>>());
	}
	public void agregar(String campo,List<Map<String,Object>>registros){
		registros.stream().forEach(registro->agregar(campo,registro));
	}
	public void agregar(String campo,Map<String,Object>datos){
		datos.forEach((k,v)->{
			if(registros.get(registros.size()-1).get(campo+"."+k)==null){
				registros.get(registros.size()-1).put(campo+"."+k,new ArrayList<Object>());
			}
			registros.get(registros.size()-1).get(campo+"."+k).add(v);
		});
	}
	public List<Map<String,List<Object>>>getRegistros(){
		return registros;
	}
}