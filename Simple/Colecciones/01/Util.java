import java.util.*;
public class Util{
	public static<E>List<List<E>>dividir(List<E>lista,int tamano){
		List<List<E>>salida=new ArrayList<List<E>>();
		for(int i=0,t=tamano;i<lista.size();i+=t){
			if(i+tamano>=lista.size()){
				t=lista.size()-i;
			}
			salida.add(lista.subList(i,i+t));
		}
		return salida;
	}
	public static void t01(){
		ArrayList<String>a=new ArrayList<>();
		a.add("X01");
		a.add("X02");
		a.add("X03");
		a.add("X04");
		a.add("X05");
		a.add("X06");
		a.add("X07");
		a.add("X08");
		a.add("X09");
		a.add("X10");
		a.add("X11");
		a.add("X12");
		a.add("X13");
		a.add("X14");
		a.add("X15");
		a.add("X16");
		a.add("X17");
		a.add("X18");
		a.add("X19");
		a.add("X20");
		a.add("X21");
		a.add("X22");
		a.add("X23");
		a.add("X24");
		a.add("X25");
		//a.add("X26");
		//a.add("X27");
		c.U.imp(dividir(a,5)+"");
	}
	private static List<String>renglon(ArrayList<ArrayList<ArrayList<Object>>>estructura){
		List<String>resultado=new ArrayList<>();
		estructura.stream().forEach(seccion->seccion.stream().forEach(segmento->segmento.stream().forEach(valor->resultado.add(valor+""))));
		return resultado;
	}
	public static List<List<String>>formatoInformacion(String[]formato,List<Map<String,List<Object>>>registros){
		c.U.imp("Entrada:"+registros);
		HashMap<String,Integer>verticales=new HashMap<>();
		Arrays.stream(formato).filter(x->x.startsWith("&")).forEach(x->verticales.put(x.substring(1),1));
		List<List<String>>reporte=new ArrayList<List<String>>();
		ArrayList<ArrayList<String>>segmentos=new ArrayList<>();
		segmentos.add(new ArrayList<String>());
		for(int i=0;i<formato.length;i++){
			if(formato[i].equals("*")){
				segmentos.add(new ArrayList<String>());
				continue;
			}
			segmentos.get(segmentos.size()-1).add(formato[i]);
		}
		registros.forEach(registro->{
			for(int i=0;i<segmentos.size();i++){
				int longitudVertical=0;
				for(int indice=0;indice<=longitudVertical;indice++){
					ArrayList<ArrayList<ArrayList<Object>>>estructura=new ArrayList<ArrayList<ArrayList<Object>>>();
					String nombreSeccion="";
					for(int j=0;j<segmentos.get(i).size();j++){
						String seccionCampo=segmentos.get(i).get(j).split("\\.")[0];
						if(!nombreSeccion.equals(seccionCampo)){
							estructura.add(new ArrayList<ArrayList<Object>>());
							nombreSeccion=seccionCampo;
						}
						if(registro.get(segmentos.get(i).get(j))!=null){
							if(verticales.get(seccionCampo)==null){
								for(int k=0;k<registro.get(segmentos.get(i).get(j)).size();k++){
									if(k>=estructura.get(estructura.size()-1).size()){
										estructura.get(estructura.size()-1).add(new ArrayList<Object>());
									}
									estructura.get(estructura.size()-1).get(k).add(registro.get(segmentos.get(i).get(j)).get(k));
								}
							}else{
								estructura.get(estructura.size()-1).add(new ArrayList<Object>());
								estructura.get(estructura.size()-1).get(0).add(registro.get(segmentos.get(i).get(j)).get(indice));
								longitudVertical=registro.get(segmentos.get(i).get(j)).size()-1;
							}
						}
					}
					if(estructura.size()>0){
						reporte.add(renglon(estructura));
					}
				}
			}
		});
		return reporte;
	}
	public static void t02(){
		List<Map<String,Object>>p=new ArrayList<Map<String,Object>>();
		p.add(new HashMap<String,Object>());
		p.get(p.size()-1).put("c1","pv1");
		p.get(p.size()-1).put("c2","pv2");
		p.get(p.size()-1).put("c3","pv3");
		List<Map<String,Object>>r=new ArrayList<Map<String,Object>>();
		r.add(new HashMap<String,Object>());
		r.get(r.size()-1).put("c1","rv1");
		r.get(r.size()-1).put("c2","rv2");
		r.get(r.size()-1).put("c3","rv3");
		List<Map<String,Object>>o=new ArrayList<Map<String,Object>>();
		o.add(new HashMap<String,Object>());
		o.get(r.size()-1).put("c1","ov1");
		o.get(r.size()-1).put("c2","ov2");
		o.get(r.size()-1).put("c3","ov3");
		List<Map<String,Object>>c1=new ArrayList<Map<String,Object>>();
		c1.add(new HashMap<String,Object>());
		c1.get(c1.size()-1).put("c1","c1v1");
		c1.get(c1.size()-1).put("c2","c1v2");
		c1.get(c1.size()-1).put("c3","c1v3");
		List<Map<String,Object>>c2=new ArrayList<Map<String,Object>>();
		c2.add(new HashMap<String,Object>());
		c2.get(c2.size()-1).put("c1","c2v1");
		c2.get(c2.size()-1).put("c2","c2v2");
		c2.get(c2.size()-1).put("c3","c2v3");
		Informacion informacion=new Informacion();
		informacion.nuevoRegistro();
		informacion.agregar("P",p);
		informacion.agregar("R",r);
		informacion.agregar("O",o);
		informacion.agregar("C",c1);
		informacion.agregar("C",c2);
		String r01=formatoInformacion(new String[]{"P.c1","P.c2","P.c3","C.c1","C.c2","R.c1","R.c2","R.c3","O.c1","O.c2"},informacion.getRegistros())+"";
		c.U.imp(""+r01);
		if(!"[[pv1, pv2, pv3, c1v1, c1v2, c2v1, c2v2, rv1, rv2, rv3, ov1, ov2]]".equals(r01)){
			c.U.imp("falla");
		}
		String r02=formatoInformacion(new String[]{"P.c1","P.c2","P.c3","C.c1","C.c2","R.c1","R.c2","R.c3","C.c3","O.c1","O.c2","P.c1"},informacion.getRegistros())+"";
		c.U.imp(""+r02);
		if(!"[[pv1, pv2, pv3, c1v1, c1v2, c2v1, c2v2, rv1, rv2, rv3, c1v3, c2v3, ov1, ov2, pv1]]".equals(r02)){
			c.U.imp("falla");
		}
		String r03=formatoInformacion(new String[]{"&R","&C","P.c1","R.c1","R.c2","P.c2","*","P.c1","C.c1","C.c2","P.c2","*"},informacion.getRegistros())+"";
		c.U.imp(""+r03);
		if(!"[[pv1, rv1, rv2, pv2], [pv1, c1v1, c1v2, pv2], [pv1, c2v1, c2v2, pv2]]".equals(r03)){
			c.U.imp("falla");
		}
		String r04=formatoInformacion(new String[]{"&R","&C","P.c1","R.c1","R.c2","P.c2","*","P.c1","C.c1","C.c2","P.c2"},informacion.getRegistros())+"";
		c.U.imp(""+r04);
		if(!"[[pv1, rv1, rv2, pv2], [pv1, c1v1, c1v2, pv2], [pv1, c2v1, c2v2, pv2]]".equals(r04)){
			c.U.imp("falla");
		}
	}
	public static void main(String[]argumentos){
		try{
			c.U.imp("Inicio");
			t02();
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}