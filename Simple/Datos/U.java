import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.nio.charset.StandardCharsets;
import java.io.*;
@FunctionalInterface
interface LogicaRegistro<T>{
	String logicaRegistro(T objeto);
}
public class U<T>{
	public static PrintStream ps;
	public static String ruta="log.txt";
	private LogicaRegistro<T>logicaRegistro;
	public static void init()throws Exception{
		ps=new PrintStream(new FileOutputStream(new File(ruta)));
	}
	public static void imp(Throwable t){
		try{
			if(ps==null){
				init();
			}
			t.printStackTrace(ps);
		}catch(Throwable t2){
		}
	}
	public static void imp(String m){
		try{
			if(m==null||m.length()<=5){
				return;
			}
			if(ps==null){
				init();
			}
			ps.print(m);
		}catch(Throwable t){
		}
	}
	public U(){
		this.logicaRegistro=x->null;
	}
	public U(LogicaRegistro<T>x){
		this.logicaRegistro=x;
	}
	public byte[]escribir(List<ArrayList<Object>>datos,String separador,String separadorLinea,String caracterInicial,String caracterFinal){
		return escribirACadena(datos,separador,separadorLinea,caracterInicial,caracterFinal).getBytes(StandardCharsets.UTF_8);
	}
	public String escribirACadena(List<ArrayList<Object>>datos,String separador,String separadorLinea,String caracterInicial,String caracterFinal){
		return datos.stream().map(x->escribirLinea((List<Object>)x,separador)).collect(Collectors.joining(separadorLinea,caracterInicial,caracterFinal));
	}
	public String escribirLinea(List<Object>datos,String separador){
		Object regla=logicaRegistro.logicaRegistro((T)datos);
		if(regla!=null){
			return regla+"";
		}
		return datos.stream().map(Object::toString).collect(Collectors.joining(separador));
	}
	public static void main(String[]argumentos){
		try{
			List<ArrayList<Object>>a=new ArrayList<ArrayList<Object>>();
			a.add(new ArrayList<Object>());
			a.get(0).add("A0");
			a.get(0).add("B0");
			a.get(0).add("C0");
			a.add(new ArrayList<Object>());
			a.get(1).add("A1");
			a.get(1).add("B1");
			a.get(1).add("C1");
			/*imp(new U<List<Object>>(x->{
				if(x.get(0).equals("A0")){
					return "NA";
				}
				return null;
			}).escribirACadena(a,",","\r\n","",""));*/
			imp();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}