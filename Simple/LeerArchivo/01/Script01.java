import java.util.*;
import java.util.stream.*;
import java.io.*;
public class Script01{
	public static String insertar(String t,ArrayList<String>ics,ArrayList<String>ivs,ArrayList<String>ccs,ArrayList<String>cvs){
		StringBuilder r=new StringBuilder();
		r.append("\tINSERT INTO "+t+"("+ics.stream().collect(Collectors.joining(","))+")SELECT "+ivs.stream().map(x->vx(x)).collect(Collectors.joining(","))+" FROM dual WHERE NOT EXISTS(SELECT 1 FROM "+t+" WHERE "+cep(ccs,cvs,""," AND ","=","'")+");\n");
		return r+"";
	}
	public static String hx(String k){
		HashMap<String,String>h=new HashMap<>();
		h.put("Iron Mountain","100116");
		h.put("Alpopular","100118");
		if(h.get(k)==null){
			return k;
		}
		return h.get(k);
	}
	public static String vx(String v){
		if(v.startsWith("(")){
			return v;
		}else{
			return "'"+v+"'";
		}
	}
	public static String cep(ArrayList<String>c,ArrayList<String>v,String a,String i,String m,String z){
		HashMap<String,String>h=new HashMap<>();
		h.put("="," IS NULL");
		h.put("<>"," IS NOT NULL");
		StringBuilder r=new StringBuilder();
		for(int j=0;j<c.size()&&j<v.size();j++){
			if(j==0){
				r.append(a);
			}else{
				r.append(i);
			}
			if(c.get(j).toLowerCase().indexOf("fecha")>=0){
				r.append("NVL("+c.get(j)+",sysdate)");
			}else{
				r.append("NVL("+c.get(j)+",'*')");
			}
			if(v.get(j).equals("")){
				r.append(h.get(m));
			}else{
				r.append(m);
				if(hx(v.get(j)).startsWith("(")){
					r.append(hx(v.get(j)));
				}else{
					r.append(z+hx(v.get(j))+z);
				}
			}
		}
		return r+"";
	}
	public static String rep(ArrayList<String>c,ArrayList<String>v,String a,String i,String m,String z){
		StringBuilder r=new StringBuilder();
		for(int j=0;j<c.size()&&j<v.size();j++){
			if(j==0){
				r.append(a);
			}else{
				r.append(i);
			}
			r.append(c.get(j));
			r.append(m);
			if(hx(v.get(j)).startsWith("(")){
				r.append(hx(v.get(j)));
			}else{
				r.append(z+hx(v.get(j))+z);				
			}
		}
		return r+"";
	}
	public static String formato(String v,int n,String c){
		int l=n-v.length();
		for(int i=0;i<l;i++){
			v=c+v;
		}
		return v;
	}
	public static void main(String[]argumentos){
		try{
			StringBuilder r=new StringBuilder();
			StringBuilder r2=new StringBuilder();
			StringBuilder r3=new StringBuilder();
			StringBuilder r4=new StringBuilder();
			r.append("BEGIN\n");
			//C:\Desarrollo\Onnovaci�n\GIT\datosonnovacion\Java\atl\Correcci�n de Datos\Estados de pagar�s en el SIC 1000 (2).csv
			//Scanner s=new Scanner(new File("C:\\Desarrollo\\Onnovaci�n\\GIT\\datosonnovacion\\Java\\atl\\Correcci�n de Datos\\Estados de pagar�s en el SIC 1000 (2).csv"));
			Scanner s=new Scanner(new File("Datos02.csv"));
			ArrayList<String>wc=new ArrayList<>();
			ArrayList<ArrayList<String>>xwc=new ArrayList<>();
			ArrayList<Integer>wp=new ArrayList<>();
			ArrayList<ArrayList<String>>wv=new ArrayList<>();
			ArrayList<ArrayList<String>>xsc=new ArrayList<>();
			ArrayList<String>sc=new ArrayList<>();
			ArrayList<Integer>sp=new ArrayList<>();
			ArrayList<ArrayList<String>>sv=new ArrayList<>();
			wp.add(4);
			wc.add("numero");
			sp.add(9);
			sc.add("estado_atl");
			sp.add(16);
			sc.add("custodia_pagare");
			sp.add(18);
			sc.add("codigo_deceval");
			s.nextLine();
			s.nextLine();
			s.nextLine();
			s.nextLine();
			while(s.hasNextLine()){
				String l=s.nextLine();
				String[]p=l.split(",");
				p[4]=formato(p[4],9,"0");
				wv.add(new ArrayList<>());
				xwc.add(new ArrayList<>());
				xsc.add(new ArrayList<>());
				for(int i=0;i<wp.size();i++){
					xwc.get(xwc.size()-1).add(wc.get(i));
					wv.get(wv.size()-1).add(p[wp.get(i)]);
				}
				sv.add(new ArrayList<>());
				for(int i=0;i<sp.size();i++){
					xsc.get(xsc.size()-1).add(sc.get(i));
					sv.get(sv.size()-1).add(p[sp.get(i)]);
				}
				if(p[9].equals("Pendiente")){
					xsc.get(xsc.size()-1).add("fecha_inicial_deceval");
					xsc.get(xsc.size()-1).add("fecha_final_deceval");
					sv.get(sv.size()-1).add("");
					sv.get(sv.size()-1).add("");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.estado_atl='Pendiente' AND a.fecha_inicial<>(TO_DATE('"+p[10]+"','dd/mm/yyyy'));\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.estado_atl IN('Aceptado','Rechazado','Reclamado');\n");
					ArrayList<String>scs=new ArrayList<>();
					ArrayList<String>svs=new ArrayList<>();
					/*scs.add("fecha_inicial");
					svs.add("");*/
					scs.add("fecha_final");
					svs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
					ArrayList<String>wcs=new ArrayList<>();
					ArrayList<String>wvs=new ArrayList<>();
					wcs.add("adocume_consecutivo");
					wvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
					wcs.add("estado_atl");
					wvs.add("Pendiente");
					r2.append("\tUPDATE ts_ase_doc_custodios "+rep(scs,svs,"SET ",",","=","'")+" WHERE "+cep(wcs,wvs,""," AND ","=","'")+" AND("+cep(scs,svs,""," OR ","<>","'")+");\n");
					ArrayList<String>ics=new ArrayList<>();
					ArrayList<String>ivs=new ArrayList<>();
					ArrayList<String>ccs=new ArrayList<>();
					ArrayList<String>cvs=new ArrayList<>();
					ics.add("adocume_consecutivo");
					ivs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
					ics.add("fecha_inicial");
					ivs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
					ics.add("custodio");
					ivs.add(hx(p[16]));
					ics.add("fecha_final");
					ivs.add("");
					ics.add("estado_atl");
					ivs.add("Pendiente");
					ccs.add("adocume_consecutivo");
					cvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
					ccs.add("estado_atl");
					cvs.add("Pendiente");
					r2.append(insertar("ts_ase_doc_custodios",ics,ivs,ccs,cvs));
					{
						r4.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.fecha_inicial=TO_DATE('"+p[10]+"','dd/mm/yyyy')AND NOT EXISTS(SELECT * FROM ts_ase_documentos d WHERE d.consecutivo=a.adocume_consecutivo AND(NVL(estado_atl,'*')='Pendiente' AND NVL(custodia_pagare,'*')='"+hx(p[16])+"'));\n");
					}
				}
				if(p[9].equals("Aceptado")){
					xsc.get(xsc.size()-1).add("fecha_inicial_deceval");
					xsc.get(xsc.size()-1).add("fecha_final_deceval");
					sv.get(sv.size()-1).add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
					sv.get(sv.size()-1).add("");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND estado_atl='Pendiente' AND a.f_creacion>(SELECT MIN(b.f_creacion)FROM ts_ase_doc_custodios b WHERE a.adocume_consecutivo=b.adocume_consecutivo AND estado_atl='Pendiente');\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND estado_atl='Aceptado' AND a.fecha_inicial<>(TO_DATE('"+p[10]+"','dd/mm/yyyy'));\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.estado_atl IN('Rechazado','Reclamado');\n");
					{
						ArrayList<String>scs=new ArrayList<>();
						ArrayList<String>svs=new ArrayList<>();
						scs.add("fecha_final");
						svs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ArrayList<String>wcs=new ArrayList<>();
						ArrayList<String>wvs=new ArrayList<>();
						wcs.add("adocume_consecutivo");
						wvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						wcs.add("estado_atl");
						wvs.add("Pendiente");
						r3.append("\tUPDATE ts_ase_doc_custodios "+rep(scs,svs,"SET ",",","=","'")+" WHERE "+cep(wcs,wvs,""," AND ","=","'")+" AND("+cep(scs,svs,""," OR ","<>","'")+");\n");
					}
					{
						ArrayList<String>scs=new ArrayList<>();
						ArrayList<String>svs=new ArrayList<>();
						scs.add("fecha_inicial");
						svs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						scs.add("fecha_final");
						svs.add("");
						ArrayList<String>wcs=new ArrayList<>();
						ArrayList<String>wvs=new ArrayList<>();
						wcs.add("adocume_consecutivo");
						wvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						wcs.add("estado_atl");
						wvs.add("Aceptado");
						r3.append("\tUPDATE ts_ase_doc_custodios "+rep(scs,svs,"SET ",",","=","'")+" WHERE "+cep(wcs,wvs,""," AND ","=","'")+" AND("+cep(scs,svs,""," OR ","<>","'")+");\n");
					}
					{
						ArrayList<String>ics=new ArrayList<>();
						ArrayList<String>ivs=new ArrayList<>();
						ArrayList<String>ccs=new ArrayList<>();
						ArrayList<String>cvs=new ArrayList<>();
						ics.add("adocume_consecutivo");
						ivs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						ics.add("fecha_inicial");
						ivs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy')-30)");
						ics.add("custodio");
						ivs.add(hx(p[16]));
						ics.add("fecha_final");
						ivs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ics.add("estado_atl");
						ivs.add("Pendiente");
						ccs.add("adocume_consecutivo");
						cvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						ccs.add("estado_atl");
						cvs.add("Pendiente");
						r2.append(insertar("ts_ase_doc_custodios",ics,ivs,ccs,cvs));
					}
					{
						ArrayList<String>ics=new ArrayList<>();
						ArrayList<String>ivs=new ArrayList<>();
						ArrayList<String>ccs=new ArrayList<>();
						ArrayList<String>cvs=new ArrayList<>();
						ics.add("adocume_consecutivo");
						ivs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						ics.add("fecha_inicial");
						ivs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ics.add("custodio");
						ivs.add(hx(p[16]));
						ics.add("fecha_final");
						ivs.add("");
						ics.add("estado_atl");
						ivs.add("Aceptado");
						ccs.add("adocume_consecutivo");
						cvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						ccs.add("estado_atl");
						cvs.add("Aceptado");
						r2.append(insertar("ts_ase_doc_custodios",ics,ivs,ccs,cvs));
					}
					{
						r4.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.fecha_inicial=TO_DATE('"+p[10]+"','dd/mm/yyyy')AND NOT EXISTS(SELECT * FROM ts_ase_documentos d WHERE d.consecutivo=a.adocume_consecutivo AND(NVL(estado_atl,'*')='Aceptado' AND NVL(custodia_pagare,'*')='"+hx(p[16])+"'));\n");
					}
				}
				if(p[9].equals("Reclamado")){
					xsc.get(xsc.size()-1).add("fecha_final_deceval");
					sv.get(sv.size()-1).add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND estado_atl='Pendiente' AND a.f_creacion>(SELECT MIN(b.f_creacion)FROM ts_ase_doc_custodios b WHERE a.adocume_consecutivo=b.adocume_consecutivo AND estado_atl='Pendiente');\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND estado_atl='Aceptado' AND a.f_creacion>(SELECT MIN(b.f_creacion)FROM ts_ase_doc_custodios b WHERE a.adocume_consecutivo=b.adocume_consecutivo AND estado_atl='Aceptado');\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND estado_atl='Reclamado' AND a.fecha_inicial<>(TO_DATE('"+p[10]+"','dd/mm/yyyy'));\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.estado_atl IN('Rechazado');\n");
					{
						ArrayList<String>scs=new ArrayList<>();
						ArrayList<String>svs=new ArrayList<>();
						scs.add("fecha_final");
						svs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ArrayList<String>wcs=new ArrayList<>();
						ArrayList<String>wvs=new ArrayList<>();
						wcs.add("adocume_consecutivo");
						wvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						wcs.add("estado_atl");
						wvs.add("Aceptado");
						r3.append("\tUPDATE ts_ase_doc_custodios "+rep(scs,svs,"SET ",",","=","'")+" WHERE "+cep(wcs,wvs,""," AND ","=","'")+" AND("+cep(scs,svs,""," OR ","<>","'")+");\n");
					}
					{
						ArrayList<String>scs=new ArrayList<>();
						ArrayList<String>svs=new ArrayList<>();
						scs.add("fecha_inicial");
						svs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ArrayList<String>wcs=new ArrayList<>();
						ArrayList<String>wvs=new ArrayList<>();
						wcs.add("adocume_consecutivo");
						wvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						wcs.add("estado_atl");
						wvs.add("Reclamado");
						r3.append("\tUPDATE ts_ase_doc_custodios "+rep(scs,svs,"SET ",",","=","'")+" WHERE "+cep(wcs,wvs,""," AND ","=","'")+" AND("+cep(scs,svs,""," OR ","<>","'")+");\n");
					}
					{
						ArrayList<String>ics=new ArrayList<>();
						ArrayList<String>ivs=new ArrayList<>();
						ArrayList<String>ccs=new ArrayList<>();
						ArrayList<String>cvs=new ArrayList<>();
						ics.add("adocume_consecutivo");
						ivs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						ics.add("fecha_inicial");
						ivs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ics.add("fecha_final");
						ivs.add("");
						ics.add("custodio");
						ivs.add(hx(p[16]));
						ics.add("estado_atl");
						ivs.add("Reclamado");
						ccs.add("adocume_consecutivo");
						cvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						ccs.add("estado_atl");
						cvs.add("Reclamado");
						r2.append(insertar("ts_ase_doc_custodios",ics,ivs,ccs,cvs));
					}
					{
						r4.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.fecha_inicial=TO_DATE('"+p[10]+"','dd/mm/yyyy')AND NOT EXISTS(SELECT * FROM ts_ase_documentos d WHERE d.consecutivo=a.adocume_consecutivo AND(NVL(estado_atl,'*')='Reclamado' AND NVL(custodia_pagare,'*')='"+hx(p[16])+"'));\n");
					}
				}
				if(p[9].equals("Rechazado")){
					xsc.get(xsc.size()-1).add("fecha_inicial_deceval");
					xsc.get(xsc.size()-1).add("fecha_final_deceval");
					sv.get(sv.size()-1).add("");
					sv.get(sv.size()-1).add("");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND estado_atl='Pendiente' AND a.f_creacion>(SELECT MIN(b.f_creacion)FROM ts_ase_doc_custodios b WHERE a.adocume_consecutivo=b.adocume_consecutivo AND estado_atl='Pendiente');\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND estado_atl='Aceptado' AND a.f_creacion>(SELECT MIN(b.f_creacion)FROM ts_ase_doc_custodios b WHERE a.adocume_consecutivo=b.adocume_consecutivo AND estado_atl='Aceptado');\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND estado_atl='Rechazado' AND a.fecha_inicial<>(TO_DATE('"+p[10]+"','dd/mm/yyyy'));\n");
					r2.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.estado_atl IN('Reclamado');\n");
					{
						ArrayList<String>scs=new ArrayList<>();
						ArrayList<String>svs=new ArrayList<>();
						scs.add("fecha_final");
						svs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ArrayList<String>wcs=new ArrayList<>();
						ArrayList<String>wvs=new ArrayList<>();
						wcs.add("adocume_consecutivo");
						wvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						wcs.add("estado_atl");
						wvs.add("Aceptado");
						r3.append("\tUPDATE ts_ase_doc_custodios "+rep(scs,svs,"SET ",",","=","'")+" WHERE "+cep(wcs,wvs,""," AND ","=","'")+" AND("+cep(scs,svs,""," OR ","<>","'")+");\n");
					}
					{
						ArrayList<String>scs=new ArrayList<>();
						ArrayList<String>svs=new ArrayList<>();
						scs.add("fecha_inicial");
						svs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ArrayList<String>wcs=new ArrayList<>();
						ArrayList<String>wvs=new ArrayList<>();
						wcs.add("adocume_consecutivo");
						wvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						wcs.add("estado_atl");
						wvs.add("Rechazado");
						r3.append("\tUPDATE ts_ase_doc_custodios "+rep(scs,svs,"SET ",",","=","'")+" WHERE "+cep(wcs,wvs,""," AND ","=","'")+" AND("+cep(scs,svs,""," OR ","<>","'")+");\n");
					}
					{
						ArrayList<String>ics=new ArrayList<>();
						ArrayList<String>ivs=new ArrayList<>();
						ArrayList<String>ccs=new ArrayList<>();
						ArrayList<String>cvs=new ArrayList<>();
						ics.add("adocume_consecutivo");
						ivs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						ics.add("fecha_inicial");
						ivs.add("(TO_DATE('"+p[10]+"','dd/mm/yyyy'))");
						ics.add("fecha_final");
						ivs.add("");
						ics.add("custodio");
						ivs.add(hx(p[16]));
						ics.add("estado_atl");
						ivs.add("Rechazado");
						ccs.add("adocume_consecutivo");
						cvs.add("(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')");
						ccs.add("estado_atl");
						cvs.add("Rechazado");
						r2.append(insertar("ts_ase_doc_custodios",ics,ivs,ccs,cvs));
					}
					{
						r4.append("\tDELETE ts_ase_doc_custodios a WHERE a.adocume_consecutivo=(SELECT consecutivo FROM ts_ase_documentos WHERE numero='"+p[4]+"')AND a.fecha_inicial=TO_DATE('"+p[10]+"','dd/mm/yyyy')AND NOT EXISTS(SELECT * FROM ts_ase_documentos d WHERE d.consecutivo=a.adocume_consecutivo AND(NVL(estado_atl,'*')='Rechazado' AND NVL(custodia_pagare,'*')='"+hx(p[16])+"'));\n");
					}
				}
			}
			r.append(r3+"");
			r.append(r4+"");
			for(int i=0;i<wv.size();i++){
				r.append("\tUPDATE ts_ase_documentos "+rep(xsc.get(i),sv.get(i),"SET ",",","=","'")+" WHERE "+cep(wc,wv.get(i),""," AND ","=","'")+" AND("+cep(xsc.get(i),sv.get(i),""," OR ","<>","'")+");\n");
			}
			r.append(r2+"");
			r.append("\tCOMMIT;\n");
			r.append("EXCEPTION\n");
			r.append("\tWHEN OTHERS THEN\n");
			r.append("\t\tROLLBACK;\n");
			r.append("\t\tdbms_output.put_line(sqlerrm);\n");
			r.append("\t\tdbms_output.put_line(dbms_utility.format_error_stack());\n");
			r.append("\t\tdbms_output.put_line(dbms_utility.format_error_backtrace());\n");
			r.append("END;");
			c.U.imp(r+"");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}