import java.util.*;
import java.util.stream.*;
import java.io.*;
public class Script03{
	public static String formato(String v,int n,String c){
		int l=n-v.length();
		for(int i=0;i<l;i++){
			v=c+v;
		}
		return v;
	}
	public static String hx(String k){
		HashMap<String,String>h=new HashMap<>();
		h.put("Iron Mountain","100116");
		h.put("Alpopular","100118");
		if(h.get(k)==null){
			return k;
		}
		return h.get(k);
	}
	public static void main(String[]argumentos){
		try{
			Scanner s=new Scanner(new File("C:\\Desarrollo\\Onnovación\\GIT\\datosonnovacion\\Java\\atl\\Corrección de Datos02\\Datos.csv"));
			int x=1000;
			int k=0;
			s.nextLine();
			StringBuilder r=new StringBuilder();
			while(s.hasNextLine()){
				String l=s.nextLine()+";|";
				String[]p=l.split(";");
				Arrays.stream(p).forEach(z->c.U.imp("["+z+"]"));
				p[3]=formato(p[3],9,"0");
				p[5]=p[5].trim();
				p[9]=p[9].trim();
				if(k==0||(k%x==1&&k>x)){
					r.append("/\nDECLARE\n");
					r.append("\tvconsecutivo ts_ase_documentos.consecutivo%TYPE;\n");
					r.append("\tvnumero ts_ase_documentos.numero%TYPE;\n");
					r.append("BEGIN\n");
				}
				r.append("\t--"+p[3]+":"+p[5]+"\n");
				r.append("\tvnumero:='"+p[3]+"';\n");
				r.append("\tSELECT consecutivo INTO vconsecutivo FROM ts_ase_documentos WHERE numero='"+p[3]+"';\n");
				r.append("\tUPDATE ts_ase_documentos SET codigo_deceval='"+p[9]+"' WHERE consecutivo=vconsecutivo;\n");
				if(p[5].equals("Pendiente")){
					r.append("\tDELETE ts_ase_doc_custodios WHERE adocume_consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_documentos SET estado_atl='' WHERE consecutivo=vconsecutivo;\n");
					r.append("\tDELETE ts_ase_doc_custodios WHERE adocume_consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_documentos SET custodia_pagare='"+hx(p[8])+"',estado_atl='"+p[5]+"',fecha_inicial_deceval='',fecha_final_deceval='',codigo_deceval='' WHERE consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_doc_custodios SET fecha_inicial=TO_DATE('"+p[6]+"','dd/mm/yyyy')WHERE adocume_consecutivo=vconsecutivo AND estado_atl='"+p[5]+"';\n");
				}
				if(p[5].equals("Aceptado")){
					r.append("\tDELETE ts_ase_doc_custodios WHERE adocume_consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_documentos SET estado_atl='' WHERE consecutivo=vconsecutivo;\n");
					r.append("\tDELETE ts_ase_doc_custodios WHERE adocume_consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_documentos SET custodia_pagare='"+hx(p[8])+"',estado_atl='"+p[5]+"',fecha_inicial_deceval=TO_DATE('"+p[6]+"','dd/mm/yyyy'),fecha_final_deceval='' WHERE consecutivo=vconsecutivo;\n");
				}
				if(p[5].equals("Reclamado")){
					r.append("\tDELETE ts_ase_doc_custodios WHERE adocume_consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_documentos SET estado_atl='' WHERE consecutivo=vconsecutivo;\n");
					r.append("\tDELETE ts_ase_doc_custodios WHERE adocume_consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_documentos SET custodia_pagare='COTRAFA',estado_atl='"+p[5]+"',fecha_final_deceval=TO_DATE('"+p[6]+"','dd/mm/yyyy')WHERE consecutivo=vconsecutivo;\n");
				}
				if(p[5].equals("Rechazado")){
					r.append("\tDELETE ts_ase_doc_custodios WHERE adocume_consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_documentos SET estado_atl='' WHERE consecutivo=vconsecutivo;\n");
					r.append("\tDELETE ts_ase_doc_custodios WHERE adocume_consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_documentos SET custodia_pagare='COTRAFA',estado_atl='"+p[5]+"',fecha_final_deceval=''WHERE consecutivo=vconsecutivo;\n");
					r.append("\tUPDATE ts_ase_doc_custodios SET fecha_inicial=TO_DATE('"+p[6]+"','dd/mm/yyyy')WHERE adocume_consecutivo=vconsecutivo AND estado_atl='"+p[5]+"';\n");
				}
				if(k>0&&k%x==0){
					r.append("\tCOMMIT;\n");
					r.append("EXCEPTION WHEN OTHERS THEN ROLLBACK;\n");
					r.append("\tdbms_output.put_line('vnumero:'||vnumero);\n");
					r.append("\tdbms_output.put_line(SQLERRM);\n");
					r.append("\tdbms_output.put_line(dbms_utility.format_error_stack());\n");
					r.append("\tdbms_output.put_line(dbms_utility.format_error_backtrace());\n");
					r.append("END;\n");
					/*PrintStream ps=new PrintStream(new FileOutputStream(new File("Salida.sql")));
					ps.println(r+"");
					System.exit(0);*/
				}				
				k++;
			}
			r.append("\tCOMMIT;\n");
			r.append("EXCEPTION WHEN OTHERS THEN ROLLBACK;\n");
			r.append("\tdbms_output.put_line('vnumero:'||vnumero);\n");
			r.append("\tdbms_output.put_line(SQLERRM);\n");
			r.append("\tdbms_output.put_line(dbms_utility.format_error_stack());\n");
			r.append("\tdbms_output.put_line(dbms_utility.format_error_backtrace());\n");
			r.append("END;\n");
			PrintStream ps=new PrintStream(new FileOutputStream(new File("Salida.sql")));
			ps.println(r+"");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}
