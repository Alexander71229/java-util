package test.logica;
import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;
import logica.Mutantes;
public class MutantesTest{
	@Before
	public void configuracion(){
	}
	@Test
	public void test01(){
		String[]adn={"ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
		Assert.assertTrue(Mutantes.esADN(adn));
	}
	@Test
	public void test02(){
		String[]adn={"ATGXGA","CAGTGZ","TQATGT","AGAAGG","CCHCTA","TCACTG"};
		Assert.assertTrue(!Mutantes.esADN(adn));
	}
	@Test
	public void test03(){
		String[]adn={"ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCTG"};
		Assert.assertTrue(!Mutantes.esADN(adn));
	}
	@Test
	public void test04(){
		String[]adn={};
		Assert.assertTrue(!Mutantes.esADN(adn));
	}
	@Test
	public void test05(){
		String[]adn=null;
		Assert.assertTrue(!Mutantes.esADN(adn));
	}
	@Test
	public void test91(){
		String[]adn={"ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
		Assert.assertTrue(Mutantes.esMutante(adn));
	}
	@Test
	public void test92(){
		String[]adn={"GGGGTA","CAGTCA","TTATGA","AGAAGA","ACACTG","TCACTG"};
		Assert.assertTrue(Mutantes.esMutante(adn));
	}
	@Test
	public void test93(){
		String[]adn={"GGAGTA","CAGTCA","TTATGT","AGAAGA","ACTCTG","TCACTG"};
		Assert.assertTrue(!Mutantes.esMutante(adn));
	}
	@Test
	public void test94(){
		String[]adn={"GGAGTA","CGGTCA","TTGTGT","AGAGGA","ACTCTG","TCACTG"};
		Assert.assertTrue(!Mutantes.esMutante(adn));
	}
	@Test
	public void test95(){
		String[]adn={"GTAGAT","AGAGCC","GTGGAT","ACAGAT","TCAATC","TCAATT"};
		Assert.assertTrue(Mutantes.esMutante(adn));
	}
	@Test
	public void test96(){
		String[]adn={"GAATAA","AGATAA","GAGAAT","CATGCA","ATATAA","AATAAA"};
		Assert.assertTrue(!Mutantes.esMutante(adn));
	}
	@Test
	public void test97(){
		String[]adn={"TAAT","TATA","TTAA","TAAA"};
		Assert.assertTrue(Mutantes.esMutante(adn));
	}
	@Test
	public void test98(){
		String[]adn={"TAAT","TATA","TTAA","TAHA"};
		Assert.assertTrue(!Mutantes.esMutante(adn));
	}
}