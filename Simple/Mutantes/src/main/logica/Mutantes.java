package logica;
import java.util.*;
public class Mutantes{
	public static boolean esADN(String[]datos){
		if(datos==null||datos.length==0){
			return false;
		}
		int cantidadColumnas=datos[0].length();
		HashMap<Character,Integer>h=new HashMap<>();
		h.put('G',1);
		h.put('T',1);
		h.put('A',1);
		h.put('C',1);
		for(int i=0;i<datos.length;i++){
			if(cantidadColumnas!=datos[i].length()){
				return false;
			}
			for(int j=0;j<datos[i].length();j++){
				if(h.get(datos[i].charAt(j))==null){
					return false;
				}
			}
		}
		return true;
	}
	public static boolean esMutante(String[]datos){
		try{
			HashMap<Character,Integer>h=new HashMap<>();
			h.put('G',1);
			h.put('T',1);
			h.put('A',1);
			h.put('C',1);
			int[][][]secuencias=new int[datos.length][datos[0].length()][4]; //Se define una matriz, con 4 contadores, una por cada dirección a considerar: horizontal, vertical y dos diagonales. Cada vez que se encuentre una coincidencia se incrementa en uno el contador.
			int cantidadSecuencias=0;
			for(int i=0;i<secuencias.length;i++){
				for(int j=0;j<secuencias.length;j++){
					if(h.get(datos[i].charAt(j))==null){
						return false; //Valido que las letras sean solo G,T,A,C: Si hay una letra distinta a esas 4 retorno falso.
					}
					for(int direcciones=0;direcciones<4;direcciones++){
						int dx=-1;
						int dy=0;
						if(direcciones>0){
							dy=-1;
							dx=direcciones-2;
						}
						if(i+dy>=0&&j+dx>=0&&j+dx<secuencias.length){
							if(datos[i+dy].charAt(j+dx)==datos[i].charAt(j)){
								secuencias[i][j][direcciones]=secuencias[i+dy][j+dx][direcciones]+1;
								if(secuencias[i][j][direcciones]==3){
									cantidadSecuencias++;
								}
							}
						}
					}
				}
			}
			if(cantidadSecuencias>1){
				return true;
			}
			return false;
		}catch(Exception e){
			//Si ocurre alguna excepción quiere decir que no se cumple con los aspectos para que sea un ADN válido y por tal razón no sería un mutante.
			return false;
		}
	}
}