import java.io.*;
import java.util.*;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
public class Main{
	public static List<String>gids(File f)throws Exception{
		List<String>r=new ArrayList<>();
		Scanner s=new Scanner(f);
		while(s.hasNext()){
			r.add(s.next());
		}
		return r;
	}
	public static void main(String[]argumentos){
		try{
			c.U.imp("INICIO");
			long tini=new Date().getTime();
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection x=DriverManager.getConnection("jdbc:oracle:thin:@10.18.203.24:1521/JUPITER.COTRAFA","sic","cot202006");
			x.setAutoCommit(false);
			List<String>ids=gids(new File("ids.txt"));
			int l=1000;
			StringBuilder b=null;
			List<String>r=new ArrayList<String>();
			for(int i=0;i<ids.size();i++){
				if(i%l==0){
					if(i>0){
						Statement statement=x.createStatement();
						ResultSet rs=statement.executeQuery(b+"");
						while(rs.next()){
							r.add(rs.getObject("id")+":"+rs.getObject("nombre_completo"));
						}
						statement.close();
					}
					b=new StringBuilder();
				}else{
					b.append("union all ");
				}
				b.append("select id,nombre_completo from ts_personas where id=");
				b.append(ids.get(i));
				b.append("\n");
			}
			c.U.imp("t:"+(new Date().getTime()-tini));
			c.U.imp("r.size():"+r.size());
			for(int i=0;i<r.size();i++){
				c.U.imp(r.get(i));
			}
			c.U.imp("t:"+(new Date().getTime()-tini));
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}