import java.util.*;
import java.io.*;
public class ArchivoGrande{
	public static void generar(String ruta,long tamano,String mensaje)throws Exception{
		File f=new File(ruta);
		PrintStream ps=new PrintStream(new FileOutputStream(f));
		int k=0;
		while(f.length()<tamano){
			ps.print(mensaje);
			ps.format("%010d%n",k);
			k++;
		}
	}
	public static void partir(File f,long tamano)throws Exception{
		System.out.println("Tamaño:"+f.length());
		Scanner s=new Scanner(f);
		int k=0;
		File fx=null;
		PrintStream ps=null;
		while(s.hasNextLine()){
			if(fx==null||fx.length()>=tamano){
				fx=new File(String.format("p%010d",k));
				ps=new PrintStream(new FileOutputStream(fx));
				k++;
			}
			ps.println(s.nextLine());
		}
	}
	public static void partir2(File f,int tamano)throws Exception{
		System.out.println("Tamaño:"+f.length());
		FileInputStream fis=new FileInputStream(f);
		byte[]datos=new byte[tamano];
		int k=0;
		while(fis.read(datos)>0){
			new FileOutputStream(new File(String.format("p%010d",k))).write(datos);
			k++;
		}
		int dato=0;
		FileOutputStream residuo=new FileOutputStream(new File(String.format("p%010d",k)));
		while((dato=fis.read())>=0){
			residuo.write(dato);
		}
	}
	public static void partir3(File f,int tamano)throws Exception{
		System.out.println("Tamaño:"+f.length());
		FileInputStream fis=new FileInputStream(f);
		int k=0;
		int dato=0;
		FileOutputStream fos=null;
		int t=0;
		while((dato=fis.read())>=0){
			if(fos==null||t>=tamano){
				fos=new FileOutputStream(new File(String.format("p%010d",k)));
				t=0;
				k++;
			}
			fos.write(dato);
			t++;
		}
	}
	public static void main(String[]argumentos){
		try{
			//generar("Grande.txt",1024*1024*10,"asdf aafo3aneoadkc asdneocansdf wodasdnfas cwoasdf nas ceasdf onac adsfasdfwonasdf asdfasdfa adsfhaskdfj asdfneocnalsdf kec asdfnocekasd vneoasdf naower nadonasdf");
			partir3(new File("20200929.txt"),1024*1024*100);
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}