import java.util.*;
import java.util.stream.Collectors;
public class Java8{
	public Long sum(String s){
		if(s==null){
			return null;
		}
		//return Arrays.asList(s.split(",")).stream().filter(a->{try{Long.parseLong(a);}catch(Exception e){return false;}return true;}).mapToLong(a->Long.parseLong(a)).sum();
		return Arrays.asList(s.split(",")).stream().filter(a->a.matches("\\d+")).mapToLong(a->Long.parseLong(a)).sum();
	}
	public static void testSum()throws Exception{
		if((new Java8()).sum("1,A,1")!=2){
			throw new Exception("Falla");
		}
		if((new Java8()).sum(null)!=null){
			throw new Exception("Falla");
		}
		if((new Java8()).sum("1,A,1,2")!=4){
			throw new Exception("Falla");
		}
		if((new Java8()).sum("1,A,1,2,X")!=4){
			throw new Exception("Falla");
		}
		if((new Java8()).sum("1,A,1,2,X,1, ,1")!=6){
			throw new Exception("Falla");
		}
		if((new Java8()).sum("1,A,1,2,X,1, ,1,B,G,Uasdfasdfa")!=6){
			throw new Exception("Falla");
		}
		if((new Java8()).sum("1,A,1,2,X,1, ,1,B,G,Uasdfasdfa,1")!=7){
			throw new Exception("Falla");
		}
		if((new Java8()).sum("1,A,1,2,X,1, ,1,B,G,1.1,8.9,1")!=7){
			throw new Exception("Falla");
		}
		System.out.println("Correcto");
	}
	public static int bin(int a,int b){
		return a+b;
	}
	public static void main(String[]argumentos){
		try{
			//System.out.println(java.util.stream.IntStream.of(1,1,2).reduce(0,Java8::bin));
			//System.out.println(java.util.stream.Stream.of("A","B","C").collect(Collectors.toList()));
			/*Class clase=Class.forName("java.util.ArrayList");
			String s=clase.getResource("/java/util/ArrayList.class")+"";
			System.out.println(s);*/
			testSum();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}
