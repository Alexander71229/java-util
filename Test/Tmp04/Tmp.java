import java.util.*;
import java.util.stream.Collectors;
public class Tmp{
	private void n3(){
		n2();
	}
	private void n2(){
		n1();
	}
	private void n1(){
		traza(1);
	}
	private static String tx(StackTraceElement[]x,int a){
		StringBuffer sb=new StringBuffer();
		for(int i=2;i<x.length&&i<2+a;i++){
			sb.append(x[i].getClassName());
			sb.append(".");
			sb.append(x[i].getMethodName());
			sb.append(":");
			sb.append(x[i].getLineNumber());
			sb.append(" ");
		}
		return sb+"";
	}
	public static void traza(int a){
		System.out.println(tx(Thread.currentThread().getStackTrace(),a));
	}
	public static void main(String[]argumentos){
		try{
			(new Tmp()).n3();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}
