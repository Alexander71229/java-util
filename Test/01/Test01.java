//https://epmapd09-03.corp.epm.com.co/EPM.Materiales.Biztalk.WsHttp/DespachoMaterialesService.svc <https://epmapd09-03.corp.epm.com.co/EPM.Materiales.Biztalk.WsHttp/DespachoMaterialesService.svc> ?WSDL   
import java.io.*;
import javax.xml.bind.*;
public class Test01{
	public static void main(String[]args){
		try{
			JAXBContext jc=JAXBContext.newInstance(A.class);
			Unmarshaller u=jc.createUnmarshaller();
			File f=new File("a.xml");
			A a=(A)u.unmarshal(f);
			System.out.println(a.getNombre());
			System.out.println(a.getValor());
		}catch(Exception e){
			e.printStackTrace();
		}
	}	
}