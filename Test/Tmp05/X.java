import java.util.*;
import java.io.*;
class Grupo{
	public String nombre;
	public ArrayList<Jugador>jugadores;
	public Grupo(){
		jugadores=new ArrayList<Jugador>();
	}
	public Grupo(String nombre){
		this.nombre=nombre;
		jugadores=new ArrayList<Jugador>();
	}
	public void add(Jugador jugador){
		this.jugadores.add(jugador);
	}
	public int size(){
		return jugadores.size();
	}
	public String toString(){
		StringBuffer r=new StringBuffer();
		for(int i=0;i<jugadores.size();i++){
			if(i>0){
				r.append(" - ");
			}
			r.append(jugadores.get(i)+"");
		}
		return r+"";
	}
}
class Jugador{
	public String nombre;
	public String categoria;
	public Jugador(){
	}
	public Jugador(String nombre){
		this.nombre=nombre;
	}
	public Jugador(String nombre,String categoria){
		this.nombre=nombre;
		this.categoria=categoria;
	}
	public String toString(){
		return nombre;
	}
}
public class X{
	public static String formato2(ArrayList<Grupo>grupos){
		StringBuffer r=new StringBuffer();
		for(int i=0;i<grupos.size();i++){
			r.append("Grupo "+i+"\n");
			r.append("\t"+grupos.get(i)+"\n");
		}
		return r+"";
	}
	public static String formato(ArrayList<ArrayList<String>>grupos){
		StringBuffer r=new StringBuffer();
		for(int i=0;i<grupos.size();i++){
			r.append("Grupo "+i+"\n");
			for(int j=0;j<grupos.get(i).size();j++){
				if(j==0){
					r.append("\t");
				}else{
					r.append(" - ");
				}
				r.append(grupos.get(i).get(j));
			}
			r.append("\n");
		}
		return r+"";
	}
	public static ArrayList<ArrayList<String>>organizar(ArrayList<String>nombres,int t){
		ArrayList<ArrayList<String>>grupos=new ArrayList<ArrayList<String>>();
		Collections.shuffle(nombres);
		ArrayList<String>grupo=new ArrayList<String>();
		for(int i=0;i<nombres.size();i++){
			if(nombres.get(i).indexOf("$")>=0){
				continue;
			}
			if(grupo.size()>=t){
				grupos.add(grupo);
				grupo=new ArrayList<String>();
			}
			grupo.add(nombres.get(i));
		}
		grupos.add(grupo);
		return grupos;
	}
	public static ArrayList<Grupo>organizar2(ArrayList<String>cadenas){
		ArrayList<Grupo>grupos=new ArrayList<Grupo>();
		String categoria=null;
		HashMap<String,ArrayList<Jugador>>h=new HashMap<String,ArrayList<Jugador>>();
		ArrayList<Jugador>jugadores=new ArrayList<Jugador>();
		ArrayList<String>categorias=new ArrayList<String>();
		for(int i=0;i<cadenas.size();i++){
			if(cadenas.get(i).indexOf("$")>=0){
				categoria=cadenas.get(i).replace("$","");
				categorias.add(categoria);
				continue;
			}
			if(h.get(categoria)==null){
				h.put(categoria,new ArrayList<Jugador>());
			}
			jugadores.add(new Jugador(cadenas.get(i),categoria));
			h.get(categoria).add(jugadores.get(jugadores.size()-1));
		}
		//Collections.shuffle(h.get(categorias.get(1)));
		h.values().stream().forEach(x->Collections.shuffle(x));
		for(int i=0;i<h.get(categorias.get(0)).size();i++){
			Grupo grupo=new Grupo();
			grupo.add(h.get(categorias.get(0)).get(i));
			grupo.add(h.get(categorias.get(1)).get(i));
			grupos.add(grupo);
		}
		return grupos;
	}
	public static ArrayList<String>leer(File f)throws Exception{
		ArrayList<String>r=new ArrayList<String>();
		Scanner s=new Scanner(f);
		while(s.hasNextLine()){
			r.add(s.nextLine());
		}
		return r;
	}
	public static List<Grupo>crearEquipos(int n){
		List<Grupo>r=new ArrayList<Grupo>(n);
		for(int i=1;i<=n;i++){
			r.add(new Grupo("Grupo "+i));
		}
		return r;
	}
	public static List<Grupo>creados(File f)throws Exception{
		ArrayList<Grupo>r=new ArrayList<Grupo>();
		Scanner s=new Scanner(f);
		Grupo g=new Grupo();
		while(s.hasNext()){
			if(g.size()<2){
				g.add(new Jugador(s.next()));
			}else{
				r.add(g);
				g=new Grupo();
			}
		}
		return r;
	}
	public static void gen(List<Grupo>grupos){
		HashMap<String,String>h=new HashMap<>();
		for(int i=0;i<grupos.size();i++){
			U.imp("Fase "+i);
			for(int j=0;j<grupos.size();j++){
				if(i!=j){
					//U.imp("\t"+grupos.get(i).nombre+" -- "+grupos.get(j).nombre);
					String k=i+":"+j;
					if(i>j){
						k=j+":"+i;
					}
					if(h.get(k)!=null){
						String e="\t["+grupos.get(i)+"] -- ["+grupos.get(j)+"]";
						h.put(k,e);
						U.imp(e);
					}
				}
			}
		}
	}
	public static List<Jugador>leerJugadores(File f)throws Exception{
		List<Jugador>r=new ArrayList<Jugador>();
		Scanner s=new Scanner(f);
		while(s.hasNext()){
			r.add(new Jugador(s.next()));
		}
		return r;
	}
	public static void genJugadores(List<Jugador>jugadores){
		HashMap<String,String>h=new HashMap<>();
		for(int i=0;i<jugadores.size();i++){
			for(int j=0;j<jugadores.size();j++){
				if(i!=j){
					String k=i+":"+j;
					if(i>j){
						k=j+":"+i;
					}
					if(h.get(k)==null){
						String e=jugadores.get(i)+" -- "+jugadores.get(j);
						h.put(k,e);
						U.imp(e);
					}
				}
			}
		}
	}
	public static void main(String[]argumentos){
		try{
			U.ruta="Log.txt";
			U.imp("Inicio");
			//List<Grupo>grupos=organizar2(leer(new File("Entrada.txt")));
			//U.imp(formato2(grupos));
			//List<Grupo>grupos=crearEquipos(10);
			//List<Grupo>grupos=creados(new File("EquiposEntrada.txt"));
			//gen(grupos);
			genJugadores(leerJugadores(new File("Jugadores.txt")));
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}