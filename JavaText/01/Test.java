import java.io.*;
import java.util.*;
import java.math.*;
public class Test{
	public static PrintStream ps;
	public static void tAmbito01()throws Exception{
		Ambito.add();
		Ambito.add(new Var("Variable1","double"));
		Ambito.add(new Var("Variable2","double"));
		Ambito.add();
		Ambito.add(new Var("Variable3","double"));
		Ambito.add(new Var("Variable4","double"));
		Ambito.add();
		Ambito.add(new Var("Variable5","double"));
		Ambito.add(new Var("Variable6","double"));
		ps.println(Ambito.b("Variable3"));
		ps.println(Ambito.b("Variable1"));
		ps.println(Ambito.b("Variable2"));
		ps.println(Ambito.b("Variable6"));
		ps.println(Ambito.b("Variable7"));
		Ambito.rem();
		ps.println(Ambito.b("Variable6"));
		ps.println(Ambito.b("Variable5"));
	}
	public static void tTok01()throws Exception{
		String l="if(\"\\\"Esta es una 'prueba'\\\"\".length()=='3')/*Comentario corto*/{//Comentario final";
		ps.println(l);
		TDat d=Tok.tok(l);
		ps.println(d.toks);
		ps.println(d.cod+"");
		d=Tok.tok(l,true);
		ps.println(d.toks);
		ps.println(d.cod+"");
		l="final XSeedNumber test;";
		ps.println(l);
		d=Tok.tok(l);
		ps.println(d.toks);
		ps.println(d.cod+"");
		ps.println("Resultado de la b�squeda:"+d.b(new BEle[]{new BEle(new String[]{"XSeedNumber","double"},'P'),new BEle('P')}));
		l="public void funcion(int o,double x,XSeedNumber j){";
		ps.println(l);
		d=Tok.tok(l);
		ps.println(d.toks);
		ps.println(d.cod+"");
		ps.println("Resultado de la b�squeda:"+d.b(new BEle[]{new BEle(new String[]{"XSeedNumber","double"},'P'),new BEle('P')}));
		l="int y=5;";
		ps.println(l);
		d=Tok.tok(l);
		ps.println(d.toks);
		ps.println(d.cod+"");
		l="xsen=this.move(par1,par2,xsen,5,3,\"sasdf\");";
		ps.println(l);
		d=Tok.tok(l);
		ps.println(d.toks);
		ps.println(d.cod+"");
		ps.println("Resultado de la b�squeda:"+d.b("N,N,\""));
	}
	public static void tTox01()throws Exception{
		String l="if(\"\\\"Esta es una 'prueba'\\\"\".length()=='3')/*Comentario corto*/{//Comentario final";
		/*ps.println(l);
		TDat d=Tok.tok(l);
		ps.println(d.toks);
		ps.println(d.cod+"");
		d=Tok.tok(l,true);
		ps.println(d.toks);
		ps.println(d.cod+"");*/
	}
	public static void tSInto01()throws Exception{
		SInto.ejecucion();
	}
	public static void main(String[]argumentos){
		try{
			ps=new PrintStream(new FileOutputStream(new File("Test.txt")));
			//Ambito01();
			//tTok01();
			//tSInto01();
			//GFile.ejecucion();
			//GTable.ejecucion();
			//GMet.c();
			Main06.main(argumentos);
			//tTox01();
			//DatosTablas.crearAlters();
			//ActualizarTablasInnovation.main(argumentos);
			//InformacionArchivos.main(argumentos);
		}catch(Throwable t){
			ps.println(t);
			t.printStackTrace();
			t.printStackTrace(ps);
			//t.printStackTrace(new PrintStream(new FileOutputStream(new File("D:\\Desarrollo\\Java\\JavaText\\01\\LogC40.txt"))));
			try{
				t.printStackTrace(new PrintStream(new FileOutputStream(new File("D:\\Innovation\\COMPRASE\\Audit\\LogC40.txt"))));
			}catch(Exception expX){
			}
		}
	}
}