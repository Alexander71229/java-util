import java.io.*;
import java.util.*;
public class SInto{
	public static PrintStream ps=null;
	public static void ejecucion()throws Exception{
		ps=new PrintStream(new FileOutputStream(new File("SInto.txt")));
		ArrayList<File>fs=new ArrayList<File>();
		{
			File o=new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\Harvest\\Fuentes\\FuentesInnovation56\\Pages\\");
			File[]lista=o.listFiles();
			fs.addAll(Arrays.asList(lista));
		}
		{
			File o=new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\Harvest\\Fuentes\\FuentesInnovation56\\Reports\\");
			File[]lista=o.listFiles();
			fs.addAll(Arrays.asList(lista));
		}
		{
			File o=new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\Harvest\\Fuentes\\FuentesInnovation56\\Rules\\");
			File[]lista=o.listFiles();
			fs.addAll(Arrays.asList(lista));
		}
		fs=Datos.listarArchivos(fs);
		File[]lista=fs.toArray(new File[fs.size()]);
		HashMap<String,Integer>tipos=new HashMap<String,Integer>();
		//tipos.put("double",1);
		tipos.put("XseedNumber",1);
		//tipos.put("BigDecimal",1);
		for(int i=0;i<lista.length;i++){
			Ambito.reset();
			Ambito.add();
			BufferedReader br=new BufferedReader(new FileReader(lista[i]));
			String line=br.readLine();
			int k=0;
			boolean c2=false;
			boolean m=false;
			String nm=null;
			boolean iniciado=false;
			ArrayList<Var>ms=new ArrayList<Var>();
			boolean into=false;
			int cp=0;
			while(line!=null){
				k++;
				if(line.equals("<Methods>")||line.equals("<Attributes>")){
					iniciado=true;
					line=br.readLine();
					continue;
				}
				if(!iniciado){
					line=br.readLine();
					continue;
				}
				if(line.equals("</Attributes>")){
					iniciado=false;
					line=br.readLine();
					continue;
				}
				if(line.equals("</Methods>")){
					break;
				}
				TDat d=Tok.tok(line,c2);
				String cod=d.cod+"";
				char[]cs=cod.toCharArray();
				if(cs.length>0&&cs[cs.length-1]=='P'&&tipos.get(d.toks.get(cs.length-1))!=null){
					ps.println(lista[i]+"("+k+"): fin inesperado:"+line);
				}
				for(int j=0;j<d.toks.size();j++){
					String token=d.toks.get(j);
					if(cs[j]==';'){
						if(j>1&&cs[j-2]=='P'&&cs[j-1]=='P'){
							if(tipos.get(d.toks.get(j-2))!=null){
								Ambito.add(new Var(d.toks.get(j-1),d.toks.get(j-2)));
							}
						}
						continue;
					}
					if(cs[j]=='='){
						if(j>1&&cs[j-2]=='P'&&cs[j-1]=='P'){
							if(tipos.get(d.toks.get(j-2))!=null){
								Ambito.add(new Var(d.toks.get(j-1),d.toks.get(j-2)));
							}
						}
						continue;
					}
					if(cs[j]==','){
						if(j>1&&cs[j-2]=='P'&&cs[j-1]=='P'){
							if(tipos.get(d.toks.get(j-2))!=null){
								ms.add(new Var(d.toks.get(j-1),d.toks.get(j-2)));
								if(!m){
									ps.println(lista[i]+"("+k+"): Esto debe arreglarse:"+line);
								}
							}
						}
						continue;
					}
					if(cs[j]=='{'){
						m=false;
						Ambito.add();
						for(int x=0;x<ms.size();x++){
							Ambito.add(ms.get(x));
						}
						ms=new ArrayList<Var>();
						continue;
					}
					if(cs[j]=='}'){
						Ambito.rem();
						continue;
					}
					if(cs[j]=='('){
						cp++;
						if(j>1&&cs[j-2]=='P'&&cs[j-1]=='P'&&!d.g(j-2).equals("new")){
							m=true;
						}
						continue;
					}
					if(cs[j]==')'){
						cp--;
						if(cp==0&&into||true){
							into=false;
						}
						continue;
					}
					if(cs[j]=='.'){
						if(into){
							ps.println(lista[i]+"("+k+"):Variable compuesta desconocida:"+line);
						}
						continue;
					}
					if(token.equals("new")){
						if(tipos.get(d.toks.get(j+1))!=null&&cs[j-1]=='='&&cs[j+2]=='('){
							String n=d.toks.get(j-2);
							int l=d.gi(j+3);
							int p=d.gi(j+5);
							Var v=Ambito.b(n);
							if(v==null){
								ps.println(lista[i]+"("+k+"): No se encuentra la variable '"+n+"':"+line);
							}else{
								if(v.l>0&&v.p>0){
									if(v.l!=l||v.p!=p){
										ps.println(lista[i].getName()+"("+k+"): Redefinición de '"+n+"'["+v.l+","+v.p+"]:"+line);
									}
								}
								v.l=l;
								v.p=p;
							}
						}
						continue;
					}
					if(token.equals("into")){
						if(cs[j-1]=='.'&&cs[j+1]=='('){
							into=true;
						}
						continue;
					}
					if(into&&Ambito.b(token)!=null){
						Var v=Ambito.b(token);
						if(v.p>0&&v.p!=6){
							ps.println(Datos.categoria(lista[i])+":"+lista[i].getName()+"("+k+"):Consulta en variables numéricas("+v.n+")["+v.l+","+v.p+"]:"+line);
						}
						continue;
					}
				}
				c2=d.c2;
				line=br.readLine();
			}
		}
	}
	public static void main(String[]argumentos){
		try{
			ejecucion();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}