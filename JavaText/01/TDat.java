import java.util.*;
public class TDat{
	public String original;
	public ArrayList<String>toks;
	public StringBuffer cod;
	public boolean c2;
	public TDat(){
		this.toks=new ArrayList<String>();
		this.cod=new StringBuffer();
	}
	public void add(String palabra){
		this.toks.add(palabra);
	}
	public void append(String c){
		this.cod.append(c);
	}
	public ArrayList<String>b(BEle[]els){
		ArrayList<String>resultado=new ArrayList<String>();
		String cod=this.cod+"";
		for(int i=0;i<this.toks.size();i++){
			int x=0;
			for(int j=0;j<els.length;j++){
				int k=0;
				if(els[j].c>0){
					if(i+j<cod.length()-1&&els[j].c==cod.charAt(i+j)){
						k++;
					}
				}else{
					k++;
				}
				if(k==1){
					if(els[j].ts!=null){
						if(els[j].ts.get(this.toks.get(i))!=null){
							k++;
						}
					}else{
						k++;
					}
				}
				if(k==2){
					x++;
				}else{
					break;
				}
			}
			if(x==els.length){
				for(int j=0;j<x;j++){
					resultado.add(this.toks.get(i+j));
				}
			}
		}
		return resultado;
	}
	public ArrayList<String>b(String c){
		BEle[]bs=new BEle[c.length()];
		for(int i=0;i<c.length();i++){
			bs[i]=new BEle(c.charAt(i));
		}
		return b(bs);
	}
	public ArrayList<String>b(String c,int[]x){
		ArrayList<String>resultado=new ArrayList<String>();
		HashMap<Integer,Integer>h=new HashMap<Integer,Integer>();
		ArrayList<String>y=b(c);
		int mx=-1;
		for(int i=0;i<x.length;i++){
			if(x[i]>mx){
				mx=x[i];
			}
		}
		if(y.size()>mx){
			for(int i=0;i<x.length;i++){
				resultado.add(y.get(x[i]));
			}
		}
		return resultado;
	}
	public ArrayList<String>bs(String c){
		ArrayList<String>resultado=new ArrayList<String>();
		String[]ps=c.split(" ");
		char[]cs=(this.cod+"").toCharArray();
		if(this.toks.size()<ps.length){
			return resultado;
		}
		boolean t=false;
		for(int i=0;i<ps.length;i++){
			String p=ps[i];
			if(p.length()==3&&p.charAt(0)=='{'&&p.charAt(2)=='}'){
				if(cs[i]==p.charAt(1)||p.charAt(1)=='?'){
					if(p.charAt(1)=='?'){
						resultado.add(this.toks.get(i));
					}
				}else{
					t=true;
					break;
				}
			}else{
				if(p.equals(this.toks.get(i))){
				}else{
					t=true;
					break;
				}
			}
		}
		if(t){
			return new ArrayList<String>();
		}
		return resultado;
	}
	public ArrayList<Double>ds(){
		ArrayList<Double>resultado=new ArrayList<Double>();
		String c=this.cod+"";
		for(int i=0;i<c.length();i++){
			if(c.charAt(i)=='N'){
				resultado.add(Double.parseDouble(this.toks.get(i)));
			}
		}
		return resultado;
	}
	public ArrayList<Integer>is(){
		ArrayList<Integer>resultado=new ArrayList<Integer>();
		String c=this.cod+"";
		for(int i=0;i<c.length();i++){
			if(c.charAt(i)=='N'){
				resultado.add(Integer.parseInt(this.toks.get(i)));
			}
		}
		return resultado;
	}
	public Integer gi(int x)throws Exception{
		return Integer.parseInt(this.toks.get(x));
	}
	public String g(int x){
		return this.toks.get(x);
	}
	public String toString(){
		StringBuffer sb=new StringBuffer();
		char[]cs=(this.cod+"").toCharArray();
		for(int i=0;i<this.toks.size();i++){
			sb.append(this.toks.get(i));
			if(cs[i]=='P'&&i<this.toks.size()-1&&cs[i+1]=='P'){
				sb.append(" ");
			}
		}
		return sb+"";
	}
}