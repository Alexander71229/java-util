import java.io.*;
import java.util.*;
public class Main06{
	public static PrintStream ps=null;
	public static void main(String[]argumentos){
		try{
			HashMap<String,Integer>hu=new HashMap<String,Integer>();
			hu.put("multiply",1);
			hu.put("divide",1);
			HashMap<String,Integer>hx=new HashMap<String,Integer>();
			hx.put("multiply",1);
			hx.put("divide",1);
			hx.put("subtract",1);
			hx.put("add",1);
			HashMap<String,Integer>h=new HashMap<String,Integer>();
			h.put("multiply",1);
			h.put("move",1);
			h.put("divide",1);
			h.put("flag",1);
			h.put("subtract",1);
			h.put("add",1);
			HashMap<String,Integer>h2=new HashMap<String,Integer>();
			h2.put("*",1);
			h2.put("/",1);
			//h2.put("multiply",1);
			//h2.put("divide",1);
			ps=new PrintStream(new FileOutputStream(new File("Salida06.txt")));
			File[]lista=Datos.listarArchivos();
//lista=new File[]{new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\ProcyonProd\\RESTIMADOS.java")};
			for(int i=0;i<lista.length;i++){
				if(lista[i].getName().toLowerCase().indexOf(".java")<0){
					continue;
				}
				String nombre=lista[i].getName().replace(".java","");
				if(XVars.g(nombre)==null){
					continue;
				}
				String pre=nombre+":";
				BufferedReader br=new BufferedReader(new FileReader(lista[i]));
				String line=br.readLine();
				int k=0;
				boolean c2=false;
				String xseedRts="xseedRts";
				String HLegacy="HLegacy";
				String tipo=null;
				HashMap<String,Integer>vars=new HashMap<String,Integer>();
				HashMap<String,Integer>vxs=new HashMap<String,Integer>();
				HashMap<String,Integer>mods=new HashMap<String,Integer>();
				vars.put("ZEROS",0);
				vars.put("100",0);
				vars.put("0",0);
				XVars.a(vars,nombre);
				while(line!=null){
					k++;
					if(tipo==null){
						if(line.indexOf(xseedRts)>=0){
							tipo=xseedRts;
							ps.println(Datos.categoria(nombre)+":"+lista[i]+":"+tipo);
							line=br.readLine();
							continue;
						}
						if(line.indexOf(HLegacy)>=0){
							tipo=HLegacy;
							ps.println(Datos.categoria(nombre)+":"+lista[i]+":"+tipo);
							line=br.readLine();
							continue;
						}
					}
					TDat d=Tok.tok(line,c2);
					String cod=d.cod+"";
					char[]cs=cod.toCharArray();
					boolean bh=false;
					int l=0;
					int p=0;
					int cz=0;
					String pcor=null;
					String vx=null;
					String asig=null;
					String mtd=null;
					int[]basep=null;
					int pact=0;
					int cl=0;
					for(int j=0;j<d.toks.size();j++){
						String token=d.toks.get(j);
/*if(k==737){
	ps.println(cs[j]+"<----->"+token);
}*/
						if(h2.get(token)!=null){
							ps.println(pre+"("+k+")(Operaciones)->"+line);
							break;
						}
						if(token.equals("into")&&cs[j-1]=='.'){
							ps.println(pre+"("+k+")(INTO)->"+line);
						}
						if(token.equals("set")&&cs[j-1]=='.'){//&&vars.get(d.toks.get(j-2))!=null
							String aset=d.toks.get(j-2);
							for(;j<d.toks.size();j++){
								if(cs[j]==')'&&cs[j+1]==';'){
									int npx=-1;
									if(cs[j-1]=='P'&&cs[j-2]=='.'&&cs[j-3]=='P'&&!d.toks.get(j-3).equals("this")){
										String ntx=d.toks.get(j-3)+"."+d.toks.get(j-1);
										ntx=ntx.toUpperCase();
										if(DatosTablas.obtener(ntx)==null){
											ps.println(pre+"("+k+"):Compuesto desconocido96:"+ntx+":"+line);
										}else{
											npx=DatosTablas.obtener(ntx)[3];
										}
									}else{
										String ntx=d.toks.get(j-1);
										if(cs[j-1]==')'){
											ntx=d.toks.get(j-3);
											if(ntx.equals("toDouble")){
												ntx=d.toks.get(j-5);
											}
										}
										if(vars.get(ntx)==null){
											if(vars.get(aset)!=null){
												ps.println(pre+"("+k+"):desconocido103(SET):"+ntx+":"+line);
											}
										}else{
											npx=vars.get(ntx);
										}
									}
									if(npx>=0&&npx>vars.get(aset)){
										vars.put(aset,npx);
										mods.put(aset,npx);
										ps.println(pre+"actualizado(SET)"+aset+"="+npx);
										ps.println(pre+"Se actualiza(SET)"+aset+" a "+vars.get(aset));
									}
								}
							}
							break;
						}
						if(h.get(token)!=null){
							bh=true;
							mtd=token;
							if(asig==null){
								//ps.println(pre+"("+k+")(Sin asig)->"+line);
								j=j+2;
								int npxmx=0;
								for(;j<d.toks.size();j++){
									if(cs[j]==','){
										int npx=0;
										if(cs[j-1]=='P'&&cs[j-2]=='.'&&cs[j-3]=='P'&&!d.toks.get(j-3).equals("this")){
											String ntx=d.toks.get(j-3)+"."+d.toks.get(j-1);
											ntx=ntx.toUpperCase();
											if(DatosTablas.obtener(ntx)==null){
												ps.println(pre+"("+k+"):Compuesto desconocido:"+ntx+":"+line);
											}else{
												npx=DatosTablas.obtener(ntx)[3];
											}
										}else{
											String ntx=d.toks.get(j-1);
											if(vars.get(ntx)==null){
												ps.println(pre+"("+k+"):desconocido110:"+ntx+":"+line);
											}else{
												npx=vars.get(ntx);
											}
										}
										if(mtd.equals("add")||mtd.equals("subtract")){
											if(npx>npxmx){
												npx=npxmx;
											}
										}
										if(mtd.equals("divide")){
											npxmx=6;
										}
										if(mtd.equals("multiply")){
											npxmx+=npx;
										}
									}
									if(cs[j]==')'&&cs[j+1]==';'){
										if(vars.get(d.toks.get(j-1))==null){
											ps.println(pre+"("+k+"):Asignación a desconocido:"+d.toks.get(j-1)+":"+line);
										}else{
											if(npxmx>vars.get(d.toks.get(j-1))){
												vars.put(d.toks.get(j-1),npxmx);
												mods.put(d.toks.get(j-1),npxmx);
												ps.println(pre+"actualizado(operaciones)"+d.toks.get(j-1)+"="+npxmx);
												ps.println(pre+"Se actualiza(operaciones)"+d.toks.get(j-1)+" a "+vars.get(d.toks.get(j-1)));
											}
										}
									}
								}
								break;
							}
							continue;
						}
						if(asig!=null){
							if(cs[j]=='['){
								pcor=d.toks.get(j-1);
								cl++;
								if(mtd!=null&&mtd.equals("divide")){
									pact=6;
									continue;
								}
							}
							if(cs[j]==']'){
								cl--;
							}
							if(cl>0){
								continue;
							}
						}
						if(cs[j]==','){
							cz++;
						}
						if(j+1<d.toks.size()&&cs[j]==')'&&cs[j+1]==';'){
							cz++;
						}
						if(bh){
							if(cz==1){
								if((j>=1&&cs[j]==','&&cs[j-1]=='P')||(j>=1&&cs[j]==','&&cs[j-1]==']')){
									String ntx=d.toks.get(j-1);
									if(pcor!=null){
										ntx=pcor;
									}
									if(vars.get(ntx)!=null){
										pact=vars.get(ntx);
										continue;
									}
									if(DatosTablas.obtener(ntx.replace("_","."))!=null){
										pact=DatosTablas.obtener(ntx.replace("_","."))[3];
										continue;
									}
									if(DatosQuery.p(ntx)>=0){
										pact=DatosQuery.p(ntx);
										continue;
									}
									vx=ntx;
									if(asig!=null&&asig.equals("COMPUTE")){
										vxs.put(vx,-1);
									}
									if(hx.get(mtd)!=null){
										ps.println(pre+"("+k+"):desconocido95:"+ntx+":"+line);
										break;
									}
								}
							}
							if(cz==2){
								if(hx.get(mtd)!=null){
									if(j>=1&&(cs[j]==','||cs[j]==')')&&cs[j-1]=='P'){
										String ntx=d.toks.get(j-1);
										int npx=-1;
										if(asig.equals(ntx)&&hu.get(mtd)==null){
											continue;
										}
										if(vars.get(ntx)!=null){
											npx=vars.get(ntx);
										}else{
											if(DatosTablas.obtener(ntx.replace("_","."))!=null){
												npx=DatosTablas.obtener(ntx.replace("_","."))[3];
											}else{
												if(DatosQuery.p(ntx)>=0){
													npx=DatosQuery.p(ntx);
												}
											}
										}
										if(npx==-1){
											vxs.put(vx,-1);
											vxs.put(ntx,-1);
											ps.println(pre+"("+k+"):desconocido121:"+ntx+":"+line);
											//break;
										}
										if(mtd.equals("divide")){
											pact=6;
										}
										if(mtd.equals("multiply")){
											pact+=npx;
										}
										if((mtd.equals("add")||mtd.equals("substract"))){
											if(pact<npx){
												pact=npx;
											}
										}
										if(cs[j]==')'){
											vars.put(asig,pact);
											ps.println(asig+"="+vars.get(asig)+"("+k+"):"+line);
										}
									}
								}
							}
						}
						if(bh&&j>=2&&cs[j]=='N'&&cs[j-1]==','&&cs[j-2]=='N'){
							vxs.put(vx,-1);
							p=Integer.parseInt(d.toks.get(j));
							l=Integer.parseInt(d.toks.get(j-2));
							int npx=Math.max(p,pact);
							if(pact>0){
								npx=pact;
							}
							if(vars.get(asig)!=null){
								npx=Math.max(npx,vars.get(asig));
							}
							if(npx>6){
								ps.println(pre+"Supera la precisión se establece en 6");
								npx=6;
							}
							if(p!=pact){
								if(vx!=null){
									ps.println(pre+"("+k+"){Desconocido:}"+vx);
								}
								ps.println(pre+"("+k+"){Se estima "+pact+"}"+asig+"=["+pact+"="+p+"]->"+line);
							}
							if(vars.get(asig)==null){
								vars.put(asig,npx);
								ps.println(pre+"Creado "+asig+"="+npx);
								if(vxs.get(asig)!=null){
									vxs.put(asig,npx);
								}
							}else{
								//if(vars.get(asig)!=p&&vars.get(asig)!=npx){
								if(vars.get(asig)!=npx){
									ps.println(pre+"("+k+")"+asig+"=["+vars.get(asig)+"="+p+"]->"+line);
									vars.put(asig,npx);
									if(vxs.get(asig)!=null){
										vxs.put(asig,npx);
									}
									ps.println(pre+"actualizado "+asig+"="+npx);
									ps.println(pre+"Se actualiza "+asig+" a "+vars.get(asig));
								}
							}
							if(basep!=null){
								if(basep[3]!=p){
									/*ps.println(pre+"("+k+"){campo de la base de datos}"+asig+"=["+l+"="+basep[2]+","+p+"="+basep[3]+"]->"+line);
									break;*/
								}
							}
							if(p>0){
								//ps.println(pre+"("+k+")(Precision mayor a 0)->"+line);
								//break;
							}
						}
						if(cs[j]=='P'){
							if(DatosTablas.obtener(d.toks.get(j).toUpperCase().replace("_","."))!=null){
								int[]ds=DatosTablas.obtener(d.toks.get(j).toUpperCase().replace("_","."));
								basep=ds;
								if(ds[3]>6){
									/*ps.println(pre+"("+k+")["+d.toks.get(j)+"]("+ds[2]+","+ds[3]+")->"+line);
									break;*/
								}
							}
						}
						if(j>=2&&cs[j]=='P'&&cs[j-1]=='.'&&cs[j-2]=='P'){
							String ncam=(d.toks.get(j-2)+"."+d.toks.get(j)).toUpperCase();
							if(DatosTablas.obtener(ncam)!=null){
								int[]ds=DatosTablas.obtener(ncam);
								basep=ds;
								if(ds[3]>6){
									ps.println(pre+"("+k+")["+ncam+"]("+ds[2]+","+ds[3]+")->"+line);
									break;
								}
							}
						}
						if(j>=1&&cs[j]=='='&&cs[j-1]=='P'){
							asig=d.toks.get(j-1);
							if(DatosTablas.obtener(asig.replace("_","."))==null&&line.indexOf(".getDouble(")>=0){
								ps.println(pre+"("+k+"){Origen de consulta}"+asig+":"+line);
							}
							if(d.toks.get(j+1).equals("new")&&d.toks.get(j+2).equals("XseedNumber")){
								int npx=Integer.parseInt(d.toks.get(j+6));
								ps.println(pre+"("+k+")Creado "+asig+"="+npx);
								vars.put(asig,npx);
							}
							continue;
						}
						if(j>=1&&cs[j]=='='&&cs[j-1]==']'){
							int ccx=0;
							for(int dx=j;dx>=0;dx--){
								if(cs[dx]==']'){
									ccx++;
									continue;
								}
								if(cs[dx]=='['){
									ccx--;
									continue;
								}
								if(cs[dx]=='P'&&ccx==0){
									asig=d.toks.get(dx);
									break;
								}
							}
							continue;
						}
					}
					c2=d.c2;
					line=br.readLine();
				}
				ps.println(lista[i]);
				ps.println("Modificaciones");
				for(Map.Entry<String,Integer>et:mods.entrySet()){
					if(et.getValue()>=0){
						ps.println(et.getKey()+":"+et.getValue());
					}
				}
				ps.println("inicio");
				for(Map.Entry<String,Integer>et:vxs.entrySet()){
					if(et.getValue()>=0){
						ps.println("vars.put(\""+et.getKey()+"\","+et.getValue()+");");
					}
				}
				for(Map.Entry<String,Integer>et:vars.entrySet()){
					if(et.getValue()>=6&&DatosTablas.obtener(et.getKey().replace("_","."))!=null&&DatosTablas.obtener(et.getKey().replace("_","."))[3]<6){
						ps.println(et.getKey().replace("_","."));
					}
				}
				for(Map.Entry<String,Integer>et:vars.entrySet()){
					if(et.getValue()>=6&&DatosTablas.obtener(et.getKey().replace("_","."))==null){
						ps.println(et.getKey().replace("_","-"));
					}
				}
			}
		}catch(Throwable t){
			t.printStackTrace(ps);
		}
	}
}