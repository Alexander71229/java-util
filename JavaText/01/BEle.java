import java.util.*;
public class BEle{
	public HashMap<String,String>ts;
	public char c;
	public BEle(){
	}
	public BEle(HashMap<String,String>ts,char c){
		this.ts=ts;
		this.c=c;
	}
	public BEle(String[]ts,char c){
		this.ts=new HashMap<String,String>();
		for(int i=0;i<ts.length;i++){
			this.ts.put(ts[i],"");
		}
		this.c=c;
	}
	public BEle(HashMap<String,String>ts){
		this.ts=ts;
		this.c=0;
	}
	public BEle(String[]ts){
		this.ts=new HashMap<String,String>();
		for(int i=0;i<ts.length;i++){
			this.ts.put(ts[i],"");
		}
		this.c=0;
	}
	public BEle(char c){
		this.c=c;
		this.ts=null;
	}
}