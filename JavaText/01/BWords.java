import java.io.*;
import java.util.*;
public class BWords{
	public static HashMap<String,Integer>h;
	public static boolean px(String w)throws Exception{
		for(int i=0;i<w.length();i++){
			if(!Character.isLetter(w.charAt(i))){
				return false;
			}
		}
		return true;
	}
	public static void main(String[]argumentos){
		try{
			h=new HashMap<String,Integer>();
			String dir="D:\\Tmp\\En\\Words\\";
			String base="Base.txt";
			BufferedReader br=new BufferedReader(new FileReader(new File(dir+base)));
			BufferedReader entrada=new BufferedReader(new FileReader(new File(dir+"entrada.txt")));
			PrintStream ps=new PrintStream(new FileOutputStream(new File(dir+"Export.txt")));
			String line=br.readLine();
			while(line!=null){
				String[]ss=line.split(" ");
				for(int i=0;i<ss.length;i++){
					ss[i]=ss[i].trim().toLowerCase();
					if(ss[i].length()>0&&px(ss[i])){
						h.put(ss[i],1);
					}
				}
				line=br.readLine();
			}
			br.close();
			line=entrada.readLine();
			while(line!=null){
				String[]ss=line.split(" ");
				for(int i=0;i<ss.length;i++){
					ss[i]=ss[i].trim().toLowerCase();
					if(ss[i].length()>0&&px(ss[i])){
						h.put(ss[i],1);
					}
				}
				line=entrada.readLine();
			}
			entrada.close();
			PrintStream psb=new PrintStream(new FileOutputStream(new File(dir+base)));
			for(Map.Entry<String,Integer>e:h.entrySet()){
				psb.println(e.getKey());
			}
			psb.close();
			for(Map.Entry<String,Integer>e:h.entrySet()){
				ps.println(e.getKey()+", "+e.getKey()+", "+e.getKey());
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}