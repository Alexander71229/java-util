import java.util.*;
public class Ambito{
	public HashMap<String,Var>v;
	public static ArrayList<Ambito>a=new ArrayList<Ambito>();
	public Ambito(){
		v=new HashMap<String,Var>();
	}
	public static void add(){
		a.add(new Ambito());
	}
	public static void add(Var v){
		a.get(a.size()-1).v.put(v.n,v);
	}
	public static void add(HashMap<String,Var>vx){
		a.get(a.size()-1).v=vx;
	}
	public static Var b(String n){
		for(int i=a.size()-1;i>=0;i--){
			Var v=a.get(i).v.get(n);
			if(v!=null){
				return v;
			}
		}
		return null;
	}
	public static void rem(){
		a.remove(a.size()-1);
	}
	public static void reset(){
		a=new ArrayList<Ambito>();
	}
	public String imprimir(){
		StringBuffer sb=new StringBuffer();
		for(Map.Entry<String,Var>entrada:v.entrySet()){
			sb.append(entrada.getValue().toString());
			sb.append("\n");
		}
		return sb+"";
	}
	public static String imp(){
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<a.size();i++){
			sb.append(a.get(i).imprimir());
		}
		return sb+"";
	}
}