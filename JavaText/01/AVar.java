import java.io.*;
import java.util.*;
public class AVar{
	public static ArrayList<String>ps;
	public static ArrayList<Character>ts;
	public static int i;
	public static PrintStream px;
	public static HashMap<String,Integer>tipos;
	public static ArrayList<Var>b;
	public static int c;
	public static HashMap<String,Var>vs;
	public static void reset(){
		ps=null;
		ts=null;
		vs=new HashMap<String,Var>();
		c=0;
	}
	public static void add(String p,char t){
		if(tipos==null){
			tipos=new HashMap<String,Integer>();
			tipos.put("double",1);
			tipos.put("Double",1);
			tipos.put("float",1);
			tipos.put("Float",1);
			tipos.put("XseedNumber",1);
			tipos.put("BigDecimal",1);
		}
		if(ps==null){
			ps=new ArrayList<String>();
		}
		if(ts==null){
			ts=new ArrayList<Character>();
		}
		if(b==null){
			b=new ArrayList<Var>();
		}
		ps.add(p);
		ts.add(t);
		i=ts.size()-1;
	}
	public static boolean a(String p,char t){
		add(p,t);
		//Character[]ts=AVar.ts.toArray(new Character[AVar.ts.size()]);
		//String[]ps=AVar.ps.toArray(new String[AVar.ps.size()]);
		if(ts.get(i)=='{'){
			Ambito.add();
			for(int j=0;j<b.size();j++){
				Ambito.add(b.get(j));
			}
			b=new ArrayList<Var>();
			return false;
		}
		if(ts.get(i)==';'){
			for(int j=0;j<b.size();j++){
				Ambito.add(b.get(j));
			}
			b=new ArrayList<Var>();
			return false;
		}
		if(ts.get(i)=='}'){
			Ambito.rem();
			return false;
		}
		if(ts.size()>2){
			if((ts.get(i)==';'||ts.get(i)=='=')&&ts.get(i-1)=='P'&&ts.get(i-2)=='P'&&tipos.get(ps.get(i-2))!=null){
				Ambito.add(new Var(ps.get(i-1),ps.get(i-2)));
				return true;
			}
			if((ts.get(i)==','||ts.get(i)==')')&&ts.get(i-1)=='P'&&ts.get(i-2)=='P'&&tipos.get(ps.get(i-2))!=null){
				b.add(new Var(ps.get(i-1),ps.get(i-2)));
				//px.println(b);
				return true;
			}
		}
		return false;
	}
	public static void g(String p,char t){
		add(p,t);
		if(ts.get(i)=='{'){
			c++;
			return;
		}
		if(ts.get(i)=='}'){
			c--;
			return;
		}
		if(c==1&&ts.size()>2){
			if((ts.get(i)==';'||ts.get(i)=='=')&&ts.get(i-1)=='P'&&ts.get(i-2)=='P'&&tipos.get(ps.get(i-2))!=null){
				vs.put(ps.get(i-1),new Var(ps.get(i-1),ps.get(i-2)));
				return;
			}
		}
	}
	public static boolean a(Ambitos ambito,String p,char t){
		add(p,t);
		//Character[]ts=AVar.ts.toArray(new Character[AVar.ts.size()]);
		//String[]ps=AVar.ps.toArray(new String[AVar.ps.size()]);
		if(ts.get(i)=='{'){
			ambito.add();
			for(int j=0;j<b.size();j++){
				ambito.add(b.get(j));
			}
			b=new ArrayList<Var>();
			return false;
		}
		if(ts.get(i)==';'){
			for(int j=0;j<b.size();j++){
				ambito.add(b.get(j));
			}
			b=new ArrayList<Var>();
			return false;
		}
		if(ts.get(i)=='}'){
			ambito.rem();
			return false;
		}
		if(ts.size()>2){
			if((ts.get(i)==';'||ts.get(i)=='=')&&ts.get(i-1)=='P'&&ts.get(i-2)=='P'&&tipos.get(ps.get(i-2))!=null){
				ambito.add(new Var(ps.get(i-1),ps.get(i-2)));
				return true;
			}
			if((ts.get(i)==','||ts.get(i)==')')&&ts.get(i-1)=='P'&&ts.get(i-2)=='P'&&tipos.get(ps.get(i-2))!=null){
				b.add(new Var(ps.get(i-1),ps.get(i-2)));
				//px.println(b);
				return true;
			}
		}
		return false;
	}
}