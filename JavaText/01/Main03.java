import java.io.*;
import java.util.*;
public class Main03{
	public static String mensaje="";
	public static StringBuffer redondeo(String linea)throws Exception{
		StringBuffer sb=new StringBuffer();
		HashMap<Integer,Integer>a=new HashMap<Integer,Integer>();
		HashMap<Integer,Integer>b=new HashMap<Integer,Integer>();
		HashMap<Integer,Integer>x=new HashMap<Integer,Integer>();
		for(int i=0;i<linea.length();i++){
			if((linea.charAt(i)=='*'||linea.charAt(i)=='/')&&x.get(i)==null){
				int j=i-1;
				int c=0;
				mensaje=linea;
				while(!(c==0&&(linea.charAt(j)==','||linea.charAt(j)=='('||linea.charAt(j)=='='))){
					if(linea.charAt(j)==')'){
						c++;
					}
					if(linea.charAt(j)=='('){
						c--;
					}
					j--;
				}
				j++;
				a.put(j,1);
				c=0;
				while(!(c==0&&(j>=linea.length()||linea.charAt(j)==','||linea.charAt(j)==')'||linea.charAt(j)==';'))){
					if(linea.charAt(j)==')'){
						c++;
					}
					if(linea.charAt(j)=='('){
						c--;
					}
					if((linea.charAt(j)=='*'||linea.charAt(j)=='/')&&c==0){
						x.put(j,1);
					}
					j++;
				}
				b.put(j,1);
			}
		}
		for(int i=0;i<=linea.length();i++){
			if(a.get(i)!=null){
				sb.append("Precision.ajustar(");
			}
			if(b.get(i)!=null){
				sb.append(")");
			}
			if(i<linea.length()){
				sb.append(linea.charAt(i));
			}
		}
		return sb;
	}
	public static StringBuffer tabulacion(String linea)throws Exception{
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<linea.length();i++){
			char c=linea.charAt(i);
			if(c=='\t'||c==' '){
				sb.append(c);
			}else{
				break;
			}
		}
		return sb;
	}
	public static void main(String[]argumentos){
		PrintStream ps1=null;
		try{
			//redondeo("ab.set(ad+5,ac1.dc2()*xc4.ec5()/3.4,rx2,afc2());");
			ps1=new PrintStream(new FileOutputStream(new File("Tmp.txt")));
			//ps1.println(redondeo("ab2=acf(sd3+67,ugt7.gred2()*ttry1.dfgh1()+agr3.rty6()*4.5*gds.hs3()/7.8,ty())*ac3()/6.7;"));
			ps1.println(Tok.tok("final XseedNumber n=new XseedNumber(3,4);"));
			System.exit(0);
			String carpeta="D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\Harvest\\Fuentes\\FuentesInnovation56\\Reports\\";
			//String carpeta="D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\Harvest\\Fuentes\\FuentesInnovation56\\Pages\\";
			String nombre="GenerarReporteEnergiasMNR.Report";
			String completo=carpeta+nombre;
			BufferedReader br=new BufferedReader(new FileReader(new File(completo)));
			StringBuffer sb=new StringBuffer();
			String linea=br.readLine();
			int k=0;
			boolean c2=false;
			boolean iniciar=false;
			while(linea!=null){
				if(iniciar&&linea.startsWith("<")){
					iniciar=false;
				}
				if(!iniciar){
					if(linea.equals("<Methods>")){
						iniciar=true;
					}
					sb.append(linea);
					sb.append("\r\n");
					linea=br.readLine();
					continue;
				}
				k++;
				int pmultiply=linea.indexOf("multiply");
				int pdivide=linea.indexOf("divide");
				if(pmultiply>=0||pdivide>=0){
					int pcom=linea.indexOf("//");
					if(pcom>=0){
						if(pcom<pmultiply||pcom<pdivide){
							sb.append(linea);
							sb.append("\r\n");
							linea=br.readLine();
							continue;
						}
					}
					int a=linea.lastIndexOf(',');
					int b=linea.lastIndexOf(')');
					String variable=linea.substring(a+1,b).trim();
					StringBuffer tab=tabulacion(linea);
					sb.append(tab+variable+".decimals=Precision.precision();\r\n");
					sb.append(linea);
					sb.append("\r\n");
					sb.append(tab+"Precision.autoAjustar("+variable+");\r\n");
					linea=br.readLine();
					continue;
				}
				boolean c=false;
				for(int j=0;j<linea.length();j++){
					if(c2){
						if(linea.charAt(j)=='*'){
							if(linea.charAt(j+1)=='/'){
								c2=false;
								j++;
								continue;
							}
						}
					}else{
						if(linea.charAt(j)=='/'){
							if(linea.charAt(j+1)=='*'){
								c2=true;
								j++;
								continue;
							}
						}
						if(c){
							if(linea.charAt(j)=='\\'){
								if(linea.charAt(j+1)=='"'){
									j++;
									continue;
								}
							}
							if(linea.charAt(j)=='"'){
								c=false;
								continue;
							}
						}else{
							if(linea.charAt(j)=='"'){
								c=true;
								continue;
							}
							if(linea.charAt(j)=='/'&&linea.charAt(j+1)=='/'){
								j++;
								break;
							}
							if(linea.charAt(j)=='*'||linea.charAt(j)=='/'){
								if(linea.indexOf("this.GLB.MSGINDEX")<0){
									linea=redondeo(linea)+"";
									break;
								}
							}
						}
					}
				}
				sb.append(linea);
				sb.append("\r\n");
				linea=br.readLine();
			}
			br.close();
			PrintStream ps=new PrintStream(new FileOutputStream(new File(completo)));
			ps.print(sb+"");
		}catch(Throwable t){
			t.printStackTrace();
			ps1.println(mensaje);
		}
	}
}
