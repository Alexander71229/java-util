import java.util.*;
public class Ambitos{
	public ArrayList<Ambito>a;
	public Ambitos(){
		a=new ArrayList<Ambito>();
	}
	public void add(){
		a.add(new Ambito());
	}
	public void add(Var v){
		a.get(a.size()-1).v.put(v.n,v);
	}
	public void add(HashMap<String,Var>vx){
		a.get(a.size()-1).v=vx;
	}
	public Var b(String n){
		for(int i=a.size()-1;i>=0;i--){
			Var v=a.get(i).v.get(n);
			if(v!=null){
				return v;
			}
		}
		return null;
	}
	public void rem(){
		a.remove(a.size()-1);
	}
	public void reset(){
		a=new ArrayList<Ambito>();
	}
	public String imp(){
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<a.size();i++){
			sb.append(a.get(i).imprimir());
		}
		return sb+"";
	}
}