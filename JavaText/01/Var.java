public class Var{
	public String n;
	public String t;
	public int l;
	public int p;
	public Var(){
	}
	public Var(String n,String t){
		this.n=n;
		this.t=t;
	}
	public Var(String n,String t,int l,int p){
		this.n=n;
		this.t=t;
		this.l=l;
		this.p=p;
	}
	public String toString(){
		return this.t+" "+this.n;
	}
}