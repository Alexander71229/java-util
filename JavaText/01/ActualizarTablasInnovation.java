import java.util.*;
import java.io.*;
public class ActualizarTablasInnovation{
	public static void main(String[]argumentos){
		try{
			PrintStream ps=new PrintStream(new FileOutputStream(new File("ActualizarTablasInnovation.txt")));
			File o=new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\Harvest\\Fuentes\\FuentesInnovation56\\Tables\\");
			File[]lista=o.listFiles();
			for(int i=0;i<lista.length;i++){
				if(lista[i].getName().toLowerCase().indexOf(".table")<0){
					continue;
				}
				String tabla=lista[i].getName().replace(".Table","").toUpperCase();
				BufferedReader br=new BufferedReader(new FileReader(lista[i]));
				int k=0;
				StringBuffer sb=new StringBuffer();
				String line=br.readLine();
				String campo=null;
				while(line!=null){
					k++;
					if(line.indexOf("<Name>")>=0){
						campo=line.replace("<Name>","").replace("</Name>","").trim();
						ps.println(campo);
					}
					if(line.indexOf("<Length>")>=0){
						if(DatosTablas.obtener(tabla+"."+campo)!=null&&DatosTablas.obtener(tabla+"."+campo)[2]>0){
							line="    <Length>"+DatosTablas.obtener(tabla+"."+campo)[2]+"</Length>";
						}
					}
					if(line.indexOf("<Decimals>")>=0){
						if(DatosTablas.obtener(tabla+"."+campo)!=null){
							line="    <Decimals>"+DatosTablas.obtener(tabla+"."+campo)[3]+"</Decimals>";
						}
					}
					sb.append(line);
					sb.append("\r\n");
					line=br.readLine();
				}
				br.close();
				{
					PrintStream psx=new PrintStream(new FileOutputStream(lista[i]));
					psx.print(sb);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}