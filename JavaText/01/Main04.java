import java.io.*;
import java.util.*;
public class Main04{
	public static void main(String[]argumentos){
		try{
			PrintStream ps=new PrintStream(new FileOutputStream(new File("Salida04.txt")));
			File o=new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\ProcyonProd\\");
			File[]lista=Datos.listarArchivos();
			for(int i=0;i<lista.length;i++){
				if(lista[i].getName().toLowerCase().indexOf(".java")<0){
					continue;
				}
				String nombre=lista[i].getName().replace(".java","");
				BufferedReader br=new BufferedReader(new FileReader(lista[i]));
				String line=br.readLine();
				int k=0;
				while(line!=null){
					k++;
					if(line.indexOf("multiply")>=0||line.indexOf("divide")>=0||line.toLowerCase().indexOf(" round")>=0||line.toLowerCase().indexOf(".round")>=0||line.toLowerCase().indexOf(".redondear")>=0||line.toLowerCase().indexOf(" redondear")>=0){
						if(line.indexOf("G_VALACTN")<0&&line.indexOf("G_VALANTN")<0){
							ps.println(Datos.categoria(nombre)+":"+nombre+"("+k+")->"+line);
						}
					}
					boolean c=false;
					for(int j=0;j<line.length();j++){
						if(c){
							if(line.charAt(j)=='\\'){
								if(line.charAt(j+1)=='"'){
									j++;
									continue;
								}
							}
							if(line.charAt(j)=='"'){
								c=false;
								continue;
							}
						}else{
							if(line.charAt(j)=='"'){
								c=true;
								continue;
							}
							if(line.charAt(j)=='/'&&line.charAt(j+1)=='/'){
								j++;
								continue;
							}
							if(line.charAt(j)=='*'||line.charAt(j)=='/'){
								if(line.indexOf("this.GLB.MSGINDEX")<0){
									ps.println(categoria(nombre)+":"+nombre+"("+k+")->"+line.charAt(j)+":"+j+"|"+line);
									continue;
								}
							}
						}
					}
					line=br.readLine();
				}
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}