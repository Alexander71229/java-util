import java.io.*;
import java.util.*;
public class Main05{
	public static void main(String[]argumentos){
		try{
			HashMap<String,Integer>tipos=new HashMap<String,Integer>();
			tipos.put("double",1);
			tipos.put("XseedNumber",1);
			tipos.put("BigDecimal",1);
			HashMap<String,Integer>mods=new HashMap<String,Integer>();
			mods.put("move",1);
			mods.put("add",1);
			mods.put("subtract",1);
			mods.put("multiply",1);
			mods.put("divide",1);
			HashMap<String,Integer>inm=new HashMap<String,Integer>();
			inm.put("round",1);
			inm.put("multiply",1);
			inm.put("divide",1);
			inm.put("redondear",1);
			inm.put("*",1);
			inm.put("/",1);
			PrintStream ps=new PrintStream(new FileOutputStream(new File("Salida05.txt")));
			File o=new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\ProcyonProd\\");
			File[]lista=Datos.listarArchivos();
			lista=new File[]{new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\ProcyonProd\\C03CONTRATOMNR.java"),new File("D:\\Desarrollo\\EPM\\COMPRASE\\Fuentes\\ProcyonProd\\ProyeccionConsumos.java")};
			for(int i=0;i<lista.length;i++){
				String nombre=lista[i].getName().replace(".java","");
				Ambito.reset();
				BufferedReader br=new BufferedReader(new FileReader(lista[i]));
				ps.println(lista[i]);
				String line=br.readLine();
				int k=0;
				boolean c2=false;
				boolean tipo=false;
				boolean mod=false;
				String ts="";
				String atok=null;
				Var m=null;
				String vasig=null;
				while(line!=null){
					k++;
					TDat d=Tok.tok(line,c2);
					String cod=d.cod+"";
					for(int j=0;j<d.toks.size();j++){
						String token=d.toks.get(j);
						if(cod.charAt(j)==';'){
							vasig=null;
							m=null;
						}
						if(cod.charAt(j)=='{'){
							vasig=null;
							Ambito.add();
							continue;
						}
						if(cod.charAt(j)=='}'){
							Ambito.rem();
							continue;
						}
						if(cod.charAt(j)=='='){
							if(atok!=null){
								if(Ambito.b(atok)!=null){
									m=Ambito.b(atok);
								}
								vasig=atok;
							}
							continue;
						}
						if(inm.get(token)!=null){
							ps.println(Datos.categoria(nombre)+":"+nombre+"("+k+")->"+line);
							continue;
						}
						if(tipos.get(token)!=null){
							tipo=true;
							ts=token;
							continue;
						}
						if(token.equals("new")){
							j++;
							token=d.toks.get(j);
							if(token.equals("XseedNumber")){
								ArrayList<Integer>dts=new ArrayList<Integer>();
								for(int x=j+1;x<d.toks.size()&&d.cod.charAt(x)!=')';x++){
									try{
										if(cod.charAt(x)=='N'){
											dts.add(Integer.parseInt(d.toks.get(x)));
										}
									}catch(Exception e){
									}
								}
								if(dts.size()>1){
									int p=dts.get(dts.size()-1);
									int l=dts.get(dts.size()-2);
									if(p>0&&p!=6){
										ps.println("Precisi�n diferente a 6("+lista[i].getName()+":"+k+"):"+dts+line);
									}
									if(m==null){
										if(vasig!=null){
											ps.println("XseedNumber desconocido'"+vasig+"'("+lista[i].getName()+":"+k+"):"+line);
										}
									}else{
										if(m.l==0&&m.p==0){
											m.l=l;
											m.p=p;
										}else{
											if(m.l!=l||m.p!=p){
												ps.println("Se est� cambiando la precisi�n de '"+m.n+"'("+m.l+","+m.p+") por medio de creaci�n("+lista[i].getName()+":"+k+"):"+dts+line);
												m.l=l;
												m.p=p;
											}
										}
									}
								}
							}
							continue;
						}
						if(tipo&&cod.charAt(j)=='P'){
							Ambito.add(new Var(token,ts));
						}
						if(tipo){
							tipo=false;
						}
						if(mods.get(token)!=null){
							mod=true;
							if(m!=null){
								ArrayList<String>rs=d.b("N,N,\"");
								if(rs.size()>2){
									int l=Integer.parseInt(rs.get(0));
									int p=Integer.parseInt(rs.get(2));
									if(m.l==0&&m.p==0){
										m.l=l;
										m.p=p;
									}else{
										if(m.l!=l||m.p!=p){
											ps.println("Se trata de cambiar la precisi�n "+m.n+"->"+line);
										}
									}
								}
							}
						}
						if(cod.charAt(j)=='P'){
							atok=token;
						}else{
							atok=null;
						}
					}
					c2=d.c2;
					line=br.readLine();
				}
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}