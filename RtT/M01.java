import java.util.*;
import java.io.*;
public class M01{
	public static char sim(int x){
			if(x==-1){
				return'.';
			}
			if(x==-2){
				return'|';
			}
			if(x>=1&&x<=10){
				return((x-1)+"").charAt(0);
			}
			if(x>10){
				return(char)('A'+x-11);
			}
		return' ';
	}
	public static void px(PrintStream ps,int[][]d,int p,int m,int a){
		m=(m/p)*p;
		int k=(d[0].length-a)/m;
		for(int j=0;j<=k;j++){
			for(int i=0;i<d.length;i++){
				ps.println(to(d[i],j*m+a,j*m+m+a));
			}
			ps.println(to(d[0],j*m+a,j*m+m+a));
		}
	}
	public static void px2(PrintStream ps,int[][]d,int p,int m,int a){
		m=(m/p)*p;
		int k=(d[0].length-a)/m;
		for(int j=0;j<=k;j++){
			for(int i=0;i<d.length;i++){
				ps.println(to(d[i],j*m+a,j*m+m+a));
			}
			ps.println("");
		}
	}
	public static void rx(int[][]d,ArrayList<Integer>cs,ArrayList<Integer>vs,int v,int p){
		for(int i=0;i<d.length;i++){
			for(int j=0;j<vs.size();j++){
				d[i][gp(cs,vs.get(j),p)]=v;
			}
		}
	}
	public static void rx(int[]d,ArrayList<Integer>cs,ArrayList<Integer>vs,int v,int p){
		for(int j=0;j<vs.size();j++){
			d[gp(cs,vs.get(j),p)]=v;
		}
	}
	public static String to(int[]d,int a,int b){
		StringBuilder r=new StringBuilder();
		for(int i=a;i<d.length&&i<b;i++){
			r.append(sim(d[i]));
		}
		return r+"";
	}
	public static String to(int[]d){
		StringBuilder r=new StringBuilder();
		for(int i=0;i<d.length;i++){
			r.append(sim(d[i]));
		}
		return r+"";
	}
	public static int gp(ArrayList<Integer>cs,int t,int p){
		for(int i=0;i<cs.size();i++){
			if(t<cs.get(i)){
				return(int)Math.round(p*(t-cs.get(i-1))/(1.*cs.get(i)-cs.get(i-1)))+(i-1)*p;
			}
		}
		return(cs.size()-1)*p;
	}
	public static int gt(String n,String l){
		int a=l.indexOf(n);
		l=l.substring(a+n.length()+2);
		int b=l.indexOf("\"");
		return(int)Math.round(Double.parseDouble(l.substring(0,b))*1000);
	}
	public static int gt(String l){
		return gt("time",l);
	}
	public static void main(String[]argumentos){
		try{
			c.U.ruta=".\\Log.txt";
			c.U.imp("Inicio");
			PrintStream ps=new PrintStream(new FileOutputStream(new File("Salida.txt")));
			String nombre="D:\\Java\\MidiToRocksmith\\01\\miditorocksmith\\Export\\Proyecto\\Creep\\creep_p\\arr_lead.xml";
			Scanner s=new Scanner(new File(nombre));
			int p=4;
			ArrayList<Integer>cs=new ArrayList<>();
			ArrayList<Integer>ms=new ArrayList<>();
			ArrayList<int[]>ns=new ArrayList<>();
			int cc=6;
			while(s.hasNextLine()){
				String l=s.nextLine();
				if(l.indexOf("<ebeat ")>=0){
					int tiempo=gt(l);
					cs.add(tiempo);
					if(l.indexOf("measure")>=0){
						ms.add(tiempo);
						/*if(cs.size()>=2){
							ms.add(cs.get(cs.size()-3));
						}*/
					}
					continue;
				}
				if(l.indexOf("<note ")>=0){
					int tiempo=gt(l);	
					ns.add(new int[]{tiempo,gt("string",l)/1000,gt("fret",l)/1000});
				}
			}
			/*for(int i=2;i<cs.size();i+=4){
				ms.add(cs.get(i));
			}*/
			/*for(int i=0;i<ns.size();i++){
				if(ns.get(i)[2]>=10&&ns.get(i)[1]<cc-2){
					ns.get(i)[1]=ns.get(i)[1]+2;
					ns.get(i)[2]=ns.get(i)[2]-10;
					continue;
				}
				if(ns.get(i)[2]>=5&&ns.get(i)[1]<cc-1){
					ns.get(i)[1]=ns.get(i)[1]+1;
					ns.get(i)[2]=ns.get(i)[2]-5;
				}
			}*/
			/*int[][]tab=new int[cc+1][cs.size()*p];
			rx(tab[0],cs,cs,-1,p);
			rx(tab[0],cs,ms,-2,p);
			for(int i=0;i<ns.size();i++){
				tab[cc-ns.get(i)[1]][gp(cs,ns.get(i)[0],p)]=ns.get(i)[2]+1;
			}*/
			int[][]tab=new int[cc][cs.size()*p];
			/*for(int i=0;i<tab.length;i++){
				for(int j=2;j<tab[i].length;j+=4){
					tab[i][j]=-1;
				}
			}
			for(int i=0;i<tab.length;i++){
				for(int j=2;j<tab[i].length;j+=16){
					tab[i][j]=-2;
				}
			}*/
			rx(tab,cs,cs,-1,p);
			rx(tab,cs,ms,-2,p);
			for(int i=0;i<ns.size();i++){
				tab[cc-ns.get(i)[1]-1][gp(cs,ns.get(i)[0],p)]=ns.get(i)[2]+1;
			}
			px2(ps,tab,16,180,0);
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}