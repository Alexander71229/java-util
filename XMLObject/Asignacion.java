import java.lang.reflect.*;
import utilidades.*;
import java.util.*;
public class Asignacion{
	public String metodo;
	public Object objeto;
	public static HashMap<Class,Class>primitivos;
	public Asignacion(String metodo){
		this.metodo=metodo;
	}
	public Asignacion(String metodo,Object objeto){
		this.metodo=metodo;
		this.objeto=objeto;
	}
	public void ejecutar(Object o)throws Exception{
		if(Asignacion.primitivos==null){
			Asignacion.primitivos=new HashMap<Class,Class>();
			Asignacion.primitivos.put(Integer.class,int.class);
			Asignacion.primitivos.put(Double.class,double.class);
			Asignacion.primitivos.put(Character.class,char.class);
			Asignacion.primitivos.put(Boolean.class,boolean.class);
			Asignacion.primitivos.put(Byte.class,byte.class);
			Asignacion.primitivos.put(Long.class,long.class);
			Asignacion.primitivos.put(Float.class,float.class);
			Asignacion.primitivos.put(Short.class,short.class);
		}
		Class c=o.getClass();
		Method m=null;
		Class d=this.objeto.getClass();
		Object ro=this.objeto;
		if(d==ArrayList.class){
			ArrayList lista=(ArrayList)this.objeto;
			d=Util.obtenerObjeto(o.getClass(),"get"+Util.cap(metodo));
			ro=Array.newInstance(d.getComponentType(),lista.size());
			for(int i=0;i<lista.size();i++){
				Array.set(ro,i,lista.get(i));
			}
		}
		try{
			m=c.getMethod("set"+Util.cap(metodo),d);
		}catch(Throwable t){
			if(Asignacion.primitivos.get(d)!=null){
				m=c.getMethod("set"+Util.cap(metodo),Asignacion.primitivos.get(d));
			}else{
				m=c.getMethod("set"+Util.cap(metodo),d.getSuperclass());
			}
		}
		try{
			if(m==null){
				System.out.println("Error en m�todo");
			}
			m.invoke(o,ro);
		}catch(Throwable t){
			System.out.println("o:"+o);
			System.out.println("ro:"+ro);
			t.printStackTrace();
		}
	}
	public String toString(){
		return metodo+":"+objeto;
	}
}