import java.util.*;
import java.text.*;
public class ConversorDate implements Conversor{
	public Object convertir(String o)throws Exception{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return sdf.parse(o);
	}
}