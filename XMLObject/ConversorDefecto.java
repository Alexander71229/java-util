import java.util.*;
import java.text.*;
import java.lang.reflect.*;
public class ConversorDefecto{
	public <T>T convertir(Class<T>c,String s)throws Exception{
		Constructor<T>f=c.getConstructor(String.class);
		T o=f.newInstance(s);
		return o;
	}
}