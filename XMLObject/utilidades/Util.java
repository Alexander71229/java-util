package utilidades;
import java.lang.reflect.*;
public class Util{
	public static String cap(String s){
		return s.substring(0,1).toUpperCase()+s.substring(1);
	}
	public static Class obtenerObjeto(Class c,String nombre)throws Exception{
		Method m=c.getMethod(nombre);
		return m.getReturnType();
	}
	public static String r(Object o)throws Exception{
		StringBuffer sb=new StringBuffer(o.getClass().getSimpleName());
		sb.append("{");
		Method[]ms=o.getClass().getDeclaredMethods();
		String s="";
		for(int i=0;i<ms.length;i++){
			Class[]ps=ms[i].getParameterTypes();
			if(ps.length==0&&!ms[i].getName().equals("toString")){
				sb.append(s);
				//System.out.println("ms[i].invoke(o):"+ms[i].invoke(o));
				//System.exit(0);
				sb.append(ms[i].getName().substring(3).toLowerCase()+":"+rx(ms[i].invoke(o)));
				s=",";
			}
		}
		sb.append("}");
		return sb+"";
	}
	public static String rx(Object o){
		if(o.getClass().isArray()){
			StringBuilder sb=new StringBuilder("[");
			for(int i=0;i<Array.getLength(o);i++){
				if(i>0){
					sb.append(",");
				}
				sb.append(Array.get(o,i)+"");
			}
			sb.append("]");
			return sb.toString();
		}
		return o+"";
	}
	public static String obtenerCampo(String s){
		String[]x=s.split("-");
		StringBuilder sb=new StringBuilder(x[0]);
		for(int i=1;i<x.length;i++){
			sb.append(cap(x[i]));
		}
		return sb+"";
	}
}