import java.lang.reflect.*;
import java.util.*;
import java.io.*;
import utilidades.*;
public class XMLObjeto{
	public static Object cargar(Class o,String xml)throws Exception{
		HashMap<String,Conversor>conversores=new HashMap<String,Conversor>();
		conversores.put("java.lang.String",new ConversorString());
		conversores.put("java.math.BigDecimal",new ConversorBigDecimal());
		conversores.put("java.lang.Integer",new ConversorInteger());
		conversores.put("int",new ConversorInteger());
		conversores.put("java.lang.Double",new ConversorDouble());
		conversores.put("double",new ConversorDouble());
		conversores.put("java.util.Date",new ConversorDate());
		conversores.put("java.util.Calendar",new ConversorCalendar());
		ConversorDefecto conversorDefecto=new ConversorDefecto();
		//xml+="</";
		char[]x=xml.toCharArray();
		Stack<Clase>p=new Stack<Clase>();
		p.push(new Clase(o));
		int estado=0;
		StringBuilder e=new StringBuilder();
		for(int i=0;i<x.length;i++){
			char c=x[i];
			if(x[i]=='<'&&x[i+1]=='/'){
				i++;
				Clase cx=p.pop();
				if(estado==2){
					Conversor conversor=conversores.get(cx.clase.getName());
					if(conversor==null){
						System.out.println("No se encontr� convertor de java.lang.String a "+cx.clase.getName());
						p.peek().objeto(conversorDefecto.convertir(cx.clase,e+""));
					}else{
						p.peek().objeto(conversor.convertir(e+""));
					}
				}
				if(estado==4){
					p.peek().objeto(cx.crear());
				}
				//System.out.println(p);
				estado=3;
				continue;
			}
			if(x[i]=='/'&&x[i+1]=='>'){
				i++;
				continue;
			}
			if(x[i]=='<'){
				e=new StringBuilder();
				estado=1;
				continue;
			}
			if(x[i]=='>'){
				if(estado==3){
					estado=4;
					continue;
				}
				if(estado==1){
					String campo=Util.obtenerCampo(e+"");
					Class nc=Util.obtenerObjeto(p.peek().clase,"get"+Util.cap(campo));
					if(nc.isArray()){
						if(p.peek().asignaciones.size()==0||!p.peek().m().equals(campo)){
							p.peek().add(campo,new ArrayList());
						}
						p.push(new Clase(nc.getComponentType()));
					}else{
						p.peek().add(campo);
						p.push(new Clase(nc));
					}
					//System.out.println(p);
					estado=2;
				}
				e=new StringBuilder();
				continue;
			}
			e.append(c);
		}
		Object ro=null;
		while(!p.isEmpty()){
			ro=p.pop().crear();
		}
		return ro;
	}
	public static String cargarXML(String ruta)throws Exception{
		return cargarXML(new File(ruta));
	}
	public static String cargarXML(File f)throws Exception{
		Scanner s=new Scanner(f);
		StringBuilder sb=new StringBuilder();
		while(s.hasNext()){
			sb.append(s.next());
		}
		return sb+"";
	}
}