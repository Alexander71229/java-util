import java.util.*;
import java.text.*;
public class ConversorCalendar implements Conversor{
	public Object convertir(String o)throws Exception{
		Calendar c=Calendar.getInstance();
		Date d=null;
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
		try{
			d=sdf1.parse(o);
		}catch(Throwable t){
		}
		if(d==null){
			try{
				d=sdf2.parse(o);
			}catch(Throwable t){
			}
		}
		c.setTime(d);
		return c;
	}
}