package oracle.e1.bssv.JP570000;
import java.math.BigDecimal;
public class ConversorBigDecimal implements Conversor{
	public Object convertir(String o)throws Exception{
		if(o==null){
			return null;
		}
		if(o.trim().length()==0){
			return new BigDecimal(0);
		}
		return new BigDecimal(o);
	}
}