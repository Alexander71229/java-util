package ejemplo;
import utilidades.*;
public class C{
	private String c1;
	private String c2;
	public void setC1(String c1){
		this.c1=c1;
	}
	public String getC1(){
		return this.c1;
	}
	public void setC2(String c2){
		this.c2=c2;
	}
	public String getC2(){
		return this.c2;
	}
	public String toString(){
		//return "C{c1:"+this.c1+",c2:"+this.c2+"}";
		try{
			return Util.r(this);
		}catch(Throwable t){
			return "C{c1:"+this.c1+",c2:"+this.c2+"}";
		}
	}
}