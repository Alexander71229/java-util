package ejemplo;
import utilidades.*;
public class B{
	private String c1;
	private String c2;
	private C c;
	public void setC1(String c1){
		this.c1=c1;
	}
	public String getC1(){
		return this.c1;
	}
	public void setC2(String c2){
		this.c2=c2;
	}
	public String getC2(){
		return this.c2;
	}
	public void setC(C c){
		this.c=c;
	}
	public C getC(){
		return this.c;
	}
	public String toString(){
		try{
			return Util.r(this);
		}catch(Throwable t){
			return "B";
		}
	}
}