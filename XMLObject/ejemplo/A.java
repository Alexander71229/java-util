package ejemplo;
import java.util.*;
public class A{
	private String c1;
	private String c2;
	private B[] bs;
	private int entero;
	private double numero;
	private Date fecha;
	private Calendar calendario;
	private String szNombreComplejo;
	public void setC1(String c1){
		this.c1=c1;
	}
	public String getC1(){
		return this.c1;
	}
	public void setC2(String c2){
		this.c2=c2;
	}
	public String getC2(){
		return this.c2;
	}
	public void setBs(B[] bs){
		this.bs=bs;
	}
	public B[] getBs(){
		return this.bs;
	}
	public void setEntero(int entero){
		this.entero=entero;
	}
	public int getEntero(){
		return this.entero;
	}
	public void setNumero(double numero){
		this.numero=numero;
	}
	public double getNumero(){
		return this.numero;
	}
	public void setFecha(Date fecha){
		this.fecha=fecha;
	}
	public Date getFecha(){
		return this.fecha;
	}
	public void setCalendario(Calendar calendario){
		this.calendario=calendario;
	}
	public Calendar getCalendario(){
		return this.calendario;
	}
	public void setSzNombreComplejo(String szNombreComplejo){
		this.szNombreComplejo=szNombreComplejo;
	}
	public String getSzNombreComplejo(){
		return this.szNombreComplejo;
	}
}