import java.util.*;
public class Clase{
	public Class clase;
	public ArrayList<Asignacion>asignaciones;
	public Clase(Class clase){
		this.clase=clase;
		this.asignaciones=new ArrayList<Asignacion>();
	}
	public void add(Asignacion asignacion){
		this.asignaciones.add(asignacion);
	}
	public void add(String metodo){
		this.asignaciones.add(new Asignacion(metodo));
	}
	public void add(String metodo,Object o){
		this.asignaciones.add(new Asignacion(metodo,o));
	}
	public void objeto(Object o){
		if(asignaciones.get(asignaciones.size()-1).objeto==null){
			asignaciones.get(asignaciones.size()-1).objeto=o;
		}else{
			((ArrayList)asignaciones.get(asignaciones.size()-1).objeto).add(o);
		}
	}
	public String toString(){
		return this.clase.getName()+":"+asignaciones;
	}
	public Object crear()throws Exception{
		Object o=this.clase.newInstance();
		for(int i=0;i<this.asignaciones.size();i++){
			this.asignaciones.get(i).ejecutar(o);
		}
		return o;
	}
	public String m(){
		return asignaciones.get(asignaciones.size()-1).metodo;
	}
}