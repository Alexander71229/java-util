package dbutil.driver;
import java.sql.Connection;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.sql.SQLXML;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.Savepoint;
import java.sql.Array;
import java.sql.SQLException;
import java.sql.SQLClientInfoException;
import java.util.Map;
import java.sql.Statement;
import java.sql.CallableStatement;
import java.sql.Struct;
import java.sql.NClob;
import java.sql.SQLWarning;
import java.sql.Blob;
import java.sql.Clob;
public class DepuracionConnection implements Connection{
	private Connection c;
	public DepuracionConnection(){
	}
	public DepuracionConnection(Connection c){
		this.c=c;
	}
	public void setC(Connection c){
		this.c=c;
	}
	public Connection getC(){
		return this.c;
	}
	public void setReadOnly(boolean p0)throws SQLException{
		this.c.setReadOnly(p0);
	}
	public void close()throws SQLException{
		this.c.close();
	}
	public boolean isReadOnly()throws SQLException{
		return this.c.isReadOnly();
	}
	public void abort(Executor p0)throws SQLException{
		this.c.abort(p0);
	}
	public void clearWarnings()throws SQLException{
		this.c.clearWarnings();
	}
	public void commit()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"commit",null);
		this.c.commit();
	}
	public Array createArrayOf(String p0,Object[] p1)throws SQLException{
		return this.c.createArrayOf(p0,p1);
	}
	public Blob createBlob()throws SQLException{
		return this.c.createBlob();
	}
	public Clob createClob()throws SQLException{
		return this.c.createClob();
	}
	public NClob createNClob()throws SQLException{
		return this.c.createNClob();
	}
	public SQLXML createSQLXML()throws SQLException{
		return this.c.createSQLXML();
	}
	public Statement createStatement(int p0,int p1,int p2)throws SQLException{
		return new DepuracionStatement(this.c.createStatement(p0,p1,p2));
	}
	public Statement createStatement(int p0,int p1)throws SQLException{
		return new DepuracionStatement(this.c.createStatement(p0,p1));
	}
	public Statement createStatement()throws SQLException{
		return new DepuracionStatement(this.c.createStatement());
	}
	public Struct createStruct(String p0,Object[] p1)throws SQLException{
		return this.c.createStruct(p0,p1);
	}
	public boolean getAutoCommit()throws SQLException{
		return this.c.getAutoCommit();
	}
	public String getCatalog()throws SQLException{
		return this.c.getCatalog();
	}
	public String getClientInfo(String p0)throws SQLException{
		return this.c.getClientInfo(p0);
	}
	public Properties getClientInfo()throws SQLException{
		return this.c.getClientInfo();
	}
	public int getHoldability()throws SQLException{
		return this.c.getHoldability();
	}
	public DatabaseMetaData getMetaData()throws SQLException{
		return this.c.getMetaData();
	}
	public int getNetworkTimeout()throws SQLException{
		return this.c.getNetworkTimeout();
	}
	public String getSchema()throws SQLException{
		return this.c.getSchema();
	}
	public int getTransactionIsolation()throws SQLException{
		return this.c.getTransactionIsolation();
	}
	public Map getTypeMap()throws SQLException{
		return this.c.getTypeMap();
	}
	public SQLWarning getWarnings()throws SQLException{
		return this.c.getWarnings();
	}
	public boolean isClosed()throws SQLException{
		return this.c.isClosed();
	}
	public boolean isValid(int p0)throws SQLException{
		return this.c.isValid(p0);
	}
	public String nativeSQL(String p0)throws SQLException{
		return this.c.nativeSQL(p0);
	}
	public CallableStatement prepareCall(String p0)throws SQLException{
		return this.c.prepareCall(p0);
	}
	public CallableStatement prepareCall(String p0,int p1,int p2,int p3)throws SQLException{
		return this.c.prepareCall(p0,p1,p2,p3);
	}
	public CallableStatement prepareCall(String p0,int p1,int p2)throws SQLException{
		return this.c.prepareCall(p0,p1,p2);
	}
	public PreparedStatement prepareStatement(String p0)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0),p0);
	}
	public PreparedStatement prepareStatement(String p0,int p1,int p2)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1,p2),p0);
	}
	public PreparedStatement prepareStatement(String p0,int p1,int p2,int p3)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1,p2,p3),p0);
	}
	public PreparedStatement prepareStatement(String p0,int[] p1)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1),p0);
	}
	public PreparedStatement prepareStatement(String p0,String[] p1)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1),p0);
	}
	public PreparedStatement prepareStatement(String p0,int p1)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1),p0);
	}
	public void releaseSavepoint(Savepoint p0)throws SQLException{
		this.c.releaseSavepoint(p0);
	}
	public void rollback(Savepoint p0)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"rollback",new Object[]{p0});
		this.c.rollback(p0);
	}
	public void rollback()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"rollback",null);
		this.c.rollback();
	}
	public void setAutoCommit(boolean p0)throws SQLException{
		this.c.setAutoCommit(p0);
	}
	public void setCatalog(String p0)throws SQLException{
		this.c.setCatalog(p0);
	}
	public void setClientInfo(String p0,String p1)throws SQLClientInfoException{
		this.c.setClientInfo(p0,p1);
	}
	public void setClientInfo(Properties p0)throws SQLClientInfoException{
		this.c.setClientInfo(p0);
	}
	public void setHoldability(int p0)throws SQLException{
		this.c.setHoldability(p0);
	}
	public void setNetworkTimeout(Executor p0,int p1)throws SQLException{
		this.c.setNetworkTimeout(p0,p1);
	}
	public Savepoint setSavepoint(String p0)throws SQLException{
		return this.c.setSavepoint(p0);
	}
	public Savepoint setSavepoint()throws SQLException{
		return this.c.setSavepoint();
	}
	public void setSchema(String p0)throws SQLException{
		this.c.setSchema(p0);
	}
	public void setTransactionIsolation(int p0)throws SQLException{
		this.c.setTransactionIsolation(p0);
	}
	public void setTypeMap(Map p0)throws SQLException{
		this.c.setTypeMap(p0);
	}
	public Object unwrap(Class p0)throws SQLException{
		return this.c.unwrap(p0);
	}
	public boolean isWrapperFor(Class p0)throws SQLException{
		return this.c.isWrapperFor(p0);
	}
}
