package dbutil.driver;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.Properties;
import java.sql.Driver;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.DriverPropertyInfo;
import java.util.logging.Logger;
import java.util.Enumeration;
import java.sql.DriverManager;
public class DepuracionDriver implements Driver{
	private Driver od;
	public DepuracionDriver(){
	}
	public DepuracionDriver(Driver od){
		this.od=od;
	}
	public void setOd(Driver od){
		this.od=od;
	}
	public Driver getOd(){
		return this.od;
	}
	public boolean acceptsURL(String p0)throws SQLException{
		return this.od.acceptsURL(p0);
	}
	public int getMajorVersion(){
		return this.od.getMajorVersion();
	}
	public int getMinorVersion(){
		return this.od.getMinorVersion();
	}
	public Logger getParentLogger()throws SQLFeatureNotSupportedException{
		return this.od.getParentLogger();
	}
	public DriverPropertyInfo[] getPropertyInfo(String p0,Properties p1)throws SQLException{
		return this.od.getPropertyInfo(p0,p1);
	}
	public boolean jdbcCompliant(){
		return this.od.jdbcCompliant();
	}
	public Connection connect(String url,final Properties properties)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"conectar",new Object[]{url,properties});
		Connection x=this.od.connect(url,properties);
		return new DepuracionConnection(x);
	}
	public static void otros()throws Exception{
		Enumeration e=DriverManager.getDrivers();
		while(e.hasMoreElements()){
			Driver driver=(Driver)e.nextElement();
			DriverManager.deregisterDriver(driver);
		}
	}
	public static void registrarse(Driver d)throws Exception{
		//otros();
		DriverManager.registerDriver(new DepuracionDriver(d));
		Enumeration e=DriverManager.getDrivers();
		while(e.hasMoreElements()){
			Driver driver=(Driver)e.nextElement();
			c.U.imp("driver:"+driver);
		}
	}
}