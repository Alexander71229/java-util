package dbutil.driver;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLWarning;
public class DepuracionStatement implements Statement{
	private Statement os;
	public DepuracionStatement(){
	}
	public void setOs(Statement os){
		this.os=os;
	}
	public Statement getOs(){
		return this.os;
	}
	public void addBatch(String p0)throws SQLException{
		this.os.addBatch(p0);
	}
	public void cancel()throws SQLException{
		this.os.cancel();
	}
	public void clearBatch()throws SQLException{
		this.os.clearBatch();
	}
	public void clearWarnings()throws SQLException{
		this.os.clearWarnings();
	}
	public void closeOnCompletion()throws SQLException{
		this.os.closeOnCompletion();
	}
	public Connection getConnection()throws SQLException{
		return this.os.getConnection();
	}
	public int getFetchDirection()throws SQLException{
		return this.os.getFetchDirection();
	}
	public int getFetchSize()throws SQLException{
		return this.os.getFetchSize();
	}
	public ResultSet getGeneratedKeys()throws SQLException{
		return this.os.getGeneratedKeys();
	}
	public int getMaxFieldSize()throws SQLException{
		return this.os.getMaxFieldSize();
	}
	public int getMaxRows()throws SQLException{
		return this.os.getMaxRows();
	}
	public boolean getMoreResults(int p0)throws SQLException{
		return this.os.getMoreResults(p0);
	}
	public boolean getMoreResults()throws SQLException{
		return this.os.getMoreResults();
	}
	public int getQueryTimeout()throws SQLException{
		return this.os.getQueryTimeout();
	}
	public ResultSet getResultSet()throws SQLException{
		return this.os.getResultSet();
	}
	public int getResultSetConcurrency()throws SQLException{
		return this.os.getResultSetConcurrency();
	}
	public int getResultSetHoldability()throws SQLException{
		return this.os.getResultSetHoldability();
	}
	public int getResultSetType()throws SQLException{
		return this.os.getResultSetType();
	}
	public int getUpdateCount()throws SQLException{
		return this.os.getUpdateCount();
	}
	public SQLWarning getWarnings()throws SQLException{
		return this.os.getWarnings();
	}
	public boolean isCloseOnCompletion()throws SQLException{
		return this.os.isCloseOnCompletion();
	}
	public boolean isClosed()throws SQLException{
		return this.os.isClosed();
	}
	public boolean isPoolable()throws SQLException{
		return this.os.isPoolable();
	}
	public void setCursorName(String p0)throws SQLException{
		this.os.setCursorName(p0);
	}
	public void setEscapeProcessing(boolean p0)throws SQLException{
		this.os.setEscapeProcessing(p0);
	}
	public void setFetchDirection(int p0)throws SQLException{
		this.os.setFetchDirection(p0);
	}
	public void setFetchSize(int p0)throws SQLException{
		this.os.setFetchSize(p0);
	}
	public void setMaxFieldSize(int p0)throws SQLException{
		this.os.setMaxFieldSize(p0);
	}
	public void setMaxRows(int p0)throws SQLException{
		this.os.setMaxRows(p0);
	}
	public void setPoolable(boolean p0)throws SQLException{
		this.os.setPoolable(p0);
	}
	public void setQueryTimeout(int p0)throws SQLException{
		this.os.setQueryTimeout(p0);
	}
	public Object unwrap(Class p0)throws SQLException{
		return this.os.unwrap(p0);
	}
	public boolean isWrapperFor(Class p0)throws SQLException{
		return this.os.isWrapperFor(p0);
	}
	public DepuracionStatement(Statement os){
		actor.ControladorEventos.notificar(this.getClass(),"creandoStatement",new Object[]{this});
		this.os=os;
	}
	public void close()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"close",new Object[]{this});
		this.os.close();
	}
	public int[]executeBatch()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeBatch",new Object[]{this});
		return this.os.executeBatch();
	}
	public int executeUpdate(String p0)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this,p0});
		return this.os.executeUpdate(p0);
	}
	public boolean execute(String p0)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this,p0});
		return this.os.execute(p0);
	}
	public int executeUpdate(String p0,int p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this,p0,new Integer(p1)});
		return this.os.executeUpdate(p0,p1);
	}
	public boolean execute(String p0,int p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this,p0,new Integer(p1)});
		return this.os.execute(p0,p1);
	}
	public int executeUpdate(String p0,int[] p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this,p0,p1});
		return this.os.executeUpdate(p0,p1);
	}
	public boolean execute(String p0,int[] p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this,p0,p1});
		return this.os.execute(p0,p1);
	}
	public int executeUpdate(String p0,String[] p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this,p0,p1});
		return this.os.executeUpdate(p0,p1);
	}
	public boolean execute(String p0,String[] p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this,p0,p1});
		return this.os.execute(p0,p1);
	}
	public ResultSet executeQuery(String p0)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeQuery",new Object[]{this,p0});
		return this.os.executeQuery(p0);
	}
}