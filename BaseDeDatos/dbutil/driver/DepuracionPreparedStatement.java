package dbutil.driver;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.net.URL;
import java.math.BigDecimal;
import java.io.Reader;
import java.io.InputStream;
public class DepuracionPreparedStatement extends DepuracionStatement implements PreparedStatement{
	public PreparedStatement ops;
	public String sql;
	DepuracionPreparedStatement(PreparedStatement ops){
		super((Statement)ops);
		this.ops=ops;
	}
	DepuracionPreparedStatement(PreparedStatement ops,String sql){
		super((Statement)ops);
		this.ops=ops;
		this.sql=sql;
	}
	public int executeUpdate()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this});
		return this.ops.executeUpdate();
	}
	public void addBatch()throws SQLException{
		this.ops.addBatch();
	}
	public void clearParameters()throws SQLException{
		this.ops.clearParameters();
	}
	public boolean execute()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this});
		return this.ops.execute();
	}
	public void setByte(final int p0,final byte p1)throws SQLException{
		this.ops.setByte(p0,p1);
	}
	public void setDouble(final int p0,final double p1)throws SQLException{
		this.ops.setDouble(p0,p1);
	}
	public void setFloat(final int p0,final float p1)throws SQLException{
		this.ops.setFloat(p0,p1);
	}
	public void setInt(final int p0,final int p1)throws SQLException{
		this.ops.setInt(p0,p1);
	}
	public void setNull(final int p0,final int p1)throws SQLException{
		this.ops.setNull(p0,p1);
	}
	public void setLong(final int p0,final long p1)throws SQLException{
		this.ops.setLong(p0,p1);
	}
	public void setShort(final int p0,final short p1)throws SQLException{
		this.ops.setShort(p0,p1);
	}
	public void setBoolean(final int p0,final boolean p1)throws SQLException{
		this.ops.setBoolean(p0,p1);
	}
	public void setBytes(final int p0,final byte[] p1)throws SQLException{
		this.ops.setBytes(p0,p1);
	}
	public void setAsciiStream(final int p0,final InputStream p1,final int p2)throws SQLException{
		this.ops.setAsciiStream(p0,p1,p2);
	}
	public void setBinaryStream(final int p0,final InputStream p1,final int p2)throws SQLException{
		this.ops.setBinaryStream(p0,p1,p2);
	}
	public void setUnicodeStream(final int p0,final InputStream p1,final int p2)throws SQLException{
		this.ops.setUnicodeStream(p0,p1,p2);
	}
	public void setCharacterStream(final int p0,final Reader p1,final int p2)throws SQLException{
		this.ops.setCharacterStream(p0,p1,p2);
	}
	public void setObject(final int p0,final Object p1)throws SQLException{
		this.ops.setObject(p0,p1);
	}
	public void setObject(final int p0,final Object p1,final int p2)throws SQLException{
		this.ops.setObject(p0,p1,p2);
	}
	public void setObject(final int p0,final Object p1,final int p2,final int p3)throws SQLException{
		this.ops.setObject(p0,p1,p2,p3);
	}
	public void setNull(final int p0,final int p1,final String p2)throws SQLException{
		this.ops.setNull(p0,p1,p2);
	}
	public void setString(final int p0,final String p1)throws SQLException{
		this.ops.setString(p0,p1);
	}
	public void setBigDecimal(final int p0,final BigDecimal p1)throws SQLException{
		this.ops.setBigDecimal(p0,p1);
	}
	public void setURL(final int p0,final URL p1)throws SQLException{
		this.ops.setURL(p0,p1);
	}
	public void setArray(final int p0,final Array p1)throws SQLException{
		this.ops.setArray(p0,p1);
	}
	public void setBlob(final int p0,final Blob p1)throws SQLException{
		this.ops.setBlob(p0,p1);
	}
	public void setClob(final int p0,final Clob p1)throws SQLException{
		this.ops.setClob(p0,p1);
	}
	public void setDate(final int p0,final Date p1)throws SQLException{
		this.ops.setDate(p0,p1);
	}
	public ParameterMetaData getParameterMetaData()throws SQLException{
		return null;
	}
	public void setRef(final int p0,final Ref p1)throws SQLException{
		this.ops.setRef(p0,p1);
	}
	public ResultSet executeQuery()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeQuery",new Object[]{this});
		return this.ops.executeQuery();
	}
	public ResultSetMetaData getMetaData()throws SQLException{
		return this.ops.getMetaData();
	}
	public void setTime(final int p0,final Time p1)throws SQLException{
		this.ops.setTime(p0,p1);
	}
	public void setTimestamp(final int p0,final Timestamp p1)throws SQLException{
		this.ops.setTimestamp(p0,p1);
	}
	public void setDate(final int p0,final Date p1,final Calendar p2)throws SQLException{
		this.ops.setDate(p0,p1,p2);
	}
	public void setTime(final int p0,final Time p1,final Calendar p2)throws SQLException{
		this.ops.setTime(p0,p1,p2);
	}
	public void setTimestamp(final int p0,final Timestamp p1,final Calendar p2)throws SQLException{
		this.ops.setTimestamp(p0,p1,p2);
	}
}