package dbutil.driver;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.Enumeration;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
public class DepuracionDriver implements Driver{
	public Driver od;
	public DepuracionDriver(Driver od){
		this.od=od;
	}
	public int getMajorVersion(){
		return this.od.getMajorVersion();
	}
	public int getMinorVersion(){
		return this.od.getMinorVersion();
	}
	public boolean jdbcCompliant(){
		return this.od.jdbcCompliant();
	}
	public boolean acceptsURL(final String p0)throws SQLException{
		return this.od.acceptsURL(p0);
	}
	public Connection connect(String url,final Properties properties)throws SQLException{manejoLogs.General.traza();
		actor.ControladorEventos.notificar(this.getClass(),"conectar",new Object[]{url,properties});
		Connection x=this.od.connect(url,properties);
		return new DepuracionConnection(x);
	}
	public DriverPropertyInfo[]getPropertyInfo(final String p0,final Properties p1)throws SQLException{
		return this.od.getPropertyInfo(p0,p1);
	}
	public static void otros()throws Exception{
		Enumeration e=DriverManager.getDrivers();
		while(e.hasMoreElements()){
			Driver driver=(Driver)e.nextElement();
			DriverManager.deregisterDriver(driver);
		}
	}
	public static void registrarse(Driver d)throws Exception{
		otros();
		DriverManager.registerDriver(new DepuracionDriver(d));
		Enumeration e=DriverManager.getDrivers();
		while(e.hasMoreElements()){
			Driver driver=(Driver)e.nextElement();
			manejoLogs.General.imprimir("driver:"+driver);
		}
	}
}