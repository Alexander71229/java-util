package dbutil.driver;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLWarning;
public class DepuracionStatement implements Statement{
	public Statement os;
	public DepuracionStatement(Statement os){
		actor.ControladorEventos.notificar(this.getClass(),"creandoStatement",new Object[]{this});
		this.os=os;
	}
	public int getFetchDirection()throws SQLException{
		return this.os.getFetchDirection();
	}
	public int getFetchSize()throws SQLException{
		return this.os.getFetchSize();
	}
	public int getMaxFieldSize()throws SQLException{
		return this.os.getMaxFieldSize();
	}
	public int getMaxRows()throws SQLException{
		return this.os.getMaxRows();
	}
	public int getQueryTimeout()throws SQLException{
		return this.os.getQueryTimeout();
	}
	public int getResultSetConcurrency()throws SQLException{
		return this.os.getResultSetConcurrency();
	}
	public int getResultSetHoldability()throws SQLException{
		return this.os.getResultSetHoldability();
	}
	public int getResultSetType()throws SQLException{
		return this.os.getResultSetType();
	}
	public int getUpdateCount()throws SQLException{
		return this.os.getUpdateCount();
	}
	public void cancel()throws SQLException{
		this.os.cancel();
	}
	public void clearBatch()throws SQLException{
		this.os.clearBatch();
	}
	public void clearWarnings()throws SQLException{
		this.os.clearWarnings();
	}
	public void close()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"close",new Object[]{this});
		this.os.close();
	}
	public boolean getMoreResults()throws SQLException{
		return this.os.getMoreResults();
	}
	public int[]executeBatch()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeBatch",new Object[]{this});
		return this.os.executeBatch();
	}
	public void setFetchDirection(final int p0)throws SQLException{
		this.os.setFetchDirection(p0);
	}
	public void setFetchSize(final int p0)throws SQLException{
		this.os.setFetchSize(p0);
	}
	public void setMaxFieldSize(final int p0)throws SQLException{
		this.os.setMaxFieldSize(p0);
	}
	public void setMaxRows(final int p0)throws SQLException{
		this.os.setMaxRows(p0);
	}
	public void setQueryTimeout(final int p0)throws SQLException{
		this.os.setQueryTimeout(p0);
	}
	public boolean getMoreResults(final int p0)throws SQLException{
		return this.os.getMoreResults(p0);
	}
	public void setEscapeProcessing(final boolean p0)throws SQLException{
		this.os.setEscapeProcessing(p0);
	}
	public int executeUpdate(final String p0)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this,p0});
		return this.os.executeUpdate(p0);
	}
	public void addBatch(final String p0)throws SQLException{
		this.os.addBatch(p0);
	}
	public void setCursorName(final String p0)throws SQLException{
		this.os.setCursorName(p0);
	}
	public boolean execute(final String p0)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this,p0});
		return this.os.execute(p0);
	}
	public int executeUpdate(final String p0,final int p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this,p0,new Integer(p1)});
		return this.os.executeUpdate(p0,p1);
	}
	public boolean execute(final String p0,final int p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this,p0,new Integer(p1)});
		return this.os.execute(p0,p1);
	}
	public int executeUpdate(final String p0,final int[] p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this,p0,p1});
		return this.os.executeUpdate(p0,p1);
	}
	public boolean execute(final String p0,final int[] p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this,p0,p1});
		return this.os.execute(p0,p1);
	}
	public Connection getConnection()throws SQLException{
		return this.os.getConnection();
	}
	public ResultSet getGeneratedKeys()throws SQLException{
		return this.os.getGeneratedKeys();
	}
	public ResultSet getResultSet()throws SQLException{
		return this.os.getResultSet();
	}
	public SQLWarning getWarnings()throws SQLException{
		return this.os.getWarnings();
	}
	public int executeUpdate(final String p0,final String[] p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this,p0,p1});
		return this.os.executeUpdate(p0,p1);
	}
	public boolean execute(final String p0,final String[] p1)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this,p0,p1});
		return this.os.execute(p0,p1);
	}
	public ResultSet executeQuery(final String p0)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeQuery",new Object[]{this,p0});
		return this.os.executeQuery(p0);
	}
}