package dbutil.driver;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DatabaseMetaData;
import java.sql.SQLWarning;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.util.Map;
public class DepuracionConnection implements Connection{
	public Connection c;
	public DepuracionConnection(Connection c){
		this.c=c;
	}
	public int getHoldability()throws SQLException{
		return this.c.getHoldability();
	}
	public int getTransactionIsolation()throws SQLException{
		return this.c.getTransactionIsolation();
	}
	public void clearWarnings()throws SQLException{
		this.c.clearWarnings();
	}
	public void close()throws SQLException{
		this.c.close();
	}
	public void commit()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"commit",null);
		this.c.commit();
	}
	public void rollback()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"rollback",null);
		this.c.rollback();
	}
	public boolean getAutoCommit()throws SQLException{
		return this.c.getAutoCommit();
	}
	public boolean isClosed()throws SQLException{
		return this.c.isClosed();
	}
	public boolean isReadOnly()throws SQLException{
		return this.c.isReadOnly();
	}
	public void setHoldability(final int p0)throws SQLException{
		this.c.setHoldability(p0);
	}
	public void setTransactionIsolation(final int p0)throws SQLException{
		this.c.setTransactionIsolation(p0);
	}
	public void setAutoCommit(final boolean p0)throws SQLException{
		this.c.setAutoCommit(p0);
	}
	public void setReadOnly(final boolean p0)throws SQLException{
		this.c.setReadOnly(p0);
	}
	public String getCatalog()throws SQLException{
		return this.c.getCatalog();
	}
	public void setCatalog(final String p0)throws SQLException{
		this.c.setCatalog(p0);
	}
	public DatabaseMetaData getMetaData()throws SQLException{
		return this.c.getMetaData();
	}
	public SQLWarning getWarnings()throws SQLException{
		return this.c.getWarnings();
	}
	public Savepoint setSavepoint()throws SQLException{
		return this.c.setSavepoint();
	}
	public void releaseSavepoint(final Savepoint p0)throws SQLException{
		this.c.releaseSavepoint(p0);
	}
	public void rollback(final Savepoint p0)throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"rollback",new Object[]{p0});
		this.c.rollback(p0);
	}
	public Statement createStatement()throws SQLException{
		return new DepuracionStatement(this.c.createStatement());
	}
	public Statement createStatement(final int p0,final int p1)throws SQLException{
		return new DepuracionStatement(this.c.createStatement(p0,p1));
	}
	public Statement createStatement(final int p0,final int p1,final int p2)throws SQLException{
		return new DepuracionStatement(this.c.createStatement(p0,p1,p2));
	}
	public Map getTypeMap()throws SQLException{
		return this.c.getTypeMap();
	}
	public void setTypeMap(final Map p0)throws SQLException{
		this.c.setTypeMap(p0);
	}
	public String nativeSQL(final String p0)throws SQLException{
		return this.c.nativeSQL(p0);
	}
	public CallableStatement prepareCall(final String p0)throws SQLException{
		return this.c.prepareCall(p0);
	}
	public CallableStatement prepareCall(final String p0,final int p1,final int p2)throws SQLException{
		return this.c.prepareCall(p0,p1,p2);
	}
	public CallableStatement prepareCall(final String p0,final int p1,final int p2,final int p3)throws SQLException{
		return this.c.prepareCall(p0,p1,p2,p3);
	}
	public PreparedStatement prepareStatement(final String p0)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0),p0);
	}
	public PreparedStatement prepareStatement(final String p0,final int p1)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1),p0);
	}
	public PreparedStatement prepareStatement(final String p0,final int p1,final int p2)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1,p2),p0);
	}
	public PreparedStatement prepareStatement(final String p0,final int p1,final int p2,final int p3)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1,p2,p3),p0);
	}
	public PreparedStatement prepareStatement(final String p0,final int[] p1)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1),p0);
	}
	public Savepoint setSavepoint(final String p0)throws SQLException{
		return this.c.setSavepoint(p0);
	}
	public PreparedStatement prepareStatement(final String p0,final String[] p1)throws SQLException{
		return new DepuracionPreparedStatement(this.c.prepareStatement(p0,p1),p0);
	}
}