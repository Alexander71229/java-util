// 
// Decompiled by Procyon v0.5.30
// 

package xseedRts;

import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Connection;

public abstract class XseedConnection extends XseedResource
{
    public Connection connection;
    public String identifier;
    public String url;
    public String engine;
    public String dataSource;
    public String driver;
    public String password;
    public String server;
    public String username;
    public boolean isConnected;
    public boolean convertHex;
    public String statementMode;
    
    public abstract void connect() throws Exception;
    
    public abstract void disconnect() throws Exception;
    
    public abstract void commit() throws Exception;
    
    public abstract void rollback() throws Exception;
    
    public XseedConnection(final XseedSession session) throws Exception {
        this.engine = "Access";
        this.driver = "sun.jdbc.odbc.JdbcOdbcDriver";
        this.isConnected = false;
        this.convertHex = false;
        this.statementMode = "Dynamic";
        session.database.addElement(this);
        this.session = session;
        this.dataSource = session.DBDataSource;
        this.username = session.DBUsername;
        this.password = session.DBPassword;
        this.server = session.DBServer;
        if (!session.DBEngine.equals("")) {
            this.engine = session.DBEngine;
        }
        if (!session.DBDriver.equals("")) {
            this.driver = session.DBDriver;
        }
        else {
            this.setDriver();
        }
        if (!session.DBUrl.equals("")) {
            this.url = session.DBUrl;
        }
        else {
            this.setUrl();
        }
    }
    
    public XseedConnection() throws Exception {
        this.engine = "Access";
        this.driver = "sun.jdbc.odbc.JdbcOdbcDriver";
        this.isConnected = false;
        this.convertHex = false;
        this.statementMode = "Dynamic";
    }
    
    public String getIdentifier() throws Exception {
        if (this.identifier != null) {
            return this.identifier;
        }
        return this.getClass().getName();
    }
    
    public void addElement(final XseedSession session) throws Exception {
        session.database.addElement(this);
        this.session = session;
        this.dataSource = session.DBDataSource;
        this.username = session.DBUsername;
        this.password = session.DBPassword;
        this.server = session.DBServer;
        if (!session.DBEngine.equals("")) {
            this.engine = session.DBEngine;
        }
        if (!session.DBDriver.equals("")) {
            this.driver = session.DBDriver;
        }
        else {
            this.setDriver();
        }
        if (!session.DBUrl.equals("")) {
            this.url = session.DBUrl;
        }
        else {
            this.setUrl();
        }
    }
    
    protected void setUrl() throws Exception {
        if (this.engine.equals("Oracle")) {
            this.url = "\"jdbc:oracle:thin:@" + this.dataSource;
        }
        else if (this.engine.equals("DB2")) {
            this.url = "jdbc:db2://" + this.server + "/" + this.dataSource;
        }
        else if (this.engine.equals("Sql-Server")) {
            this.url = "jdbc:microsoft:sqlserver://" + this.server + ":1433;SelectMethod=cursor;User=" + this.username + ";PassWord=" + this.password + ";DataBaseName=" + this.dataSource;
        }
        else if (this.engine.equals("Access")) {
            this.url = "jdbc:odbc:" + this.dataSource;
        }
        else {
            this.url = "jdbc:odbc:" + this.dataSource;
        }
    }
    
    protected void setDriver() throws Exception {
        if (this.engine.equals("Oracle")) {
            this.driver = "oracle.jdbc.driver.OracleDriver";
        }
        else if (this.engine.equals("DB2")) {
            this.driver = "com.ibm.db2.jcc.DB2Driver";
        }
        else if (this.engine.equals("Sql-Server")) {
            this.driver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
        }
        else if (this.engine.equals("Access")) {
            this.driver = "sun.jdbc.odbc.JdbcOdbcDriver";
        }
        else {
            this.driver = "sun.jdbc.odbc.JdbcOdbcDriver";
        }
    }
    
    public void accessConnect() throws Exception {
        try {
            Class.forName(this.driver);
            (this.connection = DriverManager.getConnection(this.url, this.username, this.password)).setAutoCommit(false);
            this.isConnected = true;
        }
        catch (SQLException ex) {
            this.isConnected = false;
            throw ex;
        }
        catch (ClassNotFoundException ex2) {
            this.isConnected = false;
            throw ex2;
        }
        catch (Exception ex3) {
            this.isConnected = false;
            throw ex3;
        }
    }
    
    public void defaultConnect() throws Exception {
        try {
            Class.forName(this.driver);
            (this.connection = DriverManager.getConnection(this.url, this.username, this.password)).setAutoCommit(false);
            this.isConnected = true;
        }
        catch (SQLException ex) {
            this.isConnected = false;
            throw ex;
        }
        catch (ClassNotFoundException ex2) {
            this.isConnected = false;
            throw ex2;
        }
        catch (Exception ex3) {
            this.isConnected = false;
            throw ex3;
        }
    }
    
    public void DB2Connect() throws Exception {
        try {
            Class.forName(this.driver);
            (this.connection = DriverManager.getConnection(this.url, this.username, this.password)).setAutoCommit(false);
            this.connection.createStatement().execute("set current schema " + this.dataSource);
            this.isConnected = true;
        }
        catch (SQLException ex) {
            this.isConnected = false;
            throw ex;
        }
        catch (ClassNotFoundException ex2) {
            this.isConnected = false;
            throw ex2;
        }
        catch (Exception ex3) {
            this.isConnected = false;
            throw ex3;
        }
    }
    
    public void oracleConnect() throws Exception {
        try {
            Class.forName(this.driver);System.out.println(this.url);
            (this.connection = DriverManager.getConnection(this.url, this.username, this.password)).setAutoCommit(false);
            this.connection.createStatement().execute("alter session set NLS_SORT=BINARY");
            this.isConnected = true;
        }
        catch (SQLException ex) {
            this.isConnected = false;
            throw ex;
        }
        catch (ClassNotFoundException ex2) {
            this.isConnected = false;
            throw ex2;
        }
        catch (Exception ex3) {
            this.isConnected = false;
            throw ex3;
        }
    }
    
    public void sqlServerConnect() throws Exception {
        try {
            Class.forName(this.driver);
            (this.connection = DriverManager.getConnection(this.url)).setAutoCommit(false);
            this.isConnected = true;
        }
        catch (SQLException ex) {
            this.isConnected = false;
            throw ex;
        }
        catch (ClassNotFoundException ex2) {
            this.isConnected = false;
            throw ex2;
        }
        catch (Exception ex3) {
            this.isConnected = false;
            throw ex3;
        }
    }
}
