// 
// Decompiled by Procyon v0.5.30
// 

package xseedRts;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;

public class XseedDynamicSql extends XseedSql
{
    public Statement statement;
    
    public XseedDynamicSql(final XseedDBResource resource) throws Exception {
        this.resource = resource;
        this.connection = resource.connection;
    }
    
    public XseedDynamicSql(final Connection connection, final XseedDBResource resource) throws Exception {
        this.resource = resource;
        this.connection = connection;
    }
    
    public void append(final String s) throws Exception {
        this.command += s;
    }
    
    public void close() throws Exception {
        if (this.resultSet != null) {
            this.resultSet.close();
            this.resultSet = null;
        }
        if (this.statement != null) {
            this.statement.close();
            this.statement = null;
        }
        this.bof = false;
        this.eof = false;
        this.isFirst = false;
        this.recordsAffected = 0;
        this.command = "";
        this.resource.isOpen = false;
    }
    
    public void initialize() throws Exception {
        if (this.resultSet != null) {
            this.resultSet.close();
            this.resultSet = null;
        }
        if (this.statement != null) {
            this.statement.close();
            this.statement = null;
        }
        this.bof = false;
        this.eof = false;
        this.isFirst = false;
        this.recordsAffected = 0;
        this.command = "";
    }
    
    public void insert() throws Exception {
        try {
            if (this.statement == null) {
                this.statement = this.connection.createStatement();
            }manejoLogs.General.imprimir("command:"+this.command);
            this.recordsAffected = this.statement.executeUpdate(this.command);
manejoLogs.General.imprimir("recordsAffected:"+recordsAffected);
            this.resource.lastSql = this.command;
            this.resource.rowCount = this.recordsAffected;
            this.statement.close();
            this.statement = null;
            if (this.recordsAffected == 0) {
                this.resource.exception = new XseedException("NOTINCLUDED", this.command);
            }
            else {
                this.resource.exception = null;
            }
        }
        catch (SQLException ex) {manejoLogs.General.imprimir(ex);
            this.insertExceptionHandler(ex, this.command);
        }catch(Throwable t){
					manejoLogs.General.imprimir(t);
				}
    }
    
    public void update() throws Exception {
        try {
            if (this.statement == null) {
                this.statement = this.connection.createStatement();
            }
            this.recordsAffected = this.statement.executeUpdate(this.command);
            this.resource.lastSql = this.command;
            this.resource.rowCount = this.recordsAffected;
            this.statement.close();
            this.statement = null;
            if (this.recordsAffected == 0) {
                this.resource.exception = new XseedException("NOTMODIFIED", this.command);
            }
            else {
                this.resource.exception = null;
            }
        }
        catch (SQLException ex) {
            throw new Exception(ex.getMessage() + " executing " + this.command);
        }
    }
    
    public void delete() throws Exception {
        try {
            if (this.statement == null) {
                this.statement = this.connection.createStatement();
            }
            this.recordsAffected = this.statement.executeUpdate(this.command);
            this.resource.lastSql = this.command;
            this.resource.rowCount = this.recordsAffected;
            this.statement.close();
            this.statement = null;
            if (this.recordsAffected == 0) {
                this.resource.exception = new XseedException("NOTDELETED", this.command);
            }
            else {
                this.resource.exception = null;
            }
        }
        catch (SQLException ex) {
            throw new Exception(ex.getMessage() + " executing " + this.command);
        }
    }
    
    public void select() throws Exception {
        try {
            final String upperCase = this.command.trim().substring(0, 6).toUpperCase();
            if (upperCase.equals("INSERT")) {
                this.include();
                return;
            }
            if (upperCase.equals("UPDATE")) {
                this.modify();
                return;
            }
            if (upperCase.equals("DELETE")) {
                this.delete();
                return;
            }
            if (this.statement == null) {
                this.statement = this.connection.createStatement(1004, 1007);
            }
            if (this.resultSet != null) {
                this.resultSet.close();
            }
            this.resultSet = this.statement.executeQuery(this.command);
            this.resource.lastSql = this.command;
            if (this.resultSet.next()) {
                this.isFirst = true;
                this.foundHandler();
            }
            else {
                this.isFirst = false;
                this.notFoundHandler();
            }
        }
        catch (SQLException ex) {
            throw new XseedSQLException(ex, this.command);
        }
    }
    
    public void selectRow(final int n) throws Exception {
        try {
            boolean b = false;
            if (this.statement == null) {
                this.statement = this.connection.createStatement(1004, 1007);
            }
            if (this.resultSet != null) {
                this.resultSet.close();
            }
            this.resultSet = this.statement.executeQuery(this.command);
            this.resource.lastSql = this.command;
            if (n == -1) {
                if (this.resultSet.last()) {
                    this.isFirst = true;
                    this.foundHandler();
                    b = true;
                }
            }
            else if (n > 0) {
                if (this.resultSet.first()) {
                    if (this.resultSet.relative(n - 1)) {
                        this.isFirst = true;
                        this.foundHandler();
                        b = true;
                    }
                    else if (this.resultSet.last()) {
                        this.isFirst = true;
                        this.foundHandler();
                        b = true;
                    }
                }
            }
            else if (this.resultSet.first()) {
                this.isFirst = true;
                this.foundHandler();
                b = true;
            }
            if (!b) {
                this.isFirst = false;
                this.notFoundHandler();
            }
        }
        catch (SQLException ex) {
            throw new XseedSQLException(ex, this.command);
        }
    }
    
    public void prepare() throws Exception {
    }
    
    public void setParameter(final int n, final XseedDBField xseedDBField) throws Exception {
    }
    
    public void setParameter(final int n, final Object o) throws Exception {
    }
}
