package manejoLogs;
import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.util.*;
import java.text.*;
public class General extends HttpServlet{
	public static PrintStream ps=null;
	public static long clave;
	public static String ruta;
	private static PrintStream reg=null;
	private static HashMap hash;
	public void init(ServletConfig config)throws ServletException{
		General.iniciarRuta(config.getServletContext());
	}
	public static long calcularClave(long time){
		return (long)Math.floor((time-18000000)/86400000.);
	}
	public static String obtenerNombre(Date date){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		return sdf.format(date);
	}
	public static void imprimir(String mensaje){
		try{
			obtener();
			Date tiempoActual=new Date();
			ps.println(formato(tiempoActual)+":"+(tiempoActual).getTime()+":"+mensaje);
		}catch(Exception e){
			clave=0;
		}
	}
	public static void imprimir(Throwable t){
		try{
			obtener();
			Date tiempoActual=new Date();
			ps.println(formato(tiempoActual)+":"+(tiempoActual).getTime());
			t.printStackTrace(ps);
			registrar(t);
		}catch(Exception e){
			clave=0;
		}
	}
	public static void imprimir(Throwable t,String mensaje){
		try{
			obtener();
			Date tiempoActual=new Date();
			ps.println(formato(tiempoActual)+":"+(tiempoActual).getTime()+":"+mensaje);
			t.printStackTrace(ps);
			registrar(t);
		}catch(Exception e){
			clave=0;
		}
	}
	public static void registrar(Throwable t){
		try{
			if(reg==null){
				reg=new PrintStream(new FileOutputStream(new File(ruta+"registro.txt"),true));
			}
			String clave=t.getStackTrace()[0].getClassName()+":"+t.getStackTrace()[0].getLineNumber()+"->"+t;
			if(hash==null){
				hash=new HashMap();
			}
			if(hash.get(clave)==null){
				Date tiempoActual=new Date();
				reg.println(formato(tiempoActual)+":"+(tiempoActual).getTime()+":Excepción "+(hash.size())+":Clave:"+clave);
				t.printStackTrace(reg);
				hash.put(clave,new Integer(1));
				if(hash.size()>1000){
					hash=null;
					reg.println("Más de mil errores");
				}
			}
		}catch(Throwable tx){
			reg=null;
		}
	}
	public static String formato(Date date){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		return sdf.format(date);
	}
	private static void obtener(){
		Date d=new Date();
		long time=d.getTime();
		long nuevaClave=calcularClave(time);
		if(clave!=nuevaClave||ps==null){
			try{
				String nombre=obtenerNombre(d);
				ps=new PrintStream(new FileOutputStream(new File(ruta+nombre+".txt"),true));
				clave=nuevaClave;
				imprimir(clave+":"+General.class.getName()+" INICIO LOG");
			}catch(Exception e){
			}
		}
	}
	public static void iniciarRuta(ServletContext contexto){
		ruta=contexto.getRealPath("/");
	}
	public static String traza(int a){
		String cad="";
		try{
			throw new Exception();
		}catch(Exception e){
			try{
				String s="";
				for(int i=1+a;i<e.getStackTrace().length;i++){
					cad+=s+e.getStackTrace()[i].getClassName()+"."+e.getStackTrace()[i].getMethodName()+":"+e.getStackTrace()[i].getLineNumber();
					s=" ";
				}
			}catch(Throwable t){
			}
		}
		return cad;
	}
	public static void traza(){
		imprimir(traza(1));
	}
	public static void traza(String m){
		imprimir(traza(1)+"->"+m);
	}
}
