import java.sql.*;
import java.util.*;
import xseedRts.*;


public class TCONSUMOGEN extends XseedTable
{









public XseedDBField codigoSic;
public XseedDBField exportacion1;
public XseedDBField exportacion2;
public XseedDBField fecha;
public XseedDBField hora;
public XseedDBField importacion;
public XseedDBField mercado;
public XseedDBField version;



public XseedSql PK;

TCONSUMOGEN (Connection pConnection) throws Exception
{
    connection = pConnection;
    tableInitialize();
}

TCONSUMOGEN (XseedConnection pDatabase, XseedSession pSession) throws Exception
{
    session = pSession;
    connection = pDatabase.connection;
    tableInitialize();
}

TCONSUMOGEN (XseedSession pSession) throws Exception
{
    session = pSession;
    tableInitialize();
}

public void fieldsInitialize () throws Exception
{
    fieldSetup(8, "TCONSUMOGEN");
    codigoSic = new XseedDBField ("codigoSic", "Text", 20, 0, "");
    addField(codigoSic);

    exportacion1 = new XseedDBField ("exportacion1", "Number", 18, 6, "0");
    addField(exportacion1);

    exportacion2 = new XseedDBField ("exportacion2", "Number", 18, 6, "0");
    addField(exportacion2);

    fecha = new XseedDBField ("fecha", "Number", 8, 0, "0");
    addField(fecha);

    hora = new XseedDBField ("hora", "Number", 2, 0, "0");
    addField(hora);

    importacion = new XseedDBField ("importacion", "Number", 18, 4, "0");
    addField(importacion);

    mercado = new XseedDBField ("mercado", "Text", 2, 0, "");
    addField(mercado);

    version = new XseedDBField ("version", "Text", 3, 0, "");
    addField(version);

}

public void virtualFieldsInitialize () throws Exception
{
}

public void associationsInitialize () throws Exception
{
}

public void indexesInitialize () throws Exception
{
   if (isPrepared)
   {
      PK = new XseedPreparedSql(this);
   }
   else
   {
      PK = new XseedDynamicSql(this);
   }
}

public void virtualIndexesInitialize () throws Exception
{
}

public void close() throws Exception
{
    if (PK != null)
    {
        PK.close();
        PK = null;
    }
    initialize();
}

public void initialize () throws Exception
{
    codigoSic.initialize();
    exportacion1.initialize();
    exportacion2.initialize();
    fecha.initialize();
    hora.initialize();
    importacion.initialize();
    mercado.initialize();
    version.initialize();
}

public void rulesInitialize ()
  throws Exception
{
}

public void tablesInitialize ()
  throws Exception
{
}

public void queriesInitialize ()
  throws Exception
{
}

public void filesInitialize ()
  throws Exception
{
}

public void connectionInitialize()
  throws Exception
{
}
public void virtualConstructors() throws Exception
{
}
public void automaticConstructors () throws Exception
{
}

public void getFieldsForQueries (String pFieldName, String pFieldValue) throws Exception
{
   if ( pFieldName.equalsIgnoreCase("codigoSic"))
   {
      codigoSic.setValue(pFieldValue);
   }
   if ( pFieldName.equalsIgnoreCase("exportacion1"))
   {
      exportacion1.setValue(pFieldValue);
   }
   if ( pFieldName.equalsIgnoreCase("exportacion2"))
   {
      exportacion2.setValue(pFieldValue);
   }
   if ( pFieldName.equalsIgnoreCase("fecha"))
   {
      fecha.setValue(pFieldValue);
   }
   if ( pFieldName.equalsIgnoreCase("hora"))
   {
      hora.setValue(pFieldValue);
   }
   if ( pFieldName.equalsIgnoreCase("importacion"))
   {
      importacion.setValue(pFieldValue);
   }
   if ( pFieldName.equalsIgnoreCase("mercado"))
   {
      mercado.setValue(pFieldValue);
   }
   if ( pFieldName.equalsIgnoreCase("version"))
   {
      version.setValue(pFieldValue);
   }
}

public void execute(String pAction) throws Exception
{
    pAction = pAction.toUpperCase();
    if (pAction.equals("UPDATE"))
    {
    	   update();
    }
    else if (pAction.equals("INCLUDE"))
    {
    	   include();
    }
    else if (pAction.equals("MODIFY"))
    {
    	   modify();
    }
    else if (pAction.equals("DELETE"))
    {
    	   delete();
    }
    else if (pAction.equals("FIND"))
    {
    	   find(codigoSic ,fecha ,hora ,mercado ,version);
    }
    else if (pAction.equals("FIRST"))
    {
    	   findFirst();
    }
    else if (pAction.equals("NEXT"))
    {
    	   findNext(codigoSic ,fecha ,hora ,mercado ,version);
    }
    else if (pAction.equals("PREVIOUS"))
    {
    	   findPrevious(codigoSic ,fecha ,hora ,mercado ,version);
    }
    else if (pAction.equals("LAST"))
    {
    	   findLast();
    }
    else if (pAction.equals("REFRESH"))
    {
    	   refresh();
    }
    else if (pAction.equals("IGNORE"))
    {
    	   ignore();
    }
}

public void findAll () throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.prepare();
        sql.select();
    }
    else
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.select();
    }
    setAlertMsg("find");
}

public void findFirst () throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.prepare();
        sql.select();
    }
    else
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.select();
    }
    setAlertMsg("find");
}

public void findLast () throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append( " order by ");
        sql.append("codigoSic Desc ");
        sql.append(",fecha Desc ");
        sql.append(",hora Desc ");
        sql.append(",mercado Desc ");
        sql.append(",version Desc ");
        sql.prepare();
        sql.select();
    }
    else
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append( " order by ");
        sql.append("codigoSic Desc ");
        sql.append(",fecha Desc ");
        sql.append(",hora Desc ");
        sql.append(",mercado Desc ");
        sql.append(",version Desc ");
        sql.select();
    }
    setAlertMsg("find");
}

public void findNext (XseedObject pCodigoSic,XseedObject pFecha,XseedObject pHora,XseedObject pMercado,XseedObject pVersion) throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("(codigoSic = ?  And fecha = ?  And hora = ?  And mercado = ?  And version > ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha = ?  And hora = ?  And mercado > ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha = ?  And hora > ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha > ? ) ");
        sql.append(" Or( codigoSic > ? ) ");
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.prepare();
        sql.setParameter(1, pCodigoSic);
        sql.setParameter(2, pFecha);
        sql.setParameter(3, pHora);
        sql.setParameter(4, pMercado);
        sql.setParameter(5, pVersion);
        sql.setParameter(6, pCodigoSic);
        sql.setParameter(7, pFecha);
        sql.setParameter(8, pHora);
        sql.setParameter(9, pMercado);
        sql.setParameter(10, pCodigoSic);
        sql.setParameter(11, pFecha);
        sql.setParameter(12, pHora);
        sql.setParameter(13, pCodigoSic);
        sql.setParameter(14, pFecha);
        sql.setParameter(15, pCodigoSic);
        sql.select();
    }
    else
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("(codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora = " + pHora.get() + " And mercado = '" + pMercado.get() + "' And version > '" + pVersion.get() + "') ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora = " + pHora.get() + " And mercado > '" + pMercado.get() + "') ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora > " + pHora.get() + ") ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha > " + pFecha.get() + ") ");
        sql.append(" Or( codigoSic > '" + pCodigoSic.get() + "') ");
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.select();
    }
    setAlertMsg("find");
}

public void findPrevious (XseedObject pCodigoSic,XseedObject pFecha,XseedObject pHora,XseedObject pMercado,XseedObject pVersion) throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("(codigoSic = ?  And fecha = ?  And hora = ?  And mercado = ?  And version < ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha = ?  And hora = ?  And mercado < ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha = ?  And hora < ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha < ? ) ");
        sql.append(" Or( codigoSic < ? ) ");
        sql.append( " order by ");
        sql.append("codigoSic Desc ");
        sql.append(",fecha Desc ");
        sql.append(",hora Desc ");
        sql.append(",mercado Desc ");
        sql.append(",version Desc ");
        sql.prepare();
        sql.setParameter(1, pCodigoSic);
        sql.setParameter(2, pFecha);
        sql.setParameter(3, pHora);
        sql.setParameter(4, pMercado);
        sql.setParameter(5, pVersion);
        sql.setParameter(6, pCodigoSic);
        sql.setParameter(7, pFecha);
        sql.setParameter(8, pHora);
        sql.setParameter(9, pMercado);
        sql.setParameter(10, pCodigoSic);
        sql.setParameter(11, pFecha);
        sql.setParameter(12, pHora);
        sql.setParameter(13, pCodigoSic);
        sql.setParameter(14, pFecha);
        sql.setParameter(15, pCodigoSic);
        sql.select();
    }
    else
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("(codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora = " + pHora.get() + " And mercado = '" + pMercado.get() + "' And version < '" + pVersion.get() + "') ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora = " + pHora.get() + " And mercado < '" + pMercado.get() + "') ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora < " + pHora.get() + ") ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha < " + pFecha.get() + ") ");
        sql.append(" Or( codigoSic < '" + pCodigoSic.get() + "') ");
        sql.append( " order by ");
        sql.append("codigoSic Desc ");
        sql.append(",fecha Desc ");
        sql.append(",hora Desc ");
        sql.append(",mercado Desc ");
        sql.append(",version Desc ");
        sql.select();
    }
    setAlertMsg("find");
}

public void findFrom (XseedObject pCodigoSic,XseedObject pFecha,XseedObject pHora,XseedObject pMercado,XseedObject pVersion) throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("(codigoSic = ?  And fecha = ?  And hora = ?  And mercado = ?  And version >= ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha = ?  And hora = ?  And mercado >= ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha = ?  And hora >= ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha >= ? ) ");
        sql.append(" Or( codigoSic > ? ) ");
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.prepare();
        sql.setParameter(1, pCodigoSic);
        sql.setParameter(2, pFecha);
        sql.setParameter(3, pHora);
        sql.setParameter(4, pMercado);
        sql.setParameter(5, pVersion);
        sql.setParameter(6, pCodigoSic);
        sql.setParameter(7, pFecha);
        sql.setParameter(8, pHora);
        sql.setParameter(9, pMercado);
        sql.setParameter(10, pCodigoSic);
        sql.setParameter(11, pFecha);
        sql.setParameter(12, pHora);
        sql.setParameter(13, pCodigoSic);
        sql.setParameter(14, pFecha);
        sql.setParameter(15, pCodigoSic);
        sql.select();
    }
    else
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("(codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora = " + pHora.get() + " And mercado = '" + pMercado.get() + "' And version >= '" + pVersion.get() + "') ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora = " + pHora.get() + " And mercado >= '" + pMercado.get() + "') ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora >= " + pHora.get() + ") ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha >= " + pFecha.get() + ") ");
        sql.append(" Or( codigoSic > '" + pCodigoSic.get() + "') ");
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.select();
    }
    setAlertMsg("find");
}

public void findBack (XseedObject pCodigoSic,XseedObject pFecha,XseedObject pHora,XseedObject pMercado,XseedObject pVersion) throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("(codigoSic = ?  And fecha = ?  And hora = ?  And mercado = ?  And version <= ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha = ?  And hora = ?  And mercado <= ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha = ?  And hora <= ? ) ");
        sql.append(" Or( codigoSic = ?  And fecha <= ? ) ");
        sql.append(" Or( codigoSic < ? ) ");
        sql.append( " order by ");
        sql.append("codigoSic Desc ");
        sql.append(",fecha Desc ");
        sql.append(",hora Desc ");
        sql.append(",mercado Desc ");
        sql.append(",version Desc ");
        sql.prepare();
        sql.setParameter(1, pCodigoSic);
        sql.setParameter(2, pFecha);
        sql.setParameter(3, pHora);
        sql.setParameter(4, pMercado);
        sql.setParameter(5, pVersion);
        sql.setParameter(6, pCodigoSic);
        sql.setParameter(7, pFecha);
        sql.setParameter(8, pHora);
        sql.setParameter(9, pMercado);
        sql.setParameter(10, pCodigoSic);
        sql.setParameter(11, pFecha);
        sql.setParameter(12, pHora);
        sql.setParameter(13, pCodigoSic);
        sql.setParameter(14, pFecha);
        sql.setParameter(15, pCodigoSic);
        sql.select();
    }
    else
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("(codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora = " + pHora.get() + " And mercado = '" + pMercado.get() + "' And version <= '" + pVersion.get() + "') ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora = " + pHora.get() + " And mercado <= '" + pMercado.get() + "') ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha = " + pFecha.get() + " And hora <= " + pHora.get() + ") ");
        sql.append(" Or( codigoSic = '" + pCodigoSic.get() + "' And fecha <= " + pFecha.get() + ") ");
        sql.append(" Or( codigoSic < '" + pCodigoSic.get() + "') ");
        sql.append( " order by ");
        sql.append("codigoSic Desc ");
        sql.append(",fecha Desc ");
        sql.append(",hora Desc ");
        sql.append(",mercado Desc ");
        sql.append(",version Desc ");
        sql.select();
    }
    setAlertMsg("find");
}

public void find (XseedObject pCodigoSic,XseedObject pFecha,XseedObject pHora,XseedObject pMercado,XseedObject pVersion) throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("codigoSic = ? ");
        sql.append(" And fecha = ? ");
        sql.append(" And hora = ? ");
        sql.append(" And mercado = ? ");
        sql.append(" And version = ? ");
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.prepare();
        sql.setParameter(1, pCodigoSic);
        sql.setParameter(2, pFecha);
        sql.setParameter(3, pHora);
        sql.setParameter(4, pMercado);
        sql.setParameter(5, pVersion);
        sql.select();
    }
    else
    {
        sql.initialize();
        sql.append(getQueryHeader());
        sql.append(" where ");
        sql.append("codigoSic = '" + pCodigoSic.get()+"'");
        sql.append(" And fecha = " + pFecha.get());
        sql.append(" And hora = " + pHora.get());
        sql.append(" And mercado = '" + pMercado.get()+"'");
        sql.append(" And version = '" + pVersion.get()+"'");
        sql.append( " order by ");
        sql.append("codigoSic");
        sql.append(",fecha");
        sql.append(",hora");
        sql.append(",mercado");
        sql.append(",version");
        sql.select();
    }
    setAlertMsg("find");
}

public void refresh() throws Exception
{
   initialize ();
}

public void ignore() throws Exception
{
}

public void update () throws Exception
{
    exceptionInitialize();
    include();
    if (exception != null)
    {
        modify();
    }
    setAlertMsg("update");
}

public void include () throws Exception
{System.out.println("Include");
    exceptionInitialize();
    preInsert();
    virtualConstraints();
    if (exception != null)
    {System.out.println(exception);
        return;
    }System.out.println(isPrepared);
    if (isPrepared)
    {
        upd.initialize();
        upd.append("insert into TCONSUMOGEN(");
        upd.append("codigoSic");
        upd.append(",exportacion1");
        upd.append(",exportacion2");
        upd.append(",fecha");
        upd.append(",hora");
        upd.append(",importacion");
        upd.append(",mercado");
        upd.append(",version");
        upd.append(" ) values (");
        upd.append("?");
        upd.append(",?");
        upd.append(",?");
        upd.append(",?");
        upd.append(",?");
        upd.append(",?");
        upd.append(",?");
        upd.append(",?");
        upd.append(")");
        upd.prepare();
        upd.setParameter(1,codigoSic);
        upd.setParameter(2,exportacion1);
        upd.setParameter(3,exportacion2);
        upd.setParameter(4,fecha);
        upd.setParameter(5,hora);
        upd.setParameter(6,importacion);
        upd.setParameter(7,mercado);
        upd.setParameter(8,version);
        upd.include();
    }
    else
    {System.out.println(upd.getClass().getName());
        upd.initialize();
        upd.append("insert into TCONSUMOGEN(");
        upd.append("codigoSic");
        upd.append(",exportacion1");
        upd.append(",exportacion2");
        upd.append(",fecha");
        upd.append(",hora");
        upd.append(",importacion");
        upd.append(",mercado");
        upd.append(",version");
        upd.append(" ) values (");
        upd.append(" " + dbFix(codigoSic));
        upd.append(", " + dbFix(exportacion1));
        upd.append(", " + dbFix(exportacion2));
        upd.append(", " + dbFix(fecha));
        upd.append(", " + dbFix(hora));
        upd.append(", " + dbFix(importacion));
        upd.append(", " + dbFix(mercado));
        upd.append(", " + dbFix(version));
        upd.append(")");
        upd.include();
    }
    virtualConstructors();
    automaticConstructors();
    postInsert();
    setAlertMsg("include");
}

public void modify () throws Exception
{
    exceptionInitialize();
    preUpdate();
    virtualConstraints();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        upd.initialize();
        upd.append("update TCONSUMOGEN set ");
        upd.append("codigoSic = ? ");
        upd.append(",exportacion1 = ? ");
        upd.append(",exportacion2 = ? ");
        upd.append(",fecha = ? ");
        upd.append(",hora = ? ");
        upd.append(",importacion = ? ");
        upd.append(",mercado = ? ");
        upd.append(",version = ? ");
        upd.append(" where (");
        upd.append("codigoSic = ? ");
        upd.append(" And fecha = ? ");
        upd.append(" And hora = ? ");
        upd.append(" And mercado = ? ");
        upd.append(" And version = ? ");
        upd.append(")");
        upd.prepare();
        upd.setParameter(1, codigoSic);
        upd.setParameter(2, exportacion1);
        upd.setParameter(3, exportacion2);
        upd.setParameter(4, fecha);
        upd.setParameter(5, hora);
        upd.setParameter(6, importacion);
        upd.setParameter(7, mercado);
        upd.setParameter(8, version);
        upd.setParameter(9, codigoSic);
        upd.setParameter(10, fecha);
        upd.setParameter(11, hora);
        upd.setParameter(12, mercado);
        upd.setParameter(13, version);
        upd.modify();
    }
    else
    {
        upd.initialize();
        upd.append("update TCONSUMOGEN set ");
        upd.append("codigoSic = " + dbFix(codigoSic));
        upd.append(", exportacion1 = " + dbFix(exportacion1));
        upd.append(", exportacion2 = " + dbFix(exportacion2));
        upd.append(", fecha = " + dbFix(fecha));
        upd.append(", hora = " + dbFix(hora));
        upd.append(", importacion = " + dbFix(importacion));
        upd.append(", mercado = " + dbFix(mercado));
        upd.append(", version = " + dbFix(version));
        upd.append(" where (");
        upd.append("codigoSic = '" + codigoSic.get() + "'");
        upd.append(" And fecha = " + fecha.get());
        upd.append(" And hora = " + hora.get());
        upd.append(" And mercado = '" + mercado.get() + "'");
        upd.append(" And version = '" + version.get() + "'");
        upd.append(")");
        upd.modify();
    }
    virtualConstructors();
    automaticConstructors();
    postUpdate();
    setAlertMsg("modify");
}

public void delete () throws Exception
{
    exceptionInitialize();
    preDelete();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        upd.initialize();
        upd.append(" delete from TCONSUMOGEN where ");
        upd.append("codigoSic = ?" );
        upd.append(" And fecha = ?" );
        upd.append(" And hora = ?" );
        upd.append(" And mercado = ?" );
        upd.append(" And version = ?" );
        upd.prepare();
        upd.setParameter(1, codigoSic );
        upd.setParameter(2, fecha );
        upd.setParameter(3, hora );
        upd.setParameter(4, mercado );
        upd.setParameter(5, version );
        upd.delete();
    }
    else
    {
        upd.initialize();
        upd.append(" delete from TCONSUMOGEN where ");
        upd.append("codigoSic = '" + codigoSic.get()+"'");
        upd.append(" And fecha = " + fecha.get());
        upd.append(" And hora = " + hora.get());
        upd.append(" And mercado = '" + mercado.get()+"'");
        upd.append(" And version = '" + version.get()+"'");
        upd.delete();
    }
    postDelete();
    setAlertMsg("delete");
}

/** Index */
public void PK (XseedObject pCodigoSic,XseedObject pFecha,XseedObject pHora,XseedObject pMercado,XseedObject pVersion) throws Exception
{
    exceptionInitialize();
    preQuery();
    if (exception != null)
    {
        return;
    }
    if (isPrepared)
    {
        PK.initialize();
        PK.append(getQueryHeader());
        PK.append(" where ");
        PK.append("codigoSic = ? ");
        PK.append("fecha = ? ");
        PK.append("hora = ? ");
        PK.append("mercado = ? ");
        PK.append("version = ? ");
        PK.append(" order by ");
        PK.append("codigoSic");
        PK.append(",fecha");
        PK.append(",hora");
        PK.append(",mercado");
        PK.append(",version");
        PK.prepare();
        PK.setParameter(1, pCodigoSic);
        PK.setParameter(2, pFecha);
        PK.setParameter(3, pHora);
        PK.setParameter(4, pMercado);
        PK.setParameter(5, pVersion);
        PK.select();
    }
    else
    {
        PK.initialize();
        PK.append(getQueryHeader());
        PK.append(" where ");
        PK.append("codigoSic = '" + pCodigoSic.get()+"'");
        PK.append(" And fecha = " + pFecha.get());
        PK.append(" And hora = " + pHora.get());
        PK.append(" And mercado = '" + pMercado.get()+"'");
        PK.append(" And version = '" + pVersion.get()+"'");
        PK.append(" order by ");
        PK.append("codigoSic");
        PK.append(",fecha");
        PK.append(",hora");
        PK.append(",mercado");
        PK.append(",version");
        PK.select();
    }
    setAlertMsg("select");
}

public void virtualConstraints () throws Exception
{
}







} /** End Class */
