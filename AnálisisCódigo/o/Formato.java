package o;
import java.util.*;
import java.util.stream.*;
import java.io.*;
import java.lang.reflect.*;
public class Formato{
	public HashMap<String,Object>hash;
	ArrayList objetos=new ArrayList();
	Object objeto=null;
	public Formato(){
		this.hash=new HashMap<String,Object>();
		this.objetos=new ArrayList();
	}
	public Object[]con(String[]a)throws Exception{
		Object[]os=new Object[a.length];
		for(int i=0;i<a.length;i++){
			try{
				os[i]=Integer.parseInt(a[i]);
				continue;
			}catch(Exception e){
			}
			try{
				os[i]=Double.parseDouble(a[i]);
				continue;
			}catch(Exception e){
			}
			if(a[i].startsWith("$")){
				String nombre=a[i].substring(1);
				try{
					os[i]=objetos.get(Integer.parseInt(nombre));
				}catch(Exception e){
					os[i]=hash.get(nombre);
				}
				if(os[i]==null){
					throw new Exception("No existe la variable:"+a[i]);
				}
				continue;
			}
			/*if(a[i].equals("|T")){
				os[i]=new Boolean(true);
				continue;
			}*/
			os[i]=a[i];
		}
		return os;
	}
	public static Class[]tipos(Object[]os)throws Exception{
		Class[]resultado=new Class[os.length];
		HashMap<Class,Class>h=new HashMap<Class,Class>();
		h.put(Integer.class,int.class);
		h.put(Double.class,double.class);
		for(int i=0;i<os.length;i++){
			if(h.get(os[i].getClass())!=null){
				resultado[i]=h.get(os[i].getClass());
			}else{
				resultado[i]=os[i].getClass();
			}
		}
		return resultado;
	}
	public static Object crear(Class c,Object[]os)throws Exception{
		Constructor[]cs=c.getConstructors();
		for(int i=0;i<cs.length;i++){
			try{
				return cs[i].newInstance(os);
			}catch(Exception e){
			}
		}
		return null;
	}
	public static void ejecutar(Object o,String m,Object[]os)throws Exception{
		Method[]ms=o.getClass().getMethods();
		for(int i=0;i<ms.length;i++){
			try{
				if(ms[i].getName().equals(m)){
					ms[i].invoke(o,os);
					return;
				}
			}catch(Throwable t){
			}
		}
		throw new Exception("No existe el método "+m+Arrays.stream(os).map(x->x.getClass()+"").collect(Collectors.joining(",","(",")"))+" en "+o.getClass().getName());
	}
	public Object ins(String codigo)throws Exception{
		//c.U.imp("codigo:"+codigo);
		Object resultado=null;
		if(codigo==null){
			return null;
		}
		String[]lineas=codigo.split("\n");
		for(int i=0;i<lineas.length;i++){
			String l=lineas[i];
			if(l.startsWith("-")){
				return null;
			}
			String[]p=l.split(" ");
			for(int ix=1;ix<p.length;ix++){
				p[ix].replaceAll("-"," ");
			}
			if(p[0].startsWith("|")){
				String nombreClase=p[0].substring(1);
				Class c=Class.forName(nombreClase);
				if(p.length==1){
					objeto=c.newInstance();
				}else{
					String[]parte=Arrays.copyOfRange(p,1,p.length);
					Object[]os=con(parte);
					objeto=crear(c,os);
				}
				objetos.add(objeto);
				continue;
			}
			if(p[0].startsWith("$")){
				String nombre=p[0].substring(1);
				hash.put(nombre,objeto);
				continue;
			}
			if(p[0].startsWith(">")){c.U.imp("codigo:"+lineas[1]);
				String nombre=p[0].substring(1);
				objeto=hash.get(nombre);
				continue;
			}
			if(p[0].equals("&")){
				resultado=objeto;
				return resultado;
			}
			if(p.length==1){
				Method metodo=objeto.getClass().getMethod(p[0],(Class[])null);
				metodo.invoke(objeto,(Object[])null);
			}else{
				String[]parte=Arrays.copyOfRange(p,1,p.length);
				Object[]os=con(parte);
				ejecutar(objeto,p[0],os);
			}
		}
		return resultado;
	}
	public Object cargar(File f)throws Exception{
		Scanner s=new Scanner(f);
		Object resultado=null;
		while(s.hasNextLine()){
			Object o=ins(s.nextLine());
			if(o!=null){
				resultado=o;
			}
		}
		return resultado;
	}
	public static void main(String[]argumentos){
		try{
			Formato formato=new Formato();
			System.out.println(formato.cargar(new File("Test01.alx")));
			//Integer.parseInt("5.6");
			//Double.parseDouble("5.6");
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}