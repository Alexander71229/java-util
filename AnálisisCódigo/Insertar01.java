import java.io.*;
import java.util.*;
import java.nio.*;
import java.nio.file.*;
import c.*;
import tc.*;
import o.*;
import x.*;
public class Insertar01{
	public static void main(String[]argumentos){
		try{
			U.imp("Inicio");
			File f=null;
			Scanner s=new Scanner(new File("Test03.txt"));
			Scanner y=null;
			String a=null;
			StringBuilder r=null;
			int lx=0;
			String tmp=s.nextLine();
			U.imp(tmp);
			String[]p=tmp.split("\\|");
			int l=Integer.parseInt(p[1]);
			int c=Integer.parseInt(p[2]);
			while(true){
				if(y==null){
					a=p[0];
					f=new File(a);
					y=new Scanner(f);
					r=new StringBuilder();
					lx=0;
				}
				if(y.hasNextLine()){
					String z=y.nextLine();
					lx++;
					if(l==lx&&a.equals(p[0])){
						r.append(Cad.insertar(z,c,"c.U.imp("+p[3]+");"));
						if(s.hasNextLine()){
							p=s.nextLine().split("\\|");
							l=Integer.parseInt(p[1]);
							c=Integer.parseInt(p[2]);
						}
					}else{
						r.append(z);
					}
					r.append("\n");
				}else{
					y.close();
					y=null;
					U.imp("Escribiendo en:"+f.getCanonicalPath());
					Cad.escribir(f,r+"");
				}
			}
		}catch(Throwable t){
			U.imp(t);
		}
	}
}