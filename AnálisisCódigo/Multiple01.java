import java.io.*;
import java.util.*;
import java.nio.*;
import java.nio.file.*;
import c.*;
import tc.*;
import o.*;
import x.*;
public class Multiple01{
	public static void main(String[]argumentos){
		try{
			U.imp("Inicio");
			Files.walk(Paths.get("..\\DB\\02\\SicDesarrollo\\")).forEach(z->{
				try{
					Parser p=(Parser)(new Formato()).cargar(new File("Def06.txt"));
					p.formato.hash.put("test01",new Test02());
					p.formato.hash.put("file",z.toFile());
					p.procesar(z.toFile());
				}catch(Exception e){
					c.U.imp(e);
				}
			});
		}catch(Throwable t){
			U.imp(t);
		}
	}
}