package c;
import java.io.*;
import java.util.*;
public class Cad{
	public static String insertar(File f,int l,int c,String n)throws Exception{
		Scanner s=new Scanner(f);
		int il=0;
		StringBuilder r=new StringBuilder();
		while(s.hasNextLine()){
			il++;
			String y=s.nextLine();
			if(il==l){
				r.append(insertar(y,c,n));
			}else{
				r.append(y);
			}
			r.append("\n");
		}
		return r+"";
	}
	public static void escribir(File f,String n)throws Exception{
		new PrintStream(new FileOutputStream(f)).print(n);
	}
	public static String insertar(String o,int i,String n){
		if(i>o.length()){
			return o+n;
		}
		return o.substring(0,i)+n+o.substring(i);
	}
}