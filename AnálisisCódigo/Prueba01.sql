create or replace PACKAGE BODY     PK_CONSERES IS
  VError VARCHAR2(500);
  BResul BOOLEAN;

  --
  -- Ejecuta una instruccisn en la BD.
  FUNCTION Ejecutar_SQL(ViSQL IN VARCHAR2, VoError IN OUT VARCHAR2)
    RETURN INTEGER IS
    IResul     INTEGER;
    ICursorGen INTEGER;
  BEGIN
    ICursorGen := Dbms_Sql.Open_Cursor;
    DBMS_SQL.Parse(ICursorGen, ViSQL, Dbms_Sql.Native);
    IResul := DBMS_SQL.EXECUTE(ICursorGen);
    DBMS_SQL.CLOSE_CURSOR(ICursorGen);
    RETURN(IResul);
  EXCEPTION
    WHEN OTHERS THEN
      VoError := VoError || ' ' || SUBSTR(SQLERRM, 1, 255);
      IF Dbms_SQL.Is_Open(ICursorGen) THEN
        DBMS_SQL.CLOSE_CURSOR(ICursorGen);
      END IF;
      RETURN(0);
  END Ejecutar_SQL;

  -- Determina cuantos dias habiles hay entre dos fechas
  FUNCTION Dias_Habiles(DiInicial IN DATE, DiFinal IN DATE)
    RETURN PLS_INTEGER IS
    DInicial DATE := DiInicial;
    ICant    PLS_INTEGER := 0;
  BEGIN
    IF DiInicial IS NULL OR DiFinal IS NULL OR DiInicial > DiFinal THEN
      RETURN(0);
    END IF;
    LOOP
      IF Es_Laborable(DInicial) THEN
        ICant := ICant + 1;
      END IF;
      DInicial := DInicial + 1;
      EXIT WHEN DInicial > DiFinal;
    END LOOP;
    RETURN(Icant);
  END;

  -- Determina cuantos dias no habiles (o no laborales) hay entre dos fechas
  FUNCTION Dias_No_Habiles(DiInicial IN DATE, DiFinal IN DATE)
    RETURN PLS_INTEGER IS
    DInicial DATE := DiInicial;
    ICant    PLS_INTEGER := 0;
  BEGIN
    IF DiInicial IS NULL OR DiFinal IS NULL OR DiInicial > DiFinal THEN
      RETURN(0);
    END IF;
    LOOP
      IF NOT Es_Laborable(DInicial) THEN
        ICant := ICant + 1;
      END IF;
      DInicial := DInicial + 1;
      EXIT WHEN DInicial > DiFinal;
    END LOOP;
    RETURN(Icant);
  END;

  FUNCTION Crear_Tipo(ViTablaMaestra IN VARCHAR2,
                      ViNombreTipo   IN VARCHAR2,
                      ViCodTipo      IN VARCHAR2) RETURN NUMBER IS
    CURSOR CTip IS
      SELECT NVL(MAX(Sec), 0) + 1
        FROM CON_TIPOS;
    VSecMod VARCHAR2(30) := Sec_Modulo(ViTablaMaestra, 'TM');
    NSec    NUMBER;
  BEGIN
    OPEN CTip;
    FETCH CTip
      INTO NSec;
    CLOSE CTip;
    INSERT INTO CON_TIPOS
      (MOD_SEC,
       Sec,
       Nombre,
       Codigo,
       Esta_Activo,
       Fecha_Creacion,
       Creado_Por)
    VALUES
      (VSecMod,
       NSec,
       SUBSTR(ViNombreTipo, 1, 60),
       ViCodTipo,
       'S',
       SYSDATE,
       USER)
    RETURNING SEC INTO NSec;
    RETURN(NSec);
  END;
  -- Recibe un mensaje ocurrido por constraint y retorna el mensaje a mostrar al usuario.
  FUNCTION Msj_Constraint(ViMsj IN VARCHAR2, NiCodigo IN NUMBER)
    RETURN VARCHAR2 IS
    IPos    PLS_INTEGER;
    VNombre VARCHAR2(61);
    CURSOR CMsj(PConstraint IN VARCHAR2) IS
      SELECT SUBSTR(MSJ.Mensaje, 1, 500) Msj
        FROM Con_Vconstraints Msj
       WHERE Msj.Nombre = PConstraint;
    CURSOR CCons(PConstraint IN VARCHAR2) IS
      SELECT Ucon.Constraint_Type  Tipo,
             Ucon.Search_Condition Condicion,
             Ucon.Table_Name       Tabla
        FROM ALL_CONSTRAINTS Ucon
       WHERE Ucon.Owner = Pk_Conseres.Param('GUSUARIO_DUEGNO') AND
             Ucon.Constraint_Name = UPPER(PConstraint);
    RCons     CCons%ROWTYPE;
    VMsj      VARCHAR2(500);
    NSecCons  NUMBER;
    VProc     VARCHAR2(30) := 'msj_constraint';
    VSecTabla VARCHAR2(30);
    VTabla    CON_MODULOS.Descripcion%TYPE;
  BEGIN
    -- Busca el nombre del constraint.
    IPos := INSTR(ViMsj, '.', INSTR(ViMsj, '('));
    IF (IPos = 0) THEN
      VMsj := NULL;
    ELSE
      VNombre := SUBSTR(ViMsj, IPos + 1, INSTR(ViMsj, ')', IPos) - IPos - 1);
      -- Busca si ya hay un mensaje para el constraint.
      OPEN CMsj(VNombre);
      FETCH CMsj
        INTO VMsj;
      IF CMsj%FOUND THEN
        VMsj := NVL(VMsj, 'Fallo la regla de validacion ' || VNombre);
      ELSE
        NSecCons := Crear_Tipo('con_vconstraints',
                               SUBSTR(VNombre, 1, 60),
                               NULL);
        OPEN CCons(VNombre);
        FETCH CCons
          INTO RCons;
        IF CCons%NOTFOUND THEN
          VMsj := 'No encontro el constraint:' || VNombre;
        ELSE
          VSecTabla := Pk_Conseres.Sec_Modulo(RCons.Tabla, 'T');
          VTabla    := NVL(Pk_Conseres.Dato_Mod(VSecTabla, 'descripcion'),
                           RCons.Tabla);
          IF RCons.Tipo IN ('P') THEN
            VMsj := 'Atencion:' || CHR(10) || CHR(10) ||
                    'No pudo guardarse el registro en  "' || VTabla ||
                    '" Porque ya existe un registro igual en la base de datos.';
          ELSIF RCons.Tipo IN ('P', 'U') THEN
            VMsj := 'Atencion:' || CHR(10) || CHR(10) ||
                    'No pudo guardarse el registro en  "' || VTabla ||
                    '" Porque ya existe un registro igual en la base de datos.';
          ELSIF RCons.Tipo IN ('C') THEN
            VMsj := 'Atencion:' || CHR(10) || CHR(10) ||
                    'No pudo guardarse el registro en "' || VTabla ||
                    '" porque no cumple la siguiente condicion: ' ||
                    RCons.Condicion;
          ELSIF RCons.Tipo IN ('R') THEN
            -- Si fallo porque tiene registros relacinados...
            IF NiCodigo = 2292 THEN
              VMsj := 'Atencion:' || CHR(10) || CHR(10) ||
                      'No pudo guardarse el registro porque tiene registros relacionados en "' ||
                      VTabla || '".';
            ELSE
              VMsj := 'Atencion:' || CHR(10) || CHR(10) ||
                      'No pudo guardarse el registro en "' || VTabla || '".' ||
                      CHR(10) ||
                      'Esto debido a que no cumple la regla de referencia o clave foranea ' ||
                      VNombre;
            END IF;
          END IF;
        END IF;
        CLOSE CCons;
        Pk_Conseres.Guardar_Param_Tipo(NSecCons, 'mensaje', VMsj);
        COMMIT;
        IF VMsj IS NULL THEN
          VMsj := 'Fallo la regla de validacion ' || VNombre;
        END IF;
      END IF;
      CLOSE CMsj;
    END IF;
    RETURN(VMsj);
  END Msj_Constraint;
  -- Recibe un mensaje devuelto por la base de datos y
  -- ... extracta el pimer mensaje para ser mostrado al usuario.
  PROCEDURE Msj_Aplicacion(VioMsj IN OUT VARCHAR2) IS
    NPos NUMBER := INSTR(VioMsj, 'ORA-', 5);
  BEGIN
    IF (NPos >= 20) THEN
      VioMsj := SUBSTR(SUBSTR(VioMsj, 1, NPos - 2), 12);
    ELSE
      VioMsj := SUBSTR(VioMsj, 12);
    END IF;
  END Msj_Aplicacion;

  FUNCTION Dato_Mod(ViSec IN VARCHAR2, ViCol IN VARCHAR2) RETURN VARCHAR2 IS
    VDato VARCHAR2(2040);
    VSql  VARCHAR2(500);
    TYPE RcExp IS REF CURSOR;
    CExp RcExp;
  BEGIN
    IF ViCol IS NULL THEN
      RAISE_APPLICATION_ERROR(-20750,
                              'Debe suministrar un nombre de columna a la funcion Pk_Conseres.dato_mod');
    END IF;
    IF ViSec IS NULL THEN
      RETURN(NULL);
    END IF;
    VSql := 'SELECT ' || ViCol || ' ' || 'FROM con_modulos modu ' ||
            'WHERE modu.secuencia = ''' || ViSec || '''';
    OPEN CExp FOR VSql;
    FETCH CExp
      INTO VDato;
    CLOSE CExp;
    RETURN(VDato);
  END Dato_Mod;

  PROCEDURE Adm_Msj(ViClase     IN VARCHAR2,
                    NiCodigo    IN OUT NUMBER,
                    NiServerErr IN NUMBER,
                    ViTipo      IN VARCHAR2,
                    VioMsj      IN OUT VARCHAR2,
                    VoAccion    OUT VARCHAR2,
                    ViMsjServer IN VARCHAR2 := NULL) IS
    VCodigo VARCHAR2(12);
    CURSOR CMsj IS
      SELECT NVL(MSJ.Accion, 'MOSTRAR') Accion, MSJ.Nombre, Msj.Mensaje
        FROM Con_Vmensaje_Formas Msj
       WHERE Msj.Codigo = VCodigo;
    RMsj      CMsj%ROWTYPE;
    NSecCons  NUMBER;
    BResuelto BOOLEAN := FALSE;
  BEGIN
    VCodigo := SUBSTR(ViTipo || '-' || TO_CHAR(NiCodigo), 1, 12);
    -- si el error es de BD.
    IF (ViTipo = 'FRM' AND NiCodigo IN (40735, 40506, 40508, 40509, 40510)) THEN
      -- Si es de constraint
      IF NiServerErr IN (1, 2290, 2291, 2292) THEN
        VoAccion  := 'ALERTA';
        VioMsj    := SUBSTR(Msj_Constraint(ViMsjServer, NiServerErr),
                            1,
                            255);
        BResuelto := TRUE;
        -- Si es propio de la aplicacion
      ELSIF (NiServerErr >= 20000 AND NiServerErr <= 20999) THEN
        VioMsj   := ViMsjServer;
        VoAccion := 'ALERTA';
        Msj_Aplicacion(VioMsj);
        BResuelto := TRUE;
      END IF;
    ELSIF (ViTipo = 'FRM' AND NiCodigo IN (41009, 40735)) THEN
      -- Si es propio de la aplicacion
      IF (NiServerErr >= 20000 AND NiServerErr <= 20999) THEN
        VioMsj   := ViMsjServer;
        VoAccion := 'ALERTA';
        Msj_Aplicacion(VioMsj);
        BResuelto := TRUE;
      END IF;
    END IF;
    IF NOT BResuelto THEN
      OPEN CMsj;
      FETCH CMsj
        INTO RMsj;
      IF CMsj%NOTFOUND THEN
        NSecCons := Crear_Tipo('CON_VMENSAJE_FORMAS',
                               SUBSTR(VioMsj, 1, 60),
                               VCodigo);
        VoAccion := 'ORIGINAL';
        COMMIT;
      ELSE
        VoAccion := NVL(RMsj.Accion, 'ORIGINAL');
        IF VoAccion = 'OCULTAR' THEN
          VioMsj   := NULL;
          NiCodigo := NULL;
        ELSIF VoAccion IN ('MOSTRAR', 'ALERTA') THEN
          VioMsj   := NVL(RMsj.Mensaje, RMsj.Nombre);
          NiCodigo := NULL;
        ELSIF VoAccion = 'SOPORTE' THEN
          NULL;
        END IF;
      END IF;
      CLOSE CMsj;
    END IF;
  END Adm_Msj;

  PROCEDURE Eliminar_Reg(ViTabla IN VARCHAR2,
                         ViRowID IN VARCHAR2,
                         VoError OUT VARCHAR2) IS
    VError VARCHAR2(550);
    VSql   VARCHAR2(255);
    VConst VARCHAR2(150);
    IPos1  PLS_INTEGER;
    IPos2  PLS_INTEGER;
    IResul PLS_INTEGER;
    VTabla VARCHAR2(50);
    VDesc  VARCHAR2(255);
    VProp  VARCHAR2(50);
    CURSOR CConst IS
      SELECT Cons.TABLE_NAME
        FROM ALL_CONSTRAINTS Cons
       WHERE Cons.Owner = UPPER(VProp) AND
             Cons.CONSTRAINT_NAME = UPPER(VConst);
    CURSOR CTab IS
      SELECT Modu.Descripcion
        FROM CON_MODULOS Modu
       WHERE UPPER(Modu.Nombre) = UPPER(VTabla);
  BEGIN
    VSql   := 'DELETE ' || ViTabla || ' WHERE rowid = ''' || ViRowId || '''';
    IResul := Pk_Conseres.Ejecutar_Sql(VSql, VError);
    IF VError IS NULL THEN
      COMMIT;
    ELSE
      IF SUBSTR(LTRIM(VError), 1, 9) = 'ORA-02292' THEN
        -- Detemina el constraint involucrado.
        IPos1  := INSTR(VError, '(', 1) + 1;
        IPos2  := INSTR(VError, '.', IPos1);
        VProp  := SUBSTR(VError, IPos1, IPos2 - IPos1);
        IPos1  := IPos2 + 1;
        IPos2  := INSTR(VError, ')', IPos1);
        VConst := SUBSTR(VError, IPos1, IPos2 - IPos1);
        IF VConst IS NOT NULL THEN
          OPEN CConst;
          FETCH CConst
            INTO VTabla;
          IF CConst%FOUND THEN
            OPEN Ctab;
            FETCH Ctab
              INTO VDesc;
            CLOSE Ctab;
            VoError := 'No es posible eliminar el registro mientras exista infomación de "' ||
                       NVL(Vdesc, VTabla) || '"';
          END IF;
          CLOSE CConst;
        END IF;
      ELSE
        MSJ_APLICACION(VError);
        VoError := 'No puede eliminar el registro. ' || VError;
      END IF;
    END IF;
  END Eliminar_Reg;

  FUNCTION Sec_Modulo(ViNombre IN VARCHAR2, ViTipo IN VARCHAR2)
    RETURN VARCHAR2 IS
    VTipo2 VARCHAR2(2);
    CURSOR CMod IS
      SELECT Modu.Secuencia
        FROM CON_MODULOS Modu
       WHERE UPPER(Modu.Tipo) IN (UPPER(ViTipo), VTipo2) AND
             UPPER(Nombre) = UPPER(ViNombre);
    RMod CMod%ROWTYPE;
  BEGIN
    -- Verifica si el msdulo existe, o si no, lo crea
    IF UPPER(ViTipo) = 'F' THEN
      VTipo2 := 'SF';
    ELSIF ViTipo = 'R' THEN
      VTipo2 := 'G';
    ELSIF ViTipo = 'T' THEN
      VTipo2 := 'V';
    END IF;
    OPEN CMod;
    FETCH CMod
      INTO RMod;
    CLOSE CMod;
    RETURN(Rmod.Secuencia);
  END Sec_Modulo;
  
  
  FUNCTION Ver_Version(ViNombre  IN VARCHAR2,
                       ViTipo    IN VARCHAR2,
                       ViAutor   IN VARCHAR2,
                       DiVersion IN DATE,
                       ViDesc    IN VARCHAR2) RETURN BOOLEAN IS
  
    PRAGMA AUTONOMOUS_TRANSACTION; -- ASANCHEZ 09/06/2008 11:42:57 a.m.
   
    VTipo2 VARCHAR2(3);

    CURSOR CMod IS
      SELECT Modu.Fecha_Version, Modu.Secuencia
        FROM CON_MODULOS Modu
       WHERE UPPER(Modu.Tipo) = UPPER(ViTipo)
	     AND UPPER(Nombre) = UPPER(ViNombre);

	  RMod  CMod%ROWTYPE;
    VProc VARCHAR2(30) := 'con_version';
    vCtrl VARCHAR2(2) := Pk_Conseres.Param_General('GVERVERSION');
    
    --- JMarquez 09/Abril/2010 Req: 23127 
    ---   Variable de resultado para dejar un solo return al final del paquete
    bResultado  BOOLEAN := TRUE;
    vResultado  VARCHAR2(200) := 'Versión correcta';
  BEGIN
    -- Verifica si el msdulo existe, o si no, lo crea

    OPEN CMod;
    FETCH CMod
      INTO RMod;
    CLOSE CMod; -- JMARQUEZ 21/09/2004 para corregir el hecho de que retorna dejando el cursor abierto

    IF RMod.Fecha_Version IS NULL THEN
       --
       -- Req. 19811
       -- Fecha modificación : 09/06/2008 11:42:57 a.m.
       -- Usuario modifica   : Alberto Eduardo Sánchez B.
       -- Causa modificación : Controlar las versiones de las formas
       IF NVL(vCtrl,'NO') = 'SI' THEN
         INSERT INTO sproduccion.ts_sop_notificaciones
                     (tipo, secuencia, observaciones)
              VALUES ('FORMA',RMod.Secuencia,'NO EXISTE,'||TO_CHAR(Pks_Sessiones.RETORNAR_CODDEPENDENCIA));
         COMMIT;     
       END IF;  
       --
       -- Fin Req. 19811
       --- JMarquez 09/Abril/2010 Req: 23127 
       ---   Variable de resultado para dejar un solo return al final del paquete
    	 -- RETURN(FALSE);
       bResultado := FALSE;
       vResultado := 'No esta registrada la versión actual';
    END IF;

    -- Verifica que la versisn enviada no sea inferior a la versisn registrada.
    IF RMod.Fecha_Version IS NOT NULL AND
       TRUNC(DiVersion) < TRUNC(RMod.Fecha_Version) THEN
       --
       -- Req. 19811
       -- Fecha modificación : 09/06/2008 11:42:57 a.m.
       -- Usuario modifica   : Alberto Eduardo Sánchez B.
       -- Causa modificación : Controlar las versiones de las formas
       IF NVL(vCtrl,'NO') = 'SI' THEN
       INSERT INTO sproduccion.ts_sop_notificaciones
                   (tipo, secuencia, observaciones)
            VALUES ('FORMA',RMod.Secuencia,'FECHA,'||TO_CHAR(Pks_Sessiones.RETORNAR_CODDEPENDENCIA));
       COMMIT;
       END IF;
       --
       -- Fin Req. 19811

       --- JMarquez 09/Abril/2010 Req: 23127 
       ---   Variable de resultado para dejar un solo return al final del paquete
    	 -- RETURN(FALSE);
       bResultado := FALSE;
       vResultado := 'Desactualizada, versión inferior a la actual.';
    END IF;

    -- Crea un registro en soporte.
    IF TRUNC(DiVersion) > TRUNC(RMod.Fecha_Version) THEN
        -- Actualiza la nueva versisn cuano esta es superior
        UPDATE CON_MODULOS
           SET Fecha_Version = DiVersion,
               Autor         = ViAutor
         WHERE Secuencia = RMod.Secuencia;
        COMMIT;
        
       --- JMarquez 09/Abril/2010 Req: 23127 
       ---   Variable de resultado para dejar un solo return al final del paquete
    	 -- RETURN(TRUE);
       bResultado := TRUE;
       vResultado := 'Crea un nueva versión.';
    END IF;
   
    reg_uso_opcion(RMod.Secuencia, vResultado);
    RETURN(bResultado);
  END Ver_Version;

  -- Retorna la secuencia de un registro en la tabla con_tipos
  -- correspondiente a un msdulo (tabla maestra) y un csdigo
  FUNCTION Sec_Tipo_Codigo(NiSecTipo IN NUMBER, ViCodigo IN VARCHAR2)
    RETURN NUMBER IS
    CURSOR CTipo IS
      SELECT Tipo.Sec
        FROM CON_TIPOS Tipo
       WHERE Tipo.Codigo = ViCodigo AND Tipo.Mod_Sec = TO_CHAR(NisecTipo);
    NSec NUMBER;
  BEGIN

    -- jmarquez 21/09/2004 Elimina el condicional puesto que es manejado por el cursor
    --                     y es el caso menos utilizado (evitar preguntar cuando es llamado desde otros programas)
    --    IF niSecTipo IS NULL OR viCodigo IS NULL THEN
    --       RETURN(NULL);
    --    END IF;
    OPEN CTipo;
    FETCH CTipo
      INTO NSec;
    CLOSE CTipo;
    RETURN(NSec);
  END Sec_Tipo_Codigo;

  -- Retorna la secuencia del un registro en la tabla con_tipos
  FUNCTION Sec_Tipo_Nombre(NiSecTipo IN NUMBER, ViNombre IN VARCHAR2)
    RETURN NUMBER IS
    CURSOR CTipo IS
      SELECT Sec
        FROM CON_TIPOS
       WHERE UPPER(Nombre) LIKE '%' || UPPER(ViNombre) || '%' AND
             Mod_Sec = NisecTipo;
    NSec NUMBER;
  BEGIN
    -- jmarquez 21/09/2004 Elimina el condicional puesto que es manejado por el cursor
    --                     y es el caso menos utilizado (evitar preguntar cuando es llamado desde otros programas)
    --    IF niSecTipo IS NULL OR viCodigo IS NULL THEN
    --       RETURN(NULL);
    --    END IF;
    OPEN CTipo;
    FETCH CTipo
      INTO NSec;
    CLOSE CTipo;
    RETURN(NSec);
  END Sec_Tipo_Nombre;

  -- Retorna el nombre de la aplicacisn a la que el usuario esta conectado.
  FUNCTION Nombre_Aplicacion RETURN VARCHAR2 IS
  BEGIN
    IF VNomAplicacion IS NULL THEN
      VNomAplicacion := pks_sessiones.get_usuario_info(USER, 'UULTIMA_APP');
    END IF;
    RETURN(NVL(VNomAplicacion, 'Debe conectarse desde la MDI'));
  END;
  -- Retorna el nombre de la empresa donde esta instalada la aplicacisn.
  FUNCTION Nombre_Empresa RETURN VARCHAR2 IS
  BEGIN
    RETURN(NVL(VNomEmpresa, 'Debe conectarse desde la MDI'));
  END;

  PROCEDURE GUARDAR_PARAM_Tipo(NiSecTipo   IN NUMBER,
                               ViParametro IN VARCHAR2,
                               ViValor     IN VARCHAR2) IS
    CURSOR CParam IS
      SELECT 1
        FROM CON_PARAMETROS Para
       WHERE Para.Codigo = ViParametro;
    CURSOR CParCod(PSecTablaM IN NUMBER) IS
      SELECT Para.Codigo
        FROM CON_PARAMETROS Para, CON_DET_MODULOS Dmod
       WHERE Para.Detmo_Codigo = Dmod.Codigo AND
             UPPER(NVL(Para.Codigo_Alterno, Para.Titulo)) =
             UPPER(ViParametro) AND Dmod.Mod_Secuencia = PSecTablaM;
    CURSOR CSecTabla IS
      SELECT Tip.Mod_Sec
        FROM CON_TIPOS Tip
       WHERE Tip.Sec = NiSecTipo;
    ITemp      PLS_INTEGER;
    VCodParam  VARCHAR2(30) := ViParametro;
    NSecTablaM NUMBER;
  BEGIN
    IF ViParametro IS NULL OR NiSecTipo IS NULL THEN
      RETURN;
    END IF;
    OPEN CSecTabla;
    FETCH CSecTabla
      INTO NSecTablaM;
    IF CSecTabla%NOTFOUND THEN
      RAISE_APPLICATION_ERROR(-20752,
                              'No se encontro el registro de tipo al que se le quiere agregar el parámetro: ' ||
                              TO_CHAR(Nisectipo));
    END IF;
    CLOSE CSecTabla;
    OPEN CParam;
    FETCH CParam
      INTO ITemp;
    IF CParam%NOTFOUND THEN
      OPEN CParCod(NSecTablaM);
      FETCH CParCod
        INTO VCodParam;
      CLOSE CParCod;
      IF VCodParam IS NULL THEN
        RAISE_APPLICATION_ERROR(-20752,
                                'No pudo encontrarse un parámetro para la tabla tipo con nombre ' ||
                                ViParametro);
      END IF;
    END IF;
    CLOSE CParam;
    IF ViValor IS NULL THEN
      DELETE CON_CONFIG_TIPOS
       WHERE Param_Codigo = VCodParam AND Tipo_Sec = NiSecTipo;
    ELSE
      BEGIN
        INSERT INTO CON_CONFIG_TIPOS
          (PARAM_CODIGO, Tipo_Sec, VALOR)
        VALUES
          (VCodParam, NiSecTipo, ViValor);
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
          UPDATE CON_CONFIG_TIPOS
             SET Valor = ViValor
           WHERE Param_Codigo = VCodParam AND Tipo_Sec = NiSecTipo;
      END;
    END IF;
  END Guardar_Param_Tipo;

  -- Permite leer el contenido de un parametro de un registro de la tabla tipos.
  -- --------------------------------------
  FUNCTION PARAM_Tipo(NiSecTipo IN NUMBER, ViCodParam IN VARCHAR2)
    RETURN VARCHAR2 IS
    -- Define cursores.
    CURSOR CTipoParam IS
      SELECT Tipo, Condicion, Defecto
        FROM CON_PARAMETROS
       WHERE Codigo = UPPER(ViCodParam);
    CURSOR CParam IS
      SELECT NVL(Pm.Valor, p.Defecto)
        FROM CON_PARAMETROS p, CON_CONFIG_TIPOS Pm
       WHERE Pm.Param_Codigo(+) = p.Codigo AND Pm.Tipo_Sec = NiSecTipo AND
             p.Codigo = UPPER(ViCodParam);
    -- Define variables.
    VValor     CON_PARAMETROS_MODULOS.Valor%TYPE;
    VTipo      CON_PARAMETROS.Tipo%TYPE;
    VDefecto   CON_PARAMETROS.Defecto%TYPE;
    VCondicion CON_PARAMETROS.Condicion%TYPE;
  BEGIN
    IF ViCodParam IS NULL THEN
      RETURN(NULL);
    END IF;
    -- Verifica si es una fsrmula
    OPEN CTipoParam;
    FETCH CTipoParam
      INTO VTipo, VCondicion, VDefecto;
    CLOSE CTipoParam; --- JMARQUEZ 21/09/2004 se modifica para cerrar el cursor antes de retornar

    IF VTipo IS NULL THEN
      RETURN(NULL);
    END IF;

    -- Determina el valor del parametro.
    OPEN CParam;
    FETCH CParam
      INTO VValor;
    CLOSE CParam;
    RETURN(NVL(VValor, VDefecto));
  END Param_Tipo;

  -- Reversa el consecutivo recibido, sslo si es el zltimo.
  -- ------------------------------------------------------
  PROCEDURE Reversar_Code(ViNombre IN VARCHAR2, NiSec IN NUMBER) IS
    SAVE_ROWID CHAR(18);
    CURSOR C IS
      SELECT CC_NEXT_VALUE, ROWID
        FROM CG_CODE_CONTROLS
       WHERE CC_DOMAIN = ViNombre
         FOR UPDATE OF CC_NEXT_VALUE NOWAIT;
    NSecActual NUMBER;
    BContinuar BOOLEAN := TRUE;
    NIntentos  NUMBER := 100;
  BEGIN
    WHILE BContinuar AND NIntentos <= 100 LOOP
      BEGIN
        OPEN C;
        FETCH C
          INTO NSecActual, SAVE_ROWID;
        IF C%NOTFOUND THEN
          CLOSE C;
          RAISE NO_DATA_FOUND;
        END IF;
        CLOSE C;
        IF NiSec = NSecActual - 1 THEN
          UPDATE CG_CODE_CONTROLS
             SET CC_NEXT_VALUE = CC_NEXT_VALUE - 1
           WHERE ROWID = SAVE_ROWID;
        END IF;
        BContinuar := FALSE;
      EXCEPTION
        WHEN OTHERS THEN
          IF SQLCODE = -54 THEN
            NIntentos := NIntentos + 1;
          ELSE
            RAISE_APPLICATION_ERROR(-20750,
                                    'Error al reversar la secuencia "' ||
                                    ViNombre || '" ' || SQLERRM);
          END IF;
      END;
      IF NIntentos > 100 THEN
        RAISE_APPLICATION_ERROR(-20751,
                                'No fue posible devolver la secuencia. Intente la operación en unos minutos.');
      END IF;
    END LOOP;
  END Reversar_Code;

  -- Genera la siguiente secuencia para una tabla.
  -- ----------------------------------------------
  PROCEDURE CODE_CONTROLS(ViNombre IN VARCHAR2, NoSec IN OUT NUMBER) IS
    SAVE_ROWID CHAR(18);
    CURSOR C IS
      SELECT CC_NEXT_VALUE, ROWID
        FROM CG_CODE_CONTROLS
       WHERE UPPER(CC_DOMAIN) = UPPER(ViNombre)
         FOR UPDATE OF CC_NEXT_VALUE NOWAIT;
    BContinuar BOOLEAN := TRUE;
    NIntentos  NUMBER := 1;
  BEGIN
    IF ViNombre IS NULL THEN
      RAISE_APPLICATION_ERROR(-20750,
                              'Pk_Conseres.code_controls: debe especificar el nombre de la secuencia.');
    ELSE
      WHILE BContinuar AND NIntentos <= 100 LOOP
        BEGIN
          OPEN C;
          FETCH C
            INTO NoSec, SAVE_ROWID;
          IF C%NOTFOUND THEN
            CLOSE C;
            RAISE_APPLICATION_ERROR(-20750,
                                    'La secuencia "' || ViNombre ||
                                    '" no existe en cg_code_controls');
          END IF;
          CLOSE C;
          UPDATE CG_CODE_CONTROLS
             SET CC_NEXT_VALUE = CC_NEXT_VALUE + NVL(CC_INCREMENT, 1)
           WHERE ROWID = SAVE_ROWID;
          
          BContinuar := FALSE;
          --
          -- 37635 (M) JACEVEDO 20150528 - Se controla en ACTIVO-ACTIVO que la secuencia corresponda a PAR o IMPAR
          -- Debe ser IMPAR (PRINCIPAL)
          If F_GLOBAL_NAME = pks_replicacion.F_NODO_PRINCIPAL and mod(NoSec,2) <> 0 then
             BContinuar := true;
          -- Debe ser PAR (SECUNDARIA)
          ElsIf F_GLOBAL_NAME = pks_replicacion.F_NODO_SECUNDARIO and mod(NoSec,2) = 0 then
             BContinuar := true;
          End If;
          -- 37635 Linea Modificada
          --
          
        EXCEPTION
          WHEN OTHERS THEN
            IF SQLCODE = -54 THEN
              NIntentos := NIntentos + 1;
            ELSE
              RAISE_APPLICATION_ERROR(-20751,
                                      'Error al asignar la secuencia "' ||
                                      ViNombre || '" ' || SQLERRM);
            END IF;
        END;
        IF NIntentos > 100 THEN
          RAISE_APPLICATION_ERROR(-20751,
                                  'No fue posible asignar Próxima Secuencia. Intente la operación en unos minutos');
        END IF;
      END LOOP;
    END IF;
  END CODE_CONTROLS;

  -- Suma una cantidad de dias habiles a una fecha y retorna la fecha resultante.
  -- ---------------------------------------------------------
  FUNCTION Sumar_Habiles(DiInicial IN DATE, IiCant PLS_INTEGER) RETURN DATE IS
    ICantReal   PLS_INTEGER := 0;
    DFinal      DATE := DiInicial;
    ICantLimite PLS_INTEGER := IiCant;
  BEGIN
    IF ICantLimite = 0 AND NOT Es_Laborable(DFinal) THEN
      ICantLimite := 1;
    ELSIF ICantLimite = 0 AND Es_Laborable(DFinal) THEN
      RETURN(DFinal);
    END IF;
    LOOP
      IF Es_Laborable(DFinal + 1) THEN
        ICantReal := ICantReal + 1;
      END IF;
      DFinal := DFinal + 1;
      EXIT WHEN ICantReal >= ICantLimite;
    END LOOP;
    RETURN(DFinal);
  END Sumar_Habiles;

  -- Retorna lo festivos definidos entre dos fechas
  -- --------------------------------------------------
  FUNCTION Festivos(DiInicial IN DATE, DiFinal IN DATE) RETURN VARCHAR2 IS
    CURSOR CFest IS
      SELECT Fecha, Nombre
        FROM Con_Vfestivos
       WHERE TRUNC(Fecha) BETWEEN TRUNC(DiInicial) AND TRUNC(DiFinal);
    VFestivos VARCHAR2(2000) := NULL;
  BEGIN
    FOR RFest IN CFest LOOP
      VFestivos := VFestivos || TO_CHAR(RFest.Fecha, 'mmdd') || ',"' ||
                   Rfest.Nombre || '"';
    END LOOP;
    RETURN(VFestivos);
  END Festivos;

  -- Retorna True si la fecha es laborable (No es festivo ni sabado)
  -- ----------------------------------
  FUNCTION Es_Laborable(DiFecha           IN DATE,
                        BiSabadoLaborable IN BOOLEAN := NULL) RETURN BOOLEAN IS
    CURSOR CFest(PdFecha IN DATE) IS
      SELECT '1'
        FROM Con_Vfestivos
       WHERE TRUNC(Fecha) = TRUNC(PdFecha);
    CURSOR CParam IS
      SELECT UPPER(Valor)
        FROM CON_PARAMETROS_MODULOS
       WHERE Param_Codigo = 'GSABADO_LABORABLE'
       AND   NVL(FECHA_FINAL,SYSDATE) >= SYSDATE
       AND   FECHA_INICIAL <= SYSDATE;
    VValor  VARCHAR2(5);
    VNombre VARCHAR2(60);
  BEGIN
    -- Si es domingo ...
    IF TO_CHAR(DiFecha, 'd') = '1' THEN
      RETURN(FALSE);
    END IF;
    -- Si es sabado y el sabado no es laborable
    IF TO_CHAR(DiFecha, 'd') = '7' THEN
      IF BiSabadoLaborable IS NULL THEN
        OPEN Cparam;
        FETCH CParam
          INTO VValor;
        CLOSE Cparam;
        IF NVL(VValor, 'NO') = 'NO' THEN
          RETURN(FALSE);
        END IF;
      ELSE
        IF NOT BiSabadoLaborable THEN
          RETURN(FALSE);
        END IF;
      END IF;
    END IF;
    OPEN CFest(Difecha);
    FETCH CFest
      INTO VNombre;
    IF CFest%FOUND THEN
      CLOSE CFest;
      RETURN(FALSE);
    END IF;
    CLOSE CFest;
    RETURN(TRUE);
  END;
  -- Retorna el nombre de un tipo dada su secuencia.
  -- -------------------------------------
  FUNCTION Nombre_Tipo(NiSecTipo IN NUMBER) RETURN VARCHAR2 IS
    CURSOR CTipo IS
      SELECT Nombre
        FROM CON_TIPOS
       WHERE Sec = NisecTipo;
    VNombre VARCHAR2(60);
  BEGIN
    IF NiSecTipo IS NULL THEN
      RETURN(NULL);
    END IF;
    OPEN CTipo;
    FETCH CTipo
      INTO VNombre;
    CLOSE CTipo;
    RETURN(VNombre);
  END Nombre_Tipo;

  PROCEDURE Registrar(ViModulo IN VARCHAR2, ViAccion IN VARCHAR2) IS
    IResul PLS_INTEGER;
    VError VARCHAR2(500);
  BEGIN
    Dbms_Application_Info.Set_Action(SUBSTR(ViModulo || '/' || ViAccion,
                                            1,
                                            34));
  END Registrar;

  --
  -- Retorna true si la condicisn recibida se cumple.
  FUNCTION Cumple_Condicion(ViCondicion IN VARCHAR2,
                            VoError     IN OUT VARCHAR2) RETURN BOOLEAN IS
    VSql   VARCHAR2(2000);
    BResul BOOLEAN;
    IResul PLS_INTEGER;
    TYPE RcGen IS REF CURSOR;
    CGen RcGen;
  BEGIN
    IF ViCondicion IS NULL THEN
      VoError := 'Condición vacía';
      RETURN FALSE;
    END IF;
    VSql := 'SELECT 1 FROM dual WHERE ' || ViCondicion;
    OPEN CGen FOR VSql;
    FETCH CGen
      INTO IResul;
    IF CGen%FOUND THEN
      BResul := TRUE;
    ELSE
      BResul := FALSE;
    END IF;
    CLOSE CGen;
    RETURN(BResul);
  EXCEPTION
    WHEN OTHERS THEN
      VoError := VoError || ' Error en condición "' || ViCondicion || '">>' ||
                 SUBSTR(SQLERRM, 1, 255);
      RETURN(FALSE);
  END Cumple_Condicion;
  -- Evalua una fsrmula.
  FUNCTION Evaluar_Expresion(ViExpresion IN VARCHAR2,
                             VoError     IN OUT VARCHAR2) RETURN VARCHAR2 IS
    VSql        VARCHAR2(2000);
    IResul      INTEGER;
    Cursor_Name INTEGER;
    VValor      VARCHAR2(2000);
  BEGIN
    -- Construye la instruccion SELECT con la fsrmula.
    VSql        := 'SELECT ' || ViExpresion || ' valor FROM DUAL';
    Cursor_Name := DBMS_SQL.OPEN_CURSOR;
    DBMS_SQL.PARSE(Cursor_Name, VSQL, DBMS_SQL.V7);
    DBMS_SQL.DEFINE_COLUMN(Cursor_Name, 1, VValor, 2000);
    -- Ejecuta el CURSOR y trae el valor;
    IResul := DBMS_SQL.EXECUTE(Cursor_Name);
    IF DBMS_SQL.FETCH_ROWS(Cursor_Name) > 0 THEN
      DBMS_SQL.COLUMN_VALUE(Cursor_Name, 1, VValor);
    ELSE
      VValor := NULL;
    END IF;
    DBMS_SQL.CLOSE_CURSOR(Cursor_Name);
    RETURN(VValor);
  EXCEPTION
    WHEN OTHERS THEN
      VoError := VoError || ' Error en Expresión "' || ViExpresion || '">>' ||
                 SUBSTR(SQLERRM, 1, 255);
      IF Dbms_SQL.Is_Open(Cursor_Name) THEN
        DBMS_SQL.CLOSE_CURSOR(Cursor_Name);
      END IF;
      RETURN(NULL);
  END Evaluar_Expresion;

  --
  -- Evalua una fsrmula de parametros.
  FUNCTION Evaluar_Formula(ViFormula IN VARCHAR2) RETURN VARCHAR2 IS
    VFormula  CON_PARAMETROS.Condicion%TYPE := ViFormula;
    NPos1     NUMBER;
    NPos2     NUMBER;
    VCodParam VARCHAR2(1000);
    VValParam VARCHAR2(1000);
    VError    VARCHAR2(300);
  BEGIN
    -- Busca las referencias a otros parametros y los reemplaza por su valor
    -- estas referencias se escriben entre el signo: 
    LOOP
      NPos1 := INSTR(VFormula, '');
      IF NPos1 = 0 THEN
        EXIT;
      END IF;
      NPos2 := INSTR(VFormula, '', NPos1 + 1);
      IF Npos2 = 0 THEN
        RETURN(NULL);
      END IF;
      VCodParam := LTRIM(RTRIM(SUBSTR(VFormula,
                                      Npos1 + 1,
                                      NPos2 - Npos1 - 1)));
      VValParam := NVL(Param_Gnral(VCodParam), '0');
      IF VValParam IS NULL THEN
        VValParam := '0';
      END IF;
      VFormula := LTRIM(RTRIM(SUBSTR(VFormula, 1, Npos1 - 1))) || VValParam ||
                  LTRIM(RTRIM(SUBSTR(VFormula, Npos2 + 1)));
    END LOOP;
    RETURN(Evaluar_Expresion(VFormula, VError));
  END Evaluar_Formula;
  --
  -- Permite leer el contenido de un parametro general a una fecha de corte dada.
  FUNCTION PARAM_HIS(ViCodParam IN VARCHAR2, DiCorte IN DATE := SYSDATE) RETURN VARCHAR2 IS
    -- Define cursores.
    CURSOR CTipoParam IS
      SELECT Tipo, Condicion, Defecto
        FROM CON_PARAMETROS
       WHERE Codigo = UPPER(ViCodParam);

    /* Busca si existe un parametro configurado para la empresa correspondiente a la aplicacion
    actual y si no existe toma el valor por defecto */

    CURSOR CParam(VSecEmp VARCHAR2) IS
      SELECT NVL(Pm.Valor, p.Defecto)
        FROM CON_PARAMETROS p, CON_PARAMETROS_MODULOS Pm
       WHERE p.Codigo = UPPER(ViCodParam)
	     AND p.mod_sec = vSecEmp
	     AND Pm.Param_Codigo(+) = p.Codigo
	     AND Pm.mod_secuencia (+) = p.mod_sec
         AND (DiCorte BETWEEN Pm.Fecha_Inicial AND NVL(Pm.Fecha_Final, SYSDATE + 1)
		        OR pm.param_codigo IS NULL);

    /* Busca si existe un parametro configurado para otra empresa, en caso de no encontrar para la empresa actual */

    CURSOR CParam2 IS
      SELECT NVL(Pm.Valor, p.Defecto)
        FROM CON_PARAMETROS p, CON_PARAMETROS_MODULOS Pm
       WHERE p.Codigo = UPPER(ViCodParam)
	     AND Pm.Param_Codigo(+) = p.Codigo
	     AND Pm.mod_secuencia (+) = p.mod_sec
         AND (DiCorte BETWEEN Pm.Fecha_Inicial AND NVL(Pm.Fecha_Final, SYSDATE + 1)
		        OR pm.param_codigo IS NULL);

	-- Define variables.
    VValor     CON_PARAMETROS_MODULOS.Valor%TYPE;
    VTipo      CON_PARAMETROS.Tipo%TYPE;
    VCondicion CON_PARAMETROS.Condicion%TYPE;
    VDefecto   CON_PARAMETROS.Defecto%TYPE;
	VSecuencia VARCHAR2(10);
	BEncontrado BOOLEAN;
  BEGIN
    VSecuencia := pks_sessiones.Get_SecEmp;

    IF ViCodParam IS NOT NULL THEN
      -- Verifica si es una formula
      OPEN CTipoParam;
      FETCH CTipoParam
        INTO VTipo, VCondicion, VDefecto;
      CLOSE CTipoParam;

      IF VTipo = 'F' THEN
        --vValor := Evaluar_formula(vCondicion);
        NULL;
      ELSIF VTipo = 'S' THEN
        VValor := '-----------------------------------';
      ELSE
        -- Determina el valor del parametro.
        OPEN CParam(VSecuencia);
        FETCH CParam INTO VValor;
		BEncontrado := CParam%FOUND;
        CLOSE CParam;

		/*
		   Si no encuentra el parametro para la empresa actual, lo busca en otra empresa
		   Esto se hace por el funcionamiento de los modulos compartidos, ya que deben funcionar,
		   si involucra parametros generales, asi no esten definidos para la empresa actual.
		*/
		IF NOT BEncontrado THEN
		  OPEN CParam2;
		  FETCH CParam2 INTO VValor;
		  CLOSE CParam2;
		END IF;
      END IF;
    END IF;
    RETURN(NVL(VValor, VDefecto));
  END PARAM_HIS;
  --
  -- Guarda el valor de un parametro general.
  PROCEDURE Guardar_Param(ViParametro IN VARCHAR2,
                          ViModulo    IN VARCHAR2,
                          ViValor     IN VARCHAR2) IS
  BEGIN
    IF ViParametro IS NOT NULL AND ViModulo IS NOT NULL THEN
      IF ViValor IS NULL THEN
        DELETE CON_PARAMETROS_MODULOS
         WHERE Param_Codigo = ViParametro AND Mod_Secuencia = ViModulo;
      ELSE
        UPDATE CON_PARAMETROS_MODULOS
           SET Valor = Vivalor
         WHERE Param_Codigo = ViParametro AND Mod_Secuencia = ViModulo;

        IF SQL%NOTFOUND THEN
          INSERT INTO CON_PARAMETROS_MODULOS
            (PARAM_CODIGO,
             MOD_SECUENCIA,
             FECHA_CREACION,
             Creado_Por,
             VALOR)
          VALUES
            (ViParametro, ViModulo, SYSDATE, UPPER(USER), ViValor);
        END IF;
      END IF;
    END IF;
  END Guardar_Param;
  --
  -- Guarda mensajes para los usuarios que tienen privilegio en un msdulo.
  PROCEDURE Alerta(ViMensaje IN VARCHAR2, ViNomModulo IN VARCHAR2) IS
    CURSOR CProxima IS

    -- JHGUTIER 25-09-2004. Se le generó una secuencia a la tabla, debido a que
    -- la forma como la estaba haciendo genera bloqueo con usuarios concurrentes
    -- 37635 (M) JACEVEDO 20151203 - Se controla en ACTIVO-ACTIVO que la secuencia corresponda a PAR o IMPAR
    --  SELECT Ss_Con_Alertas.NEXTVAL
      SELECT PK_UTIL.Proxima_sec_Oracle('SS_CON_ALERTAS')
        FROM Dual;
    -- 37635 Linea Final
            
  /*
    CURSOR CUsu IS
      SELECT Permi.Usu_Usuario, Descripcion
        FROM CON_PERMISOS Permi, CON_MODULOS Modul
       WHERE Permi.Mod_Secuencia = Modul.Secuencia AND
             UPPER(Modul.Nombre) = UPPER(ViNomModulo);
    VSec VARCHAR2(30);
  BEGIN
    FOR CdUsu IN CUsu LOOP
      OPEN CProxima;
      FETCH CProxima
        INTO VSec;
      CLOSE CProxima;
      INSERT INTO CON_ALERTAS
        (Secuencia,
         Mensaje,
         Tema,
         Usu_Usuario,
         Usu_Usuario_Enviar,
         Mostrar,
         Recibido_En,
         Enviado_En)
      VALUES
        (VSec,
         ViMensaje,
         SUBSTR(CdUsu.Descripcion, 1, 30),
         CdUsu.Usu_Usuario,
         UPPER(USER),
         'S',
         NULL,
         SYSDATE);
    END LOOP;
	*/
  BEGIN
    NULL;
  END Alerta;

  PROCEDURE Alerta_Usuario(ViMensaje IN VARCHAR2,
                           ViTema    IN VARCHAR2,
                           ViUsuario IN VARCHAR2,
                           VioSec    IN OUT VARCHAR2) IS
    -- JHGUTIER 25-09-2004. Se le generó una secuencia a la tabla, debido a que
    -- la forma como la estaba haciendo genera bloqueo con usuarios concurrentes
    CURSOR CProxima IS
    -- 37635 (M) JACEVEDO 20151203 - Se controla en ACTIVO-ACTIVO que la secuencia corresponda a PAR o IMPAR
    --  SELECT Ss_Con_Alertas.NEXTVAL
      SELECT PK_UTIL.Proxima_sec_Oracle('SS_CON_ALERTAS')
        FROM Dual;
    -- 37635 Linea Final
    CURSOR CUsua IS
      SELECT '1'
        FROM CON_USUARIOS Usua
       WHERE Usua.Usuario = UPPER(ViUsuario);
    VTemp VARCHAR2(1);
  BEGIN
    OPEN CUsua;
    FETCH CUsua
      INTO VTemp;
    IF CUsua%NOTFOUND THEN
      CLOSE CUsua; --- JMARQUEZ 21/09/2004 Cierra el cursor antes de disparar el error
      RAISE_APPLICATION_ERROR(-20710, 'El usuario especificado no existe.');
    END IF;
    CLOSE CUsua;
    IF VioSec IS NULL THEN
      OPEN CProxima;
      FETCH CProxima
        INTO VioSec;
      CLOSE CProxima;
      INSERT INTO CON_ALERTAS
        (Secuencia,
         Mensaje,
         Tema,
         Usu_Usuario,
         Usu_Usuario_Enviar,
         Mostrar,
         Enviado_En)
      VALUES
        (VioSec,
         ViMensaje,
         SUBSTR(ViTema, 1, 30),
         UPPER(ViUsuario),
         UPPER(USER),
         'S',
         SYSDATE);
    ELSE
      -- Actualiza la que ya existe
      UPDATE CON_ALERTAS
         SET Mensaje = Mensaje || CHR(10) || ViMensaje
       WHERE Secuencia = VioSec;
    END IF;
  END Alerta_Usuario;
  --
  -- Permite leer el contenido de un parametro general.
  FUNCTION PARAM(ViCodParam IN VARCHAR2) RETURN VARCHAR2 IS
  BEGIN
    RETURN(Param_His(ViCodParam, SYSDATE));
  END PARAM;

  FUNCTION PARAM_GNRAL(ViCodParam IN VARCHAR2) RETURN VARCHAR2 IS
  BEGIN
    RETURN(Param_HIS(ViCodParam, SYSDATE));
  END PARAM_GNRAL;

  -- Permite leer el contenido de un parametro general sin incluir tipo de parametros tipo formula.
  FUNCTION PARAM_GENERAL(ViCodParam IN VARCHAR2) RETURN VARCHAR2 IS
  BEGIN
    RETURN(Param_HIS(ViCodParam, SYSDATE));
  END PARAM_GENERAL;

  FUNCTION Ejecutar_Procedimiento(ViProc  IN VARCHAR2,
                                  VoError IN OUT VARCHAR2) RETURN INTEGER IS
    VProc  VARCHAR2(2000) := ViProc;
    NResul NUMBER;
  BEGIN
    VProc := 'Begin' || CHR(10) || '  ' || VProc || ';' || CHR(10) ||
             'End;';
    -- Ejecuta el procedimiento.
    NResul := Ejecutar_Sql(VProc, VoError);
    RETURN(NResul);
  EXCEPTION
    WHEN OTHERS THEN
      VoError := VoError || ' ' || SUBSTR(SQLERRM, 1, 255);
      RETURN(0);
  END Ejecutar_Procedimiento;
  
  
  FUNCTION ejecutar_query ( viQuery IN VARCHAR2 ) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: ejecutar_query
      PROPOSITO:
        Ejecutar query dinámico con el fin de retornar un único valor
      PARAMETROS:
      viQuery IN VARCHAR2 Sentencia a ejecutar

      HISTORIAL
      Fecha                          Usuario                 Versión           Descripción
      2012/05/26                     JNARANJO                1.0.0             1. Creación del programa Req. 27965
      REQUISITOS:
      NOTAS:
    ****************************************************************************************************/
    TYPE refcursor IS REF CURSOR;
    rQuery refcursor;
    vNombre VARCHAR2(2000);
  BEGIN
    EXECUTE IMMEDIATE viQuery INTO vNombre;
    RETURN vNombre;   
  EXCEPTION
    WHEN OTHERS THEN
         pks_case_exception.raise_app_exception( vi_msj => 'Error al ejecutar consulta.',
                                                 vi_errorTecnico => SQLCODE||' '||SQLERRM );
  END;    
  
END Pk_Conseres;

 

 
