package tc;
import java.util.*;
public class Objeto{
	public String nombre;
	private HashMap<String,String>h;
	private HashMap<String,ArrayList<String>>l;
	private ArrayList<Objeto>os;
	public Objeto(){
		h=new HashMap<String,String>();
		l=new HashMap<>();
		os=new ArrayList<Objeto>();
	}
	public Objeto(String nombre){
		this();
		this.nombre=nombre;
	}
	public String toString(){
		return this.nombre+":"+h+"->"+os;
	}
	public void put(String k,String v){
		h.put(k,v);
	}
	public String get(String k){
		return h.get(k);
	}
	public void concatenar(String k,String v){
		if(l.get(k)!=null){
			ArrayList<String>lx=l.get(k);
			lx.set(lx.size()-1,lx.get(lx.size()-1)+v);
			return;
		}
		if(h.get(k)==null){
			h.put(k,"");
		}
		h.put(k,h.get(k)+v);
	}
	public void insertar(String k,String v){
		if(l.get(k)==null){
			l.put(k,new ArrayList<String>());
			l.get(k).add("");
		}
		l.get(k).set(l.get(k).size()-1,l.get(k).get(l.get(k).size()-1)+v);
	}
	public void agregarObjeto(Objeto o){
		os.add(o);
	}
	public Objeto obtenerObjeto(int i){
		return os.get(i);
	}
	public Objeto obtenerObjeto(){
		return os.get(os.size()-1);
	}
}