package tc;
import java.util.*;
import java.io.*;
import o.*;
public class Parser{
	public Formato formato;
	HashMap<String,Estado>hash;
	ArrayList<String[]>transversales;
	public boolean u=false;
	ArrayList<Operador>operadores;
	HashMap<String,Estado>expresiones;
	public Parser(){
		hash=new HashMap<String,Estado>();
		transversales=new ArrayList<>();
		operadores=new ArrayList<>();
		formato=new Formato();
		formato.hash.put("formato",formato);
		formato.hash.put("parser",this);
		expresiones=new HashMap<>();
	}
	public void setU(){
		this.u=true;
	}
	public void crearEstado(String nombre){
		if(hash.get(nombre)==null){
			hash.put(nombre,new Estado(nombre,this.u));
		}
	}
	public void addt(String estado,String tokens){
		crearEstado(estado);
		hash.get(estado).add(tokens,"pop");
	}
	public void addt(String estado,String tokens,String nuevoEstado){
		crearEstado(estado);
		crearEstado(nuevoEstado);
		hash.get(estado).add(tokens,nuevoEstado);
	}
	public void addt(String estado,String tokens,String nuevoEstado,String objeto){
		addt(estado,tokens,nuevoEstado);
		hash.get(nuevoEstado).crear=true;
		hash.get(nuevoEstado).objeto=objeto;
	}
	public void addp(String estado,String nombre,String nuevoEstado){
		addt(estado,"#",nuevoEstado);
		//hash.get(estado).propiedad=nombre;
		hash.get(estado).hp.put("#",nombre);
	}
	public void addp(String estado,String nombre){
		addp(estado,nombre,"pop");
	}
	public void addp(String estado,String token,String nuevoEstado,String objeto,String propiedad){
		addt(estado,token,nuevoEstado);
		hash.get(nuevoEstado).crear=true;
		hash.get(nuevoEstado).objeto=objeto;
		//hash.get(nuevoEstado).propiedad=propiedad;
		hash.get(estado).hp.put(token,propiedad);
	}
	public void ignorar(String estado,String cadena){
		c.U.imp("ignorar->estado:"+estado+" cadena:"+cadena);
	}
	public void transversal(String estado,String inicio,String fin){
		c.U.imp("transversal->estado:"+estado+" inicio:"+inicio+" fin:"+fin);
		pop(estado,fin);
		hash.get(estado).transversal=true;
		transversales.add(new String[]{inicio,estado});
	}
	public void excepcion(String origen,String destino){
		c.U.imp("excepcion->origen:"+origen+" destino:"+destino);
	}
	public void procesar(File f)throws Exception{
		this.formato.hash.put("file",f);
		ArrayList<Token>ts=Tokenizer.leer(new FileInputStream(f));
		procesar(ts);
	}
	public void push(String estado,String tokens,String nuevoEstado){
		crearEstado(estado);
		crearEstado(nuevoEstado);
		hash.get(estado).add(tokens,nuevoEstado);
	}
	public void pop(String estado,String tokens){
		crearEstado(estado);
		hash.get(estado).add(tokens,"pop");
	}
	public void pop(String estado,String tokens,int c){
		crearEstado(estado);
		hash.get(estado).add(tokens,"pop|"+c);
	}
	public void aci(String estado,String comandoInicio){
		hash.get(estado).comandoInicio=comandoInicio.replaceAll("-"," ").replaceAll("/","\n");
	}
	public void acc(String estado,String comandoContinuo){
		hash.get(estado).comandoContinuo=comandoContinuo.replaceAll("-"," ").replaceAll("/","\n");
	}
	public void imp(Object m){c.U.traza();
		c.U.imp("Parser imprimiendo:"+m);
	}
	public void operador(String o,int t,int p){
		operadores.add(new Operador(o,t,p));
	}
	public void expresion(String e){
		expresiones.put(e,hash.get(e));
	}
	public void procesar(ArrayList<Token>ts)throws Exception{
		formato.hash.put("tokens",ts);
		Stack<Estado>s=new Stack<Estado>();
		s.push(hash.get("INICIO"));
		Objeto objeto=new Objeto("Programa");
		for(int j=0;j<ts.size();){
			for(int k=0;k<transversales.size();k++){
				if(!s.peek().transversal){
					push(s.peek().nombre,transversales.get(k)[0],transversales.get(k)[1]);
				}
			}
			c.U.imp(s.peek()+""+s.size()+":"+j+":"+s.peek().h+"("+objeto+")---"+ts.get(j));
			formato.hash.put("token",ts.get(j));
			formato.hash.put("indice",j);
			formato.ins(s.peek().comandoContinuo);
			P ns=s.peek().a(ts,j);
			if(ns==null){
				j++;
				continue;
			}
			if(ns.v.equals("=")){
				j+=ns.i;
				continue;
			}
			if(ns.v.equals("pop")){
				int cpop=ns.cpop;
				for(int i=0;i<cpop;i++){
					s.pop();
				}
				j+=ns.i;
				continue;
			}
			Estado ne=hash.get(ns.v);
			if(ne!=null){
				formato.ins(ne.comandoInicio);
				if(ns.x){
					s.pop();
				}
				s.push(ne);
				if(ne.crear){
					objeto.agregarObjeto(new Objeto(ne.objeto));
					objeto=objeto.obtenerObjeto();
				}
			}
			j+=ns.i;
		}
	}
}