package tc;
import java.util.*;
public class Estado{
	public String nombre;
	public HashMap<String,String>h;
	public int t;
	public boolean crear=false;
	public String objeto;
	public ArrayList<String>ngs;
	public HashMap<String,String>hp;
	public String comandoInicio;
	public String comandoContinuo;
	public boolean transversal;
	public static int gid=0;
	public int id;
	public boolean u=false;
	public Estado(){
		h=new HashMap<String,String>();
		ngs=new ArrayList<String>();
		hp=new HashMap<String,String>();
		this.id=gid++;
	}
	public Estado(String nombre){
		this();
		this.nombre=nombre;
	}
	public Estado(String nombre,boolean u){
		this();
		this.nombre=nombre;
		this.u=u;
	}
	public void add(String tokens,String nuevoEstado){
		int tx=tokens.split("#").length;
		if(tx>this.t){
			this.t=tx;
		}
		if(tokens.startsWith("!")){
			ngs.add(tokens);
		}
		h.put(tokens,nuevoEstado);
	}
	public String o(ArrayList<Token>ts,int i,int tx){
		StringBuilder r=new StringBuilder();
		for(int j=i;j<i+tx&&j<ts.size();j++){
			if(j>i){
				r.append("#");
			}
			r.append(ts.get(j).v);
		}
		return r+"";
	}
	public boolean esIdentificador(String v){
		char[]cs=v.toCharArray();
		for(int i=0;i<cs.length;i++){
			if(!Character.isLetterOrDigit(cs[i])){
				return false;
			}
		}
		return true;
	}
	public void ap(String token,String valor){
		if(this.hp.get(token)!=null){
			//this.o.h.put(this.hp.get(token),valor);
		}
	}
	public P a(final ArrayList<Token>ts,int j){
		for(int i=this.t;i>0;i--){
			String k=o(ts,j,i);
			String ns=this.get(k);
			if(ns!=null){
				int n=1;
				if(ns.startsWith("pop|")){
					try{
						n=Integer.parseInt(ns.substring(4));
						ns="pop";
					}catch(Throwable t){
					}
				}
				return new P(ns,i,n,false);
			}
		}
		if(this.get("#")!=null){
			this.ap("#",ts.get(j).v);
			return new P(this.get("#"),1,1,false);
		}
		for(int i=0;i<ngs.size();i++){
			if(!ts.get(j).v.equals(ngs.get(i).substring(1))){
				this.ap(ngs.get(i),ts.get(j).v);
				return new P(this.get(ngs.get(i)),1,1,false);
			}
		}
		if(esIdentificador(ts.get(j).v)){
			if(this.get("%I")!=null){
				this.ap("%I",ts.get(j).v);
				return new P(this.get("%I"),1,1,false);
			}
		}
		return null;
	}
	public String toString(){
		return this.nombre+"["+this.id+"]";
	}
	public String get(String k){
		if(u){
			k=k.toUpperCase();
		}
		return h.get(k);
	}
}