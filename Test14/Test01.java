import java.io.*;
import java.util.*;
import java.math.*;
public class Test01{
	public static PrintStream ps;
	public static int cxz(double d){
		java.text.DecimalFormat f=new java.text.DecimalFormat("######################################################.######################################################################");
		return cxz(f.format(d));
	}
	public static int cxz(String s){
		String x=null;ps.println("Entradacxz:"+s);
		try{
			x=s.split("\\.")[1];
		}catch(Exception e){
			return 0;
		}
		int c=0;
		for(int i=x.length()-1;i>=0;i--){
			if(x.charAt(i)=='0'){
				c++;
			}else{
				break;
			}
		}
		return x.length()-c;
	}
	public static double generar(double n,int a,int b){
		if(b==0){
			return n;
		}
		BigDecimal d=new BigDecimal(n);
		d=d.setScale(b,BigDecimal.ROUND_HALF_EVEN);
		String s=d+"";ps.println("d1:"+s);
		if(s.charAt(s.length()-1)=='0'){
			d=new BigDecimal(s.substring(0,s.length()-1)+"1");
		}
		if(s.charAt(s.length()-1)=='1'&&cxz(d.doubleValue())!=b){
			d=new BigDecimal(s.substring(0,s.length()-1)+"2");
		}
		ps.println("d2:"+d);
		double r=d.doubleValue();
		ps.println("r?:"+r);
		if(cxz(r)!=b){
			//xseedRts.XseedFunctions.log("\nHL:Error redondeo:{"+b+"}:"+n+"->"+r);
			ps.println("HL:Error redondeo["+cxz(r)+"]:{"+b+"}:"+n+"->"+r);
		}
		//xseedRts.XseedFunctions.log("\nHL{"+b+"}:"+n+"->"+r);
		return r;
	}
	public static void main(String[]argumentos){
		try{
			ps=new PrintStream(new FileOutputStream(new File("Test.txt")));
			//ps.println("cxz:"+cxz("zzz.0223000"));
			//ps.println("cxz:"+cxz(1.0E-10));
			//double r=2./3.;
			//double r=1.0E-3;
			//double r=0.120;
			//double r=1.587254648378119E10;
			//double r=9.14610280950641E9;
			//double r=5.77715968390782E8;
			double r=9.14610280950641E9;
			ps.println("cxz:"+cxz(r));
			ps.println("r:"+generar(r,1,6));
		}catch(Throwable t){
			ps.println(t);
			t.printStackTrace();
			t.printStackTrace(ps);
			//t.printStackTrace(new PrintStream(new FileOutputStream(new File("D:\\Desarrollo\\Java\\JavaText\\01\\LogC40.txt"))));
			try{
				t.printStackTrace(new PrintStream(new FileOutputStream(new File("D:\\Innovation\\COMPRASE\\Audit\\LogC40.txt"))));
			}catch(Exception expX){
			}
		}
	}
}