import java.util.*;
import java.io.*;
public class CrearDatos{
	public static void main(String[]args){
		String tablaError="";
		String campoError="";
		String tipoError="";
		try{
			PrintStream ps=new PrintStream(new FileOutputStream(new File("BaseDatos.js"),false));
			HashMap<String,String>tablas=new HashMap<String,String>();
			ArrayList<String>listaTablas=new ArrayList<String>();
			String resultado="";
			Scanner r=new Scanner(new File("Referencias.txt"));
			Scanner c=new Scanner(new File("Columnas.txt"));
			String codigoTablas="";
			HashMap<String,Integer>tipos=new HashMap<String,Integer>();
			tipos.put("VARCHAR2",1);
			tipos.put("DATE",1);
			tipos.put("NUMBER",1);
			tipos.put("RAW",1);
			tipos.put("LONG",1);
			tipos.put("LONGRAW",1);
			tipos.put("CHAR",1);
			while(r.hasNext()){
				String tabla=r.next();
				if(tablas.get(tabla)==null){
					tablas.put(tabla,tabla);
					listaTablas.add(tabla);
				}
				String tablaPadre=r.next();
				if(tablas.get(tablaPadre)==null){
					tablas.put(tablaPadre,tablaPadre);
					listaTablas.add(tablaPadre);
				}
				String restriccion=r.next();
				resultado+=tabla+".add("+tablaPadre+");\n";
			}
			for(int i=0;i<listaTablas.size();i++){
				codigoTablas+="var "+listaTablas.get(i)+"=new Tabla('"+listaTablas.get(i)+"');\n";
			}
			while(c.hasNext()){
				String tabla=c.next();
				tablaError=tabla;
				String campo=c.next();
				campoError=campo;
				String tipo=c.next();
				tipoError=tipo;
				resultado+=tabla+".agregarCampo(new Campo('"+campo+"','"+tipo+"'));\n";
				if(tipos.get(tipo)==null){
					//System.out.println(tabla+":"+campo+":"+tipo);
					//System.exit(0);
				}
			}
			resultado+="}catch(e){alert(e.stack);}";
			resultado="try{\n"+codigoTablas+resultado;
			ps.println(resultado);
		}catch(Exception e){
			System.out.println("tabla:"+tablaError);
			System.out.println("campo:"+campoError);
			System.out.println("tipo:"+tipoError);
			e.printStackTrace();
		}
	}
}