import java.util.*;
import java.io.*;
class Tabla{
	public String nombre;
	public ArrayList<String>hijos;
	public HashMap<String,Integer>padres;
	public HashMap<String,Integer>ancestros;
	public ArrayList<String>listaPadres;
	public HashMap<String,Integer>hash;
	public int cantidad;
	public boolean mostrados=false;
	public static HashMap<String,Tabla>tablas=new HashMap<String,Tabla>();
	public static ArrayList<Tabla>listaTablas=new ArrayList<Tabla>();
	public Tabla(String nombre){
		this.nombre=nombre;
		hijos=new ArrayList<String>();
		hash=new HashMap<String,Integer>();
		padres=new HashMap<String,Integer>();
		listaPadres=new ArrayList<String>();
		ancestros=new HashMap<String,Integer>();
	}
	public static Tabla getTabla(String nombre){
		if(tablas.get(nombre)==null){
			Tabla tabla=new Tabla(nombre);
			tablas.put(nombre,tabla);
			listaTablas.add(tabla);
		}
		return tablas.get(nombre);
	}
	public void add(String nombre){
		if(hash.get(nombre)==null){
			hijos.add(nombre);
			hash.put(nombre,1);
		}
	}
	public void addPadre(String nombre){
		if(padres.get(nombre)==null){
			listaPadres.add(nombre);
			ancestros.put(nombre,1);
			padres.put(nombre,1);
		}
	}
	public void imprimir(PrintStream ps,int nivel,HashMap<String,Integer>a){
		String tab="";
		for(int i=0;i<nivel;i++){
			tab+="\t";
		}
		String x="";
		for(int i=0;i<this.listaPadres.size();i++){
			x+=" "+listaPadres.get(i);
		}
		ps.println(tab+this.nombre+":"+this.cantidad+" ------->"+x);
		if(a.get(this.nombre)!=null){
			return;
		}
		a.put(this.nombre,1);
		for(int i=0;i<hijos.size();i++){
			Tabla.getTabla(this.hijos.get(i)).imprimir(ps,nivel+1,a);
		}
		this.mostrados=true;
	}
	public void contar()throws Exception{
		if(this.cantidad>0){
			System.out.println(":"+this.nombre);
			return;
		}
		this.cantidad=this.hijos.size();
		for(int i=0;i<this.hijos.size();i++){
			Tabla hijo=Tabla.getTabla(this.hijos.get(i));
			hijo.contar();
			cantidad+=hijo.cantidad;
		}
	}
}
public class CrearTexto{
	public static void main(String[]args){
		U.imp("Inicio");
		String tablaError="";
		String campoError="";
		String tipoError="";
		try{
			PrintStream ps=new PrintStream(new FileOutputStream(new File("BaseDatosSIC.txt"),false));
			String resultado="";
			Scanner r=new Scanner(new File("ReferenciasSIC.txt"));
			String codigoTablas="";
			HashMap<String,Integer>tipos=new HashMap<String,Integer>();
			tipos.put("VARCHAR2",1);
			tipos.put("DATE",1);
			tipos.put("NUMBER",1);
			tipos.put("RAW",1);
			tipos.put("LONG",1);
			tipos.put("LONGRAW",1);
			tipos.put("CHAR",1);
			while(r.hasNext()){
				String tabla=r.next();
				String tablaPadre=r.next();
				String restriccion=r.next();
				Tabla.getTabla(tablaPadre).add(tabla);
				Tabla.getTabla(tabla).addPadre(tablaPadre);
			}
			for(int i=0;i<Tabla.listaTablas.size();i++){
				Tabla tabla=Tabla.listaTablas.get(i);
				tabla.contar();
			}
			int max=0;
			Tabla tablaMax=null;
			for(int i=0;i<Tabla.listaTablas.size();i++){
				Tabla tabla=Tabla.listaTablas.get(i);
				if(tabla.cantidad>max){
					max=tabla.cantidad;
					tablaMax=tabla;
				}
			}
			System.out.println(tablaMax.nombre);
			for(int i=0;i<Tabla.listaTablas.size();i++){
				Tabla tabla=Tabla.listaTablas.get(i);
				if(tabla.padres.size()==0){
					tabla.imprimir(ps,0,new HashMap<String,Integer>());
				}
			}
			//ps.println(resultado);
		}catch(Exception e){
			U.imp("tabla:"+tablaError);
			U.imp("campo:"+campoError);
			U.imp("tipo:"+tipoError);
			U.imp(e);
		}
	}
}