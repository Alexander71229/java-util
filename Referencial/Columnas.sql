select t.table_name,t.column_name,t.data_type from all_tab_cols t where t.owner='FENIX' and t.table_name in (
select distinct r.table_name a from all_constraints r,all_constraints p where r.owner='FENIX' and r.constraint_type='R'
and p.owner='FENIX' and p.constraint_name=r.r_constraint_name
union
select distinct p.table_name b from all_constraints r,all_constraints p where r.owner='FENIX' and r.constraint_type='R'
and p.owner='FENIX' and p.constraint_name=r.r_constraint_name) order by 1,2;