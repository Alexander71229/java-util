import java.io.*;
import java.net.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.util.*;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
public class Sim01 extends HttpServlet{
	public String rutaPublica=null;
	public void init(ServletConfig config)throws ServletException{
		rutaPublica=config.getServletContext().getRealPath("/");
		if(rutaPublica==null){
			rutaPublica=calcularRuta();
		}
	}
	public static String calcularRuta(){
		String ruta="";
		try{
			Class c=Monitor.class;
			String rutax=c.getName();
			File f=new File(c.getResource("/"+rutax.replaceAll("\\.","/")+".class").getFile());
			while(f!=null){
				if(f.getName().equals("WEB-INF")){
					f=f.getParentFile();
					break;
				}
				f=f.getParentFile();
			}
			ruta=f.getCanonicalPath()+File.separator;
		}catch(Throwable t){
			ruta="";
		}
		return ruta;
	}
	public String obtenerRutaReal(ServletContext contexto,String ruta){
		String rutaReal=contexto.getRealPath(ruta);
		if(rutaReal==null&&rutaPublica!=null){
			rutaReal=rutaPublica+ruta;
		}
		return rutaReal;
	}
	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		accion(request,response);
	}
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		accion(request,response);
	}
	public void accion(HttpServletRequest request,HttpServletResponse response){
		try{
			ServletContext contexto=request.getSession().getServletContext();
			String ruta=request.getParameter("r");
			String rutaReal=ruta;
			if(rutaReal.indexOf(":")<0){
				rutaReal=obtenerRutaReal(contexto,ruta);
				if(rutaReal==null&&rutaPublica!=null){
					rutaReal=rutaPublica+ruta;
				}
			}
			File origen=new File(rutaReal);
			File[]lista=origen.listFiles();
			response.getOutputStream().println(origen.getCanonicalPath()+"<br>");
			response.getOutputStream().println(Monitor.mostrarListaArchivos(lista));
		}catch(Exception e){
		}
	}
}
