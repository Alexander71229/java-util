import java.io.*;
import java.util.*;
public class ObtenerArchivos{
	public void crearCarpetas(File f)throws Exception{
		f.getParentFile().mkdirs();
	}
	public void obtener(InputStream o,String d)throws Exception{
		while(true){
			DataInputStream dis=new DataInputStream(o);
			StringBuffer sb=new StringBuffer();
			char c=dis.readChar();
			while(c!=':'){
				sb.append(c);
				c=dis.readChar();
			}
			String nombre=sb+"";
			System.out.println(nombre);
			long l=dis.readLong();
			System.out.println(l);
			byte[]datos=new byte[(int)l];
			dis.readFully(datos);
			File f=new File(d+"/"+nombre);
			crearCarpetas(f);
			FileOutputStream fos=new FileOutputStream(f);
			fos.write(datos);
			fos.close();
		}
	}
	public void obtener(File o,String d)throws Exception{
		obtener(new FileInputStream(o),d);
	}
	public void obtener(String o,String d)throws Exception{
		obtener(new File(o),d);
	}
	public static void main(String[]argumentos){
		try{
			ObtenerArchivos oa=new ObtenerArchivos();
			oa.obtener("Pruebas/ObtenerArchivos/01/resultado01.txt","Pruebas/ObtenerArchivos/01/Salida");
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}
