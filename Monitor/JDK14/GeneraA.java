import java.io.*;
import java.util.*;
public class GeneraA{
	public static void main(String[]argumentos){
		try{
			String origen="Mass02";
			String destino="A_"+origen;
			Scanner s=new Scanner(new File(origen+".java"));
			PrintStream ps=new PrintStream(new FileOutputStream(new File(destino+".java")));
			while(s.hasNextLine()){
				String l=s.nextLine().replace(origen,destino);
				ps.println(l);
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}