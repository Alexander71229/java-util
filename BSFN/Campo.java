public class Campo{
	public String tipo;
	public String nombre;
	public String detalle;
	public Campo(){
	}
	public Campo(String nombre){
		this.nombre=nombre;
		this.tipo="String";
		if(nombre.startsWith("jd")){
			this.tipo="Date";
		}
		if(nombre.startsWith("mn")){
			this.tipo="MathNumeric";
		}
	}
	public Campo(String tipo,String nombre){
		this.tipo=tipo;
		this.nombre=nombre;
	}
	public String declaracion(int i){
		return Utilidades.tab(i)+"private "+tipo+" "+nombre+";\n";
	}
	public String declaracion(){
		return declaracion(1);
	}
	public String toString(){
		return declaracion();
	}
	public String get(int i){
		StringBuilder sb=new StringBuilder(Utilidades.tab(i));
		sb.append("public ");
		sb.append(this.tipo);
		sb.append(" get");
		sb.append(Utilidades.cap(this.nombre));
		sb.append("(){\n");
		sb.append(Utilidades.tab(i+1));
		sb.append("return this.");
		sb.append(this.nombre);
		sb.append(";\n");
		sb.append(Utilidades.tab(i));
		sb.append("}\n");
		return sb.toString();
	}
	public String set(int i){
		StringBuilder sb=new StringBuilder(Utilidades.tab(i));
		sb.append("public void set");
		sb.append(Utilidades.cap(this.nombre));
		sb.append("(");
		sb.append(this.tipo);
		sb.append(" ");
		sb.append(this.nombre);
		sb.append("){\n");
		sb.append(Utilidades.tab(i+1));
		sb.append("this.");
		sb.append(this.nombre);
		sb.append("=");
		sb.append(this.nombre);
		sb.append(";\n");
		sb.append(Utilidades.tab(i));
		sb.append("}\n");
		return sb.toString();
	}
}