import java.util.*;
import java.io.*;
public class ProyectoComunicacion{
	public String codigo;
	public String nombre;
	public String destino;
	public ArrayList<MetodoFuncionNegocio>metodos;
	public ProyectoComunicacion(){
		metodos=new ArrayList<MetodoFuncionNegocio>();
	}
	public ProyectoComunicacion(String codigo,String nombre){
		metodos=new ArrayList<MetodoFuncionNegocio>();
		this.codigo=codigo;
		this.nombre=nombre;
	}
	public String getPaquete(){
		return "oracle.e1.bssv."+this.codigo;
	}
	public String getDestinoValueObject(){
		return this.destino+"valueobject\\";
	}
	public void agregar(MetodoFuncionNegocio metodoFuncionNegocio){
		this.metodos.add(metodoFuncionNegocio);
	}
	public void generar()throws Exception{
		StringBuilder sb=new StringBuilder("package ");
		sb.append(this.getPaquete());
		sb.append(";\nimport oracle.e1.bssvfoundation.base.*;\nimport oracle.e1.bssvfoundation.connection.*;\nimport oracle.e1.bssvfoundation.exception.*;\nimport oracle.e1.bssvfoundation.services.*;\nimport oracle.e1.bssvfoundation.util.*;\nimport java.util.*;\nimport oracle.e1.bssv.J5712004.valueobject.*;\n");
		sb.append("public abstract class "+this.nombre+" extends BusinessService{\n");
		for(int i=0;i<metodos.size();i++){
			sb.append(metodos.get(i).imprimir());
		}
		sb.append("}\n");
		PrintStream ps=new PrintStream(new FileOutputStream(new File(destino+nombre+".java")));
		ps.print(sb+"");
	}
}