import java.util.*;
public class Clase{
	public String paquete;
	public String nombre;
	public String extension;
	public String imports;
	public ArrayList<Campo>campos;
	public Clase(){
		campos=new ArrayList<Campo>();
	}
	public Clase(String paquete,String nombre){
		this();
		this.paquete=paquete;
		this.nombre=nombre;
	}
	public String imprimir(){
		StringBuilder sb=new StringBuilder();
		sb.append("package "+this.paquete+";\n");
		sb.append(this.imports+"\n");
		if(this.extension!=null){
			sb.append("public class "+this.nombre+" "+this.extension+"{\n");
		}else{
			sb.append("public class "+this.nombre+"{\n");
		}
		for(int i=0;i<campos.size();i++){
			sb.append(campos.get(i).declaracion());
		}
		for(int i=0;i<campos.size();i++){
			sb.append(campos.get(i).set(1));
		}
		for(int i=0;i<campos.size();i++){
			sb.append(campos.get(i).get(1));
		}
		sb.append("}\n");
		return sb.toString();
	}
	public void add(Campo campo){
		campos.add(campo);
	}
}