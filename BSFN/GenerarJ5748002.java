import java.util.*;
//D:\Desarrollo\EPM\FE\BSSV\oracle\e1\bssv\J5748002
public class GenerarJ5748002{
	public static void main(String[]argumentos){
		try{
			ProyectoComunicacion proyecto=new ProyectoComunicacion("J5748002","RecibirRespuesta");
			proyecto.destino="D:\\Desarrollo\\EPM\\FE\\BSSV\\oracle\\e1\\bssv\\"+proyecto.codigo+"\\";
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="AlmacenarRespuestaTransaccion";
				/*D:\Desarrollo\EPM\FE\BSSV\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\BSSV\\Estructuras\\D5748001I.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="CerrarOTFSM";
				/*D:\Desarrollo\EPM\FE\BSSV\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\BSSV\\Estructuras\\D5748001G.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="ValidarDatosRespuesta";
				/*D:\Desarrollo\EPM\FE\BSSV\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\BSSV\\Estructuras\\D5748001H.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			proyecto.generar();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}