public class MetodoFuncionNegocio{
	public GeneradorValueObject valueObject;
	public MetodoFuncionNegocio(){
	}
	public MetodoFuncionNegocio(GeneradorValueObject valueObject){
		this.valueObject=valueObject;
	}
	public String imprimir(){
		StringBuilder sb=new StringBuilder();
		sb.append(Utilidades.tab(1));
		sb.append("public static E1MessageList ejecutar");
		sb.append(this.valueObject.nombre);
		sb.append("(IContext context,IConnection connection,");
		sb.append(this.valueObject.nombre);
		sb.append(" vo){\n");
		sb.append(Utilidades.tab(2));
		sb.append("E1MessageList messages=new E1MessageList();\n");
		sb.append(Utilidades.tab(2));
		sb.append("BSFNParameters bsfnParams=new BSFNParameters();\n");
		for(int i=0;i<valueObject.clase.campos.size();i++){
			if(valueObject.clase.campos.get(i).detalle.toLowerCase().indexOf("input")>=0||valueObject.clase.campos.get(i).detalle.toLowerCase().indexOf("both")>=0){
				sb.append(Utilidades.tab(2));
				sb.append("bsfnParams.setValue(\"");
				sb.append(valueObject.clase.campos.get(i).nombre);
				sb.append("\",vo.get");
				sb.append(Utilidades.cap(valueObject.clase.campos.get(i).nombre));
				sb.append("());\n");
			}
		}
		sb.append(Utilidades.tab(2));
		sb.append("try{\n");
		sb.append(Utilidades.tab(3));
		sb.append("IBSFNService bsfnService=context.getBSFNService();\n");
		sb.append(Utilidades.tab(3));
		sb.append("bsfnService.execute(context,connection,\""+this.valueObject.nombre+"\",bsfnParams);\n");
		sb.append(Utilidades.tab(2));
		sb.append("}catch(BSFNServiceInvalidArgException invalidArgEx){\n");
		sb.append(Utilidades.tab(3));
		sb.append("messages.addMessage(new E1Message(context,\"018FIS\",invalidArgEx.getMessage()));\n");
		sb.append(Utilidades.tab(3));
		sb.append("return messages;\n");
		sb.append(Utilidades.tab(2));
		sb.append("}catch(BSFNServiceSystemException systemEx){\n");
		sb.append(Utilidades.tab(3));
		sb.append("messages.addMessage(new E1Message(context,\"019FIS\",systemEx.getMessage()));\n");
		sb.append(Utilidades.tab(3));
		sb.append("return messages;\n");
		sb.append(Utilidades.tab(2));
		sb.append("}\n");
		for(int i=0;i<valueObject.clase.campos.size();i++){
			if(valueObject.clase.campos.get(i).detalle.toLowerCase().indexOf("output")>=0||valueObject.clase.campos.get(i).detalle.toLowerCase().indexOf("both")>=0){
				sb.append(Utilidades.tab(2));
				sb.append("vo.set");
				sb.append(Utilidades.cap(valueObject.clase.campos.get(i).nombre));
				sb.append("(("+valueObject.clase.campos.get(i).tipo+")bsfnParams.getValue(\"");
				sb.append(valueObject.clase.campos.get(i).nombre);
				sb.append("\"));\n");
			}
		}
		sb.append(Utilidades.tab(2));
		sb.append("return bsfnParams.getE1MessageList();\n");
		sb.append(Utilidades.tab(1));
		sb.append("}\n");
		return sb.toString();
	}
}