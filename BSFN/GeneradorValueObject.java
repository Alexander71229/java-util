import java.util.*;
import java.io.*;
public class GeneradorValueObject{
	public String paquete;
	public String ruta;
	public String destino;
	public String nombre;
	public Clase clase;
	public GeneradorValueObject(){
	}
	public void generar()throws Exception{
		StringBuilder sb=new StringBuilder(paquete);
		sb.append("public class "+nombre+"{");
		Scanner scanner=new Scanner(new File(ruta));
		clase=new Clase(this.paquete,this.nombre);
		clase.extension="extends ValueObject implements Serializable";
		clase.imports="import java.io.Serializable;\nimport java.util.Date;\nimport oracle.e1.bssvfoundation.base.ValueObject;\nimport oracle.e1.bssvfoundation.util.MathNumeric;";
		while(scanner.hasNextLine()){
			String linea=scanner.nextLine();
			String[]valores=linea.split(","); 
			try{
				int indice=Integer.parseInt(valores[0]);
				Campo campo=new Campo(valores[1].replace("\"",""));
				campo.detalle=valores[4];
				clase.add(campo);
			}catch(Throwable t){
			}
		}
		PrintStream ps=new PrintStream(new FileOutputStream(new File(destino+nombre+".java")));
		System.out.println(clase.campos);
		ps.print(clase.imprimir());
	}
}