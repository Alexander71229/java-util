import java.util.*;
//D:\Desarrollo\EPM\FE\BSSV\oracle\e1\bssv\J5712004
public class GenerarJ5712004{
	public static void main(String[]argumentos){
		try{
			ProyectoComunicacion proyecto=new ProyectoComunicacion("J5712004","TrasladarOperacionActivosOperativosTMP");
			proyecto.destino="D:\\Desarrollo\\EPM\\FE\\BSSV\\oracle\\e1\\bssv\\"+proyecto.codigo+"\\";
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="ValidarInformacionCabecera";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021A.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="ValidarInformacionDetalle";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021B.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="GrabarCuentasCabecera";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021C.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="GrabarCuentasDetalle";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021D.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="ObtenerPorcentajeMonto";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021E.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="BorradoTablasTemporales";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021F.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="ValidarTotalesPorcentajeMonto";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021G.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="ConsultaCuentasDistribucion";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021H.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="CuadraDistribucion100x100";
				/*D:\Desarrollo\EPM\FE\JDE\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\Estructuras\\D5712021H.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="GenerarTransaccionesContables";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712021I.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			{
				GeneradorValueObject objeto=new GeneradorValueObject();
				objeto.paquete=proyecto.getPaquete()+".valueobject";
				objeto.destino=proyecto.getDestinoValueObject();
				objeto.nombre="CallUBER5712050";
				/*D:\Desarrollo\EPM\FE\JDE\TrasladosOperaciónActivo\Estructuras\*/
				objeto.ruta="D:\\Desarrollo\\EPM\\FE\\JDE\\TrasladosOperaciónActivo\\Estructuras\\D5712015D.csv";
				objeto.generar();
				proyecto.agregar(new MetodoFuncionNegocio(objeto));
			}
			proyecto.generar();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}