public class Utilidades{
	public static String tabulacion="\t";
	public static String tab(int n){
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<n;i++){
			sb.append(tabulacion);
		}
		return sb.toString();
	}
	public static String cap(String s){
		return s.substring(0,1).toUpperCase()+s.substring(1);
	} 
}