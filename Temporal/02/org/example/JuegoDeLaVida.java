package org.example;

import java.util.Random;
import java.util.Map;
import java.util.HashMap;
import javax.swing.JFrame;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.Canvas;
import java.awt.image.BufferStrategy;
import java.awt.Color;

public class JuegoDeLaVida extends JFrame implements KeyListener, Runnable {
    private int w, h, g, s;
    private String p;
    private boolean activo = true;
    private Canvas canvas;
    private int tamanoCelula = 40;
    public void inicio(String[] args) throws Exception {
        Map < String, String > parametrosEntrada = leerParametros(args);
        validarParametros(parametrosEntrada);
        w = Integer.parseInt(parametrosEntrada.get("w")); // Ancho
        h = Integer.parseInt(parametrosEntrada.get("h")); // Alto
        p = parametrosEntrada.get("p"); // Configuración inicial del tablero.
        s = Integer.parseInt(parametrosEntrada.get("s")); // Tiempo de espera en milisegundos
        g = Integer.parseInt(parametrosEntrada.get("g")); // generaciones
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setUndecorated(false);
        this.setResizable(false);
        this.setSize(w * tamanoCelula, h * tamanoCelula);
        this.setVisible(true);
        this.addKeyListener(this);
        this.setLocationRelativeTo(null);
        this.canvas = new Canvas();
        this.canvas.setSize(this.w * tamanoCelula, this.h * tamanoCelula);
        this.add(canvas);
        this.canvas.createBufferStrategy(2);
        this.getContentPane().setBackground(Color.BLACK);
        (new Thread(this)).run();
    }
    public void run() {
        try {
            int contadorGeneraciones = 0;
            int[][] generacionActual = crearGeneracionInicial(w, h, p, .5);
            while ((g == 0 && activo) || contadorGeneraciones < g) {
                mostrarGeneracion(generacionActual);
                generacionActual = calcularSiguienteGeneracion(generacionActual);
                try {
                    Thread.sleep(s); //Tiempo de espera basado en s
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                contadorGeneraciones++;
            }
            this.dispose();
        } catch (Exception e) {
            this.dispose();
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        try {
            new JuegoDeLaVida().inicio(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public Map < String, String > leerParametros(String[] args) throws Exception {
        Map < String, String > parametrosEntrada = new HashMap < > ();
        for (int i = 0; i < args.length; i++) {
            try {
                String[] partes = args[i].split("=");
                parametrosEntrada.put(partes[0], partes[1]);
            } catch (Exception ex) {
                throw new Exception("El parámetro '" + args[i] + "' no tiene valor");
            }
        }
        return parametrosEntrada;
    }
    public void validarParametros(Map < String, String > parametros) throws Exception {
        if (parametros.get("w") == null) {
            throw new Exception("Debe especificar un valor para el parámetro 'w'");
        }
        if (parametros.get("h") == null) {
            throw new Exception("Debe especificar un valor para el parámetro 'h'");
        }
        if (parametros.get("s") == null) {
            throw new Exception("Debe especificar un valor para el parámetro 's'");
        }
        if (parametros.get("g") == null) {
            throw new Exception("Debe especificar un valor para el parámetro 'g'");
        }
        if (parametros.get("p") == null) {
            throw new Exception("Debe especificar un valor para el parámetro 'p'");
        }
        if (!(parametros.get("w").equals("10") || parametros.get("w").equals("20") || parametros.get("w").equals("40") || parametros.get("w").equals("80"))) {
            throw new Exception("El valor de 'w' debe ser 10, 20, 40 u 80");
        }
        if (!(parametros.get("h").equals("10") || parametros.get("h").equals("20") || parametros.get("h").equals("40"))) {
            throw new Exception("El valor de 'h' debe ser 10, 20 o 40");
        }
        try {
            int s = Integer.parseInt(parametros.get("s"));
            if (s < 250 || s > 1000) {
                throw new Exception("El valor del parámetro 's' debe estar entre 250 y 1000");
            }
        } catch (Exception e) {
            throw new Exception("El valor del parámetro 's' no es válido");
        }
        try {
            int g = Integer.parseInt(parametros.get("g"));
            if (g < 0) {
                throw new Exception("El valor del parámetro 'g' debe ser un número positivo");
            }
        } catch (Exception e) {
            throw new Exception("El valor del parámetro 'g' no es válido");
        }
    }
    public int[][] crearGeneracionInicial(int w, int h, String p, double pb) throws Exception { // se crea la matriz y se genera los nùmeros aleatorios
        int[][] generacion = new int[h][w];
        Random random = new Random();
        if (p.equals("rnd") || p.equals("\"rnd\"")) {
            for (int i = 0; i < h; i++) {
                for (int j = 0; j < w; j++) {
                    generacion[i][j] = (random.nextDouble() < pb) ? 1 : 0;
                }
            }
        } else {
            int x = 0;
            int y = 0;
            for (int i = 0; i < p.length(); i++) {
                if (p.charAt(i) == '0') {
                    x++;
                }
                if (p.charAt(i) == '1') {
                    try {
                        generacion[y][x] = 1;
                        x++;
                    } catch (Exception e) {
                        throw new Exception("La generación inicial indicada por el parámetro 'p' no encaja en la cuadrícula");
                    }
                }
                if (p.charAt(i) == '#') {
                    y++;
                    x = 0;
                }
            }
        }
        return generacion;
    }

    public void mostrarGeneracion(int[][] generacion) throws Exception { //para mostrar estado cèlula
        BufferStrategy bf = this.canvas.getBufferStrategy();
        Graphics graficas = bf.getDrawGraphics();
        for (int i = 0; i < generacion.length; i++) {
            for (int j = 0; j < generacion[0].length; j++) {
                if (generacion[i][j] == 1) {
                    graficas.setColor(Color.white);
                } else {
                    graficas.setColor(Color.black);
                }
                graficas.fillRect(j * tamanoCelula, i * tamanoCelula, tamanoCelula, tamanoCelula);
            }
        }
        bf.show();
        Toolkit.getDefaultToolkit().sync();
    }


    public int contarVecinasVivas(int[][] generacion, int fila, int columna) {
        int count = 0;
        int h = generacion.length;
        int w = generacion[0].length;

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int vecinaFila = (fila + i);
                int vecinaColumna = (columna + j);
                if (vecinaFila >= 0 && vecinaFila < h && vecinaColumna >= 0 && vecinaColumna < w) {
                    count += generacion[vecinaFila][vecinaColumna];
                }
            }
        }

        count -= generacion[fila][columna];
        return count;
    }
    public int[][] calcularSiguienteGeneracion(int[][] generacionActual) {
        int h = generacionActual.length;
        int w = generacionActual[0].length;
        int[][] siguienteGeneracion = new int[h][w];

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                int vecinasVivas = contarVecinasVivas(generacionActual, i, j);
                siguienteGeneracion[i][j] = calcularSiguienteEstado(generacionActual[i][j], vecinasVivas);
            }
        }

        return siguienteGeneracion;
    }



    public int calcularSiguienteEstado(int estadoActual, int vecinasVivas) { //ya implementaciòn de las reglas
        if (estadoActual == 1 && (vecinasVivas < 2 || vecinasVivas > 3)) {
            return 0; // muere
        } else if (estadoActual == 0 && vecinasVivas == 3) {
            return 1; // nace
        } else {
            return estadoActual; //estado actual
        }
    }
    @Override
    public void keyPressed(KeyEvent event) {}
    @Override
    public void keyReleased(KeyEvent event) {
        if (g == 0) {
            activo = false;
        }
    }
    @Override
    public void keyTyped(KeyEvent event) {}
}