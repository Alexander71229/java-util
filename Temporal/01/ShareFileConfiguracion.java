package sura.dealersharefile.sharefile;

import com.citrix.sharefile.api.SFApiClient;
import com.citrix.sharefile.api.SFSdk;
import com.citrix.sharefile.api.authentication.SFOAuth2Token;
import com.citrix.sharefile.api.authentication.SFOAuthService;
import com.citrix.sharefile.api.entities.SFFolderTemplatesEntity;
import com.citrix.sharefile.api.exceptions.*;
import com.citrix.sharefile.api.https.SFUploadRunnable;
import com.citrix.sharefile.api.https.TransferRunnable;
import com.citrix.sharefile.api.interfaces.ISFOAuthService;
import com.citrix.sharefile.api.interfaces.ISFQuery;
import com.citrix.sharefile.api.models.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import sura.dealersharefile.domain.respuesta.RespuestaGestorArchivos;
import sura.dealersharefile.domain.solicitud.SolicitudGestorArchivos;
import sura.dealersharefile.dto.RespuestaGestorArchivosDTO;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Repository   //Tener en cuenta
public class ShareFileConfiguracion {

    private static final Object lock = new Object();

    @Value("${sharefile.shareFileCredentials.application}")
    private String aplicacion;

    @Value("${sharefile.shareFileCredentials.clientId}")
    private String idCliente;

    @Value("${sharefile.shareFileCredentials.clientSecret}")
    private String secretCliente;

    @Value("${sharefile.shareFileCredentials.controlPane}")
    private String controlPanel;

    @Value("${sharefile.shareFileCredentials.user}")
    private String usuario;

    @Value("${sharefile.shareFileCredentials.password}")
    private String contrasena;

    @Value("${sharefile.idZonaSharefile}")
    private String idZonaSharefile;

    public static final String RESPONSE_CODE_OK = "200";

    public static final String SEARCH_GROUP_NAME = "Usuarios de Consulta";

    private SFApiClient apiClient;

    //Inicializa una sesion en sharefile para interactuar con la api
    @PostConstruct
    public void inicializarClienteShareFile() throws SFInvalidStateException, SFNotAuthorizedException, SFJsonException {
        SFSdk.init(idCliente, secretCliente, aplicacion + "." + controlPanel);
        this.apiClient = new SFApiClient( autenticarClienteShareFile(aplicacion, controlPanel, usuario, contrasena),
                usuario, idCliente, secretCliente, new TokenAutenticacion(), null);
    }

    public SFOAuth2Token autenticarClienteShareFile(String subdomain, String apiControlPlane, String username, String password)
            throws SFNotAuthorizedException, SFJsonException, SFInvalidStateException {
        ISFOAuthService oAuthService = new SFOAuthService();
        return oAuthService.authenticate(subdomain, apiControlPlane, username, password);
    }


    //Crea la carpeta y sube el documento
    public RespuestaGestorArchivosDTO almacenarDocumento(SolicitudGestorArchivos solicitudGestorArchivos, File archivo) throws Exception {
        //Folder name concatea la ruta parcial con la carpeta con el nombre de la operacion a realizar sobre la poliza, basicamente es un string con la ruta completa
        String folderName = ((solicitudGestorArchivos.getRutaGuardado() != null && !solicitudGestorArchivos.getRutaGuardado().isEmpty())
                ? solicitudGestorArchivos.getRutaGuardado() + "/"
                : "") + solicitudGestorArchivos.getOperacion();

        folderName = folderName.replace("LABO/123", "REACTIVETEST");

        folderName = updateFolderName(folderName);

        System.out.println("Nombre del folder: "+folderName);
        SFItem folder = crearCarpeta(folderName);
        System.out.println("Esta es la url del folder: "+folder.geturl());

        String nombre = solicitudGestorArchivos.getRutaDocumentoFTP().substring(solicitudGestorArchivos.getRutaDocumentoFTP().lastIndexOf("/") + 1);
        subirArchivo(folder.geturl(), archivo, nombre);  //Añadir nombre del archivo segun lo que tiene solicitudGestorArchivos.rutaDocumentoFTP o archivo.name
        RespuestaGestorArchivosDTO response = RespuestaGestorArchivosDTO.builder()
                .codigoRetorno(RESPONSE_CODE_OK)
                .descError("OK")
                .idOperacion(folder.getId())
                .urlCarpeta(String.format("https://%s.sharefile.com/f/%s", this.aplicacion, folder.getId()))
                .build();
        System.out.println(response.toString());
        return response;
    }


    /*Crea una jerarquia de carpetas de forma recursiva, esto implica que valida desde el folder que debe contener el archivo hacia atras
      creando el primer folder con parent = null obteniendo el id en sharefile de nuestra carpeta raiz y asignandosela como padre,
      a partir de ahi con base en el primer padre va creando las demas carpetas hijas.*/
    public SFItem crearCarpeta(String path) throws Exception {
        synchronized (lock) {
            LocalDate fechaActual = LocalDate.now();
            String mes = fechaActual.format(DateTimeFormatter.ofPattern("MMMM")).toUpperCase();
            SFItem parent = null;
            try { // A/B
                return obtenerIdCarpeta(path);
            } catch (SFNotFoundException e) {
                String parentFolder = getParentFolder(path);
                if (!parentFolder.isEmpty()) {  // A/B
                    parent = crearCarpeta(parentFolder);
                }
            } catch (SFSDKException e) {
                throw new Exception("Se ha presentado un error al crear el directorio: ", e);
            }
            SFFolder newFolder = new SFFolder();
            newFolder.setName(path.split("/")[path.split("/").length - 1]);

            //Controla fecha de caducidad
            if (newFolder.getName().toString().equals(mes)){
                Calendar calendar = Calendar.getInstance();
                Date currentDate = calendar.getTime();
                Date fechaExpiracion = obtenerFechaExpiracion(currentDate);
                System.out.println("La carpeta se llama: "+newFolder.getName().toString());
                newFolder.setExpirationDate(fechaExpiracion);
                System.out.println("A la carpeta se le asigno fecha de caducidad: "+newFolder.getExpirationDate());

            }
            return crearCarpetaApi(parent != null ? parent.geturl().toString() : null, newFolder);
        }
    }


    //OJO QUE EL FUNCIONAMIENTO DE ESTO DEPENDE DE QUE SE BLOQUEE EL HILO Y DE QUE crearCarpetaApi controle la excepcion retornando return (SFFolder) obtenerIdCarpeta(folder.getName());
    public SFItem crearCarpeta2(String path) throws Exception {
        synchronized (lock) {
            LocalDate fechaActual = LocalDate.now();
            String mes = fechaActual.format(DateTimeFormatter.ofPattern("MMMM")).toUpperCase();
            SFItem parent = null;
            try {
                return obtenerIdCarpeta(path);
            } catch (SFNotFoundException e) {

                for (final String id : path.split("/")) {

                    SFFolder newFolder = new SFFolder();
                    newFolder.setName(id);
                    if (newFolder.getName().toString().equals(mes)){
                        Calendar calendar = Calendar.getInstance();
                        Date currentDate = calendar.getTime();
                        Date fechaExpiracion = obtenerFechaExpiracion(currentDate);
                        System.out.println("La carpeta se llama: "+newFolder.getName().toString());
                        newFolder.setExpirationDate(fechaExpiracion);
                        System.out.println("A la carpeta se le asigno fecha de caducidad: "+newFolder.getExpirationDate());

                    }

                    parent = crearCarpetaApi(parent != null ? parent.geturl().toString() : null, newFolder);

                    //pathparent = pathparent.equals("") ? id : (pathparent + "/" + id);
                }

            } catch (SFSDKException e) {
                throw new Exception("Se ha presentado un error al crear el directorio: ", e);
            }

            return parent;
        }
    }


  //Crea una sola carpeta con base en el id de su padre(Si es la primera carpeta su padre sera la raiz y parentFolder sera null)
    public SFFolder crearCarpetaApi(String parentFolder, SFFolder folder) throws SFOtherException, SFInvalidStateException, SFServerException, SFNotAuthorizedException, SFOAuthTokenRenewException {
        URI parentUri = null;
        try {
            if (Optional.ofNullable(parentFolder).isPresent()) { //Valida si existe un directorio padre, de ser asi le crea una uri
                parentUri = crearUriCarpetaPadre(parentFolder);
            } else {
                parentUri = apiClient.items().get().execute().getParent().geturl(); //Devuelve el id en sharefile(URI) de la carpeta raiz(Carpetas compartidas)
            }
            return apiClient.items().createFolder(parentUri, folder).execute(); //Llamado al api para crear un folder segun el uri del padre
        } catch (SFSDKException e1) {
            System.out.println("Se ha presentado un error: "+ e1);  //Aqui debo manejar la excepcion
        }

        return (SFFolder) obtenerIdCarpeta(folder.getName());
    }

    private URI crearUriCarpetaPadre(String parentFolder) throws SFInvalidStateException, SFServerException,
            SFNotAuthorizedException, SFOAuthTokenRenewException, SFOtherException {
        URI parentUri;
        try {
            parentUri = new URI(parentFolder);
        } catch (URISyntaxException e) {
            parentUri = apiClient.items().get().execute().getParent().geturl(); //Devuelve el id en sharefile(URI) de la carpeta raiz(Carpetas compartidas)

        }
        return parentUri;
    }

    //Un llamado al api para obtener el id de un folder en SF segun su ruta descrita en un String(Ej del String: LABO/ASESOR/POLIZA/OPERACION)
    public SFItem obtenerIdCarpeta(String path) throws SFInvalidStateException, SFServerException, SFNotAuthorizedException,
            SFOAuthTokenRenewException, SFOtherException {
        return apiClient.items().byPath(path).execute();
    }

    //Segun el ultimo "/" de la ruta toma las carpetas desde ahi hacia atras, ya que eso seria la carpeta padre de cualquier folder
    private String getParentFolder(String path) {
        if (path.lastIndexOf('/') == -1) { //Si el path no contiene un "/" es porque es la primera carpeta de la jerarquia, entonces retorna vacio
            return "";
        }
        return path.substring(0, path.lastIndexOf('/'));
    }



    //Subir archivo
    private void subirArchivo(URI parentUrl, File file, String nombre) throws IOException, SFInvalidStateException, SFServerException {
        TransferRunnable.IProgress progressListener = new TransferRunnable.IProgress()
        {
            @Override
            public void bytesTransfered(long bytesTrasnfered) {}

            @Override
            public void onError(SFSDKException e, long bytesTrasnfered) {}

            @Override
            public void onComplete(long bytesTrasnfered) {}
        };

        FileInputStream inputStream = new FileInputStream(file);

        //Se alamacena en un SFUploadRequestParams propiedades del archivo y la url en sharefile de la carpeta que la contiene
        SFUploadRequestParams requestParams = new SFUploadRequestParams();
        requestParams.setFileName(nombre+".pdf");
        requestParams.setDetails("details");
        requestParams.setFileSize((long) inputStream.available());
        requestParams.seturl(parentUrl);

        //Instancia objeto uploader de sharefile con los requestParams, el archivo en un inputStream y el progressListener
        SFUploadRunnable uploader = apiClient.getUploader(requestParams,inputStream,progressListener);

        //Indica que suba el archivo
        uploader.start();
        System.out.println("El archivo se subio con exito");
    }



    //API de usuarios(Necesario para administrar permisos)
    public SFUser getUserByEmail(String correo) throws SFInvalidStateException, SFServerException, SFNotAuthorizedException,
            SFOAuthTokenRenewException, SFOtherException {
        return apiClient.users().get().addQueryString("emailAddress", correo).execute();
    }

    public SFUser createUser(SolicitudGestorArchivos solicitudGestorArchivos, Boolean canresetpassword, Boolean canviewmysettings) throws SFSDKException {
        try {
            return getUserByEmail(solicitudGestorArchivos.getCorreo());
        } catch (SFNotFoundException e) {
            SFUser sfUser = createSFUser(solicitudGestorArchivos, canresetpassword, canviewmysettings);
            return apiClient.users().create(sfUser).execute();
        }

    }

    private SFUser createSFUser(SolicitudGestorArchivos solicitudGestorArchivos, Boolean canresetpassword, Boolean canviewmysettings) {
        SFUser sfUser = new SFUser();
        sfUser.setCompany(solicitudGestorArchivos.getCompania());
        sfUser.setEmail(solicitudGestorArchivos.getCorreo());
        sfUser.setFirstName(solicitudGestorArchivos.getPrimerNombre());
        sfUser.setLastName(solicitudGestorArchivos.getSegundoNombre());
        sfUser.setPassword(solicitudGestorArchivos.getCodigoUsuario());  //La contraseña que se crea tiene este formato: Codigo + nro de asesor Ej: Codigo4999
        SFUserPreferences preferences = new SFUserPreferences();
        preferences.setCanResetPassword(canresetpassword);
        preferences.setCanViewMySettings(canviewmysettings);
        sfUser.setPreferences(preferences);
        return sfUser;
    }



    //Permisos
    private SFAccessControl createControl(SFPrincipal principal, Boolean canDownload, Boolean canUpload,
                                          Boolean canDelete, Boolean canManagePermissions, Boolean canView) {
        SFAccessControl control = new SFAccessControl();
        control.setCanDownload(canDownload);
        control.setCanUpload(canUpload);
        control.setCanDelete(canDelete);
        control.setCanManagePermissions(canManagePermissions);
        control.setCanView(canView);
        control.setPrincipal(principal);
        return control;
    }

    //Permite obtener un grupo de usuarios como por ejemplo "Usuarios de Consulta"
    /*
    * Se puede optimizar con programacion reactiva
    * */
    public SFGroup getGroupByName(String searchGroupName) throws Exception {
        List<SFGroup> filteredGroup;
        try {
            filteredGroup = apiClient.groups().get().execute().getFeed().stream()
                    .filter(x -> x.getName().equals(searchGroupName)).collect(Collectors.toList());
        } catch (SFSDKException e) {
            throw new Exception("Error al intentar obtener el grupo "+e);
        }
        if (!filteredGroup.isEmpty()) {
            return filteredGroup.get(0);
        }
        return null;
    }

    //Para asignar permisos al grupo de usuarios de consulta


    public RespuestaGestorArchivosDTO createAccessConsulta(SolicitudGestorArchivos solicitudGestorArchivos, String parentID)
            throws IOException, Exception {
        try {
            SFGroup group = getGroupByName(SEARCH_GROUP_NAME);
            if (Optional.ofNullable(group).isPresent()) {
                SFItem folder = obtenerIdCarpeta(solicitudGestorArchivos.getRutaGuardado().split("/")[0]);
                SFAccessControl control = createControl(group, false, false, false, false, true);

                return createSFControl(folder.geturl().toString(), group, control, "", "");
            } else {
                throw new Exception(new IllegalArgumentException("El grupo base de consulta fue eliminado"));
            }
        } catch (SFSDKException e) {
            throw new Exception(e);
        }

    }


    private RespuestaGestorArchivosDTO createSFControl(String idFolder, SFPrincipal principal, SFAccessControl control, String urlCarpeta, String idProceso)
            throws SFInvalidStateException, SFServerException, SFNotAuthorizedException, SFOAuthTokenRenewException,
            SFOtherException {
        System.out.println("Entra a asignar permisos y los datos son: "+ principal.getEmail() + " "+ urlCarpeta + " " + idFolder);
        ArrayList<SFAccessControl> controls = getAccessControls(idFolder);

        if (controls.stream().map(x -> x.getPrincipal().getId()).filter(y -> y.equals(principal.getId()))
                .collect(Collectors.toList()).isEmpty()) {
            control = apiClient.accessControls().createByItem(crearUriCarpetaPadre(idFolder), control, true, false)
                    .execute();
        } else {
            control = controls.stream().filter(y -> y.getPrincipal().getId().equals(principal.getId()))
                    .collect(Collectors.toList()).get(0);
        }
        RespuestaGestorArchivosDTO  response = RespuestaGestorArchivosDTO.builder()
                .codigoRetorno(RESPONSE_CODE_OK)
                .descError(control.getId())
                .idOperacion(idProceso)
                .urlCarpeta(urlCarpeta)
                .build();
        System.out.println("Respuesta asignacion de permisos: "+response.toString());
        return response;
    }

    private ArrayList<SFAccessControl> getAccessControls(String idFolder) throws SFInvalidStateException,
            SFServerException, SFNotAuthorizedException, SFOAuthTokenRenewException, SFOtherException {
        return apiClient.accessControls().getByItem(crearUriCarpetaPadre(idFolder)).execute().getFeed();
    }

    public RespuestaGestorArchivosDTO createAccessControl(SolicitudGestorArchivos solicitudGestorArchivos, RespuestaGestorArchivos respuestaAlmacenamiento) throws IOException, Exception {

        try {
            SFUser sfUser = createUser(solicitudGestorArchivos, true, false);
            SFItem folder = obtenerIdCarpeta(solicitudGestorArchivos.getRutaGuardado().split("/")[0]);  //Aqui esta el error, el permiso siempre se asigna a labo en este caso
            SFAccessControl control = createControl(sfUser, true, false, true, false, true);
            return createSFControl(folder.geturl().toString(), sfUser, control, respuestaAlmacenamiento.getUrlCarpeta(), solicitudGestorArchivos.getIdProceso());
        } catch (SFSDKException e) {
            throw new Exception("Ha ocurrido un error al intentar asignar permisos: ",e);
            //Aqui llena y retorna entidad con error
        }
    }

//Incluir mes del año en ruta
public static String updateFolderName(String folderName) {
    LocalDate fechaActual = LocalDate.now();
    String mes = fechaActual.format(DateTimeFormatter.ofPattern("MMMM")).toUpperCase();

    StringBuilder actualizarNombreCarpeta = new StringBuilder();

    String[] parts = folderName.split("/");
    boolean laboPresent = parts[0].equals("LABO");

    if (laboPresent) {
        actualizarNombreCarpeta.append("LABO/");
        actualizarNombreCarpeta.append(parts[1]);
        actualizarNombreCarpeta.append("/");
    } else {
        actualizarNombreCarpeta.append(parts[0]);
        actualizarNombreCarpeta.append("/");
    }

    actualizarNombreCarpeta.append(mes);
    actualizarNombreCarpeta.append("/");

    for (int i = laboPresent ? 2 : 1; i < parts.length; i++) {
        actualizarNombreCarpeta.append(parts[i]);
        if (i < parts.length - 1) {
            actualizarNombreCarpeta.append("/");
        }
    }

    return actualizarNombreCarpeta.toString();
}

//Politica de expiracion de carpeta segun el mes
    public static Date obtenerFechaExpiracion(Date fechaEntrada) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaEntrada);
        calendar.add(Calendar.DAY_OF_MONTH, 120);
        Date fechaDespues = calendar.getTime();

        return fechaDespues;
    }

    public static Date obtenerFechaExpiracion(LocalDateTime fechaEntrada) {

        Date fechaDespues = Date.from(fechaEntrada.plusDays(120).atZone(ZoneId.systemDefault()).toInstant());
        return fechaDespues;
    }

}
