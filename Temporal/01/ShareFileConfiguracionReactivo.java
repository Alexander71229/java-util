package sura.dealersharefile.sharefile;

import com.citrix.sharefile.api.SFApiClient;
import com.citrix.sharefile.api.SFSdk;
import com.citrix.sharefile.api.authentication.SFOAuth2Token;
import com.citrix.sharefile.api.authentication.SFOAuthService;
import com.citrix.sharefile.api.exceptions.*;
import com.citrix.sharefile.api.https.SFUploadRunnable;
import com.citrix.sharefile.api.https.TransferRunnable;
import com.citrix.sharefile.api.interfaces.ISFOAuthService;
import com.citrix.sharefile.api.models.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import sura.dealersharefile.domain.solicitud.SolicitudGestorArchivos;
import sura.dealersharefile.dto.RespuestaGestorArchivosDTO;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Repository   //Tener en cuenta
public class ShareFileConfiguracionReactivo {

    private static final Object lock = new Object();

    @Value("${sharefile.shareFileCredentials.application}")
    private String aplicacion;

    @Value("${sharefile.shareFileCredentials.clientId}")
    private String idCliente;

    @Value("${sharefile.shareFileCredentials.clientSecret}")
    private String secretCliente;

    @Value("${sharefile.shareFileCredentials.controlPane}")
    private String controlPanel;

    @Value("${sharefile.shareFileCredentials.user}")
    private String usuario;

    @Value("${sharefile.shareFileCredentials.password}")
    private String contrasena;

    @Value("${sharefile.idZonaSharefile}")
    private String idZonaSharefile;

    public static final String RESPONSE_CODE_OK = "200";

    public static final String SEARCH_GROUP_NAME = "Usuarios de Consulta";

    private SFApiClient apiClient;

    //Inicializa una sesion en sharefile para interactuar con la api
    @PostConstruct
    public void inicializarClienteShareFile() throws SFInvalidStateException, SFNotAuthorizedException, SFJsonException {
        SFSdk.init(idCliente, secretCliente, aplicacion + "." + controlPanel);
        this.apiClient = new SFApiClient(autenticarClienteShareFile(aplicacion, controlPanel, usuario, contrasena),
                usuario, idCliente, secretCliente, new TokenAutenticacion(), null);
    }

    public SFOAuth2Token autenticarClienteShareFile(String subdomain, String apiControlPlane, String username, String password)
            throws SFNotAuthorizedException, SFJsonException, SFInvalidStateException {
        ISFOAuthService oAuthService = new SFOAuthService();
        return oAuthService.authenticate(subdomain, apiControlPlane, username, password);
    }


    //Crea la carpeta y sube el documento

    public Mono<RespuestaGestorArchivosDTO> almacenarDocumentoReactiva(SolicitudGestorArchivos solicitudGestorArchivos, File archivo) throws SFOtherException, SFInvalidStateException, SFServerException, SFNotAuthorizedException, SFOAuthTokenRenewException {
        String folderName = ((solicitudGestorArchivos.getRutaGuardado() != null && !solicitudGestorArchivos.getRutaGuardado().isEmpty())
                ? solicitudGestorArchivos.getRutaGuardado() + "/"
                : "") + solicitudGestorArchivos.getOperacion();

        folderName = folderName.replace("LABO/123", "REACTIVETEST");

        folderName = updateFolderName(folderName);

        System.out.println("Nombre del folder: " + folderName);

        return crearCarpetaReactiva(folderName)
                .flatMap(folder -> {
                    System.out.println("Esta es la url del folder: " + folder.geturl());

                    String nombre = solicitudGestorArchivos.getRutaDocumentoFTP().substring(solicitudGestorArchivos.getRutaDocumentoFTP().lastIndexOf("/") + 1);

                    return subirArchivoReactiva(folder.geturl(), archivo, nombre)
                            .thenReturn(folder); // Pasar el folder como resultado para el siguiente paso
                })
                .map(folder -> {
                    RespuestaGestorArchivosDTO respuestaGestorArchivosDTO = RespuestaGestorArchivosDTO.builder()
                            .codigoRetorno(RESPONSE_CODE_OK)
                            .descError("OK")
                            .idOperacion(folder.getId())
                            .urlCarpeta(String.format("https://%s.sharefile.com/f/%s", this.aplicacion, folder.getId()))
                            .build();
                    return respuestaGestorArchivosDTO;
                })
                .doOnSuccess(respuestaGestorArchivosDTO -> System.out.println(respuestaGestorArchivosDTO));
    }

    public Mono<SFItem> crearCarpetaReactiva(String path) {
        String[]carpetas=path.split("/");
        String actual = "";
        for (int i = 0; i < carpetas.length; i++){
            try {

                apiClient.items().byPath(actual).execute();
            }catch (Exception e){
                e.printStackTrace();
                System.exit(0);
            }
        }
        return null;
    }


    /*Crea una jerarquia de carpetas de forma recursiva, esto implica que valida desde el folder que debe contener el archivo hacia atras
      creando el primer folder con parent = null obteniendo el id en sharefile de nuestra carpeta raiz y asignandosela como padre,
      a partir de ahi con base en el primer padre va creando las demas carpetas hijas.*/


    //Se le debe añadir el bloqueo del hilo

    /*
    public Mono<SFItem> crearCarpetaReactiva(String path) {
        LocalDate fechaActual = LocalDate.now();
        String mes = fechaActual.format(DateTimeFormatter.ofPattern("MMMM")).toUpperCase();
        return obtenerIdCarpeta(path)        //Poner un filtro y buscar cada path hasta encontrar uno que no exista
                .onErrorResume(throwable -> Mono.just(new SFItem()))
                .flatMap(sfItem -> {
                            return (sfItem.getId() == null) ?
                                    this.idCarpetaNoPresente(path) :
                                    Mono.just(sfItem);
                        }
                ).flatMap(parent -> {



                    SFFolder newFolder = new SFFolder();
                    newFolder.setName(path.split("/")[path.split("/").length - 1]);

                    //Controla fecha de caducidad
                    if (newFolder.getName().toString().equals(mes)) {

                        System.out.println("La carpeta se llama: " + newFolder.getName().toString());
                        newFolder.setExpirationDate(obtenerFechaExpiracion(LocalDateTime.now()));
                        System.out.println("A la carpeta se le asigno fecha de caducidad: " + newFolder.getExpirationDate());

                    }



                    return crearCarpetaApi(parent.geturl() != null ? parent.geturl().toString() : null, newFolder);
                });
    }

    //Añadir una validacion despues de obtener el padre con obtenerIdCarpeta de modo que se valide si el padre ya existe, de ser asi, se crea la
    private Mono<? extends SFItem> idCarpetaNoPresente(String path) {
        return getParentFolder(path)
                .flatMap(parent ->
                        !parent.isEmpty() ?
                                crearCarpetaReactiva(parent) :
                                Mono.just(new SFItem())
                );
    }
*/

    //Crea una sola carpeta con base en el id de su padre(Si es la primera carpeta su padre sera la raiz y el parametro parentFolder llegara null)
    public Mono<SFFolder> crearCarpetaApi(String parentFolder, SFFolder folder) {
        /*return Mono.just(folder)
                .flatMap(sfFolder -> {
                    if (Optional.ofNullable(parentFolder).isPresent()) {
                        return crearUriCarpetaPadre(parentFolder);
                    }
                    return getUriMono();
                })
                .flatMap(uri -> Mono.fromCallable(() -> apiClient.items().createFolder(uri, folder).execute())
                        .onErrorResume(throwable -> {
                            // Manejar la excepción y retornar un resultado alternativo
                            return obtenerIdCarpeta(folder.getName()).map(sfItem -> {
                                return (SFFolder) sfItem;
                            });
                        })
                );*/
        return (Optional.ofNullable(parentFolder).isPresent() ?
                crearUriCarpetaPadre(parentFolder) :
                getUriMono())
                .flatMap(parentUri -> Mono.fromCallable(() -> apiClient.items().createFolder(parentUri, folder).execute()))
                .onErrorResume(throwable -> {
                    return obtenerIdCarpeta(folder.getName()).map(sfItem -> {
                        return (SFFolder) sfItem;
                    });
                });

    }


    private Mono<URI> crearUriCarpetaPadre(String parentFolder) {
        return Mono.just(parentFolder)
                .map(pf -> URI.create(pf))
                .onErrorResume(throwable -> getUriMono());
    }

    private Mono<URI> getUriMono() {
        return Mono.fromCallable(() -> apiClient.items().get().execute().getParent().geturl()); //Devuelve uri de la raiz
    }

    //Un llamado al api para obtener el id de un folder en SF segun su ruta descrita en un String(Ej del String: LABO/ASESOR/POLIZA/OPERACION)
    public Mono<SFItem> obtenerIdCarpeta(String path) {
        return Mono.fromCallable(() -> apiClient.items().byPath(path).execute());
    }


    //Segun el ultimo "/" de la ruta toma las carpetas desde ahi hacia atras, ya que eso seria la carpeta padre de cualquier folder
    private Mono<String> getParentFolder(String path) {
        return Mono.just(path)
                .map(s -> {
                    if (path.lastIndexOf('/') == -1) {
                        return "";
                    }
                    return path.substring(0, path.lastIndexOf('/'));
                });
    }


    //Subir archivo
    private void subirArchivo(URI parentUrl, File file, String nombre) throws IOException, SFInvalidStateException, SFServerException {
        TransferRunnable.IProgress progressListener = new TransferRunnable.IProgress() {
            @Override
            public void bytesTransfered(long bytesTrasnfered) {
            }

            @Override
            public void onError(SFSDKException e, long bytesTrasnfered) {
            }

            @Override
            public void onComplete(long bytesTrasnfered) {
            }
        };

        FileInputStream inputStream = new FileInputStream(file);

        //Se alamacena en un SFUploadRequestParams propiedades del archivo y la url en sharefile de la carpeta que la contiene
        SFUploadRequestParams requestParams = new SFUploadRequestParams();
        requestParams.setFileName(nombre + ".pdf");
        requestParams.setDetails("details");
        requestParams.setFileSize((long) inputStream.available());
        requestParams.seturl(parentUrl);

        //Instancia objeto uploader de sharefile con los requestParams, el archivo en un inputStream y el progressListener
        SFUploadRunnable uploader = apiClient.getUploader(requestParams, inputStream, progressListener);

        //Indica que suba el archivo
        uploader.start();
        System.out.println("El archivo se subio con exito");
    }


    private Mono<Void> subirArchivoReactiva(URI parentUrl, File file, String nombre) {
        return Mono.fromCallable(() -> {
                    TransferRunnable.IProgress progressListener = new TransferRunnable.IProgress() {
                        @Override
                        public void bytesTransfered(long bytesTransferred) {
                        }

                        @Override
                        public void onError(SFSDKException e, long bytesTransferred) {
                        }

                        @Override
                        public void onComplete(long bytesTransferred) {
                        }
                    };

                    FileInputStream inputStream = new FileInputStream(file);

                    SFUploadRequestParams requestParams = new SFUploadRequestParams();
                    requestParams.setFileName(nombre + ".pdf");
                    requestParams.setDetails("details");
                    requestParams.setFileSize((long) inputStream.available());
                    requestParams.seturl(parentUrl);

                    SFUploadRunnable uploader = apiClient.getUploader(requestParams, inputStream, progressListener);

                    uploader.start();
                    System.out.println("El archivo se subió con éxito");
                    return null;
                })
                .flatMap(result -> Mono.empty())
                .onErrorResume(e -> Mono.error(new RuntimeException(e)))
                .subscribeOn(Schedulers.boundedElastic()).then();
    }


    //Incluir mes del año en ruta
    public static String updateFolderName(String folderName) {
        LocalDate fechaActual = LocalDate.now();
        String mes = fechaActual.format(DateTimeFormatter.ofPattern("MMMM")).toUpperCase();

        StringBuilder actualizarNombreCarpeta = new StringBuilder();

        String[] parts = folderName.split("/");
        boolean laboPresent = parts[0].equals("LABO");

        if (laboPresent) {
            actualizarNombreCarpeta.append("LABO/");
            actualizarNombreCarpeta.append(parts[1]);
            actualizarNombreCarpeta.append("/");
        } else {
            actualizarNombreCarpeta.append(parts[0]);
            actualizarNombreCarpeta.append("/");
        }

        actualizarNombreCarpeta.append(mes);
        actualizarNombreCarpeta.append("/");

        for (int i = laboPresent ? 2 : 1; i < parts.length; i++) {
            actualizarNombreCarpeta.append(parts[i]);
            if (i < parts.length - 1) {
                actualizarNombreCarpeta.append("/");
            }
        }

        return actualizarNombreCarpeta.toString();
    }

    //Politica de expiracion de carpeta segun el mes
    public static Date obtenerFechaExpiracion(LocalDateTime fechaEntrada) {

        Date fechaDespues = Date.from(fechaEntrada.plusDays(120).atZone(ZoneId.systemDefault()).toInstant());
        return fechaDespues;
    }


}
