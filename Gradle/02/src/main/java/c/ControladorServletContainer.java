package c;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
public class ControladorServletContainer extends org.glassfish.jersey.servlet.ServletContainer{
	public void init(ServletConfig config)throws ServletException{
		try{
			c.U.imp(this.getClass()+":"+config);
			super.init(config);
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
	public void service(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException{
		try{
			c.U.imp(this.getClass()+":"+req.getQueryString());
			super.service(req,res);
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
	public void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException{
		try{
			c.U.imp("doPost:"+this.getClass()+":"+req.getQueryString());
			super.doPost(req,res);
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
	public void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException{
		try{
			c.U.imp("doGet:"+this.getClass()+":"+req.getQueryString());
			super.doGet(req,res);
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}