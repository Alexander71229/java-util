package c;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
public class ControladorCXFServlet extends org.apache.cxf.transport.servlet.CXFServlet{
	public void init(ServletConfig config)throws ServletException{
		try{
			c.U.imp(this.getClass()+":"+config);
			super.init(config);
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
	public void service(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException{
		try{
			c.U.imp(this.getClass()+":"+req);
			super.service(req,res);
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}