package prueba.bloques;
import java.util.*;
public class Utilidades{
	public static boolean puedoObtenerPalabra(String entrada,List<Bloque>bloques){
		HashMap<Character,ArrayList<Bloque>>h=new HashMap<>();
		for(int i=0;i<bloques.size();i++){
			for(int j=0;j<bloques.get(i).letras.size();j++){
				ArrayList<Bloque>t=h.get(bloques.get(i).letras.get(j));
				if(t==null){
					t=new ArrayList<>();
					h.put(bloques.get(i).letras.get(j),t);
				}
				t.add(bloques.get(i));
			}
		}
		HashMap<Bloque,Integer>x=new HashMap<>();
		for(int i=0;i<entrada.length();i++){
			ArrayList<Bloque>t=h.get(entrada.charAt(i));
			for(int j=0;j<t.size();j++){
				x.put(t.get(j),1);
			}
		}
		if(entrada.length()<=x.size()){
			return true;
		}
		return false;
	}
	public static boolean puedoObtenerPalabra(String entrada){
		List<Bloque>bloques=new ArrayList<>();
		bloques.add(new Bloque('B','O'));
		bloques.add(new Bloque('X','K'));
		bloques.add(new Bloque('D','Q'));
		bloques.add(new Bloque('C','P'));
		bloques.add(new Bloque('N','A'));
		bloques.add(new Bloque('G','T'));
		bloques.add(new Bloque('R','E'));
		bloques.add(new Bloque('T','G'));
		bloques.add(new Bloque('Q','D'));
		bloques.add(new Bloque('F','S'));
		bloques.add(new Bloque('H','U'));
		bloques.add(new Bloque('V','I'));
		bloques.add(new Bloque('A','T'));
		bloques.add(new Bloque('O','B'));
		bloques.add(new Bloque('E','R'));
		bloques.add(new Bloque('F','S'));
		bloques.add(new Bloque('L','Y'));
		bloques.add(new Bloque('P','C'));
		bloques.add(new Bloque('Z','M'));
		bloques.add(new Bloque('J','W'));
		return puedoObtenerPalabra(entrada,bloques);
	}
}