package prueba;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import prueba.bloques.Utilidades;
public class Pruebas{
	@Test
	public void setInicial01(){
		assertEquals(true,Utilidades.puedoObtenerPalabra("A"));
	}
	@Test
	public void setInicial02(){
		assertEquals(true,Utilidades.puedoObtenerPalabra("LIBRO"));
	}
	@Test
	public void setInicial03(){
		assertEquals(false,Utilidades.puedoObtenerPalabra("BOZO"));
	}
	@Test
	public void setInicial04(){
		assertEquals(true,Utilidades.puedoObtenerPalabra("TRAJE"));
	}
	@Test
	public void setInicial05(){
		assertEquals(true,Utilidades.puedoObtenerPalabra("COMUN"));
	}
	@Test
	public void setInicial06(){
		assertEquals(false,Utilidades.puedoObtenerPalabra("CAMPANA"));
	}
	@Test
	public void setInicial07(){
		assertEquals(true,Utilidades.puedoObtenerPalabra("DORITO"));
	}
	@Test
	public void setInicial08(){
		assertEquals(true,Utilidades.puedoObtenerPalabra("ARLEQUIN"));
	}
}