import java.io.*;
import java.util.*;
public class Tokenizer{
	private InputStream entrada;
	private int residuo=-1;
	public String actual=null;
	private int indice=0;
	private ArrayList<String>lista;
	private boolean guardando;
	private int puntero=0;
	private HashMap<String,Integer>ignorados;
	public boolean ignorar=true;
	public Elemento elemento;
	public ArrayList<Elemento>observadores;
	public boolean activarObservadores=true;
	public StringBuffer codigo;
	public int linea=1;
	public int columna=1;
	public boolean alfaNumerico=false;
	public Tokenizer(InputStream entrada){
		this.entrada=entrada;
		lista=new ArrayList<String>();
		ignorados=new HashMap<String,Integer>();
		ignorados.put(((char)13)+"",1);
		ignorados.put(((char)9)+"",1);
		ignorados.put(" ",1);
		observadores=new ArrayList<Elemento>();
	}
	public boolean esAlfaNumerico(int c){
		return Character.isLetterOrDigit(c);
	}
	public boolean hasNext()throws Exception{
		if(!guardando&&indice<lista.size()){
			return true;
		}
		if(residuo>=0){
			return true;
		}
		residuo=leer();
		if(residuo>=0){
			return true;
		}
		return false;
	}
	public void guardar(){
		if(!guardando){
			lista=new ArrayList<String>();
			lista.add(actual);
			guardando=true;
		}
	}
	public void cargar()throws Exception{
		if(guardando){
			indice=1;
			setActual(lista.get(0));
			guardando=false;
		}
	}
	public void borrar(){
		lista=new ArrayList<String>();
		guardando=false;
	}
	public String next()throws Exception{
		setActual(siguiente());
		if(this.ignorar){
			while(this.hasNext()&&ignorados.get(actual)!=null){
				//System.out.println("["+actual+"]");
				setActual(siguiente());
			}
		}
		if(guardando){
			lista.add(actual);
		}
		return actual;
	}
	private String siguiente()throws Exception{
		if(!guardando&&indice<lista.size()){
			return lista.get(indice++);
		}
		StringBuffer sb=new StringBuffer();
		int letra=-1;
		if(residuo>=0){
			letra=residuo;
		}else{
			letra=leer();
		}
		if(letra<0){
			return null;
		}
		sb.append((char)letra);
		if(esAlfaNumerico(letra)){
			residuo=leer();
			while(esAlfaNumerico(residuo)){
				sb.append((char)residuo);
				residuo=leer();
			}
		}else{
			if(letra==13){
				linea++;
				residuo=leer();
				if(residuo==10){
					residuo=-1;
				}
				columna=1;
			}else{
				residuo=-1;
			}
		}
		return sb+"";
	}
	public int leer()throws Exception{
		puntero++;
		columna++;
		int b=entrada.read();
		if(codigo!=null){
			codigo.append((char)b);
		}
		return b;
	}
	public String mostrarEstado()throws Exception{
		StringBuffer resultado=new StringBuffer();
		resultado.append("residuo:");
		resultado.append(residuo);
		resultado.append("\n");
		resultado.append("actual:");
		resultado.append(actual);
		resultado.append("\n");
		resultado.append("indice:");
		resultado.append(indice);
		resultado.append("\n");
		resultado.append("lista:");
		resultado.append(lista);
		resultado.append("\n");
		resultado.append("guardando:");
		resultado.append(guardando);
		resultado.append("\n");
		resultado.append("puntero:");
		resultado.append(puntero);
		resultado.append("\n");
		return resultado+"";
	}
	public void setActual(String actual)throws Exception{
		this.actual=actual;
		alfaNumerico=false;
		if(esAlfaNumerico(actual.codePointAt(0))){
			alfaNumerico=true;
		}
		if(activarObservadores){
			activarObservadores=false;
			for(int i=0;i<observadores.size();i++){
				observadores.get(i).c(this);
			}
			activarObservadores=true;
		}
	}
}