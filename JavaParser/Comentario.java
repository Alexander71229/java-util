import java.io.*;
public class Comentario extends Elemento{
	private String texto;
	private int tipo;
	public Comentario(){
	}
	public Comentario(int tipo,String texto){
		this.texto=texto;
		this.tipo=tipo;
	}
	public Elemento c(Tokenizer t)throws Exception{
		return Comentario.comprobar(t);
	}
	public static Comentario comprobar(Tokenizer t)throws Exception{
		t.guardar();
		if(t.actual.equals("/")){
			t.next();
			if(t.actual.equals("/")){
				t.ignorar=false;
				t.borrar();
				StringBuffer sb=new StringBuffer();
				while(t.hasNext()){
					t.next();
					if(t.actual.codePointAt(0)==13){
						break;
					}
					sb.append(t.actual);
				}
				t.ignorar=true;
				return new Comentario(0,sb+"");
			}
			if(t.actual.equals("*")){
				t.ignorar=false;	
				t.borrar();
				StringBuffer sb=new StringBuffer();
				int c=0;
				while(t.hasNext()){
					t.next();
					if(c==1){
						if(t.actual.codePointAt(0)=='/'){
							break;
						}else{
							c=0;
							sb.append('*');
						}
					}
					if(t.actual.codePointAt(0)=='*'){
						c++;
						continue;
					}
					sb.append(t.actual);
				}
				t.ignorar=true;
				t.next();
				return new Comentario(1,sb+"");
			}
		}
		t.cargar();
		return null;
	}
	public void imprimir(OutputStream os,int formato)throws Exception{
		PrintStream ps=new PrintStream(os);
		if(formato==0){
			if(tipo==0){
				ps.println("//"+this.texto);
			}
			if(tipo==1){
				ps.println("/*"+this.texto+"*/");
			}
		}
	}
}