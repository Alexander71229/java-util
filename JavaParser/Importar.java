import java.io.*;
public class Importar extends Elemento{
	private String texto;
	public Importar(){
	}
	public Importar(String texto){
		this.texto=texto;
	}
	public static Importar comprobar(Tokenizer t)throws Exception{
		if(t.actual.equals("import")){
			int linea=t.linea;
			int columna=t.columna;
			StringBuffer sb=new StringBuffer();
			while(t.hasNext()){
				t.next();
				if(t.actual.codePointAt(0)==';'){
					break;
				}
				sb.append(t.actual);
			}
			return new Importar(sb+"");
		}
		return null;
	}
	public void imprimir(OutputStream os,int formato)throws Exception{
		PrintStream ps=new PrintStream(os);
		if(formato==0){
			ps.println("import "+this.texto+";");
		}
	}
	public Elemento c(Tokenizer t)throws Exception{
		return Importar.comprobar(t);
	}
}