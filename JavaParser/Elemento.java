import java.util.*;
import java.io.*;
public class Elemento{
	public int linea;
	public int columna;
	private ArrayList<Elemento>elementos;
	public void add(Elemento e){
		if(e!=null){
			if(elementos==null){
				elementos=new ArrayList<Elemento>();
			}
			elementos.add(e);
		}
	}
	public void imprimir(OutputStream os,int formato)throws Exception{
		if(elementos!=null){
			for(int i=0;i<elementos.size();i++){
				if(elementos.get(i)!=null){
					elementos.get(i).imprimir(os,formato);
				}
			}
		}
	}
	public ArrayList<Elemento>getElementos(){
		return elementos;
	}
	public Elemento c(Tokenizer t)throws Exception{
		return this;
	}
}