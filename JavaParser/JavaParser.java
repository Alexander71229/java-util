import java.io.*;
import java.util.*;
public class JavaParser{
	public Programa programa;
	public JavaParser(){
		programa=new Programa();
	}
	public void parse(InputStream is)throws Exception{
		Tokenizer t=new Tokenizer(is);
		t.observadores.add(new Comentario());
		ArrayList<Elemento>elementos=new ArrayList<Elemento>();
		elementos.add(new Paquete());
		elementos.add(new Importar());
		elementos.add(new Clase());
		while(t.hasNext()){
			t.next();
			for(int i=0;i<elementos.size();i++){
				programa.add(elementos.get(i).c(t));
			}
		}
	}
}