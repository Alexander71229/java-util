import java.io.*;
public class Paquete extends Elemento{
	private String nombre;
	public Paquete(){
	}
	public Paquete(String nombre){
		this.nombre=nombre;
	}
	public static Paquete comprobar(Tokenizer t)throws Exception{
		if(t.actual.equals("package")){
			int linea=t.linea;
			int columna=t.columna;
			StringBuffer sb=new StringBuffer();
			while(t.hasNext()){
				t.next();
				if(t.actual.codePointAt(0)==';'){
					break;
				}
				sb.append(t.actual);
			}
			return new Paquete(sb+"");
		}
		return null;
	}
	public void imprimir(OutputStream os,int formato)throws Exception{
		PrintStream ps=new PrintStream(os);
		if(formato==0){
			ps.println("package "+this.nombre+";");
		}
	}
	public Elemento c(Tokenizer t)throws Exception{
		return Paquete.comprobar(t);
	}
}