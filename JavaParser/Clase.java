import java.io.*;
import java.util.*;
public class Clase extends Elemento{
	private String nombre;
	private String padre;
	private String modificador;
	private ArrayList<String>interfaces;
	public Clase(){
		this.interfaces=new ArrayList<String>();
	}
	public Elemento c(Tokenizer t)throws Exception{
		return Clase.comprobar(t);
	}
	public static Clase comprobar(Tokenizer t)throws Exception{
		Clase c=new Clase();
		if(t.actual.equals("public")||t.actual.equals("private")){
			c.modificador=t.actual;
			t.next();
		}
		if(t.actual.equals("class")){
			c.nombre=t.next();
			System.out.println(":"+t.actual);
			t.next();
			if(t.actual.equals("extends")){
				c.padre=t.next();
				t.next();
			}
			if(t.actual.equals("implements")){
				t.next();
				while(!t.actual.equals("{")){
					if(!t.actual.equals(",")){
						c.interfaces.add(t.actual);
					}
					t.next();
				}
			}
			ArrayList<Elemento>elementos=new ArrayList<Elemento>();
			elementos.add(new VariableClase());
			while(t.hasNext()&&!t.actual.equals("}")){
				for(int i=0;i<elementos.size();i++){
					c.add(elementos.get(i).c(t));
				}
				t.next();
			}
			return c;
		}
		return null;
	}
	public void imprimir(OutputStream os,int formato)throws Exception{
		PrintStream ps=new PrintStream(os);
		if(formato==0){
			StringBuffer cadena=new StringBuffer();
			if(this.modificador!=null){
				cadena.append(this.modificador);
				cadena.append(" ");
			}
			cadena.append("class ");
			cadena.append(this.nombre);
			if(this.padre!=null){
				cadena.append(" extends ");
				cadena.append(this.padre);
			}
			if(this.interfaces.size()>0){
				cadena.append(" implements ");
				for(int i=0;i<this.interfaces.size();i++){
					if(i>0){
						cadena.append(",");
					}
					cadena.append(this.interfaces.get(i));
				}
			}
			cadena.append("{");
			ps.println(cadena+"");
			super.imprimir(os,formato);
			ps.println("}");
		}
	}
}