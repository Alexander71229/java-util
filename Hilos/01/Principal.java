import java.util.*;
public class Principal{
	public static void a(String nombre)throws Exception{
		System.out.println("Inicia:"+nombre);
		int tiempo=(new Random()).nextInt(10000);
		long tiempoInicial=(new Date()).getTime();
		while((new Date()).getTime()-tiempoInicial<tiempo){
			System.out.println("Ejecutando:"+nombre);
			try{
				Thread.sleep(500);
			}catch(Exception e){}
		}
		System.out.println("Fin:"+nombre);
	}
	public static void main(String[]argumentos){
		try{
			SincronizarHilos s=new SincronizarHilos();
			s.agregarEjecutable(new SincronizarHilos.Ejecutable(){
				public void ejecutar()throws Exception{
					a("A");
				}
			});
			s.agregarEjecutable(new SincronizarHilos.Ejecutable(){
				public void ejecutar()throws Exception{
					a("B");
				}
			});
			s.agregarEjecutable(new SincronizarHilos.Ejecutable(){
				public void ejecutar()throws Exception{
					a("C");
				}
			});
			s.agregarEjecutable(new SincronizarHilos.Ejecutable(){
				public void ejecutar()throws Exception{
					a("D");
				}
			});
			s.agregarEjecutable(new SincronizarHilos.Ejecutable(){
				public void ejecutar()throws Exception{
					a("E");
				}
			});
			s.agregarFinalizador(new SincronizarHilos.Ejecutable(){
				public void ejecutar()throws Exception{
					System.out.println("Lógica de finalización");
					System.exit(0);
				}
			});
			s.ejecutar();
			Thread.sleep(30000);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}