import java.util.*;
public class SincronizarHilos{
	private boolean ejecutando;
	public interface Ejecutable{
		public void ejecutar()throws Exception;
	}
	private ArrayList<Ejecutable>ejecutables;
	private HashMap<Ejecutable,Integer>hash;
	private ArrayList<Ejecutable>finalizadores;
	private ArrayList<Exception>excepciones;
	public SincronizarHilos(){
		ejecutables=new ArrayList<Ejecutable>();
		finalizadores=new ArrayList<Ejecutable>();
		excepciones=new ArrayList<Exception>();
		hash=new HashMap<Ejecutable,Integer>();
	}
	public void agregarEjecutable(Ejecutable ejecutable){
		if(ejecutando){
			return;
		}
		if(hash.get(ejecutable)==null){
			ejecutables.add(ejecutable);
			hash.put(ejecutable,1);
		}
	}
	public void agregarFinalizador(Ejecutable ejecutable){
		if(ejecutando){
			return;
		}
		finalizadores.add(ejecutable);
	}
	public void ejecutar(){
		SincronizarHilos s=this;
		hash=new HashMap<Ejecutable,Integer>();
		ejecutando=true;
		for(int i=0;i<ejecutables.size();i++){
			final Ejecutable ejecutable=ejecutables.get(i);
			(new Thread(){
				public void run(){
					try{
						ejecutable.ejecutar();
					}catch(Exception e){
						excepciones.add(e);
					}
					s.notificar(ejecutable);
				}
			}).start();
		}
	}
	public void notificar(Ejecutable ejecutable){
		hash.put(ejecutable,1);
		if(this.hash.size()==this.ejecutables.size()){
			for(int i=0;i<this.finalizadores.size();i++){
				try{
					this.finalizadores.get(i).ejecutar();
				}catch(Exception e){
					excepciones.add(e);
				}
			}
		}
		ejecutando=false;
	}
	public ArrayList<Exception>obtenerExcepciones(){
		return this.excepciones;
	}
}