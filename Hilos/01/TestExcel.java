import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.*;
import java.util.*;
import java.io.*;
public class TestExcel{
	public static void escribir(String nombre,SXSSFSheet hoja)throws Exception{
		for(int i=0;i<90000;i++){
			Row row=null;
			//synchronized(TestExcel.class){
				row=hoja.createRow(i);
			//}
			for(int j=0;j<100;j++){
				//synchronized(TestExcel.class){
					row.createCell(j).setCellValue(nombre+":"+i+","+j);
				//}
			}
			//System.out.println(nombre);
			//Thread.sleep(500);
		}
	}
	public static void escribir(String nombre,SXSSFWorkbook wb)throws Exception{
		SXSSFSheet hoja=null;
		synchronized(TestExcel.class){
			hoja=(SXSSFSheet)wb.createSheet(nombre);
		}
		escribir(nombre,hoja);
	}
	public static void main(String[]argumentos){
		try{
			SincronizarHilos s=new SincronizarHilos();
			SXSSFWorkbook wb=new SXSSFWorkbook(2);
			/*escribir("Hoja1",wb);
			escribir("Hoja2",wb);
			wb.write(new FileOutputStream(new File("output.xlsx")));
			System.exit(0);*/
			/*try{new java.io.PrintStream(new FileOutputStream(new File("Salida.txt"),true)).println("Inicio");}catch(Exception e){}
			try{new java.io.PrintStream(new FileOutputStream(new File("Salida.txt"),true)).println("Fin");}catch(Exception e){}*/
			for(int i=0;i<3;i++){
				final String nombre="Esquema"+i;
				s.agregarEjecutable(new SincronizarHilos.Ejecutable(){
					public void ejecutar()throws Exception{
						escribir(nombre,wb);
					}
				});
			}
			s.agregarFinalizador(new SincronizarHilos.Ejecutable(){
				public void ejecutar()throws Exception{
					wb.write(new FileOutputStream(new File("output.xlsx")));
				}
			});
			s.ejecutar();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}