import java.io.*;
import java.util.*;
public class Act{
	public static void main(String[]args){
		try{
			PrintStream ps=new PrintStream(new FileOutputStream(new File("Act.txt"),true));
			Scanner scannerInput=new Scanner(System.in);
			String line="";
			long momentoAnterior=0;
			long tiempoCambio=10000;
			while(true){
				long momento=(new Date()).getTime();
				line=scannerInput.nextLine();
				if(line.equals("E")){
					break;
				}
				if(momento-momentoAnterior>tiempoCambio){
					ps.println("XxxX "+momento);
				}
				ps.println(line);
				momentoAnterior=momento;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}