import java.io.*;
public class Lib{
	public static String jar(String ruta)throws Exception{
		Class clase=Class.forName(ruta);
		return clase.getResource("/"+ruta.replaceAll("\\.","/")+".class")+"";
	}
	public static void main(String[]argumentos){
		PrintStream ps=null;
		try{
			ps=new PrintStream(new FileOutputStream(new File("log.txt")));
			//ps.println(jar("com.jdedwards.database.jdb.JDBAdministrator"));
			//ps.println(jar("com.jdedwards.database.impl.physical.JDBConnectionManager"));
			//ps.println(jar("com.jdedwards.mgmt.agent.E1AgentUtils"));
			//ps.println(jar("oracle.xml.parser.v2.XMLParseException"));
			//ps.println(jar("org.apache.http.client.methods.HttpUriRequest"));
			//ps.println(jar("org.apache.http.HttpRequest"));
			//ps.println(jar("com.peoplesoft.pt.e1.ppm.api.IPSPerf"));
			//ps.println(jar("com.jdedwards.system.connector.dynamic.Connector"));
			//ps.println(jar("com.jdedwards.system.bsfn.IUserSession"));
			//ps.println(jar("com.jdedwards.system.net.JdeNetUtil"));
			//ps.println(jar("org.apache.commons.codec.binary.Base64"));
			//ps.println(jar("com.jdedwards.base.spec.DDInfo"));
			//ps.println(jar("com.jdedwards.jas.services.dstrule.DSTRuleLookup"));
			ps.println(jar("javax.xml.ws.Service"));
		}catch(Throwable t){
			t.printStackTrace(ps);
		}
	}
}