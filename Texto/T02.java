import java.util.*;
import java.io.*;
public class T02{
	public static void main(String[]argumentos){
		try{
			int n=10;
			Scanner s=new Scanner(new File("e02.txt"));
			//int i=0;
			ArrayList<String>a=new ArrayList<String>();
			while(s.hasNext()){
				String p=s.next();
				a.add(p);
				//i++;
			}
			int k=1000;//3861
			StringBuilder cm=new StringBuilder("BEGIN");
			for(int i=0;i<a.size();i++){
				if(i%k==0){
					if(i>0){
						cm.append(");");
					}
					cm.append("\n\tUPDATE tortr SET estacodi=0,prdicons=0,prdifepr=0 WHERE ortrcons IN(");
					cm.append(a.get(i));
					continue;
				}
				cm.append(",");
				cm.append(a.get(i));
			}
			cm.append(");\n\tUPDATE tortr SET estacodi=0,prdicons=0,prdifepr=0 WHERE progcodi=2 AND estacodi IN(8,9);\n\tCOMMIT;\nEND;");
			PrintStream ps=new PrintStream(new FileOutputStream(new File("WO0000000187439.sql")));
			ps.print(cm);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}