<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.2" name="9445zs" tilewidth="16" tileheight="16" spacing="1" margin="1" tilecount="600" columns="24">
 <image source="../Tiles/01/9445.png" width="410" height="427"/>
 <tile id="93">
  <properties>
   <property name="n" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="117">
  <properties>
   <property name="n" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="141">
  <properties>
   <property name="n" type="int" value="1"/>
  </properties>
 </tile>
</tileset>
