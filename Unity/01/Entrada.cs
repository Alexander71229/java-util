using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Entrada{
	public static float Sign(float n){
		if(n<0){
			return -1;
		}
		if(n>0){
			return 1;
		}
		return 0;
	}
	public static Vector2 mover2d(float vx,float vy){
		return new Vector2(Sign(Input.GetAxis("Horizontal"))*vx,Sign(Input.GetAxis("Vertical"))*vy);
	}
}
