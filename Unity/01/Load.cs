using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class Load:MonoBehaviour{
	public TextAsset mapa;
	public String nombre;
	public Grid grid;
	public Tile tile;
	void Start(){
		if(grid==null){
			grid=new GameObject("Grid").AddComponent<Grid>();
		}
		char[]mc=mapa.text.ToCharArray();
		GameObject o=new GameObject(nombre);
		o.AddComponent<Tilemap>();
		o.transform.SetParent(grid.transform);
		o.AddComponent<TilemapRenderer>();
		o.AddComponent<TilemapCollider2D>();
		o.AddComponent<Rigidbody2D>().bodyType=RigidbodyType2D.Static;
		o.AddComponent<CompositeCollider2D>();
		o.GetComponent<Tilemap>().SetTile(new Vector3Int(0,0,0),tile);
	}
}
