using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Destruir:MonoBehaviour{
	private float t=-1;
	public float duracion=5;
	void Update(){
		if(t==-1){
			t=Time.time;
			return;
		}
		if(Time.time-t>duracion){
			Destroy(gameObject);
		}
	}
}
