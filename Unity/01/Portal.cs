using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class Portal:MonoBehaviour{
	void OnTriggerStay2D(Collider2D c){
		if(c.gameObject.tag=="Player"){
			Bounds bs=c.bounds;
			Vector3 min=bs.min;
			Vector3 max=bs.max;
			Vector3[]ps=new Vector3[4];
			ps[0]=new Vector3(min.x,min.y,0);
			ps[1]=new Vector3(max.x,min.y,0);
			ps[2]=new Vector3(max.x,max.y,0);
			ps[3]=new Vector3(min.x,max.y,0);
			for(int i=0;i<ps.Length;i++){
				Vector3Int p=GetComponent<Tilemap>().WorldToCell(ps[i]);
				if(GetComponent<Tilemap>().HasTile(p)){
					GetComponent<Tilemap>().SetTile(p,null);
					Controlador.n("Collectar",gameObject,c.gameObject,1);
				}
			}
		}
	}
}
