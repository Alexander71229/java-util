using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AleatorioMovimiento01:MonoBehaviour{
	public float min=1;
	public float max=4;
	public float rate=1;
	private float t=-100;
	void Start(){
		if(gameObject.GetComponent<Movimiento01>()!=null){
			gameObject.AddComponent<Movimiento01>();
		}
	}
	void FixedUpdate(){
		if(Time.time-t>rate){
			t=Time.time;
			gameObject.GetComponent<Movimiento01>().frecuencia=Random.Range(min,max);
			print(gameObject.GetComponent<Movimiento01>().frecuencia);
		}
	}
}
