using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Evento{
	public delegate void Accion(GameObject a,GameObject b,float v);
	public event Accion accion;
	public void lanzar(GameObject a,GameObject b,float v){
		if(accion!=null){
			accion(a,b,v);
		}
	}
}
