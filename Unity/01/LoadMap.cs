using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class LoadMap:MonoBehaviour{
	void Start(){
		String m=Resources.Load<TextAsset>("M01").text;
		char[]mc=m.ToCharArray();
		int y=0;
		int x=0;
		GameObject es=new GameObject("Bloques");
		Rigidbody2D a1=es.AddComponent<Rigidbody2D>()as Rigidbody2D;
		a1.bodyType=RigidbodyType2D.Static;
		es.AddComponent<CompositeCollider2D>();
		for(int i=0;i<mc.Length;i++){
			if(mc[i]=='\n'){
				y++;
				x=-1;
			}
			if(mc[i]=='p'){
				GameObject o=new GameObject();
				SpriteRenderer s=o.AddComponent<SpriteRenderer>()as SpriteRenderer;
				s.color=Color.yellow;
				s.sprite=Resources.Load<Sprite>("Square")as Sprite;
				o.transform.position=new Vector3(x-.5f,-y+.5f,0);
				o.AddComponent<Rigidbody2D>();
				BoxCollider2D bc=o.AddComponent<BoxCollider2D>()as BoxCollider2D;
				continue;
			}
			if(mc[i]=='#'){
				GameObject o=new GameObject();
				SpriteRenderer s=o.AddComponent<SpriteRenderer>()as SpriteRenderer;
				s.color=Color.red;
				s.sprite=Resources.Load<Sprite>("Square")as Sprite;
				o.transform.position=new Vector3(x-.5f,-y+.5f,0);
				BoxCollider2D bc=es.AddComponent<BoxCollider2D>()as BoxCollider2D;
				bc.offset=new Vector2(x-.5f,-y+.5f);
				bc.usedByComposite=true;
			}
			x++;
		}
	}
	void Update(){
	}
}
