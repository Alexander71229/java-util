using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretControl : MonoBehaviour
{
	public float fireRate=19f;
	public Vector3 offset=new Vector3(0,0,0);
	private float t=-100;
	public Sprite sprite;
	public Vector2 velocity=new Vector2(0,1);
	void Start()
	{
	}
	void Update()
	{
		if(Time.time-t>=fireRate&&Input.GetButton("Fire1")){
			t=Time.time;
			fire();
		}
	}
	void fire(){
		GameObject b=new GameObject();
		b.tag="Bullet";
		b.AddComponent<SpriteRenderer>();
		b.GetComponent<SpriteRenderer>().sprite=sprite;
		b.transform.position=gameObject.transform.position+offset;
		b.AddComponent<Rigidbody2D>();
		b.GetComponent<Rigidbody2D>().gravityScale=0;
		b.AddComponent<BoxCollider2D>();
		b.GetComponent<BoxCollider2D>().isTrigger=true;
		b.GetComponent<Rigidbody2D>().velocity=velocity;
	}
}
