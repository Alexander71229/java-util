using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Controlador:MonoBehaviour{
	public static IDictionary<string,Evento>d;
	public static void init(){
		if(d==null){
			d=new Dictionary<string,Evento>();
		}
	}
	public static void init(string k){
		init();
		if(!d.ContainsKey(k)){
			d.Add(k,new Evento());
		}
	}
	public static void n(string k,GameObject a,GameObject b,float v){
		init(k);
		d[k].lanzar(a,b,v);
	}
	public static Evento r(string k){
		init(k);
		return d[k];
	}
}