using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Xml;
public class CrearSprites:MonoBehaviour{
	public Texture2D imagen;
	public int ancho;
	public int alto;
	public int espaciox;
	public int espacioy;
	public int iniciox;
	public int inicioy;
	void cortar(){
		int dx=espaciox+ancho;
		int dy=espacioy+alto;
		int id=0;
		for(int y=inicioy;y<=imagen.height-dy;y+=dy){
			for(int x=iniciox;x<=imagen.width-dx;x+=dx){
				Debug.Log(x+":"+y);
				Sprite sprite=Sprite.Create(imagen,new Rect(x,y,ancho,alto),new Vector2(0.5f, 0.5f));
				AssetDatabase.CreateAsset(sprite,"Assets/Resources/Sprites/"+(imagen.name+id)+".asset");
				AssetDatabase.SaveAssets();
				Resources.UnloadAsset(sprite);
				Resources.Load<Sprite>(imagen.name+id);
				id++;
			}
		}
	}
	void Start(){
		cortar();
	}
}
