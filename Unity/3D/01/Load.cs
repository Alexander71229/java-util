using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.Tilemaps;
public class Load:MonoBehaviour{
	public TextAsset mapa;
	public String nombre;
	void load(){
		string txt=mapa.text;
		XmlDocument doc=new XmlDocument();
		doc.LoadXml(txt);
		XmlNodeList data=doc.GetElementsByTagName("data");
		string csv=data[0].InnerXml;
		int w=int.Parse(doc.GetElementsByTagName("map")[0].Attributes["width"].Value);
		int h=int.Parse(doc.GetElementsByTagName("map")[0].Attributes["height"].Value);
		int tw=int.Parse(doc.GetElementsByTagName("map")[0].Attributes["tilewidth"].Value);
		int th=int.Parse(doc.GetElementsByTagName("map")[0].Attributes["tileheight"].Value);
		string[]datos=csv.Split(",");
		int k=0;
		GameObject o=new GameObject("Mapa2D");
		o.AddComponent<Tilemap>();
		o.transform.SetParent(new GameObject("Grid").AddComponent<Grid>().transform);
		o.AddComponent<TilemapRenderer>();
		for(int y=0;y<h;y++){
			for(int x=0;x<w;x++){
				int id=int.Parse(datos[k]);
				if(id>0){
					id--;
					Tile tile=ScriptableObject.CreateInstance<Tile>();
					tile.sprite=Resources.Load<Sprite>("Sprites/"+(nombre+id))as Sprite;
					//o.GetComponent<Tilemap>().cellSize=new Vector3Int(tw,th,0);
					o.GetComponent<Tilemap>().SetTile(new Vector3Int(x,y,0),tile);
					GridLayout gridLayout=o.GetComponent<Tilemap>().layoutGrid;
					GridLayout.CellLayout cellLayout=new GridLayout.CellLayout();
					cellLayout.cellSize=new Vector3Int(tw,th,0);
					gridLayout.SetLayoutCell(o.GetComponent<Tilemap>().LocalToCell(o.GetComponent<Tilemap>().cellBounds.min),cellLayout);
				}
				k++;
			}
		}
	}
	void Start(){
		load();
	}
}
