import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;
interface I{
	public void m();
	public void z();
}
class C implements I{
	@Override
	public void m(){
		System.out.println(this);
	}
	@Override
	public void z(){
		System.out.println("z");
	}
}
class X implements InvocationHandler{
	private Object o;
	public X(Object o){
		this.o=o;
	}
	@Override
	public Object invoke(Object proxy,Method method,Object[]args)throws Throwable{
		System.out.println("antes");
		Object resultado=method.invoke(o,args);c.U.traza();
		System.out.println("después");
		return resultado;
	}
}
public class C01{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="Log.txt";
			//new C().m();
			C o=new C();
			o.m();
			I x=(I)Proxy.newProxyInstance(I.class.getClassLoader(),new Class[]{I.class},new X(o));
			x.z();
			Proxy a=(Proxy)x;
			c.U.imp(x.getClass().getSuperclass().getName());
			c.U.imp(x.getClass().getTypeName());
			Arrays.stream(x.getClass().getInterfaces()).map(z->z.toString()).forEach(c.U::imp);
			Arrays.stream(x.getClass().getClasses()).map(z->z.toString()).forEach(c.U::imp);
			Arrays.stream(x.getClass().getDeclaredClasses()).map(z->z.toString()).forEach(c.U::imp);
			Arrays.stream(x.getClass().getFields()).map(z->z.toString()).forEach(c.U::imp);
			Arrays.stream(x.getClass().getDeclaredFields()).map(z->z.toString()).forEach(c.U::imp);
			Arrays.stream(x.getClass().getDeclaredConstructors()).map(z->z.toString()).forEach(c.U::imp);
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}