import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
class C{
	public void m(){
		System.out.println(this);
	}
	public void z(){
		System.out.println("z");
	}
}
class X implements InvocationHandler{
	private Object o;
	public X(Object o){
		this.o=o;
	}
	@Override
	public Object invoke(Object proxy,Method method,Object[]args)throws Throwable{
		System.out.println("antes");
		Object resultado=method.invoke(o,args);c.U.traza();
		System.out.println("después");
		return resultado;
	}
}
public class C02{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="Log.txt";
			//new C().m();
			C o=new C();
			o.m();
			C x=(C)Proxy.newProxyInstance(C.class.getClassLoader(),new Class[]{C.class},new X(o));
			x.z();
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}