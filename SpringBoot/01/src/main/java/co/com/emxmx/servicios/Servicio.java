package co.com.emxmx.servicios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import co.com.emxmx.repositorio.Repositorio;
@Service
public class Servicio{
	@Autowired
	private Repositorio repositorio;
	public int pruebaInsertar(){
		return repositorio.pruebaInsertar();
	}
}