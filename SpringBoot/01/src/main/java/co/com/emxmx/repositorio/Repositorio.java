package co.com.emxmx.repositorio;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import javax.sql.DataSource;
@Repository
public class Repositorio{
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public Repositorio(final DataSource dataSource){
		this.dataSource=dataSource;
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	public int pruebaInsertar(){
		jdbcTemplate.update("INSERT INTO materiales(idMaterial,Material)VALUES('1','prueba')");
		return 1;
	}
}