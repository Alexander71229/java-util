package co.com.emxmx.controladores;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import co.com.emxmx.servicios.Servicio;
@RestController
@RequestMapping("/prueba")
public class Controlador{
	@Autowired
	private Servicio servicio;
	@RequestMapping(value={"/insertar"},method=RequestMethod.GET)
	public int pruebaInsertar()throws Exception{
		return servicio.pruebaInsertar();
	}
}