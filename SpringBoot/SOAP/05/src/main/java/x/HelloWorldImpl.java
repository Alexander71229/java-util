package x;
@WebService
public interface HelloWorldService {
    String helloWorld();
}
// HelloWorldService.class
@WebService(endpointInterface = "com.example.ws.service.HelloWorldService", serviceName="helloWorldService")
public class HelloWorldImpl implements HelloWorldService{
    @Override
    public String helloWorld() {
        return "Hi";
    }
}