package x;
@Configuration
@EnableAutoConfiguration
@ComponentScan
@ImportResource({"classpath:META-INF/cxf/cxf.xml"})
public class ApplicationApplication extends SpringBootServletInitializer{
		@Autowired
		private ApplicationContext applicationContext;
		public static void main(String[] args) {
				SpringApplication.run(Application.class, args);
		}
		// Replaces web.xml
		@Bean
		public ServletRegistrationBean servletRegistrationBean(ApplicationContext context) {
				ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new CXFServlet(), "/services/*");
				servletRegistrationBean.setLoadOnStartup(1);
				return servletRegistrationBean;
		}
		// Replaces cxf-servlet.xml
		@Bean
		// <jaxws:endpoint id="helloWorld" implementor="demo.spring.service.HelloWorldImpl" address="/HelloWorld"/>
		public EndpointImpl helloService() {
				Bus bus = (Bus) applicationContext.getBean(Bus.DEFAULT_BUS_ID);
				HelloWorldImpl helloWorldService = new HelloWorldImpl();
				Object implementor = helloWorldService;
				EndpointImpl endpoint = new EndpointImpl(bus, implementor);
				endpoint.publish("/helloService");
				endpoint.getServer().getEndpoint().getInInterceptors().add(new LoggingInInterceptor());
				endpoint.getServer().getEndpoint().getOutInterceptors().add(new LoggingOutInterceptor());
				return endpoint;
		}
		// Configure the embedded tomcat to use same settings as default standalone tomcat deploy
		@Bean
		public EmbeddedServletContainerFactory embeddedServletContainerFactory() {
				TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory("/ws-server-example-1.0", 8090);
				return factory;
		}
		// Used when deploying to a standalone servlet container, i.e. tomcat
		@Override
		protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
				return application.sources(Application.class);
		}
}