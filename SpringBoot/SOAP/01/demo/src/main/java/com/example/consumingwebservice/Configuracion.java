package com.example.consumingwebservice;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Value;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;
@Configuration
public class Configuracion{
	@Value("${client.default-uri}")
	private String defaultUri;
	@Value("${client.usuario.login}")
	private String usuario;
	@Value("${client.usuario.clave}")
	private String clave;
	@Bean
	public Jaxb2Marshaller marshaller(){
		Jaxb2Marshaller marshaller=new Jaxb2Marshaller();
		marshaller.setContextPath("https://www.febtw.co:8087/ServiceBTW/FEServicesBTW.svc?wsdl");
		return marshaller;
	}
	@Bean
	public Test01 c(Jaxb2Marshaller marshaller){
		Test01 t=new Test01();
		t.setDefaultUri(defaultUri);
		t.setMarshaller(marshaller);
		t.setUnmarshaller(marshaller);
		t.setMessageSender(httpComponentsMessageSender());
		return t;
	}
  @Bean
  public HttpComponentsMessageSender httpComponentsMessageSender() {
    HttpComponentsMessageSender httpComponentsMessageSender=new HttpComponentsMessageSender();
    httpComponentsMessageSender.setCredentials(usernamePasswordCredentials());
    return httpComponentsMessageSender;
  }
  @Bean
  public UsernamePasswordCredentials usernamePasswordCredentials(){
    return new UsernamePasswordCredentials(usuario,clave);
  }
}