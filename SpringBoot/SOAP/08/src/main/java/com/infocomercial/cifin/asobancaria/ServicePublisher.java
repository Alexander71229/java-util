package com.infocomercial.cifin.asobancaria;

import javax.xml.ws.Endpoint;

public class ServicePublisher {

    public static void main(String[] args) {
        String url = "http://localhost:8080/cambiopassword";
        Endpoint.publish(url, new CambioPasswordService());
        System.out.println("Servicio publicado en: " + url);
    }
}
