package com.infocomercial.cifin.asobancaria;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "CambioPasswordService", targetNamespace = "http://infocomercial.cifin.asobancaria.com")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class CambioPasswordService {

    @WebMethod(operationName = "cambioPassword")
    public String cambioPassword(@WebParam(name = "nuevoPassword") String nuevoPassword) {
        // Aquí puedes realizar cualquier lógica necesaria con el nuevoPassword recibido
				System.out.println("nuevoPassword:"+nuevoPassword);
        return "Password cambiado exitosamente";
    }
}
