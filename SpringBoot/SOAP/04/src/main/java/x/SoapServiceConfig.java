package x;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.interceptor.PayloadLoggingInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.transport.http.WsdlDefinitionHandlerAdapter;
import javax.servlet.ServletRegistration;
import java.util.List;
@Configuration
@EnableWs
public class SoapServiceConfig extends WsConfigurerAdapter {
	@Bean
	public PayloadLoggingInterceptor payloadLoggingInterceptor() {
		return new PayloadLoggingInterceptor();
	}
	@Override
	public void addInterceptors(List<EndpointInterceptor> interceptors) {
		interceptors.add(payloadLoggingInterceptor());
	}
	@Bean
	public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet() {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean<>(servlet, "/ws/*");
	}
	@Bean
	public ServletRegistrationBean<WsdlDefinitionHandlerAdapter> wsdlDefinitionHandlerAdapter() {
		return new ServletRegistrationBean<>(new WsdlDefinitionHandlerAdapter(), "/ws/*");
	}
}
