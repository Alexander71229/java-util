package com.example.soapservice;
import org.springframework.stereotype.Service;
import javax.jws.WebMethod;
import javax.jws.WebService;
@Service
@WebService
public class CalculatorService{
	@WebMethod
	public int add(int a,int b){
		return a+b;
	}
}
