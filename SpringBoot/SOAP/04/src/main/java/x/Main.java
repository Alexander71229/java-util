package x;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.soap.server.endpoint.interceptor.PayloadLoggingInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.transport.http.WsdlDefinitionHandlerAdapter;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;
import javax.servlet.ServletRegistration;
import java.util.List;
@SpringBootApplication
@EnableWs
public class Main extends WsConfigurerAdapter{
	public static void main(String[]args){
		SpringApplication.run(Main.class,args);
	}
	@Bean
	public Wsdl11Definition wsdl11Definition(){
		DefaultWsdl11Definition wsdl11Definition=new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("CalculatorPort");
		wsdl11Definition.setLocationUri("/ws");
		wsdl11Definition.setTargetNamespace("http://example.com/soapservice");
		wsdl11Definition.setSchema(calculatorSchema());
		return wsdl11Definition;
	}
	@Bean
	public ServletRegistrationBean<MessageDispatcherServlet>messageDispatcherServlet(){
		MessageDispatcherServlet servlet=new MessageDispatcherServlet();
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean<>(servlet,"/ws/*");
	}
	@Bean
	public ServletRegistrationBean<WsdlDefinitionHandlerAdapter>wsdlDefinitionHandlerAdapter(){
		return new ServletRegistrationBean<>(new WsdlDefinitionHandlerAdapter(),"/ws/*");
	}
	@Override
	public void addInterceptors(List<EndpointInterceptor>interceptors){
		interceptors.add(new PayloadLoggingInterceptor());
	}
}