package x;
import org.springframework.stereotype.Component;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@Component
public class HelloWorldEndpoint {

    @PayloadRoot(namespace = "http://example.com/soap", localPart = "sayHelloRequest")
    @ResponsePayload
    public SayHelloResponse sayHello(@RequestPayload SayHelloRequest request) {
        SayHelloResponse response = new SayHelloResponse();
        response.setMessage("Hello, " + request.getName() + "!");
        return response;
    }
}
