package x;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
@WebService(endpointInterface="x.Servicio")
//@SOAPBinding(style=SOAPBinding.Style.RPC,use=SOAPBinding.Use.ENCODED,parameterStyle=SOAPBinding.ParameterStyle.WRAPPED,encodingStyle="http://schemas.xmlsoap.org/soap/encoding/")
@SOAPBinding(style=SOAPBinding.Style.RPC,use=SOAPBinding.Use.ENCODED,parameterStyle=SOAPBinding.ParameterStyle.WRAPPED)
public class Servicio{
	@WebMethod
	public String consulta(Objeto o){
		return "Respuesta";
	}
}
