package x;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
@EnableWs
@Configuration
public class SoapServiceConfig extends WsConfigurerAdapter{
	@Bean
	public ServletRegistrationBean<MessageDispatcherServlet>messageDispatcherServlet(){
		MessageDispatcherServlet servlet=new MessageDispatcherServlet();
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean<>(servlet,"/ws/*");
	}
	@Bean(name="exampleService")
	public DefaultWsdl11Definition defaultWsdl11Definition(){
		DefaultWsdl11Definition wsdl11Definition=new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("ExampleServicePort");
		wsdl11Definition.setLocationUri("/ws");
		wsdl11Definition.setTargetNamespace("http://example.com/schemas");
		//wsdl11Definition.setSchema(new ClassPathResource("example.xsd"));
		return wsdl11Definition;
	}
}
