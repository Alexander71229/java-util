import java.util.*;
import java.io.*;
import java.util.stream.Collectors;
class Campo{
	public String nombre;
	public String tipo;
	public int escala;
	public String restriccion="";
	public String tablaReferencia;
	public Campo(String nombre){
		this.nombre=nombre;
	}
	public String d(){
		return nombre+":"+tipo;
	}
	public String toString(){
		return d();
	}
}
class Tabla{
	public String nombre;
	public ArrayList<Campo>cs;
	public HashMap<String,Integer>tipos;
	public Tabla(String nombre){
		this.nombre=nombre;
		cs=new ArrayList<Campo>();
		tipos=new HashMap<String,Integer>();
	}
	public void add(Campo c){
		cs.add(c);
	}
	public String d(){
		StringBuilder sb=new StringBuilder();
		sb.append(this.nombre);
		sb.append("\n");
		for(int i=0;i<this.cs.size();i++){
			sb.append("\t");
			sb.append(""+this.cs.get(i).d());
			sb.append("\n");
		}
		return ""+sb;
	}
	public void actualizarTipos(){
		cs.stream().forEach(x->tipos.put(x.tipo,1));
	}
}
public class Crear{
	public static HashMap<String,String>nombresTabla;
	public static void nombrarTablas(){
		nombresTabla=new HashMap<String,String>();
		nombresTabla.put("TS_ASE_SERVICIOS","Servicio");
		nombresTabla.put("CON_TIPOS","Tipos");
	}
	public static String cap(String c){
		return c.substring(0,1).toUpperCase()+c.substring(1).toLowerCase();
	}
	public static String primerMinuscula(String c){
		return c.substring(0,1).toLowerCase()+c.substring(1);
	}
	public static String formatoCampo(String n){
		String[]p=n.split("_");
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<p.length;i++){
			sb.append(cap(p[i]));
		}
		return primerMinuscula(""+sb);
	}
	public static String obtenerTipo(Campo campo){
		HashMap<String,String>h=new HashMap<String,String>();
		h.put("VARCHAR2","String");
		h.put("DATE","Date");
		String tipo=h.get(campo.tipo);
		if(tipo==null){
			if(campo.escala==0){
				tipo="Long";
			}else{
				tipo="Double";
			}
		}
		return tipo;
	}
	public static String dibujarCampo(String nombre,Campo campo){
		if(nombresTabla.get(campo.tablaReferencia)!=null){
			return "private "+nombresTabla.get(campo.tablaReferencia)+" "+nombresTabla.get(campo.tablaReferencia).toLowerCase();
		}
		return "private "+obtenerTipo(campo)+" "+nombre;
	}
	public static void imprimirClase(String paquete,String nombre,Tabla t)throws Exception{
		t.actualizarTipos();
		PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre+".java")));
		ps.println("package "+paquete+";");
		ps.println("import lombok.*;");
		if(t.tipos.get("DATE")!=null){
			ps.println("import java.util.Date;");
		}
		ps.println("import java.io.Serializable;");
		ps.println("@Getter");
		ps.println("@Setter");
		ps.println("@NoArgsConstructor");
		ps.println("public class "+nombre+" implements Serializable{");
		t.cs.stream().forEach(c->ps.println("\t"+dibujarCampo(formatoCampo(c.nombre),c)+";"));
		ps.println("}");
	}
	public static String crearId(String paquete,String nombre,Tabla tabla)throws Exception{
		List<Campo>campos=tabla.cs.stream().filter(x->x.restriccion.equals("P")).collect(Collectors.toList());
		if(campos.size()<2){
			return "";
		}else{
			String nombreClase=nombre+"Id";
			Tabla t=new Tabla(nombreClase);
			campos.stream().forEach(c->t.add(c));
			imprimirClase(paquete,nombreClase,t);
			return "@IdClass("+nombreClase+".class)\n";
		}
	}
	public static String dibujarTabla(String paquete,String nombre,Tabla tabla)throws Exception{
		StringBuilder r=new StringBuilder();
		r.append("@Entity\n");
		r.append("@Table(name=\""+tabla.nombre+"\")\n");
		r.append("@Getter\n");
		r.append("@Setter\n");
		r.append("@NoArgsConstructor\n");
		r.append(crearId(paquete,nombre,tabla));
		r.append("public class "+nombre+" implements Serializable{\n");
		for(int i=0;i<tabla.cs.size();i++){
			if(tabla.cs.get(i).restriccion.equals("P")){
				r.append("\t");
				r.append("@Id\n");
			}
			if(tabla.cs.get(i).restriccion.equals("R")&&nombresTabla.get(tabla.cs.get(i).tablaReferencia)!=null){
				r.append("\t");
				r.append("@ManyToOne\n");
				r.append("\t");
				r.append("@JoinColumn(name=\""+tabla.cs.get(i).nombre+"\")\n");
			}else{
				r.append("\t");
				r.append("@Column(name=\""+tabla.cs.get(i).nombre+"\")\n");
			}
			r.append("\t");
			r.append(dibujarCampo(formatoCampo(tabla.cs.get(i).nombre),tabla.cs.get(i)));
			r.append(";\n");
		}
		r.append("}");
		return ""+r;
	}
	public static void imprimirTabla(String paquete,String nombre,Tabla t)throws Exception{
		t.actualizarTipos();
		PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre+".java")));
		ps.println("package "+paquete+";");
		ps.println("import lombok.*;");
		ps.println("import javax.persistence.*;");
		if(t.tipos.get("DATE")!=null){
			ps.println("import java.util.Date;");
		}
		ps.println("import java.io.Serializable;");
		ps.println(dibujarTabla(paquete,nombre,t));
	}
	public static void main(String[]argumentos){
		try{
			nombrarTablas();
			U.ruta="Log.txt";
			U.imp("Inicia");
			Scanner s=new Scanner(new File("Datos003.txt"));
			ArrayList<String>tokens=new ArrayList<String>();
			HashMap<String,String>nom=new HashMap<String,String>();
			nom.put("TS_GAR_VEHICULOS","Vehiculos");
			nom.put("TS_GAR_AVALUOS","Avaluos");
			nom.put("TS_GAR_FASECOLDA_VALORES","FasecoldaValores");
			nom.put("CON_TIPOS","Tipos");
			nom.put("TS_TAR_TEMP_BASE_INFORMES","Temp");
			String paquete="co.com.onnovacion.avaluos.sic.entities";
			while(s.hasNext()){
				tokens.add(s.next());
			}
			Tabla t=null;
			for(int i=0;i<tokens.size();){
				if(tokens.get(i).equals("P")){
					t.cs.get(t.cs.size()-1).restriccion=tokens.get(i++);
				}
				if(i>=tokens.size()){
					break;
				}
				if(tokens.get(i).equals("R")){
					//t.cs.get(t.cs.size()-1).restriccion=tokens.get(i++);
					//t.cs.get(t.cs.size()-1).tablaReferencia=tokens.get(i++);
					i++;i++;
				}
				if(t==null){
					t=new Tabla(tokens.get(i));
				}else{
					if(!t.nombre.equals(tokens.get(i))){
						imprimirTabla(paquete,nom.get(t.nombre),t);
						t=new Tabla(tokens.get(i));
					}
				}
				i++;
				t.add(new Campo(tokens.get(i++)));
				t.cs.get(t.cs.size()-1).tipo=tokens.get(i++);
				t.cs.get(t.cs.size()-1).escala=Integer.parseInt(tokens.get(i++));
			}
			imprimirTabla(paquete,nom.get(t.nombre),t);
		}catch(Throwable t){
			U.imp(t);
		}
	}
}