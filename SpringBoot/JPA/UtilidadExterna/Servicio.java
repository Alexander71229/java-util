package test.dominio;
@Entity
@Table(name="TS_ASE_SERVICIOS")
@Getter
@Setter
@@NoArgsConstructor
public class Servicio{
	@ManyToOne
	@JoinColumn(name="TIPO_DESTINO")
	private Tipos tipos
	@Column(name="ATIPPER_LIQUIDACION")
	private String ATIPPER_LIQUIDACION
	@Column(name="ATIPPER_PAGO")
	private String ATIPPER_PAGO
	@ManyToOne
	@JoinColumn(name="TIPO_CAUSAL_CANCELACION")
	private Tipos tipos
	@Column(name="ADEPEN_CANCELACION")
	private Long ADEPEN_CANCELACION
	@Column(name="ADEPEN_ASIGNADA")
	private Long ADEPEN_ASIGNADA
	@ManyToOne
	@JoinColumn(name="TIPO_ESTADO_DECISION")
	private Tipos tipos
	@Column(name="PPEROCU_ID")
	private Long PPEROCU_ID
	@ManyToOne
	@JoinColumn(name="ASERVIC_SOBREGIRO")
	private Servicio servicio
	@Column(name="ASE_CONSECUTIVO_ASESORIA")
	private Long ASE_CONSECUTIVO_ASESORIA
	@Column(name="AFORPAG_NOMBRE")
	private String AFORPAG_NOMBRE
	@Column(name="PDIREC_ID")
	private Long PDIREC_ID
	@Column(name="CPUC_AEMPRES_CODIGO")
	private Long CPUC_AEMPRES_CODIGO
	@Column(name="CPUC_CUENTA")
	private Long CPUC_CUENTA
	@Column(name="SEG_ID")
	private Long SEG_ID
	@ManyToOne
	@JoinColumn(name="ASERVIC_DESEMBOLSO_PARCIAL")
	private Servicio servicio
	@ManyToOne
	@JoinColumn(name="ASERVIC_RENOVACION")
	private Servicio servicio
	@Column(name="USU_USUARIO")
	private String USU_USUARIO
	@Column(name="AHTIPO_CUENTA")
	private String AHTIPO_CUENTA
	@Column(name="AHAUTORIZA_CANCELAR_CAJA")
	private String AHAUTORIZA_CANCELAR_CAJA
	@Column(name="AHESTA_BLOQUEADO")
	private String AHESTA_BLOQUEADO
	@Column(name="AHESTA_EMBARGADO")
	private String AHESTA_EMBARGADO
	@Column(name="AHNUMERO_CUENTA_CORRIENTE")
	private String AHNUMERO_CUENTA_CORRIENTE
	@Column(name="AHFECHA_INICIO_AHORRO")
	private Date AHFECHA_INICIO_AHORRO
	@Column(name="AHFECHA_ULTIMA_RENOVACION")
	private Date AHFECHA_ULTIMA_RENOVACION
	@Column(name="AHVALOR_ULTIMA_RENOVACION")
	private Double AHVALOR_ULTIMA_RENOVACION
	@Column(name="AHMONTO_APERTURA")
	private Double AHMONTO_APERTURA
	@Column(name="AHDESTINO_INTERES")
	private String AHDESTINO_INTERES
	@Column(name="AHACEPTA_APERTURA_AUTOMATICA")
	private String AHACEPTA_APERTURA_AUTOMATICA
	@Column(name="AHNUMERO_TITULO")
	private String AHNUMERO_TITULO
	@Column(name="AFFORMA_AMORTIZACION")
	private String AFFORMA_AMORTIZACION
	@Column(name="AFMODALIDAD_COBRO")
	private String AFMODALIDAD_COBRO
	@Column(name="AFSERIE")
	private Long AFSERIE
	@Column(name="AFCONSECUTIVO")
	private Long AFCONSECUTIVO
	@Column(name="AFVALOR_AFILIACION")
	private Double AFVALOR_AFILIACION
	@Column(name="CRTIPO_CREDITO")
	private String CRTIPO_CREDITO
	@Column(name="CRCOMPROMETE_CESANTIAS")
	private String CRCOMPROMETE_CESANTIAS
	@Column(name="CRVALOR_CREDITO")
	private Double CRVALOR_CREDITO
	@Column(name="CRVALOR_DESEMBOLSO")
	private Double CRVALOR_DESEMBOLSO
	@Column(name="CRTIPO_AMORTIZACION")
	private String CRTIPO_AMORTIZACION
	@Column(name="CRCUOTA_TOTAL_LIQUIDACION")
	private Double CRCUOTA_TOTAL_LIQUIDACION
	@Column(name="CRCUOTA_TOTAL_PAGO")
	private Double CRCUOTA_TOTAL_PAGO
	@Column(name="CRPERIODOS_GRACIA_CAPITAL")
	private Long CRPERIODOS_GRACIA_CAPITAL
	@Column(name="CRPERIODOS_SIN_INTERESES")
	private Long CRPERIODOS_SIN_INTERESES
	@Column(name="CRINTERES_POR_COBRAR")
	private Double CRINTERES_POR_COBRAR
	@Column(name="CRFECHA_MINIMA_DESEMBOLSO")
	private Date CRFECHA_MINIMA_DESEMBOLSO
	@Column(name="CRPERTENECE_LEY50")
	private String CRPERTENECE_LEY50
	@Column(name="CRPORC_COMPROMETER_CESANTIAS")
	private Double CRPORC_COMPROMETER_CESANTIAS
	@Column(name="CRPORC_COMPROMETER_PRIMAS")
	private Double CRPORC_COMPROMETER_PRIMAS
	@Column(name="CRTIPO_GARANTIA")
	private String CRTIPO_GARANTIA
	@Column(name="CRGARANTIA_NUEVA_USADA")
	private String CRGARANTIA_NUEVA_USADA
	@Column(name="CROBSERVACIONES_GARANTIA")
	private String CROBSERVACIONES_GARANTIA
	@Column(name="CRPUNTAJE_ASESORIA")
	private Double CRPUNTAJE_ASESORIA
	@Column(name="CRSUGERENCIA_ASESORIA")
	private String CRSUGERENCIA_ASESORIA
	@Column(name="CRPUNTAJE_OTORGAMIENTO")
	private Double CRPUNTAJE_OTORGAMIENTO
	@Column(name="CRSUGERENCIA_OTORGAMIENTO")
	private String CRSUGERENCIA_OTORGAMIENTO
	@Column(name="ROVALOR_CUPO")
	private Double ROVALOR_CUPO
	@Column(name="ROTIPO_AMORTIZACION")
	private String ROTIPO_AMORTIZACION
	@Column(name="ROCUPO_DISPONIBLE")
	private Double ROCUPO_DISPONIBLE
	@Column(name="ROCUOTA_LIQUIDADA")
	private Long ROCUOTA_LIQUIDADA
	@Column(name="RODIA_CORTE")
	private Long RODIA_CORTE
	@Column(name="RODIA_PAGO")
	private Long RODIA_PAGO
	@Column(name="REFECHA_INICIAL")
	private Date REFECHA_INICIAL
	@Column(name="REVALOR_A_RECAUDAR")
	private Double REVALOR_A_RECAUDAR
	@Column(name="RETEXTO1")
	private String RETEXTO1
	@Column(name="RETEXTO2")
	private String RETEXTO2
	@Column(name="RETEXTO3")
	private String RETEXTO3
	@Column(name="RETEXTO4")
	private String RETEXTO4
	@Column(name="RETEXTO5")
	private String RETEXTO5
	@Column(name="RETEXTO6")
	private String RETEXTO6
	@Column(name="RETEXTO7")
	private String RETEXTO7
	@Column(name="RETEXTO8")
	private String RETEXTO8
	@Column(name="REFECHA_MODIFICACION")
	private Date REFECHA_MODIFICACION
	@Column(name="FECHA_ULT_MVTO")
	private Date FECHA_ULT_MVTO
	@Column(name="PLAZO_LIQUIDACION")
	private Long PLAZO_LIQUIDACION
	@Column(name="AHDESTINO_CANCELACION")
	private String AHDESTINO_CANCELACION
	@Column(name="AHVENCIMIENTO_PLAZO")
	private String AHVENCIMIENTO_PLAZO
	@Column(name="AHINTERES_PROYECTADO")
	private Long AHINTERES_PROYECTADO
	@Column(name="AHAHORRO_PROYECTADO")
	private Long AHAHORRO_PROYECTADO
	@Column(name="USU_AUTORIZA_PUNTOS")
	private String USU_AUTORIZA_PUNTOS
	@Column(name="AHTASA_SUGERIDA")
	private Double AHTASA_SUGERIDA
	@Column(name="CRGRACIA_CAPITAL_PPAGO")
	private Long CRGRACIA_CAPITAL_PPAGO
	@Column(name="CRSIN_INTERESES_PPAGO")
	private Long CRSIN_INTERESES_PPAGO
	@Column(name="CRSALDO_PENDIENTE_LIQUIDAR")
	private Double CRSALDO_PENDIENTE_LIQUIDAR
	@Column(name="CRVALOR_PREDICHO")
	private Double CRVALOR_PREDICHO
	@Column(name="AHNUMERO_SERIE")
	private String AHNUMERO_SERIE
	@Column(name="FECHA_ESTADO_DECISION")
	private Date FECHA_ESTADO_DECISION
	@Column(name="ES_VALIDO")
	private String ES_VALIDO
	@Column(name="AACTA_ID")
	private Long AACTA_ID
	@Column(name="CRCUOTA_LIQUIDACION_EMPLEADO")
	private Double CRCUOTA_LIQUIDACION_EMPLEADO
	@Column(name="CRCUOTA_TOTAL_LIQUIDACION_EMPL")
	private Double CRCUOTA_TOTAL_LIQUIDACION_EMPL
	@Column(name="CRINTERES_ANTICIPADO")
	private Double CRINTERES_ANTICIPADO
	@Column(name="CRVARIABLE_CALCULO")
	private String CRVARIABLE_CALCULO
	@Column(name="CRVARIABLE_INGRESADA")
	private String CRVARIABLE_INGRESADA
	@Column(name="CRTOTAL_REFINANCIACION")
	private Double CRTOTAL_REFINANCIACION
	@Column(name="SOLICITAR_CONSECUTIVO")
	private String SOLICITAR_CONSECUTIVO
	@Column(name="PRIMERA_CUOTA_AUTOMATICA")
	private String PRIMERA_CUOTA_AUTOMATICA
	@Column(name="CRCATEGORIA")
	private String CRCATEGORIA
	@Column(name="CRPROVISION")
	private Double CRPROVISION
	@Column(name="AHCUPO_AUTORIZADO")
	private Double AHCUPO_AUTORIZADO
	@Column(name="AHCUPO_DISPONIBLE")
	private Double AHCUPO_DISPONIBLE
	@Column(name="AHAPROBACION_BANCO")
	private String AHAPROBACION_BANCO
	@Column(name="DCONTR_NRO_CUPONES")
	private Long DCONTR_NRO_CUPONES
	@Column(name="DCONTR_DOC_ID")
	private Long DCONTR_DOC_ID
	@Column(name="DIAS_MORA")
	private Long DIAS_MORA
	@Column(name="TRVALOR_INICIAL")
	private Double TRVALOR_INICIAL
	@Column(name="OBSERVACIONES")
	private String OBSERVACIONES
	@Column(name="AHPAGOS_PREVIOS")
	private Double AHPAGOS_PREVIOS
	@Column(name="AHNUMERO_TRANSACCION")
	private String AHNUMERO_TRANSACCION
	@Column(name="CRCATEGORIA_MORA")
	private String CRCATEGORIA_MORA
	@Id
	@Column(name="CONSECUTIVO_SERVICIO")
	private Long CONSECUTIVO_SERVICIO
	@Column(name="ASERVIC_TYPE")
	private String ASERVIC_TYPE
	@Column(name="PER_ID")
	private Long PER_ID
	@Column(name="SER_CODIGO")
	private String SER_CODIGO
	@Column(name="ES_VIABLE")
	private String ES_VIABLE
	@Column(name="U_CREACION")
	private String U_CREACION
	@Column(name="F_CREACION")
	private Date F_CREACION
	@Column(name="FECHA_EFECTIVO")
	private Date FECHA_EFECTIVO
	@Column(name="NUMERO_SERVICIO")
	private String NUMERO_SERVICIO
	@Column(name="RECLAMA_PERSONAL")
	private String RECLAMA_PERSONAL
	@Column(name="MODALIDAD_INTERES")
	private String MODALIDAD_INTERES
	@Column(name="TIPO_TASA_INTERES")
	private String TIPO_TASA_INTERES
	@Column(name="PLAZO")
	private Long PLAZO
	@Column(name="TASA_EFECTIVA")
	private Double TASA_EFECTIVA
	@ManyToOne
	@JoinColumn(name="TASA_BASE_INTERES")
	private Tipos tipos
	@Column(name="FECHA_PRIMERA_CUOTA")
	private Date FECHA_PRIMERA_CUOTA
	@Column(name="FECHA_CORTE")
	private Date FECHA_CORTE
	@Column(name="FECHA_VENCIMIENTO")
	private Date FECHA_VENCIMIENTO
	@Column(name="CUOTA_PERIODO_PAGO")
	private Double CUOTA_PERIODO_PAGO
	@Column(name="CUOTA_PERIODO_LIQUIDACION")
	private Double CUOTA_PERIODO_LIQUIDACION
	@Column(name="SALDO")
	private Double SALDO
	@Column(name="VALOR_MORA")
	private Double VALOR_MORA
	@Column(name="U_ACTUALIZACION")
	private String U_ACTUALIZACION
	@Column(name="F_ACTUALIZACION")
	private Date F_ACTUALIZACION
	@Column(name="ACOMPAN_ID")
	private Long ACOMPAN_ID
	@Column(name="ARESOLU_NUMERO_ACTA")
	private Long ARESOLU_NUMERO_ACTA
	@Column(name="CDETETA_ID")
	private Long CDETETA_ID
	@Column(name="CRPROVISION_INTERES")
	private Double CRPROVISION_INTERES
	@Column(name="CRCAR_ESTADO_FECHA")
	private Date CRCAR_ESTADO_FECHA
	@Column(name="CRCAR_RADICADO_GASTO")
	private String CRCAR_RADICADO_GASTO
	@Column(name="CRCAR_ESTADO_ID")
	private Long CRCAR_ESTADO_ID
	@Column(name="CRCAR_MODALIDAD")
	private String CRCAR_MODALIDAD
	@Column(name="CRCAR_PER_ID_ABOGADO")
	private Long CRCAR_PER_ID_ABOGADO
	@Column(name="AHATIPPER_INTERESES")
	private String AHATIPPER_INTERESES
	@Column(name="AHSALDO_CANJE")
	private Double AHSALDO_CANJE
	@Column(name="PENDIENTE_ABONAR")
	private Double PENDIENTE_ABONAR
	@Column(name="AHCUOTAS_PROYECTADAS")
	private Long AHCUOTAS_PROYECTADAS
	@Column(name="AHPAGO_HASTA")
	private Date AHPAGO_HASTA
	@Column(name="AHPAGO_PROYECTADO")
	private Date AHPAGO_PROYECTADO
	@Column(name="OCFECHA_INTERES")
	private Date OCFECHA_INTERES
	@Column(name="OCINTERES_NO_APLICADO")
	private Double OCINTERES_NO_APLICADO
	@Column(name="CRSALDO_HONORARIOS")
	private Double CRSALDO_HONORARIOS
	@Column(name="OCSIN_SOPORTE")
	private String OCSIN_SOPORTE
	@Column(name="TRBENEFICIARIO_CHEQUE")
	private String TRBENEFICIARIO_CHEQUE
	@Column(name="RECODIGO_CONTROL_PAGO")
	private Long RECODIGO_CONTROL_PAGO
	@Column(name="REVALOR_TRASLADADO")
	private Double REVALOR_TRASLADADO
	@ManyToOne
	@JoinColumn(name="TIPO_CAUSAL_DEVOLUCION")
	private Tipos tipos
	@Column(name="CRIRRECUPERABLE")
	private String CRIRRECUPERABLE
	@Column(name="FECHA_AUTORIZA_CANCELACION")
	private Date FECHA_AUTORIZA_CANCELACION
	@Column(name="CRSUSPENSION_CAUSACION")
	private String CRSUSPENSION_CAUSACION
	@Column(name="CRFECHA_SUSPENSION")
	private Date CRFECHA_SUSPENSION
	@Column(name="CRSIN_SOPORTE")
	private String CRSIN_SOPORTE
	@Column(name="REFECHA_RETIRO_RENOVACION")
	private Date REFECHA_RETIRO_RENOVACION
	@Column(name="REFECHA_RENOVACION")
	private Date REFECHA_RENOVACION
	@Column(name="ACSIDTN_ID_REINTEGRO")
	private Long ACSIDTN_ID_REINTEGRO
	@Column(name="TRFECHA_INICIAL")
	private Date TRFECHA_INICIAL
	@Column(name="AHCAUSAL_REPOSICION")
	private String AHCAUSAL_REPOSICION
	@Column(name="AHPENDIENTE_TESORO")
	private String AHPENDIENTE_TESORO
	@Column(name="AHFECHA_ULT_INACTIVO")
	private Date AHFECHA_ULT_INACTIVO
	@Column(name="AHSALDO_ULT_INACTIVO")
	private Double AHSALDO_ULT_INACTIVO
	@Column(name="ADEPEN_REACTIVA")
	private Long ADEPEN_REACTIVA
	@Column(name="USU_REACTIVA")
	private String USU_REACTIVA
	@Column(name="AHSALDO_REACTIVA")
	private Double AHSALDO_REACTIVA
	@Column(name="ACSIDTN_ID_TRASLADO")
	private Long ACSIDTN_ID_TRASLADO
	@Column(name="TRCAUSAL_DEVOLUCION")
	private Long TRCAUSAL_DEVOLUCION
	@Column(name="AHMOMENTO_CANCELACION")
	private String AHMOMENTO_CANCELACION
	@Column(name="CMOVPAR_ID")
	private Long CMOVPAR_ID
	@Column(name="CRCATEGORIA_COMITE")
	private String CRCATEGORIA_COMITE
	@Column(name="FECHA_HASTA_INTERESES")
	private Date FECHA_HASTA_INTERESES
	@Column(name="FECHA_HASTA_PROYECCION")
	private Date FECHA_HASTA_PROYECCION
	@Column(name="NOMBRE_CUENTA")
	private String NOMBRE_CUENTA
	@Column(name="CRSCORE_CREDITICIO")
	private Long CRSCORE_CREDITICIO
	@Column(name="CRVALOR_SOLICITADO_CREDITO")
	private Double CRVALOR_SOLICITADO_CREDITO
	@Column(name="CRVALOR_SOLICITADO_DESEMBOLSO")
	private Double CRVALOR_SOLICITADO_DESEMBOLSO
	@Column(name="AHFECHA_OTRA_ENTIDAD")
	private Date AHFECHA_OTRA_ENTIDAD
	@Column(name="AHPLAZO_PROGRAMADO")
	private Long AHPLAZO_PROGRAMADO
	@Column(name="AHCUOTA_PROGRAMADA")
	private Double AHCUOTA_PROGRAMADA
	@Column(name="AHTRASLADO")
	private String AHTRASLADO
	@ManyToOne
	@JoinColumn(name="ASERVIC_SUSTITUYE")
	private Servicio servicio
	@Column(name="CRVALOR_PESOS")
	private Double CRVALOR_PESOS
	@Column(name="CRVALOR_SMLMV")
	private Double CRVALOR_SMLMV
	@Column(name="CRFINANC_MAXIMA")
	private Double CRFINANC_MAXIMA
	@Column(name="CRFINANC_ACTUAL")
	private Double CRFINANC_ACTUAL
	@Column(name="CRVALOR_SUBSIDIO")
	private Double CRVALOR_SUBSIDIO
	@Column(name="CRBENEFICIO")
	private String CRBENEFICIO
	@Column(name="CRTASA_PLENA")
	private Double CRTASA_PLENA
	@Column(name="CRTASA_BENEFICIO")
	private Double CRTASA_BENEFICIO
	@Column(name="CRREDESCUENTO")
	private String CRREDESCUENTO
	@Column(name="CRPORC_REDESCUENTO")
	private Long CRPORC_REDESCUENTO
	@Column(name="CRSOLICITUD_REDESCUENTO")
	private String CRSOLICITUD_REDESCUENTO
	@Column(name="CRFECHA_REDESCUENTO")
	private Date CRFECHA_REDESCUENTO
	@Column(name="CRPORC_GARANTIA")
	private Long CRPORC_GARANTIA
	@ManyToOne
	@JoinColumn(name="CRTIPO_FONDO_GARANTIA")
	private Tipos tipos
	@ManyToOne
	@JoinColumn(name="CRTIPO_REDESCUENTO")
	private Tipos tipos
	@ManyToOne
	@JoinColumn(name="CRTIPO_FONDO_DESTINACION")
	private Tipos tipos
	@ManyToOne
	@JoinColumn(name="CRTIPO_FONDO_PRODUCTO")
	private Tipos tipos
	@ManyToOne
	@JoinColumn(name="CRTIPO_VIVIENDA")
	private Tipos tipos
	@Column(name="CRTERMINO_BENEFICIO")
	private Date CRTERMINO_BENEFICIO
	@Column(name="CRTERMINO_GARANTIA")
	private Date CRTERMINO_GARANTIA
	@Column(name="CRREGISTRO_GARANTIA")
	private Date CRREGISTRO_GARANTIA
	@ManyToOne
	@JoinColumn(name="CRTIPO_ENTIDAD_CEDENTE")
	private Tipos tipos
	@Column(name="CRRECLAMA_GARANTIA")
	private Date CRRECLAMA_GARANTIA
	@Column(name="CRCAUSA_TERMINACION")
	private String CRCAUSA_TERMINACION
	@ManyToOne
	@JoinColumn(name="CRTIPO_ENTIDAD_CESIONARIA")
	private Tipos tipos
	@Column(name="CRCAUSA_CANC_FONDO")
	private String CRCAUSA_CANC_FONDO
	@Column(name="CRUSUARIO_CANC_FONDO")
	private String CRUSUARIO_CANC_FONDO
	@ManyToOne
	@JoinColumn(name="CRCAUSAL_CANCELACION")
	private Tipos tipos
	@Column(name="ROCUPO_SUGERIDO")
	private Double ROCUPO_SUGERIDO
	@Column(name="ROCUOTA_BASE_SUGERIDA")
	private Double ROCUOTA_BASE_SUGERIDA
	@Column(name="ROCUOTA_BASE_SOLICITADA")
	private Double ROCUOTA_BASE_SOLICITADA
	@Column(name="ROBLOQUEO")
	private String ROBLOQUEO
	@Column(name="NUMERO_SERVICIO_ANT")
	private String NUMERO_SERVICIO_ANT
	@Column(name="CRCATEGORIA_INTERNA")
	private String CRCATEGORIA_INTERNA
	@Column(name="CATEGORIA_REESTRUCTURADO")
	private String CATEGORIA_REESTRUCTURADO
	@Column(name="EXCLUIDO")
	private String EXCLUIDO
}
