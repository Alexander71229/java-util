package com.epm.service.common;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.ResultSet;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCallback;
public class Util{
	public static HashMap<String,String>obtenerCampoValor(Object objeto){
		HashMap<String,String>resultado=new HashMap<String,String>();
		Field[]campos=objeto.getClass().getDeclaredFields();
		for(int i=0;i<campos.length;i++){
			try{
				String nombreMetodo="get"+campos[i].getName().substring(0,1).toUpperCase()+campos[i].getName().substring(1);
				resultado.put(campos[i].getName(),objeto.getClass().getDeclaredMethod(nombreMetodo).invoke(objeto)+"");
			}catch(Throwable t){
			}
		}
		return resultado;
	}
	public static String crearConsulta(Object objeto){
		HashMap<String,String>valores=obtenerCampoValor(objeto);
		//Crear una consulta desde el mapa de valores
		return valores.entrySet().stream().map(entrada->"'"+entrada.getValue()+"' "+entrada.getKey()).collect(Collectors.joining(",","SELECT "," FROM dual"));
	}
	public static<T>String crearConsulta(List<T>objetos){
		return""+objetos.stream().map(objeto->crearConsulta(objeto)).collect(Collectors.joining(" UNION ALL "));
	}
	public static ResultSet obtenerCursor(JdbcTemplate jdbcTemplate,String consulta)throws Exception{
		ResultSet resultado=jdbcTemplate.execute(new CallableStatementCreator(){
			public CallableStatement createCallableStatement(Connection conexion)throws SQLException{
				CallableStatement sql=conexion.prepareCall("BEGIN OPEN ? FOR "+consulta+"; END;");
				sql.registerOutParameter(1,Types.REF_CURSOR);
				return sql;
			}
		},new CallableStatementCallback<ResultSet>(){
			public ResultSet doInCallableStatement(CallableStatement callableStatement)throws SQLException,DataAccessException{
				callableStatement.executeUpdate();
				return(ResultSet)callableStatement.getObject(1);
			}
		});
		return resultado;
	}
}