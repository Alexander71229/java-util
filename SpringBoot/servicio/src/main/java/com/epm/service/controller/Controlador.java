package com.epm.service.controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import com.epm.service.core.IServicio;
import com.epm.service.core.Servicio;
import com.epm.service.to.RespuestaTO;
import com.epm.service.to.SolicitudTO;
import java.util.List;
@RestController
@RequestMapping("/api")
public class Controlador{
	@Autowired
	private IServicio servicio;
	@RequestMapping(value={"/integracion"},method=RequestMethod.POST)
	public RespuestaTO integracion(@RequestBody List<SolicitudTO>solicitudes)throws Exception{
		return servicio.integracion(solicitudes);
	}
}