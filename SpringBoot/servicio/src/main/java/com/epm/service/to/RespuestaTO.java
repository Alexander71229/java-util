package com.epm.service.to;
public class RespuestaTO{
	private String onucode;
	private String osberror;
	public void setOnucode(String onucode){
		this.onucode=onucode;
	}
	public String getOnucode(){
		return this.onucode;
	}
	public void setOsberror(String osberror){
		this.osberror=osberror;
	}
	public String getOsberror(){
		return this.osberror;
	}
}