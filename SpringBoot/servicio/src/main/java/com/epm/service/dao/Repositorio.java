package com.epm.service.dao;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import com.epm.service.to.RespuestaTO;
import com.epm.service.to.SolicitudTO;
import com.epm.service.common.Util;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCallback;
import java.sql.Connection;
import java.sql.CallableStatement;
import org.springframework.dao.DataAccessException;
import java.sql.SQLException;
import java.sql.Types;
@Repository
public class Repositorio implements IRepositorio{
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setDataSource(final DataSource dataSource){
		this.dataSource=dataSource;
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String esquema,String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withSchemaName(esquema).withCatalogName(paquete).withProcedureName(nombreProcedimiento).execute(parametrosEntrada);
	}
	public<T>ResultSet obtenerCursor(List<T>objetos)throws Exception{
		return Util.obtenerCursor(jdbcTemplate,Util.crearConsulta(objetos));
	}
	public void test()throws Exception{
		Object o=jdbcTemplate.execute(new CallableStatementCreator(){
			public CallableStatement createCallableStatement(Connection conexion)throws SQLException{
				CallableStatement sql=conexion.prepareCall(com.epm.service.common.U.g("D:\\Desarrollo\\EPM\\Fenix\\entrada.txt"));
				sql.registerOutParameter(1,Types.ARRAY);
				return sql;
			}
		},new CallableStatementCallback(){
			public Object doInCallableStatement(CallableStatement callableStatement)throws SQLException,DataAccessException{
				callableStatement.executeUpdate();
				com.epm.service.common.U.imp("callableStatement.getObject(1):"+callableStatement.getObject(1));
				return null;
			}
		});
	}
}