package com.epm.service.dao;
import java.util.List;
import java.util.Map;
import java.sql.ResultSet;
import org.springframework.stereotype.Repository;
import com.epm.service.to.RespuestaTO;
import com.epm.service.to.SolicitudTO;
@Repository
public interface IRepositorio{
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String esquema,String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada);
	public<T>ResultSet obtenerCursor(List<T>objetos)throws Exception;
	public void test()throws Exception;
}