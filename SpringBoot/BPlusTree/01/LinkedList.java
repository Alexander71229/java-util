public class LinkedList<T>{
	public static class Node<T>{
		Node<T>pNext;
		Node<T>pAfter;
		T element;
		public Node(T element){
			this.element=element;
		}
		public Node<T>next(){
			return this.pNext;
		}
		public Node<T>after(){
			return this.pAfter;
		}
		public T get(){
			return this.element;
		}
		public void add(Node<T>node){
			if(node!=null){
				node.pNext=this.pNext;
				node.pAfter=this;
				if(this.pNext!=null){
					this.pNext.pAfter=node;
				}
				this.pNext=node;
			}
		}
		public void after(Node<T>node){
			if(node!=null){
				node.pAfter=this.pAfter;
				node.pNext=this;
				if(this.pAfter!=null){
					this.pAfter.pNext=node;
				}
				this.pAfter=node;
			}
		}
		public String toString(){
			return ""+this.element;
		}
	}
	Node<T>head;
	Node<T>cursor;
	int size=0;
	public Node<T>add(T element){
		Node<T>node=new Node<>(element);
		if(head==null){
			head=node;
			cursor=node;
			this.size=1;
			return node;
		}
		if(cursor==null){
			return null;
		}
		cursor.add(node);
		this.size++;
		return node;
	}
	public Node<T>node(){
		return this.cursor;
	}
	public String toString(){
		Node node=this.head;
		StringBuilder r=new StringBuilder("[");
		String s="";
		while(node!=null){
			r.append(s+node.get());
			node=node.next();
			s=",";
		}
		r.append("]");
		return r+"";
	}
	public Node<T>after(T element){
		Node<T>node=new Node<>(element);
		if(head==null){
			head=node;
			cursor=node;
			this.size=1;
			return node;
		}
		if(cursor==null){
			return null;
		}
		this.cursor.after(node);
		if(head.pAfter==node){
			head=node;
		}
		return node;
	}
	public int size(){
		return this.size;
	}
	public T get(){
		return this.cursor.get();
	}
	public void next(){
		this.cursor=this.cursor.next();
	}
	public void start(){
		this.cursor=head;
	}
	public void nav(Node<T>node){
		this.cursor=node;
	}
	public boolean hasNext(){
		if(this.cursor==null){
			return false;
		}
		if(this.cursor.pNext!=null){
			return true;
		}
		return false;
	}
	public boolean isEmpty(){
		if(head==null){
			return true;
		}
		return false;
	}
}