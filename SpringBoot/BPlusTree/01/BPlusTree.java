public class BPlusTree<T>{
	public static class Node<T>{
		int id;
		T value;		
		public Node(int id,T value){
			this.id=id;
			this.value=value;
		}
		public String toString(){
			return this.id+":"+this.value;
		}
	}
	public static class SubList<T>{
		int level;
		int size;
		private int tag;
		SubList<T>left;
		SubList<T>right;
		SubList<T>less;
		SubList<T>subList;
		SubList<T>up;
		private LinkedList.Node<Node<T>>node;
		public SubList<T>nav(int cantidad){
			SubList<T>inicio=this;
			for(int i=0;i<cantidad;i++){
				inicio=inicio.right;
			}
			return inicio;
		}
		public void toRight(SubList<T>element){
			element.right=this.right;
			element.left=this;
			this.right=element;
			element.size=0;
			this.size++;
			element.up=this.up;
		}
		public void toLeft(SubList<T>element){
			element.right=this;
			element.left=this.left;
			this.left=element;
			element.size=this.size+1;
			this.size=0;
			element.up=this.up;
		}
		public void toUp(SubList<T>element){
			element.subList=this;
			element.up=this.up;
			this.up=element;
			element.level=this.level+1;
		}
		public void setTag(int tag){
			this.tag=tag;
		}
		public int getTag(){
			return this.tag;
		}
		public void setNode(LinkedList.Node<Node<T>>node){
			this.node=node;
		}
		public String toString(){
			StringBuilder r=new StringBuilder();
			if(this.tag>0){
				r.append(this.tag+":"+this.size+"["+this.subList+"]->"+this.right);
				if(this.less!=null){
					r.append("\n"+this.tag+"<"+this.less);
				}
			}else{
				r.append(this.size+":["+this.node+"]");
			}
			return r+"";
		}
	}
	int m=0;
	int l=0;
	private LinkedList<Node<T>>data;
	private SubList<T>root=null;
	public BPlusTree(int m,int l){
		this.m=m;
		this.l=l;
		data=new LinkedList<>();
	}
	public int add(int id,T value){
		c.U.imp("\nadd("+id+")");
		Node<T>node=new Node<>(id,value);
		if(data.isEmpty()){
			data.add(node);
			root=new SubList<>();
			root.size=1;
			root.node=data.node();
			return 1;
		}
		SubList<T>t=this.searchSubList(id);
		c.U.imp("Buscando "+id+" se llegó al nodo:"+t);
		if(this.search(id,t)!=null){c.U.imp("\nExiste:"+id);
			return 0;
		}
		LinkedList.Node<Node<T>>tnode=t.node;
		data.nav(tnode);
		while(data.hasNext()&&id>data.node().next().get().id){
			data.next();
		}
		if(id<data.get().id){
			t.node=data.after(node);
		}else{
			data.add(node);
		}
		t.size++;
		if(t.size>l){c.U.imp("\nSobrecarga en:"+t.node.get().id);
			if((t.left==null||t.left.size>=l)&&(t.right==null||t.right.size>=l)){c.U.imp("\nSe generó split:"+t.node.get().id);
				c.U.imp("t.up:"+t.up);
				t.size=l/2-1;
				t.toRight(new SubList<T>());
				t.right.size=l+1-t.size;
				data.nav(t.node);
				for(int i=0;i<t.size;i++){
					data.next();
				}
				t.right.node=data.node();
				t.right.toUp(new SubList<>());
				t.right.up.size=1;
				t.right.up.less=t;
				t.right.up.right=t.right;
				t.right.up.setTag(t.right.node.get().id);
				if(t.up!=null){
					SubList<T>t1=null;
					if(t.right.up.tag>t.up.tag){
						t.up.toRight(t.right.up);
						t1=t.up;
					}else{
						t.up.toLeft(t.right.up);
						t1=t.right.up;
						if(t.up==root){
							root=t.right.up;
						}
					}
					if(t1.size>=m){
						c.U.imp("\nSeleccionado:"+t1.tag+":"+t1.size);
						c.U.imp("\nNAV:"+t1.nav(m/2).tag);
						c.U.imp("\nROOT:"+root.tag+":"+root.size);
						if(t1==root){
							root=t1.nav(m/2);
						}
					}
				}
				t.up=t.right.up;
				if(t==root){
					root=t.right.up;
				}
			}else{
				if(t.left.size<l){
					t.left.size++;
					t.node=t.node.next();
					t.size=l;
					t.up.setTag(t.node.get().id);
				}else{
					t.size=l;
					t.right.size++;
					if(id<t.right.node.get().id){
						t.right.node=t.right.node.after();
						t.right.upsetTag(t.right.node.get().id);
					}
				}
			}
		}
		c.U.imp("\n"+id+" Agregado\n"+root+"\n"+data);
		return 1;
	}
	public SubList<T>searchSubList(int id){
		SubList<T>t=root;
		while(t.tag>0){
			if(id>=t.tag){
				if(t.right==null||id<t.right.tag){
					t=t.subList;
				}else{
					t=t.right;
				}
			}else{
				t=t.less;
			}
		}
		return t;
	}
	public T search(int id,SubList<T>subList){
		data.nav(subList.node);
		while(data.node()!=null&&data.get().id<=id){
			if(data.get().id==id){
				return data.get().value;
			}
			data.next();
		}
		return null;		
	}
	public T search(int id){
		return search(id,searchSubList(id));
	}
}