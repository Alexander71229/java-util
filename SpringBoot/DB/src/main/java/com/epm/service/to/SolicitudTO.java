package com.epm.service.to;
public class SolicitudTO{
	private String identificador;
	private String evento;
	private String negocio;
	private String aplicacion;
	private String tipoDocumento;
	private String numeroOT;
	private String respuesta;
	private String codigo;
	private String fecha;
	private String codigoInteraccion;
	private String tipoInteraccion;
	private String idTransaccion;
	public void setIdentificador(String identificador){
		this.identificador=identificador;
	}
	public String getIdentificador(){
		return this.identificador;
	}
	public void setEvento(String evento){
		this.evento=evento;
	}
	public String getEvento(){
		return this.evento;
	}
	public void setNegocio(String negocio){
		this.negocio=negocio;
	}
	public String getNegocio(){
		return this.negocio;
	}
	public void setAplicacion(String aplicacion){
		this.aplicacion=aplicacion;
	}
	public String getAplicacion(){
		return this.aplicacion;
	}
	public void setTipoDocumento(String tipoDocumento){
		this.tipoDocumento=tipoDocumento;
	}
	public String getTipoDocumento(){
		return this.tipoDocumento;
	}
	public void setNumeroOT(String numeroOT){
		this.numeroOT=numeroOT;
	}
	public String getNumeroOT(){
		return this.numeroOT;
	}
	public void setRespuesta(String respuesta){
		this.respuesta=respuesta;
	}
	public String getRespuesta(){
		return this.respuesta;
	}
	public void setCodigo(String codigo){
		this.codigo=codigo;
	}
	public String getCodigo(){
		return this.codigo;
	}
	public void setFecha(String fecha){
		this.fecha=fecha;
	}
	public String getFecha(){
		return this.fecha;
	}
	public void setCodigoInteraccion(String codigoInteraccion){
		this.codigoInteraccion=codigoInteraccion;
	}
	public String getCodigoInteraccion(){
		return this.codigoInteraccion;
	}
	public void setTipoInteraccion(String tipoInteraccion){
		this.tipoInteraccion=tipoInteraccion;
	}
	public String getTipoInteraccion(){
		return this.tipoInteraccion;
	}
	public void setIdTransaccion(String idTransaccion){
		this.idTransaccion=idTransaccion;
	}
	public String getIdTransaccion(){
		return this.idTransaccion;
	}
}