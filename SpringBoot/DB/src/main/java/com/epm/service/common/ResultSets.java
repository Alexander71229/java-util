package com.epm.service.common;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
public class ResultSets{
	public static List<ArrayList<Object>>convertirALista(ResultSet entrada)throws Exception{
		List<ArrayList<Object>>resultado=new ArrayList<ArrayList<Object>>();
		while(entrada.next()){
			ArrayList<Object>fila=new ArrayList<>();
			for(int i=1;i<=entrada.getMetaData().getColumnCount();i++){
				fila.add(entrada.getObject(i));
			}
			resultado.add(fila);
		}
		entrada.close();
		return resultado;
	}
	public static ArrayList<String>obtenerEncabezado(ResultSet entrada)throws Exception{
		ArrayList<String>resultado=new ArrayList<String>();
		for(int i=1;i<=entrada.getMetaData().getColumnCount();i++){
			resultado.add(entrada.getMetaData().getColumnName(i));
		}
		return resultado;
	}
	public static List<ArrayList<Object>>convertirAListaConEncabezado(ResultSet entrada)throws Exception{
		List<ArrayList<Object>>resultado=new ArrayList<ArrayList<Object>>();
		ArrayList<Object>encabezado=new ArrayList<Object>();
		encabezado.addAll(obtenerEncabezado(entrada));
		resultado.add(encabezado);
		resultado.addAll(convertirALista(entrada));
		return resultado;
	}
	public static List<Map<String,Object>>convertirAListaMapa(ResultSet entrada)throws Exception{
		List<Map<String,Object>>resultado=new ArrayList<Map<String,Object>>();
		while(entrada.next()){
			Map<String,Object>fila=new LinkedHashMap<>();
			for(int i=1;i<=entrada.getMetaData().getColumnCount();i++){
				fila.put(entrada.getMetaData().getColumnName(i),entrada.getObject(i));
			}
			resultado.add(fila);
		}
		entrada.close();
		return resultado;
	}
}