package com.epm.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.boot.CommandLineRunner;
import com.epm.service.dao.IRepositorio;
import com.epm.service.dao.Repositorio;
import com.epm.service.common.U;
@Component
public class Inicio implements CommandLineRunner{
	@Autowired
	private IRepositorio repositorio;
	@Override
	public void run(String...arumentos)throws Exception{
		try{
			c.U.imp("Inicio");
			c.U.imp(""+(new java.util.Date()));
			c.U.imp("repositorio:"+repositorio);
			repositorio.test05();
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}