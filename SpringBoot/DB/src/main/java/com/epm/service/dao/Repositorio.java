package com.epm.service.dao;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import com.epm.service.to.RespuestaTO;
import com.epm.service.to.SolicitudTO;
import com.epm.service.common.Util;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCallback;
import java.sql.Connection;
import java.sql.CallableStatement;
import org.springframework.dao.DataAccessException;
import java.sql.SQLException;
import java.sql.Types;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.HashMap;
import java.math.BigDecimal;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.ParameterMode;
@Repository
public class Repositorio implements IRepositorio{
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	public void setDataSource(final DataSource dataSource){
		this.dataSource=dataSource;
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String esquema,String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withSchemaName(esquema).withCatalogName(paquete).withProcedureName(nombreProcedimiento).execute(parametrosEntrada);
	}
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withCatalogName(paquete).withProcedureName(nombreProcedimiento).execute(parametrosEntrada);
	}
	public<T>T ejecutarFuncion(Class<T>retorno,String esquema,String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withSchemaName(esquema).withCatalogName(paquete).withProcedureName(nombreProcedimiento).executeFunction(retorno,parametrosEntrada);
	}
	public<T>T ejecutarFuncion(Class<T>retorno,String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withCatalogName(paquete).withProcedureName(nombreProcedimiento).executeFunction(retorno,parametrosEntrada);
	}
	public<T>ResultSet obtenerCursor(List<T>objetos)throws Exception{
		return Util.obtenerCursor(jdbcTemplate,Util.crearConsulta(objetos));
	}
	public void test01()throws Exception{
		Object o=jdbcTemplate.execute(new CallableStatementCreator(){
			public CallableStatement createCallableStatement(Connection conexion)throws SQLException{
				CallableStatement sql=conexion.prepareCall(com.epm.service.common.U.g("C:\\desarrollo\\Java\\LogTemp\\Entrada001.sql"));
				//sql.registerOutParameter(1,Types.ARRAY,"T");
				//sql.registerOutParameter(1,Types.NUMERIC);
				//sql.registerOutParameter(1,Types.OTHER);
				sql.registerOutParameter(1,Types.REF_CURSOR);
				return sql;
			}
		},new CallableStatementCallback(){
			public Object doInCallableStatement(CallableStatement callableStatement)throws SQLException,DataAccessException{
				callableStatement.executeUpdate();
				Object o=callableStatement.getObject(1);
				com.epm.service.common.U.imp("callableStatement.getObject(1):"+o+":"+o.getClass().getName());
				ResultSet rs=(ResultSet)o;
				/*rs.next();
				com.epm.service.common.U.imp("rs.getObject(1):"+rs.getObject(1)+":"+rs.getMetaData().getColumnClassName(1));*/
				try{
					//com.epm.service.common.U.imp(com.epm.service.common.ResultSets.convertirAListaConEncabezado(rs)+"");
					com.epm.service.common.U.imp(com.epm.service.common.ResultSets.convertirAListaMapa(rs)+"");
				}catch(Throwable t){
					com.epm.service.common.U.imp(t);
				}
				return null;
			}
		});
	}
	public Object ejecutarCodigo(String s)throws Exception{
		Object o=jdbcTemplate.execute(new CallableStatementCreator(){
			public CallableStatement createCallableStatement(Connection conexion)throws SQLException{
				CallableStatement sql=conexion.prepareCall(s);
				return sql;
			}
		},new CallableStatementCallback(){
			public Object doInCallableStatement(CallableStatement callableStatement)throws SQLException,DataAccessException{
				callableStatement.executeUpdate();
				return null;
			}
		});
		return o;
	}
	@Transactional
	public void test02()throws Exception{
		com.epm.service.common.U.imp("Ejecuta");
		jdbcTemplate.update("insert into ts_tar_temp_base_informes(columna01)values('A01')");
		jdbcTemplate.update("insert into ts_tar_temp_base_informes(columna01)values('B01')");
		jdbcTemplate.update("update ts_tar_temp_base_informes set columna01='M01' where columna01='A01'");
		ejecutarCodigo(com.epm.service.common.U.g("C:\\desarrollo\\Java\\LogTemp\\Entrada002.sql"));
		List<Map<String,Object>>o=jdbcTemplate.queryForList("select * from ts_tar_temp_base_informes");
		for(int i=0;i<o.size();i++){
			com.epm.service.common.U.imp(i+":"+o.get(i).get("COLUMNA01"));
		}
	}
	public void test03()throws Exception{
		Map<String,Object>parametrosEntrada=new HashMap<String,Object>();
		parametrosEntrada.put("diFechaCorte",new Date(1572498000000L));
		parametrosEntrada.put("viPortafolio","ATL-1");
		parametrosEntrada.put("viTipoGestor","Iron");
		parametrosEntrada.put("VIMARCAR","FALSE");
		Map<String,Object>salida=ejecutarProcedimientoAlmacenado("pks_portafolios_atl","generar_gestor_documental",parametrosEntrada);
		//Map<String,Object>salida=new SimpleJdbcCall(dataSource).withCatalogName("pks_portafolios_atl").withProcedureName("generar_gestor_documental").execute((new MapSqlParameterSource()).addValue("diFechaCorte",new Date(1572498000000L)).addValue("viPortafolio","ATL-1").addValue("viTipoGestor","Iron"));
		ArrayList al=(ArrayList)salida.get("CPORTAFOLIO");
		Map<String,Object>mp=(Map<String,Object>)al.get(0);
		com.epm.service.common.U.imp("salida:"+al.getClass().getName());
		com.epm.service.common.U.imp("salida:"+al.get(0).getClass().getName());
		com.epm.service.common.U.imp("salida:"+mp.get("DUMMy"));
		com.epm.service.common.U.imp("salida:"+salida);
	}
	public void test04(){
		try{
			c.U.traza();
			Map<String,Object>parametros=new HashMap<>();
			parametros.put("vcodformato","441");
			parametros.put("nidprogramacion","22362");
			parametros.put("vperiodicidad","DIARIO");
			parametros.forEach((k,v)->c.U.imp(k+":"+v));
			String salida=ejecutarFuncion(String.class,"PKS_REPORTES_SUPER","FORMATO_441",parametros);
			c.U.imp("salida:"+salida);
		}catch(Exception e){
			c.U.imp(e);
		}
	}
	public void test05()throws Exception{
		c.U.traza();
		StoredProcedureQuery procedimiento=entityManager.createStoredProcedureQuery("PKS_REPORTES_SUPER.FORMATO_441");
		procedimiento.registerStoredProcedureParameter("vcodformato",String.class,ParameterMode.IN);
		procedimiento.registerStoredProcedureParameter("nIdProgramacion",BigDecimal.class,ParameterMode.IN);
		procedimiento.registerStoredProcedureParameter("vPeriodicidad",String.class,ParameterMode.IN);
		//procedimiento.registerStoredProcedureParameter(1,String.class,ParameterMode.OUT);
		procedimiento.setParameter("vcodformato","441");
		procedimiento.setParameter("nIdProgramacion",new BigDecimal(22362));
		procedimiento.setParameter("vPeriodicidad","DIARIO");
		procedimiento.execute();
	}
}