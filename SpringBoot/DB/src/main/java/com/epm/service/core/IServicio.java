package com.epm.service.core;
import com.epm.service.to.RespuestaTO;
import com.epm.service.to.SolicitudTO;
import java.util.List;
public interface IServicio{
	public RespuestaTO integracion(List<SolicitudTO>solicitudes)throws Exception;
}