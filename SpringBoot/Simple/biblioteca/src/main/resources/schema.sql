CREATE TEMPORARY TABLE prestamo(
	id INT PRIMARY KEY NOT NULL,
	isbn VARCHAR2(10) NOT NULL,
	identificacionUsuario VARCHAR2(10) NOT NULL,
	tipoUsuario NUMERIC(1) NOT NULL,
	fechaMaximaDevolucion TIMESTAMP NOT NULL
);
