package com.ceiba.biblioteca.controladores;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.ceiba.biblioteca.negocio.ExcepcionPrestamoExistente;
@ControllerAdvice
public class ManejadorGlobalExcepciones{
	@ExceptionHandler(ExcepcionTipoUsuarioNoValido.class)
	@ResponseBody
	public ResponseEntity<RespuestaError>manejadorExcepcionTipoUsuarioNoValido(ExcepcionTipoUsuarioNoValido excepcion){
		return ResponseEntity.badRequest().body(new RespuestaError("Tipo de usuario no permitido en la biblioteca"));
	}
	@ExceptionHandler(ExcepcionPrestamoExistente.class)
	@ResponseBody
	public ResponseEntity<RespuestaError>manejadorExcepcionPrestamoExistente(ExcepcionPrestamoExistente excepcion){
		c.U.imp(excepcion);
		return ResponseEntity.badRequest().body(new RespuestaError("El usuario con identificación "+excepcion.getIdentificacionUsuario()+" ya tiene un libro prestado por lo cual no se le puede realizar otro préstamo"));
	}
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseEntity<RespuestaError>manejadorExcepcionGeneral(Exception excepcion){
		c.U.imp(excepcion);
		return ResponseEntity.badRequest().body(new RespuestaError(excepcion.getMessage()));
	}
}