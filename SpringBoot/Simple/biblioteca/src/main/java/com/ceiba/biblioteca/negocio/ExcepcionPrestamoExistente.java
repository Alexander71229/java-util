package com.ceiba.biblioteca.negocio;
@lombok.Getter
public class ExcepcionPrestamoExistente extends RuntimeException{
	private String identificacionUsuario;
	public ExcepcionPrestamoExistente(String identificacionUsuario){
		this.identificacionUsuario=identificacionUsuario;
	}
}