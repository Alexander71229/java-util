package com.ceiba.biblioteca.adaptadores.repositorio;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
public interface RepositorioJPA extends JpaRepository<Prestamo,Integer>{
	Prestamo findById(int id);
	List<Prestamo>findByIdentificacionUsuario(String identificacionUsuario);
}