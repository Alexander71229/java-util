package com.ceiba.biblioteca.servicios;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.modelmapper.ModelMapper;
import com.ceiba.biblioteca.controladores.SolicitudPrestamo;
import com.ceiba.biblioteca.controladores.RespuestaPrestamo;
import com.ceiba.biblioteca.negocio.Puerto;
import java.util.Date;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
@Service
public class Servicio{
	private final Puerto logica;
	public Servicio(Puerto logica){
		this.logica=logica;
	}
	public RespuestaPrestamo consultarPrestamoPorId(int id){
		return new ModelMapper().map(logica.consultarPrestamoPorId(id),RespuestaPrestamo.class);
	}
	public RespuestaPrestamo prestar(SolicitudPrestamo solicitudPrestamo){
		com.ceiba.biblioteca.negocio.Prestamo prestamo=logica.prestar(new ModelMapper().map(solicitudPrestamo,com.ceiba.biblioteca.negocio.SolicitudPrestamo.class));
		return new ModelMapper().map(prestamo,RespuestaPrestamo.class);
	}
}