package com.ceiba.biblioteca.controladores;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.ceiba.biblioteca.servicios.Servicio;
@RestController
@RequestMapping("prestamo")
public class PrestamoControlador {
	@Autowired
	Servicio servicio;
	@PostMapping()
	public RespuestaPrestamo prestar(@RequestBody SolicitudPrestamo solicitudPrestamo)throws ExcepcionTipoUsuarioNoValido{
		if(solicitudPrestamo.getTipoUsuario()>3){
			throw new ExcepcionTipoUsuarioNoValido();
		}
		return servicio.prestar(solicitudPrestamo);
	}
	@GetMapping("/{id}")
	public RespuestaPrestamo prestar(@PathVariable int id){
		return servicio.consultarPrestamoPorId(id);
	}
	static{
		c.U.ruta="D:\\Desarrollo\\log.txt";c.U.imp("Inicio");
	}
}

