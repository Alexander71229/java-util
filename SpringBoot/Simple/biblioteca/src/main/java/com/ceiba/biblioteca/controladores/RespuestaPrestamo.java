package com.ceiba.biblioteca.controladores;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
@lombok.Data
public class RespuestaPrestamo{
	int id;
	String isbn;
	String identificacionUsuario;
	int tipoUsuario;
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd/MM/yyyy")
	Date fechaMaximaDevolucion;
}