package com.ceiba.biblioteca;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.ceiba.biblioteca.negocio.Repositorio;
import com.ceiba.biblioteca.negocio.Puerto;
@Configuration
public class Configuracion{
	@Bean
	public Repositorio obtenerRepositorio(){
		return new com.ceiba.biblioteca.adaptadores.Repositorio();
	}
	@Bean
	public Puerto obtenerLogica(){
		return new com.ceiba.biblioteca.negocio.Logica(obtenerRepositorio());
	}
}
