package com.ceiba.biblioteca.negocio;
import java.util.Date;
@lombok.Data
public class SolicitudPrestamo{
	String isbn;
	String identificacionUsuario;
	int tipoUsuario;
}