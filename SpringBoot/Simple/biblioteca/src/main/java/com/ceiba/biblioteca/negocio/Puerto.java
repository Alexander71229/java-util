package com.ceiba.biblioteca.negocio;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;
public interface Puerto{
	public Prestamo consultarPrestamoPorId(int id);
	public Prestamo prestar(SolicitudPrestamo solicitudPrestamo);
}