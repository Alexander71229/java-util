package com.ceiba.biblioteca.controladores;
@lombok.Data
public class SolicitudPrestamo{
	private String isbn;
	private String identificacionUsuario;
	private int tipoUsuario;
}