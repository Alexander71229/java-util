package com.ceiba.biblioteca.controladores;
@lombok.Data
@lombok.AllArgsConstructor
public class RespuestaError{
	private String mensaje;
}