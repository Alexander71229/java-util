package com.ceiba.biblioteca.negocio;
import java.util.List;
public interface Repositorio{
	Prestamo consultarPrestamoPorId(int id);
	Prestamo crearPrestamo(Prestamo prestamo);
	List<Prestamo>prestamosPorUsuario(String identificacionUsuario);
}