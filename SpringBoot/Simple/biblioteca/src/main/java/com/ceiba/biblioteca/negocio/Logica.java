package com.ceiba.biblioteca.negocio;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;
import java.util.List;
public class Logica implements Puerto{
	public static final int USUARIO_AFILIADO=1;
	public static final int USUARIO_EMPLEADO=2;
	public static final int USUARIO_INVITADO=3;
	private final Repositorio repositorio;
	public Logica(Repositorio repositorio){
		this.repositorio=repositorio;
	}
	public Date agregarDiasSaltandoFinesSemana(Date fecha,int cantidad){
		Calendar calendario=Calendar.getInstance();
		calendario.setTime(fecha);
		int agregados=0;
		while(agregados<cantidad){
			calendario.add(Calendar.DATE,1);
			int diaSemana=calendario.get(Calendar.DAY_OF_WEEK);
			if(diaSemana!=7&&diaSemana!=1){
				agregados++;
			}
		}
		return calendario.getTime();
	}
	public int cantidadDiasPrestamoPorTipoUsuario(int tipoUsuario){
		Map<Integer,Integer>mapa=new HashMap<>();
		mapa.put(Logica.USUARIO_AFILIADO,10);
		mapa.put(Logica.USUARIO_EMPLEADO,8);
		mapa.put(Logica.USUARIO_INVITADO,7);
		return mapa.get(tipoUsuario);
	}
	public Prestamo consultarPrestamoPorId(int id){
		return repositorio.consultarPrestamoPorId(id);
	}
	public Prestamo prestar(SolicitudPrestamo solicitudPrestamo)throws ExcepcionPrestamoExistente{
		Prestamo prestamo=new Prestamo();
		prestamo.setIsbn(solicitudPrestamo.getIsbn());
		prestamo.setIdentificacionUsuario(solicitudPrestamo.getIdentificacionUsuario());
		prestamo.setTipoUsuario(solicitudPrestamo.getTipoUsuario());
		prestamo.setFechaMaximaDevolucion(agregarDiasSaltandoFinesSemana(new Date(),cantidadDiasPrestamoPorTipoUsuario(prestamo.getTipoUsuario())));
		if(prestamo.getTipoUsuario()==Logica.USUARIO_INVITADO){
			List<Prestamo>prestamos=repositorio.prestamosPorUsuario(prestamo.getIdentificacionUsuario());
			if(prestamos.size()==1){
				throw new ExcepcionPrestamoExistente(prestamo.getIdentificacionUsuario());
			}
		}
		prestamo=repositorio.crearPrestamo(prestamo);
		return prestamo;
	}
}