package com.ceiba.biblioteca.adaptadores;
import com.ceiba.biblioteca.negocio.Prestamo;
import org.springframework.beans.factory.annotation.Autowired;
import com.ceiba.biblioteca.adaptadores.repositorio.RepositorioJPA;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;
import com.google.gson.reflect.TypeToken;
import java.util.List;
@Repository
public class Repositorio implements com.ceiba.biblioteca.negocio.Repositorio{
	@Autowired
	RepositorioJPA repositorioJPA;
	@Override
	public Prestamo consultarPrestamoPorId(int id){
		return new ModelMapper().map(repositorioJPA.findById(id),Prestamo.class);
	}
	@Override
	public Prestamo crearPrestamo(Prestamo prestamo){
		return new ModelMapper().map(repositorioJPA.save(new ModelMapper().map(prestamo,com.ceiba.biblioteca.adaptadores.repositorio.Prestamo.class)),Prestamo.class);
	}
	@Override
	public List<Prestamo>prestamosPorUsuario(String identificacionUsuario){
		return new ModelMapper().map(repositorioJPA.findByIdentificacionUsuario(identificacionUsuario),new TypeToken<List<Prestamo>>(){}.getType());
	}
}