package com.ceiba.biblioteca.adaptadores.repositorio;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.Date;
@lombok.Data
@Entity
@Table(name="prestamo")
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class Prestamo{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	int id;
	@Column(name="isbn")
	String isbn;
	@Column(name="identificacionUsuario")
	String identificacionUsuario;
	@Column(name="tipoUsuario")
	int tipoUsuario;
	@Column(name="fechaMaximaDevolucion")
	Date fechaMaximaDevolucion;
}