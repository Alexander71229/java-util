package com.ceiba.biblioteca.negocio;
import java.util.Date;
@lombok.Data
public class Prestamo{
	int id;
	String isbn;
	String identificacionUsuario;
	int tipoUsuario;
	Date fechaMaximaDevolucion;
}