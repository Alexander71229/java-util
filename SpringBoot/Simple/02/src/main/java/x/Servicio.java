package x;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.*;
import org.springframework.http.MediaType;
@RestController
@RequestMapping(value="/test",produces=MediaType.APPLICATION_JSON_VALUE)
public class Servicio{
	@GetMapping(path="/test")
	public String test(){
		return"{}";
	}
	@PostMapping(path="/mockEnterprise")
	public String information(@RequestBody Map<String,Object>datos){
		if(datos.get("DocumentType").equals("NIT")&&datos.get("DocumentNumber").equals("1020123455")){
			return"{\"MsgInformation\":{\"MsgCode\":\"5623\",\"MsgDescription\":\"string\"},\"Status\":true,\"EnrollmentId\":1234}";
		}
		return"{\"MsgInformation\":{\"MsgCode\":\"9273\",\"MsgDescription\":\"string\"}}";
	}
	@PostMapping(path="/mockBelonging")
	public String mockBelonging(@RequestBody Map<String,Object>datos){
		return"{\"meta\":{\"_messageId\":\"7388de91-9b30-4ea6-9035-2c24ce3bbfe3\",\"_requestTimeStamp\":\"2023-04-20T11:36:47.490Z\",\"_applicationId\":\"615e157f2fc39e075d8cc6106021b287\",\"_responseSize\":1,\"_version\":\"3.0\"},\"data\":[{\"header\":{\"type\":\"Account-Management\",\"id\":\"Account-Management-2023-04-20T11:36:47.490Z\"},\"participant\":{\"relation\":\"TITULAR\"}}],\"links\":{\"self\":\"/v1/operations/product-specific/deposits/accounts/belonging\"}}";
	}
	@PostMapping(path="/mockControl")
	public String mockControl(@RequestBody Map<String,Object>datos){
		return"{\"meta\":{\"_messageId\":\"08e510d8-d585-492d-a6c2-29d98897da22\",\"_version\":\"2.0\",\"_requestDate\":\"2023-04-19T07:10:01.875Z\",\"_responseSize\":1,\"_clientRequest\":\"942adda8f8dad4b5f9ef506f768e72c6\"},\"data\":[{\"header\":{\"id\":\"ControlList-2023-04-19T07:10:01.875Z\",\"type\":\"ControlList\"},\"status\":{\"code\":\"000\",\"description\":\"Transacción Exitosa\"},\"system\":\"AWS\",\"product\":\"PSHEC\",\"customerDocumentType\":\"CC\",\"customerDocumentId\":\"000000123456789\",\"customerFullName\":\"PEPITO PEREZ\",\"customerType\":\"N\",\"thirdPartyControl\":\"CLIENTE\",\"customerStatus\":\"INACTIVO\",\"state\":\"Vigente\",\"alerts\":\"2\",\"message\":\"REALIZAR BIOMETRIA-NO RADICAR NOVEDAD EN SAP\",\"categories\":\"69\",\"ofacMessageOther\":\"CON\",\"passport\":\"\"}],\"links\":{\"self\":\"https://gw-otras-funciones-de-apoyo.apps.ocpdes.ambientesbc.lab/private-bancolombia/development/v2/risk-compliance/models/customer-behavior/customer/actions/check-control-list\"}}";
	}
	@PostMapping(path="/mockContactos")
	public String mockContactos(@RequestBody Map<String,Object>datos){
		return"{\"meta\":{\"_messageId\":\"c4e6bd04-5149-11e7-b114-b2f933d5fe66\",\"_requestDateTime\":\"2017-01-24T05:00:00.000Z\",\"_applicationId\":\"acxff62e-6f12-42de-9012-3e7304418abd\",\"additionalProp1\":{}},\"data\":{\"relationshipInformation\":[{\"mdmKey\":\"TIPDOC_FS000\",\"relationshipType\":\"BUR001\",\"mdmKeyRelationship\":\"123132132132\",\"contactType\":\"TIPCTO_01\",\"contactArea\":\"AREA_01\",\"legalRepresentativeFlag\":\"P\",\"participationPercentage\":\"\"}]},\"links\":{\"self\":\"/v1/sales-services/customer-management/Customer-Relationship-Management/customer-relationships/retrieve-contact-relationships\"}}";
	}
	@PostMapping(path="/mockInformacion")
	public String mockInformacion(@RequestBody Map<String,Object>datos){
		return"{\"meta\":{\"_messageId\":\"c4e6bd04-5149-11e7-b114-b2f933d5fe66\",\"_requestDateTime\":\"2017-01-24T05:00:00.000Z\",\"_applicationId\":\"acxff62e-6f12-42de-9012-3e7304418abd\",\"additionalProp1\":{}},\"data\":{\"mdmKey\":\"123456\",\"customer\":{\"documentType\":\"CARNE DIPLOMÁTICO\",\"documentId\":\"123456789\"},\"typeCustomer\":\"Persona Natural\",\"fullName\":\"string\",\"role\":\"PROSPECTO\",\"vinculationState\":\"CREACIÓN\",\"vinculationDate\":\"2020-08-11T08:01:20.000Z\",\"dateLastUpdate\":\"2020-08-11T08:01:20.000Z\",\"customerStatus\":\"INACTIVO\",\"relatedType\":\"Conyuge\",\"authorizeSharingInformation\":\"S\",\"specialDial\":\"BLOQUEO\"},\"links\":{\"self\":\"/v2/sales-services/customer-management/customer-reference-data-management/customer-personal-data/retrieve-basic-information\"}}";
	}
}