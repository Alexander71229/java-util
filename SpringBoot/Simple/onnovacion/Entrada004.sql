DECLARE
	CURSOR c IS SELECT * FROM ts_ase_servicios WHERE ROWNUM<=10;
	--CURSOR c IS SELECT * FROM dual WHERE ROWNUM<=10;
	TYPE t IS TABLE OF c%ROWTYPE;
	r c%ROWTYPE;
	n VARCHAR2(13);
	v t:=t();
	i NUMBER:=0;
	x sys_refcursor;
	FUNCTION f RETURN t PIPELINED IS
	BEGIN
		FOR r IN c LOOP
			PIPE ROW(R);
		END LOOP;
	END;
BEGIN
	/*FOR r IN c LOOP
		--?:=r.numero_servicio;
		?:=r;
	END LOOP;*/
	--i:=dbms_sql.open_cursor;
	--dbms_sql.parse(i,'FOR SELECT * FROM TABLE(v)',dbms_sql.native);
	FOR r IN c LOOP
		v.extend;
		v(v.count):=r;
	END LOOP;
	v:=?;
	--OPEN x FOR SELECT * FROM TABLE(v);
	/*FOR r IN c LOOP
		?:=i||':'||r.numero_servicio;
		i:=i+1;
	END LOOP;*/
	--OPEN ? FOR SELECT * FROM TABLE(t);
	/*OPEN c;
	FETCH c INTO ?;*/
	--SELECT * FROM TABLE(v);
END;

