DECLARE
	CURSOR c IS SELECT column_name n,data_length l,data_type t FROM all_tab_cols WHERE table_name='TS_TAR_TEMP_BASE_INFORMES';
	s1 VARCHAR2(10000);
	s2 VARCHAR2(10000);
	s VARCHAR2(1):='';
BEGIN
	s1:='INSERT INTO TS_TAR_TEMP_BASE_INFORMES(';
	FOR r IN c LOOP
		IF r.t='VARCHAR2' THEN
			s1:=s1||s||r.n;
			s2:=s2||s||''''||SUBSTR(r.n,1,r.l)||'''';
			s:=',';
		END IF;
	END LOOP;
	s1:=s1||')VALUES('||s2||')';
	DBMS_OUTPUT.PUT_LINE(s1);
	EXECUTE IMMEDIATE s1;
	OPEN ? FOR
	SELECT columna01 nitEmisor,columna02 claseDocumento,columna80 fechaGPagare,columna03 tipoPagare,
				 columna04 numeroPagare,columna05 reembolsable,columna81 fechaDesembolso, columna87 oficinaDesembolso,
				 columna06 pais,
				 columna07 departamento,columna08 ciudad,columna09 nitGestor,columna10 codGeoReferenciacion,
				 columna85 fechaFirmaFisico,columna11 nombreImagen,columna12 codDepositante,columna13 usuario,
				 columna21 clasePersonaRL,columna22 rolPersonaRL,columna23 tipoIdentificacionRL,columna24 identificacionRL,
				 columna25 cuentaDECEVALRL,columna26 emailRL,columna27 nombresRL,columna28 primerApellidoRL,
				 columna29 segundoApellidoRL,columna40 celularRL,columna41 tipoRL,columna42 nitEntidadRL,
				 columna43 clasePersonaOt,columna44 rolPersonaOt,columna45 tipoIdentificacionOt,columna46 identificacionOt,
				 columna48 cuentaDECEVALOt,columna49 emailOt,columna50 nombresOt,columna51 primerApellidoOt,
				 columna62 segundoApellidoOt,columna63 celularOt,columna66 tipoOt,columna67 nitEntidadOt,
				 columna68 clasePersonaCo,columna69 rolPersonaCo,columna70 tipoIdentificacionCo,columna71 identificacionCo,
				 columna72 cuentaDECEVALCo,columna73 emailCo,columna74 nombresCo,columna75 primerApellidoCo,
				 columna76 segundoApellidoCo,columna77 celularCo,columna78 tipoCo,columna79 nitEntidadCo
	FROM ts_tar_temp_base_informes t;
	COMMIT;
END;