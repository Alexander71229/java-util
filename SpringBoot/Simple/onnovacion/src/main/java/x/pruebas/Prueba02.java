package x.pruebas;
import x.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.*;
import co.com.cotrafa.cleanarchitecture.commons.util.Mapper;
import org.springframework.transaction.annotation.Transactional;
import x.dao.entidades.e01.Temp;
import x.dao.TempRepository;
import x.pruebas.b.B02;
import x.pruebas.b.B03;
import co.com.onnovacion.exceltransform.usecases.EscribirCsv;
import co.com.onnovacion.exceltransform.usecases.EscribirExcel;
import co.com.onnovacion.exceltransform.usecases.LeerCsv;
import co.com.onnovacion.exceltransform.enums.ExcelType;
import java.math.BigDecimal;
@Service
public class Prueba02{
	@Autowired
	private IRepositorio repositorio;
	@Autowired
	private TempRepository tempRepository;
	//@Autowired
	//private TempRepository01 tempRepository01;
	@Transactional
	public void e2()throws Exception{
		c.U.traza();
		Temp t=new Temp();
		t.setColumna01("Valor01");
		tempRepository.save(t);
		repositorio.t01();
		List<Temp>lista=tempRepository.findAll();
		lista.forEach(x->c.U.imp(x.getColumna01()));
		System.exit(0);
	}
	public void e()throws Exception{
		c.U.traza();
		repositorio.t02();
	}
	public void e3()throws Exception{
		c.U.traza();
		Map<String,Object>m=repositorio.ejecutarProcedimientoAlmacenado("procedimiento01",new HashMap<String,Object>());
		c.U.imp(m+"");
		System.exit(0);
	}
	public void e4()throws Exception{
		{
			List<B02>l=new ArrayList<>();
			l.add(new B02("A","B",new Date(),new BigDecimal(26)));
			l.add(new B02("C,","X\"K,\"",null,new BigDecimal(26)));
			String datos=new EscribirCsv<B02>().escribirACadena(l,",","\n","","","nulo");
			c.U.imp(datos);
			List<B03>lista=LeerCsv.getInstance(B03.class).leer(datos.getBytes(),",",false).getDatos();
			lista.stream().forEach(x->c.U.imp(x));
		}
		{
			List<B02>l=new ArrayList<>();
			l.add(new B02("A","B",new Date(),new BigDecimal(26)));
			l.add(new B02("C,","X\"K,\"",null,new BigDecimal(26)));
			String datos=new EscribirCsv<B02>().escribirACadena(l,";","\n","","","nulo");
			c.U.imp(datos);
			List<B03>lista=LeerCsv.getInstance(B03.class).leer(datos.getBytes(),";",false).getDatos();
			lista.stream().forEach(x->c.U.imp(x));
		}
		System.exit(0);
	}
	public void e5()throws Exception{
		c.U.traza();
		repositorio.t03();
		System.exit(0);
	}
	public void e6()throws Exception{
		c.U.traza();
		List<List<Object>>datos=new ArrayList<>();
		datos.add(new ArrayList<Object>());
		datos.get(datos.size()-1).add("C0V01");
		datos.get(datos.size()-1).add("C0V02");
		datos.get(datos.size()-1).add("C0V03");
		datos.get(datos.size()-1).add("C0V04");
		datos.get(datos.size()-1).add("C0V05");
		datos.add(new ArrayList<Object>());
		datos.get(datos.size()-1).add("C1V01");
		datos.get(datos.size()-1).add("C1V02");
		datos.get(datos.size()-1).add("C1V03");
		datos.get(datos.size()-1).add("C1V04");
		datos.get(datos.size()-1).add("C1V05");
		c.U.write("T01.xlsx",EscribirExcel.getInstance().escribirObjeto(datos,ExcelType.XLSX));
		System.exit(0);
	}
}