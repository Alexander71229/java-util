package x.pruebas.b;
import java.math.BigDecimal;
public class B01{
	private BigDecimal perId;
	private String serCodigo;
	private String numeroServicio;
	private int plazo;
	public void setPerId(BigDecimal perId){
		this.perId=perId;
	}
	public BigDecimal getPerId(){
		return this.perId;
	}
	public void setSerCodigo(String serCodigo){
		this.serCodigo=serCodigo;
	}
	public String getSerCodigo(){
		return this.serCodigo;
	}
	public void setNumeroServicio(String numeroServicio){
		this.numeroServicio=numeroServicio;
	}
	public String getNumeroServicio(){
		return this.numeroServicio;
	}
	public void setPlazo(int plazo){
		this.plazo=plazo;
	}
	public int getPlazo(){
		return this.plazo;
	}
	public String toString(){
		return perId+":"+serCodigo+":"+numeroServicio+":"+plazo;
	}
}