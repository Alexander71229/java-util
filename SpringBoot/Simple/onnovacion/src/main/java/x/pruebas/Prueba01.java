package x.pruebas;
import x.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.*;
import co.com.cotrafa.cleanarchitecture.commons.util.Mapper;
@Service
public class Prueba01{
	@Autowired
	private IRepositorio repositorio;
	public void prueba01()throws Exception{
		Map<String,Object>parametros=new HashMap<>();
		parametros.put("diFechaCorte",new Date());
		parametros.put("viPortafolio","viPortafolio");
		parametros.put("viTipoGestor","viTipoGestor");
		Map<String,Object>salida=repositorio.ejecutarProcedimientoAlmacenado("PKS_PORTAFOLIOS_ATL","GENERAR_GESTOR_DOCUMENTAL",parametros);
		Map<String,String>mapaNombres=new HashMap<String,String>();
		mapaNombres.put("PER_ID","perId");
		mapaNombres.put("SER_CODIGO","serCodigo");
		mapaNombres.put("NUMERO_SERVICIO","numeroServicio");
		mapaNombres.put("PLAZO","plazo");
		c.U.imp("------>"+Arrays.stream("PER_ID".split("_")).map(x->x.substring(0,1).toUpperCase()+x.substring(1).toLowerCase()).collect(Collectors.joining("")));
		c.U.imp("Mapeo:"+Mapper.convertirListaMapaAListaObjeto((List<Map<String,Object>>)salida.get("CPORTAFOLIO"),x.pruebas.b.B01.class,mapaNombres));
	}
	public void prueba02()throws Exception{
		Map<String,Object>parametros=new HashMap<>();
		parametros.put("vcodformato","441");
		parametros.put("nidprogramacion","22362");
		parametros.put("vperiodicidad","DIARIO");
		Map<String,Object>salida=repositorio.ejecutarProcedimientoAlmacenado("PKS_REPORTES_SUPER","FORMATO_441",parametros);
		salida.forEach((k,v)->c.U.imp(k+":"+v));
	}
}