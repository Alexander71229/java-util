package x.beans;
import co.com.onnovacion.exceltransform.annotations.NombreColumna;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PortafolioATL {
	
	@NombreColumna("")
	private String nitEmisor;
	@NombreColumna("")
	private String claseDocumento;
	@NombreColumna("")
	private String fechaGPagare;
	@NombreColumna("")
	private String tipoPagare;
	@NombreColumna("")
	private String numeroPagare;
	@NombreColumna("")
	private String reembolsable;
	@NombreColumna("")
	private String fechaDesembolso;
	@NombreColumna("")
	private String oficinaDesembolso;
	@NombreColumna("")
	private String pais;
	@NombreColumna("")
	private String departamento;
	@NombreColumna("")
	private String ciudad;
	@NombreColumna("")
	private String nitGestor;
	@NombreColumna("")
	private String codGeoReferenciacion;
	@NombreColumna("")
	private String fechaFirmaFisico;
	@NombreColumna("")
	private String nombreImagen;
	@NombreColumna("")
	private String codDepositante;
	@NombreColumna("")
	private String usuario;
	
	/****************************Representante legal**********************************/
	@NombreColumna("")
	private String clasePersonaRL;
	@NombreColumna("")
	private String rolPersonaRL;
	@NombreColumna("")
	private String tipoIdentificacionRL;
	@NombreColumna("")
	private String identificacionRL;
	@NombreColumna("")
	private String cuentaDECEVALRL;
	@NombreColumna("")
	private String emailRL;
	@NombreColumna("")
	private String nombresRL;
	@NombreColumna("")
	private String primerApellidoRL;
	@NombreColumna("")
	private String segundoApellidoRL;
	@NombreColumna("")
	private String celularRL;
	@NombreColumna("")
	private String tipoRL;
	@NombreColumna("")
	private String nitEntidadRL;

	/****************************Codeudores**********************************/
	@NombreColumna("")
	private String clasePersonaCo;
	@NombreColumna("")
	private String rolPersonaCo;
	@NombreColumna("")
	private String tipoIdentificacionCo;
	@NombreColumna("")
	private String identificacionCo;
	@NombreColumna("")
	private String cuentaDECEVALCo;
	@NombreColumna("")
	private String emailCo;
	@NombreColumna("")
	private String nombresCo;
	@NombreColumna("")
	private String primerApellidoCo;
	@NombreColumna("")
	private String segundoApellidoCo;
	@NombreColumna("")
	private String celularCo;
	@NombreColumna("")
	private String tipoCo;
	@NombreColumna("")
	private String nitEntidadCo;
	
	/****************************Otorgante**********************************/
	@NombreColumna("")
	private String clasePersonaOt;
	@NombreColumna("")
	private String rolPersonaOt;
	@NombreColumna("")
	private String tipoIdentificacionOt;
	@NombreColumna("")
	private String identificacionOt;
	@NombreColumna("")
	private String cuentaDECEVALOt;
	@NombreColumna("")
	private String emailOt;
	@NombreColumna("")
	private String nombresOt;
	@NombreColumna("")
	private String primerApellidoOt;
	@NombreColumna("")
	private String segundoApellidoOt;
	@NombreColumna("")
	private String celularOt;
	@NombreColumna("")
	private String tipoOt;
	@NombreColumna("")
	private String nitEntidadOt;

	
}