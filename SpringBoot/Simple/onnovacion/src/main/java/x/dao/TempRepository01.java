package x.dao;
import x.dao.entidades.e01.Temp;
import java.util.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.sql.DataSource;
@org.springframework.stereotype.Repository
@Qualifier("Temp01")
public class TempRepository01 implements TempRepository{
	private TempRepository i;
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setDataSource(final DataSource dataSource){
		this.dataSource=dataSource;
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	public void setI(TempRepository i){
		this.i=i;
	}
	public TempRepository getI(){
		return this.i;
		
	}
	public Temp save(Temp t){
		c.U.imp("Dentro:"+t+":"+i);
		return i.save(t);
	}
	public Optional<Temp>findById(String id){
		return i.findById(id);
	}
	public List<Temp>findAll(){
		return i.findAll();
	}
}