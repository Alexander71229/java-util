package x.dao;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.sql.*;
import java.sql.Types;
import java.sql.Connection;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import co.com.cotrafa.cleanarchitecture.commons.util.Mapper;
import x.u.Mapper3;
import co.com.cotrafa.cleanarchitecture.database.util.ResultSets;
import java.sql.ResultSet;
import x.beans.PortafolioATL;
import oracle.sql.StructDescriptor;
import oracle.sql.STRUCT;
import oracle.jdbc.OracleTypes;
@Repository
public class Repositorio implements IRepositorio{
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setDataSource(final DataSource dataSource){
		this.dataSource=dataSource;
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	public DataSource getDataSource(){
		return this.dataSource;
	}
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String esquema,String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withSchemaName(esquema).withCatalogName(paquete).withProcedureName(nombreProcedimiento).execute(parametrosEntrada);
	}
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withCatalogName(paquete).withProcedureName(nombreProcedimiento).execute(parametrosEntrada);
	}
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withProcedureName(nombreProcedimiento).execute(parametrosEntrada);
	}
	public void t01()throws Exception{
		Connection conexion=this.dataSource.getConnection();
		{
			CallableStatement x=conexion.prepareCall(c.U.g("Entrada001.sql"));
			x.registerOutParameter(1,Types.VARCHAR);
			x.executeUpdate();
			c.U.imp(x.getString(1));
		}
		{
			CallableStatement x=conexion.prepareCall(c.U.g("Entrada001.sql"));
			x.registerOutParameter(1,Types.VARCHAR);
			x.executeUpdate();
			c.U.imp(x.getString(1));
		}
		List<ArrayList<Object>>lista=ResultSets.convertirALista(conexion.createStatement().executeQuery(c.U.g("Entrada002.sql")));
		c.U.imp("Mostranto la lista:");
		lista.forEach(z->c.U.imp(z+""));
	}
	public void t02()throws Exception{
		Connection conexion=this.dataSource.getConnection();
		//CallableStatement x=conexion.prepareCall("{CALL PKS_PORTAFOLIOS_ATL.portafolios_ATL(?,?,?)}");
		CallableStatement x=conexion.prepareCall(c.U.g("Entrada003.sql"));
		//x.setString(1,"31/10/2019");
		//x.setString(2,"ATL-1");
		//x.registerOutParameter(3,Types.REF_CURSOR);
		x.registerOutParameter(1,Types.REF_CURSOR);
		x.executeUpdate();
		ResultSet rs=(ResultSet)x.getObject(1);
		c.U.traza();
		List<PortafolioATL>lista=Mapper3.convertirListaMapaAListaObjeto(ResultSets.convertirAListaMapa(rs),PortafolioATL.class,1);
		c.U.imp("Mostranto la lista:"+lista);
		//ResultSets.convertirAListaMapa(rs).forEach(z->c.U.imp(z+""));
		lista.forEach(z->c.U.imp((Object)z));
		System.exit(0);
	}
	public void t03()throws Exception{
		Connection conexion=this.dataSource.getConnection();
		CallableStatement x=conexion.prepareCall(c.U.g("Entrada004.sql"));
		//Statement x=conexion.createStatement();
		//ResultSet rs=x.executeQuery(c.U.g("Entrada004.sql"));
		//x.setObject(1,new STRUCT(StructDescriptor.createDescriptor("R",conexion),conexion,new Object[]{"X"}));
		//x.registerOutParameter(1,OracleTypes.VARCHAR);
		//x.registerOutParameter(1,Types.REF_CURSOR);
		x.registerOutParameter(1,OracleTypes.JAVA_STRUCT,"TS_ASE_SERVICIOS%ROWTYPE");
		//x.registerOutParameter(1,OracleTypes.REF_CURSOR);
		//x.registerOutParameter(1,Types.VARCHAR);
		x.executeUpdate();
		c.U.imp(x.getObject(1)+"");
		//c.U.imp(rs+"");
	}
}