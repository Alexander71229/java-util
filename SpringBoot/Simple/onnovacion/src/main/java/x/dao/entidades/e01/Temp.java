package x.dao.entidades.e01;
import lombok.*;
import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;
@Entity
@Table(name="TS_TAR_TEMP_BASE_INFORMES")
@Getter
@Setter
@NoArgsConstructor
public class Temp implements Serializable{
	@Column(name="COLUMNA110")
	private String columna110;
	@Column(name="COLUMNA111")
	private String columna111;
	@Column(name="COLUMNA112")
	private String columna112;
	@Column(name="COLUMNA113")
	private String columna113;
	@Column(name="COLUMNA114")
	private String columna114;
	@Column(name="COLUMNA115")
	private String columna115;
	@Column(name="COLUMNA116")
	private String columna116;
	@Column(name="COLUMNA117")
	private String columna117;
	@Column(name="COLUMNA118")
	private String columna118;
	@Column(name="COLUMNA119")
	private Double columna119;
	@Column(name="COLUMNA120")
	private Double columna120;
	@Column(name="COLUMNA121")
	private Double columna121;
	@Column(name="PER_ID")
	private Long perId;
	@Column(name="ASERVIC_CONSECUTIVO_SERVICIO")
	private Long aservicConsecutivoServicio;
	@Column(name="TYPE_RECORD")
	private String typeRecord;
	@Column(name="TRANS_CREDITO")
	private Double transCredito;
	@Column(name="TRANS_DEBITO")
	private Double transDebito;
	@Column(name="COLUMNA122")
	private String columna122;
	@Column(name="COLUMNA123")
	private String columna123;
	@Column(name="COLUMNA124")
	private String columna124;
	@Column(name="COLUMNA125")
	private String columna125;
	@Column(name="COLUMNA126")
	private String columna126;
	@Column(name="COLUMNA127")
	private String columna127;
	@Column(name="COLUMNA128")
	private String columna128;
	@Column(name="COLUMNA129")
	private String columna129;
	@Column(name="COLUMNA130")
	private String columna130;
	@Column(name="COLUMNA131")
	private String columna131;
	@Id
	@Column(name="COLUMNA01")
	private String columna01;
	@Column(name="COLUMNA02")
	private String columna02;
	@Column(name="COLUMNA03")
	private String columna03;
	@Column(name="COLUMNA04")
	private String columna04;
	@Column(name="COLUMNA05")
	private String columna05;
	@Column(name="COLUMNA06")
	private String columna06;
	@Column(name="COLUMNA07")
	private String columna07;
	@Column(name="COLUMNA08")
	private String columna08;
	@Column(name="COLUMNA09")
	private String columna09;
	@Column(name="COLUMNA10")
	private String columna10;
	@Column(name="COLUMNA11")
	private String columna11;
	@Column(name="COLUMNA12")
	private String columna12;
	@Column(name="COLUMNA13")
	private String columna13;
	@Column(name="COLUMNA14")
	private String columna14;
	@Column(name="COLUMNA15")
	private String columna15;
	@Column(name="COLUMNA16")
	private String columna16;
	@Column(name="COLUMNA17")
	private String columna17;
	@Column(name="COLUMNA18")
	private String columna18;
	@Column(name="COLUMNA19")
	private String columna19;
	@Column(name="COLUMNA20")
	private Long columna20;
	@Column(name="COLUMNA21")
	private String columna21;
	@Column(name="COLUMNA22")
	private String columna22;
	@Column(name="COLUMNA23")
	private String columna23;
	@Column(name="COLUMNA24")
	private String columna24;
	@Column(name="COLUMNA25")
	private String columna25;
	@Column(name="COLUMNA26")
	private String columna26;
	@Column(name="COLUMNA27")
	private String columna27;
	@Column(name="COLUMNA28")
	private String columna28;
	@Column(name="COLUMNA29")
	private String columna29;
	@Column(name="COLUMNA30")
	private Long columna30;
	@Column(name="COLUMNA31")
	private Long columna31;
	@Column(name="COLUMNA32")
	private Long columna32;
	@Column(name="COLUMNA33")
	private Long columna33;
	@Column(name="COLUMNA34")
	private Long columna34;
	@Column(name="COLUMNA35")
	private Long columna35;
	@Column(name="COLUMNA36")
	private Long columna36;
	@Column(name="COLUMNA37")
	private Long columna37;
	@Column(name="COLUMNA38")
	private Long columna38;
	@Column(name="COLUMNA39")
	private Long columna39;
	@Column(name="COLUMNA40")
	private String columna40;
	@Column(name="COLUMNA41")
	private String columna41;
	@Column(name="COLUMNA42")
	private String columna42;
	@Column(name="COLUMNA43")
	private String columna43;
	@Column(name="COLUMNA44")
	private String columna44;
	@Column(name="COLUMNA45")
	private String columna45;
	@Column(name="COLUMNA46")
	private String columna46;
	@Column(name="COLUMNA47")
	private Long columna47;
	@Column(name="COLUMNA48")
	private String columna48;
	@Column(name="COLUMNA49")
	private String columna49;
	@Column(name="COLUMNA50")
	private String columna50;
	@Column(name="COLUMNA51")
	private String columna51;
	@Column(name="COLUMNA60")
	private Double columna60;
	@Column(name="COLUMNA61")
	private Double columna61;
	@Column(name="COLUMNA62")
	private String columna62;
	@Column(name="COLUMNA63")
	private String columna63;
	@Column(name="COLUMNA64")
	private Long columna64;
	@Column(name="COLUMNA65")
	private Long columna65;
	@Column(name="COLUMNA66")
	private String columna66;
	@Column(name="COLUMNA67")
	private String columna67;
	@Column(name="COLUMNA68")
	private String columna68;
	@Column(name="COLUMNA69")
	private String columna69;
	@Column(name="COLUMNA70")
	private String columna70;
	@Column(name="COLUMNA71")
	private String columna71;
	@Column(name="COLUMNA72")
	private String columna72;
	@Column(name="COLUMNA73")
	private String columna73;
	@Column(name="COLUMNA74")
	private String columna74;
	@Column(name="COLUMNA75")
	private String columna75;
	@Column(name="COLUMNA76")
	private String columna76;
	@Column(name="COLUMNA77")
	private String columna77;
	@Column(name="COLUMNA78")
	private Long columna78;
	@Column(name="COLUMNA79")
	private String columna79;
	@Column(name="COLUMNA80")
	private String columna80;
	@Column(name="COLUMNA81")
	private String columna81;
	@Column(name="COLUMNA82")
	private Long columna82;
	@Column(name="COLUMNA83")
	private Long columna83;
	@Column(name="COLUMNA84")
	private Long columna84;
	@Column(name="COLUMNA85")
	private String columna85;
	@Column(name="COLUMNA86")
	private Long columna86;
	@Column(name="COLUMNA87")
	private String columna87;
	@Column(name="COLUMNA88")
	private Long columna88;
	@Column(name="COLUMNA89")
	private String columna89;
	@Column(name="COLUMNA90")
	private Long columna90;
	@Column(name="COLUMNA91")
	private String columna91;
	@Column(name="COLUMNA92")
	private Long columna92;
	@Column(name="COLUMNA93")
	private String columna93;
	@Column(name="COLUMNA94")
	private Long columna94;
	@Column(name="COLUMNA95")
	private String columna95;
	@Column(name="COLUMNA96")
	private String columna96;
	@Column(name="COLUMNA97")
	private String columna97;
	@Column(name="COLUMNA98")
	private String columna98;
	@Column(name="COLUMNA99")
	private Long columna99;
	@Column(name="COLUMNA100")
	private Double columna100;
	@Column(name="COLUMNA101")
	private Double columna101;
	@Column(name="COLUMNA102")
	private Date columna102;
	@Column(name="COLUMNA103")
	private Date columna103;
	@Column(name="COLUMNA104")
	private Date columna104;
	@Column(name="COLUMNA105")
	private Date columna105;
	@Column(name="COLUMNA106")
	private Date columna106;
	@Column(name="COLUMNA107")
	private String columna107;
	@Column(name="COLUMNA108")
	private String columna108;
	@Column(name="COLUMNA109")
	private String columna109;
}
