package x.dao;
import java.util.List;
import java.util.Map;
import java.sql.ResultSet;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
@Repository
public interface IRepositorio{
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String esquema,String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada);
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada);
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String nombreProcedimiento,Map<String,Object>parametrosEntrada);
	public DataSource getDataSource();
	public void t01()throws Exception;
	public void t02()throws Exception;
	public void t03()throws Exception;
}