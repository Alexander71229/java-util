package x.dao;
import x.dao.entidades.e01.Temp;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.*;
import java.math.BigDecimal;
@org.springframework.stereotype.Repository
public interface TempRepository extends Repository<Temp,String>{
	Temp save(Temp t);
	Optional<Temp>findById(String id);
	List<Temp>findAll();
}