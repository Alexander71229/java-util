package x;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.ProceedingJoinPoint;
import java.nio.file.Paths;
@Aspect
@Component
public class Ascpecto01{
	//@Around("execution(* *.*.*(*))")
	//x.pruebas
	@Around("execution(public * x.*.*(..))||execution(public * x.pruebas.*.*(..))")
	public void m01(ProceedingJoinPoint p){
		try{
			//c.U.imp(c.U.jar(org.springframework.aop.aspectj.AbstractAspectJAdvice.class)+"");
			//c.U.imp(c.U.jar(org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.class)+"");
			c.U.imp("p:"+p);
			c.U.imp("Antes:"+p.getTarget());
			Object o=p.proceed();
			//c.U.imp("Despues");
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}
