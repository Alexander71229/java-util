package x;
import org.springframework.stereotype.Component;
import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Autowired;
@Component
public class Inicio implements CommandLineRunner{
	@Autowired
	private x.pruebas.Prueba01 p;
	@Override
	public void run(String...argumentos)throws Exception{
		try{
			c.U.imp("Inicio:"+(new java.util.Date()));
			p.prueba2();
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}