package co.com.onnovacion.exceltransform.usecases;

import co.com.onnovacion.exceltransform.repository.LogicaRegistroLectura;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Clase que permite leer un archivo csv con todas las utilidades necesarias
 * @author hcaro
 * @since 04/11/2020
 */
public class LeerCsv<T> {

    // Lista con los datos que contiene el archivo csv
    private List<T> datos = new ArrayList<>();

    // Lista con la informacion del encabezado del archivo csv
    private List<String> encabezado;

    private Class<T> clase;

    public static LeerCsv getInstance(Class clase){
        LeerCsv reader =  new LeerCsv(new ArrayList());
        reader.setClase(clase);
        return reader;
    }

    private void setClase(Class<T> clase){
        this.clase = clase;
    }

    private LeerCsv(List<T> datos){
        this.datos = datos;
    }
    /**
     * Metodo que lee un archivo csv para convertirlo en la lista del tipo especificada
     * @author hcaro
     * @since 04/11/2020
     * @Param archivo: Documento de csv en array de bytes
     * @Param clase: Clase que contiene el modelo donde se insertara la informacion del archivo
     * @Param separador: Separador que tiene cada registro
     * @Param tieneEncabezado: Si el archivo posee encabezado
     * @return LeerCsv<T: Retorna la inforamcion this de esta clase para extraer la informacion
            * @throws InstantiationException
            * @throws InvocationTargetException
            * @throws IOException
            * @throws IllegalAccessException
            * @throws NoSuchMethodException
     */
    public LeerCsv<T> leer(byte[] archivo, String separador, boolean tieneEncabezado) throws InvocationTargetException, IOException, InstantiationException, NoSuchMethodException, IllegalAccessException {
        return (LeerCsv<T>) leer(archivo, separador, tieneEncabezado, x->x);
    }
    public LeerCsv<T> leer(byte[] archivo, String separador, boolean tieneEncabezado, LogicaRegistroLectura<T> logicaRegistroLectura)
            throws InstantiationException, IllegalAccessException, IOException, InvocationTargetException, NoSuchMethodException {
        Class<T> clase = this.clase;
        InputStream entrada = null;
        String linea = null;
        entrada = new ByteArrayInputStream(archivo);
        try (BufferedReader lector = new BufferedReader(new InputStreamReader(entrada));) {
            while ((linea = lector.readLine()) != null) {
                List<String> informacion = new ArrayList<>();
								boolean literal = false;
								StringBuilder valor = new StringBuilder();
								for(int i = 0; i < linea.length(); i++){
									if(literal){
										if(linea.charAt(i) == '"'){
											literal = false;
											continue;
										}
										valor.append(linea.charAt(i));
									}else{
										if(linea.charAt(i) == '"'){
											literal = true;
											continue;
										}
										if(linea.substring(i).startsWith(separador)){
											informacion.add((valor+"").replaceAll("”","\""));
											valor=new StringBuilder();
											continue;
										}
										valor.append(linea.charAt(i));
									}
								}
								informacion.add((valor+"").replaceAll("”","\""));
                if (tieneEncabezado){
                    setEncabezado(informacion);
                    tieneEncabezado = false;
                    continue;
                }
                T claseReferencia = clase.getDeclaredConstructor().newInstance();
                int posicion = 0;
                List<Field> listOfFieldNames = obtenerNombreAtributos(clase);
                for (Field nombreAtributo : listOfFieldNames) {
                    if (posicion >= informacion.size()) {
                        break;
                    }
                    asignarValorAtributo(claseReferencia, nombreAtributo, informacion.get(posicion++));
                }
                getDatos().add(logicaRegistroLectura.logicaRegistroLectura(claseReferencia));
            }
        }
        return this;
    }

    /**
     * Metodo que inserta un valor a un atributo de una clase indeterminada
     * @author hcaro
     * @since 04/11/2020
     * @Param claseReferencia: Clase que contiene el atributo
     * @Param atributo: Atributo de la clase al cual se le asignara el valor
     * @Param valor: Valor extraido de una linea del archivo csv
     * @return atributo: Atributo con el valor ya insertado
     * @throws IllegalAccessException
     */
    private Field asignarValorAtributo(T claseReferencia, Field atributo, String valor)
            throws IllegalAccessException {
        atributo.setAccessible(true);
        atributo.set(claseReferencia, valor);
        atributo.setAccessible(false);
        return atributo;
    }

    /**
     * Metodo que obtiene los atributos de una clase indeterminada
     * @author hcaro
     * @since 27/10/2020
     * @Param clase: Clase que se extraeran los atributos
     * @return nombreAtributos: Lista de los atributos de la clase
     */
    private List<Field> obtenerNombreAtributos(Class<?> clase) {
        List<Field> nombreAtributos = new ArrayList<>();
        Collections.addAll(nombreAtributos, clase.getDeclaredFields());
        return nombreAtributos;
    }

    /**
     * @return Lista de datos
     */
    public List<T> getDatos() {
        return datos;
    }

    /**
     * @Param Datos ha establecer en la lista
     */
    public void setDatos(List<T> datos) {
        this.datos = datos;
    }

    /**
     * @return Encabezado del archivo
     */
    public List<String> getEncabezado() {
        return encabezado;
    }

    /**
     * @Param Datos ha establecer en la lista del encabezado
     */
    public void setEncabezado(List<String> encabezado) {
        this.encabezado = encabezado;
    }

}
