package co.com.onnovacion.exceltransform.handlers;

public interface SpreadSheetPort<T, K> {
    public int getNumberOfRows();
    public String getName();
    public String[] getRow(int rowNumber, Integer totalColumnas);
    public K getRowByMapper(String[] row);
}
