package co.com.onnovacion.exceltransform.usecases;

import co.com.onnovacion.exceltransform.handlers.OpenDocumentSpreadSheetAdapter;
import co.com.onnovacion.exceltransform.handlers.PoiSpreadSheetAdapter;
import co.com.onnovacion.exceltransform.handlers.SpreadSheetPort;
import co.com.onnovacion.exceltransform.repository.SpreadSheetMapper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.odftoolkit.simple.SpreadsheetDocument;
import co.com.onnovacion.exceltransform.enums.ExcelType;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Interface que permite leer archivos en formato ods, xls, xlsx devolviendo una lista mapeada a tipo generico
 * @author oflorez
 * @since 04/11/2020
 */
public class LeerExcel<O> {

    private LeerExcel() {
    }

    public static LeerExcel getInstance(){
        return new LeerExcel();
    }

    public List<O> mapearExcelEnObjetoDesdeStream(InputStream file, String fileName, SpreadSheetMapper mapper) throws Exception {
        List<O> rows = new ArrayList<>();
        if (fileName.endsWith(ExcelType.ODS.getExtension())) {
            rows = leerOdsPorStream(file, mapper);
        } else if (fileName.endsWith(ExcelType.XLS.getExtension()) || fileName.endsWith(ExcelType.XLSX.getExtension())) {
            rows = leerExcelDesdeStream(fileName, file, mapper);
        }
        return rows;
    }
    /**
     *
     * @param fileName Ruta del archivo ods, xls o xlsx que se desea leer y convertir a objeto
     * @param mapper De tipo SpreadSheetMapper, permite traducir un String[] con el resultado por fila del archivo excel al tipo generico.
     * @return List<T> Retorna una lista con el tipo generico que se requiera
     * @throws Exception
     */
    public List<O> mapearExcelEnObjetoDesdeRuta(String fileName, SpreadSheetMapper mapper) throws Exception {
        List<O> rows = new ArrayList<>();

        if(fileName.endsWith(ExcelType.ODS.getExtension())){
            rows = leerOdsPorRuta(fileName, mapper);
        } else if(fileName.endsWith(ExcelType.XLS.getExtension()) || fileName.endsWith(ExcelType.XLSX.getExtension())) {
            rows = leerExcelDesdeStream(fileName, new FileInputStream(new File(fileName)), mapper);
        }

        return rows;
    }

    private List<O> leerExcelDesdeStream(String fileName, InputStream file, SpreadSheetMapper mapper) throws IOException {
        List<O> rows = new ArrayList<>();
        Workbook document = fileName.endsWith(ExcelType.XLS.getExtension())? new HSSFWorkbook(file):new XSSFWorkbook(file);
        Integer sheet = document.getNumberOfSheets();
        Integer totalColumnas = mapper.obtenerTotalColumnas();
        PoiSpreadSheetAdapter handler = PoiSpreadSheetAdapter.getInstance(document, mapper);
        Integer totalFila;
        for(Integer idSheet = 0; idSheet < sheet; idSheet ++){
            //Lectura de las filas por hoja
            handler.setSheet(idSheet);
            totalFila = document.getSheetAt(idSheet).getLastRowNum();
            for(int row = 1; row <= totalFila; row++) {
                rows.add((O) handler.getRowByMapper(handler.getRow(row, totalColumnas)));
            }
        }

        return rows;
    }

    private List<O> leerOdsPorRuta(String fileName, SpreadSheetMapper mapper) throws Exception {
        SpreadsheetDocument document = SpreadsheetDocument.loadDocument(fileName);
        return leerOds(mapper, document);
    }

    private List<O> leerOdsPorStream(InputStream file, SpreadSheetMapper mapper) throws Exception {
        SpreadsheetDocument document = SpreadsheetDocument.loadDocument(file);
        return leerOds(mapper, document);
    }

    private List<O> leerOds(SpreadSheetMapper mapper, SpreadsheetDocument document) {
        List<O> rows = new ArrayList<>();
        Integer sheet = document.getSheetCount();
        // Lectura de hojas del archivo
        OpenDocumentSpreadSheetAdapter handler = OpenDocumentSpreadSheetAdapter.getInstance(document, mapper);
        Integer totalColumnas = mapper.obtenerTotalColumnas();
        Integer totalFila;
        for(Integer idSheet = 0; idSheet < sheet; idSheet ++){
            //Lectura de las filas por hoja
            handler.setSheet(idSheet);
            totalFila = document.getSheetByIndex(idSheet).getRowCount();
            for(int row = 1; row < totalFila; row++) {
                rows.add((O) handler.getRowByMapper(handler.getRow(row, totalColumnas)));
            }
        }
        return rows;
    }
}
