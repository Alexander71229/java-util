package co.com.onnovacion.exceltransform.handlers;

import co.com.onnovacion.exceltransform.repository.SpreadSheetMapper;
import org.odftoolkit.simple.SpreadsheetDocument;
import org.odftoolkit.simple.table.Cell;
import org.odftoolkit.simple.table.Row;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OpenDocumentSpreadSheetAdapter<T> implements SpreadSheetPort<SpreadsheetDocument, T> {

        private SpreadsheetDocument spreadsheetDocument;
        private Integer sheet;
        private SpreadSheetMapper spreadSheetMapper;

        public static OpenDocumentSpreadSheetAdapter getInstance(final SpreadsheetDocument spreadsheetDocument,
                                                                 final SpreadSheetMapper spreadSheetMapper){

            return new OpenDocumentSpreadSheetAdapter(spreadsheetDocument, spreadSheetMapper);

        }

        private OpenDocumentSpreadSheetAdapter(final SpreadsheetDocument spreadsheetDocument,
                                               final SpreadSheetMapper spreadSheetMapper){
            this.spreadsheetDocument = spreadsheetDocument;
            this.spreadSheetMapper = spreadSheetMapper;
        }

        public void setSheet(Integer idSheet){
            this.sheet = idSheet;
        }

        public int getNumberOfRows() {
            return spreadsheetDocument.getSheetByIndex(sheet).getRowCount();
        }

        public String getName() {
            return spreadsheetDocument.getOfficeMetadata().getTitle();
        }

        public String[] getRow(int rowNumber, Integer totalColumnas) {
            Row row = spreadsheetDocument.getSheetByIndex(sheet).getRowByIndex(rowNumber);
            List<String> cells = new ArrayList<>();

            if (row == null) {
                return null;
            } else {

                for(Integer j = 0 ; j < totalColumnas; j ++) {
                    Cell cell = row.getCellByIndex(j);

                    if(Objects.isNull(cell.getValueType()) ){
                        cells.add("");
                    } else {
                        switch (cell.getValueType()){
                            case "boolean":
                                cells.add(cell.getBooleanValue().toString());
                                break;
                            case "currency":
                                cells.add(cell.getCurrencyCode().toString());
                                break;
                            case "date":
                                cells.add(new SimpleDateFormat("dd/MM/yyyy").format(cell.getDateValue().getTime()));
                                break;
                            case "float":
                                cells.add(cell.getDoubleValue().toString());
                                break;
                            case "percentage":
                                cells.add(cell.getPercentageValue().toString());
                                break;
                            case "string":
                                cells.add(cell.getStringValue());
                                break;
                            case "time":
                                cells.add(new SimpleDateFormat("HH:mm:ss").format(cell.getTimeValue().getTime()));
                                break;
                            default:
                                String formula = cell.getFormula();
                                if(null != formula) {
                                    cells.add(formula);
                                }
                                break;
                        }
                    }

                }
            }

            return cells.toArray(new String[0]);
        }

    public T getRowByMapper(String[] row) {
            return (T) spreadSheetMapper.obtenerObjetoConvertido(row);
    }
}
