package co.com.onnovacion.exceltransform.handlers;

import co.com.onnovacion.exceltransform.repository.SpreadSheetMapper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PoiSpreadSheetAdapter<T> implements SpreadSheetPort<Workbook, T> {
    private static Workbook document;
    private Integer sheet;
    private static SpreadSheetMapper spreadSheetMapper;

    private PoiSpreadSheetAdapter(Workbook workbook, SpreadSheetMapper mapper) throws IOException {
        document = workbook;
        spreadSheetMapper = mapper;
    }

    public void setSheet(Integer sheet) {
        this.sheet = sheet;
    }

    public static PoiSpreadSheetAdapter getInstance(Workbook document, SpreadSheetMapper mapper) throws IOException {
        return new PoiSpreadSheetAdapter(document, mapper);
    }

    @Override
    public int getNumberOfRows() {
        return this.document.getSheetAt(this.sheet).getLastRowNum();
    }

    @Override
    public String getName() {
        return this.document.getSheetAt(this.sheet).getSheetName();
    }

    @Override
    public String[] getRow(int rowNumber, Integer totalColumnas) {
        Row row = this.document.getSheetAt(this.sheet).getRow(rowNumber);
        List<String> cells = new ArrayList<>();
        for(Integer j = 0 ; j < totalColumnas; j ++) {
            Cell cell = row.getCell(j);
            if(Objects.isNull(cell.getCellType()) ){
                cells.add("");
            } else {
                switch (cell.getCellType().name()){
                    case "BOOLEAN":
                        cells.add(String.valueOf(cell.getBooleanCellValue()));
                        break;
                    case "NUMERIC":
                        cells.add(String.valueOf(cell.getNumericCellValue()));
                        break;
                    case "STRING":
                        cells.add(cell.getStringCellValue());
                        break;
                    case "FORMULA":
                        cells.add(cell.getCellFormula());
                        break;
                    default:
                        cells.add("");
                        break;
                }
            }
        }
        return cells.toArray(new String[0]);
    }

    @Override
    public T getRowByMapper(String[] row) {
        return (T) spreadSheetMapper.obtenerObjetoConvertido(row);
    }


}
