package co.com.onnovacion.exceltransform.repository;

/**
 * Interface funcional que especifica la logica de cada registro de un archivo mediante un Predicate
 * @author hcaro
 * @since 04/11/2020
 */
@FunctionalInterface
public interface LogicaRegistroLectura<T> {
	
	/**
	 * Expresion logica de cada registro de un archivo mediante un Predicate
	 * @author hcaro
	 * @since 04/11/2020
	 * @Param objeto: Objeto generico al cual se le va a aplicar una logica especifica
	 * @return String: Objeto generico
	 */
	T logicaRegistroLectura(T objeto);
	
}