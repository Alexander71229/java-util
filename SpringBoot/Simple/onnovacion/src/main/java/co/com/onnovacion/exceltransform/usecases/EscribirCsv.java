package co.com.onnovacion.exceltransform.usecases;


import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import co.com.onnovacion.exceltransform.annotations.NombreColumna;
import co.com.onnovacion.exceltransform.repository.LogicaRegistroEscritura;
import org.apache.commons.beanutils.PropertyUtils;


/**
 * Clase que permite generar un archivo csv con todas las utilidades necesarias
 * @author hcaro
 * @since 04/11/2020
 */
public class EscribirCsv<T> {

	// Logica especifica para cada registro
	private LogicaRegistroEscritura<T> logicaRegistroEscritura;

	/**
	 * Metodo constructor
	 * @author hcaro
	 * @since 04/11/2020
	 */
	public EscribirCsv() {
		this.logicaRegistroEscritura = x -> null;
	}

	/**
	 * Metodo constructor con Predicate para cada registro
	 * @author hcaro
	 * @Param logicaRegistroEscritura: Predicado con la logica que se realizara a cada registro antes de escribir
	 * @since 04/11/2020
	 */
	public EscribirCsv(LogicaRegistroEscritura<T> logicaRegistroEscritura) {
		this.logicaRegistroEscritura = logicaRegistroEscritura;
	}

	/**
	 * Metodo para escribir una lista de objetos en un String
	 * @author amolina
	 * @since 17/11/2020
	 * @Param datos: Lista de objetos.
	 * @Param separador: Separador usado entre objetos en una fila.
	 * @Param separadorLinea: Separador usado entre filas.
	 * @Param caracterInicial: Caracter inicial.
	 * @Param caracterFinal: Caracter final.
	 * @return array de bytes que representa los datos en formato CSV.
	 */
	public byte[] escribir(List<T>datos, String separador, String separadorLinea, String caracterInicial,
						   String caracterFinal, String reemplazarNull) {

		return escribirACadena(datos, separador, separadorLinea, caracterInicial ,caracterFinal, reemplazarNull)
				.getBytes(StandardCharsets.UTF_8);
	}

	/**
	 * Metodo para escribir una lista de objetos en un String
	 * @author amolina
	 * @since 17/11/2020
	 * @Param datos: Lista de objetos.
	 * @Param separador: Separador usado entre objetos en una fila.
	 * @Param separadorLinea: Separador usado entre filas.
	 * @Param caracterInicial: Caracter inicial.
	 * @Param caracterFinal: Caracter final.
	 * @return array de bytes que representa los datos en formato CSV.
	 */
	@SuppressWarnings("unchecked")
	public String escribirACadena(List<T> datos, String separador, String separadorLinea,
								  String caracterInicial, String caracterFinal, String reemplazarNull){

		ArrayList<Object> objetos = new ArrayList<>();
		ArrayList<Object> nombreColumnas = new ArrayList<>();
		nombreColumnas.addAll(obtenerNombreColumnas(datos.get(0).getClass()));
		if (!nombreColumnas.isEmpty()) {
			objetos.add(0, nombreColumnas);
		}
		objetos.addAll(datos);

		return objetos.stream().map(x -> {
			String linea = logicaRegistroEscritura.logicaRegistroEscritura((T)x);
			if(linea == null){
				return escribirLinea((T)x, separador, reemplazarNull);
			}
			return linea;
		}).collect(Collectors.joining(separadorLinea, caracterInicial, caracterFinal));
	}

	/**
	 * Metodo para representar un elemento en una cadena
	 * @author amolina
	 * @since 17/11/2020
	 * @Param elemento: elemento que representa una fila.
	 * @Param separador: Cadena que sera usada entre los valores del elemento.
	 * @return Cadena que representa el elemento.
	 */
	public String escribirLinea(T elemento, String separador, String reemplazarNull){

		return escribirListaLinea(convertirALista(elemento),separador, reemplazarNull);
	}

	/**
	 * Metodo para escribir una lista de objetos en un String
	 * @author amolina
	 * @since 17/11/2020
	 * @Param lista: lista que representa una fila.
	 * @Param separador: Cadena que será usada entre los valores de la lista.
	 * @return Cadena que representa la lista.
	 */
	public String escribirListaLinea(List<Object> lista, String separador, String reemplazarNull){
		return lista.stream().map(objeto->{
			if(objeto==null){
				if(reemplazarNull!=null){
					return reemplazarNull;
				}
				return "";
			}
			String celda=objeto.toString().replaceAll("\"","”");
			if(celda.indexOf(separador)>=0){
				celda="\""+celda+"\"";
			}
			return celda;
		}).collect(Collectors.joining(separador));
	}

	/**
	 * Metodo para convertir un elemento T a List<Object>
	 * @author amolina
	 * @since 17/11/2020
	 * @Param elemento: Elemento a convertir.
	 * @return List<Object> que representa el elemento.
	 */
	@SuppressWarnings("unchecked")
	public List<Object> convertirALista(T elemento) {

		if(elemento instanceof List){
			return (List<Object>) elemento;
		}

		return Arrays.stream(elemento.getClass().getDeclaredFields()).map(campo -> {
			try{
				return PropertyUtils.getProperty(elemento, campo.getName());
			}catch(Exception e){
				return "";
			}
		}).collect(Collectors.toList());
	}

	/**
	 * Metodo que obtiene los nombres de las columnas anotadas de un atributo de una clase indeterminada
	 * @author hcaro
	 * @since 27/10/2020
	 * @Param clase: Clase que se extraeran las columnas
	 * @return nombreColumnas: Lista de los nombres de las columnas de los atributos
	 */
	private List<String> obtenerNombreColumnas(Class<?> clase) {

		List<String> nombreColumnas = new ArrayList<>();
		for (Field atributo : clase.getDeclaredFields()) {
			NombreColumna nombreColumna = atributo.getAnnotation(NombreColumna.class);
			if (null != nombreColumna && !"".equals(nombreColumna.value())) {
				nombreColumnas.add(nombreColumna.value());
			} else if (null != nombreColumna){
				nombreColumnas.add(atributo.getName());
			}
		}
		return nombreColumnas;
	}

}