package co.com.onnovacion.exceltransform.enums;

public enum ExcelType {
    ODS(".ods"), XLS(".xls"), XLSX(".xlsx");

    private String ext;

    ExcelType(String ext){
        this.ext = ext;
    }

    public String getExtension(){
        return this.ext;
    }
}
