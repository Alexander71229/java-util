package co.com.onnovacion.exceltransform.usecases;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import co.com.onnovacion.exceltransform.annotations.NombreColumna;
import co.com.onnovacion.exceltransform.enums.ExcelType;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Clase que permite generar un archivo en Excel con todas las utilidades necesarias
 * @author hcaro
 * @since 27/10/2020
 */
public class EscribirExcel {
	
	/**
     * Metodo constructor
     * @author hcaro
     * @since 27/10/2020
     */
	private EscribirExcel() {}

	public static EscribirExcel getInstance(){
		return new EscribirExcel();
	}

	/**
     * Metodo que genera un archivo de Excel xlsx a raiz de un modelo de datos
     * @author hcaro
     * @since 27/10/2020
     * @Param datos: Modelo de datos que contiene la informaci�n a escribir en Excel
     * @return documento: Documento de Excel xlsx en array de bytes
	 * @throws IOException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
     */
	public <T> byte[] escribirDto(List<T> datos, ExcelType tipoArchivo)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] documento = null;
		if (null != datos && !datos.isEmpty()) {
			try (Workbook libro = (tipoArchivo == ExcelType.XLSX ?
					new XSSFWorkbook() : new HSSFWorkbook())) {
				Sheet hoja = libro.createSheet();
				List<String> nombreColumnas = obtenerNombreColumnas(datos.get(0).getClass());
				int numeroFila = 0;
				int numeroColumna = 0;
				if (!nombreColumnas.isEmpty()) {
					Row fila = hoja.createRow(numeroFila++);
					for (String nombreColumna : nombreColumnas) {
						Cell celda = fila.createCell(numeroColumna++);
						celda.setCellValue(nombreColumna);
					}
				}
				Class<? extends Object> clase = datos.get(0).getClass();
				for (T dato : datos) {
					Row fila = hoja.createRow(numeroFila++);
					numeroColumna = 0;
					List<String> nombreAtributos = obtenerNombreAtributos(datos.get(0).getClass());
					for (String atributo : nombreAtributos) {
						Method metodo = clase.getMethod("get" + mayusculaInicial(atributo));
						insertarValorCelda(fila.createCell(numeroColumna),
								metodo.invoke(dato, (Object[]) null));
						numeroColumna++;
					}
				}
				libro.write(bos);
				documento = bos.toByteArray();
			}
		}
		return documento;
	}
	
	/**
     * Metodo que inserta un object valor en una celda determinada
     * @author hcaro
     * @since 27/10/2020
     * @Param celda: Celda creada en el documento de Excel
     * @Param valor: valor a insertar en la celda
     */
	private static void insertarValorCelda(Cell celda, Object valor) {
		if (valor != null) {
			if (valor instanceof String) {
				celda.setCellValue((String) valor);
			} else if (valor instanceof Long) {
				celda.setCellValue((Long) valor);
			} else if (valor instanceof Integer) {
				celda.setCellValue((Integer) valor);
			} else if (valor instanceof Double) {
				celda.setCellValue((Double) valor);
			} else if (valor instanceof Date) {
				celda.setCellValue((Date) valor);
			} else if (valor instanceof Calendar) {
				celda.setCellValue((Calendar) valor);
			} else if (valor instanceof Boolean) {
				celda.setCellValue((Boolean) valor);
			} else {
				celda.setCellValue(valor.toString());
			}
		}
	}
	
	/**
     * Metodo que obtiene los nombres de los atributos de una clase indeterminada
     * @author hcaro
     * @since 27/10/2020
     * @Param clase: Clase que se extraeran los atributos
     * @return nombreAtributos: Lista de los atributos de la clase
     */
	private static List<String> obtenerNombreAtributos(Class<?> clase) {
		List<String> nombreAtributos = new ArrayList<>();
		for (Field atributo : clase.getDeclaredFields()) {
			nombreAtributos.add(atributo.getName());
		}
		return nombreAtributos;
	}
	
	/**
     * Metodo que obtiene los nombres de las columnas anotadas de un atributo de una clase indeterminada
     * @author hcaro
     * @since 27/10/2020
     * @Param clase: Clase que se extraeran las columnas
     * @return nombreColumnas: Lista de los nombres de las columnas de los atributos
     */
	private static List<String> obtenerNombreColumnas(Class<?> clase) {
		List<String> nombreColumnas = new ArrayList<>();
		for (Field atributo : clase.getDeclaredFields()) {
			NombreColumna nombreColumna = atributo.getAnnotation(NombreColumna.class);
			if (null != nombreColumna && !"".equals(nombreColumna.value())) {
				nombreColumnas.add(nombreColumna.value());
			} else if (null != nombreColumna){
				nombreColumnas.add(atributo.getName());
			}
		}
		return nombreColumnas;
	}
	
	/**
     * Metodo que convierte la letra inicial de una palabra en mayuscula
     * @author hcaro
     * @since 27/10/2020
     * @Param palabra: Palabra a la cual se le realizara la conversion
     * @return palabra: Palabra con la letra inicial en mayuscula
     */
	private static String mayusculaInicial(String palabra) {
		if (palabra.length() == 0) {
			return palabra;
		}
		return palabra.substring(0, 1).toUpperCase() + palabra.substring(1);
	}

	/**
		* Método para escribir una lista de objetos en un libro HSSF.
		* @author amolina
		* @since 18/11/2020
		* @Param datos: Lista de objetos.
		* @return Libro HSSF.
	*/
	public byte[] escribirObjeto(List<List<Object>> datos, ExcelType tipoArchivo) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] documento = null;
		if (null != datos && !datos.isEmpty()) {
			Workbook libro = (tipoArchivo == ExcelType.XLSX ? new XSSFWorkbook() : new HSSFWorkbook());
			Sheet hoja = libro.createSheet();
			hoja.createRow(0);
			datos.stream().forEach(datoFila -> {
				Row fila = hoja.getRow(hoja.getLastRowNum());
				hoja.createRow(hoja.getLastRowNum() + 1);
				datoFila.forEach(dato ->{
					Cell celda=null;
					if (fila.getLastCellNum() == -1) {
						celda = fila.createCell(0);
					} else {
						celda = fila.createCell(fila.getLastCellNum());
					}
					insertarValorCelda(celda, dato);
				});
			});
			libro.write(bos);
			documento = bos.toByteArray();
		}
		return documento;
	}
}
