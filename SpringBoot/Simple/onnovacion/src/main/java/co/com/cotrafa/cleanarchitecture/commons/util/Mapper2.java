package co.com.cotrafa.cleanarchitecture.commons.util;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.DestinationSetter;
import org.modelmapper.spi.MatchingStrategy;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class Mapper2 {

    private static ModelMapper modelMapper = new ModelMapper();

    static {
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);
    }

    private Mapper2() {
    }

    public static <D, T> D map(final T entidad, Class<D> dto) {
        return modelMapper.map(entidad, dto);
    }

    public static <D, T> List<D> mapAll(final Collection<T> listaEntidad, Class<D> dto) {
        return listaEntidad.stream()
                .map(entity -> map(entity, dto))
                .collect(Collectors.toList());
    }

    public static <S, D> D map(final S source, D destination) {
        modelMapper.map(source, destination);
        return destination;
    }
		
	public static void setMatchingStrategy(MatchingStrategy strategy) {
			modelMapper.getConfiguration().setMatchingStrategy(strategy);
	}

	public static <S, D, V> void addSkip(Class<S> sourceType, Class<D> destinationType,
											DestinationSetter<D, V> data) {
			modelMapper.typeMap(sourceType, destinationType).addMappings(mapper -> mapper.skip(data));
	}
	
	/**
	* Retorna un mapa de los métodos declarados de la clase indexados por el nombre del método.
	* @author amolina
	* @since 23/11/2020
	* @Param clase: Clase de la que se extraerán los métodos declarados.
	* @Param cantidadParametros: Tomará los métodos declarados con la cantidad de parámetros especificados. Si el valor es -1 no tendrá en cuenta la cantidad de parámetros.
	* @return Mapa de métodos indexado por el nombre de los métodos.
	*/
	public static Map<String, Method> obtenerMapaMetodosDeclarados(Class clase, int cantidadParametros) {
		Map<String, Method> mapaMetodos = new HashMap<String, Method>();
		Arrays.stream(clase.getDeclaredMethods()).forEach(m -> {
			if(m.getParameterCount() == cantidadParametros || cantidadParametros == -1) {
				mapaMetodos.put(m.getName(), m);
			}
		});
		return mapaMetodos;
	}

	/**
	* Retorna una lista de objetos de tipo D desde un lista de lista de objetos.
	* @author amolina
	* @since 19/11/2020
	* @Param datos: Lista de lista de objetos con la información de entrada.
	* @Param clase: Clase que representa al objeto de tipo D.
	* @Param nombres: Nombres de los campos declarados a los que se les asignará el valor desde su respectivo método set.
	* @return Lista de objetos de tipo D.
	*/
	public static <D> List<D> convertirAListaObjeto (List<ArrayList<Object>>datos, Class<D>clase, String[]nombres) {
		return datos.stream().map(x -> map(nombres, x, clase)).collect(Collectors.toList());
	}

	/**
	* Retorna una lista de objetos de tipo D desde un lista de lista de objetos.
	* @author amolina
	* @since 19/11/2020
	* @Param datos: Lista de lista de objetos con la información de entrada.
	* @Param clase: Clase que representa al objeto de tipo D.
	* @return Lista de objetos de tipo D.
	*/
	public static <D> List<D> convertirAListaObjeto(List<ArrayList<Object>>datos, Class<D> clase) {
		return convertirAListaObjeto(datos, clase,
				Arrays.stream(clase.getDeclaredFields()).map(Field::getName).toArray(String[]::new));
	}

	/**
	* Retorna un objeto de tipo D tomando la información de una lista de objetos y una clase que la describe.
	* @author amolina
	* @since 19/11/2020
	* @Param nombres: Nombres de los campos declarados a los que se les asignará el valor desde su respectivo método set.
	* @Param lista: lista de objetos con la información de entrada.
	* @Param clase: Clase que representa al objeto de tipo D.
	* @return Instancia de D con la información de la lista de objetos.
	*/
	public static <D> D map(String[] nombres, List<Object> lista, Class<D> clase) {
		try {
			D objeto = clase.newInstance();
			Map<String, Method> mapaMetodos = obtenerMapaMetodosDeclarados(clase, 1);
			for (int i = 0; i < nombres.length; i++) {
				mapaMetodos.get("set" + StringUtils.capitalize(nombres[i])).invoke(objeto, lista.get(i));
			}
			return objeto;
		} catch(Throwable t) {
			return null;
		}
	}

	/**
	* Retorna un objeto de tipo D tomando la información de un mapa de objetos y una clase que la describe.
	* @author amolina
	* @since 23/11/2020
	* @Param mapaNombres: Mapa desde la clave del mapa de datos al nombre del campo de la clase.
	* @Param mapa: mapa de objetos con la información de entrada.
	* @Param clase: Clase que representa al objeto de tipo D.
	* @return Instancia de D con la información del mapa de objetos.
	*/
	public static <D> D map(Map<String, String> mapaNombres, Map<String, Object> mapa, Class<D> clase) {
		try{
			D objeto = clase.newInstance();
			Map<String, Method> mapaMetodos = obtenerMapaMetodosDeclarados(clase, 1);
			mapa.forEach((clave, valor) -> {
				try{
					mapaMetodos.get("set" + StringUtils.capitalize(mapaNombres.get(clave))).invoke(objeto, valor);
				} catch(Exception e) {
				}
			});
			return objeto;
		} catch(Throwable t) {
			return null;
		}
	}

	/**
	* Retorna una lista de objetos de tipo D desde un lista de mapa de objetos.
	* @author amolina
	* @since 23/11/2020
	* @Param datos: Lista de lista de objetos con la información de entrada.
	* @Param clase: Clase que representa al objeto de tipo D.
	* @Param mapaNombres: Mapa desde la clave del mapa de datos al nombre del campo de la clase.
	* @return Lista de objetos de tipo D.
	*/
	public static <D> List<D> convertirListaMapaAListaObjeto(List<Map<String, Object>> datos, Class<D> clase, Map<String, String> mapaNombres) {
		return datos.stream().map(x -> map(mapaNombres, x, clase)).collect(Collectors.toList());
	}

	/**
	* Retorna una lista de objetos de tipo D desde un lista de mapa de objetos. Los mapas de cada registro deben ser linked. El orden de las claves del mapa debe coincidir con el orden de los campos de la clase.
	* @author amolina
	* @since 23/11/2020
	* @Param datos: Lista de lista de objetos con la información de entrada.
	* @Param clase: Clase que representa al objeto de tipo D.
	* @return Lista de objetos de tipo D.
	*/
	public static <D> List<D> convertirListaMapaAListaObjeto(List<Map<String, Object>> datos, Class<D> clase) {
		return convertirListaMapaAListaObjeto(datos, clase, 0);
	}

	/**
	* Retorna una lista de objetos de tipo D desde un lista de mapa de objetos.
	* @author amolina
	* @since 23/11/2020
	* @Param datos: Lista de lista de objetos con la información de entrada.
	* @Param clase: Clase que representa al objeto de tipo D.
	* @Param tipoMapeo: Si es 0 el mapeo será por posición. Si es 1 será un mapeo automático por nombre, ejemplo: "NUMERO_SERVICIO" -> "numeroServicio"
	* @return Lista de objetos de tipo D.
	*/
	public static <D> List<D> convertirListaMapaAListaObjeto(List<Map<String, Object>> datos, Class<D> clase,int tipoMapeo) {
		Map<String, String> mapaNombres = new HashMap<String, String>();
		if(tipoMapeo == 0) {
			ArrayList<String> nombres = new ArrayList<String>();
			nombres.addAll(datos.get(0).keySet());
			for(int i=0; i < nombres.size() && i < clase.getDeclaredFields().length; i++) {
				mapaNombres.put(nombres.get(i), clase.getDeclaredFields()[i].getName());
			}
		}
		if(tipoMapeo == 1){
			datos.get(0).forEach((clave,valor) -> mapaNombres.put(clave, formatoCampo(clave)));
		}
		return convertirListaMapaAListaObjeto(datos, clase, mapaNombres);
	}
	
	/**
	* Este método debería ir en alguna librería de utilidades para Bean.
	* Convierte el nombre de un campo de una tabla al nombre de un bean de Java.
	* @author amolina
	* @since 24/11/2020
	* @Param campo: Nombre del campo de una tabla.
	* @return Una cadena con el nombre para un bean de Java.
	*/
	public static String formatoCampo(String campo) {
		String resultado = Arrays.stream(campo.split("_")).map(x -> x.substring(0, 1).toUpperCase() + x.substring(1).toLowerCase()).collect(Collectors.joining(""));
		return resultado.substring(0, 1).toLowerCase() + resultado.substring(1);
	}
}