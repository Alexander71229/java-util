package co.com.onnovacion.exceltransform.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Interface que especifica la anotacion del nombre de una columna de un archivo
 * @author hcaro
 * @since 27/10/2020
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface NombreColumna {
	
	/**
	 * Atributo valor de la anotacion
	 * @author hcaro
	 * @since 27/10/2020
	 * @return String: Valor de la anotacion
	 */
	public String value() default "";

}
