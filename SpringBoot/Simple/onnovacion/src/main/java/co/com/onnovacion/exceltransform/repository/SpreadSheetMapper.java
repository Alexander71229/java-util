package co.com.onnovacion.exceltransform.repository;

/**
 * Estructura de conversion de excel a tipo generico
 * @author oflorez
 * @param <T>
 * @since 04/11/2020
 */
public interface SpreadSheetMapper<T> {
    T obtenerObjetoConvertido(String[] row);
    Integer obtenerTotalColumnas();
}
