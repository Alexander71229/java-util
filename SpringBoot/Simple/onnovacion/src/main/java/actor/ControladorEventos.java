package actor;
import java.util.ArrayList;
import java.util.HashMap;
public class ControladorEventos{
	public static HashMap actores=new HashMap();
	public static void registrar(String e,Object o){
		registrarc(e,o);
	}
	public static void registrar(Class c,String e,Object o){
		registrarc(c.getName()+"."+e,o);
	}
	public static void registrarc(String c,Object o){
		if(actores.get(c)==null){
			actores.put(c,new ArrayList());
		}
		((ArrayList)actores.get(c)).add(o);
	}
	public static void notificar(Class actor,String evento,Object[]valores){
		ArrayList lista=new ArrayList();
		if(actores.get(evento)!=null){
			lista.addAll((ArrayList)actores.get(evento));
		}
		String c=actor.getName()+"."+evento;
		if(actores.get(c)!=null){
			lista.addAll((ArrayList)actores.get(c));
		}
		for(int i=0;i<lista.size();i++){
			Actor a=(Actor)lista.get(i);
			a.notificar(actor,evento,valores);
		}
	}
}