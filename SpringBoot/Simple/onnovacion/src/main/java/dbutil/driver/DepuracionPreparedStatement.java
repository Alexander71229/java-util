package dbutil.driver;
import java.sql.Time;
import java.net.URL;
import java.sql.Timestamp;
import java.sql.SQLXML;
import java.io.Reader;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Array;
import java.sql.Date;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.sql.ParameterMetaData;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.ResultSetMetaData;
import java.io.InputStream;
public class DepuracionPreparedStatement extends DepuracionStatement implements PreparedStatement{
	private PreparedStatement ops;
	public String sql;
	DepuracionPreparedStatement(PreparedStatement ops){
		super((Statement)ops);
		this.ops=ops;
	}
	DepuracionPreparedStatement(PreparedStatement ops,String sql){
		super((Statement)ops);
		this.ops=ops;
		this.sql=sql;
	}
	public int executeUpdate()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeUpdate",new Object[]{this});
		return this.ops.executeUpdate();
	}
	public boolean execute()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"execute",new Object[]{this});
		return this.ops.execute();
	}
	public ResultSet executeQuery()throws SQLException{
		actor.ControladorEventos.notificar(this.getClass(),"executeQuery",new Object[]{this});
		return this.ops.executeQuery();
	}
	public DepuracionPreparedStatement(){
	}
	public void setOps(PreparedStatement ops){
		this.ops=ops;
	}
	public PreparedStatement getOps(){
		return this.ops;
	}
	public void addBatch()throws SQLException{
		this.ops.addBatch();
	}
	public void clearParameters()throws SQLException{
		this.ops.clearParameters();
	}
	public ResultSetMetaData getMetaData()throws SQLException{
		return this.ops.getMetaData();
	}
	public ParameterMetaData getParameterMetaData()throws SQLException{
		return this.ops.getParameterMetaData();
	}
	public void setArray(int p0,Array p1)throws SQLException{
		this.ops.setArray(p0,p1);
	}
	public void setAsciiStream(int p0,InputStream p1,int p2)throws SQLException{
		this.ops.setAsciiStream(p0,p1,p2);
	}
	public void setAsciiStream(int p0,InputStream p1)throws SQLException{
		this.ops.setAsciiStream(p0,p1);
	}
	public void setAsciiStream(int p0,InputStream p1,long p2)throws SQLException{
		this.ops.setAsciiStream(p0,p1,p2);
	}
	public void setBigDecimal(int p0,BigDecimal p1)throws SQLException{
		this.ops.setBigDecimal(p0,p1);
	}
	public void setBinaryStream(int p0,InputStream p1)throws SQLException{
		this.ops.setBinaryStream(p0,p1);
	}
	public void setBinaryStream(int p0,InputStream p1,int p2)throws SQLException{
		this.ops.setBinaryStream(p0,p1,p2);
	}
	public void setBinaryStream(int p0,InputStream p1,long p2)throws SQLException{
		this.ops.setBinaryStream(p0,p1,p2);
	}
	public void setBlob(int p0,Blob p1)throws SQLException{
		this.ops.setBlob(p0,p1);
	}
	public void setBlob(int p0,InputStream p1,long p2)throws SQLException{
		this.ops.setBlob(p0,p1,p2);
	}
	public void setBlob(int p0,InputStream p1)throws SQLException{
		this.ops.setBlob(p0,p1);
	}
	public void setBytes(int p0,byte[] p1)throws SQLException{
		this.ops.setBytes(p0,p1);
	}
	public void setCharacterStream(int p0,Reader p1,int p2)throws SQLException{
		this.ops.setCharacterStream(p0,p1,p2);
	}
	public void setCharacterStream(int p0,Reader p1,long p2)throws SQLException{
		this.ops.setCharacterStream(p0,p1,p2);
	}
	public void setCharacterStream(int p0,Reader p1)throws SQLException{
		this.ops.setCharacterStream(p0,p1);
	}
	public void setClob(int p0,Reader p1,long p2)throws SQLException{
		this.ops.setClob(p0,p1,p2);
	}
	public void setClob(int p0,Clob p1)throws SQLException{
		this.ops.setClob(p0,p1);
	}
	public void setClob(int p0,Reader p1)throws SQLException{
		this.ops.setClob(p0,p1);
	}
	public void setDate(int p0,Date p1,Calendar p2)throws SQLException{
		this.ops.setDate(p0,p1,p2);
	}
	public void setDate(int p0,Date p1)throws SQLException{
		this.ops.setDate(p0,p1);
	}
	public void setNCharacterStream(int p0,Reader p1)throws SQLException{
		this.ops.setNCharacterStream(p0,p1);
	}
	public void setNCharacterStream(int p0,Reader p1,long p2)throws SQLException{
		this.ops.setNCharacterStream(p0,p1,p2);
	}
	public void setNClob(int p0,NClob p1)throws SQLException{
		this.ops.setNClob(p0,p1);
	}
	public void setNClob(int p0,Reader p1)throws SQLException{
		this.ops.setNClob(p0,p1);
	}
	public void setNClob(int p0,Reader p1,long p2)throws SQLException{
		this.ops.setNClob(p0,p1,p2);
	}
	public void setNString(int p0,String p1)throws SQLException{
		this.ops.setNString(p0,p1);
	}
	public void setNull(int p0,int p1)throws SQLException{
		this.ops.setNull(p0,p1);
	}
	public void setNull(int p0,int p1,String p2)throws SQLException{
		this.ops.setNull(p0,p1,p2);
	}
	public void setObject(int p0,Object p1,int p2)throws SQLException{
		this.ops.setObject(p0,p1,p2);
	}
	public void setObject(int p0,Object p1,int p2,int p3)throws SQLException{
		this.ops.setObject(p0,p1,p2,p3);
	}
	public void setObject(int p0,Object p1)throws SQLException{
		this.ops.setObject(p0,p1);
	}
	public void setRef(int p0,Ref p1)throws SQLException{
		this.ops.setRef(p0,p1);
	}
	public void setRowId(int p0,RowId p1)throws SQLException{
		this.ops.setRowId(p0,p1);
	}
	public void setSQLXML(int p0,SQLXML p1)throws SQLException{
		this.ops.setSQLXML(p0,p1);
	}
	public void setString(int p0,String p1)throws SQLException{
		this.ops.setString(p0,p1);
	}
	public void setTime(int p0,Time p1)throws SQLException{
		this.ops.setTime(p0,p1);
	}
	public void setTime(int p0,Time p1,Calendar p2)throws SQLException{
		this.ops.setTime(p0,p1,p2);
	}
	public void setUnicodeStream(int p0,InputStream p1,int p2)throws SQLException{
		this.ops.setUnicodeStream(p0,p1,p2);
	}
	public void setBoolean(int p0,boolean p1)throws SQLException{
		this.ops.setBoolean(p0,p1);
	}
	public void setByte(int p0,byte p1)throws SQLException{
		this.ops.setByte(p0,p1);
	}
	public void setDouble(int p0,double p1)throws SQLException{
		this.ops.setDouble(p0,p1);
	}
	public void setFloat(int p0,float p1)throws SQLException{
		this.ops.setFloat(p0,p1);
	}
	public void setInt(int p0,int p1)throws SQLException{
		this.ops.setInt(p0,p1);
	}
	public void setLong(int p0,long p1)throws SQLException{
		this.ops.setLong(p0,p1);
	}
	public void setShort(int p0,short p1)throws SQLException{
		this.ops.setShort(p0,p1);
	}
	public void setTimestamp(int p0,Timestamp p1)throws SQLException{
		this.ops.setTimestamp(p0,p1);
	}
	public void setTimestamp(int p0,Timestamp p1,Calendar p2)throws SQLException{
		this.ops.setTimestamp(p0,p1,p2);
	}
	public void setURL(int p0,URL p1)throws SQLException{
		this.ops.setURL(p0,p1);
	}
}