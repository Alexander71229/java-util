package dbutil.actor;
import actor.*;
public class Cursores{
	static class AlConectar implements Actor{
		public void notificar(Class actor,String evento,Object[]valores){
			c.U.traza("Conectar");
		}
	}
	static class AlCrearStatement implements Actor{
		public void notificar(Class actor,String evento,Object[]valores){
			c.U.traza("AlCrearStatement");
		}
	}
	static class AlCrearPreparedStatement implements Actor{
		public void notificar(Class actor,String evento,Object[]valores){
			c.U.traza("AlCrearPreparedStatement");
		}
	}
	static class AlEjecutar implements Actor{
		public void notificar(Class actor,String evento,Object[]valores){
			c.U.traza(actor.getName()+":"+evento+":"+mostrar(valores));
		}
	}
	public static String mostrar(Object[]os){
		if(os==null){
			return "nulo";
		}
		if(os.length==0){
			return "vacío";
		}
		StringBuffer sb=new StringBuffer();
		sb.append(os[0]+"");
		for(int i=1;i<os.length;i++){
			sb.append("|");
			sb.append(os[i]);
		}
		return sb+"";
	}
	public static void registrarse(){
		actor.ControladorEventos.registrar("conectar",new AlConectar());
		actor.ControladorEventos.registrar("creandoStatement",new AlCrearStatement());
		actor.ControladorEventos.registrar("creaPreparedStatement",new AlCrearPreparedStatement());
		actor.ControladorEventos.registrar("execute",new AlEjecutar());
		actor.ControladorEventos.registrar("executeUpdate",new AlEjecutar());
		actor.ControladorEventos.registrar("executeQuery",new AlEjecutar());
	}
}