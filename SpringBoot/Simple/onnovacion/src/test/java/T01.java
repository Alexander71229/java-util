import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import x.dao.entidades.e01.Temp;
import x.dao.TempRepository;
import java.util.Optional;
public class T01{
	@Mock
	private TempRepository r;
	@BeforeEach
	public void configuracion(){
		try{
			c.U.imp("Se ejecuta la configuración");
			MockitoAnnotations.openMocks(this);
			Temp t=new Temp();
			t.setColumna01("ValorColumna01");
			c.U.imp("r:"+r);
			Mockito.when(r.findById("1")).thenReturn(Optional.of(t));
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
	@Test
	public void t01(){
		c.U.imp(r.findById("1").get().getColumna01());
	}
}