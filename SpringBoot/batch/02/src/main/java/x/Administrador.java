package x;
import java.util.Map;
import java.util.HashMap;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
@Service
@EnableScheduling
public class Administrador{
	private JobLauncher lanzador;
	private JobBuilderFactory creador;
	private StepBuilderFactory creadorPasos;
	private Job trabajo;
	@Autowired
	public void setLanzador(JobLauncher lanzador){
		this.lanzador=lanzador;
	}
	@Autowired
	public void setTrabajo(Job trabajo){
		this.trabajo=trabajo;
	}
	@Scheduled
	public void lanzar()throws Exception{
		JobParameters parametros=new JobParameters(new HashMap<String,JobParameter>());
		JobExecution ejecucion=lanzador.run(trabajo,parametros);
	}
	@Bean
	public Job trabajo01(@Autowired Step paso){
		return creador.get("personaArchivoJob").incrementer(new RunIdIncrementer()).flow(paso).end().build();
	}
	@Bean
	public Step paso(){
		return this.creadorPasos.get("paso").<String,String>chunk(10).reader(()->"").processor((ItemProcessor<String,String>)x->x+"").writer(x->c.U.imp(x+"")).build();
	}
	@Autowired
	public void setCreador(JobBuilderFactory creador){
		this.creador=creador;
	}
	@Autowired
	public void setCreadorPasos(StepBuilderFactory creadorPasos){
		this.creadorPasos=creadorPasos;
	}
}
