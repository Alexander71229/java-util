turno=-1;
function obtenerJuego(){
	var r=new XMLHttpRequest();
	r.open('GET','/act');
	r.onreadystatechange=function(){
		if(r.readyState==4&&r.status==200){
			var div=document.createElement('div');
			div.innerHTML=r.responseText;
			var info=div.getElementsByTagName("svg")[0].getAttribute('info');
			if(turno!=info){
				turno=info;
				document.getElementById('area').innerHTML=r.responseText;
			}
		}
	}
	r.send();
}
//setInterval(obtenerJuego,100);
pos=[];
function seleccionar(e){
	e.sel=true;
	document.getElementById('c'+e.getAttribute('fila')+e.getAttribute('columna')).setAttribute('stroke','red');
	pos.push(e);
}
function deseleccionar(e){
	document.getElementById('c'+e.getAttribute('fila')+e.getAttribute('columna')).removeAttribute('stroke');
	e.sel=false;
}
function click(e){
	if(npromueve){
		return;
	}
	if(document.getElementById('jugador').getAttribute('info')!="true"){
		return;
	}
	if(pos.length==0||pos[pos.length-1]!=e){
		seleccionar(e);
		if(pos.length==2){
			enviarMovimiento();
		}
	}else{
		deseleccionar(e);
		pos=[];
	}
}
function clickp(e){
	if(!npromueve){
		return;
	}
	pm=e.getAttribute('tipo');
	ocultar();
	enviarMovimiento2();
}
npromueve=false;
function mostrar(){
	document.getElementById('promo').style.display="";
	npromueve=true;
}
function ocultar(){
	document.getElementById('promo').style.display="none";
	npromueve=false;
}
function enviarMovimiento(){
	console.log(pos[0]);
	console.log(pos[1]);
	if(pos[0].getAttribute("tipo")=='6'){
		if((pos[0].getAttribute("color")=='0'&&pos[1].getAttribute("columna")=='0')||(pos[0].getAttribute("color")=='1'&&pos[1].getAttribute("columna")=='7')){
			console.log("promueve");
			mostrar();
			return;
		}
	}
	enviarMovimiento2();
}
pm='0';
function enviarMovimiento2(){
	var r=new XMLHttpRequest();
	console.log('pm:'+pm);
	r.open('GET','/mover?o='+pos[0].getAttribute('fila')+'&o='+pos[0].getAttribute('columna')+'&d='+pos[1].getAttribute('fila')+'&d='+pos[1].getAttribute('columna')+'&t='+pm);
	deseleccionar(pos[1]);
	deseleccionar(pos[0]);
	pos=[];
	r.onreadystatechange=function(){
		if(r.readyState==4&&r.status==200){
			var div=document.createElement('div');
			div.innerHTML=r.responseText;
			var info=div.getElementsByTagName("svg")[0].getAttribute('info');
			if(turno!=info){
				turno=info;
				document.getElementById('area').innerHTML=r.responseText;
			}
		}
	}
	r.send();
}