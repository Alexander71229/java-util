package t;
import x.clases.*;
import java.util.*;
import java.io.*;
public class Test01{
	public static String[]gm(Motor m,String ini,String movs,int n,int t1,int t2)throws Exception{
		String a="";
		String b=m.cm("position "+ini+" moves"+movs+"\nd","Fen");
		HashMap<String,Integer>h=new HashMap<String,Integer>();
		StringBuilder sb=new StringBuilder();
		int k=0;
		String t="-";
		String[]r=new String[2];
		while(!a.equals(b)){
			if(b.indexOf(" ")>=0){
				String fen=b.split(" ")[1];
				if(h.get(fen)!=null){
					h.put(fen,h.get(fen)+1);
					if(h.get(fen)>=5){
						t="C";
						break;
					}
				}else{
					h.put(fen,1);
				}
				String fenl=fen.toLowerCase();
				if(fenl.indexOf("r")<0&&fenl.indexOf("n")<0&&fenl.indexOf("b")<0&&fenl.indexOf("q")<0&&fenl.indexOf("p")<0){
					t="P";
					break;
				}
			}
			if(k>=n){
				break;
			}
			a=b;
			int tiempo=t1;
			if(k%2==1){
				tiempo=t2;
			}
			k++;
			//c.U.imp("t:"+tiempo);
			String mov=m.cm("go movetime "+tiempo,"bestmove").split(" ")[1];
			if(mov.equals("(none)")){
				t="M";
				break;
			}
			sb.append(" "+mov);
			b=m.cm("position "+ini+" moves"+movs+sb+"\nd","Fen");
		}
		r[0]=t;
		r[1]=sb+"";
		return r;
	}
	public static void main(String[]argumentos){
		try{
			c.U.ruta="Log01.txt";
			c.U.append=true;
			HashMap<String,Integer>h=new HashMap<String,Integer>();
			try{
				Scanner s=new Scanner(new File("Log01.txt"));
				while(s.hasNextLine()){
					h.put(s.nextLine(),1);
				}
			}catch(Exception e){
			}
			Motor m=new Motor();
			m.cm("uci","uciok");
			String ini="startpos";
			String movs=" e2e4";
			while(true){
				String[]r=gm(m,ini,movs,1000,1200000,1200000);
				if(true||r[0].equals("M")){
					if(h.get(r[1])==null){
						c.U.imp(r[1]);
						h.put(r[1],1);
					}
				}
			}
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}