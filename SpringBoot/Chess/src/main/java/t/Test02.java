package t;
import x.clases.*;
import java.util.*;
import java.io.*;
public class Test02{
	public static String gm(Motor m,String ini,String movs,int n,int t1,int t2)throws Exception{
		String a="";
		String b=m.cm("position "+ini+" moves"+movs+"\nd","Fen");
		HashMap<String,Integer>h=new HashMap<String,Integer>();
		StringBuilder sb=new StringBuilder();
		int k=0;
		while(!a.equals(b)){
			if(b.indexOf(" ")>=0){
				String fen=b.split(" ")[1];
				if(h.get(fen)!=null){
					h.put(fen,h.get(fen)+1);
					if(h.get(fen)>=5){
						//c.U.imp("Ciclo");
						break;
					}
				}else{
					h.put(fen,1);
				}
				String fenl=fen.toLowerCase();
				if(fenl.indexOf("r")<0&&fenl.indexOf("n")<0&&fenl.indexOf("b")<0&&fenl.indexOf("q")<0&&fenl.indexOf("p")<0){
					//c.U.imp("Pocas fichas");
					break;
				}
			}
			if(k>=n){
				break;
			}
			a=b;
			int tiempo=t1;
			if(k%2==1){
				tiempo=t2;
			}
			k++;
			//c.U.imp("t:"+tiempo);
			String mov=m.cm("go movetime "+tiempo,"bestmove").split(" ")[1];
			if(mov.equals("(none)")){
				//c.U.imp("Mate");
				break;
			}
			sb.append(" "+mov);
			b=m.cm("position "+ini+" moves"+movs+sb+"\nd","Fen");
		}
		return sb+"";
	}
	public static void main(String[]argumentos){
		try{
			HashMap<String,Integer>h=new HashMap<String,Integer>();
			try{
				Scanner s=new Scanner(new File("Log02.txt"));
				while(s.hasNextLine()){
					h.put(s.nextLine(),1);
				}
			}catch(Exception e){
			}
			c.U.append=true;
			Motor m=new Motor();
			m.cm("uci","uciok");
			String ini="startpos";
			//String movs=" e2e4";
			String movs="";
			int n=10;
			int t1=1200000;
			int t2=1200000;
			String id=t1+"v"+t2+" L"+n+" "+ini+movs;
			c.U.ruta=id+".txt";
			if(h.get(id)==null){
				h.put(id,1);
				c.U.imp(id);
			}
			while(true){
				String x=gm(m,ini,movs,n,t1,t2).trim();
				if(h.get(x)==null){
					h.put(x,1);
					c.U.imp(x);
				}
			}
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}