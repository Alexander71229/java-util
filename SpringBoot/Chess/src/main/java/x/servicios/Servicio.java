package x.servicios;
import x.clases.*;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
@Service
public class Servicio{
	@Autowired
	private Sesion sesion;
	public void registrar(String nombre){
		sesion.put("nombre",nombre);
		sesion.put("jugador",new Jugador(nombre));
	}
	public void crearPartida(){
		Jugador jugador=(Jugador)sesion.get("jugador");
		Partida partida=new Partida(jugador.getNombre());
		partida.add(jugador);
		partida.setJuego(new Juego(partida));
		sesion.put("partida",partida);
		sesion.put("juego",partida.getJuego());
	}
	public Partida actualizar(){
		Partida partida=(Partida)sesion.get("partida");
		return partida;
	}
	public Juego obtenerJuego(){
		return(Juego)sesion.get("juego");
	}
	public void unirse(String nombrePartida)throws Exception{
		Movimientos.m.cm("uci","uciok");
		Movimientos.m.cm("position startpos\nd","Fen");
		Partida partida=Partida.get(nombrePartida);
		partida.add((Jugador)sesion.get("jugador"));
		sesion.put("partida",partida);
		sesion.put("juego",partida.getJuego());
	}
	public boolean enTurno(){
		Juego juego=(Juego)sesion.get("juego");
		if(juego!=null&&juego.jugadorActual()==sesion.get("jugador")){
			return true;
		}
		return false;
	}
	public Jugador getJugador(){
		return(Jugador)sesion.get("jugador");
	}
	public void mover(int[]o,int[]d,int t)throws Exception{
		if(enTurno()){
			Juego juego=(Juego)sesion.get("juego");
			juego.mover(o,d,t);
		}
	}
	public void crearPartidaComputadora()throws Exception{
		Movimientos.m.cm("uci","uciok");
		Movimientos.m.cm("position startpos\nd","Fen");
		Jugador jugador=(Jugador)sesion.get("jugador");
		Partida partida=new Partida(jugador.getNombre());
		partida.add(jugador);
		partida.add(new Jugador("Stockfish12"));
		partida.getJugadores().get(1).setTipo(1);
		partida.setJuego(new Juego(partida));
		sesion.put("partida",partida);
		sesion.put("juego",partida.getJuego());
	}
	public void crearPartidaComputadora2()throws Exception{
		Movimientos.m.cm("uci","uciok");
		Movimientos.m.cm("position startpos\nd","Fen");
		Jugador jugador=(Jugador)sesion.get("jugador");
		Partida partida=new Partida(jugador.getNombre());
		partida.add(new Jugador("Stockfish12"));
		partida.add(jugador);
		partida.getJugadores().get(0).setTipo(1);
		partida.setJuego(new Juego(partida));
		sesion.put("partida",partida);
		sesion.put("juego",partida.getJuego());
	}
	public void crearDemo()throws Exception{
		Movimientos.m.cm("uci","uciok");
		Movimientos.m.cm("position startpos\nd","Fen");
		//Movimientos.m.cm("setoption name UCI_LimitStrength value false\nd","Fen");
		Jugador jugador=(Jugador)sesion.get("jugador");
		Partida partida=new Partida(jugador.getNombre());
		partida.add(new Jugador("Stockfish12"));
		partida.add(new Jugador("Stockfish12"));
		partida.getJugadores().get(0).setTipo(1);
		partida.getJugadores().get(1).setTipo(1);
		partida.setJuego(new Juego(partida));
		sesion.put("partida",partida);
		sesion.put("juego",partida.getJuego());
	}
	public void crearAnalisis(String movimientos)throws Exception{
		Movimientos.m.cm("uci","uciok");
		Movimientos.m.cm("position startpos\nd","Fen");
		Partida partida=new Partida("Test");
		Jugador jugador=new Jugador("Test");
		partida.add(jugador);
		partida.add(jugador);
		partida.setJuego(new Juego(partida));
		partida.getJuego().agregarMovimientos(movimientos);
		sesion.put("partida",partida);
		sesion.put("juego",partida.getJuego());
		sesion.put("jugador",partida.getJugadores().get(0));
	}
	public void mover(int tiempo)throws Exception{
		try{
			if(this.obtenerJuego()!=null&&this.obtenerJuego().jugadorActual().getTipo()==1&&!this.obtenerJuego().jugadorActual().getMoviendo()){
				Jugador computadora=this.obtenerJuego().jugadorActual();
				computadora.setMoviendo(true);
				String mov=Movimientos.m.cm("go movetime "+tiempo,"bestmove").split(" ")[1];
				if(!mov.equals("(none)")){
					String mo=mov.substring(0,2);
					String md=mov.substring(2,4);
					String mt=mov.substring(4);
					this.obtenerJuego().mover(Movimientos.c(mo),Movimientos.c(md),Movimientos.ct(mt));
				}
				computadora.setMoviendo(false);
			}
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}