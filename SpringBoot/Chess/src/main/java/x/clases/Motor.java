package x.clases;
import java.io.*;
public class Motor{
	public BufferedReader ip;
	public BufferedWriter op;
	public Process p;
	public Motor()throws Exception{
		init();
	}
	public void init()throws Exception{
		p=Runtime.getRuntime().exec("stockfish_20090216_x64_bmi2");
		ip=new BufferedReader(new InputStreamReader(p.getInputStream()));
		op=new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
	}
	public void cm(String c)throws Exception{
		op.write(c+"\n");
		op.flush();
	}
	public String rs(String e)throws Exception{
		String l=null;
		while((l=ip.readLine())!=null){
			if(l.indexOf(e)>=0){
				return l;
			}
		}
		return l;
	}
	public String rs(String e1,String e2)throws Exception{
		String l=null;
		while((l=ip.readLine())!=null){
			if(l.indexOf(e1)>=0||l.indexOf(e2)>=0){
				return l;
			}
		}
		return l;
	}
	public String cm(String c,String e)throws Exception{
		cm(c);
		return rs(e);
	}
	public String cm(String c,String e1,String e2)throws Exception{
		cm(c);
		return rs(e1,e2);
	}
}