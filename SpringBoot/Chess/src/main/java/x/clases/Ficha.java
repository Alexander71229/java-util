package x.clases;
import java.util.*;
public class Ficha{
	private int tipo;
	private int color;
	private int[]pos;
	public Ficha(int tipo,int color){
		this.pos=new int[2];
		this.tipo=tipo;
		this.color=color;
		this.pos=pos;
	}
	public void setTipo(int tipo){
		this.tipo=tipo;
	}
	public int getTipo(){
		return this.tipo;
	}
	public void setColor(int color){
		this.color=color;
	}
	public int getColor(){
		return this.color;
	}
	public void setPos(int[]pos){
		this.pos=pos;
	}
	public int[]getPos(){
		return this.pos;
	}
	public int getFila(){
		return this.pos[0];
	}
	public int getColumna(){
		return this.pos[1];
	}
	public void setFila(int v){
		this.pos[0]=v;
	}
	public void setColumna(int v){
		this.pos[1]=v;
	}
}
