package x.clases;
public class Jugador{
	private String nombre;
	private Partida partida;
	private String info;
	private boolean enTurno;
	private int tipo;
	private boolean moviendo;
	public Jugador(){
	}
	public Jugador(String nombre){
		this.nombre=nombre;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
	public String toString(){
		return "Nombre:"+nombre;
	}
	public void setPartida(Partida partida){
		this.partida=partida;
	}
	public void setInfo(String info){
		this.info=info;
	}
	public String getInfo(){
		return this.info;
	}
	public void setEnTurno(boolean enTurno){
		this.enTurno=enTurno;
	}
	public boolean getEnTurno(){
		return this.enTurno;
	}
	public void setTipo(int tipo){
		this.tipo=tipo;
	}
	public int getTipo(){
		return this.tipo;
	}
	public boolean getMoviendo(){
		return this.moviendo;
	}
	public void setMoviendo(boolean moviendo){
		this.moviendo=moviendo;
	}
}