package x.clases;
import java.util.*;
import java.util.stream.*;
public class Partida{
	private static Map<String,Partida>partidas=new HashMap<>();
	private List<Jugador>jugadores;
	private String nombre;
	private String estado;
	private Juego juego;
	private String info;
	public Partida(String nombre){
		jugadores=new ArrayList<>();
		this.nombre=nombre;
		Partida.add(this);
	}
	public void setJugadores(List<Jugador>jugadores){
		this.jugadores=jugadores;
	}
	public List<Jugador>getJugadores(){
		return this.jugadores;
	}
	public void add(Jugador jugador){
		this.jugadores.add(jugador);
		jugador.setPartida(this);
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
	public String toString(){
		return ""+jugadores;
	}
	public static void add(Partida partida){
		partidas.put(partida.nombre,partida);
	}
	public static Collection<Partida>listaPartidas(){
		return partidas.values().stream().filter(x->x.getJugadores().size()==1).collect(Collectors.toList());
	}
	public static Partida get(String nombre){
		return partidas.get(nombre);
	}
	public void setJuego(Juego juego){
		this.juego=juego;
	}
	public Juego getJuego(){
		return this.juego;
	}
	public void setInfo(String info){
		this.info=info;
	}
	public String getInfo(){
		return this.info;
	}
	public boolean esComputadora(){
		for(int i=0;i<jugadores.size();i++){
			if(jugadores.get(i).getTipo()==1){
				return true;
			}
		}
		return false;
	}
}