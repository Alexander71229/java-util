package x.clases;
import java.util.*;
public class Movimientos{
	public static Motor m;
	public static int[]c(String p)throws Exception{
		HashMap<String,Integer>h=new HashMap<String,Integer>();
		h.put("a",0);
		h.put("b",1);
		h.put("c",2);
		h.put("d",3);
		h.put("e",4);
		h.put("f",5);
		h.put("g",6);
		h.put("h",7);
		return new int[]{h.get(p.substring(0,1)),8-Integer.parseInt(p.substring(1))};
	}
	public static int ct(String t){
		HashMap<String,Integer>h=new HashMap<String,Integer>();
		h.put("",0);
		h.put("q",1);
		h.put("b",2);
		h.put("r",3);
		h.put("n",4);
		return h.get(t);
	}
	public static String mov(int[]o,int[]d){
		char[]e=new char[]{'a','b','c','d','e','f','g','h'};
		return e[o[0]]+""+(8-o[1])+""+e[d[0]]+""+(8-d[1]);
	}
	public static boolean validador2(Juego juego,Ficha f,int[]d){
		if(f.getTipo()==1){
			for(int i=0;i<f.getPos().length;i++){
				if(Math.abs(f.getPos()[i]-d[i])>1){
					return false;
				}
			}
		}
		return true;
	}
	public static boolean validador(Juego j,Ficha f,int[]d,int t)throws Exception{
		String[]pm=new String[]{"","","q","b","r","n","p"};
		String mv=mov(f.getPos(),d)+pm[t];
		String f1=m.cm("position startpos moves"+j.obtenerMovimientos()+"\nd","Fen");
		String f2=m.cm("position startpos moves"+j.obtenerMovimientos()+" "+mv+"\nd","Fen");
		if(f1.equals(f2)){
			c.U.imp("Movimiento rechazado:");
			c.U.imp("position startpos moves"+j.obtenerMovimientos()+" "+mv);
			return false;
		}
		j.agregarMovimiento(mv);
		return true;
	}
	public static void setTablero(Juego z,String fen){
		HashMap<Character,Integer>h=new HashMap<Character,Integer>();
		h.put('k',1);
		h.put('q',2);
		h.put('b',3);
		h.put('r',4);
		h.put('n',5);
		h.put('p',6);
		char[]d=fen.toCharArray();
		int cx=0;
		int fx=0;
		for(int i=0;i<8;i++){
			for(int j=0;j<8;j++){
				z.getTablero()[i][j]=null;
			}
		}
		for(int i=0;i<d.length;i++){
			if(d[i]=='/'){
				fx++;
				cx=0;
				continue;
			}
			if(d[i]>='0'&&d[i]<='8'){
				cx+=(d[i]-48);
				continue;
			}
			if(d[i]==' '){
				break;
			}
			if(d[i]>=97){
				z.getTablero()[cx][fx]=new Ficha(h.get(d[i]),1);
			}else{
				z.getTablero()[cx][fx]=new Ficha(h.get((char)(d[i]+32)),0);
			}
			cx++;
		}
	}
	public static Juego desdeMotor()throws Exception{
		Juego j=new Juego(new Partida("Test"));
		setTablero(j,m.cm("d","Fen").substring(5));
		return j;
	}
}