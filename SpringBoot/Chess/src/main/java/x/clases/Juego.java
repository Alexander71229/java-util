package x.clases;
import java.util.*;
import java.util.stream.*;
public class Juego{
	public Partida partida;
	private int turno=-1;
	private Ficha[][]tablero;
	private ArrayList<Ficha>[]cementerio;
	private int cursor=-1;
	private ArrayList<String>movimientos;
	public Juego(Partida partida){
		this.partida=partida;
		this.tablero=new Ficha[8][8];
		this.posicionInicial();
		cementerio=new ArrayList[2];
		cementerio[0]=new ArrayList<Ficha>();
		cementerio[1]=new ArrayList<Ficha>();
		movimientos=new ArrayList<>();
	}
	public void posicionInicial(){
		this.tablero[0][0]=new Ficha(4,1);
		this.tablero[7][0]=new Ficha(4,1);
		this.tablero[1][0]=new Ficha(5,1);
		this.tablero[6][0]=new Ficha(5,1);
		this.tablero[2][0]=new Ficha(3,1);
		this.tablero[5][0]=new Ficha(3,1);
		this.tablero[3][0]=new Ficha(2,1);
		this.tablero[4][0]=new Ficha(1,1);
		for(int i=0;i<8;i++){
			this.tablero[i][1]=new Ficha(6,1);
			this.tablero[i][6]=new Ficha(6,0);
		}
		this.tablero[0][7]=new Ficha(4,0);
		this.tablero[7][7]=new Ficha(4,0);
		this.tablero[1][7]=new Ficha(5,0);
		this.tablero[6][7]=new Ficha(5,0);
		this.tablero[2][7]=new Ficha(3,0);
		this.tablero[5][7]=new Ficha(3,0);
		this.tablero[3][7]=new Ficha(2,0);
		this.tablero[4][7]=new Ficha(1,0);
	}
	public int indicador(){
		return turno%2;
	}
	public Jugador jugadorActual(){
		if(partida.getJugadores().size()==2){
			if(turno==-1){
				turno=0;
			}
			return partida.getJugadores().get(indicador());
		}
		return null;
	}
	public Ficha[][]getTablero(){
		return this.tablero;
	}
	public int getTurno(){
		return this.turno;
	}
	public void setTurno(int turno){
		this.turno=turno;
	}
	public void mover2(int[]o,int[]d,int t)throws Exception{
		Ficha a=tablero[o[0]][o[1]];
		if(a==null){
			return;
		}
		c.U.imp(a);
		if(a.getColor()!=indicador()){
			return;
		}
		Ficha b=tablero[d[0]][d[1]];
		if(b!=null){
			if(b.getColor()==indicador()){
				return;
			}
			b.setPos(null);
			cementerio[b.getColor()].add(b);
		}
		if(!Movimientos.validador(this,a,d,t)){
			return;
		}
		c.U.traza();
		mover(a,d);
		turno++;
		c.U.imp(this+"");
	}
	public void mover(int[]o,int[]d,int t)throws Exception{
		Ficha a=tablero[o[0]][o[1]];
		if(a==null){
			return;
		}
		if(!Movimientos.validador(this,a,d,t)){
			return;
		}
		Ficha b=tablero[d[0]][d[1]];
		if(b!=null){
			b.setPos(null);
			cementerio[b.getColor()].add(b);
		}
		if(t==0){
			mover(a,d);
			if(a.getTipo()==1){
				if(Math.abs(o[0]-d[0])==2){
					int[]x=new int[]{(o[0]+d[0])/2,o[1]};
					if(d[0]<3){
						mover(tablero[0][o[1]],x);
					}else{
						mover(tablero[7][o[1]],x);
					}
				}
			}
		}else{
			tablero[a.getPos()[0]][a.getPos()[1]]=null;
			tablero[d[0]][d[1]]=new Ficha(t,a.getColor());
			tablero[d[0]][d[1]].getPos()[0]=d[0];
			tablero[d[0]][d[1]].getPos()[1]=d[1];
		}
		turno++;
	}
	private void mover(Ficha f,int[]p){
		tablero[f.getPos()[0]][f.getPos()[1]]=null;
		f.getPos()[0]=p[0];
		f.getPos()[1]=p[1];
		tablero[p[0]][p[1]]=f;
	}
	public String toString(){
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<tablero.length;i++){
			for(int j=0;j<tablero[i].length;j++){
				if(tablero[i][j]==null){
					sb.append("  ");
				}else{
					sb.append(tablero[i][j].getTipo()+""+tablero[i][j].getColor());
				}
			}
			sb.append("\n");
		}
		return sb+"";
	}
	public void agregarMovimiento(String m){
		while(movimientos.size()>cursor+1){
			movimientos.remove(movimientos.size()-1);
		}
		movimientos.add(m);
		cursor++;
	}
	public void agregarMovimientos(String m)throws Exception{
		final Juego juego=this;
		Arrays.stream(m.split(" ")).forEach(x->{
			juego.agregarMovimiento(x);
			juego.turno++;
		});
		actualizar();
		c.U.imp(this.movimientos+"");
	}
	public String obtenerMovimientos(){
		StringBuilder s=new StringBuilder();
		for(int i=0;i<=cursor;i++){
			s.append(" ");
			s.append(movimientos.get(i));
		}
		return s+"";
	}
	public void retroceder()throws Exception{
		retroceder(2);
	}
	public void retroceder(int d)throws Exception{
		if(cursor>=d-1){
			cursor=cursor-d;
			turno=cursor+1;
			actualizar();
		}
	}
	public void avanzar()throws Exception{
		avanzar(2);
	}
	public void avanzar(int d)throws Exception{
		if(cursor+d<movimientos.size()){
			cursor=cursor+d;
			turno=cursor+1;
			actualizar();
		}
	}
	public void inicio()throws Exception{
		cursor=-1;
		turno=cursor+1;
		actualizar();
	}
	public void actualizar()throws Exception{
		Movimientos.setTablero(this,Movimientos.m.cm("position startpos moves"+this.obtenerMovimientos()+"\nd","Fen").substring(5));
	}
}
