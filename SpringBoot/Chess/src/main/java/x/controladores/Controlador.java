package x.controladores;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import x.clases.*;
import x.vista.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import x.servicios.Servicio;
import java.util.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.apache.commons.io.IOUtils;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletRequest;
@Controller
//@RequestMapping("/partida")
public class Controlador{
	@Autowired
	private Servicio servicio;
	@GetMapping("/test")
	public String test(Model model){
		return "test";
	}
	@GetMapping("/index")
	public String index(){
		return "ingresar";
	}
	@GetMapping("/ingreso")
	public String ingreso(@RequestParam String nombre){
		servicio.registrar(nombre);
		return "menu";
	}
	@GetMapping("/crearPartida")
	public String crearPartida(Model model)throws Exception{
		servicio.crearPartida();
		//return "actualizar";
		return actualizar(model);
	}
	@GetMapping("/partidas")
	public String partidas(Model model){
		model.addAttribute("partidas",Partida.listaPartidas());
		return "partidas";
	}
	@GetMapping("/unirse")
	public String unirse(Model model,@RequestParam String nombre)throws Exception{
		servicio.unirse(nombre);
		//return "actualizar";
		return actualizar(model);
	}
	@GetMapping("/act")
	public String act(Model model)throws Exception{
		servicio.mover(400);
		return actualizar(model);
	}
	private String actualizar(Model model)throws Exception{
		Partida partida=servicio.actualizar();
		Jugador jugador=servicio.getJugador();
		Juego juego=servicio.obtenerJuego();
		if(juego==null){
			juego=new Juego(partida);
		}
		model.addAttribute("juego",VistaJuego.v(juego,jugador));
		model.addAttribute("partida",partida);
		model.addAttribute("jugador",jugador);
		if(jugador!=null){
			if(servicio.enTurno()){
				jugador.setInfo("En turno");
				jugador.setEnTurno(true);
			}else{
				jugador.setInfo("");
				jugador.setEnTurno(false);
			}
		}
		if(model.getAttribute("juego")==null||model.getAttribute("partida")==null||model.getAttribute("jugador")==null){
			return "vacio";
		}
		return "act";
	}
	@GetMapping("/juego")
	public String juego(Model model)throws Exception{
		if(servicio.obtenerJuego()==null){
			Juego juego=new Juego(new Partida(""));
			juego.setTurno(-2);
			model.addAttribute("juego",VistaJuego.v(juego,servicio.getJugador()));
		}else{
			model.addAttribute("juego",VistaJuego.v(servicio.obtenerJuego(),servicio.getJugador()));
		}
		return "juego";
	}
	@GetMapping("/svg")
	public String svg(Model model,@RequestParam int tipo,@RequestParam int color){
		c.U.imp("Entrada:"+tipo+":"+color);
		return "svg";
	}
	@GetMapping(value="/svgs",produces="image/svg+xml")
	public ResponseEntity<String>svgs(@RequestParam String nombre)throws Exception{
		c.U.imp("JAR:"+c.U.jar(this.getClass()));
		return ResponseEntity.ok(IOUtils.toString(new ClassPathResource("svgs/"+nombre+".svg").getInputStream(),StandardCharsets.UTF_8));
	}
	@ExceptionHandler
	public ResponseEntity<String>controlExcepciones(Exception excepcion,HttpServletRequest request){
		c.U.imp(excepcion);
		return ResponseEntity.status(500).body(excepcion.getMessage());
	}
	@GetMapping("/info")
	public String info(Model model){
		Jugador jugador=servicio.getJugador();
		model.addAttribute("jugador",jugador);
		return "info";
	}
	@GetMapping("/mover")
	public String mover(Model model,@RequestParam int[]o,@RequestParam int[]d,@RequestParam int t)throws Exception{
		servicio.mover(o,d,t);
		return actualizar(model);
	}
	@GetMapping("/computadora")
	public String computadora(Model model)throws Exception{
		servicio.crearPartidaComputadora();
		return actualizar(model);
	}
	@GetMapping("/computadora2")
	public String computadora2(Model model)throws Exception{
		servicio.crearPartidaComputadora2();
		return actualizar(model);
	}
	@GetMapping("/test01")
	public String info(Model model,@RequestParam String fen)throws Exception{
		Juego j=new Juego(new Partida("Test"));
		Movimientos.setTablero(j,fen);
		model.addAttribute("juego",VistaJuego.v(j,new Jugador()));
		return "juego";
	}
	@GetMapping("/inicio")
	public String inicio(Model model)throws Exception{
		if(servicio.actualizar().esComputadora()||servicio.actualizar().getJugadores().get(0)==servicio.actualizar().getJugadores().get(1)){
			servicio.obtenerJuego().inicio();
		}
		return actualizar(model);
	}
	@GetMapping("/retroceder")
	public String retroceder(Model model)throws Exception{
		if(servicio.actualizar().esComputadora()){
			servicio.obtenerJuego().retroceder();
		}else{
			if(servicio.actualizar().getJugadores().get(0)==servicio.actualizar().getJugadores().get(1)){
				servicio.obtenerJuego().retroceder(1);
			}
		}
		return actualizar(model);
	}
	@GetMapping("/avanzar")
	public String avanzar(Model model)throws Exception{
		if(servicio.actualizar().esComputadora()){
			servicio.obtenerJuego().avanzar();
		}else{
			if(servicio.actualizar().getJugadores().get(0)==servicio.actualizar().getJugadores().get(1)){
				servicio.obtenerJuego().avanzar(1);
			}
		}
		return actualizar(model);
	}
	@GetMapping("/motor")
	public String motor(Model model)throws Exception{
		VistaJuego j=VistaJuego.v(Movimientos.desdeMotor(),new Jugador());
		j.setInfo(Movimientos.m.cm("d","Fen").substring(5));
		model.addAttribute("juego",j);
		return "juego";
	}
	@GetMapping("/demo")
	public String demo(Model model)throws Exception{
		servicio.crearDemo();
		return actualizar(model);
	}
	@GetMapping("/movimientos")
	public String movimientos(Model model,@RequestParam String movimientos)throws Exception{
		servicio.crearAnalisis(movimientos);
		return actualizar(model);
	}
}