package x.vista;
import x.clases.*;
import java.util.*;
public class VistaJuego{
	private Juego j;
	private Jugador x;
	private String info;
	public VistaJuego(Juego j,Jugador x){
		this.j=j;
		this.x=x;
	}
	public static VistaJuego v(Juego j,Jugador x){
		return new VistaJuego(j,x);
	}
	public List<Ficha>getFichas(){
		List<Ficha>fichas=new ArrayList<>();
		for(int i=0;i<this.j.getTablero().length;i++){
			for(int j=0;j<this.j.getTablero()[i].length;j++){
				if(this.j.getTablero()[i][j]!=null){
					this.j.getTablero()[i][j].getPos()[0]=i;
					this.j.getTablero()[i][j].getPos()[1]=j;
					fichas.add(this.j.getTablero()[i][j]);
				}
			}
		}
		return fichas;
	}
	public static String color(int i){
		return (new String[]{"white","black","#d18b47","#ffce9e"})[i];
	}
	public static ArrayList<Casilla>casillas(){
		ArrayList<Casilla>r=new ArrayList<>();
		for(int i=0;i<8;i++){
			for(int j=0;j<8;j++){
				r.add(new Casilla(j,i,(i+j+1)%2+2));
			}
		}
		return r;
	}
	public int getTurno(){
		return j.getTurno();
	}
	public int c(int v){
		if(j.partida.getJugadores().size()==2&&x!=j.partida.getJugadores().get(0)){
			return 7-v;
		}
		return v;
	}
	public int f(int v){
		if(j.partida.getJugadores().size()==2&&x!=j.partida.getJugadores().get(0)){
			return 7-v;
		}
		return v;
	}
	public String getInfo(){
		return this.info;
	}
	public void setInfo(String info){
		this.info=info;
	}
	public String getMovimientos(){
		return this.j.obtenerMovimientos();
	}
}