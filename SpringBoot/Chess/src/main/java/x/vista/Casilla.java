package x.vista;
import x.clases.*;
import java.util.*;
public class Casilla{
	private int fila;
	private int columna;
	private int color;
	public Casilla(){
	}
	public Casilla(int fila,int columna,int color){
		this.fila=fila;
		this.columna=columna;
		this.color=color;
	}
	public void setFila(int fila){
		this.fila=fila;
	}
	public int getFila(){
		return this.fila;
	}
	public void setColumna(int columna){
		this.columna=columna;
	}
	public int getColumna(){
		return this.columna;
	}
	public void setColor(int color){
		this.color=color;
	}
	public int getColor(){
		return this.color;
	}
}