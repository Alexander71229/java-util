package x.beans;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.WebApplicationContext;
import x.clases.Sesion;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ScopedProxyMode;
@Configuration
public class Configuracion{
	@Bean
	@Scope(value=WebApplicationContext.SCOPE_SESSION,proxyMode=ScopedProxyMode.TARGET_CLASS)
	public Sesion sesion(){
		return new Sesion();
	}
}