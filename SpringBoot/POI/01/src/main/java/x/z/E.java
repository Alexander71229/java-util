package x.z;
import java.util.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
public class E{
	public Workbook crearLibro(List<ArrayList<Object>>datos){
		Workbook libro=new HSSFWorkbook();
		Sheet hoja=libro.createSheet();
		hoja.createRow(0);
		datos.stream().forEach(datoFila->{
			Row fila=hoja.getRow(hoja.getLastRowNum());
			hoja.createRow(hoja.getLastRowNum()+1);
			datoFila.forEach(dato->insertarValorCelda(fila.createCell(fila.getLastCellNum()+1),dato));
		});
		return libro;
	}
	private static void insertarValorCelda(Cell celda,Object valor){
		if(valor!=null){
			if(valor instanceof String){
				celda.setCellValue((String)valor);
			}else if(valor instanceof Long){
				celda.setCellValue((Long)valor);
			}else if(valor instanceof Integer){
				celda.setCellValue((Integer)valor);
			}else if(valor instanceof Double){
				celda.setCellValue((Double)valor);
			}else if(valor instanceof Date){
				celda.setCellValue((Date)valor);
			}else if(valor instanceof Calendar){
				celda.setCellValue((Calendar)valor);
			}else if(valor instanceof Boolean){
				celda.setCellValue((Boolean)valor);
			}
		}
	}
	public<T>byte[]escribir(List<T> datos){
		return null;
	}
}