package c;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.JoinPoint;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;
@Aspect
@Component
public class A01{
	//@Around("execution(public * co.com.cotrafa.cajas.repository.custom.*.*(..))||execution(public * co.com.cotrafa.cajas.repository.*.*(..))")
	public void m01(ProceedingJoinPoint p){
		try{
			c.U.traza();
			//Object o=p.proceed();
			//c.U.traza();
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
	@Before("execution(public * co.com.cotrafa.cajas.repository.custom.*.*(..))||execution(public * co.com.cotrafa.cajas.repository.*.*(..))")
	public void m02(JoinPoint jp){
		c.U.traza(""+jp.getSignature());
		//Arrays.stream(jp.getArgs()).forEach(x->c.U.imp(x+""));
		c.U.imp(Arrays.stream(jp.getArgs()).map(x->x+"").collect(Collectors.joining("|")));
	}
}
