package com.prueba.x;
import co.com.cotrafa.auditoria.business.AuditoriaBusiness;
import co.com.cotrafa.generales.dto.PeticionStoreDTO;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import co.com.cotrafa.generales.dto.JWTContext;
@Aspect
@Component
public class Auditoria{
	@Autowired
	private AuditoriaBusiness auditoriaBusiness;
	@Autowired
	private PeticionStoreDTO peticionStore;
	@Before("execution(* com.prueba.x..*(..))")
	public void auditar(){
		c.U.imp("peticionStore:"+peticionStore);
		c.U.imp("auditoriaBusiness:"+auditoriaBusiness);
		c.U.imp("JWTContext.getToken():"+JWTContext.getToken());
		c.U.imp("peticionStore.getJwt():"+peticionStore.getJwt());
		c.U.imp("peticionStore.getJwt().getToken():"+peticionStore.getJwt().getToken());
		c.U.imp(peticionStore);
		c.U.imp(peticionStore.getAuditoria());
		this.auditoriaBusiness.auditar(peticionStore.getAuditoria());
	}
}
