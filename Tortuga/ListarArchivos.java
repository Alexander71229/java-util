import java.util.*;
import java.io.*;
public class ListarArchivos{
	public static ArrayList<File>listar(File o,HashMap<String,Integer>h)throws Exception{
		if(!o.isDirectory()){
			return null;
		}
		ArrayList<File>r=new ArrayList<File>();
		File[]ls=o.listFiles();
		for(int i=0;i<ls.length;i++){
			if(ls[i].isDirectory()){
				r.addAll(ListarArchivos.listar(ls[i],h));
			}else{
				String x=gx(ls[i]);
				if(h.get(x)==null){
					r.add(ls[i]);
				}
			}
		}
		return r;
	}
	public static String gx(File f){
		return f.getName().substring(f.getName().lastIndexOf(".")+1).toUpperCase();
	}
	public static String[]gi(File f){
		return f.getName().split("\\.");
	}
	public static String gn(File f){
		return f.getName().substring(0,f.getName().lastIndexOf("."));
	}
	public static void main(String[]argumentos){
		try{
			PrintStream ps=new PrintStream(new FileOutputStream(new File("ListarArchivosLog.txt")));
			HashMap<String,Integer>h=new HashMap<String,Integer>();
			File o=new File("D:\\Desarrollo\\EPM\\COMPRASE\\Entregas\\Decimales\\Fuentes\\");
			h.put("DEF",1);
			h.put("QUE",1);
			h.put("SQL",1);
			h.put("XLSX",1);
			ArrayList<File>l=listar(o,h);
			ps.println("copy \"D:\\Desarrollo\\EPM\\COMPRASE\\War\\Origen\\WEB-INF\\classes\\Precision.class\" \"D:\\Desarrollo\\EPM\\COMPRASE\\Entregas\\Decimales\\LocalTotal2\\Clases\\\"");
			for(int i=0;i<l.size();i++){
				//ps.println(gn(l.get(i))+"\t"+gx(l.get(i)));
				ps.println("copy \"D:\\Desarrollo\\EPM\\COMPRASE\\War\\Origen\\WEB-INF\\classes\\"+gn(l.get(i))+".class\" \"D:\\Desarrollo\\EPM\\COMPRASE\\Entregas\\Decimales\\LocalTotal2\\Clases\\\"");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}