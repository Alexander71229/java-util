declare
	pedido fnx_pedidos.pedido_id%type:='100054801';
	pedidoSalida fnx_pedidos.pedido_id%type:='100054801X01';
	procedure exportarRegistros(propietario in varchar2,tablasConsulta in varchar,tabla in varchar2,condicion in varchar2,cambios in varchar2)is
		nombres varchar2(1000):='';
		valores varchar2(5000):='';
		separador varchar2(1):='';
		cursor columnas is select column_name nombre from all_tab_cols where table_name=tabla and owner=propietario and column_name not like '%$%';
		--filaCursor columnas%rowtype;
		nombre varchar2(1000);
		cambiosModificados varchar2(1000);
		instruccion varchar2(3000);
		consulta varchar2(3000);
		cursorId integer;
		ignore integer;
		valor varchar2(1000);
		contador integer;
		tablas varchar2(3000);
	begin
		if tablasConsulta is null then
			tablas:=propietario||'.'||tabla||' a';
		else
			tablas:=tablasConsulta;
		end if;
		cambiosModificados:=nvl(cambios,'$,$');
		cambiosModificados:=''''||replace(cambiosModificados,',',''',''')||'''';
		for columna in columnas loop
			instruccion:='select decode('''||columna.nombre||''','||cambiosModificados||') from dual';
			execute immediate instruccion into nombre;
			if nombre is null then
				valores:=valores||separador||'a.'||columna.nombre;
			else
				valores:=valores||separador||nombre||' '||columna.nombre;
			end if;
			nombres:=nombres||separador||columna.nombre;
			separador:=',';
		end loop;
		consulta:='select '||valores||' from '||tablas||' where '||condicion;
		consulta:=replace(consulta,'|','''');
		consulta:=replace(consulta,'&',',');
		cursorId:=dbms_sql.open_cursor;
		--dbms_output.put_line('Consulta:'||consulta);
		dbms_sql.parse(cursorId,consulta,dbms_sql.native);
		contador:=1;
		for columna in columnas loop
			dbms_sql.define_column(cursorId,contador,valor,1000);
			contador:=contador+1;
		end loop;
		ignore:=dbms_sql.execute(cursorId);
		loop
			if dbms_sql.fetch_rows(cursorId)>0 then
				separador:='';
				instruccion:='insert into '||propietario||'.'||tabla||'('||nombres||') values(';
				contador:=1;
				for columna in columnas loop
					dbms_sql.column_value(cursorId,contador,valor);
					contador:=contador+1;
					instruccion:=instruccion||separador||''''||valor||'''';
					separador:=',';
				end loop;
				instruccion:=instruccion||');';
				dbms_output.put_line('begin');
				dbms_output.put_line(instruccion);
				dbms_output.put_line('exception when DUP_VAL_ON_INDEX then null;');
				dbms_output.put_line('end;');
			else
				exit;
			end if;
		end loop;
		dbms_sql.close_cursor(cursorId);
	end;
begin
	dbms_output.put_line('begin');
	exportarRegistros('FENIX',null,'FNX_LIDER_EJECUTIVOS','LIDER_EJECUTIVO_ID in(select LIDER_CATEGORIA from fnx_ejecutivos where EJECUTIVO_ID in(select EJECUTIVO_ID from fnx_subpedidos where pedido_id=|'||pedido||'|))',null);
	exportarRegistros('FENIX',null,'FNX_EJECUTIVOS','EJECUTIVO_ID in(select EJECUTIVO_ID from fnx_subpedidos where pedido_id=|'||pedido||'|)',null);
	exportarRegistros('FENIX',null,'FNX_USUARIOS','usuario_id=(select usuario_id from fnx_pedidos where pedido_id=|'||pedido||'|)',null);
	exportarRegistros('FENIX',null,'FNX_USUARIOS','usuario_id in(select usuario_id from fnx_subpedidos where pedido_id=|'||pedido||'|)',null);
	exportarRegistros('FENIX',null,'FNX_CLIENTES','cliente_id=(select cliente_id from fnx_pedidos where pedido_id=|'||pedido||'|)',null);
	exportarRegistros('FENIX',null,'FNX_AGRUPADORES','agrupador_id in(select agrupador_id from FNX_IDENTIFICADORES where identificador_id in(select identificador_id from fnx_solicitudes where pedido_id=|'||pedido||'|))',null);
	exportarRegistros('FENIX',null,'FNX_SERIES','serie_id in(select serie_id from FNX_IDENTIFICADORES where identificador_id in(select identificador_id from fnx_solicitudes where pedido_id=|'||pedido||'|))',null);
	exportarRegistros('FENIX',null,'FNX_IDENTIFICADORES','identificador_id in(select identificador_id from fnx_solicitudes where pedido_id=|'||pedido||'|)',null);
	exportarRegistros('FENIX',null,'FNX_CONFIGURACIONES_IDENTIF','identificador_id in(select identificador_id from fnx_solicitudes where pedido_id=|'||pedido||'|)',null);
	exportarRegistros('FENIX',null,'FNX_PEDIDOS','PEDIDO_ID=|'||pedido||'|','PEDIDO_ID,|'||pedidoSalida||'|');
	exportarRegistros('FENIX',null,'FNX_SUBPEDIDOS','PEDIDO_ID=|'||pedido||'|','PEDIDO_ID,|'||pedidoSalida||'|');
	exportarRegistros('FENIX','
		(select n.pedido_id,n.subpedido_id,n.solicitud_id,max(n.consecutivo)c from fnx_novedades_solicitudes n
		where n.pedido_id=|'||pedido||'|
		and n.usuario_id=n.usuario_id
		group by n.pedido_id,n.subpedido_id,n.solicitud_id)n,fnx_solicitudes a,fnx_novedades_solicitudes b
	','FNX_SOLICITUDES','
		n.pedido_id=a.pedido_id
		and n.subpedido_id=a.subpedido_id
		and n.solicitud_id=a.solicitud_id
		and b.pedido_id=a.pedido_id
		and b.subpedido_id=a.subpedido_id
		and b.solicitud_id=a.solicitud_id
		and b.consecutivo=n.c	
	','PEDIDO_ID,|'||pedidoSalida||'|,ESTADO_SOLI,|PENDI|,ESTADO_ID,decode((select c.concepto_id_actual from fnx_novedades_solicitudes c where c.pedido_id=n.pedido_id and c.subpedido_id=n.subpedido_id and c.solicitud_id=n.solicitud_id and c.consecutivo=n.c)&|PETEC|&|TECNI|&|PRACC|&|TECNI|&|PRUTA|&|RUTAS|&|42|&|INGRE|&|PERLV|&|INGRE|&|CUMPL|&|CUMPL|),CONCEPTO_ID,(select c.concepto_id_actual from fnx_novedades_solicitudes c where c.pedido_id=n.pedido_id and c.subpedido_id=n.subpedido_id and c.solicitud_id=n.solicitud_id and c.consecutivo=n.c)');
	exportarRegistros('FENIX',null,'FNX_CARACTERISTICA_SOLICITUDES','PEDIDO_ID=|'||pedido||'| and (a.pedido_id,a.subpedido_id,a.solicitud_id) in (select b.pedido_id,b.subpedido_id,b.solicitud_id from fnx_novedades_solicitudes b where b.usuario_id=b.usuario_id)','PEDIDO_ID,|'||pedidoSalida||'|');
	exportarRegistros('FENIX',null,'FNX_TRABAJOS_SOLICITUDES','PEDIDO_ID=|'||pedido||'| and (a.pedido_id,a.subpedido_id,a.solicitud_id) in (select b.pedido_id,b.subpedido_id,b.solicitud_id from fnx_novedades_solicitudes b where b.usuario_id=b.usuario_id)','PEDIDO_ID,|'||pedidoSalida||'|');
	exportarRegistros('FENIX',null,'FNX_REPORTES_INSTALACION','PEDIDO_ID=|'||pedido||'| and (a.pedido_id,a.subpedido_id,a.solicitud_id) in (select b.pedido_id,b.subpedido_id,b.solicitud_id from fnx_novedades_solicitudes b where b.usuario_id=b.usuario_id)','PEDIDO_ID,|'||pedidoSalida||'|');
	exportarRegistros('FENIX',null,'FNX_NOVEDADES_SOLICITUDES','PEDIDO_ID=|'||pedido||'| and usuario_id=usuario_id','PEDIDO_ID,|'||pedidoSalida||'|');
	dbms_output.put_line('commit;');
	dbms_output.put_line('exception when others then rollback;');
	dbms_output.put_line('dbms_output.put_line(sqlerrm);');
	dbms_output.put_line('dbms_output.put_line(dbms_utility.format_error_backtrace);');
	dbms_output.put_line('end;');
	rollback;
exception
	when others then dbms_output.put_line('Resultado temporal:'||sqlerrm);
	dbms_output.put_line(dbms_utility.format_error_backtrace);
	rollback;
end;
