import java.util.*;
import java.io.*;
public class CrearScript{
	public static String leer(String tabulacion,Scanner s)throws Exception{
		String resultado="";
		while(s.hasNextLine()){
			resultado+=tabulacion+s.nextLine()+"\n";
		}
		return resultado;
	}
	public static String leer(String tabulacion,File file)throws Exception{
		FileInputStream fis=new FileInputStream(file);
		byte[]data=new byte[(int)file.length()];
		fis.read(data);
		fis.close();
		//String s=new String(data,"UTF-8");
		String s=new String(data);
		return leer(tabulacion,new Scanner(s));
	}
	public static String leer(String tabulacion,String nombre)throws Exception{
		return leer(tabulacion,new File(nombre));
	}
	public static void main(String[]args){
		try{
			String nombre="TempExportar.sql";
			PrintStream ps=new PrintStream(new FileOutputStream(new File("ResultadoTemporal.sql"),false));
			Scanner entrada=new Scanner(new File(nombre));
			while(entrada.hasNextLine()){
				String linea=entrada.nextLine();
				if(linea.trim().indexOf("@")==0){
					ps.print(leer("\t",linea.trim().replace("@","")));
				}else{
					ps.println(linea);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}