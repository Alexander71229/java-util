drop sequence slog;
drop table tlog;
create table tlog(i number(10),m varchar2(4000),f date,t varchar2(4000));
create sequence slog increment by 1 start with 1;
create or replace procedure lg(m in varchar2)is
	pragma autonomous_transaction;
begin
	insert into tlog values(slog.nextval,m,sysdate,substr(dbms_utility.format_call_stack,1,4000));
	commit;
exception when others then null;
end;
