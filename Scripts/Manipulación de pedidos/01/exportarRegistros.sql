procedure exportarRegistros(propietario in varchar2,tablasConsulta in varchar,tabla in varchar2,condicion in varchar2,cambios in varchar2)is
	nombres varchar2(1000):='';
	valores varchar2(5000):='';
	separador varchar2(1):='';
	cursor columnas is select column_name nombre from all_tab_cols where table_name=tabla and owner=propietario and column_name not like '%$%';
	--filaCursor columnas%rowtype;
	nombre varchar2(1000);
	cambiosModificados varchar2(1000);
	instruccion varchar2(3000);
	consulta varchar2(3000);
	cursorId integer;
	ignore integer;
	valor varchar2(1000);
	contador integer;
	tablas varchar2(3000);
begin
	if tablasConsulta is null then
		tablas:=propietario||'.'||tabla||' a';
	else
		tablas:=tablasConsulta;
	end if;
	cambiosModificados:=nvl(cambios,'$,$');
	cambiosModificados:=''''||replace(cambiosModificados,',',''',''')||'''';
	for columna in columnas loop
		instruccion:='select decode('''||columna.nombre||''','||cambiosModificados||') from dual';
		execute immediate instruccion into nombre;
		if nombre is null then
			valores:=valores||separador||'a.'||columna.nombre;
		else
			valores:=valores||separador||nombre||' '||columna.nombre;
		end if;
		nombres:=nombres||separador||columna.nombre;
		separador:=',';
	end loop;
	consulta:='select '||valores||' from '||tablas||' where '||condicion;
	consulta:=replace(consulta,'|','''');
	consulta:=replace(consulta,'&',',');
	cursorId:=dbms_sql.open_cursor;
	--dbms_output.put_line('Consulta:'||consulta);
	dbms_sql.parse(cursorId,consulta,dbms_sql.native);
	contador:=1;
	for columna in columnas loop
		dbms_sql.define_column(cursorId,contador,valor,1000);
		contador:=contador+1;
	end loop;
	ignore:=dbms_sql.execute(cursorId);
	loop
		if dbms_sql.fetch_rows(cursorId)>0 then
			separador:='';
			instruccion:='insert into '||propietario||'.'||tabla||'('||nombres||') values(';
			contador:=1;
			for columna in columnas loop
				dbms_sql.column_value(cursorId,contador,valor);
				contador:=contador+1;
				instruccion:=instruccion||separador||''''||valor||'''';
				separador:=',';
			end loop;
			instruccion:=instruccion||');';
			dbms_output.put_line('begin');
			dbms_output.put_line(instruccion);
			dbms_output.put_line('exception when DUP_VAL_ON_INDEX then null;');
			dbms_output.put_line('end;');
		else
			exit;
		end if;
	end loop;
	dbms_sql.close_cursor(cursorId);
end;