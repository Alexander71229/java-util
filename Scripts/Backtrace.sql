create or replace function bt return varchar2 is
	x dbms_debug.backtrace_table;
	i number;
	r varchar2(4000);
begin
	dbms_debug.print_backtrace(x);
	i:=x.first();
	while i is not null loop
		r:=r||i||':'||x(i).name||'('||x(i).line#||')';
		i:=x.next(i);
	end loop;
	return r;
exception when others then
	dbms_output.put_line(sqlerrm);
	dbms_output.put_line(dbms_utility.format_error_stack());
	dbms_output.put_line(dbms_utility.format_error_backtrace());
	return '';
end;