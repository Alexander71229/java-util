package tc;
import java.util.*;
import java.io.*;
public class Tokenizer{
	public static ArrayList<Token>leer(InputStream entrada)throws Exception{
		ArrayList<Token>r=new ArrayList<Token>();
		int l=1;
		int c=0;
		Token t=null;
		int e=-1;
		StringBuffer sb=new StringBuffer();
		int b=-1;
		HashMap<Character,Integer>ignorar=new HashMap<Character,Integer>();
		ignorar.put('\r',1);
		ignorar.put(' ',1);
		//ignorar.put('\t',1);
		while((b=entrada.read())>=0){
			c++;
			if(Character.isLetterOrDigit(b)||b=='_'){
				if(e!=1){
					r.add(new Token(l,c));
					sb=new StringBuffer();
				}
				e=1;
				sb.append((char)b);
				continue;
			}
			if(e==1){
				r.get(r.size()-1).v=sb+"";
			}
			e=0;
			if(ignorar.get((char)b)!=null){
				e=-1;
				continue;
			}
			r.add(new Token(l,c,((char)b)+""));
			if(b=='\n'){
				l++;
				c=0;
				continue;
			}
			if(b=='\t'){
				c++;
				continue;
			}
		}
		if(e==1){
			r.get(r.size()-1).v=sb+"";
		}
		return r;
	}
}