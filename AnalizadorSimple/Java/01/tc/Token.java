package tc;
public class Token{
	public String v;
	public String t;
	public int l;
	public int c;
	public Token(){
	}
	public Token(int l,int c){
		this.l=l;
		this.c=c;
	}
	public Token(int l,int c,String v){
		this.l=l;
		this.c=c;
		this.v=v;
	}
	public String toString(){
		return l+":"+c+":"+v;
	}
}