package c;
import java.util.*;
import java.util.stream.Collectors;
class A{
	Date fecha;
	int id;
	String nombre;
	public void setFecha(Date fecha){
		this.fecha=fecha;
	}
	public Date getFecha(){
		return this.fecha;
	}
	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return this.id;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
}
public class Main{
	public static void m0(){
		while(true);
		//((Object)null).toString();
	}
	public static void m1(){
		m0();
	}
	public static void m2(){
		m1();
	}
	public static void m3(){
		m2();
	}
	public static void m4(){
		m3();
	}
	public static void main(String[]argumentos){
		try{
			U.ruta="Log.txt";
			U.imp("Inicio");
			//U.imp(Thread.currentThread().getStackTrace()[1]+"");
			//U.trazador().start();
			//m4();
			A a=new A();
			a.setFecha(new Date());
			a.setId(9452);
			a.setNombre("Eterzuli");
			U.imp(U.valoresCampos(a));
			U.imp(U.jar(a.getClass())+"");
			U.imp(U.listaMetodos(Collectors.class));
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}