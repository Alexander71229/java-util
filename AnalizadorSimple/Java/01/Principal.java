import java.io.*;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import tc.*;
public class Principal{
	public static HashMap<String,Integer>h01=new HashMap<>();
	public static void ch01(File f){
		try{
			ArrayList<Token>tokens=Tokenizer.leer(new FileInputStream(f));
			boolean comentario=false;
			boolean comentarioLinea=false;
			boolean comentarioMulti=false;
			for(int i=1;i<tokens.size();i++){
				if(!comentario){
					if(tokens.get(i).v.equals("/")&&tokens.get(i-1).v.equals("/")){
						comentario=true;
						comentarioLinea=true;
					}
					if(tokens.get(i-1).v.equals("/")&&tokens.get(i).v.equals("*")){
						comentario=true;
						comentarioMulti=true;
					}
					if(tokens.get(i-1).v.equals("@")&&tokens.get(i).v.endsWith("Mapping")){
						h01.put(tokens.get(i).v,1);
					}
				}else{
					if(comentarioLinea&&tokens.get(i).v.equals("\n")){
						comentario=false;
						comentarioLinea=false;
					}
					if(comentarioMulti&&tokens.get(i-1).v.equals("*")&&tokens.get(i).v.equals("/")){
						comentario=false;
						comentarioMulti=false;
					}
				}
			}
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
	public static void t01()throws Exception{
		//String ini="C:\\desarrollo\\COTRAFA\\Java\\atl\\cambio_formato_al_popular_atl\\api-atl\\src\\main\\java\\";
		String ini="C:\\desarrollo\\COTRAFA\\Java\\api-cajas\\git\\integracion_recaudos\\api-cajas\\src\\main\\java\\";
		Files.walk(Paths.get(ini)).filter(Files::isRegularFile).map(x->x.toFile()).filter(x->x.getName().toLowerCase().endsWith(".java")).forEach(x->ch01(x));
		c.U.imp(h01+"");
	}
	public static void main(String[]argumentos){
		try{
			c.U.imp("Inicio:"+new Date());
			t01();
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}