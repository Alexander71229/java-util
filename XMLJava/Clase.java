import java.util.*;
import java.io.*;
public class Clase{
	public String nombre;
	private ArrayList<Campo>campos;
	private HashMap<String,Campo>hash;
	public static ArrayList<Clase>clases;
	private static HashMap<String,Clase>h;
	public boolean lectura=false;
	public Clase(String nombre){
		this.nombre=nombre;
		this.campos=new ArrayList<Campo>();
		this.hash=new HashMap<String,Campo>();
	}
	public Campo agregarCampo(String tipo,String nombre){
		return agregarCampo(tipo,nombre,false);
	}
	public Campo agregarCampo(String tipo,String nombre,boolean clase){
		if(lectura){
			return null;
		}
		if(hash.get(nombre)==null){
			Campo campo=new Campo(tipo,nombre,clase);
			this.hash.put(nombre,campo);
			campos.add(campo);
			return campo;
		}
		Campo campo=hash.get(nombre);
		if(campo.tipo.indexOf("List")<0){
			campo.tipo="List<"+campo.tipo+">";
		}
		return campo;
	}
	public static Clase crear(String nombre){
		nombre=cap(nombre);
		if(h==null){
			h=new HashMap<String,Clase>();
		}
		if(clases==null){
			clases=new ArrayList<Clase>();
		}
		if(h.get(nombre)==null){
			Clase c=new Clase(nombre);
			h.put(nombre,c);
			clases.add(c);
			return c;
		}
		return h.get(nombre);
	}
	public String toString(){
		String r=this.nombre+"{";
		for(int i=0;i<this.campos.size();i++){
			r+=this.campos.get(i);
		}
		r+="}";
		return r;
	}
	public static String cap(String c){
		return c.substring(0,1).toUpperCase()+c.substring(1);
	}
	public static String ucap(String c){
		return c.substring(0,1).toLowerCase()+c.substring(1);
	}
	public String x(){
		String r="import java.util.*;\n";
		r+="import javax.xml.bind.annotation.*;\n";
		r+="@XmlRootElement(name=\""+ucap(this.nombre)+"\")\n";
		r+="@XmlAccessorType(XmlAccessType.FIELD)\n";
		r+="public class "+this.nombre+"{\n";
		for(int i=0;i<this.campos.size();i++){
			Campo campo=this.campos.get(i);
			if(campo.clase){
				r+="\t@XmlElement(name=\""+ucap(campo.nombre)+"\")\n";
			}
			r+="\tprivate "+ftipo(campo.tipo)+campo.nombre+";\n";
		}
		for(int i=0;i<this.campos.size();i++){
			Campo campo=this.campos.get(i);
			r+="\tpublic "+ftipo(campo.tipo)+"get"+cap(campo.nombre)+"(){\n";
			r+="\t\treturn this."+campo.nombre+";\n";
			r+="\t}\n";
		}
		for(int i=0;i<this.campos.size();i++){
			Campo campo=this.campos.get(i);
			r+="\tpublic void set"+cap(campo.nombre)+"("+ftipo(campo.tipo)+campo.nombre+"){\n";
			r+="\t\tthis."+campo.nombre+"="+campo.nombre+";\n";
			r+="\t}\n";
		}
		r+="}";
		return r;
	}
	public void escribir()throws Exception{
		PrintStream ps=new PrintStream(new FileOutputStream(new File(this.nombre+".java")));
		ps.println(this.x());
	}
	public String ftipo(String tipo){
		if(tipo.indexOf(">")>=0){
			return tipo;
		}
		return tipo+" ";
	}
}