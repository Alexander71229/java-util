import java.util.*;
import javax.xml.bind.annotation.*;
@XmlRootElement(name="solicitudDespachoMateriales")
@XmlAccessorType(XmlAccessType.FIELD)
public class SolicitudDespachoMateriales{
	@XmlElement(name="epm_SolicitudDespachoMateriales")
	private Epm_SolicitudDespachoMateriales Epm_SolicitudDespachoMateriales;
	public Epm_SolicitudDespachoMateriales getEpm_SolicitudDespachoMateriales(){
		return this.Epm_SolicitudDespachoMateriales;
	}
	public void setEpm_SolicitudDespachoMateriales(Epm_SolicitudDespachoMateriales Epm_SolicitudDespachoMateriales){
		this.Epm_SolicitudDespachoMateriales=Epm_SolicitudDespachoMateriales;
	}
}
