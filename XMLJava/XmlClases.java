import java.util.*;
import java.io.*;
public class XmlClases{
	public static void main(String[]argumentos){
		try{
			Scanner s=new Scanner(new File("SolicitudDespachoMateriales.xml"));
			String d="";
			String e="";
			String t="String";
			Stack<Clase>p=new Stack<Clase>();
			int estado=0;
			while(s.hasNext()){
				String z=s.next();
				d+=z;
			}
			char[]x=d.toCharArray();
			for(int i=0;i<x.length;i++){
				char c=x[i];
				if(x[i]=='<'&&x[i+1]=='/'){
					i++;
					if(estado==0){
						p.peek().lectura=true;
						Clase o=p.pop();
						p.peek().agregarCampo(o.nombre,o.nombre,true);
					}
					if(estado==2){
						p.peek().agregarCampo(t,e);
						estado=0;
					}
					continue;
				}
				if(x[i]=='/'&&x[i+1]=='>'){
					i++;
					p.peek().agregarCampo(t,e);
					continue;
				}
				if(x[i]=='<'){
					if(estado==2){
						p.push(Clase.crear(e));
					}
					e="";
					estado=1;
					continue;
				}
				if(x[i]=='>'){
					if(estado==1){
						estado=2;
					}
					continue;
				}
				if(estado==1){
					e+=c;
				}
			}
			for(int i=1;i<Clase.clases.size();i++){
				System.out.println(Clase.clases.get(i));
				Clase.clases.get(i).escribir();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}