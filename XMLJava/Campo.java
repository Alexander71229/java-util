public class Campo{
	public String tipo;
	public String nombre;
	public boolean clase;
	public Campo(String tipo,String nombre,boolean clase){
		this.tipo=tipo;
		this.nombre=nombre;
		this.clase=clase;
	}
	public String toString(){
		return tipo+" "+nombre;
	}
}