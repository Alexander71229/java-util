import java.util.*;
import java.io.*;
import javax.xml.bind.*;
public class Test{
	public static Object test(Object o)throws Exception{
		JAXBContext jc=JAXBContext.newInstance(o.getClass());
		Unmarshaller um=jc.createUnmarshaller();
		o=um.unmarshal(new File(o.getClass().getSimpleName()+".xml"));;
		return o;
	}
	public static void main(String[]argumentos){
		try{
			SolicitudDespachoMateriales o=(SolicitudDespachoMateriales)test(new SolicitudDespachoMateriales());
			System.out.println(o.getEpm_SolicitudDespachoMateriales().getMateriales().getMaterial().get(1).getFechahoratransaccion());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}