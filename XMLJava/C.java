import java.util.*;
import javax.xml.bind.annotation.*;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class C{
	private String n;
	private String v;
	public String getN(){
		return this.n;
	}
	public String getV(){
		return this.v;
	}
	public void setN(String n){
		this.n=n;
	}
	public void setV(String v){
		this.v=v;
	}
}
