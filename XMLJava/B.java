import java.util.*;
import javax.xml.bind.annotation.*;
@XmlRootElement(name="b")
@XmlAccessorType(XmlAccessType.FIELD)
public class B{
	private String nombre;
	private String valor;
	public String getNombre(){
		return this.nombre;
	}
	public String getValor(){
		return this.valor;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public void setValor(String valor){
		this.valor=valor;
	}
}
