import java.util.*;
import javax.xml.bind.annotation.*;
@XmlRootElement(name="epm_SolicitudDespachoMateriales")
@XmlAccessorType(XmlAccessType.FIELD)
public class Epm_SolicitudDespachoMateriales{
	private String aplicaciondestino;
	private String sistemaorigen;
	@XmlElement(name="materiales")
	private Materiales Materiales;
	public String getAplicaciondestino(){
		return this.aplicaciondestino;
	}
	public String getSistemaorigen(){
		return this.sistemaorigen;
	}
	public Materiales getMateriales(){
		return this.Materiales;
	}
	public void setAplicaciondestino(String aplicaciondestino){
		this.aplicaciondestino=aplicaciondestino;
	}
	public void setSistemaorigen(String sistemaorigen){
		this.sistemaorigen=sistemaorigen;
	}
	public void setMateriales(Materiales Materiales){
		this.Materiales=Materiales;
	}
}
