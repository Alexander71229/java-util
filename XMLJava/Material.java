import java.util.*;
import javax.xml.bind.annotation.*;
@XmlRootElement(name="material")
@XmlAccessorType(XmlAccessType.FIELD)
public class Material{
	private String almacen;
	private String cantidadentregada;
	private String codigomaterial;
	private String costotransaccion;
	private String descripcionmaterial;
	private String negocio;
	private String fechahoratransaccion;
	private String plantadespacho;
	private String noreserva;
	private String numerodeserie;
	private String ot_erp;
	private String ot_origen;
	private String secuencia;
	private String tipodespacho;
	private String identificadoritem;
	private String ubicacion;
	public String getAlmacen(){
		return this.almacen;
	}
	public String getCantidadentregada(){
		return this.cantidadentregada;
	}
	public String getCodigomaterial(){
		return this.codigomaterial;
	}
	public String getCostotransaccion(){
		return this.costotransaccion;
	}
	public String getDescripcionmaterial(){
		return this.descripcionmaterial;
	}
	public String getNegocio(){
		return this.negocio;
	}
	public String getFechahoratransaccion(){
		return this.fechahoratransaccion;
	}
	public String getPlantadespacho(){
		return this.plantadespacho;
	}
	public String getNoreserva(){
		return this.noreserva;
	}
	public String getNumerodeserie(){
		return this.numerodeserie;
	}
	public String getOt_erp(){
		return this.ot_erp;
	}
	public String getOt_origen(){
		return this.ot_origen;
	}
	public String getSecuencia(){
		return this.secuencia;
	}
	public String getTipodespacho(){
		return this.tipodespacho;
	}
	public String getIdentificadoritem(){
		return this.identificadoritem;
	}
	public String getUbicacion(){
		return this.ubicacion;
	}
	public void setAlmacen(String almacen){
		this.almacen=almacen;
	}
	public void setCantidadentregada(String cantidadentregada){
		this.cantidadentregada=cantidadentregada;
	}
	public void setCodigomaterial(String codigomaterial){
		this.codigomaterial=codigomaterial;
	}
	public void setCostotransaccion(String costotransaccion){
		this.costotransaccion=costotransaccion;
	}
	public void setDescripcionmaterial(String descripcionmaterial){
		this.descripcionmaterial=descripcionmaterial;
	}
	public void setNegocio(String negocio){
		this.negocio=negocio;
	}
	public void setFechahoratransaccion(String fechahoratransaccion){
		this.fechahoratransaccion=fechahoratransaccion;
	}
	public void setPlantadespacho(String plantadespacho){
		this.plantadespacho=plantadespacho;
	}
	public void setNoreserva(String noreserva){
		this.noreserva=noreserva;
	}
	public void setNumerodeserie(String numerodeserie){
		this.numerodeserie=numerodeserie;
	}
	public void setOt_erp(String ot_erp){
		this.ot_erp=ot_erp;
	}
	public void setOt_origen(String ot_origen){
		this.ot_origen=ot_origen;
	}
	public void setSecuencia(String secuencia){
		this.secuencia=secuencia;
	}
	public void setTipodespacho(String tipodespacho){
		this.tipodespacho=tipodespacho;
	}
	public void setIdentificadoritem(String identificadoritem){
		this.identificadoritem=identificadoritem;
	}
	public void setUbicacion(String ubicacion){
		this.ubicacion=ubicacion;
	}
}
