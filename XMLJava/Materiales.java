import java.util.*;
import javax.xml.bind.annotation.*;
@XmlRootElement(name="materiales")
@XmlAccessorType(XmlAccessType.FIELD)
public class Materiales{
	@XmlElement(name="material")
	private List<Material>Material;
	public List<Material>getMaterial(){
		return this.Material;
	}
	public void setMaterial(List<Material>Material){
		this.Material=Material;
	}
}
