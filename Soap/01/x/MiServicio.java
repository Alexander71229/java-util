package x;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService
public class MiServicio {

    @WebMethod
    public String saludar(String nombre) {
        return "Hola, " + nombre + "!";
    }

    public static void main(String[] args) {
        String url = "http://localhost:8080/miServicio";
        Endpoint.publish(url, new MiServicio());
        System.out.println("Servicio publicado en: " + url);
    }
}