import java.lang.reflect.Method;
public class PropertyUtils{
	public static String cap(String s){
		return s.substring(0,1).toUpperCase()+s.substring(1).toLowerCase();
	}
	public static Object getProperty(Object o,String nombre)throws Exception{
		Method m=o.getClass().getMethod("get"+cap(nombre));
		return m.invoke(o);
	}
}