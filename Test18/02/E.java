import java.util.*;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import java.lang.reflect.Field;
public class E<T>{
	private LogicaRegistro<T>logicaRegistro;
	public E(){
		this.logicaRegistro=x->null;
	}
	public E(LogicaRegistro<T>logicaRegistro){
		this.logicaRegistro=logicaRegistro;
	}
	public byte[]escribir(List<T>datos,String separador,String separadorLinea,String caracterInicial,String caracterFinal){
		return escribirACadena(datos,separador,separadorLinea,caracterInicial,caracterFinal).getBytes(StandardCharsets.UTF_8);
	}
	public String escribirACadena(List<T>datos,String separador,String separadorLinea,String caracterInicial,String caracterFinal){
		ArrayList<Object>objetos=new ArrayList<Object>();
		ArrayList<Object>nombreColumnas=new ArrayList<Object>();
		nombreColumnas.addAll(obtenerNombreColumnas(datos.get(0).getClass()));
		if(!nombreColumnas.isEmpty()){
			objetos.add(0,nombreColumnas);
		}
		objetos.addAll(datos);
		return objetos.stream().map(x->{
			String linea=null;
			try{
				linea=logicaRegistro.logicaRegistro((T)x);
			}catch(Throwable t){
			}
			if(linea==null){
				return escribirLinea((T)x,separador);
			}
			return linea;
		}).collect(Collectors.joining(separadorLinea,caracterInicial,caracterFinal));
	}
	public List<Object>convertirALista(T elemento){
		List<Object>lista=null;
		if(elemento instanceof List){
			return lista=(List<Object>)elemento;
		}
		lista=Arrays.stream(elemento.getClass().getDeclaredFields()).map(campo->{
			try{
				return ""+PropertyUtils.getProperty(elemento,campo.getName());
			}catch(Exception e){
				return "";
			}
		}).collect(Collectors.toList());
		return lista;
	}
	public String escribirLinea(T elemento,String separador){
		return escribirListaLinea(convertirALista(elemento),separador);
	}
	public String escribirListaLinea(List<Object>lista,String separador){
		return lista.stream().map(Object::toString).collect(Collectors.joining(separador));
	}
	private List<String>obtenerNombreColumnas(Class<?>clase){
		List<String>nombreColumnas=new ArrayList<>();
		for(Field atributo:clase.getDeclaredFields()){
			NombreColumna nombreColumna=atributo.getAnnotation(NombreColumna.class);
			if(null!=nombreColumna&&!"".equals(nombreColumna.value())){
				nombreColumnas.add(nombreColumna.value());
			}else if(null!=nombreColumna){
				nombreColumnas.add(atributo.getName());
			}
		}
		return nombreColumnas;
	}
}