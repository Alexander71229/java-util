public class D{
	@NombreColumna
	private int id;
	@NombreColumna
	private String nombre;
	public D(){
	}
	public D(int id,String nombre){
		this.id=id;
		this.nombre=nombre;
	}
	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return this.id;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
}