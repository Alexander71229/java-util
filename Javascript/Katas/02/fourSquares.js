function s(z){
	var v=[0n,1n,1n,1n,2n,2n,2n,2n,2n,3n,3n,3n];
	if(v.length>z){
		return v[z];
	}
	var x=z/2n-1n;
	var a=3n;
	var b=x;
	var d=z-x*x;
	var l=1000000000000000000000;
	while(d!=0n&&b-a>1n&&--l){
		if(x*x>z){
			b=x;
		}else{
			a=x;
		}
		x=(a+b)/2n;
		d=z-x*x;
	}
	return x;
}
var h={};
function f(x,k){
	r=[];
	var n=x;
  while(n>0){
		r.push(k||s(n));k=0n;
		h[r[r.length-1]]=x;
		n=n-r[r.length-1]*r[r.length-1];
	}
	while(r.length<4){
		r.push(0n);
	}
	return r;
}
function rz(a,b){
	var x=100000000000000;
	return BigInt(Math.floor(Math.random()*x))*(b-a)/(BigInt(x))+a;
}
function rx(a,b){
	var l=100;
	var r=rz(a,b);
	while(h[r]&&--l){
		r=rz(a,b);
	}
	return r;
}
const fourSquares=function(x){
	h={};
	var r=f(x);console.log(r);
	var l=1000;
	var mx=r[0];
	var k=1n;
	var i=1n;
	var a=r[1];
	var b=r[0];
	var q=(b-a)/2n**k;
	while(r.length>4&&--l){
		if(r[1]+i*q>=r[0]){
			k++;
			i=1n;
			q=(b-a)/2n**k;
		}
		if(!h[r[1]+i*q]){
			r=f(x,r[1]+i*q);
		}
		i++;
	}
	return r;
}
function t1(l){
	var r=[];
	var a=1000;
	var b=1000;
	while(--l){
		r.push(a*a+b*b);
		if(a==b){
			a++;
			b=0;
		}
		b++;
	}
	r.sort((a,b)=>a-b);
	var x=[];
	for(var i=1;i<r.length;i++){
		x.push(r[i]-r[i-1]);
	}
	console.log(x);
	return r;
}
function t2(l){
	var r=[];
	var z=[1,1,1,1];
	while(--l){
		r.push([[z[0],z[1],z[2],z[3]],t3(z)]);
		if(z[0]==z[1]&&z[1]==z[2]&&z[2]==z[3]){
			z[0]++;
			z[1]=z[2]=z[3]=0;continue;
		}
		if(z[1]==z[2]&&z[2]==z[3]){
			z[1]++;
			z[2]=z[3]=0;continue;
		}
		if(z[2]==z[3]){
			z[2]++;
			z[3]=0;continue;
		}
		z[3]++;
	}
	r.sort((a,b)=>a[1]-b[1]);
	return r;
}
function t3(r){
	var t=0;
	for(var i=0;i<r.length;i++){
		t+=r[i]*r[i];
	}
	return t;
}
