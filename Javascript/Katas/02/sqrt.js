function sqrt(value) {
  let guess = value / 2n;
  let precision = 100n; // The number of decimal places to calculate

  while (true) {
    let quotient = value / guess;
    let quotientRounded = Math.round(quotient * (10n ** precision)) / (10n ** precision);

    if (quotientRounded === guess) {
      // The guess is accurate enough
      break;
    } else {
      // Adjust the guess and continue the loop
      guess = (guess + quotient) / 2n;
    }
  }

  return guess;
}
