var calc=function(expression){
	var o={"#":1,"*":2,"/":2,"+":3,"-":3,"(":4,")":5};
	var x={"*":(a,b)=>a*b,"/":(a,b)=>b/a,"+":(a,b)=>a*1+b*1,"-":(a,b)=>b-a,"#":1};
	expression="("+expression+")";
	var e=expression.replace(/ /g,"").replace(/\(-/g,"(#").replace(/--/g,"-#").replace(/\+-/g,"+#").replace(/\*-/g,"*#").replace(/\/-/g,"/#");
	var os=[];
	var ts=[];
	var n="";
	for(var i=0;i<e.length;i++){
		var c=e.charAt(i);
		if(o[c]){
			if(n.length>0){
				ts.push(n);
				n="";
			}
			while(c!='('&&o[os[os.length-1]]<=o[c]&&x[os[os.length-1]]){
				if(os[os.length-1]=="#"){
					ts.push(-ts.pop());os.pop();
				}else{
					ts.push(x[os.pop()](ts.pop(),ts.pop()));
				}
			}
			if(c==')'){
				os.pop();
			}else{
				os.push(c);
			}
		}else{
			n+=c;
		}
	}
	return+ts.pop();
};
