const {exec}=require('child_process');
const fs=require('fs');
const v='F:/Desarrollo/Audio/RVC/Alex3n/500e/resultados/10/';
const rd='F:/Desarrollo/Javascript/NodeJs/Sincronizador/sincronizador/public/c/karaoke/';
const b='F:/Desarrollo/Audio/RVC/Recursos/';
const rj='F:/Desarrollo/Javascript/NodeJs/Sincronizador/sincronizador/public/admin/datos/Karaoke/';
function convertirMP3aMono(archivoEntrada,archivoSalida){
	const comando=`ffmpeg -i "${archivoEntrada}" -ac 1 "${archivoSalida}" -y`;
	exec(comando,(error,stdout,stderr)=>{
		if(error){
			console.error(`Error al convertir el archivo: ${error.message}`);
			return;
		}
		if(stderr){
			console.error(`stderr: ${stderr}`);
			return;
		}
		console.log(`Archivo convertido exitosamente a mono: ${archivoSalida}`);
	});
}
fs.readdir(v,(error,archivos)=>{
	for(let i=0;i<archivos.length;i++){
		let m=archivos[i];
		let n=m.replace(".mp3","");
		let ax='-mv';
		fs.readdir(b+n+ax,(error,archivos)=>{
			if(error){
				console.error(error);
				return;
			}
			for(let i=0;i<archivos.length;i++){
				let bfm="";
				if(archivos[i].indexOf('other')>=0&&archivos[i].indexOf('vocal')<=0){
					bfm=archivos[i];
					let r={};
					r.n=n;
					r.d=[];
					r.d.push({o:0,r:-1,t:"a",f:"mp3",u:"/c/karaoke/"+n+"-V.mp3"});
					r.d.push({o:0,r:1,t:"a",f:"mp3",u:"/c/karaoke/"+n+"-V.mp3"});
					r.d.push({o:0,r:4,t:"a",f:"mp3",u:"/c/karaoke/"+n+"-B.mp3"});
					convertirMP3aMono(v+m,rd+n+"-V.mp3");
					convertirMP3aMono(b+n+ax+"/"+bfm,rd+n+"-B.mp3");
					fs.writeFile(rj+n+'.json',JSON.stringify(r),(error)=>{
						if(error){
							console.trace(rj+n+'.json',error);
						}
					});
				}
			}
		});
	}
});