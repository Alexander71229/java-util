ex={};
ex['maj']=[0,4,7];
ex['min']=[0,3,7];
ex['7']=[0,4,7,10];
ex['5']=[0,7];
ex['aug']=[0,4,8];
ex['dim']=[0,3,6];
ex['maj6']=[0,4,7,9];
ex['min6']=[0,3,7,9];
ns={};
ns['C']=0;
ns['D']=2;
ns['E']=4;
ns['F']=5;
ns['G']=7;
ns['A']=9;
ns['B']=11;
function ver(){
	let h={};
	for(let c in Chordify.song.chords){
		Chordify.song.chords[c].handleParts&&(h[Chordify.song.chords[c].handleParts.extension]=1);
	}
	console.log(h);
}
function cx(a){
	let r="evs=[];\n";
	for(let i in a){
		r+="evs.push(new Evento("+Math.round(a[i].a*1000)+","+Math.round(a[i].b*1000)+",0,0,0,0,0,'',"+a[i].t+",1,"+a[i].n+"));\n";
	}
	return r;
}
function vh(v){
	let r=ns[v.charAt(0)]
	if(v.length>1){
		if(v.charAt(1)=='#'){
			r++;
		}
		if(v.charAt(1)=='b'){
			r--;
		}
	}
	return r;
}
function ac(n,c,e,l,z){
	let r=[];
	let h={};
	z=z||{};
	for(let i in ex[e]){
		h[ex[e][i]]=1;
	}
	let x=vh(c);
	let k=0;
	while(r.length<l&&k<12){
		if(h[(n+k-x)%12]&&!z[(n+k)%12]){
			r.push(n+k);
		}
		k++;
	}
	return r;
}
function v01(){
	let as=[];
	let cs={1:1,3:1};
	//let cs={1:1,2:1,3:1,4:1};
	for(c in Chordify.song.chords){
		if(Chordify.song.chords[c].handleParts&&cs[Chordify.song.chords[c].beat]){
			let n1=ac(48,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,1)[0];
			let z={};
			z[n1%12]=1;
			let n2=ac(60,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,1,z)[0];
			as.push({a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to,t:2,n:n1});
			as.push({a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to,t:3,n:n2});
		}
	}
	return as;
}
function v02(){
	let as=[];
	let cs={1:1,3:1};
	//let cs={1:1,2:1,3:1,4:1};
	for(c in Chordify.song.chords){
		if(Chordify.song.chords[c].handleParts&&cs[Chordify.song.chords[c].beat]){
			let n1=ac(48,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,1);
			let n2=ac(60,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,3);
			for(let k in n1){
				as.push({a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to,t:2,n:n1[k]});
			}
			for(let k in n2){
				as.push({a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to,t:3,n:n2[k]});
			}
		}
	}
	return as;
}
function v03(){
	let as=[];
	let cs={1:1,3:1};
	//let cs={1:1,2:1,3:1,4:1};
	for(c in Chordify.song.chords){
		if(Chordify.song.chords[c].handleParts&&cs[Chordify.song.chords[c].beat]){
			let n1=ac(48,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,3);
			let n2=ac(60,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,1);
			for(let k in n1){
				as.push({a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to,t:2,n:n1[k]});
			}
			for(let k in n2){
				as.push({a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to,t:3,n:n2[k]});
			}
		}
	}
	return as;
}
function v04(){
	let as=[];
	let cs={1:1,3:1};
	//let cs={1:1,2:1,3:1,4:1};
	for(c in Chordify.song.chords){
		if(Chordify.song.chords[c].handleParts&&cs[Chordify.song.chords[c].beat]){
			let n1=ac(48,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,3);
			let n2=ac(60,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,3);
			for(let k in n1){
				as.push({a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to,t:2,n:n1[k]});
			}
			for(let k in n2){
				as.push({a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to,t:3,n:n2[k]});
			}
		}
	}
	return as;
}
console.log(cx(v04()));
