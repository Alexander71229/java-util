ex={};
ex['maj']=[0,4,7];
ex['min']=[0,3,7];
ex['7']=[0,4,7,10];
ex['5']=[0,7];
ex['aug']=[0,4,8];
ex['dim']=[0,3,6];
ex['maj6']=[0,4,7,9];
ex['min6']=[0,3,7,9];
ns={};
ns['C']=0;
ns['D']=2;
ns['E']=4;
ns['F']=5;
ns['G']=7;
ns['A']=9;
ns['B']=11;
function vh(v){
	let r=ns[v.charAt(0)]
	if(v.length>1){
		if(v.charAt(1)=='#'){
			r++;
		}
		if(v.charAt(1)=='b'){
			r--;
		}
	}
	return r;
}
function ac(n,c,e,l,z){
	let r=[];
	let h={};
	z=z||{};
	for(let i in ex[e]){
		h[ex[e][i]]=1;
	}
	let x=vh(c);
	let k=0;
	while(r.length<l&&k<12){
		if(h[(n+k-x)%12]&&!z[(n+k)%12]){
			r.push(n+k);
		}
		k++;
	}
	return r;
}
res=960;
function v01(){
	let as=[];
	let tm=[];
	let ix=0;
	for(c in Chordify.song.chords){
		if(Chordify.song.chords[c].handleParts){
			let n1=ac(48,Chordify.song.chords[c].handleParts.root,Chordify.song.chords[c].handleParts.extension,3);
			for(let k in n1){
				as.push({a:res*ix,b:res*(ix+1),n:n1[k],t:0});
			}
		}
		tm.push({k:res*ix,a:Chordify.song.chords[c].from,b:Chordify.song.chords[c].to});
		ix++;
	}
	return[as,tm];
}
function cx(v){
	let as=v[0];
	let tm=v[1];
	let r="res="+res+"\n";
	for(let i=0;i<as.length;i++){
		r+="mx.add(new N("+as[i].a+","+as[i].b+","+as[i].n+","+as[i].t+"));\n";
	}
	r+="audioh={};\n";
	for(let i=0;i<tm.length;i++){
		r+="audioh["+tm[i].k+"]=["+tm[i].a+","+tm[i].b+"];\n";
	}
	return r;
}
console.log(cx(v01()));