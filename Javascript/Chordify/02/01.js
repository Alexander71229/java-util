v={};
/*nl=58;
v[4]="r 1 1 [ B A ] A A A";
v[2]="r 1 1 [ B A ] A";
v[1]="r 1 1 [ B A ]";*/
/*
nl=70;
v[1]="r 1 1 [ 4 3 2 1 ]";
*/
/*nl=53;
v[4]="r 1 2 3 2 1 2 1 2 1 2";
v[3]="r 1 2 3 2 1 2 1 2";
v[2]="r 1 2 3 2 1 2";
v[1]="r 1 2 3 2";*/
nl=53;
v[4]="r 3 2 B A r 1 1 A";
v[3]="r 3 2 B A";
//v[2]="r 1 2 3 2 1 2";
v[2]="r 3 2 B r 1 2 A";
v[1]="r 1 1 A";
ex={};
ex['maj']=[0,4,7];
ex['min']=[0,3,7];
ex['7']=[0,4,7,10];
ex['5']=[0,7];
ex['aug']=[0,4,8];
ex['dim']=[0,3,6];
ex['maj6']=[0,4,7,9];
ex['min6']=[0,3,7,9];
ex['maj7']=[0,4,7,11];
ns={};
ns['C']=0;
ns['C#']=1;
ns['Db']=1;
ns['D']=2;
ns['D#']=3;
ns['Eb']=3;
ns['E']=4;
ns['F']=5;
ns['F#']=6;
ns['Gb']=6;
ns['G']=7;
ns['G#']=8;
ns['Ab']=8;
ns['A']=9;
ns['A#']=10;
ns['Bb']=10;
ns['B']=11;
function cov(h,n){
	if(n.toUpperCase()=="A"){
		return cov(h,"2")+" "+cov(h,"1");
	}
	if(n.toUpperCase()=="B"){
		n="3";
	}
	if(n=="0"){
		let i=ns[h[2]||h[0]];
		for(;i<nl;i+=12);
		return i;
	}
	if(!ex[h[1]]){
		console.log(h[1]);
		asdfadsf
	}
	let v=[...((h[2]&&ex[h[1]])||ex[h[1]])];
	//let v=[...ex[h[1]]];
	//h[2]&&v.push(h[2]);
	let x=[];
	for(let i=0;i<v.length;i++){
		for(let j=v[i]+ns[h[0]];j<nl;j+=12){
			x.push(j);
		}
	}
	return x.sort((a,b)=>b-a)[n*1-1];
}
d={};
d=Chordify.song.chords;
r="";
{
	let a="";
	for(i=0;d[i];i++){
		if(d[i].handle!=a){
			a=d[i].handle;
			o=d[i];
			o.c=1;
			d[i].p=0;
			continue;
		}
		d[i].p=o.c;
		o.c++;
	}
}
function imp(m){
	r+=m;
}
function fmx(tc){
	let m=-1;
	for(let i in v){
		if(tc>=i&&i>m){
			m=i;
		}
	}
	return m;
}
u=[];
for(i=0;d[i];i++){
	if(d[i].c){
		let h=d[i].handleParts&&[d[i].handleParts.root,d[i].handleParts.extension,d[i].handleParts.bassNote];
		h=h||['N'];
		let o={h,c:d[i].c,v:''};
		let tc=d[i].c;
		while(tc>0){
			let x=fmx(tc);
			tc=tc-x;
			o.v+=v[x]+" ";
		}
		u.push(o);
	}
}
af="";
pn=false;
imp("c\n");
for(i=0;u[i];i++){
	if(u[i].h[0]=="N"){
		for(let k=0;k<u[i].c;k++){
			let f="1:1";
			if(f!=af){
				imp(" r 1 1");
				af=f;
			}
			imp(" s");
		}
		continue;
	}
	let q=u[i].v.split(" ");
	for(j=0;q[j];j++){
		if(q[j]=='r'){
			let f=q[j+1]+":"+q[j+2];
			if(f!=af){
				imp(' r '+q[j+1]+" "+q[j+2]);
				af=f;
			}
			j+=2;
			continue;
		}
		if(q[j]=='['){
			imp(" [");
			pn=true;
			continue;
		}
		if(q[j]==']'){
			imp(" ]");
			pn=false;
			continue;
		}
		let nx=q[j];
		let vax=cov(u[i].h,nx);
		if(!vax){
			console.log(u[i].h,nx);
			asdfadsf
		}
		if(!pn&&q[j].toUpperCase()=="A"&&vax.indexOf(" ")>0){
			vax="[ "+vax+" ]";
		}
		imp(" "+vax);
	}
}
imp("\nc");
imp("\nT 0");
imp("\nr 1 1");
for(let i=0;d[i];i++){
	let tx=60/(d[i].to-d[i].from);
	imp("\nt "+(tx+"").replace(".",",")+" s");
}
console.log(r);