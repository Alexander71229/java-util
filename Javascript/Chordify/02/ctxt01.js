ex={};
ex['maj']=[0,4,7];
ex['min']=[0,3,7];
ex['7']=[0,4,7,10];
ex['5']=[0,7];
ex['aug']=[0,4,8];
ex['dim']=[0,3,6];
ex['maj6']=[0,4,7,9];
ex['min6']=[0,3,7,9];
ex['min7']=[0,3,7,11];
ex['maj7']=[0,4,7,11];
ex['sus4']=[0,5,7];
ns={};
ns['C']=0;
ns['C#']=1;
ns['Db']=1;
ns['D']=2;
ns['D#']=3;
ns['Eb']=3;
ns['E']=4;
ns['F']=5;
ns['F#']=6;
ns['Gb']=6;
ns['G']=7;
ns['G#']=8;
ns['Ab']=8;
ns['A']=9;
ns['A#']=10;
ns['Bb']=10;
ns['B']=11;
function cov(h,n,v,nl){
	if(n=="s"){
		return n;
	}
	if(nl>0){
		if(n=="A"){
			return cov(h,"2",v,nl)+" "+cov(h,"1",v,nl);
		}
		if(n=="B"){
			n="3";
		}
		if(n=="0"){
			let i=ns[h[2]||h[0]];
			for(;i<nl;i+=12);
			return i;
		}
	}
	if(!ex[h[1]]){
		console.log(h[1]);
		asdfadsf
	}
	let vx=[...((h[2]&&ex[h[1]])||ex[h[1]])];
	let x=[];
	if(nl>0){
		for(let i=0;i<vx.length;i++){
			for(let j=vx[i]+ns[h[0]];j<nl;j+=12){
				x.push(j);
			}
		}
		let ox=x.sort((a,b)=>b-a);
		return ox[n*1-1];
	}else{
		let x=[];
		if(h[2]&&!vx.includes(ns[h[2]]-ns[h[0]])){
			vx.unshift(ns[h[2]]-ns[h[0]]);
		}
		for(let i=0;i<vx.length;i++){
			for(let j=vx[i]+ns[h[0]];j<=108;j+=12){
				x.push(j);
			}
		}
		x=x.sort((a,b)=>a-b);
		let bn=-1;
		for(let i=0;i<x.length;i++){
			if(h[2]){
				if(x[i]>=-nl&&x[i]%12==ns[h[2]]){
					bn=i;
					break;
				}
			}else{
				if(x[i]>=-nl&&x[i]%12==ns[h[0]]){
					bn=i;
					break;
				}
			}
		}
		return x[bn+n*1];
	}
}
function fmx(tc,v){
	let m=-1;
	for(let i in v){
		if(tc>=i&&i>m){
			m=i;
		}
	}
	return m;
}
function ctxt(v,d,nl,ofs,nci){debugger;
	ofs=ofs||0;
	r="";
	{
		let a="";
		for(i=0;d[i];i++){
			if(d[i].handle!=a){
				a=d[i].handle;
				o=d[i];
				o.c=1;
				d[i].p=0;
				continue;
			}
			d[i].p=o.c;
			o.c++;
		}
	}
	u=[];
	for(i=0;d[i];i++){
		if(d[i].c){
			let h=d[i].handleParts&&[d[i].handleParts.root,d[i].handleParts.extension,d[i].handleParts.bassNote];
			h=h||['N'];
			let o={h,c:d[i].c,v:''};
			let tc=d[i].c;
			while(tc>0){
				let x=fmx(tc,v);
				tc=tc-x;
				if(v[x].toUpperCase){
					o.v+=v[x]+" ";
				}else{
					const ci=Math.floor((d[i].beat-1)/x);
					o.v+=v[x][ci%v[x].length]+" ";
				}
			}
			u.push(o);
		}
	}
	af="";
	pn=false;
	r+=("c\n");
	if(nci){
		r+=("r "+nci+" 1 s\n");
	}
	for(i=0;u[i];i++){
		if(u[i].h[0]=="N"){
			for(let k=0;k<u[i].c;k++){
				let f="1:1";
				if(f!=af){
					r+=(" r 1 1");
					af=f;
				}
				r+=(" s");
			}
			continue;
		}
		let q=u[i].v.split(" ");
		for(j=0;q[j];j++){
			if(q[j]=='r'){
				let f=q[j+1]+":"+q[j+2];
				if(f!=af){
					r+=(' r '+q[j+1]+" "+q[j+2]);
					af=f;
				}
				j+=2;
				continue;
			}
			if(q[j]=='['){
				r+=(" [");
				pn=true;
				continue;
			}
			if(q[j]==']'){
				r+=(" ]");
				pn=false;
				continue;
			}
			let nx=q[j];
			let vax=cov(u[i].h,nx,v,nl);
			if(!vax||(vax+"").indexOf("undefined")>=0){
				console.log(vax);
				console.log(u[i].h,nx);
				asdfadsf
			}
			if(!pn&&q[j]=="A"&&vax.indexOf(" ")>0){
				vax="[ "+vax+" ]";
			}
			r+=(" "+vax);
		}
	}
	r+=("\nc");
	r+=("\nT 0");
	r+=("\nr 1 1");
	for(let i=0;i<nci;i++){
		let tx=60000*nci/ofs;
		r+="\nt "+(tx+"").replace(".",",")+" s";
	}
	for(let i=0;d[i];i++){
		let tx=60/(d[i].to-d[i].from);
		r+=("\nt "+(tx+"").replace(".",",")+" s");
	}
	return r;
}
module.exports={ctxt};