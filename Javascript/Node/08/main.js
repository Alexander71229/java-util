const fs=require('fs');
p="F:\\Desarrollo\\Javascript\\NodeJs\\Sincronizador\\sincronizador\\public\\c\\letras\\Annie's Song.lyx";
z="F:\\Desarrollo\\Javascript\\NodeJs\\Sincronizador\\sincronizador\\public\\c\\letras\\Annie's Song.lyc";
function fmt(x){
	//console.log(x.replace(",",".")*1000);
	return Math.round(x.replace(",",".")*1000);
}
fs.readFile(p,'utf-8',(error,datos)=>{
	if(error){
		//console.log(error);
		return;
	}
	let ids=datos.split('::\r\n');
	let trns=null;
	if(ids.length>1){
		trns=ids[1].split('\r\n');
	}
	let lins=ids[0].split('\r\n');
	var r='';
	var e='';
	for(let i=0;i<lins.length;i++){
		let x=lins[i].split(':');
		if(x.length!=3){
			break;
		}
		r+=fmt(x[0])+":"+fmt(x[1])+":"+x[2]+"\r\n";
		if(trns){
			e+=fmt(x[0])+":"+fmt(x[1])+":"+trns[i]+"\r\n";
		}
	}
	r=r+"::\r\n"+e;
	fs.writeFile(z,r,(error)=>{
		if(error){
			console.log(error);
		}
	});
});
