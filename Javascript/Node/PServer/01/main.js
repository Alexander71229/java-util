const express=require('express');
const app=express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
const path=require('path');
app.use(express.static(path.join(__dirname,'public')));
const fs=require('fs');
log=(m)=>{
	fs.writeFile('log.txt',m,(error)=>{
		if(error)console.error(error);
	});
}
const rutasAdministrador=require('./administrador/index.js');
app.use("/admin",rutasAdministrador);
const rutasCliente=require('./cliente/index.js');
app.use("/cliente",rutasCliente);
const rutasDebug=require('./debug/index.js');
app.use("/debug",rutasDebug);
const rutasContenido=require('./contenido/index.js');
app.use("/c",rutasContenido);
app.listen(80,()=>{
	manual=0;
	tid=0;
	global={};
	console.log('Servidor iniciado puerto 80');
	fetch('https://ifconfig.me/ip').then(x=>x.text()).then(x=>pip=x);
});
rm=(t,u)=>{
	if(t=='a'){
		return'<audio id=\'rep\' oncanplaythrough="lr()"><source src="'+u+'" type="audio/mpeg"/></audio>';
	}
	if(t=='v'){
		return'<video id=\'rep\' oncanplaythrough="lr()"><source src="'+u+'" type="video/mp4"/></video>';
	}
}
datosCliente={};
leerClientes=()=>{
	fs.readFile('datosCliente.json','utf8',(error,datos)=>{
		if(error){
			log(error+"");
		}
		datosCliente=JSON.parse(datos);
	});
};
leerClientes();
guardarCliente=(id,valor)=>{
	datosCliente[id]=valor;
	fs.writeFile('datosCliente.json',JSON.stringify(datosCliente),(error)=>{
		if(error){
			log(error+"");
			return;
		}
	});
}
