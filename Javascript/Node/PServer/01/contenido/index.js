const express=require('express');
const rutas=express.Router();
const fs=require('fs');
rutas.route("/").get((req,res)=>{
	fs.readFile('./public/c/'+req.query.p,(error,datos)=>{
		if(error){
			console.log(error);
			res.writeHead(404,{'Content-Type':'text/plain'});
			res.end('');
			return;
		}
		res.end(datos);
	});
});
module.exports=rutas;