const express=require('express');
const rutas=express.Router();
const fs=require('fs');
const path=require('path');
rutas.route("/main").get((req,res)=>{
	fs.readFile('./template/maintemplate.html',(error,datos)=>{
		if(error){
			log(error+"");
			res.end('error');
			return;
		}
		let tp=datos+"";
		fs.readdir('./public/admin/datos/',(error,archivos)=>{
			if(error){
				console.error(error);
				log(error);
				return;
			}
			global.medios=archivos;
			let r="";
			for(let i=0;i<archivos.length;i++){
				r+="<div onclick='select("+i+")'>"+archivos[i]+"</div>";
			}
			res.send(tp.replace("xxxxx",r));
		});
	});
});
rutas.route("/select").get((req,res)=>{
	fs.readFile('./public/admin/datos/'+global.medios[req.query.n],(error,datos)=>{
		if(error){
			log(error+"");
			res.end('error');
			return;
		}
		global.sesion=eval('('+datos+')');
		global.idsesion=global.idsesion||0;
		global.idsesion++;
		global.hse={};
		global.listos={};
		for(let i=0;i<global.sesion.d.length;i++){
			global.hse[global.sesion.d[i].r]=global.sesion.d[i];
		}
		res.send({h:'Sesión:'+global.sesion.n,u:global.hse[-1].u,f:global.hse[-1].f,t:global.hse[-1].t});
	});
});
rutas.route("/r").get((req,res)=>{
	global.tr=req.query.tr;
	res.send(global.listos||{});
});
rutas.route("/time").post((req,res)=>{
	manual=new Date().getTime();
	res.send({respuesta:0});
});
rutas.route("/to").post((req,res)=>{
	req.body.t.ts=new Date().getTime();
	global.sina=req.body.t;
	res.send({respuesta:0});
});
rutas.route("/time").post((req,res)=>{
	manual=new Date().getTime();
	res.send({respuesta:0});
});
rutas.route("/time.html").get(async(req,res)=>{
	res.sendFile(path.join(__dirname,'public','admin/time.html'));
});
rutas.route("/index.html").get((req,res)=>{
	var pipx=pip;
	if(pip.indexOf(':')>=0){
		pipx="["+pip+"]";
	}
	const htmlContent=`
<body>
	<input type='text' value='http://${pipx}/cliente/time.html'/><br>
	<input type='text' value='http://${pipx}/cliente/o.html'/><br>
	<input type='text' value='http://${pipx}/cliente/main.html'/>
</body>
	`;
	res.setHeader('Content-Type','text/html');
	res.send(htmlContent);
});
module.exports=rutas;