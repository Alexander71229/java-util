function pro(o,con,zoom){
	for(let i=0;o.s&&i<o.s.length;i++){
		try{
			document.querySelector(o.s[i].s).innerHTML=o.s[i].h;
		}catch(e){
		}
	}
	let m=null;
	if(o.t=='a'){
		m=new Audio();
	}
	if(o.t=='v'){
		m=new Video();
	}
	m.src=o.u;
	if(zoom&&o.t=='a'){
		TimelinePlugin=WaveSurfer.Timeline;
		player=WaveSurfer.create({container:con,minPxPerSec:100,media:m,plugins:[TimelinePlugin.create()],interact:true});
		player.once('decode',()=>{
			const slider=document.querySelector(zoom);
			slider.addEventListener('input',(e)=>{
				player.zoom(e.target.valueAsNumber);
			});
		});
	}else{
		con=con||document.createElement('div');
		player=WaveSurfer.create({container:con,media:m,interact:false});
	}
	player.once('ready',lr);
}
function stop(){
	window.player&&player.stop();
}
function play(){
	player.play();
}
function seek(v){
	player.setTime(v/1000);
}
function getTime(){
	return player.getCurrentTime()*1000;
}