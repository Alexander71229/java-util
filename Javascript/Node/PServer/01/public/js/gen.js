function pet(m,u,o,f){
	fetch(u,{
		method:m,
		headers:{
			'Content-Type':'application/json'
		},
		body:o&&JSON.stringify(o)
	}).then(x=>x.json()).then(f);
}
function post(u,o,f){
	pet('POST',u,o,f);
}
function get(u,f){
	pet('GET',u,null,f);
}
