const express=require('express');
const mgdb=require('../BaseDeDatos/conexion.js');
const enrutador=express.Router();
enrutador.route("/").post((req,res)=>{
	res.send('{}');
});
enrutador.route("/").get((r,s)=>s.send("Respuesta al GET desde la ruta"));
enrutador.route("/").put();
enrutador.route("/").delete();
module.exports=enrutador;