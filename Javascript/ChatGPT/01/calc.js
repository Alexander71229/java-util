function evaluateOperation(operation) {
  // Define a map of operations and their corresponding functions
  const operations = {
    "+": (x, y) => x + y,
    "-": (x, y) => x - y,
    "*": (x, y) => x * y,
    "/": (x, y) => x / y
  };

  // Split the operation string into an array of numbers and operators
  const tokens = operation.split(/\s*([-+*/])\s*/);

  // Initialize the result to the first number in the array
  let result = parseFloat(tokens[0]);

  // Iterate over the remaining tokens, applying the corresponding operation
  // to the result and the next number in the array
  for (let i = 1; i < tokens.length; i += 2) {
    const operator = tokens[i];
    const number = parseFloat(tokens[i + 1]);
    result = operations[operator](result, number);
  }

  // Return the result of the operation
  return result;
}
