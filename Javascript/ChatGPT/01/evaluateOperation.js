function evaluateOperation(operation) {
  // Define a map of operations and their corresponding functions
  const operations = {
    "+": (x, y) => x + y,
    "-": (x, y) => x - y,
    "*": (x, y) => x * y,
    "/": (x, y) => x / y
  };

  // Define a map of operations and their corresponding precedence levels
  const precedence = {
    "+": 1,
    "-": 1,
    "*": 2,
    "/": 2
  };

  // Initialize an empty array of tokens
  const tokens = [];

  // Initialize a string to hold the current token
  let currentToken = "";

  // Iterate over the characters in the operation string
  for (let i = 0; i < operation.length; i++) {
    // Get the current character
    const character = operation[i];

    // If the character is a digit, add it to the current token
    if (/\d/.test(character)) {
      currentToken += character;
    } else {
      // If the current token is a number, push it to the array of tokens
      if (currentToken.length > 0) {
        tokens.push(parseFloat(currentToken));
        currentToken = "";
      }

      // If the character is an operator, push it to the array of tokens
      if (operations.hasOwnProperty(character)) {
        tokens.push(character);
      }
    }
  }

  // If the last token is a number, push it to the array of tokens
  if (currentToken.length > 0) {
    tokens.push(parseFloat(currentToken));
  }

  // Initialize an empty stack of numbers
  const numbers = [];

  // Initialize an empty stack of operators
  const operators = [];

  // Iterate over the tokens in the array
  for (let i = 0; i < tokens.length; i++) {
    const token = tokens[i];

    // If the token is a number, push it to the numbers stack
    if (typeof token === "number") {
      numbers.push(token);
    } else {
      // If the token is an operator, push it to the operators stack if
      // the stack is empty or if the operator has a higher precedence
      // than the top operator on the stack
      while (operators.length > 0 && precedence[token] <= precedence[operators[operators.length - 1]]) {
        const operator = operators.pop();
        const right = numbers.pop();
        const left = numbers.pop();
        numbers.push(operations[operator](left, right));
      }
      operators.push(token);
    }
  }

  // While there are still operators on the stack, pop the top operator and
  // the top two numbers from the numbers stack, and apply the operator to
  // the numbers, pushing the result back onto the numbers stack
  while (operators.length > 0) {
    const operator = operators.pop();
    const right = numbers.pop();
    const left = numbers.pop();
    numbers.push(operations[operator](left,
