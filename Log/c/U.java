package c;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors; 
import java.net.URL;
import java.text.*;
public class U{
	public static PrintStream ps;
	public static String ruta="."+File.separator;
	public static long k=0;
	public static String ft="yyyyMMdd-HH:mm:ss";
	public static File fc=null;
	public static String formato(Date d,String p){
		SimpleDateFormat sdf=new SimpleDateFormat(p);
		return sdf.format(d);
	}
	public static String formato(Date d){
		return formato(d,ft)+"|"+d.getTime()+"|";
	}
	public static String formato(){
		return formato(new Date());
	}
	public static String obtenerNombre(Date d){
		return formato(d,"yyyyMMdd")+".txt";
	}
	public static String calcularRuta(){
		String ruta="";
		try{
			Class c=U.class;
			String rutax=c.getName();
			File f=new File(c.getResource("/"+rutax.replaceAll("\\.","/")+".class").getFile());
			while(f!=null){
				if(f.getName().equals("WEB-INF")){
					f=f.getParentFile();
					break;
				}
				f=f.getParentFile();
			}
			ruta=f.getCanonicalPath()+File.separator;
		}catch(Throwable t){
			ruta="";
		}
		return ruta;
	}
	public static void init()throws Exception{
		if(ruta==null){
			ruta=calcularRuta();
		}
		File f=new File(ruta);
		if(!f.isDirectory()){
			if(ps==null){
				fc=f;
				ps=new PrintStream(new FileOutputStream(fc));
			}
		}else{
			Date d=new Date();
			if(ps==null){
				fc=new File(ruta+obtenerNombre(d));
				ps=new PrintStream(new FileOutputStream(fc));
				return;
			}
			long nk=(long)Math.floor((d.getTime()-18000000)/86400000.);
			if(nk!=k){
				k=nk;
				fc=new File(ruta+obtenerNombre(d));
				ps=new PrintStream(new FileOutputStream(fc));
			}
		}
	}
	public static void imp(Throwable t){
		try{
			init();
			ps.println(formato());
			t.printStackTrace(ps);
		}catch(Throwable t2){
		}
	}
	public static void imp(String m){
		try{
			if(m==null){
				return;
			}
			init();
			ps.println(formato()+m);
		}catch(Throwable t){
		}
	}
	public static void inp(String m){
		try{
			if(m==null){
				return;
			}
			init();
			ps.print(formato()+m);
		}catch(Throwable t){
		}
	}
	public static String g(String a){
		try{
			File f=new File(a);
			byte[]d=new byte[(int)f.length()];
			(new FileInputStream(f)).read(d);
			return new String(d);
		}catch(Throwable t){
		}
		return "";
	}
	public static String posicion(StackTraceElement s){
		return s.getClassName()+":"+s.getLineNumber();
	}
	public static String formatoTraza(StackTraceElement[]t,int a,int b){
		String s="";
		StringBuffer cad=new StringBuffer();
		for(int i=a;i<t.length&&i<b;i++){
			cad.append(s);
			cad.append(t[i].getClassName());
			cad.append(".");
			cad.append(t[i].getMethodName());
			cad.append(":");
			cad.append(t[i].getLineNumber());
			s=" ";
		}
		return cad+"";
	}
	public static String formatoTraza(Map.Entry<Thread,StackTraceElement[]>e,int a,int b){
		String f=formatoTraza(e.getValue(),a,b);
		if(f.length()==0){
			return "";
		}
		return e.getKey()+"->"+f;
		//return f;
	}
	public static String traza(int a,int b){
		return formatoTraza(Thread.currentThread().getStackTrace(),a,b);
	}
	public static void traza(){
		imp(traza(3,10000));
	}
	public static void traza(String m){
		imp(traza(3,10000)+"\n"+m);
	}
	public static boolean validar(StackTraceElement[]t){
		for(int i=0;i<t.length;i++){
			if(t[i].getClassName().indexOf("comunes")>=0){
				return true;
			}
		}
		return false;
	}
	public static String trazas(Map<Thread,StackTraceElement[]>t,int a,int b){
		return t.entrySet().stream().filter(e->validar(e.getValue())).map(e->formatoTraza(e,a,b)).filter(e->e.length()>5).collect(Collectors.joining("\n"));
	}
	public static void trazas(){
		Map<Thread,StackTraceElement[]>t=Thread.getAllStackTraces();
		imp(trazas(t,0,10000));
	}
	public static Thread trazador(){
		Thread t=new Thread(()->{
			String ta="";
			while(true){
				String s=trazas(Thread.getAllStackTraces(),0,10000);
				if(!s.equals(ta)){
					imp(s);
					ta=s;
				}
			}
		});
		return t;
	}
	public static String primeraMayuscula(String c){
		return c.substring(0,1).toUpperCase()+c.substring(1);
	}
	public static String valoresCampos(Object o){
		return Arrays.stream(o.getClass().getDeclaredFields()).map(c->{
			try{
				return c.getName()+"="+o.getClass().getMethod("get"+primeraMayuscula(c.getName())).invoke(o);
			}catch(Throwable t){
				return "";
			}
		}).collect(Collectors.joining(";"));
	}
	public static URL jar(Class clase){
		try{
			String ruta=clase.getName();
			return clase.getResource("/"+ruta.replaceAll("\\.","/")+".class");
		}catch(Throwable t){
			return null;
		}
	}
	public static String listaMetodos(Class c){
		return Arrays.stream(c.getMethods()).map(m->m+"").collect(Collectors.joining("\n"));
	}
	public static void imp(Object o){
		imp(valoresCampos(o));
	}
}