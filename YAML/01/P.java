import java.util.*;
public class P{
	public String t;
	public int p;
	public ArrayList<P>h;
	public HashMap<String,P>d;
	public String v;
	public int l;
	public P(int p,String t,int l){
		h=new ArrayList<>();
		this.t=t;
		this.p=p;
		this.v="";
		this.l=l;
	}
	public void act(){
		if(d!=null){
			return;
		}
		d=new HashMap<>();
		for(int i=0;i<h.size();i++){
			d.put(h.get(i).t,h.get(i));
		}
	}
	public String toString(){
		return t+"["+l+":"+p+"]:"+v;
	}
	public void act(String clave){
		act();
		String[]ks=clave.split("\\.");
		P z=d.get(ks[0]);
		z.act();
		for(int i=1;i<ks.length;i++){
			z=z.d.get(ks[i]);
			z.act();
		}
	}
	public P val(String clave){
		act();
		String[]ks=clave.split("\\.");
		P z=d.get(ks[0]);
		z.act();
		for(int i=1;i<ks.length;i++){
			z=z.d.get(ks[i]);
			z.act();
		}
		return z;
	}
	public static P b(ArrayList<P>z,String n){
		for(int i=0;i<z.size();i++){
			if(z.get(i).t.equals(n)){
				return z.get(i);
			}
		}
		return null;
	}
}
