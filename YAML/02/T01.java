import java.util.*;
import java.io.*;
class Z{
	public String t;
	public String n;
	public String j;
	public int mn;
	public int mx;
	public boolean o;
	public Z(String t,String n,String j){
		this.t=t;
		this.n=n;
		this.j=j;
	}
	public String toString(){
		StringBuilder r=new StringBuilder();
		r.append("\t@JsonProperty(\""+j+"\")\n");
		if(o){
			r.append("\t@NotNull(message=\""+j+" cannot be null\")\n");
		}
		if(mx>0){
			r.append("\t@Size(min="+mn+",max="+mx+",message=\"The length of typeAccount must be between "+mn+" and "+mx+"\")\n");
		}
		r.append("\tprivate ");
		if(t.endsWith(">")){
			r.append(t+""+n+";\n");
		}else{
			r.append(t+" "+n+";\n");
		}
		return r+"";
	}
}
class C{
	public String p;
	public String n;
	public ArrayList<String>is;
	public ArrayList<Z>cs;
	public HashMap<String,Integer>hi;
	public C(String p,String n){
		this.p=p;
		this.n=n;
		is=new ArrayList<>();
		cs=new ArrayList<>();
		hi=new HashMap<>();
	}
	public Z add(String t,String n,String j){
		cs.add(new Z(t,n,j));
		return cs.get(cs.size()-1);
	}
	public void ai(String v){
		if(hi.get(v)==null){
			is.add(v);
			hi.put(v,1);
		}
	}
	public String toString(){
		StringBuilder r=new StringBuilder();
		r.append("package ");
		r.append(p);
		r.append(";\n");
		for(int i=0;i<is.size();i++){
			r.append("import ");
			r.append(is.get(i));
			r.append(";\n");
		}
		r.append("@lombok.Data\npublic class ");
		r.append(n);
		r.append("{\n");
		for(int i=0;i<cs.size();i++){
			r.append(cs.get(i)+"");
		}
		r.append("}\n");
		return r+"";
	}
	public void e(String d)throws Exception{
		PrintStream ps=new PrintStream(new FileOutputStream(new File(d+this.n+".java")));
		ps.println(this+"");
	}
}
public class T01{
	public static ArrayList<String>tipos;
	public static HashMap<String,String>htipos;
	public static String pk;
	public static String des;
	public static boolean val=true;
	public static ArrayList<C>cs;
	public static HashMap<String,C>hcs;
	public static String uc(String v){
		return v.substring(0,1).toUpperCase()+v.substring(1);
	}
	public static String ul(String v){
		return v.substring(0,1).toLowerCase()+v.substring(1);
	}
	public static C cc(String n){
		hcs.put(n,new C(pk,n));
		cs.add(hcs.get(n));
		hcs.get(n).ai("com.fasterxml.jackson.annotation.JsonProperty");
		return hcs.get(n);
	}
	public static void g(File f)throws Exception{
		c.U.imp(des);
		tipos=new ArrayList<>();
		tipos.add("string");
		tipos.add("boolean");
		tipos.add("integer");
		htipos=new HashMap<>();
		htipos.put("string","String");
		htipos.put("boolean","Boolean");
		htipos.put("integer","Integer");
		cs=new ArrayList<>();
		hcs=new HashMap<>();
		Scanner s=new Scanner(f);
		String a=null;
		Stack<C>kc=new Stack<>();
		Z ta=null;
		while(s.hasNext()){
			String x=s.next();
			if(x.startsWith("}")){
				kc.pop();
			}
			if(x.equals("minLength:")&&val){
				ta.mn=Integer.parseInt(s.next());
				continue;
			}
			if(x.equals("maxLength:")&&val){
				kc.peek().ai("javax.validation.constraints.Size");
				ta.mx=Integer.parseInt(s.next());
				continue;
			}
			if(x.endsWith("{")){
				String nc=null;
				if(a==null){
					nc=x.replaceAll("\\[","").replaceAll("\\{","");
				}else{
					nc=a.replaceAll("\\*","");
				}
				if(kc.size()>0){
					if(x.startsWith("[")){
						ta=kc.peek().add("List<"+nc+">",ul(nc),nc);
						kc.peek().ai("java.util.List");
					}else{
						ta=kc.peek().add(nc,ul(nc),nc);
					}
					if((a==null||a.endsWith("*"))&&val){
						kc.peek().ai("javax.validation.constraints.NotNull");
						ta.o=true;
					}
				}
				kc.push(cc(nc));
				a=x;
				continue;
			}
			for(int i=0;i<tipos.size();i++){
				if(x.indexOf(tipos.get(i))>=0){
					String nc=a.replaceAll("\\*","");
					if(x.startsWith("[")){
						ta=kc.peek().add("List<"+htipos.get(tipos.get(i))+">",ul(nc),nc);
					}else{
						ta=kc.peek().add(htipos.get(tipos.get(i)),ul(nc),nc);
					}
					if((a==null||a.endsWith("*"))&&val){
						kc.peek().ai("javax.validation.constraints.NotNull");
						ta.o=true;
					}
					break;
				}
			}
			a=x;
			continue;
		}
		for(int i=0;i<cs.size();i++){
			cs.get(i).e(des);
		}
	}
	public static void main(String[]argumentos){
		try{
			c.U.ruta="Log.txt";
			c.U.imp("Inicio");
			pk="co.com.bancolombia.model.accounts.request";
			des="C:\\Desarrollo\\Proyectos\\Bancolombia\\Desarrollo\\git\\HA_3933351\\NU0448001_InterAccounts_MR\\ms_commerce_registry\\domain\\model\\src\\main\\java\\co\\com\\bancolombia\\model\\accounts\\request\\";
			val=false;
			g(new File("C:\\Desarrollo\\Proyectos\\Bancolombia\\Datos\\BelongingRequest"));
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}