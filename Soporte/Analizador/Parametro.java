public class Parametro{
	private String nombre;
	private String modo;
	private String tipo;
	public String getNombre(){
		return this.nombre;
	}
	public String getModo(){
		return this.modo;
	}
	public String getTipo(){
		return this.tipo;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public void setModo(String modo){
		this.modo=modo;
	}
	public void setTipo(String tipo){
		this.tipo=tipo;
	}
	public void addModo(String modo){
		this.modo+=modo;
	}
	public void addTipo(String tipo){
		this.tipo+=tipo;
	}
	public String toString(){
		return nombre+" "+modo+" "+tipo;
	}
	public void validar(){
	}
}