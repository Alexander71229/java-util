import java.io.*;
import java.util.*;
public class Analizador{
	public Scanner fuente;
	public Analizador(File archivo) throws Exception{
		fuente=new Scanner(archivo);
	}
	public void formatear(){
		String texto="";
		while(fuente.hasNext()){
			String linea=fuente.nextLine();
			int ind=linea.indexOf("--");
			if(ind>=0){
				linea=linea.substring(0,ind);
			}
			texto+=" "+linea;
		}
		texto=texto.replace("("," ( ");
		texto=texto.replace(")"," ) ");
		texto=texto.replace(";"," ; ");
		texto=texto.replace(","," , ");
		texto=texto.replace("+"," + ");
		texto=texto.replace("*"," * ");
		texto=texto.replace("%"," % ");
		fuente=new Scanner(texto);
	}
	public Programa analizar(){
		formatear();
		Programa programa=new Programa();
		Stack<String>ubicacion=new Stack<String>();
		Stack objetos=new Stack();
		ArrayList<Parametro>listaParametros=null;
		Parametro parametro=null;
		HashMap<String,Declaracion>declaraciones=null;
		Declaracion declaracion=null;
		Cursor cursor=null;
		Consulta consulta=null;
		CampoConsulta campoConsulta=null;
		Expresion expresion=null;
		boolean analizado=true;
		String token="";
		String segmentoCodigo="";
		int k=0;
		while(fuente.hasNext()){
			if(k>10){
				U.imp("Error:"+token);
				U.imp(ubicacion.peek());
				System.exit(0);
			}
			if(analizado){
				token=fuente.next().toUpperCase();
				analizado=false;
				k=0;
			}
			k++;
			U.imp(token);
			if(token.equals("CREATE")){
				ubicacion.push("DIRECTIVA");
				analizado=true;
				continue;
			}
			if(token.equals("OR")){
				if(ubicacion.peek().equals("DIRECTIVA")){
					fuente.next().toUpperCase();
				}
				analizado=true;
				continue;
			}
			if(token.equals("PROCEDURE")){
				Procedimiento procedimiento=new Procedimiento();
				programa.agregar(procedimiento);
				objetos.push(procedimiento);
				declaraciones=procedimiento.declaraciones;
				procedimiento.nombre=fuente.next().toUpperCase();
				fuente.next().toUpperCase(); //(
				ubicacion.push("EXCEPCIONES");
				ubicacion.push("CUERPO");
				ubicacion.push("DECLARACIONES");
				ubicacion.push("LISTA PARAMETROS");
				listaParametros=procedimiento.parametros;
				analizado=true;
				continue;
			}
			if(ubicacion.peek().equals("LISTA PARAMETROS")){
				if(token.equals(")")){
					ubicacion.pop();
					analizado=true;
				}else{
					parametro=new Parametro();
					listaParametros.add(parametro);
					parametro.setNombre(token);
					parametro.setModo(fuente.next().toUpperCase());
					String seg=fuente.next().toUpperCase();
					if(seg.equals("OUT")){
						parametro.addModo(" OUT");
						parametro.setTipo("");
					}else{
						parametro.setTipo(seg);
					}
					token=fuente.next().toUpperCase();
					while(true){
						parametro.addTipo(token);
						token=fuente.next().toUpperCase();
						if(token.equals(")")){
							break;
						}
						if(token.equals(",")){
							break;
						}
					}
					if(token.equals(",")){
						analizado=true;
					}else{
						analizado=false;
					}
				}
				continue;
			}
			if(ubicacion.peek().equals("DECLARACIONES")){
				if(token.equals("IS")){
					analizado=true;
					continue;
				}
				if(token.equals("CURSOR")){
					declaracion=new Declaracion();
					objetos.push(declaracion);
					declaracion.setNombre(fuente.next().toUpperCase());
					cursor=new Cursor(declaracion.getNombre());
					declaracion.setDeclaracion(cursor);
					declaraciones.put(declaracion.getNombre(),declaracion);
					ubicacion.push("CURSOR");
					ubicacion.push("CONSULTA CURSOR");
					ubicacion.push("LISTA PARAMETROS");
					listaParametros=cursor.parametros;
					analizado=true;
					continue;
				}
				if(token.equals("TYPE")){
					declaracion=new Declaracion();
					objetos.push(declaracion);
					declaracion.setNombre(fuente.next().toUpperCase());
					declaraciones.put(declaracion.getNombre(),declaracion);
					ubicacion.push("DECLARACION");
					analizado=true;
					continue;
				}
				if(token.equals("FUNCTION")){
					Procedimiento procedimiento=new Procedimiento();
					programa.agregar(procedimiento);
					objetos.push(procedimiento);
					declaraciones=procedimiento.declaraciones;
					procedimiento.nombre=fuente.next().toUpperCase();
					fuente.next().toUpperCase(); //(
					ubicacion.push("EXCEPCIONES");
					ubicacion.push("CUERPO");
					ubicacion.push("DECLARACIONES");
					ubicacion.push("LISTA PARAMETROS");
					listaParametros=procedimiento.parametros;
					U.imp("Error función");
					System.exit(0);
					analizado=true;
					continue;
				}
				declaracion=new Declaracion();
				objetos.push(declaracion);
				declaracion.setNombre(token);
				declaraciones.put(declaracion.getNombre(),declaracion);
				ubicacion.push("DECLARACION");
				analizado=true;
				continue;
			}
			if(ubicacion.peek().equals("DECLARACION")){
				if(token.equals("IS")){
					analizado=true;
					continue;
				}else{
					if(token.equals(";")){
						ubicacion.pop();
						objetos.pop();
						analizado=true;
						continue;
					}else{
						declaracion.addTipo(token);
						analizado=true;
						continue;
					}
				}
			}
			if(ubicacion.peek().equals("CONSULTA CURSOR")){
				if(token.equals("IS")){
					cursor.consulta=new Consulta();
					consulta=cursor.consulta;
					consulta.sql="";
					analizado=true;
					continue;
				}
				/*if(token.equals("SELECT")){
					consulta=new Consulta();
					objetos.push(consulta);
					ubicacion.push("CAMPOS CONSULTA");
					consulta.sql=token;
					analizado=true;
					continue;
				}*/
				if(token.equals(";")){
					ubicacion.pop();
					ubicacion.pop();
					objetos.pop();
					analizado=true;
					continue;
				}
				consulta.sql+=" "+token;
				analizado=true;
			}
			if(ubicacion.peek().equals("CAMPOS CONSULTA")){
				if(token.equals("(")){
					U.imp("Sub consulta");
					System.exit(0);
					analizado=true;
					continue;
				}
				campoConsulta=new CampoConsulta();
				ubicacion.push("CAMPO CONSULTA");
				objetos.push(campoConsulta);
				ubicacion.push("EXPRESION");
				expresion=new Expresion(token);
				objetos.push(expresion);
				analizado=true;
				U.imp(ubicacion.peek()+"<--->"+expresion);
				continue;
			}
			if(ubicacion.peek().equals("EXPRESION")){
				if(token.equals("FROM")){
					U.imp("Lista tablas");
					System.exit(0);
					analizado=true;
					continue;
				}
				if(token.equals(",")){
					objetos.pop();
					ubicacion.pop();
					ubicacion.pop();
					campoConsulta.expresion=expresion;
					analizado=true;
					continue;
				}
				if(token.equals("+")||token.equals("*")){
					if(expresion.operador==null&&expresion.valor1!=null){
						expresion.operador=token;
					}else{
						expresion=new Expresion(expresion,token);
					}
					analizado=true;
					continue;
				}
				if(token.equals("(")){
					continue;
				}
				if(token.equals(")")){
					continue;
				}
				if(expresion.valor1!=null&&expresion.valor2==null&&expresion.operador!=null){
					expresion.valor2=new Expresion(token);
					U.imp("BIN:"+expresion);
					System.exit(0);
					analizado=true;
					continue;
				}
				U.imp("DEF:"+expresion);
				System.exit(0);
			}
		}
		return programa;
	}
}