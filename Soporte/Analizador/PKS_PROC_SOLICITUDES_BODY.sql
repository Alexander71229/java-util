create or replace PACKAGE BODY     PKS_PROC_SOLICITUDES AS

  -- Author  : Alberto Eduardo Sanchez B.
  -- Created : 24/03/2004 11:57:07 a.m.
  -- Purpose : Utilidades y procedimientos del modulo de procesamiento de solicitudes:
  --
  -- HISTORIAL DE MODIFICACIONES
  -- Versión                  : 1.0
  -- Fecha                  : 24/03/2004 11:57:07 a.m.
  -- Persona                  : Alberto Eduardo Sanchez B.
  -- Descripcion de la modificacion     : Creación del Package Body

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 12:00:00 p.m.
  -- Este procedimiento actualiza la cuotas de afiliacion

  GrAseServicioCre RAseServicioCre;
  nIncluyeSegSocial SIMPLE_INTEGER := 0;--40784 JNARANJO 20160412  
  nRedondeoNivel PLS_INTEGER := NVL(pk_conseres.param('G_REDONDEO_NIVEL'),38);--41208 JNARANJO 20170307

 /***************************************************************************************************
  ESPECIFICACIONES PRIVADAS
  ****************************************************************************************************/

  FUNCTION AplazadaBPM (niConsAsesoria  IN NUMBER ) RETURN VARCHAR2 IS
    
    CURSOR cUltimoEstado IS
    SELECT ct.codigo
     FROM ts_ase_decisiones adec, 
          ts_ase_servicios aservic, 
          con_tipos ct
    WHERE adec.aservic_consecutivo_servicio = aservic.consecutivo_servicio
      AND adec.tipo_estado_decision = ct.sec
      AND aservic.ase_consecutivo_asesoria = niConsAsesoria
      AND adec.fecha_final IS NULL;
    vUltimoEstado VARCHAR2(10);
        
    CURSOR cAnteriorEstado IS
    SELECT ct.codigo
     FROM ts_ase_decisiones adec, 
          ts_ase_servicios aservic, 
          con_tipos ct
    WHERE adec.aservic_consecutivo_servicio = aservic.consecutivo_servicio
      AND adec.tipo_estado_decision = ct.sec
      AND aservic.ase_consecutivo_asesoria = niConsAsesoria
      AND adec.fecha_final IS NOT NULL
    ORDER BY adec.fecha_inicial DESC;   
    vAnteriorEstado VARCHAR2(10);
    -- 43054 JMarquez 20161005 (A) Se permite cuando la instancia esta en una tarea asignada al asesor comercial
    CURSOR cInstanciaAsesor IS
      SELECT 'SI'
        FROM ts_ase_bpm_procesos abpm, ts_gpc_instancia_datos gpc
       WHERE abpm.ase_consecutivo_asesoria = niConsAsesoria 
         AND abpm.fecha_final IS NULL
         AND gpc.ginsta_instancia = abpm.id_proceso
         AND gpc.estado = 'ASIGNA'
         AND gpc.responsable = 'G_ASESOR_COMERCIAL';
    -- FIN 43054 JMarquez 20161005 (A)
        
    vAplazada VARCHAR2(10) := 'NO';
  BEGIN
    -- Obtiene el ultimo estado
     OPEN cUltimoEstado;
    FETCH cUltimoEstado INTO vUltimoEstado;
    CLOSE cUltimoEstado;
    -- El ultimo estado debe ser "2.01 - Servicio en estudio"
    IF vUltimoEstado = '2.01' THEN
        OPEN cAnteriorEstado;
       FETCH cAnteriorEstado INTO vAnteriorEstado;
       CLOSE cAnteriorEstado;
       -- El anterior estado debe ser "2.12 - Servicio aplazado"
       IF vAnteriorEstado = '2.12' THEN
          vAplazada := 'SI';
       END IF;
    END IF;
    -- 43054 JMarquez 20161005 (A) Se permite cuando la instancia esta en una tarea asignada al asesor comercial
    IF vAplazada = 'NO' THEN
      OPEN cInstanciaAsesor;
      FETCH cInstanciaAsesor INTO vAplazada;
      CLOSE cInstanciaAsesor;
    END IF;
    -- FIN 43054 JMarquez 20161005 (A)
    RETURN vAplazada;
  END AplazadaBPM;


  --40784 JNARANJO 20160412
  FUNCTION IncluyeSegSocial( niCnv IN NUMBER ) RETURN VARCHAR2 IS
    CURSOR cCnv IS
      SELECT DECODE(NVL(Cd.Incluye_Seg_Social,'SI'),'SI',0,-1)
      FROM ts_cnv_deducciones Cd
      WHERE SYSDATE BETWEEN Cd.Fecha_Inicio AND NVL(Cd.Fecha_Final, SYSDATE) AND Cd.Con_Id = niCnv;
    vIncSegSocial VARCHAR2(2);
  BEGIN
    OPEN cCnv;
    FETCH cCnv INTO vIncSegSocial;
    CLOSE cCnv;
    RETURN(vIncSegSocial);
  END IncluyeSegSocial;
  
  --40784 JNARANJO 20160412 Para poder asignar desde forms
  PROCEDURE SetnIncluyeSegSocial( niValor IN NUMBER ) IS    
  BEGIN
    nIncluyeSegSocial := niValor;
  END SetnIncluyeSegSocial;  
  
  FUNCTION calcSeguridadSocial( niPerId IN NUMBER,
                                niConsServicio IN NUMBER,
                                diFechaInicial IN DATE ) RETURN NUMBER IS

  CURSOR cAperson IS
    -- 40784 JMarquez 20160330 (M) Agregar otros ingresos
    --SELECT aperson.ingresos_basicos ingresos_basicos,ase.aforpag_nombre aforpag_nombre,ase.atipper_pago atipper_pago,
    SELECT aperson.ingresos_basicos ing_deduccion_ley, (aperson.ingresos_basicos + nvl(aperson.otro_ingresos,0)) ingresos_basicos,ase.aforpag_nombre aforpag_nombre,ase.atipper_pago atipper_pago,  
           aservic.per_id perIdServicio,    
           aperson.atipper_nombre attiperAperson,--41208 JNARANJO 20170405
    --- Fin 40784 JMarquez 20160330 (M)
           ( SELECT pperocu.con_deduccion_nomina FROM ts_per_persona_ocupaciones pperocu WHERE pperocu.id = aservic.pperocu_id ) cnvId,
           aperson.tipo_vivienda,aperson.tipo_ubicacion,aperson.posee_vehiculo,aperson.estado_civil,aperson.soporte_vivienda,aperson.modelo,
           --41208 JNARANJO 20170331
           --(SELECT pperocu.atipocu_codigo FROM ts_per_persona_ocupaciones pperocu WHERE pperocu.id = aservic.pperocu_id) tipoOcupacion           
           
           DECODE( aperson.aperson_type,'SOLICI',(SELECT pperocu.atipocu_codigo FROM ts_per_persona_ocupaciones pperocu WHERE pperocu.id = aservic.pperocu_id)
                                                ,(SELECT MIN(pperocu.atipocu_codigo) FROM ts_per_persona_ocupaciones pperocu WHERE pperocu.per_id = niPerId AND NVL(pperocu.fecha_final,SYSDATE) >= SYSDATE) ) tipoOcupacion
           --41208 JNARANJO 20170331
    FROM ts_ase_personas aperson,ts_ase_servicios aservic,ts_asesorias ase
    WHERE aperson.aservic_consecutivo_servicio = aservic.consecutivo_servicio
      AND aservic.ase_consecutivo_asesoria = ase.consecutivo_asesoria
      AND aperson.per_id = niPerId
      AND aperson.aservic_consecutivo_servicio = niConsServicio
      AND aperson.fecha_inicial = diFechaInicial;
  rAperson cAperson%ROWTYPE;
  --Concepto para el manejo de los egresos de las ocupaciones
  CURSOR cConceptoOcupaciones ( diFechaVig IN DATE,
                                niTipoOcu IN NUMBER,
                                niValorIng IN NUMBER ) IS
    SELECT aegrocu.tipo,aegrocu.valor
    FROM ts_adm_egreso_ocupaciones aegrocu
    WHERE aegrocu.aingvig_fecha_inicial = diFechaVig
      AND aegrocu.atipocu_codigo = niTipoOcu
      AND ROUND(niValorIng,2) BETWEEN aegrocu.valor_inicial AND aegrocu.valor_final;
    --Tipo de ingreso según ingresos básicos
  CURSOR cConceptoGastos ( niValorIng IN NUMBER ) IS
    SELECT aingvig.fecha_inicial,aingval.tipo_ingreso
    FROM ts_adm_ingreso_vigencias aingvig,
         ts_adm_ingreso_valores aingval
    WHERE aingvig.fecha_inicial = aingval.aingvig_fecha_inicial
      AND SYSDATE BETWEEN aingvig.fecha_inicial AND NVL(aingvig.fecha_final,SYSDATE + 1)
      AND ROUND(niValorIng,2) BETWEEN aingval.valor_inicial AND aingval.valor_final;      
    vTipoParametro VARCHAR2(10);
    nVrParametro NUMBER; 
    nVrDedLey NUMBER := 0; 
    dFecVigencia DATE;  
    nTipoIng NUMBER;
    nVrSalarioConvertido NUMBER;--Valor del salario convertido al período de pago indicado
    nVrSMMLV NUMBER := NVL(Pk_Conseres.Param_Gnral('GSMLMV'),0);
    eError EXCEPTION;--Error al llamar procedimiento
    vMsjUsu VARCHAR2(200);
    vMsjTec VARCHAR2(1000);
    bExito BOOLEAN;
  BEGIN
    OPEN cAperson;
    FETCH cAperson INTO rAperson;
    CLOSE cAperson;
    rastro('niPerId:'||niPerId||' niPerIdAperson:'||rAperson.perIdServicio||' niConsServicio:'||niConsServicio||' diFechaInicial:'||diFechaInicial,'DEDLEYNOM');
    --41208 JNARANJO 20170405
    /*IF niPerId != rAperson.perIdServicio THEN
      RETURN 0;
    END IF;*/
    --41208 JNARANJO 20170405 Fin modificación
    --41208 JNARANJO 20170327
    IF rAperson.perIdServicio = niPerId THEN    
      nVrSalarioConvertido := NVL(ROUND(pks_cpl0031.convertir_valor(rAperson.ing_deduccion_ley,rAperson.atipper_pago,'MENSUAL')),0);
    ELSE
      nVrSalarioConvertido := NVL(ROUND(pks_cpl0031.convertir_valor(rAperson.ing_deduccion_ley,rAperson.attiperAperson,'MENSUAL')),0);    
    END IF;
    OPEN cConceptoGastos(ROUND((nVrSalarioConvertido/nVrSMMLV),2));
    FETCH cConceptoGastos INTO dFecVigencia,nTipoIng;
    CLOSE cConceptoGastos;
    --Para conceptos según el tipo de ocupación: Deducciones de Ley
    OPEN cConceptoOcupaciones( dFecVigencia,
                               rAperson.tipoOcupacion,
                               (NVL(nVrSalarioConvertido,0)/nVrSMMLV) );--61782 JMarquez 20200212 (E) eliminación de redondeo
    FETCH cConceptoOcupaciones INTO vTipoParametro,nVrParametro;
    CLOSE cConceptoOcupaciones;
    IF vTipoParametro IS NULL THEN
      rastro('DEDLEY atipocu_codigo:'||rAperson.tipoOcupacion||'  salario:'||NVL(nVrSalarioConvertido,0)/nVrSMMLV,'GASTOS');
      vMsjUsu := 'No existe configuración para el concepto de Deducciones de Ley.';
      RAISE eError;
    END IF;
    --rConceptos.nombre := 'DEDUCCIONES DE LEY';
    IF vTipoParametro = 'VALFIJ' THEN
      nVrDedLey := nVrParametro;
      --nVrDedLey := NVL(ROUND(pks_cpl0031.convertir_valor(nVrParametro,'MENSUAL',rAperson.atipper_pago)),0);
    ELSIF vTipoParametro = 'PORCEN' THEN     
      nVrDedLey := ROUND(rAperson.ing_deduccion_ley*nVrParametro/100);
    ELSE
      vMsjUsu := 'No existe manejo para configuración de tipo:'||vTipoParametro||' .';
      RAISE eError;
    END IF;
/*    --41208 JNARANJO 20170327
    IF rAperson.perIdServicio != niPerId THEN
      nVrDedLey := 0;
    END IF;
    --41208 JNARANJO 20170327 Fin modificación*/
    RETURN nVrDedLey;    
EXCEPTION
  WHEN eError THEN
       Pks_Generales.Case_Exception( 'pks_proc_solicitudes.calcSeguridadSocial',
                                     vMsjUsu||'Error al calcular las deducciones de ley para niPerId:'||niPerId||'  servicio:'||niConsServicio,
                                     SQLCODE,
                                     vMsjTec,
                                     bExito );
       RAISE_APPLICATION_ERROR(-20751,'Error al calcular las deducciones de ley, ver log.'); 
  WHEN OTHERS THEN
       Pks_Generales.Case_Exception( 'pks_proc_solicitudes.calcSeguridadSocial',
                                     'Error al calcular las deducciones de ley para niPerId:'||niPerId||'  servicio:'||niConsServicio,
                                     SQLCODE,
                                     vMsjTec,
                                     bExito ); 
       RAISE_APPLICATION_ERROR(-20751,'Error al calcular las deducciones de ley, ver log.');                                       
  END;

  /*******************************************************************************
  ESPECIFICACIONES PUBLICAS
  *******************************************************************************/  
  PROCEDURE ActualizarCuotasAfiliacion(ViSerAfi   IN VARCHAR2,
                                       NiCuota    IN NUMBER,
                                       NiAsesoria IN NUMBER,
                                       VoMensaje  OUT VARCHAR2) IS

  BEGIN
    BEGIN
      UPDATE TS_ASE_SERVICIOS Ase
         SET Ase.Cuota_Periodo_Pago = NiCuota
       WHERE Ase.Ase_Consecutivo_Asesoria = NiAsesoria AND
             Ase.Ser_Codigo = ViSerAfi;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        VoMensaje := 'No se encontro el registro ';
      WHEN OTHERS THEN
        VoMensaje := 'Error:  ' || SQLERRM;
    END;
  END ActualizarCuotasAfiliacion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 27/04/2004 08:30:00 a.m.
  -- Actualiza estado servicios asesorias

  PROCEDURE ActEstServicios(NiConsec_Asesor IN TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE,
                            VoMsjtec        OUT VARCHAR2,
                            VoMsjusu        OUT VARCHAR2,
                            BoExito         OUT BOOLEAN) IS
    CURSOR CServicios IS
      SELECT Aservic.Consecutivo_Servicio, Aservic.Tipo_Estado_Decision,
             Aservic.Ser_Codigo, Ase.Tipo_Asesoria, Aservic.Pperocu_Id
        FROM TS_ASE_SERVICIOS Aservic, TS_ASESORIAS Ase
       WHERE Ase.Consecutivo_Asesoria(+) = Aservic.Ase_Consecutivo_Asesoria AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             (Aservic.Tipo_Estado_Decision IS NULL OR
              Aservic.Tipo_Estado_Decision =
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.12'));

    --41822 JNARANJO 20160621
    nEstAplazado PLS_INTEGER := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.12');
    CURSOR cAuxilio IS
      SELECT 'SI' existe
      FROM ts_ase_servicios aservic, ts_asesorias ase
       WHERE ase.consecutivo_asesoria(+) = aservic.ase_consecutivo_asesoria 
         AND aservic.ase_consecutivo_asesoria = NiConsec_Asesor 
         AND ( aservic.tipo_estado_decision IS NULL OR aservic.tipo_estado_decision = nEstAplazado)
         AND NOT EXISTS ( SELECT 1
                          FROM ts_ase_servicios s2
                          WHERE s2.ase_consecutivo_asesoria = NiConsec_Asesor
                            AND s2.ser_codigo != '001.01.00.002' )     
         AND EXISTS ( SELECT 1
                      FROM ts_ase_servicios s2
                      WHERE s2.ase_consecutivo_asesoria = NiConsec_Asesor
                        AND s2.ser_codigo = '001.01.00.002' );  
    vExisteAuxilio VARCHAR2(2) := 'NO';
    --41822 JNARANJO 20160621
    /*
    CURSOR CRecaudoTercero IS
      SELECT '2.08' Estado
        FROM TS_ASESORIAS Ase
       WHERE Ase.Consecutivo_Asesoria = NiConsec_Asesor AND NOT EXISTS
       (SELECT 1
                FROM TS_ASE_SERVICIOS Aservic
               WHERE Aservic.Ase_Consecutivo_Asesoria =
                     Ase.Consecutivo_Asesoria AND
                     Aservic.Ser_Codigo NOT LIKE '001.04%');
*/
    VEstDecision VARCHAR2(5); -- Utilizada para asingar el estado de decision a asignar
  BEGIN
    --41822 JNARANJO 20160621
    OPEN cAuxilio;
    FETCH cAuxilio INTO vExisteAuxilio;
    CLOSE cAuxilio;
    --41822 JNARANJO 20160621 Fin mod
    FOR CSer IN CServicios LOOP
      VEstDecision := '2.01';     
      --41822 JNARANJO 20160621
      --IF CSer.Ser_Codigo LIKE '001.04%' THEN
      IF CSer.Ser_Codigo LIKE '001.04%' OR NVL(vExisteAuxilio,'NO') = 'SI' THEN  
      --41822 JNARANJO 20160621 Fin mod
        --
        -- Req. 14539
        -- Fecha modificación : 11/07/2006 09:39:53 a.m.
        -- Usuario modifica   : Alberto Eduardo Sánchez B.
        -- Causa modificación :  Cpl0068 "Radicar solicitud definitiva", en el cual se especifica que al momento de radicar
        -- una solicitud debe quedar en estado "EN ESTUDIO" a excepción de varios casos,
        -- entre ellos los servicios de Recaudo a terceros los cuales deben quedar en estado "EFECTIVO" (numeral 4 c)).
        -- OPEN CRecaudoTercero;
        -- FETCH CRecaudoTercero
        --  INTO VEstDecision;
        -- CLOSE CRecaudoTercero;
        VEstDecision := '2.08';
        --
        -- Fin Req. 14539

      ELSIF CSer.Tipo_Asesoria = 'ROTATI' THEN
        VEstDecision := '2.24';
      END IF;

      IF CSer.Tipo_Estado_Decision IS NULL OR
         CSer.Tipo_Estado_Decision =
         Pk_Conseres.Sec_Tipo_Codigo(1264, '2.12') THEN

        UPDATE TS_ASE_SERVICIOS
           SET Tipo_Estado_Decision = Pk_Conseres.Sec_Tipo_Codigo(1264,
                                                                   VEstDecision)
         WHERE Consecutivo_Servicio = CSer.Consecutivo_Servicio;
        ActEstDecision(CSer.Consecutivo_Servicio,
                       VEstDecision,
                       NULL,
                       NULL,
                       VoMsjtec,
                       VoMsjusu,
                       BoExito,
                       'NO');
      END IF;
    END LOOP;

    IF BoExito THEN
      UPDATE TS_ASE_DOCUMENTOS Adocumen
         SET Recibido_Asesor = 'SI', Fecha_Recepcion_Asesor = SYSDATE
       WHERE NVL(Adocumen.Excluir, 'NO') = 'NO' AND
             Adocumen.Ase_Consecutivo_Asesoria = NiConsec_Asesor;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.ACTESTSERVICIOS',
                                       'Error al actualizar el estado de servicios ',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presentó problemas actualizando los estados de servicio';
  END ActEstServicios;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 27/04/2004 08:30:00 a.m.
  -- Actualiza el estado de decision de un servicios

  PROCEDURE ActEstDecision(NiConsec_Servic IN TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE,
                           NiEstado        IN VARCHAR2, --Codigo del estado de decision
                           NiCausal        IN TS_ASE_DECISIONES.Tipo_Cau_Ser_Noefectivo%TYPE,
                           ViObservacion   IN TS_ASE_DECISIONES.Observacion%TYPE,
                           VoMsjtec        OUT VARCHAR2,
                           VoMsjusu        OUT VARCHAR2,
                           BoExito         OUT BOOLEAN,
                           ViEs_Parcial    IN VARCHAR2 DEFAULT 'SI') IS

    NEstDecision TS_ASE_DECISIONES.Tipo_Estado_Decision%TYPE;
    NId_Estado   NUMBER;
    NConsecutivo TS_ASE_DECISIONES.Id%TYPE;
    VEstado      TS_ASE_DECISIONES.Tipo_Estado_Decision%TYPE;

    CURSOR CEstados IS
      SELECT Ade.Id, Ade.Tipo_Estado_Decision
        FROM TS_ASE_DECISIONES Ade
       WHERE Fecha_Final IS NULL AND
             Ade.Aservic_Consecutivo_Servicio = NiConsec_Servic;

  BEGIN
    NEstDecision := Pk_Conseres.Sec_Tipo_Codigo(1264, NiEstado);
    OPEN CEstados;
    FETCH CEstados
      INTO NConsecutivo, VEstado;
    CLOSE CEstados;
    IF NConsecutivo IS NULL THEN
      NId_Estado := Pk_Util.Proxima_Sec_Oracle('SS_ASE_DECISIONES');
      --           Pks_Generales.Code_Controls('SS_ASE_DECISION', nId_estado);
      IF NId_Estado IS NOT NULL THEN
        INSERT INTO TS_ASE_DECISIONES
          (Id, Tipo_Estado_Decision, Usu_Usuario, Fecha_Inicial, Es_Parcial,
           Tipo_Cau_Ser_Noefectivo, Observacion, u_Creacion, f_Creacion,
           Aservic_Consecutivo_Servicio)
        VALUES
          (NId_Estado, NEstDecision, USER, SYSDATE, ViEs_Parcial, NiCausal,
           ViObservacion, USER, SYSDATE, NiConsec_Servic);
        BoExito := TRUE;
      ELSE
        VoMsjusu := 'No fue posible asignar la secuencia al estado de decisión';
        BoExito  := FALSE;
      END IF;
    ELSE
      IF VEstado <> NVL(NEstDecision, 0) THEN
        UPDATE TS_ASE_DECISIONES
           SET Fecha_Final = SYSDATE, u_Actualizacion = USER,
               f_Actualizacion = SYSDATE
         WHERE Id = NConsecutivo AND Fecha_Final IS NULL;
        NId_Estado := Pk_Util.Proxima_Sec_Oracle('SS_ASE_DECISIONES');
        -- Pks_Generales.Code_Controls('SS_ASE_DECISION', nId_estado);
        IF NEstDecision IS NOT NULL THEN
          INSERT INTO TS_ASE_DECISIONES
            (Id, Tipo_Estado_Decision, Usu_Usuario, Fecha_Inicial,
             Es_Parcial, Tipo_Cau_Ser_Noefectivo, Observacion, u_Creacion,
             f_Creacion, Aservic_Consecutivo_Servicio)
          VALUES
            (NId_Estado, NEstDecision, USER, SYSDATE, ViEs_Parcial,
             NiCausal, ViObservacion, USER, SYSDATE, NiConsec_Servic);
          BoExito := TRUE;
        ELSE
          VoMsjusu := 'No fue posible asignar la secuencia al estado de decisión';
          BoExito  := FALSE;
        END IF;
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.ACTESTDECISION',
                                       'Error al actualizar el estado de decision ',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presentó problemas actualizando los estados de decision';
      CLOSE CEstados;
  END ActEstDecision;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 10:30:00 p.m.
  -- Actualiza el débito automático de la asesoria
  -- teniendo el consecutivo del servicio y el consecutivo de la cuenta de ahorros.

  PROCEDURE ActualizarDebito(NiConsecutivo_Servicio IN TS_ASE_SERVICIOS.CONSECUTIVO_SERVICIO%TYPE, -- Consecutivo del servicio
                             NiConsecutivo_Debito   IN TS_ASE_SERVICIOS.CONSECUTIVO_SERVICIO%TYPE, -- Consecutivo de la cuenta de ahorros
                             ViAtipper              IN TS_ASE_DEBITO_AUTOMATICOS.ATIPPER_NOMBRE%TYPE, -- Nombre del período de aplicación
                             DiFecha_Final          IN DATE, -- Fecha final del débito
                             NoValor_Descontado     OUT NUMBER, -- Valor a descontar por cuota
                             VoMsjtec               OUT VARCHAR2,
                             VoMsjusu               OUT VARCHAR2,
                             BoExito                OUT BOOLEAN) IS

    -- Verifica el estado
    CURSOR CEstados IS
      SELECT Numero_Servicio Numero_Cuenta, Aservic.Ser_Codigo Cod_Servicio
        FROM TS_ASE_SERVICIOS Aservic, Vs_Tipo_Estado_Decision Testdec
       WHERE Testdec.Codigo IN ('2.08', '2.01') AND
             Aservic.Tipo_Estado_Decision = Testdec.Sec(+) AND
             Aservic.Consecutivo_Servicio = NiConsecutivo_Debito;

    -- Verifica si hay bloqueos
    CURSOR CBloqueos IS
      SELECT Aserres.Fecha_Inicial
        FROM TS_ASE_SER_RESTRINGIDOS Aserres
       WHERE Aserres.Aservic_Consecutivo_Servicio = NiConsecutivo_Debito AND
             SYSDATE BETWEEN Aserres.Fecha_Inicial AND
             NVL(Aserres.Fecha_Final, SYSDATE);

    REst CEstados%ROWTYPE;
    RBlo CBloqueos%ROWTYPE;

  BEGIN
    OPEN CEstados;
    FETCH CEstados
      INTO REst;
    CLOSE CEstados;
    IF REst.Cod_Servicio IS NULL THEN
      BoExito  := FALSE;
      VoMsjusu := 'El servicio no se encuentra efectivo o en estudio';
    ELSE
      IF SUBSTR(REst.Cod_Servicio, 1, 9) IN ('001.02.01', '001.02.04') THEN
        OPEN CBloqueos;
        FETCH CBloqueos
          INTO RBlo;
        CLOSE CBloqueos;
        IF RBlo.Fecha_Inicial IS NULL THEN
          INSERT INTO TS_ASE_DEBITO_AUTOMATICOS Adebaut
            (Adebaut.Aservic_Consecutivo_Servicio,
             Adebaut.Aservic_Cons_Debitado_Desde, Adebaut.Fecha_Inicial,
             Adebaut.Atipper_Nombre, Adebaut.Cuota, Adebaut.Fecha_Final)
          VALUES
            (NiConsecutivo_Servicio, NiConsecutivo_Debito, TRUNC(SYSDATE),
             ViAtipper, NoValor_Descontado, DiFecha_Final);
          BoExito := TRUE;
        ELSE
          BoExito  := FALSE;
          VoMsjusu := 'El servicio se encuentra bloqueado desde ' ||
                      RBlo.Fecha_Inicial;
        END IF;
      ELSE
        BoExito  := FALSE;
        VoMsjusu := 'Solo se debitan cuentas de ahorro a la vista o cuenta corriente especial';
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.Actualizar_debito',
                                       'Error no fue posible grabar el debito ',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en actualizar debito, verifique el LOG';
      CLOSE CEstados;
      CLOSE CBloqueos;
  END ActualizarDebito;

  -- 48118 JMarquez 20171216 (A) Desbloquear el cupo rotativo cuando se refinancia o reestructura
  PROCEDURE RotativoReestructurado(nAsesoria IN NUMBER) IS
    CURSOR cRotativos IS
      SELECT cre.crtipo_credito tipo, rot.consecutivo_servicio servicio, rot.robloqueo
        FROM ts_ase_servicios cre, ts_ase_cre_reestructuraciones r, ts_ase_servicios rot
       WHERE cre.ase_consecutivo_asesoria = nAsesoria
         AND r.aservic_consecutivo_servicio = cre.consecutivo_servicio
         AND rot.consecutivo_servicio = r.arefinanciada_reestructurada_a
         AND rot.aservic_type = 'AROTATI'
         AND nvl(rot.robloqueo,'SINBLOQUEO') = cre.crtipo_credito;
  BEGIN
    FOR rCupo IN cRotativos LOOP
      UPDATE ts_ase_servicios SET robloqueo = 'PROVIS'
       WHERE consecutivo_servicio = rCupo.Servicio;
    END LOOP;
  END RotativoReestructurado; 
  
  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 27/04/2004 05:00:00 p.m.
  -- Permite anular un radicado

  PROCEDURE AnularRadicado(NiConsec_Asesor IN TS_ASESORIAS.Consecutivo_Asesoria%TYPE,
                           VoMsjtec        OUT VARCHAR2,
                           VoMsjusu        OUT VARCHAR2,
                           BoExito         OUT BOOLEAN) IS

  BEGIN
    DELETE FROM TS_ASE_ESTADO_CONTROLES
     WHERE Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    UPDATE TS_ASE_SERVICIOS
       SET Tipo_Estado_Decision = NULL
     WHERE Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    UPDATE TS_ASESORIAS
       SET Aestcon_Codigo = NULL, Numero_Radicado = NULL,
           Fecha_Radicacion = NULL, Adepen_Radicacion = NULL,
           Usu_Usuario_Radicacion = NULL
     WHERE Consecutivo_Asesoria = NiConsec_Asesor;
     --Jorge Naranjo al anular el radicado debe eliminar las cuotas generadas
     IF BoExito THEN
       Pks_Calcular_Cuota.Eliminar_Cuotas ( NiConsec_Asesor,
                                            'R',
                                            boExito,
                                            Vomsjtec,
                                            Vomsjusu );
     END IF;
     --Modifica Jorge Naranjo 20090711 Requerimiento 21437
     --Si se anula la radicación debe devolver los valores para los campos crvalor_solicitado_credito y crvalor_solicitado_desembolso
     UPDATE ts_ase_servicios aservic
     SET aservic.crvalor_solicitado_credito = NULL,
         aservic.crvalor_solicitado_desembolso = NULL
     WHERE aservic.ase_consecutivo_asesoria = NiConsec_Asesor
       AND aservic.aservic_type = 'ACREDIT';
     --Fin modificación 0090711 Requerimiento 21437
     -- 24717 (A) 01/04/2011 JNARANJO
     --Si se anula la radicación debe devolver los valores para los campos crvalor_solicitado_credito
     UPDATE ts_ase_servicios aservic
     SET aservic.crvalor_solicitado_credito = NULL
     WHERE aservic.ase_consecutivo_asesoria = NiConsec_Asesor
       AND aservic.aservic_type = 'AROTATI';
     -- 24717 01/04/2011 Fin modificación
     -- 48118 JMarquez 20171216 (A) Desbloquear el cupo rotativo cuando se refinancia o reestructura
     RotativoReestructurado(NiConsec_Asesor);
     --FIN 48118 JMarquez 20171216 (A)
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.AnularRadicado',
                                       'Error no fue posible anular el radicado',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en anular radicado, verifique el LOG';
  END AnularRadicado;

  PROCEDURE Act_Cta_Ahorros(NiConsec_Asesor IN NUMBER,
                            VoMsjTec        OUT VARCHAR2,
                            VoMsjUsu        OUT VARCHAR2,
                            BoExito         OUT BOOLEAN,
                            biRenovacion    IN  BOOLEAN DEFAULT FALSE,
                            niOficina       IN  NUMBER DEFAULT NULL) IS
    /****************************************************************************************************
      OBJETO: FS_Act_cta_ahorros

      PROPOSITO:
      Actualizar las cuentas de ahorro al efectuar el commit

      PARAMETROS:
      Consec_asesori IN NUMBER

      HISTORIAL
             Fecha                          Usuario                 Versión           Descripción
      14/10/04 02:10:56 p.m.        Alberto Eduardo Sánchez B.       1.0.0       1. Creacion de Act_cta_ahorros
      26/09/06 08:30:00 a.m.        Fredy Sanchez Castro             1.0.1       2. Se agregan parametros para el
                                                                                    renovacion masiva de servicios
                                                                                    de recaudo a terceros
                                                                                    (biRenovacion, niOficina)

      REQUISITOS:
      Haber efectuado commit en la forma

      NOTAS:
      Se realiza para evitar el bloqueo de TS_ADM_CONSECUTIVOS

    ****************************************************************************************************/

    -- Variables locales utilizadas en el proceso
    VConsecutivo VARCHAR(38);
    NCon_Comprob NUMBER(3);
    VSerie       VARCHAR2(38);
    VCodigo      VARCHAR2(2); -- guardar el codigo de la oficina
    NProceso     NUMBER; -- Número de Procesos
    VNroCuenta   TS_ASE_SERVICIOS.Numero_Servicio%TYPE;
    NServicio    NUMBER;
    BConfirmar   BOOLEAN;
    --
    -- Fecha: 13/08/04 03:20:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar que servicios de ahorro hay en la asesoria
    --

    --27968 20120601 JNARANJO
    nEdoRechazado PLS_INTEGER := pk_conseres.sec_tipo_codigo(1264,'2.13');
    nEdoNoEfectivo PLS_INTEGER := pk_conseres.sec_tipo_codigo(1264,'2.14');
    nEdoAnulado PLS_INTEGER := pk_conseres.sec_tipo_codigo(1264,'2.22');
    --27968 20120601 Fin modificación
    
    CURSOR CServicios IS
      SELECT Aservic.Numero_Servicio, Aservic.Consecutivo_Servicio,
             Aservic.Ser_Codigo, Aservic.Solicitar_Consecutivo,
             Aservic.Adepen_Asignada,
             -- Linea Adicionada
             -- Fecha : 27-02-2008
             -- Usuario : JACEVEDO
             -- Se adiciona el Per_Id, para controlar los estados del cliente
             Aservic.Per_Id
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor
       --27968 20120601 JNARANJO
         AND NVL(aservic.tipo_estado_decision,0) NOT IN (nEdoRechazado,nEdoNoEfectivo,nEdoAnulado);
       --27968 20120601 Fin modificación

    --
    -- Linea Adicionada
    -- Fecha : 27-02-2008
    -- Usuario : JACEVEDO
    -- Recupera Informacion para determinar el estado del cliente
    --
    CURSOR CEstado_Cliente(NPer_Id IN NUMBER) IS
      SELECT Pestado.Estado_Persona
        FROM TS_PER_ESTADOS Pestado
       WHERE Pestado.Per_Id = NPer_Id AND Pestado.Fecha_Final IS NULL;

    vEstado      VARCHAR2(6); -- Utilizada para determinar el estado de la persona
    dFec_Proceso DATE := SYSDATE; -- Determina la fecha en que se haran efectivo los servicios
    --
    -- Fecha: 26/08/04 03:29:06 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el codigo de la oficina para la signación del número de cuenta
    --
    CURSOR CCod_Dependencia(Dep_Id IN NUMBER) IS
      SELECT SUBSTR(LPAD(TO_CHAR(TO_NUMBER(Adepend.Codigo)), 3, '0'), -2, 2) Codigo
        FROM TS_ADM_DEPENDENCIAS Adepend
       WHERE Adepend.Id = Dep_Id;

    --
    -- Fecha: 26/08/04 03:29:06 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el comprobante del consecutivo
    --
    CURSOR CProceso(Niproceso IN NUMBER) IS
      SELECT Atipcon.Codigo Tipoconsec
        FROM TS_ADM_PROCESOS Aproces, TS_ADM_TIPO_CONSECUTIVOS Atipcon
       WHERE Atipcon.Codigo = Aproces.Atipcon_Codigo AND
             Aproces.Codigo = NiProceso;

    NDependencia NUMBER(3);
    eErrorCuota EXCEPTION;--Maneja el error al insertar en ts_ase_cuotas
  BEGIN
    IF NOT biRenovacion THEN
      NDependencia := TO_NUMBER(Pks_Sessiones.RETORNAR_CODDEPENDENCIA);
    END IF;
    BConfirmar   := FALSE;
    -- JMarquez 20110628 (M) Req: 26048  El número de servicio se estandariza en la función pks_utilidad_servicios.numero_servicio
    -- NProceso     := 0; 
    FOR CSer IN CServicios LOOP
    
      NServicio := CSer.Consecutivo_Servicio;
    
      -- JMarquez 20110628 (M) Req: 26048  El número de servicio se estandariza en la función pks_utilidad_servicios.numero_servicio
      /*
      IF SUBSTR(CSer.Ser_Codigo, 1, 6) = '001.01' THEN
        NProceso := 158;
      ELSIF SUBSTR(CSer.Ser_Codigo, 1, 6) = '001.02' THEN
        NProceso := 131;
      ELSIF SUBSTR(CSer.Ser_Codigo, 1, 6) = '001.03' THEN
        NProceso := 157;
      ELSIF SUBSTR(CSer.Ser_Codigo, 1, 6) = '001.04' THEN
        NProceso := 185;
      END IF;
      IF NVL(NProceso, 0) <> 0 THEN
        IF CSer.Numero_Servicio IS NULL AND
          NVL(CSer.Solicitar_Consecutivo, 'NO') = 'SI' THEN
          -- JGHUTIER 28-10-2006. Si no se está haciendo una Renovación masiva
          -- de recaudo a terceros se obtiene el consecutivo de forma normal
          IF NOT biRenovacion THEN
            Consecutivo(NProceso, VConsecutivo, VoMsjtec, VoMsjusu, BoExito);
          -- JGHUTIER 28-10-2006. Si se está haciendo una Renovación masiva
          -- de recaudo a terceros se obtiene el consecutivo pasándole la
          -- oficina definida por el llamado
          ELSE
            Consecutivo(NProceso
              , VConsecutivo
              , VoMsjtec
              , VoMsjusu
              , BoExito
              , biRenovacion
              , niOficina);
          END IF;
          -- JGHUTIER 28-10-2006. *** FIN ***

          IF VConsecutivo IS NULL THEN
            BoExito  := FALSE;
            VoMsjusu := 'El consecutivo ' || TO_CHAR(NProceso) ||
                        ' asociado al proceso, presenta inconsistencias';
          ELSE
            BoExito := TRUE;
          END IF;
          IF SUBSTR(CSer.Ser_Codigo, 1, 6) <> '001.03' THEN
            OPEN CCod_Dependencia(CSer.Adepen_Asignada);
            FETCH CCod_Dependencia
              INTO VCodigo;
            CLOSE CCod_Dependencia;
            VNroCuenta := VCodigo || SUBSTR(CSer.Ser_Codigo, 6, 1) ||
                          SUBSTR(CSer.Ser_Codigo, 9, 1) ||
                          SUBSTR(CSer.Ser_Codigo, 12, 2) ||
                          SUBSTR(VConsecutivo, -7);
          ELSE
            VNroCuenta := SUBSTR(VConsecutivo, 2, 3) ||
                          SUBSTR(VConsecutivo, -5);
          END IF;
          */
      
      -- 08/11/2012
      -- John Arley Marín Valencia    
      -- Se limpia la variable para que al insertar no se viole el UK de numero_servicio.
      -- Inicio req:29100
      VNroCuenta := NULL;    
      -- fin req:29100
      IF CSer.Numero_Servicio IS NULL AND NVL(CSer.Solicitar_Consecutivo, 'NO') = 'SI' THEN    
        VNroCuenta := pks_utilidad_servicios.NumeroServicio(viSerCodigo => CSer.Ser_Codigo, niDependencia => CSer.Adepen_Asignada);
      END IF;  
      -- FIN JMarquez 20110628 (M) Req: 26048

      --
      -- Fecha modificación : 29/03/2005 04:13:57 p.m.
      -- Usuario modifica   : Alberto Eduardo Sánchez B.
      -- Causa modificación : Se adiciona la dependencia de radicación, para que no tome la de asesoria
      --
      IF VNroCuenta IS NOT NULL THEN
        --Jorge Naranjo 20061229 Se deben crear los registros en ts_ase_cuotas
        BEGIN
          Pks_Calcular_Cuota.Insertar_Cuotas ( CSer.Consecutivo_Servicio,
                                               boExito,
                                               Vomsjtec,
                                               Vomsjusu );
          IF NOT boExito THEN
            RAISE eErrorCuota;
          END IF;
        END;
            
        UPDATE TS_ASE_SERVICIOS
           SET Numero_Servicio = VNroCuenta,
               Adepen_Asignada = NVL(NDependencia, Adepen_Asignada)
         WHERE Consecutivo_Servicio = CSer.Consecutivo_Servicio;

        -- END IF;  JMarquez 20110628 (M) Req: 26048  Ahora todo depende de la asignación del numero de servicio
        IF CSer.Ser_Codigo = '001.01.00.001' THEN
          NCon_Comprob := 0;
          OPEN CProceso(135);
          FETCH CProceso
            INTO NCon_Comprob;
          CLOSE CProceso;
          IF NCon_Comprob IS NULL THEN
            -- 4
            VoMsjusu := 'La configuración del consecutivo 135 , no aparece asociada al proceso de Hacer efectivo el servicio';
            BoExito  := FALSE;
          ELSE
            Pks_Consecutivo.Proximo(NCon_Comprob,
                                    NULL,
                                    VConsecutivo,
                                    VSerie);
            IF VConsecutivo IS NULL THEN
              -- 5
              BoExito  := FALSE;
              VoMsjusu := 'El consecutivo 135 asociado al proceso, presenta inconsistencias';
            ELSE
              UPDATE TS_ASE_SERVICIOS
                 SET Afserie = VSerie, Afconsecutivo = VConsecutivo
               WHERE Consecutivo_Servicio = CSer.Consecutivo_Servicio;
            END IF; -- Fin 5
          END IF; -- Fin 4
        END IF;
        BConfirmar := TRUE;
        -- END IF;   Marquez 20110628 (M) Req: 26048  Ahora todo depende de la asignación del numero de servicio
        -- END IF;  JMarquez 20110628 (M) Req: 26048  Ahora todo depende de la asignación del numero de servicio
    ----------------------------------------------------------
    -- LINEA ADICIONADA
    -- FECHA : 27-02-2008
    -- USUARIO: JACEVEDO Æ
    -- Se debe controlar si se debe crear el estado activo para la persona

    -- IF VNroCuenta IS NOT NULL THEN JMarquez 20110628 (M) Req: 26048  Ahora todo depende de la asignación del numero de servicio

        -- En caso que el solicitante presente el estado "POSIBLE CLIENTE"
        -- ó "CLIENTE INACTIVO" se pasa a  "CLIENTE ACTIVO", registrando
        -- la fecha, hora y usuario.
        OPEN CEstado_Cliente(CSer.Per_Id);
        FETCH CEstado_Cliente
          INTO VEstado;
        CLOSE CEstado_Cliente;
        IF VEstado IN ('POSIBL', 'INACTI') THEN
          UPDATE TS_PER_ESTADOS
             SET Fecha_Final = DFec_Proceso
           WHERE Per_Id = CSer.Per_Id AND Fecha_Final IS NULL;

          INSERT INTO TS_PER_ESTADOS
            (Per_Id, Fecha_Inicial, Estado_Persona, u_Creacion,
             f_Creacion, Fecha_Final)
          VALUES
            (CSer.Per_Id, DFec_Proceso, 'ACTIVO', USER, SYSDATE,
             NULL);
        END IF;
        Rastro('Con Número','Estado');
      ELSE
        Rastro('Sin Número','Estado');
      END IF;

    END LOOP;
    IF BoExito AND BConfirmar THEN
       --
       -- Req. Cia
       -- Fecha modificación : 10/09/2007 01:08:00 p.m.
       -- Usuario modifica   : Alberto Eduardo Sánchez B.
       -- Causa modificación : En ocasiones no queda almacenada la compañia
       pks_compania.OBTENER_CIA(NiConsec_Asesor,
                                 voMsjTec,
                                 voMsjUsu,
                                 boExito);
       --
       -- Fin Req. Cia
       COMMIT;
    END IF;
  EXCEPTION
    WHEN eErrorCuota THEN
        BoExito  := FALSE;
        Rastro('Error al insertar ts_ase_cuotas. '||SQLCODE||'-'||VoMsjtec,'');
        VoMsjusu := 'Error al asignar el próximo consecutivo, verifique el LOG.';
        RETURN;
    WHEN OTHERS THEN
      IF SQLCODE = -20301 THEN
        BoExito  := FALSE;
        VoMsjusu := 'No fue posible asignar Próximo Consecutivo. Intente la operación en unos minutos.';
        RETURN;
      ELSIF SQLCODE = -20751 THEN
        BoExito  := FALSE;
        VoMsjusu := 'No existe un consecutivo configurado. Por favor verifíquelo para continuar con el proceso.';
        RETURN;
      ELSE
        Pks_Garantia_Real.CASE_EXCEPTION('Act_cta_ahorros',
                                         'Error en Act_cta_ahorros ' ||
                                         TO_CHAR(NServicio) ||
                                         ' número de cuenta ' || VNroCuenta,
                                         SQLCODE,
                                         VoMsjtec,
                                         BoExito);
        VoMsjUsu := 'Se presentó problemas al asignar el número del servicio, verifique el LOG';
      END IF;
  END Act_Cta_Ahorros;

  PROCEDURE Asignar_DocControlado(NiDoc_Id               IN NUMBER,
                                  NiNro_Cupones          IN NUMBER,
                                  NiPer_Id               IN NUMBER,
                                  NiConsecutivo_Servicio IN NUMBER,
                                  DiFecha_Inicial        IN DATE,
                                  ViConsecutivo          OUT VARCHAR2,
                                  VoMsjTec               OUT VARCHAR2,
                                  VoMsjUsu               OUT VARCHAR2,
                                  BoExito                OUT BOOLEAN) IS
    /****************************************************************************************************
      OBJETO: Asignar_DocControlado

      PROPOSITO:
      Asignar los documentos controlados al servicio

      PARAMETROS:
      nIdoc_id       Id del documento controlado
      nNro_cupones   Número de documentos controlados

      HISTORIAL
             Fecha                          Usuario                 Versión           Descripción
      02/11/04 09:10:07 a.m.        Alberto Eduardo Sánchez B.       1.0.0       1. Creacion de Asginar_DocControlado

      REQUISITOS:
      Los documentos deben estar disponibles y asignados a la oficina

      NOTAS:
      Este procedimiento es utilizado al radicar y hacer efectivo el servicio

    ****************************************************************************************************/

    -- Variables locales utilizadas en el proceso

    --
    -- Recupera el inventario en la oficina actual del documento solicitado
    --
    CURSOR CUniDoc(Ndoc_Id IN NUMBER, Nnro_Cupones IN NUMBER) IS
      SELECT Dconuni.Consecutivo_Inicio -- * --41208 JNARANJO 20170307 Cambio el "*"
        FROM TS_DOC_CONT_UNIDADES Dconuni
       WHERE Dconuni.Tipo_Estado_Doc =
             Pk_Conseres.Sec_Tipo_Codigo('791', 'ASIOFI') AND
             Adepen_Id = Pks_Generales.FDependencia(USER) AND
             Dconuni.Dcontr_Doc_Id = Ndoc_Id AND
             Dconuni.Dcontr_Nro_Cupones = Nnro_Cupones
       ORDER BY Consecutivo_Inicio;

    --
    -- Bloquea el documento solicitado
    --
    CURSOR CBlockDoc(Ndoc_Id IN NUMBER, Nnro_Cupones IN NUMBER, NConsecutivo IN VARCHAR2) IS
      SELECT Dconuni.Consecutivo_Inicio --*
        FROM TS_DOC_CONT_UNIDADES Dconuni
       WHERE Dconuni.Tipo_Estado_Doc =
             Pk_Conseres.Sec_Tipo_Codigo('791', 'ASIOFI') AND
             Adepen_Id = Pks_Generales.FDependencia(USER) AND
             Dconuni.Consecutivo_Inicio = NConsecutivo AND
             Dconuni.Dcontr_Doc_Id = Ndoc_Id AND
             Dconuni.Dcontr_Nro_Cupones = Nnro_Cupones
         FOR UPDATE OF Tipo_Estado_Doc, Apaservic_Consecutivo_Servicio, Aperson_Per_Id, Aperson_Fecha_Inicial, Causal_Cambio, Valor_Documento NOWAIT;

    RUniDoc    CUniDoc%ROWTYPE;
    RBloDoc    CBlockDoc%ROWTYPE;
    BContinuar BOOLEAN; -- Verifica si se encuentran documentos disponibles
    NIntentos  NUMBER; -- Permite controlar el número de intentos
  BEGIN
    BoExito    := TRUE;
    BContinuar := TRUE;
    NIntentos  := 1;
    OPEN CUniDoc(NiDoc_Id, NiNro_Cupones);
    FETCH CUniDoc
      INTO RUniDoc;
    IF CUniDoc%FOUND THEN
      WHILE BContinuar AND CUniDoc%FOUND AND NIntentos <= 100 LOOP
        BEGIN
          OPEN CBlockDoc(NiDoc_Id,
                         NiNro_Cupones,
                         RUniDoc.Consecutivo_Inicio);
          FETCH CBlockDoc
            INTO RBloDoc;
          IF RBloDoc.Consecutivo_Inicio IS NOT NULL THEN
            ViConsecutivo := RBloDoc.Consecutivo_Inicio;
            UPDATE TS_DOC_CONT_UNIDADES
               SET Tipo_Estado_Doc = Pk_Conseres.Sec_Tipo_Codigo('791',
                                                                  'ASICLI'),
                   Aperson_Per_Id = NiPer_Id,
                   Apaservic_Consecutivo_Servicio = NiConsecutivo_Servicio,
                   Aperson_Fecha_Inicial = DiFecha_Inicial
             WHERE CURRENT OF CBlockDoc;
          END IF;
          CLOSE CBlockDoc;
          BContinuar := FALSE;
        EXCEPTION
          WHEN OTHERS THEN
            IF SQLCODE = -54 THEN
              FETCH CUniDoc
                INTO RUniDoc;
              BContinuar := TRUE;
              NIntentos  := NIntentos + 1;
            ELSE
              IF CUniDoc%ISOPEN THEN
                CLOSE CUniDoc;
              END IF;
              IF CBlockDoc%ISOPEN THEN
                CLOSE CBlockDoc;
              END IF;
              BContinuar := FALSE;
              Pks_Garantia_Real.CASE_EXCEPTION('Ciclo de asignación de Asignar_DocControlado',
                                               'Error en Ciclo de asignación de Asignar_DocControlado',
                                               SQLCODE,
                                               VoMsjtec,
                                               BoExito);
              VoMsjUsu := 'Se presentó problemas en el ciclo de asignación de Asignar_DocControlado, verifique el LOG';
            END IF;
        END;
      END LOOP;
    ELSE
      VoMsjUsu := 'La oficina no posee inventario disponible del documento, por lo tanto no puede hacer entrega de este';
      VoMsjTec := 'No se encontraron registros en ts_doc_cont_unidades';
      BoExito  := FALSE;
    END IF;
    CLOSE CUniDoc;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('Asignar_DocControlado',
                                       'Error en Asignar_DocControlado',
                                       SQLCODE,
                                       VoMsjtec,
                                       BoExito);
      VoMsjUsu := 'Se presentó problemas en Asignar_DocControlado, verifique el LOG';
      IF CUniDoc%ISOPEN THEN
        CLOSE CUniDoc;
      END IF;
      IF CBlockDoc%ISOPEN THEN
        CLOSE CBlockDoc;
      END IF;
  END Asignar_DocControlado;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/04/2004 05:00:00 p.m.
  -- Si la solicitud incluye sólo el tipo de servicio
  -- de ahorro, no se requiere ninguna fase de análisis
  -- para ningún servicio y quienes intervienen en la solicitud
  -- son todos ¿CLIENTES ACTIVOS¿, el estado de decisión de cada servicio
  -- pasa a ¿SERVICIO EFECTIVO¿ y se asigna el número de cuenta

  PROCEDURE AsignarNroCtaAhorros(NiConsec_Asesor IN TS_ASESORIAS.Consecutivo_Asesoria%TYPE,
                                 ViTitulos       IN VSer_Ahorros,
                                 NiCont          IN NUMBER := NULL,
                                 VoMsjtec        OUT VARCHAR2,
                                 VoMsjusu        OUT VARCHAR2,
                                 BoExito         OUT BOOLEAN) IS

    NCont               NUMBER;
    Num                 NUMBER;
    VNroCuenta          TS_ASE_SERVICIOS.Numero_Servicio%TYPE;
    VConsecutivo_Inicio TS_DOC_CONT_UNIDADES.Consecutivo_Inicio%TYPE;
    VAhnumero_Titulo    TS_ASE_SERVICIOS.Ahnumero_Titulo%TYPE;
    VAhnumero_Serie     TS_ASE_SERVICIOS.Ahnumero_Serie%TYPE;
    VDigitos_Serie      TS_ASE_SERVICIOS.Ahnumero_Serie%TYPE;
    DFec_Proceso        DATE := SYSDATE; -- Determina la fecha en que se haran efectivo los servicios de ahorro
    NCant               NUMBER(3); -- Controla la posición de cada servicio
    VServicios          VSer_Efectivo; -- Registro para controlar la información del servicio efectivo
    VEstado             VARCHAR2(6); -- Utilizada para determinar el estado de la persona
    NFase               NUMBER(1); -- Para determinar si el servicio requiere fase de analisis
    VAsignarCons        VARCHAR2(2); -- Utilizado para determinar si el servicio requiere consecutivo
    VDocControlado      VARCHAR2(2) := 'SI'; -- Determinar si se asigna documento controlado o no
    NCtrlEfectivo       NUMBER; -- Controla si el colitante tiene servicios efectivos
    --
    -- Fecha: 13/08/04 03:20:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar cuantos servicios diferentes a ahorro hay en la asesoria
    -- Modifica Jorge Naranjo 20080125 No debe tener en cuenta coopecheque dentro de los servicios de ahorro
    CURSOR CServicio IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE ( SUBSTR(Aservic.Ser_Codigo, 1, 6) != '001.02' OR Aservic.Ser_Codigo = '001.02.04.001' ) AND
               --SUBSTR(Aservic.Ser_Codigo, 1, 6) <> '001.02' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) != '001.04' -- JMarquez 07-10-2005 Se agrega el recaudo a tercero para que funcione como una cuenta de ahorros
             AND Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    --
    -- Fecha: 13/08/04 03:20:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar que servicios de ahorro hay en la asesoria
    -- Modifica Jorge Naranjo 20080125 No debe tener en cuenta coopecheque dentro de los servicios de ahorro
    CURSOR CSer_Ahorros IS
      SELECT Aservic.Numero_Servicio, Aservic.Consecutivo_Servicio,
             Aservic.Ser_Codigo
          , aservic.atipper_liquidacion
          , aservic.cuota_periodo_liquidacion
          -- Linea Adicionada
          -- Fecha  : 12-09-2008
          -- Usuario: JACEVEDO
          -- 21002 - Se adiciona los campos Tipo destino y per_id
          , Aservic.tipo_destino
          , Aservic.Per_Id
        FROM TS_ASE_SERVICIOS Aservic
       WHERE SUBSTR(Aservic.Ser_Codigo, 1, 6) IN ('001.02', '001.04') -- JMarquez 07-10-2005 Se agrega el recaudo a tercero para que funcione como una cuenta de ahorros
             AND Aservic.Ser_Codigo != '001.02.04.001'
             AND Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    CURSOR APerson(NiConsec_Servic IN NUMBER) IS
      SELECT Aperson.Per_Id, Aperson.Fecha_Inicial
        FROM TS_ASE_SERVICIOS Aservic, TS_ASE_PERSONAS Aperson
       WHERE Aperson.Aperson_Type = 'SOLICI' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) IN ('001.02', '001.04') -- JMarquez 07-10-2005 Se agrega el recaudo a tercero para que funcione como una cuenta de ahorros
             AND Aperson.Aservic_Consecutivo_Servicio = NiConsec_Servic;

    RPerson APerson%ROWTYPE;

    CURSOR CAhoAct IS
      SELECT Adepend.Codigo, Aservic.Adepen_Asignada, Aservic.Ser_Codigo,
             Aservic.Consecutivo_Servicio, Aservic.Tipo_Estado_Decision,
             Aservic.Numero_Servicio, Aservic.Aforpag_Nombre,
             Aservic.Ahfecha_Inicio_Ahorro, Aservic.Per_Id
        FROM TS_ASE_SERVICIOS Aservic, TS_ADM_DEPENDENCIAS Adepend
       WHERE Adepend.Id = Aservic.Adepen_Asignada AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) IN ('001.02', '001.04') -- JMarquez 07-10-2005 Se agrega el recaudo a tercero para que funcione como una cuenta de ahorros
             AND Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    --
    -- Fecha: 13/08/04 03:20:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar que todas las personas relacionadas con la
    -- cuenta de ahorros se encuentren activos
    -- Modificado por JGlen en 04/01/2005

    CURSOR CAhorros(NiConsec_Servic IN NUMBER) IS
      SELECT Aservic.Per_Id
        FROM TS_ASE_PERSONAS Aperson, TS_ASE_SERVICIOS Aservic,
             TS_PER_ESTADOS Pestado
       WHERE Pestado.Estado_Persona <> 'ACTIVO' AND
             Pestado.Per_Id = Aperson.Per_Id AND
             Pestado.Fecha_Final IS NULL AND
             Aperson.Aperson_Type IN ('AHOTIT', 'AHOAUT', 'SOLICI') AND
             Aperson.Aservic_Consecutivo_Servicio =
             Aservic.Consecutivo_Servicio AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) IN ('001.02', '001.04') AND -- JMarquez 07-10-2005 Se agrega el recaudo a tercero para que funcione como una cuenta de ahorros
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

    /* CURSOR cAhorros (niConsec_servic IN NUMBER) IS
    SELECT aservic.per_id
      FROM ts_ase_personas  aperson,
           ts_ase_servicios aservic,
           ts_per_estados   pestado
     WHERE pestado.estado_persona <> 'ACTIVO'
       AND pestado.per_id = aservic.per_id
       AND pestado.fecha_final IS NULL
       AND aperson.aperson_type IN ('AHOTIT','AHOAUT','SOLICI')
       AND aperson.aservic_consecutivo_servicio = aservic.consecutivo_servicio
       AND SUBSTR(aservic.ser_codigo,1,6) = '001.02'
       AND aservic.consecutivo_servicio = niConsec_servic;*/

    --
    -- Fecha: 25/08/04 07:54:18 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si posee servicios efectivos
    --Jorge Naranjo 20061208 Se tienen en cuenta solo los servicios de afiliación,ahorros y créditos, debido a que
    --éstos deben pasar por el centro de operaciones (los servicios de recaudo y transitorias y desembolso masivo no)
    CURSOR CEfectivos IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic, TS_ASESORIAS Ase
       WHERE Aservic.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') AND
             Ase.Per_Id = Aservic.Per_Id AND
             Ase.Consecutivo_Asesoria = NiConsec_Asesor AND
             -- 25732 (M) 11/05/2011 JNARANJO
             --aservic.aservic_type IN ('AAFILIA','AAHORRO','ACREDIT') AND
             aservic.aservic_type IN ('AAFILIA','AAHORRO','ACREDIT','AROTATI') AND
             -- 25732 11/05/2011 Fin modificación 
             aservic.Ase_Consecutivo_Asesoria NOT IN
             ( SELECT asedir.consecutivo_asesoria
               FROM TS_ASESORIAS asedir
               WHERE asedir.consecutivo_asesoria = aservic.Ase_Consecutivo_Asesoria
                 AND asedir.acredir_id IS NOT NULL );

    --
    -- Fecha: 25/08/04 04:48:42 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si posee consecutivos asignados por servicio
    --
    CURSOR CDcto_Servicio(VSer_Cod IN VARCHAR2) IS
      SELECT Doc.Atipcon_Codigo, Sdocume.Control_Titulo, Doc.Maneja_Serie,
             Doc.Digitos_Serie, Doc.Numero_Digitos
        FROM VS_SER_DOCUMENTOS Sdocume, TS_DOCUMENTOS Doc
       WHERE Sdocume.Doc_Id = Doc.Id AND Sdocume.Ser_Codigo = VSer_Cod AND
             Sdocume.Control_Titulo = 'SI' AND Doc.Controlado = 'NO';

    RDoc_Ser CDcto_Servicio%ROWTYPE;

    --
    -- Fecha: 13/08/04 03:20:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar las condiciones de asignación de documentos controlados
    --

    CURSOR CDocto(VSer_Cod IN VARCHAR2) IS
      SELECT Doc.Maneja_Serie, Doc.Digitos_Serie, Doc.Numero_Digitos,
             Dcontro.Doc_Id, Dcontro.Nro_Cupones, Sdocume.Control_Titulo,
             Doc.Controlado, Atipcon.Codigo, Atipcon.Nivel
        FROM VS_SER_DOCUMENTOS Sdocume, TS_DOCUMENTOS Doc,
             TS_DOC_CONTROLADOS Dcontro, TS_ADM_TIPO_CONSECUTIVOS Atipcon
       WHERE Doc.Id = Dcontro.Doc_Id(+) AND
             Doc.Atipcon_Codigo = Atipcon.Codigo(+) AND
             Sdocume.Doc_Id = Doc.Id AND Sdocume.Ser_Codigo = VSer_Cod AND
             Sdocume.Control_Titulo = 'SI';

    RDocto CDocto%ROWTYPE;

    --
    -- Fecha: 09/08/04 08:54:10 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el estado del cliente
    --
    CURSOR CEstado_Cliente(NPer_Id IN NUMBER) IS
      SELECT Pestado.Estado_Persona
        FROM TS_PER_ESTADOS Pestado
       WHERE Pestado.Per_Id = NPer_Id AND Pestado.Fecha_Final IS NULL;

    --
    -- Fecha: 24/08/04 03:12:25 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si exite algún servicio que requiere una fase de analisis
    --
    CURSOR CFase_Analisis IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic, TS_ADM_FASE_SERVICIOS Afasser
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             Afasser.Ser_Codigo LIKE '001.02%' AND
             Afasser.Usu_Usuario LIKE 'G_ANALISTA%' AND
             Aservic.Ser_Codigo LIKE (Afasser.Ser_Codigo || '%');

    -- JHGUTIER 02-11-2006. Recupera Informacion para determinar el número de
    -- días del período
    CURSOR CDias_Per(pPerLiq VARCHAR2) IS
      SELECT atipper.numero_dias
      FROM TS_ADM_TIPO_PERIODOS Atipper
      WHERE atipper.nombre = pPerLiq;
    CURSOR cServPrimeraCuota ( niConsServicio TS_ASE_SERVICIOS.consecutivo_servicio%TYPE ) IS
      SELECT *
      FROM TS_ASE_SERVICIOS aservic
      WHERE aservic.consecutivo_servicio= niConsServicio;
    -- JHGUTIER 02-11-2006. Días del período de liquidación
    nDiasPeriodo NUMBER;

    -- JHGUTIER 02-11-2006. Fecha de pago de la primera cuota
    dFechaProximaCuota DATE;
    vAsignaTitulo VARCHAR2(2) := 'SI';--Almacena si se debe asignar título o no
    eErrorCuota EXCEPTION;--Maneja el error al insertar en ts_ase_cuotas
    dFechaPrimeraCuota DATE;
    rowAservic cServPrimeraCuota%ROWTYPE;

    -- LINEA ELIMINADA
    -- FECHA  : 28-02-2008
    -- USUARIO: JACEVEDO
    -- SE ELIMINA EL PROCESO DE ESTADOS DE DECISION DEBIDO A QUE SE DEBE
    -- COLOCAR EL ESTADO ACTIVO, SOLO PARA LOS SERVICIO CON NUMERO DE SERVICIO
    -- EN ESTA FUNCION NO SE HA ASIGNADO DICHO NUMERO
    /*
        -- LINEA ADICIONADA
        -- FECHA : 01-02-2008
        -- USUARIO: JACEVEDO Æ
        -- Se debe controlar si se debe crear el estado activo para la persona
        Cursor cNumeroServicio (nConsecutivoServicio Number) Is
        Select Aservic.Numero_Servicio
          From Ts_Ase_Servicios Aservic
         Where Aservic.Consecutivo_Servicio = nConsecutivoServicio;

        vNumeroServicio     Ts_Ase_Servicios.Numero_Servicio%Type;
        -- Hasta Aqui
     */
    -- Hasta Aqui elimina

  --Modifica Jorge Naranjo 20080125 Identifica si se pasa el servicio a Efectivo según cambio documento cronograma 2008-1
  --Extrae cada uno de los clientes de la asesoría
    NEstadoEfectivo NUMBER := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08');
    NEstadoEstudio  NUMBER := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01');
    NEstadoAprobado NUMBER := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.05');--Modifica Jorge Naranjo 20080125 agrego estado
  CURSOR cAperson IS
    SELECT aperson.per_id
    FROM ts_ase_personas aperson,ts_ase_servicios aservic
    WHERE aperson.aservic_consecutivo_servicio = aservic.consecutivo_servicio
      AND aservic.ase_consecutivo_asesoria = NiConsec_Asesor;
  --Valida si existe un servicio para el cliente en estado efectivo o estudio con número de cuenta o aprobado
  CURSOR cExisteAsesor (nPerid NUMBER) IS
    SELECT 1
    FROM ts_ase_servicios aservic
    WHERE NVL(aservic.ase_consecutivo_asesoria,0) != NiConsec_Asesor
      AND ( (aservic.tipo_estado_decision IN (nEstadoEstudio,nEstadoEfectivo) AND aservic.numero_servicio IS NOT NULL) OR
            (aservic.tipo_estado_decision IN (nEstadoAprobado))
          )
      -- 25732 (M) 11/05/2011 JNARANJO
      --AND aservic.ASERVIC_TYPE IN ('AAFILIA','ACREDIT','AAHORRO')
      AND aservic.ASERVIC_TYPE IN ('AAFILIA','ACREDIT','AAHORRO','AROTATI')
      -- 25732 11/05/2011 Fin modificación             
      AND aservic.PER_ID = nPerid
      AND NVL(aservic.Ase_Consecutivo_Asesoria,0) NOT IN
             ( SELECT asedir.consecutivo_asesoria
               FROM TS_ASESORIAS asedir
               WHERE asedir.consecutivo_asesoria = aservic.Ase_Consecutivo_Asesoria
                 AND asedir.acredir_id IS NOT NULL );
    -- Configuración de Diligenciamento de solicitud para servicios de ahorros
    CURSOR CSerAho IS
      SELECT COUNT(1)
        FROM VS_SER_AHORROS Sahorro, TS_ASE_SERVICIOS Aservic,
             TS_PERSONAS Per
       WHERE Sahorro.Diligenciar_Solicit = 'SI' AND
             (Sahorro.Tipo_Persona = Per.Per_Type OR
             Sahorro.Tipo_Persona = 'AMBAS') AND
             (Sahorro.Ser_Codigo = Aservic.Ser_Codigo OR
             Sahorro.Ser_Codigo = SUBSTR(Aservic.Ser_Codigo, 1, 6) OR
             Sahorro.Ser_Codigo = SUBSTR(Aservic.Ser_Codigo, 1, 9)) AND
             Aservic.Per_Id = Per.Id AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;
  nPoseeServicio NUMBER;--Almacena si el cliente posee servicios
  bCumpleServicios BOOLEAN := TRUE;--Identifica si posee servicios
  nReqSolicitud NUMBER;--Identifica cuántos servicios requieren solicitud
  --Identifica si existen servicios de CDT
  CURSOR cEsCdt IS
    SELECT COUNT(1)
    FROM TS_ASE_SERVICIOS Aservic
    --34347 JNARANJO 20150115
    --WHERE  Aservic.Ser_Codigo = '001.02.02.003' AND
    WHERE  Aservic.Ser_Codigo = Pks_Utilidad_Servicios.es_CDT(Aservic.Ser_Codigo) AND
    --34347 JNARANJO 20150115 Fin modificación
           Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;
  --Identifica si existen servicios diferentes a CDT
  CURSOR cNoEsCdt IS
    SELECT COUNT(1)
    FROM TS_ASE_SERVICIOS Aservic
    --34347 JNARANJO 20150115
    --WHERE  Aservic.Ser_Codigo != '001.02.02.003' AND
    WHERE  Aservic.Ser_Codigo != Pks_Utilidad_Servicios.es_CDT(Aservic.Ser_Codigo) AND
    --34347 JNARANJO 20150115 Fin modificación
           Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;
  nEsCdt NUMBER := 0;--Identifica si existen servicios de CDT
  nNoEsCdt NUMBER := 0;--Identifica si existen servicios diferentes a CDT
  --Fin modificación 20080125

  -- Linea Adicionada
  -- Fecha  : 12-09-2008
  -- Usuario: JACEVEDO
  -- 21002 - Si es persona jurídica y la nueva destinación tiene asociada una configuración
  nMesadaPensional Number(10) := Pk_Conseres.Sec_Tipo_Codigo('4549', '8'); -- Destinación consignaciones varias

  Cursor cDestino (nCodDestino Number) Is
  Select cgmfexe.valor
    From ts_con_gmf_exento_vigencias cgmfvig,
         ts_con_gmf_exentos  cgmfexe
   Where Sysdate Between cgmfvig.fecha_inicial
     and Nvl(cgmfvig.fecha_final, Sysdate)
     and cgmfvig.fecha_inicial = cgmfexe.cexevig_fecha_inicial
     and cgmfexe.destino_gmf <> nMesadaPensional
     and cgmfexe.destino_gmf = nCodDestino;
   nDestino       Number;

   Cursor cTipoPersona (nPerId Number) Is
    Select Decode(per.per_type, 'PNATUR', per.per_type,
                                          Decode(Nvl(Per.Natural_Con_Rut,'NO'), 'SI', 'PNATUR', per.per_type)) per_type
      From ts_personas per
     Where per.id = nPerId;
   vTipoPersona   Varchar2(20);
  BEGIN
    BoExito       := TRUE;
    NFase         := 0;
    NCtrlEfectivo := 0;
    --Identifica si la asesoría posee fases de análisis
    OPEN CFase_Analisis;
    FETCH CFase_Analisis
      INTO NFase;
    CLOSE CFase_Analisis;
    --Trae los datos del servicio
    OPEN CServicio;
    FETCH CServicio
      INTO NCont;
    CLOSE CServicio;
    --Identifica si posee servicios efectivos
    OPEN CEfectivos;
    FETCH CEfectivos
      INTO NCtrlEfectivo;
    CLOSE CEfectivos;
    --Modifica Jorge Naranjo 20080125
    --Identifica si existen servicios de CDT
    OPEN cEsCdt;
    FETCH cEsCdt INTO nEsCdt;
    CLOSE cEsCdt;
    nEsCdt := NVL(nEsCdt,0);
    --Identifica si existen servicios diferentes a CDT
    OPEN cNoEsCdt;
    FETCH cNoEsCdt INTO nNoEsCdt;
    CLOSE cNoEsCdt;
    nNoEsCdt := NVL(nNoEsCdt,0);
    rastro('nEsCdt '||nEsCdt||'  '||'nNoEsCdt '||nNoEsCdt,'DiligSol');
    --Fin modificación 20080125

    --Si solo posee servicios de ahorro o recaudo
    IF NVL(NCont, 0) = 0 THEN
      -- 1
      NCant := 0;
      --Modifica Jorge Naranjo 20080125
      --Si los servicios requieren diligenciar solicitud, se verifica para cada cliente de la asesoría, que posea
      --servicios efectivos o en estudio con número de cuenta asignada o aprobados.
      --Verifica que cada cliente posea servicios en estados que permitan hacer efectivo los solicitados en la asesoría
      bCumpleServicios := FALSE;
      FOR cAper IN cAperson LOOP
        nPoseeServicio := 0;
        OPEN cExisteAsesor(cAper.per_id);
        FETCH cExisteAsesor INTO nPoseeServicio;
        CLOSE cExisteAsesor;
        rastro('nPoseeServicio '||nPoseeServicio,'DiligSol');
        IF NVL(nPoseeServicio,0) = 0 THEN
          bCumpleServicios := FALSE;
          rastro('bCumpleServicios false cAper.per_id '||cAper.per_id,'bCumpleServicios');
          EXIT;
        ELSE
          bCumpleServicios := TRUE;
          rastro('bCumpleServicios truecAper.per_id '||cAper.per_id,'bCumpleServicios');
        END IF;
      END LOOP;
      --Fin modificación 20080125
      FOR CSer IN CSer_Ahorros LOOP
        -- Loop 1
        NCant := NCant + 1;
        VServicios(NCant) .Consecutivo_Servicio := CSer.Consecutivo_Servicio;
        -- Se verifica que el servicio no tenga asignado
        -- un número de cuenta, caso en el cual se suspende el proceso.
        IF CSer.Numero_Servicio IS NOT NULL THEN
          -- 1a
          VoMsjusu := 'El servicio ya posee numero de servicio asignado, proceso cancelado';
          BoExito  := FALSE;
        END IF; -- Fin 1a
        IF NOT BoExito THEN
          EXIT;
        END IF;
        OPEN CSerAho;
        FETCH CSerAho INTO nReqSolicitud;
        CLOSE CSerAho;
        rastro('NCont '||NCont||' nReqSolicitud '||nReqSolicitud ,'asignroctaaho');
        --Si el servicio no requiere fases
        --Modifica Jorge Naranjo 20080125 IF NVL(NFase, 0) = 0 por IF NVL(NFase, 0) = 0 OR bCumpleServicios
        --IF NVL(NFase, 0) = 0 THEN
        -- 31796 JMarquez 20140401 (M) Planilla 15v2 punto 5 todos los servicios de ahorro deben quedar en estudio.
        --IF ( NVL(NFase, 0) = 0 AND bCumpleServicios )
        --          OR ( nNoEsCdt = 1 AND nEsCdt = 1 AND NVL(NFase, 0) = 0 AND bCumpleServicios ) THEN
         IF (( NVL(NFase, 0) = 0 AND bCumpleServicios )
            OR ( nNoEsCdt = 1 AND nEsCdt = 1 AND NVL(NFase, 0) = 0 AND bCumpleServicios ))  AND CSer.Ser_Codigo NOT LIKE '001.02.%' THEN
        -- Fin 31796 JMarquez 20140401 (M)    
          Ncont := NULL;
          /*Modifica Jorge Naranjo 20080125
          OPEN CAhorros(CSer.Consecutivo_Servicio);
          FETCH CAhorros
            INTO NCont;
          CLOSE CAhorros;
          */
          --Modifica Jorge Naranjo 20080125 en comentario IF NVL(NCont, 0) = 0
          --IF NVL(NCont, 0) = 0 THEN
            NCont := 0;
            --Modifica Jorge Naranjo 20080125 en comentario IF NVL(NCtrlEfectivo, 0) = 1
            --IF NVL(NCtrlEfectivo, 0) = 1 THEN
              IF SUBSTR(CSer.Ser_Codigo, 1, 9) = '001.02.02' THEN
                VServicios(NCant) .Tipo_Estado_Decision := Pk_Conseres.Sec_Tipo_Codigo(1264,
                                                                                       '2.01');
                VServicios(NCant) .Estado_Decision := '2.01';
                VServicios(NCant) .Fecha_Efectivo := DFec_Proceso;
              ELSE
                VServicios(NCant) .Tipo_Estado_Decision := Pk_Conseres.Sec_Tipo_Codigo(1264,                                                                                       '2.08');
                VServicios(NCant) .Estado_Decision := '2.08';
                VServicios(NCant) .Fecha_Efectivo := DFec_Proceso;
              END IF;

            --Modifica Jorge Naranjo 20080125 en comentario IF NVL(NCtrlEfectivo, 0) = 1
            /*
            ELSE
              --
              -- Req. 12269
              -- Fecha modificación : 11/07/2006 09:39:53 a.m.
              -- Usuario modifica   : Alberto Eduardo Sánchez B.
              -- Causa modificación :  Cpl0068 "Radicar solicitud definitiva", en el cual se especifica que al momento de radicar
              -- una solicitud debe quedar en estado "EN ESTUDIO" a excepción de varios casos,
              -- entre ellos los servicios de Recaudo a terceros los cuales deben quedar en estado "EFECTIVO" (numeral 4 c)).
              IF SUBSTR(CSer.Ser_Codigo, 1, 6) = '001.04' THEN
                VServicios(NCant) .Tipo_Estado_Decision := Pk_Conseres.Sec_Tipo_Codigo(1264,
                                                                                       '2.08');
                VServicios(NCant) .Estado_Decision := '2.08';
                VServicios(NCant) .Fecha_Efectivo := DFec_Proceso;
              ELSE
              VServicios(NCant) .Tipo_Estado_Decision := Pk_Conseres.Sec_Tipo_Codigo(1264,
                                                                                     '2.01');
              VServicios(NCant) .Estado_Decision := '2.01';
              VServicios(NCant) .Fecha_Efectivo := DFec_Proceso;
              END IF;
              --
              -- Fin Req. 12269
            END IF;
            Modifica Jorge Naranjo 20080125 en comentario IF NVL(NCtrlEfectivo, 0) = 1 */



          /*Modifica Jorge Naranjo 20080125 en comentario IF NVL(NCont, 0) = 0
          ELSE
            VServicios(NCant) .Tipo_Estado_Decision := Pk_Conseres.Sec_Tipo_Codigo(1264,
                                                                                   '2.01');
            VServicios(NCant) .Estado_Decision := '2.01';
            VServicios(NCant) .Fecha_Efectivo := DFec_Proceso;

          END IF;
          Modifica Jorge Naranjo 20080125 en comentario IF NVL(NCont, 0) = 0*/

        ELSE
          -- Fecha modificación : 27/08/2009
          -- Usuario modifica   : Hernán Darío Ossa Sosa
          -- una solicitud debe quedar en estado "EN ESTUDIO" a excepción de los servicios de Recaudo a terceros los cuales deben quedar en estado "EFECTIVO"
          -- asi no posea servicios el cliente, o sea un cliente nuevo.
          IF SUBSTR(CSer.Ser_Codigo, 1, 6) = '001.04' THEN
            VServicios(NCant) .Tipo_Estado_Decision := Pk_Conseres.Sec_Tipo_Codigo(1264,'2.08');
            VServicios(NCant) .Estado_Decision := '2.08';
            VServicios(NCant) .Fecha_Efectivo := DFec_Proceso;
          ELSE
            VServicios(NCant) .Tipo_Estado_Decision := Pk_Conseres.Sec_Tipo_Codigo(1264,'2.01');
            VServicios(NCant) .Estado_Decision := '2.01';
            VServicios(NCant) .Fecha_Efectivo := DFec_Proceso;
          END IF;
         /*VServicios(NCant) .Tipo_Estado_Decision := Pk_Conseres.Sec_Tipo_Codigo(1264,'2.01');
         VServicios(NCant) .Estado_Decision := '2.01';
         VServicios(NCant) .Fecha_Efectivo := DFec_Proceso;*/
         -- Fin modificación cambio estado decisión Fecha modificación : 27/08/2009

        END IF;
        -- Linea Adicionada
        -- Fecha  : 12-09-2008
        -- Usuario: JACEVEDO
        -- 21002 - Si es persona jurídica y la nueva destinación tiene asociada una configuración
        -- de exención para el GMF, se graba la exención automáticamente para la cuenta,
        -- a excepción de consignaciones varias, para la cual se debe seguir el procedimiento normal.
         vTipoPersona := Null;
          Open cTipoPersona( CSer.Per_Id);
         Fetch cTipoPersona Into vTipoPersona;
         Close cTipoPersona;

         -- Esta operación se debe realizar solo para las personas juridicas
         If Nvl(vTipoPersona,'NULO') = 'PJURID' THEN
            nDestino := Null;
             Open cDestino(CSer.tipo_destino);
            Fetch cDestino Into nDestino;
            Close cDestino;
            -- Esto quiere decir que la destinación tiene configurada una exención
            If Nvl(nDestino,0) <> 0 Then
               Insert Into ts_con_gmf_exe_cuentas
                            (aservic_consecutivo_servicio,
                             fecha_inicial,
                             tipo_destino,
                             valor,
                             saldo)
                          Values
                            (CSer.Consecutivo_Servicio,
                             Sysdate,
                             CSer.tipo_destino,
                             nDestino,
                             nDestino);

            End If;
        End If;
        --
      END LOOP; -- Fin loop 1



      IF BoExito THEN
        -- 2
        FOR CAhor IN CAhoAct LOOP
          -- Loop 2
          VConsecutivo_Inicio := NULL;
          VAsignarCons        := 'NO';
          VDocControlado      := 'NO';
          --
          -- Fecha modificación : 30/08/2005 15:25:08
          -- Usuario modifica   : Alberto Eduardo Sánchez B.
          -- Causa modificación : Impide que asigne de nuevo el numero de titulo a otro servicio
          --
          VAhnumero_Titulo := NULL;
          VAhnumero_Serie  := NULL;
          VDigitos_Serie   := NULL;
          RDoc_Ser         := NULL;
          RDocto           := NULL;
          FOR Num IN 1 .. NCant LOOP
            IF VServicios(Num)
            .Consecutivo_Servicio = CAhor.Consecutivo_Servicio THEN
              NCont := Num;
            END IF;
          END LOOP;
          IF NVL(NFase, 0) = 0 AND
             SUBSTR(CAhor.Ser_Codigo, 1, 9) <> ('001.02.02') THEN
            VAsignarCons := 'SI';
          END IF;
          IF VServicios(NCont).Tipo_Estado_Decision IS NOT NULL -- 2a
            AND CAhor.Tipo_Estado_Decision IS NULL AND
             CAhor.Numero_Servicio IS NULL THEN
            IF SUBSTR(CAhor.Ser_Codigo, 1, 9) IN ('001.02.04') THEN
              -- 2b Cuenta Corriente especial
              UPDATE TS_ASE_SERVICIOS
                 SET Tipo_Estado_Decision = VServicios(NCont)
                                            .Tipo_Estado_Decision,
                     Fecha_Efectivo = VServicios(NCont).Fecha_Efectivo,
                     Ahesta_Bloqueado = 'NO', Ahesta_Embargado = 'NO'
               WHERE Consecutivo_Servicio = CAhor.Consecutivo_Servicio;

              ActEstDecision(CAhor.Consecutivo_Servicio,
                             VServicios(NCont).Estado_Decision,
                             NULL,
                             NULL,
                             VoMsjtec,
                             VoMsjusu,
                             BoExito,
                             'NO');
            ELSE
              -- 2b
              IF NVL(NFase, 0) = 0 AND
                 SUBSTR(CAhor.Ser_Codigo, 1, 9) <> ('001.02.02') THEN
                VAsignarCons := 'SI';
              ELSE
                VNroCuenta   := NULL;
                VAsignarCons := 'NO';
              END IF;
              IF SUBSTR(CAhor.Ser_Codigo, 1, 9) = ('001.02.02') THEN
                vAsignaTitulo := 'NO';
              ELSE
                vAsignaTitulo := 'SI';
              END IF;
              IF NVL(NFase, 0) = 0 AND NVL(vAsignaTitulo,'SI') != 'NO' THEN
                -- 2bc
                OPEN CDocto(CAhor.Ser_Codigo);
                FETCH CDocto
                  INTO RDocto;
                CLOSE CDocto;
                IF RDocto.Nivel = 'MANUAL' THEN
                  -- 2c
                  DECLARE
                    i NUMBER;
                  BEGIN
                    FOR i IN 1 .. NiCont LOOP
                      IF ViTitulos(i)
                      .Consecutivo_Servicio = CAhor.Consecutivo_Servicio THEN
                        VConsecutivo_Inicio := ViTitulos(i).Ahnumero_Titulo;
                        VDigitos_Serie      := ViTitulos(i).Ahnumero_Serie;
                      END IF;
                    END LOOP;
                  END;
                ELSE
                  -- 2c
                  IF RDocto.Controlado = 'SI' THEN
                    --2d
                    OPEN APerson(CAhor.Consecutivo_Servicio);
                    FETCH APerson
                      INTO RPerson;
                    CLOSE APerson;
                    Pks_Proc_Solicitudes.Asignar_DocControlado(RDocto.Doc_Id,
                                                               RDocto.Nro_Cupones,
                                                               CAhor.Per_Id,
                                                               CAhor.Consecutivo_Servicio,
                                                               RPerson.Fecha_Inicial,
                                                               VConsecutivo_Inicio,
                                                               VoMsjTec,
                                                               VoMsjUsu,
                                                               BoExito);
                  ELSE
                    OPEN CDcto_Servicio(CAhor.Ser_Codigo);
                    FETCH CDcto_Servicio
                      INTO RDoc_Ser;
                    CLOSE CDcto_Servicio;
                    IF RDoc_Ser.Control_Titulo = 'SI' THEN
                      IF RDoc_Ser.Atipcon_Codigo IS NOT NULL THEN
                        Pks_Consecutivo.Proximo(RDoc_Ser.Atipcon_Codigo,
                                                NULL,
                                                VConsecutivo_Inicio,
                                                VDigitos_Serie);
                      ELSE
                        BoExito  := FALSE;
                        VoMsjusu := 'El codigo del consecutivo asociado al proceso, presenta inconsistencias';
                      END IF;
                      IF VConsecutivo_Inicio IS NULL THEN
                        BoExito  := FALSE;
                        VoMsjusu := 'El consecutivo asociado al proceso, presenta inconsistencias';
                      ELSE
                        BoExito := TRUE;
                      END IF;
                    END IF;
                  END IF; -- Fin 2d
                  VAhnumero_Titulo := NULL;
                  VAhnumero_Serie  := NULL;
                  IF VConsecutivo_Inicio IS NOT NULL AND
                     (RDocto.Control_Titulo = 'SI' OR
                     RDoc_Ser.Control_Titulo = 'SI') THEN
                    -- 2e
                    IF NVL(RDocto.Maneja_Serie, 'NO') = 'SI' THEN
                      VAhnumero_Titulo := SUBSTR(VConsecutivo_Inicio,
                                                 (-1 * (NVL(NVL(RDocto.Numero_Digitos,
                                                                RDoc_Ser.Numero_Digitos),
                                                            10))),
                                                 ((NVL(NVL(RDocto.Numero_Digitos,
                                                           RDoc_Ser.Numero_Digitos),
                                                       10))));
                      IF NVL(RDocto.Digitos_Serie, 0) > 2 THEN
                        BoExito  := FALSE;
                        VoMsjUsu := 'Imposible asignar el documento controlado, Se han asignado más de 2 digitos a la serie del documento';
                      ELSE
                        VAhnumero_Serie := SUBSTR(VConsecutivo_Inicio,
                                                  1,
                                                  NVL(RDocto.Digitos_Serie,
                                                      RDoc_Ser.Digitos_Serie));
                      END IF;
                    ELSE
                      VAhnumero_Titulo := SUBSTR(VConsecutivo_Inicio,
                                                 (-1 * (NVL(NVL(RDocto.Numero_Digitos,
                                                                RDoc_Ser.Numero_Digitos),
                                                            10))),
                                                 ((NVL(NVL(RDocto.Numero_Digitos,
                                                           RDoc_Ser.Numero_Digitos),
                                                       10))));
                      VAhnumero_Serie  := NULL;
                    END IF;
                  END IF;
                END IF; -- Fin 2c
              ELSE
                -- 2bc
                VAhnumero_Titulo := NULL;
                VAhnumero_Serie  := NULL;
              END IF; -- Fin 2bc
              --Jorge Naranjo 20061231 Cálculo de la primera cuota para Ahorro Contractual
              --Jaqueline Glen 20070113 Se agregó los recaudos a terceros
              IF (SUBSTR(CAhor.Ser_Codigo,1,9) IN ('001.02.03') OR SUBSTR(CAhor.Ser_Codigo,1,6) IN ('001.04')) THEN
                OPEN cServPrimeraCuota( CAhor.Consecutivo_Servicio );
                FETCH cServPrimeraCuota INTO rowAservic;
                CLOSE cServPrimeraCuota;
                dFechaPrimeraCuota := NULL;
                BEGIN
                   Pks_Acs_Recaudo_Tercero.Obtiene_fecha_primera_cuota( rowAservic,
                                                                        voMsjTec,
                                                                       voMsjUsu,
                                                                       boExito );
                  IF boExito THEN
                     dFechaPrimeraCuota := rowAservic.fecha_primera_cuota;
                  END IF;
                END;
              END IF;
              --Fin modificación 20061231
              IF BoExito THEN

                UPDATE TS_ASE_SERVICIOS
                   SET Tipo_Estado_Decision = VServicios(NCont)
                                              .Tipo_Estado_Decision,
                       Solicitar_Consecutivo = VAsignarCons,
                       Ahnumero_Titulo = NVL(VAhnumero_Titulo,
                                              VConsecutivo_Inicio),
                       Ahnumero_Serie = NVL(VAhnumero_Serie, VDigitos_Serie),
                       Fecha_Efectivo = VServicios(NCont).Fecha_Efectivo,
                       Ahesta_Bloqueado = 'NO', Ahesta_Embargado = 'NO',
                       Fecha_Primera_Cuota = dFechaPrimeraCuota
                 WHERE Consecutivo_Servicio = CAhor.Consecutivo_Servicio;
                ActEstDecision(CAhor.Consecutivo_Servicio,
                               VServicios(NCont).Estado_Decision,
                               NULL,
                               NULL,
                               VoMsjtec,
                               VoMsjusu,
                               BoExito,
                               'NO');
              END IF;
              --
              -- Fecha modificación : 16/09/04 11:57:21 a.m.
              -- Usuario modifica   : Alberto Eduardo Sánchez B.
              -- Causa modificación : Se adiciono en el Acta 324 Cuando se haga efectivo
              -- un servicio o se asigne número de cuenta a un servicio en estudio (para el
              -- caso de cuentas de ahorro) cuya forma de pago es débito automático,
              -- se debe almacenar en TS_ASE_DEBITO_AUTOMATICO la fecha actualizada de la
              -- próxima cuota.
              --
              IF CAhor.Aforpag_Nombre = 'DEBITO' THEN
                UPDATE TS_ASE_DEBITO_AUTOMATICOS Adebaut
                   SET Adebaut.Fecha_Proxima_Cuota = CAhor.Ahfecha_Inicio_Ahorro
                 WHERE Adebaut.Aservic_Consecutivo_Servicio =
                       CAhor.Consecutivo_Servicio;
              END IF;

            END IF; -- Fin 2b

            -- LINEA ELIMINADA
            -- FECHA  : 28-02-2008
            -- USUARIO: JACEVEDO
            -- SE ELIMINA EL PROCESO DE ESTADOS DE DECISION DEBIDO A QUE SE DEBE
            -- COLOCAR EL ESTADO ACTIVO, SOLO PARA LOS SERVICIO CON NUMERO DE SERVICIO
            -- EN ESTA FUNCION NO SE HA ASIGNADO DICHO NUMERO
            /*
            -- Activar usuarios
            IF BoExito AND NVL(NFase, 0) = 0 THEN
              FOR CAho IN CAhorros(CAhor.Consecutivo_Servicio) LOOP

                  -- Linea Adicionada
                  -- Fecha : 01-02-2008
                  -- Usuario: JACEVEDO Æ
                  -- Se verifica si el servicio tiene estado el numero de servicio,
                  -- si es asi se evalua el estado de la persona
                  vNumeroServicio := Null;
                   Open cNumeroServicio(CAhor.Consecutivo_Servicio);
                  Fetch cNumeroServicio Into vNumeroServicio;
                  Close cNumeroServicio;

                  Rastro(' ',' ');
                  Rastro(CAhor.Consecutivo_Servicio,'Servicio');

                  If vNumeroServicio Is Not Null Then
                      -- En caso que el solicitante presente el estado "POSIBLE CLIENTE"
                      -- ó "CLIENTE INACTIVO" se pasa a  "CLIENTE ACTIVO", registrando
                      -- la fecha, hora y usuario.
                      OPEN CEstado_Cliente(CAho.Per_Id);
                      FETCH CEstado_Cliente
                        INTO VEstado;
                      CLOSE CEstado_Cliente;
                      IF VEstado IN ('POSIBL', 'INACTI') THEN
                        UPDATE TS_PER_ESTADOS
                           SET Fecha_Final = DFec_Proceso
                         WHERE Per_Id = CAho.Per_Id AND Fecha_Final IS NULL;

                        INSERT INTO TS_PER_ESTADOS
                          (Per_Id, Fecha_Inicial, Estado_Persona, u_Creacion,
                           f_Creacion, Fecha_Final)
                        VALUES
                          (CAho.Per_Id, DFec_Proceso, 'ACTIVO', USER, SYSDATE,
                           NULL);
                      END IF;
                      Rastro('Con Número','Estado');
                  Else
                     Rastro('Sin Número','Estado');
                  End If;
                  Rastro(' ',' ');
                -- Hasta Aqui
              END LOOP;
            END IF;
            */
            -- Hasta Aqui se elimina

          ELSE
            BoExito  := FALSE;
            VoMsjusu := VoMsjusu ||
                        'No fue posible actualizar el estado de decisión, ya posee uno';
          END IF; -- Fin 2a
        END LOOP;
      END IF; -- Fin 2
    ELSE
      BoExito := TRUE;
    END IF; -- Fin 1
  EXCEPTION
    WHEN eErrorCuota THEN
        BoExito  := FALSE;
        Rastro('Error al insertar ts_ase_cuotas. '||SQLCODE||'-'||VoMsjtec,'');
        VoMsjusu := 'Error al asignar el próximo consecutivo, verifique el LOG.';
        RETURN;
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.AsignarNroCtaAhorros',
                                       'Error no fue posible asignar el número de cuenta',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en asignar Nro CtaAhorros, verifique el LOG';
  END AsignarNroCtaAhorros;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 12:00:00 p.m.
  -- Este proceso calcula las cuotas de afiliacion que debe pagar
  -- para cada servicio exigido que posee un servicio de afiliacion
  -- se devuelven los servicios calculados en el vector  roSerExig,
  -- donde se indica el codigo del servicio, el valor y la forma de amortizacion.

  PROCEDURE CalcularCuotasAfiliacion(ViSer         IN VARCHAR2, --Codigo del servicio
                                     RiIngresos    IN Pks_Proc_Solicitudes.TIngresos, --Registro donde estan identificado los ingeresos de la persona
                                     NiPerId       IN NUMBER, --Id de la persona
                                     VPerLiquidSer IN VARCHAR2, --Periodo de liquidacion del servicio
                                     RoSerExig     OUT Pks_Proc_Solicitudes.RSerExig, --Vector que contiene el valor de las cuotas de afiliacion por cada servicio exigido
                                     NoNroSerExig  OUT NUMBER --Cantidad de servicios de afiliacion que posee la persona
                                     ) IS

    CURSOR CParSer(Viserv IN VARCHAR2) IS
      SELECT a.Tipo_Persona Tpersona, a.Cuota_Minima CMINIMA,
             a.Cuota_Maxima Cmaxima
        FROM VS_SER_AFILIACIONES a
       WHERE a.Ser_Codigo = ViServ OR a.Ser_Codigo = SUBSTR(ViServ, 1, 9) OR
             a.Ser_Codigo = SUBSTR(ViServ, 1, 6)
       ORDER BY a.Ser_Codigo, SUBSTR(a.Ser_Codigo, 1, 9),
                SUBSTR(a.Ser_Codigo, 1, 6);

    --Agosto 19 de 2003 -- Moav -- Se corrigio la vista de vs_ser_afi_cuo_aplicaciones
    --por vs_ser_afi_cuotas por cambio del modelo
    CURSOR CAfiCou(Viserv IN VARCHAR2) IS
      SELECT Ca.Forma_Amortizacion FAmortiza, Ca.Tipo_Cuota Tcuota,
             Ca.Base_Porcentaje Bporcent, Ca.Porcentaje_Mensual Pmensual
        FROM VS_SER_AFI_CUOTAS Ca
       WHERE Ca.Ser_Codigo = ViServ OR Ca.Ser_Codigo = SUBSTR(ViServ, 1, 9) OR
             Ca.Ser_Codigo = SUBSTR(ViServ, 1, 6)
       ORDER BY Ca.Ser_Codigo, SUBSTR(Ca.Ser_Codigo, 1, 9),
                SUBSTR(Ca.Ser_Codigo, 1, 6);

    CURSOR CPSVfijo(Viserv IN VARCHAR2) IS
      SELECT Cf.Basico_Minimo Bminimo, Cf.Basico_Maximo Bmaximo, Cf.Valor
        FROM VS_SER_AFI_CUOTA_FIJAS Cf
       WHERE Cf.Ser_Codigo = ViServ OR Cf.Ser_Codigo = SUBSTR(ViServ, 1, 9) OR
             Cf.Ser_Codigo = SUBSTR(ViServ, 1, 6)
       ORDER BY Cf.Ser_Codigo, SUBSTR(Cf.Ser_Codigo, 1, 9),
                SUBSTR(Cf.Ser_Codigo, 1, 6);

    CURSOR CSerEdad(Viserv IN VARCHAR2) IS
      SELECT Se.Edad_Minima Eminima, Se.Edad_Maxima Emaxima
        FROM VS_SER_EDADES Se
       WHERE Se.Ser_Codigo = ViServ OR Se.Ser_Codigo = SUBSTR(ViServ, 1, 9) OR
             Se.Ser_Codigo = SUBSTR(ViServ, 1, 6)
       ORDER BY Se.Ser_Codigo, SUBSTR(Se.Ser_Codigo, 1, 9),
                SUBSTR(Se.Ser_Codigo, 1, 6);

    RRegSer     RSerExig;
    RcParSer    CParSer%ROWTYPE;
    RcPSVfijo   CPSVfijo%ROWTYPE;
    RegcSerEdad CSerEdad%ROWTYPE;

    i               PLS_INTEGER := 0;
    j               PLS_INTEGER; --
    NEdadPer        NUMBER;
    NCuotaCancelar  NUMBER;
    NSalarioMensual NUMBER;
    RegcAfiCou      CAfiCou%ROWTYPE;
    --  grAseServicioCre    Pks_proc_solicitudes.rAseServicioCre;

  BEGIN
    NEdadPer := Pks_Genpersonas.Obteneredadper(NiPerId);
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(RiIngresos.Consecutivo_Servicio);
    END IF;

    --   grAseServicioCre:= Pks_proc_solicitudes.ParGenAseServicio(riIngresos.consecutivo_servicio);
    --se buscan cuales son los servicios exigidos para el servicio
    Rastro(ViSer, 'Servicio solicitado en calcular cuota afiliacion ', 3);
    --   pks_proc_solicitudes.Ser_Exigidos(viSer,rRegSer,j);

    --se verifica si el salario reportado es diferente a mensual,
    --en este caso se deben llevar amensual segun formula3 del cpl0129
    IF GrAseServicioCre.Aforpag_Nombre <> 'MENSUAL' THEN
      NSalarioMensual := ConvIngBasico(GrAseServicioCre.Aforpag_Nombre,
                                       RiIngresos.Ingresos_Basicos);
    ELSE
      NSalarioMensual := RiIngresos.Ingresos_Basicos;
    END IF;
    NoNroSerExig   := j;
    NCuotaCancelar := 0;
    Rastro(NoNroSerExig, 'noNroSerExig', 3);
    I := 1;
    WHILE i <= j LOOP
      IF SUBSTR(RRegSer(i).Codigo, 1, 6) = '001.01' THEN
        RoSerExig(i) .Codigo := RRegSer(i).Codigo;
        Rastro(RoSerExig(i).Codigo, 'ser exigido', 3);
        OPEN CAfiCou(RRegSer(i).Codigo);
        FETCH CAfiCou
          INTO RegcAfiCou;
        CLOSE CAfiCou;
        OPEN CParSer(RRegSer(i).Codigo);
        FETCH CParSer
          INTO RcParSer;
        RoSerExig(i) .Famortizar := RegcAfiCou.FAmortiza;
        IF NOT CParSer%NOTFOUND THEN
          --(1)
          IF (RcParSer.Tpersona = 'NATURA') OR
             (RcParSer.Tpersona = 'AMBAS') THEN
            --(2)
            OPEN CSerEdad(RRegSer(i).Codigo);
            FETCH CSerEdad
              INTO RegcSerEdad;
            CLOSE CSerEdad;
            IF NEdadPer < RegcSerEdad.Emaxima AND --(3)
               RegcAfiCou.FAmortiza = 'PERIOD' THEN
              IF RegcAfiCou.Tcuota = 'PORCEN' THEN
                --(4)
                IF RegcAfiCou.Bporcent = 'SMLMV' THEN
                  NCuotaCancelar := (RegcAfiCou.Pmensual / 100) *
                                    Pk_Conseres.Param_Gnral('GSMLMV');
                ELSE
                  NCuotaCancelar := (RegcAfiCou.Pmensual / 100) *
                                    NSalarioMensual;
                END IF;
              ELSE
                --la cuota es valor fijo
                FOR CdcPVfijo IN CPSVfijo(RRegSer(i).Codigo) LOOP
                  IF NSalarioMensual >= CdcPVfijo.Bminimo AND
                     NSalarioMensual <= CdcPVfijo.Bminimo THEN
                    NCuotaCancelar := RcPSVfijo.Valor;
                    EXIT;
                  END IF;
                END LOOP;
              END IF; --(4)
              --se determina si la cuota hallada esta entre la cuota maxima o minima pemitida por la linea
              IF NCuotaCancelar >= RcParSer.Cmaxima THEN
                NCuotaCancelar := RcParSer.Cmaxima;
              END IF;
              IF NCuotaCancelar <= RcParSer.Cminima THEN
                NCuotaCancelar := RcParSer.Cminima;
              END IF;

              --se determina el periodo de pago y de liquidacion de la linea, y se
              --se recalcula la cuota asi:
              IF GrAseServicioCre.Aforpag_Nombre <> 'MENSUAL' THEN
                IF VPerLiquidSer = 'MENSUAL' THEN
                  NCuotaCancelar := F118(NULL,
                                         GrAseServicioCre.Aforpag_Nombre,
                                         NCuotaCancelar);
                ELSE
                  NCuotaCancelar := CuotaPerPeriodoPagoMensual(GrAseServicioCre.Aforpag_Nombre,
                                                               NCuotaCancelar);
                END IF;
              END IF;
            END IF; --(3)
          END IF; --(2)
        END IF; --(1)
        CLOSE CParSer;
        --se devuelven los servicios exigidos con la afiliacion y su cuota calculada
        RoSerExig(i) .Cuota := NCuotaCancelar;
        DBMS_OUTPUT.PUT_LINE(RoSerExig(i).Cuota);
        DBMS_OUTPUT.Put_Line(RoSerExig(i).Codigo);
        Rastro(RoSerExig(i).Cuota, 'CUOTA', 3);
        Rastro(RoSerExig(i).Codigo, 'SER AFIL', 3);
      END IF;
      I := I + 1;
    END LOOP;
  END CalcularCuotasAfiliacion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/03/2004 01:30:00 p.m.
  -- Esta funcion devuelve los codeudores que respalda un credito
  -- en un vector, ademas nrocodeudor indica cuantos tiene el credito

  PROCEDURE Codeudores(NiConsSer   IN NUMBER,
                       VotCodeudor OUT Pks_Proc_Solicitudes.VtCodeudor,
                       NroCodeudor OUT PLS_INTEGER) IS

    --cursor para busca el codeudor del credito
    CURSOR CCodeudor IS
      SELECT c.Per_Id
        FROM TS_ASE_PERSONAS c
       WHERE c.Aservic_Consecutivo_Servicio = NiConsSer AND
             c.Aperson_Type = 'CRECOD' AND c.Fecha_Final IS NULL;

    i PLS_INTEGER;

  BEGIN
    i := 0;
    FOR CdcCodeudor IN CCodeudor LOOP
      i := i + 1;
      VotCodeudor(i) .Perid := CdcCodeudor.Per_Id;
    END LOOP;
    NroCodeudor := i;
  END Codeudores;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/03/2004 01:30:00 p.m.
  -- Determina el tipo de consecutivo del proceso a ejecutar.
  -- Fredy Sanchez Castro   26/09/06 08:30:00 a.m.
  -- Se agregan parametros para el renovacion masiva de servicios de recaudo a terceros
  -- (biRenovacion, niOficina)
  -- JHgutier: 28-10-2006. Parámetro biRenovación para instruir al programa que
  -- se está llamando desde la Renovación Masiva de Recaudo a Terceros

  PROCEDURE Consecutivo(NiProceso    IN NUMBER, -- Codigo del proceso
                        --                       diFecha   IN DATE,     -- Fecha de vigencia
                        VoConsec     OUT VARCHAR2, -- Consecutivo encontrado
                        VoMsjtec     OUT VARCHAR2,
                        VoMsjusu     OUT VARCHAR2,
                        BoExito      OUT BOOLEAN,
                        biRenovacion IN BOOLEAN DEFAULT FALSE, -- JHGUTIER 28-10-2006
                        niOficina    IN  NUMBER  DEFAULT NULL) IS

    CURSOR CProceso IS
      SELECT Atipcon.Codigo Tipoconsec
        FROM TS_ADM_PROCESOS Aproces, TS_ADM_TIPO_CONSECUTIVOS Atipcon
       WHERE Atipcon.Codigo = Aproces.Atipcon_Codigo AND
             Aproces.CODIGO = NiProceso;

    VConsecutivo VARCHAR2(38);
    NCon_Comprob NUMBER(3);
    VSerie       VARCHAR2(38);

  BEGIN
    OPEN CProceso;
    FETCH CProceso
      INTO NCon_Comprob;
    CLOSE CProceso;
    IF NCon_Comprob IS NULL THEN
      VoMsjusu := 'La configuración del consecutivo, no aparece asociada al proceso de Asignar radicado a la solicitud';
      BoExito  := FALSE;
    ELSE
      -- JHGUTIER 28-10-2006. Si no es llamado por Renovación masiva de recaudo
      -- a terceros se deja que el programa calcule la oficina
      IF NOT biRenovacion THEN
        Pks_Consecutivo.Proximo(NCon_Comprob, NULL, VConsecutivo, VSerie);
      -- JHGUTIER 28-10-2006. Si es llamado por Renovación masiva de recaudo
      -- a terceros se usa el parámetro de oficina pasado
      ELSE
        Pks_Consecutivo.Proximo(NCon_Comprob
          , NULL
          , VConsecutivo
          , VSerie
          , SYSDATE
          , FALSE
          , niOficina);
      END IF;
      -- JHGUTIER 28-10-2006. *** FIN ****

      IF VConsecutivo IS NULL THEN
        BoExito  := FALSE;
        VoMsjusu := 'El consecutivo asociado al proceso, presenta inconsistencias';
      ELSE
        VoConsec := VConsecutivo;
        BoExito  := TRUE;
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE = -20301 THEN
        BoExito  := FALSE;
        VoMsjusu := 'No fue posible asignar Próximo Consecutivo. Intente la operación en unos minutos.';
      ELSIF SQLCODE = -20751 THEN
        BoExito  := FALSE;
        VoMsjusu := 'No existe un consecutivo configurado. Por favor verifíquelo para continuar con el proceso.';
      ELSE
        Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.CONSECUTIVO',
                                         'Error al consultar el consecutivo',
                                         SQLCODE,
                                         Vomsjtec,
                                         Boexito);
        VoMsjusu := 'Se presento un error tecnico en asignar consecutivo, verifique el LOG';
        CLOSE CProceso;
      END IF;
  END Consecutivo;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 02/04/2004 03:00:00 p.m.
  -- Retorna la compañia del cliente
  -- teniendo el per_id y el codigo de la dependencia.
  -- Verifica que la compañia a la que pertenece el convenio de deducción de nómina o la dependencia, exista

  PROCEDURE CompaniaServicio(NiPer_Id IN NUMBER, -- Id del cliente
                             NiId     OUT NUMBER, -- id de la compañía
                             VoMsjtec OUT VARCHAR2,
                             VoMsjusu OUT VARCHAR2,
                             BoExito  OUT BOOLEAN) IS

    NOficina NUMBER(3);

    CURSOR CCompanias(NiOficina NUMBER) IS
      SELECT ACOMPAN.ID
        FROM TS_ADM_COMPANIAS ACOMPAN, TS_PER_PERSONA_OCUPACIONES PPEROCU
       WHERE PPEROCU.PER_ID_VINCULADO_A = ACOMPAN.PER_ID AND
             PPEROCU.PER_ID = NiPer_Id
      UNION
      SELECT ACOMPAN.ID
        FROM TS_ADM_COMPANIAS ACOMPAN, TS_ADM_DEPENDENCIAS ADEPEND
       WHERE ACOMPAN.ADEPEN_ID = ADEPEND.ID AND ADEPEND.CODIGO = NiOficina;

  BEGIN
    NOficina := Pks_Sessiones.RETORNAR_OFICINA;
    OPEN CCompanias(NOficina);
    FETCH CCompanias
      INTO NiId;
    CLOSE CCompanias;
    IF NiId IS NULL THEN
      VoMsjusu := 'Verifique que la compañia a la que pertenece el convenio de deducción de nómina o la dependencia, exista';
      BoExito  := FALSE;
    ELSE
      BoExito := TRUE;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.COMPANIA_SERVICIO',
                                       'Error al consultar la compania del servicio',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en compañia servicio, verifique el LOG';
      CLOSE CCompanias;
  END CompaniaServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 12:00:00 p.m.
  -- Retorna la información de cupo rotativo
  -- teniendo el consecutivo del cupo rotativo.

  PROCEDURE CupoRotativo(NiConsecutivo_Servicio IN TS_ASE_SERVICIOS.ASERVIC_DESEMBOLSO_PARCIAL%TYPE, -- Consecutivo del servicio
                         VoNumero_Servicio      OUT TS_ASE_SERVICIOS.NUMERO_SERVICIO%TYPE, -- Número del servicio
                         Voaforpag_Nombre       OUT TS_ASE_SERVICIOS.AFORPAG_NOMBRE%TYPE, -- Forma de pago
                         Voatipper_Pago         OUT TS_ASE_SERVICIOS.ATIPPER_PAGO%TYPE, -- Período de pago
                         Voatipper              OUT TS_ASESORIAS.ATIPPER_LIQUIDACION%TYPE, -- Período de liquidación
                         VoConvenio             OUT VARCHAR2, -- Nombre del Convenio
                         VoEmpresa              OUT VARCHAR2, -- Nombre de la empresa
                         VoMsjtec               OUT VARCHAR2,
                         VoMsjusu               OUT VARCHAR2,
                         BoExito                OUT BOOLEAN) IS

    CURSOR CServicios IS
      SELECT Aservic.Numero_Servicio Numero_Servicio,
             Aservic.Aforpag_Nombre Aforpag_Nombre,
             Aservic.Atipper_Pago Atipper_Pago,
             Empleadorservicio(Aservic.Pperocu_Id) Dsp_Empresario,
             Aservic.Atipper_Liquidacion Atipper_Liquidacion,
             Convenioservicio(Aservic.Pperocu_Id) Dsp_Nombre_Covenio
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConsecutivo_Servicio AND
             Aservic.Rocupo_Disponible > 0;

  BEGIN
    OPEN CServicios;
    FETCH CServicios
      INTO VoNumero_Servicio, Voaforpag_Nombre, Voatipper_Pago, Voatipper, VoConvenio, VoEmpresa;
    CLOSE CServicios;
    IF Voaforpag_Nombre IS NULL THEN
      VoMsjusu := 'El servicio de credito rotativo no se encuentra matrículado';
      BoExito  := FALSE;
    ELSE
      BoExito := TRUE;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.CUPO_ROTATIVO',
                                       'Error al consultar el cupo rotativo',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en cupo rotativo, verifique el LOG';
      CLOSE CServicios;
  END CupoRotativo;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/03/2004 01:00:00 p.m.
  -- Retorna el id y el nombre de la dependencia, teniendo el codigo de la misma.

  PROCEDURE Dependencia(NiCodigo IN NUMBER, -- Codigo de la dependencia
                        NoId     OUT NUMBER, -- Id de la dependencia
                        VoNombre OUT VARCHAR2, -- Nombre de la dependencia
                        VoMsjtec OUT VARCHAR2,
                        VoMsjusu OUT VARCHAR2,
                        BoExito  OUT BOOLEAN) IS

    CURSOR CDependencia IS
      SELECT Adepend.Id, Adepend.Nombre
        FROM TS_ADM_DEPENDENCIAS Adepend
       WHERE Adepend.Id = NiCodigo OR Adepend.Codigo = NiCodigo;

  BEGIN
    OPEN CDependencia;
    FETCH CDependencia
      INTO NoId, VoNombre;
    CLOSE CDependencia;
    IF NoId IS NOT NULL THEN
      BoExito := TRUE;
    ELSE
      BoExito  := FALSE;
      VoMsjusu := 'La dependencia indicada no existe, por favor verifique';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.DEPENDENCIA',
                                       'Error al consultar la dependencia',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en dependencia, verifique el LOG';
      CLOSE CDependencia;
  END Dependencia;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/04/2004 05:00:00 p.m.
  -- Si el cliente posee servicios de ahorro, créditos o afiliación
  -- en estado EFECTIVO, se verifica para todos los servicios solicitados
  -- en la asesoría si no se requiere elaboración física del documento
  -- de solicitud para clientes que poseen servicios de ahorro,
  -- créditos o afiliación ¿EFECTIVOS¿, caso en el cual,
  -- el estado de control de la solicitud pasa a
  -- ¿SOPORTE ARCHIVADO EN LA OFICINA¿.

  PROCEDURE DiligenciarSol(NiConsec_Asesor IN TS_ASESORIAS.Consecutivo_Asesoria%TYPE,
                           VoMsjtec        OUT VARCHAR2,
                           VoMsjusu        OUT VARCHAR2,
                           BoExito         OUT BOOLEAN,
                           VioEstControl   IN OUT VARCHAR2) IS

    NCont           NUMBER;
    NEstadoEfectivo NUMBER := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08');
    NEstadoEstudio  NUMBER := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01');
    NEstadoAprobado NUMBER := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.05');--Modifica Jorge Naranjo 20080125 agrego estado
    VEstadoControl  VARCHAR2(20) := '1.12'; -- Estado de Control Soporte archivado en la oficina
    NTipoSolicAho   NUMBER(2) := 10; -- Tipo de solicitud por defecto para aquellas que sólo tienen servicios de ahorro
    NTipoSolicCrAf  NUMBER(2) := 5; -- Tipo de solicitud por defecto para aquellas que tienen servicios de crédito o afiliación
    NPerId          NUMBER;
    -- Identificación del cliente
    CURSOR CAsesorias IS
      SELECT Ase.Per_Id
        FROM TS_ASESORIAS Ase
       WHERE Ase.Consecutivo_Asesoria = NiConsec_Asesor;

    -- Servicios en estado efectivo del mismo cliente en otras solicitudes
    CURSOR CServicios IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Tipo_Estado_Decision = NEstadoEfectivo AND
             Aservic.Per_Id = NPerId AND
             Aservic.Ase_Consecutivo_Asesoria <> NiConsec_Asesor;

    -- Servicios estado en estudio, con numero de servicio asignado del mismo cliente en otras solicitudes
    CURSOR CServiciosEst IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Tipo_Estado_Decision = NEstadoEstudio AND
             Aservic.Numero_Servicio IS NOT NULL AND
             Aservic.Per_Id = NPerId AND
             Aservic.Ase_Consecutivo_Asesoria <> NiConsec_Asesor;

    -- Clientes en estado diferente a cliente activo que intervienen en la solicitud
    CURSOR CClientesNuevos IS
      SELECT COUNT(1)
        FROM TS_ASE_PERSONAS Aperson, TS_ASE_SERVICIOS Aservic,
             TS_PER_ESTADOS Pestad
       WHERE Aperson.Aservic_Consecutivo_Servicio =
             Aservic.Consecutivo_Servicio AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             Pestad.Fecha_Final IS NULL AND
             Pestad.Estado_Persona <> 'ACTIVO' AND
             Aperson.Aperson_Type IN ('AHOTIT', 'AHOAUT', 'SOLICI') AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.02' AND
             Aperson.Per_Id = Pestad.Per_Id;

    -- Clientes distintos al solicitante en estado diferente a cliente activo que intervienen en la solicitud
    CURSOR CClientesSecNuevos IS
      SELECT COUNT(1)
        FROM TS_ASE_PERSONAS Aperson, TS_ASE_SERVICIOS Aservic,
             TS_PER_ESTADOS Pestad
       WHERE Aperson.Aservic_Consecutivo_Servicio =
             Aservic.Consecutivo_Servicio AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             Pestad.Fecha_Final IS NULL AND
             Pestad.Estado_Persona <> 'ACTIVO' AND
             Aperson.Aperson_Type IN ('AHOTIT', 'AHOAUT') AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.02' AND
             Aperson.Per_Id = Pestad.Per_Id;

    -- Servicios en la asesoria diferentes de ahorros (requieren solicitud)
    -- Modifica Jorge Naranjo 20080125 Cuando se radica una solicitud que solo posee servicios de ahorro que no necesitan
    --procesamiento (diferentes a copecheque): Se debe adicionar también el estado de decisión aprobado y en estudio con número de cuenta asignada
    CURSOR CAhorros IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       --WHERE SUBSTR(Aservic.Ser_Codigo, 1, 6) <> '001.02' AND
       WHERE ( SUBSTR(Aservic.Ser_Codigo, 1, 6) != '001.02' OR Aservic.Ser_Codigo = '001.02.04.001' ) AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    -- Configuración de Diligenciamento de solicitud para servicios de ahorros
    CURSOR CSerAho IS
      SELECT COUNT(1)
        FROM VS_SER_AHORROS Sahorro, TS_ASE_SERVICIOS Aservic,
             TS_PERSONAS Per
       WHERE Sahorro.Diligenciar_Solicit = 'SI' AND
             (Sahorro.Tipo_Persona = Per.Per_Type OR
             Sahorro.Tipo_Persona = 'AMBAS') AND
             (Sahorro.Ser_Codigo = Aservic.Ser_Codigo OR
             Sahorro.Ser_Codigo = SUBSTR(Aservic.Ser_Codigo, 1, 6) OR
             Sahorro.Ser_Codigo = SUBSTR(Aservic.Ser_Codigo, 1, 9)) AND
             Aservic.Per_Id = Per.Id AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;
  --Modifica Jorge Naranjo 20080125 Identifica si se pasa el servicio a Efectivo según cambio documento cronograma 2008-1
  --Extrae cada uno de los clientes de la asesoría
  CURSOR cAperson IS
    SELECT aperson.per_id
    FROM ts_ase_personas aperson,ts_ase_servicios aservic
    WHERE aperson.aservic_consecutivo_servicio = aservic.consecutivo_servicio
      AND aservic.ase_consecutivo_asesoria = NiConsec_Asesor;
  --Valida si existe un servicio para el cliente en estado efectivo o estudio con número de cuenta o aprobado
  CURSOR cExisteAsesor (nPerid NUMBER) IS
    SELECT 1
    FROM ts_ase_servicios aservic
    WHERE NVL(aservic.ase_consecutivo_asesoria,0) != NiConsec_Asesor
      AND ( (aservic.tipo_estado_decision IN (nEstadoEstudio,nEstadoEfectivo) AND aservic.numero_servicio IS NOT NULL) OR
            (aservic.tipo_estado_decision IN (nEstadoAprobado))
          )
      -- 25732 (M) 11/05/2011 JNARANJO
      --AND aservic.ASERVIC_TYPE IN ('AAFILIA','ACREDIT','AAHORRO')
      AND aservic.ASERVIC_TYPE IN ('AAFILIA','ACREDIT','AAHORRO','AROTATI')
      -- 25732 11/05/2011 Fin modificación 
      AND aservic.PER_ID = nPerid
      AND aservic.Ase_Consecutivo_Asesoria NOT IN
             ( SELECT asedir.consecutivo_asesoria
               FROM TS_ASESORIAS asedir
               WHERE asedir.consecutivo_asesoria = aservic.Ase_Consecutivo_Asesoria
                 AND asedir.acredir_id IS NOT NULL );
  --identifica si la asesoría posee servicios de CDT
  CURSOR cEsCdt IS
    SELECT COUNT(1)
    FROM TS_ASE_SERVICIOS Aservic
    --34347 JNARANJO 20150115
    --WHERE  Aservic.Ser_Codigo = '001.02.02.003' AND
    WHERE  Aservic.Ser_Codigo = Pks_Utilidad_Servicios.es_CDT(Aservic.Ser_Codigo) AND
    --34347 JNARANJO 20150115 Fin modificación
           Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;
  --identifica si la asesoría posee servicios diferentes a CDT
  CURSOR cNoEsCdt IS
    SELECT COUNT(1)
    FROM TS_ASE_SERVICIOS Aservic
    --34347 JNARANJO 20150115
    --WHERE  Aservic.Ser_Codigo != '001.02.02.003' AND
    WHERE  Aservic.Ser_Codigo != Pks_Utilidad_Servicios.es_CDT(Aservic.Ser_Codigo) AND
    --34347 JNARANJO 20150115 Fin modificación
           Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;
    -- Recupera Informacion para determinar si exite algún servicio que requiere una fase de analisis
    CURSOR CFase_Analisis IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic, TS_ADM_FASE_SERVICIOS Afasser
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             Afasser.Ser_Codigo LIKE '001.02%' AND
             Afasser.Usu_Usuario LIKE 'G_ANALISTA%' AND
             Aservic.Ser_Codigo LIKE (Afasser.Ser_Codigo || '%');
  nPoseeServicio NUMBER;--Almacena si el cliente posee servicios
  bCumpleServicios BOOLEAN := TRUE;--Identifica si posee servicios
  nReqSolicitud NUMBER;--Identifica cuántos servicios requieren solicitud
  NFase NUMBER(1); -- Para determinar si el servicio requiere fase de analisis
  nEsCdt NUMBER := 0;--identifica si la asesoría posee servicios de CDT
  nNoEsCdt NUMBER := 0;--identifica si la asesoría posee servicios diferentes a CDT
  --Fin modificación 20080125
  BEGIN
    -- traer identificacion del cliente
    OPEN CAsesorias;
    FETCH CAsesorias INTO NPerId;
    CLOSE CAsesorias;
    --Identifica si los servicios de la asesoría poseen fases de análisis
    OPEN CFase_Analisis;
    FETCH CFase_Analisis INTO NFase;
    CLOSE CFase_Analisis;
    NFase := NVL(NFase,0);
    --Identifica si la asesoría posee un CDT
    OPEN cEsCdt;
    FETCH cEsCdt INTO nEsCdt;
    CLOSE cEsCdt;
    nEsCdt := NVL(nEsCdt,0);
    --Identifica si la asesoría posee servicios diferentes a CDT
    OPEN cNoEsCdt;
    FETCH cNoEsCdt INTO nNoEsCdt;
    CLOSE cNoEsCdt;
    nNoEsCdt := NVL(nNoEsCdt,0);
    rastro('NFase '||NFase||' '||'nEsCdt '||nEsCdt||' '||'nNoEsCdt '||nNoEsCdt,'DiligSol');
    IF NPerId IS NOT NULL THEN
      -- Verificar si la persona tiene servicios en estado efectivo
      OPEN CServicios;
      FETCH CServicios
        INTO NCont;
      CLOSE CServicios;
      rastro('NCont '||NCont,'DiligSol');
      --Modifica Jorge Naranjo 20080125
      --Si los servicios requieren diligenciar solicitud, se verifica para cada cliente de la asesoría, que posea
      --servicios efectivos o en estudio con número de cuenta asignada o aprobados.
      --Verifica que cada cliente posea servicios en estados que permitan hacer efectivo los solicitados en la asesoría
      bCumpleServicios := FALSE;
      FOR cAper IN cAperson LOOP
        nPoseeServicio := 0;
        OPEN cExisteAsesor(cAper.per_id);
        FETCH cExisteAsesor INTO nPoseeServicio;
        CLOSE cExisteAsesor;
        rastro('nPoseeServicio '||nPoseeServicio||'  cAper.per_id:'||cAper.per_id,'DiligSol');
        IF NVL(nPoseeServicio,0) = 0 THEN
          bCumpleServicios := FALSE;
          EXIT;
        ELSE
          bCumpleServicios := TRUE;
        END IF;
      END LOOP;
      --Fin modificación 20080125

      IF NVL(NCont, 0) > 0 THEN
        -- El cliente tiene servicios en estado efectivo
        -- Verificar si en la solicitud hay servicios diferentes a ahorros
        NCont := 0;
        OPEN CAhorros;
        FETCH CAhorros
          INTO NCont;
        CLOSE CAhorros;
        rastro('NCont '||NCont,'DiligSol');
        IF NVL(NCont, 0) = 0 THEN
          -- En la solicitud sólo hay servicios de ahorros
          -- Busca la configuración si se requiere diligenciar solicitud

          UPDATE TS_ASESORIAS
             SET Tipo_Solicitud = NTipoSolicAho
           WHERE Consecutivo_Asesoria = NiConsec_Asesor;

          OPEN CSerAho;
          FETCH CSerAho
            --INTO NCont;
            INTO nReqSolicitud;
          CLOSE CSerAho;
          nReqSolicitud := NVL(nReqSolicitud,0);
          rastro('NCont '||NCont||' nReqSolicitud '||nReqSolicitud ,'DiligSol');
          --Modifica Jorge Naranjo 20080125 reemplazo la condición NVL(NCont, 0) = 0 por bCumpleServicios, ya que
          --se verifica si la solicitud no requiere ser diligenciada y si los clientes del servicio poseen servicios
          --efectivos o en estudio con número de cuenta asignada o aprobados y si la asesoría no requierre fases
          --O si la asesoría posee un CDT y no posee otros servicios diferentes a CDT.
          --IF NVL(NCont, 0) = 0 THEN
          IF ( bCumpleServicios AND nReqSolicitud = 0 AND NFase = 0 ) OR ( nNoEsCdt = 0 AND nEsCdt = 1 ) THEN
            rastro('cumplea '||NCont,'DiligSol');
            -- No se requuiere diligenciar solicitud
            -- Verifica si hay otras personas en la asesoria que son clientes nuevos

            --Modifica Jorge Naranjo 20080125 el cursor siguiente ya fue evaluado para bCumpleServicios, por lo tanto se
            --deja completamente en comentarios asignando directamente el estado de control VioEstControl := VEstadoControl

            /*OPEN CClientesNuevos;
            FETCH CClientesNuevos
              INTO NCont;
            CLOSE CClientesNuevos;
            IF NVL(NCont, 0) = 0 THEN
              -- No hay clientes nuevos
              -- UPDATE ts_asesorias
              --  SET aestcon_codigo = '1.07'
              --WHERE consecutivo_asesoria = niConsec_asesor;
              VioEstControl := VEstadoControl;
            ELSE
              VioEstControl := NULL;
            END IF;
            */
            VioEstControl := VEstadoControl;
            BoExito := TRUE;
            --Fin modificación 20080125
          ELSE
            -- Se requiere diligenciar solicitud, el estado de control permanece igual
            rastro('cumpleb '||NCont,'DiligSol');
            BoExito := TRUE;
          END IF;
        ELSE
          -- En la solicitud hay servicios distintos a ahorros, el estado de control permanece igual
          UPDATE TS_ASESORIAS
             SET Tipo_Solicitud = NTipoSolicCrAf
           WHERE Consecutivo_Asesoria = NiConsec_Asesor;
          BoExito := TRUE;
          rastro('cumplec '||NCont,'DiligSol');
        END IF;
      ELSE
        -- El cliente no tiene servicios en estado efectivo, el estado de control permanece igual
        NCont := 0;
        OPEN CAhorros;
        FETCH CAhorros
          INTO NCont;
        CLOSE CAhorros;
        rastro('NCont '||NCont,'DiligSol');
        IF NVL(NCont, 0) > 0 THEN
          -- la solicitud tiene servicios de credito o afiliacion
          UPDATE TS_ASESORIAS
             SET Tipo_Solicitud = NTipoSolicCrAf
           WHERE Consecutivo_Asesoria = NiConsec_Asesor;
        ELSE
          -- la solicitud sólo tiene servicios de ahorros
          -- Verificar si tiene servicios en estudio con numero de servicio asignado
          --Modifica Jorge Naranjo 20080125 éste cursor es reemplazado por bCumpleServicios
          /*OPEN CServiciosEst;
          FETCH CServiciosEst
            INTO NCont;
          CLOSE CServiciosEst;*/

          --Modifica Jorge Naranjo 20080125 reemplazo la condición NVL(NCont, 0) = 0 por bCumpleServicios
          --IF NVL(NCont, 0) > 0 THEN
            -- tiene servicios con numero servicio asignado en estudio o aprobado
            OPEN CSerAho;
            FETCH CSerAho
              --INTO NCont;
              INTO nReqSolicitud;
            CLOSE CSerAho;
            nReqSolicitud := NVL(nReqSolicitud,0);
            rastro('NConte '||NCont||' nReqSolicitud '||nReqSolicitud,'DiligSol');
            --Modifica Jorge Naranjo 20080125 reemplazo la condición NVL(NCont, 0) = 0 por bCumpleServicios, ya que
            --se verifica si la solicitud no requiere ser diligenciada y si los clientes del servicio poseen servicios
            --efectivos o en estudio con número de cuenta asignada o aprobados y si la asesoría no requierre fases
            --O si la asesoría posee un CDT y no posee otros servicios diferentes a CDT.
            IF ( bCumpleServicios AND nReqSolicitud = 0 AND NFase = 0 ) OR ( nNoEsCdt = 0 AND nEsCdt = 1 ) THEN
              rastro('NContf '||NCont,'DiligSol');
              -- No se requuiere diligenciar solicitud
              -- Verifica si hay otras personas distintos al solicitante en la asesoria que son clientes nuevos

              --Modifica Jorge Naranjo 20080125 el cursor siguiente ya fue evaluado para bCumpleServicios, por lo tanto se
              --deja completamente en comentarios asignando directamente el estado de control VioEstControl := VEstadoControl
              /*OPEN CClientesSecNuevos;
              FETCH CClientesSecNuevos
                INTO NCont;
              CLOSE CClientesSecNuevos;
              IF NVL(NCont, 0) = 0 THEN
                -- No hay clientes nuevos distintos al solicitante
                -- UPDATE ts_asesorias
                --  SET aestcon_codigo = '1.07'
                --WHERE consecutivo_asesoria = niConsec_asesor;

                VioEstControl := VEstadoControl;
              ELSE
                VioEstControl := NULL;
              END IF;
              */
              VioEstControl := VEstadoControl;
              BoExito := TRUE;
            ELSE
              -- Se requiere diligenciar solicitud, el estado de control permanece igual
              BoExito := TRUE;
              rastro('NContg '||NCont,'DiligSol');
            END IF; -- if no requiere diligenciar solicitud
            --Fin modificación 20080125
          --END IF; -- if servicios en estudio con numero servicio asignado Modifica Jorge Naranjo 20080125 comentarios en IF

          UPDATE TS_ASESORIAS
             SET Tipo_Solicitud = NTipoSolicAho
           WHERE Consecutivo_Asesoria = NiConsec_Asesor;
          rastro('NConth '||NCont,'DiligSol');
        END IF; -- if solicitud sólo tiene ahorros
        BoExito := TRUE;
        rastro('NConti '||NCont,'DiligSol');
      END IF;
    ELSE
      VoMsjUsu := 'La identificación la de persona es incorrecta';
      BoExito  := FALSE;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.DiligenciasSol',
                                       'Error no fue posible asignar el estado de control',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en diligenciar solicitud, verifique el LOG';
  END DiligenciarSol;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 29/03/2004 01:00:00 p.m.
  -- Retorna la sec y el nombre del estado de decision
  -- teniendo el codigo del estado.

  PROCEDURE EstadoDecision(ViCodigo IN VARCHAR2, -- Codigo del estado
                           NoSec    OUT NUMBER, -- Secuencia del estado
                           VoNombre OUT VARCHAR2, -- Nombre del estado
                           VoMsjtec OUT VARCHAR2,
                           VoMsjusu OUT VARCHAR2,
                           BoExito  OUT BOOLEAN) IS
    CURSOR CEstados IS
      SELECT Testdec.Sec, Testdec.Nombre
        FROM Vs_Tipo_Estado_Decision Testdec
       WHERE Testdec.Codigo = ViCodigo;

  BEGIN
    OPEN CEstados;
    FETCH CEstados
      INTO NoSec, VoNombre;
    CLOSE CEstados;
    IF NoSec IS NULL THEN
      VoMsjusu := 'El código de estado de decisión ' || ViCodigo ||
                  ' no se encuentra registrado';
      BoExito  := FALSE;
    ELSE
      BoExito := TRUE;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.ESTADO_DECISION',
                                       'Error al consultar el estado de decisión',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en estado decisión, verifique el LOG';
      CLOSE CEstados;
  END EstadoDecision;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 29/03/2004 01:00:00 p.m.
  -- Retorna el estado del servicio
  -- teniendo el per_id y el codigo del servicio.

  PROCEDURE EstadoServicio(NiPer_Id IN NUMBER, -- ID del cliente
                           ViCodigo IN VARCHAR2, -- Código del servicio
                           VoEstado OUT VARCHAR2, -- Estado del servicio
                           VoMsjtec OUT VARCHAR2,
                           VoMsjusu OUT VARCHAR2,
                           BoExito  OUT BOOLEAN) IS
    CURSOR CEstados IS
      SELECT Pserest.Estado_Servicio
        FROM TS_PER_SERVICIO_ESTADOS Pserest
       WHERE SYSDATE BETWEEN Fecha_Inicial AND NVL(Fecha_Final, SYSDATE) AND
             Pserest.Ser_Codigo = ViCodigo AND
             Pserest.Per_Id_Definido_Por = NiPer_Id;

  BEGIN
    OPEN CEstados;
    FETCH CEstados
      INTO VoEstado;
    CLOSE CEstados;
    IF VoEstado IS NULL THEN
      VoMsjusu := 'El servicio para esta persona no esta restringido';
      BoExito  := FALSE;
    ELSE
      BoExito := TRUE;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.ESTADO_SERVICIO',
                                       'Error al consultar el estado del servicio',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en estado del servicio, verifique el LOG';
      CLOSE CEstados;
  END EstadoServicio;

  FUNCTION EstadoControlAsesoria(NConsecutivo     IN NUMBER,
                                 VTipoConsecutivo IN VARCHAR2 DEFAULT 'A')
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: EstadoControlAsesoria

      PROPOSITO:
      Retorna el estado de control en el que se encuentra una asesoria
      basado en el consecutivo de una asesoria o en el consecutivo del servicio

      PARAMETROS:
      nConsecutivo IN NUMBER                    -- Consecutivo de servicio o asesoria
      vTipoConsecutivo IN VARCHAR2 DEFAULT 'A'  -- Indicador si el consecutivo es de servicio o asesoria

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      27/05/04            SIC      1.0.0        1. Creacion de la unidad de programa
    ****************************************************************************************************/
    --
    -- Este cursor recupera el estado de control cuando se tiene el consecutivo de la asesoria
    --
    CURSOR CAsesoria IS
      SELECT Aestcon_Codigo
        FROM TS_ASESORIAS
       WHERE Consecutivo_Asesoria = NConsecutivo;
    --
    -- Este cursor recupera el estado de control cuando se tiene el consecutivo del servicio
    --
    CURSOR CAsesoriaServicio IS
      SELECT Aestcon_Codigo
        FROM TS_ASE_SERVICIOS, TS_ASESORIAS
       WHERE Ase_Consecutivo_Asesoria = Consecutivo_Asesoria AND
             Consecutivo_Servicio = NConsecutivo;

    VResultado TS_ASESORIAS.Aestcon_Codigo%TYPE; -- Retorna el estado de control
  BEGIN
    --
    -- Se valida que el consecutivo sea el de Asesoria
    --
    IF VTipoConsecutivo = 'A' THEN
      OPEN CAsesoria;
      FETCH CAsesoria
        INTO VResultado;
      CLOSE CAsesoria;
    ELSE
      -- El consecutivo ingresado se presume que es del servicio
      OPEN CAsesoriaServicio;
      FETCH CAsesoriaServicio
        INTO VResultado;
      CLOSE CAsesoriaServicio;
    END IF;
    RETURN VResultado;
  END EstadoControlAsesoria;

  FUNCTION EstadoDecisionServicio(NConsServicio IN NUMBER) RETURN NUMBER IS
    /****************************************************************************************************
      OBJETO: EstadoDecisionServicio

      PROPOSITO:
      Retorna el estado de decision de un servicio basado en su consecutivo

      PARAMETROS:
      nConsServicio IN NUMBER -- Consecutivo de servicio

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      27/05/04              DDUQUE       1.0.0        1. Creacion de la unidad de programa

    ****************************************************************************************************/
    --
    -- Este cursor recupera el estado de control cuando se tiene el consecutivo de la asesoria
    --
    CURSOR CAseServicio IS
      SELECT b.Tipo_Estado_Decision
        FROM TS_ASE_SERVICIOS b
       WHERE Consecutivo_Servicio = NConsServicio;

    NResultado TS_ASE_SERVICIOS.Tipo_Estado_Decision%TYPE; -- Retorna el estado de control
  BEGIN
    --
    -- Se valida que el consecutivo sea el de Asesoria
    --
    OPEN CAseServicio;
    FETCH CAseServicio
      INTO NResultado;
    CLOSE CAseServicio;

    RETURN NResultado;
  END EstadoDecisionServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 12:00:00 p.m.
  -- Este proceso evalua como una persona que esta solicitando un servicio
  -- cumple con las condiciones que han configurado para este.
  -- ademas registra cuales no cumple para luego informar al asesor.
  --Modifica Jorge Naranjo 20061205 Se adiciona el parámetro niIdDoctoCtrl para el manejo de documentos en
  --la contabilización
  PROCEDURE EvaluarCondicion(NiProceso         IN NUMBER,
                             NiPerId           IN NUMBER,
                             NiConsecAsesoria  IN NUMBER,
                             BoDetener_Proceso OUT BOOLEAN,
                             VoMsjtec          OUT VARCHAR2,
                             VoMsjusu          OUT VARCHAR2,
                             BoExito           OUT BOOLEAN,
                             BoCumple          OUT BOOLEAN,
                             NiConsecServicio  IN NUMBER := NULL,
                             niIdDoctoCtrl     IN NUMBER DEFAULT NULL ) IS

    DFechaProc DATE := SYSDATE;
    VSerSol    VARCHAR2(20); --Servicio solicitado

    CURSOR CCondiciones IS
      SELECT NiPerId, VSerSol, c.Funcion, NiConsecAsesoria, NiConsecServicio,
             NiProceso, c.Id, Ca.Fecha_Inicial, Ca.Alerta, Ca.Accion,niIdDoctoCtrl
        FROM TS_ADM_CONDICION_ACCIONES Ca, TS_ADM_CONDICIONES c
       WHERE c.Id = Ca.Acondic_Id AND Ca.Aproces_Codigo = NiProceso AND
             DFechaProc >= Ca.Fecha_Inicial AND
             DFechaProc <= NVL(Ca.Fecha_Final, SYSDATE)
       ORDER BY Ca.Orden;

    VRCondicion      VResulCondicion;
    InroItems        PLS_INTEGER := 0;
    BDetener_Proceso BOOLEAN;

  BEGIN
    IF NiConsecServicio IS NOT NULL THEN
      VSerSol := ParamAseSer(NiConsecServicio, 'Ser_Codigo');
    END IF;
    OPEN CCondiciones;
    FETCH CCondiciones BULK COLLECT
      INTO VRCondicion;
    InroItems := CCondiciones%ROWCOUNT;
    CLOSE CCondiciones;
    GrAseServicioCre := ParGenAseServicio(NiConsecServicio);
    ReemplazaParametros(VRCondicion,
                        InroItems,
                        BDetener_Proceso,
                        VoMsjtec,
                        VoMsjusu,
                        BoCumple,
                        BoExito);
    BoDetener_Proceso := BDetener_Proceso;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.EvaluarCondicion',
                                       'Error no fue posible evaluar la condición ',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico evaluar condición, verifique el LOG';
      -- CLOSE cCondiciones;
  END EvaluarCondicion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/03/2004 04:00:00 p.m.
  -- Retorna el id y el nombre_completo de la persona,
  -- teniendo el tipo de identificacion y la identificacion.

  PROCEDURE NombreCompleto(ViTipo_Identificacion IN VARCHAR2, -- Tipo de identificación del cliente
                           ViIdentificacion      IN VARCHAR2, -- Identificación del cliente
                           NiId                  OUT NUMBER, -- Id del cliente
                           VoNombre_Completo     OUT VARCHAR2, -- Nombre completo del cliente
                           VoMsjtec              OUT VARCHAR2,
                           VoMsjusu              OUT VARCHAR2,
                           BoExito               OUT BOOLEAN) IS
    CURSOR CPersonas IS
      SELECT Per.Id, Per.Nombre_Completo
        FROM TS_PERSONAS Per
       WHERE Per.Identificacion = ViIdentificacion AND
             Per.Tipo_Identificacion = ViTipo_Identificacion AND
             NVL(Per.Inconsistente, 'NO') = 'NO'; -- JMarquez 27-Oct-2005 Evita que seleccionar personas inconsistentes

  BEGIN

    OPEN CPersonas;
    FETCH CPersonas
      INTO NiId, VoNombre_Completo;
    CLOSE CPersonas;
    IF NiId IS NULL THEN
      VoMsjusu := 'El cliente no se encuentra registrado';
      BoExito  := FALSE;
    ELSE
      BoExito := TRUE;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.NOMBRE_COMPLETO',
                                       'Error al consultar el nombre completo',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en nombre completo, verifique el LOG';
      CLOSE CPersonas;
  END NombreCompleto;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 02/04/2004 04:00:00 p.m.
  -- Retorna el nombre_completo de la persona,
  -- teniendo el id.

  PROCEDURE NombrePersona(NiId              IN NUMBER, -- Id del cliente
                          VoNombre_Completo OUT VARCHAR2, -- Nombre de la persona
                          VoMsjtec          OUT VARCHAR2,
                          VoMsjusu          OUT VARCHAR2,
                          BoExito           OUT BOOLEAN) IS
    CURSOR CPersonas IS
      SELECT Per.Nombre_Completo
        FROM TS_PERSONAS Per
       WHERE Per.Id = NiId;

  BEGIN
    OPEN CPersonas;
    FETCH CPersonas
      INTO VoNombre_Completo;
    CLOSE CPersonas;
    IF VoNombre_Completo IS NULL THEN
      VoMsjusu := 'El cliente no se encuentra registrado';
      BoExito  := FALSE;
    ELSE
      BoExito := TRUE;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.NOMBRE_PERSONA',
                                       'Error al consultar el nombre completo',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico nombre persona, verifique el LOG';
      CLOSE CPersonas;
  END NombrePersona;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- Este proceso reemplaza los parametros que tienen las diferentes condiciones
  -- para las cuales se debe evaluar un servicio que solicita una persona.

  PROCEDURE ReemplazaParametros(VioRCondicion     IN OUT VResulCondicion,
                                NiNroI            IN PLS_INTEGER,
                                BoDetener_Proceso OUT BOOLEAN,
                                VoMsjtec          OUT VARCHAR2,
                                VoMsjusu          OUT VARCHAR2,
                                BoCumple          OUT BOOLEAN,
                                BoExito           OUT BOOLEAN) IS

    TYPE RefCursor IS REF CURSOR;
    VRef RefCursor;

    i          PLS_INTEGER;
    VFuncion   VARCHAR2(2000);
    VMensaje   VARCHAR2(2000);
    VResultado VARCHAR2(2000);
    BCambiar   BOOLEAN;
    --22801 20120507 JNARANJO
    nProceso PLS_INTEGER;
    nConsAsesoria PLS_INTEGER;
    nPerId PLS_INTEGER;
    --22801 20120507 Fin modificación

  BEGIN
    BoCumple := FALSE;
    --
    -- Req. 21815
    -- Fecha modificación : 16/12/2008 10:28:15 a.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Se modifica el 99999 por -99999 ya que genera inconsistencias con los datos que traen 99999
    FOR i IN 1 .. NiNroI LOOP
      BCambiar := TRUE;
      VFuncion := VioRCondicion(i).Funcion;
      VFuncion := REPLACE(VFuncion,
                          '$PER_ID',
                          NVL(VioRCondicion(i).Per_Id, -99999));
      VFuncion := REPLACE(VFuncion,
                          '$CONSEC_SERVIC',
                          NVL(VioRCondicion(i).NConsecSer, -99999));
      VFuncion := REPLACE(VFuncion,
                          '$DOCTO_CTRL_ID',
                          NVL(VioRCondicion(i).niIdDoctoCtrl, -99999));
      VFuncion := REPLACE(VFuncion,
                          '$CONSEC_ASESOR',
                          NVL(VioRCondicion(i).NConsecAse, -99999));
      VFuncion := REPLACE(VFuncion,
                          '$TIPO_DEUDOR',
                          '''' || 'SOLICI' || '''');
      VFuncion := REPLACE(VFuncion,
                          '$CON_ID',
                          NVL(GrAseServicioCre.Con_Deduccion_Nomina, -99999));
      VFuncion := REPLACE(VFuncion,
                          '$SER_CODIGO',
                          '''' || GrAseServicioCre.Ser_Codigo || '''');
      VFuncion := REPLACE(VFuncion,
                          '$AFORPAG_NOMBRE',
                          '''' || GrAseServicioCre.Aforpag_Nombre || '''');
      VFuncion := REPLACE(VFuncion,
                          '$PPEROCUID',
                          NVL(GrAseServicioCre.Pperocu_Id, -99999));
      --
      -- Fin Req. 21815

      --22801 20120507 JNARANJO  
      IF VioRCondicion(i).NConsecAse IS NOT NULL THEN
        nConsAsesoria := VioRCondicion(i).NConsecAse;        
      END IF;
      IF VioRCondicion(i).Per_Id IS NOT NULL AND VioRCondicion(i).NConsecAse IS NULL THEN
        INSERT INTO ts_tar_temp_base_informes( columna01,columna30,columna31 ) 
                                      VALUES ( 'ACONDIC_PERID',VioRCondicion(i).Per_Id,vioRCondicion(i).nConsecSer );
      END IF;  
      IF vioRCondicion(i).nConsecSer IS NOT NULL AND VioRCondicion(i).NConsecAse IS NULL THEN
        INSERT INTO ts_tar_temp_base_informes( columna01,columna30,columna31 ) 
                                      VALUES ( 'ACONDIC_SERVICIO',VioRCondicion(i).Per_Id,vioRCondicion(i).nConsecSer );
      END IF;          
      IF NVL(VioRCondicion(i).NProceso,0) IS NOT NULL THEN
        nProceso := NVL(VioRCondicion(i).NProceso,0);       
      END IF;       
      --22801 20120507 Fin modificación      
      IF INSTR(VFuncion, '=', 1, 1) <> 0 THEN
        BCambiar := FALSE;
        VFuncion := SUBSTR(VFuncion, 1, INSTR(VFuncion, '=', 1, 1) - 1);
      END IF;
      IF INSTR(VFuncion, -99999, 1, 1) <> 0 THEN
        VResultado := 'NO';
      ELSE
        VioRCondicion(i) .Funcion := VFuncion;
        BEGIN
          EXECUTE IMMEDIATE 'BEGIN :a := ' || VFuncion || '; END;'
            USING IN OUT VResultado;
        END;
      END IF;
      IF VResultado IS NOT NULL THEN
        IF VResultado NOT IN ('SI', 'NO') THEN
          VioRCondicion(i) .VAlerta := VioRCondicion(i)
                                      .VAlerta || ' ' || VResultado;
          VResultado := 'SI';
        END IF;
      ELSE
        VResultado := 'NO';
      END IF;
      IF NOT BCambiar THEN
        IF VResultado = 'SI' THEN
          VResultado := 'NO';
        ELSE
          VResultado := 'SI';
        END IF;
      END IF;
      IF VResultado = 'SI' THEN
        IF VioRCondicion(i).Per_Id <> GrAseServicioCre.Per_Id THEN
          DECLARE
            VNombre TS_PERSONAS.Nombre_Completo%TYPE;
            --
            -- Fecha: 19/08/04 02:51:35 p.m.
            -- Usuario: Alberto Eduardo Sánchez B.
            -- Recupera Informacion para traer el nombre de la persona
            --
            CURSOR CNombre_Completo(NiPer_Id IN NUMBER) IS
              SELECT NVL(Per.Nombre_Completo, Per.Nombre) Nombre
                FROM TS_PERSONAS Per
               WHERE Per.Id = NiPer_Id;

          BEGIN
            OPEN CNombre_Completo(VioRCondicion(i).Per_Id);
            FETCH CNombre_Completo
              INTO VNombre;
            CLOSE CNombre_Completo;
            VioRCondicion(i) .VAlerta := VioRCondicion(i)
                                        .VAlerta || ' (' || VNombre || ')';
          END;
        END IF;
        
        RegistrarValidacionAsesoria(VioRCondicion(i).NConsecAse,
                                    VioRCondicion(i).NConsecSer,
                                    VioRCondicion(i).NProceso,
                                    VioRCondicion(i).NCondicion,
                                    VioRCondicion(i).DFechaCond,
                                    VioRCondicion(i).VAlerta,
                                    VioRCondicion(i).Per_Id,
                                    VMensaje);
        IF VioRCondicion(i).VAccion = 'PARE' THEN
          BoDetener_Proceso := TRUE;
          pks_repmercadeo.AQuery(nId => 963,vQuery => VFuncion || ' Respuesta : ' || VResultado);
        END IF;
        BoCumple := TRUE;
      ELSE
        --22801 20120507 JNARANJO
        -- Modificado NVL al consecutivo de asesoria 09-12-2006 - JNARANJO
        /*IF VioRCondicion(i).NConsecSer IS NULL THEN
          DELETE FROM TS_ASE_CONDICIONES Acondic
           WHERE Acondic.Aconacc_Acondic_Id = VioRCondicion(i).NCondicion AND
                 Acondic.Aconacc_Aproces_Codigo = VioRCondicion(i).NProceso AND
                 Acondic.Aconacc_Fecha_Inicial = VioRCondicion(i).DFechaCond AND
                 Acondic.Per_Id = VioRCondicion(i).Per_Id AND
                 NVL(Acondic.Ase_Consecutivo_Asesoria, 0) = NVL(VioRCondicion(i).NConsecAse, 0);
          IF SQL%ROWCOUNT > 0 THEN
            Rastro('2','2');
          END IF;
        ELSE
          DELETE FROM TS_ASE_CONDICIONES Acondic
          WHERE Acondic.Aconacc_Acondic_Id = VioRCondicion(i).NCondicion AND
                 Acondic.Aconacc_Aproces_Codigo = VioRCondicion(i).NProceso AND
                 Acondic.Aconacc_Fecha_Inicial = VioRCondicion(i).DFechaCond AND
                 NVL(Acondic.Ase_Consecutivo_Asesoria, 0) = NVL(VioRCondicion(i).NConsecAse, 0) AND
                 Acondic.Per_Id = VioRCondicion(i).Per_Id AND
                 NVL(Acondic.Aservic_Consecutivo_Servicio,VioRCondicion(i).NConsecSer) = VioRCondicion(i).NConsecSer;
          --                     AND acondic.aservic_consecutivo_servicio = NVL(vioRCondicion(i).nConsecSer,acondic.aservic_consecutivo_servicio);
          IF SQL%ROWCOUNT > 0 THEN
            Rastro('2','2');
          END IF;
        END IF;*/
        --Asesoría
        IF VioRCondicion(i).NConsecAse IS NOT NULL AND VioRCondicion(i).NConsecSer IS NOT NULL AND VioRCondicion(i).Per_Id IS NOT NULL THEN
          DELETE ts_ase_condiciones acondic
          WHERE acondic.aconacc_aproces_codigo = VioRCondicion(i).NProceso
            AND Acondic.Aconacc_Acondic_Id = VioRCondicion(i).NCondicion
            AND acondic.ase_consecutivo_asesoria = VioRCondicion(i).NConsecAse
            AND acondic.aservic_consecutivo_servicio = VioRCondicion(i).NConsecSer
            AND acondic.per_id = VioRCondicion(i).Per_Id
            AND acondic.per_id IS NOT NULL
            AND acondic.aservic_consecutivo_servicio IS NOT NULL
            AND acondic.ase_consecutivo_asesoria IS NOT NULL;
        END IF;
        IF VioRCondicion(i).NConsecAse IS NOT NULL AND VioRCondicion(i).NConsecSer IS NULL AND VioRCondicion(i).Per_Id IS NOT NULL THEN
          DELETE ts_ase_condiciones acondic
          WHERE acondic.aconacc_aproces_codigo = VioRCondicion(i).NProceso
            AND Acondic.Aconacc_Acondic_Id = VioRCondicion(i).NCondicion
            AND acondic.ase_consecutivo_asesoria = VioRCondicion(i).NConsecAse
            AND acondic.per_id = VioRCondicion(i).Per_Id
            AND acondic.per_id IS NOT NULL
            AND acondic.aservic_consecutivo_servicio IS NULL
            AND acondic.ase_consecutivo_asesoria IS NOT NULL;
        END IF;           
        IF VioRCondicion(i).NConsecAse IS NOT NULL AND VioRCondicion(i).NConsecSer IS NOT NULL AND VioRCondicion(i).Per_Id IS NULL THEN
          DELETE ts_ase_condiciones acondic
          WHERE acondic.aconacc_aproces_codigo = VioRCondicion(i).NProceso
            AND Acondic.Aconacc_Acondic_Id = VioRCondicion(i).NCondicion
            AND acondic.ase_consecutivo_asesoria = VioRCondicion(i).NConsecAse
            AND acondic.aservic_consecutivo_servicio = VioRCondicion(i).NConsecSer
            AND acondic.per_id IS NULL
            AND acondic.aservic_consecutivo_servicio IS NOT NULL
            AND acondic.ase_consecutivo_asesoria IS NOT NULL;
        END IF;                
        --Servicio
        IF VioRCondicion(i).NConsecAse IS NULL AND VioRCondicion(i).NConsecSer IS NOT NULL AND VioRCondicion(i).Per_Id IS NOT NULL THEN
          DELETE ts_ase_condiciones acondic
          WHERE acondic.aconacc_aproces_codigo = VioRCondicion(i).NProceso
            AND acondic.aconacc_acondic_id = VioRCondicion(i).NCondicion
            AND acondic.aservic_consecutivo_servicio = VioRCondicion(i).NConsecSer
            AND acondic.per_id = VioRCondicion(i).Per_Id
            AND acondic.per_id IS NOT NULL
            AND acondic.aservic_consecutivo_servicio IS NOT NULL
            AND acondic.ase_consecutivo_asesoria IS NULL;
        END IF;        
        IF VioRCondicion(i).NConsecAse IS NULL AND VioRCondicion(i).NConsecSer IS NOT NULL AND VioRCondicion(i).Per_Id IS NULL THEN
          DELETE ts_ase_condiciones acondic
          WHERE acondic.aconacc_aproces_codigo = VioRCondicion(i).NProceso
            AND acondic.aconacc_acondic_id = VioRCondicion(i).NCondicion
            AND acondic.aservic_consecutivo_servicio = VioRCondicion(i).NConsecSer
            AND acondic.per_id IS NULL
            AND acondic.aservic_consecutivo_servicio IS NOT NULL
            AND acondic.ase_consecutivo_asesoria IS NULL;
        END IF;   
        --Persona
        IF VioRCondicion(i).NConsecAse IS NULL AND VioRCondicion(i).NConsecSer IS NULL AND VioRCondicion(i).Per_Id IS NOT NULL THEN
          DELETE ts_ase_condiciones acondic
          WHERE acondic.aconacc_aproces_codigo = VioRCondicion(i).NProceso
            AND acondic.aconacc_acondic_id = VioRCondicion(i).NCondicion
            AND acondic.per_id = VioRCondicion(i).Per_Id
            AND acondic.per_id IS NOT NULL
            AND acondic.aservic_consecutivo_servicio IS NULL
            AND acondic.ase_consecutivo_asesoria IS NULL;
        END IF;              
        --22801 20120507 Fin modificación
      END IF;
    END LOOP;
    
    --22801 20120507 JNARANJO
    IF nConsAsesoria IS NOT NULL THEN
        DELETE ts_ase_condiciones acondic
        WHERE acondic.aconacc_aproces_codigo = nProceso
          AND acondic.ase_consecutivo_asesoria = nConsAsesoria
          AND NOT EXISTS ( SELECT 1
                           FROM ts_adm_condicion_acciones aconacc
                           WHERE aconacc.aproces_codigo = nProceso
                             AND aconacc.acondic_id = acondic.aconacc_acondic_id
                             AND aconacc.aproces_codigo = acondic.aconacc_aproces_codigo
                             AND SYSDATE BETWEEN aconacc.fecha_inicial AND NVL(aconacc.fecha_final,SYSDATE) );
    END IF;

    IF nConsAsesoria IS NULL THEN
        DELETE ts_ase_condiciones acondic
        WHERE acondic.aconacc_aproces_codigo = nProceso
          AND acondic.ase_consecutivo_asesoria IS NULL
          AND acondic.per_id IS NOT NULL
          AND EXISTS ( SELECT 1 
                       FROM ts_tar_temp_base_informes tbi 
                       WHERE acondic.per_id = tbi.columna30
                         AND NVL(acondic.aservic_consecutivo_servicio,0) = NVL(tbi.columna31,0)
                         AND tbi.columna01 = 'ACONDIC_PERID' )
          AND NOT EXISTS ( SELECT 1
                           FROM ts_adm_condicion_acciones aconacc
                           WHERE aconacc.aproces_codigo = nProceso
                             AND aconacc.acondic_id = acondic.aconacc_acondic_id
                             AND aconacc.aproces_codigo = acondic.aconacc_aproces_codigo
                             AND SYSDATE BETWEEN aconacc.fecha_inicial AND NVL(aconacc.fecha_final,SYSDATE) );
    END IF;
    
    IF nConsAsesoria IS NULL THEN
        DELETE ts_ase_condiciones acondic
        WHERE acondic.aconacc_aproces_codigo = nProceso
          AND acondic.ase_consecutivo_asesoria IS NULL
          AND acondic.aservic_consecutivo_servicio IS NOT NULL
          AND EXISTS ( SELECT 1 
                       FROM ts_tar_temp_base_informes tbi 
                       WHERE acondic.aservic_consecutivo_servicio = tbi.columna31
                         AND NVL(acondic.per_id,0) = NVL(tbi.columna30,0)
                         AND tbi.columna01 = 'ACONDIC_SERVICIO' )
          AND NOT EXISTS ( SELECT 1
                           FROM ts_adm_condicion_acciones aconacc
                           WHERE aconacc.aproces_codigo = nProceso
                             AND aconacc.acondic_id = acondic.aconacc_acondic_id
                             AND aconacc.aproces_codigo = acondic.aconacc_aproces_codigo
                             AND SYSDATE BETWEEN aconacc.fecha_inicial AND NVL(aconacc.fecha_final,SYSDATE) );
    END IF;        
    
    DELETE ts_tar_temp_base_informes tbi
    WHERE tbi.columna01 IN ('ACONDIC_PERID','ACONDIC_SERVICIO');
    --22801 20120507 Fin modificación
    
  EXCEPTION
    WHEN OTHERS THEN
      --22801 20120507 JNARANJO
      DELETE ts_tar_temp_base_informes tbi
      WHERE tbi.columna01 IN ('ACONDIC_PERID','ACONDIC_SERVICIO');
      --22801 20120507 Fin modificación
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.Remplaza_Parametros',
                                       'Error no fue posible remplazar los parametros ' ||
                                       VFUNCION,
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico en remplazar parametros, verifique el LOG';
      CLOSE VRef;
  END ReemplazaParametros;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- Este proceso crea un registro en la tabla ase condicion
  -- una vez que se han aplicado las validaciones que ha tenido una solicitud marzo 03/03 moav
  -- se adiciono la relacion con la asesoria - moav - marzo 21 de 2003

  PROCEDURE RegistrarValidacionAsesoria(NiConsecutivoAsesoria  IN NUMBER,
                                        NiConsecutivo_Servicio IN NUMBER,
                                        NiProce_Id             IN NUMBER,
                                        NiCondicion            IN NUMBER,
                                        DiFechaCond            IN DATE,
                                        ViMensaje              IN VARCHAR2,
                                        NiPer_Id               IN NUMBER,
                                        VoMensaje              OUT VARCHAR2) IS

    --
    -- Fecha: 27/08/04 12:06:50 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si la condicion ya existe
    --

    VUsuario   VARCHAR2(20);
    DFecha     DATE;
    NSecuencia NUMBER;
    BExito     BOOLEAN;

  BEGIN
    VUsuario := USER;
    DFecha   := SYSDATE;
    --Jorge Naranjo 20061209 Para permitir el manejo a nivel de servicio y no solo de asesoría
    UPDATE TS_ASE_CONDICIONES
       SET Mensaje = ViMensaje
     /* Juan Sierra 2010/11/03 No.Req.24371    
     WHERE ((Aservic_Consecutivo_Servicio = NiConsecutivo_Servicio) OR
           (Aservic_Consecutivo_Servicio IS NULL AND
           NiConsecutivo_Servicio IS NULL)) AND
           ( ( Ase_Consecutivo_Asesoria = NiConsecutivoAsesoria ) OR
           ( ( Ase_Consecutivo_Asesoria IS NULL AND NiConsecutivoAsesoria IS NULL ) AND
             (  Aservic_Consecutivo_Servicio = NiConsecutivo_Servicio ) ) ) AND
           Aconacc_Acondic_Id = Nicondicion AND
           Aconacc_Fecha_Inicial = DiFechaCond AND Per_Id = NiPer_Id AND
           Aconacc_Aproces_Codigo = NiProce_Id;*/
     WHERE NVL( Aservic_Consecutivo_Servicio, -1 ) = NVL(NiConsecutivo_Servicio, -1)
     AND NVL( Ase_Consecutivo_Asesoria, -1 ) = NVL( NiConsecutivoAsesoria, -1 ) 
     AND NVL(Per_Id, -1) = NVL(NiPer_Id, -1) 
     AND Aconacc_Acondic_Id = Nicondicion 
     AND Aconacc_Fecha_Inicial = DiFechaCond 
     AND Aconacc_Aproces_Codigo = NiProce_Id;

    IF SQL%NOTFOUND THEN
      NSecuencia := Pk_Util.Proxima_Sec_Oracle('SS_ASE_CONDICIONES');
      IF NSecuencia IS NOT NULL THEN
        INSERT INTO TS_ASE_CONDICIONES
          (Id, Mensaje, Cumple_Condicion, Aconacc_Acondic_Id,
           Aconacc_Aproces_Codigo, Aconacc_Fecha_Inicial, u_Creacion,
           f_Creacion, Ase_Consecutivo_Asesoria,
           Aservic_Consecutivo_Servicio, Per_Id)
        VALUES
          (NSecuencia, ViMensaje, 'NO', Nicondicion, NiProce_Id,
           DiFechaCond, VUsuario, DFecha, NiConsecutivoAsesoria,
           NiConsecutivo_Servicio, NiPer_Id);
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.RegistrarValidacionAsesoria',
                                       'Error no fue posible RegistrarValidacionAsesoria ',
                                       SQLCODE,
                                       VoMensaje,
                                       Bexito);
      VoMensaje := 'Se presento un error tecnico en Registrar validacion asesoria, verifique el LOG';

  END RegistrarValidacionAsesoria;

  PROCEDURE SerAfiliacion(NiPerId     IN NUMBER,
                          RAfiliacion OUT VGrupoAfi,
                          i           OUT NUMBER,
                          VoMsjtec    OUT VARCHAR2,
                          VoMsjusu    OUT VARCHAR2,
                          BoExito     OUT BOOLEAN) IS
    /****************************************************************************************************
      OBJETO: PS_SerAfiliacion

      PROPOSITO:
      Retorna los servicios de afiliación obligatorios después de realizar las validaciones previas

      PARAMETROS:
      niPerId

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      31/05/04            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      el id de la persona




    ****************************************************************************************************/

    NId            TS_ADM_COMPANIAS.Id%TYPE := NULL;
    NEdadMinima    VS_SER_EDADES.Edad_Minima%TYPE;
    NEdadMaxima    VS_SER_EDADES.Edad_Maxima%TYPE;
    NCumpleEdadReq NUMBER;
    VoResaccedeCre VARCHAR2(2);
    VTipoPersona   VARCHAR2(240);
    VNombreVista   VARCHAR2(50);

    --
    -- Fecha: 31/05/04 02:06:04 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar los servicios de afiliación que tomará el cliente
    --
    CURSOR CServicios IS
      SELECT Ser.Nombre, Ser.Codigo, Per.Per_Type
        FROM TS_SERVICIOS Ser, TS_PERSONAS Per, VS_SER_AFILIACIONES Vserafi
       WHERE Per.Id = NiPerId AND Vserafi.Ser_Codigo = Ser.Codigo AND
             (Vserafi.Tipo_Persona = 'AMBAS' OR
             Vserafi.Tipo_Persona = Per.Per_Type) AND
             Vserafi.Obligatorio = 'SI' AND
             Ser.Codigo NOT IN
             (SELECT Aservic.Ser_Codigo
                FROM TS_ASE_SERVICIOS Aservic
               WHERE Aservic.Per_Id = NiPerId AND
                     Aservic.Tipo_Estado_Decision IN
                     (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'),
                      Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
                      Pk_Conseres.Sec_Tipo_Codigo(1264, '2.05'))

              );

  BEGIN
    BoExito := TRUE;
    i       := 0;
    FOR CSer IN CServicios LOOP
      -- Verifica que la compañia a la que pertenece el convenio de deducción de nómina o la dependencia
      /*
              se remplazara por proceso de interfase
              pks_proc_solicitudes.Companiaservicio(niPerId,
                                                nId,
                                                voMsjtec,
                                                voMsjusu,
                                                boExito);
      */
      VoResaccedeCre := 'NO';
      NCumpleEdadReq := F114(NiPerId);
      IF CSer.Per_Type = 'PNATUR' THEN
        IF NVL(NCumpleEdadReq, 0) <> 0 THEN
          NEdadMinima := Pks_Servicios.Parametro(CSer.Codigo,
                                                 'VS_SER_EDADES',
                                                 'EDAD_MINIMA',
                                                 SYSDATE);
          NEdadMaxima := Pks_Servicios.Parametro(CSer.Codigo,
                                                 'VS_SER_EDADES',
                                                 'EDAD_MAXIMA',
                                                 SYSDATE);
          IF NEdadMinima IS NOT NULL AND NEdadMaxima IS NOT NULL THEN
            IF NCumpleEdadReq BETWEEN NEdadMinima AND NEdadMaxima THEN
              VoResaccedeCre := 'NO';
            ELSE
              -- se debe reportar
              VoResaccedeCre := 'SI';
              VoMsjusu       := 'El cliente no cumple con la edad requerida para acceder al servicio';
            END IF;
          ELSE
            VoResaccedeCre := 'NO';
          END IF;
        ELSE
          VoResaccedeCre := 'NO';
        END IF;
      ELSE
        VoResaccedeCre := 'NO';
      END IF;
      IF VoResaccedeCre = 'NO' THEN
        IF SUBSTR(CSer.Codigo, 1, 6) IN ('001.01') THEN
          VNombreVista := 'VS_SER_AFILIACIONES';
          VTipoPersona := Pks_Servicios.Parametro(CSer.Codigo,
                                                  VNombreVista,
                                                  'TIPO_PERSONA',
                                                  SYSDATE);
          IF VTipoPersona <> 'AMBAS' THEN
            IF VTipoPersona <> CSer.Per_Type THEN
              VoResaccedeCre := 'SI'; -- no cumple la condición
              VoMsjusu       := 'El tipo de persona del cliente no cumple con las condiciones para acceder a la linea de afiliación';
            ELSE
              VoResaccedeCre := 'NO';
            END IF;
          ELSE
            VoResaccedeCre := 'NO';
          END IF;
        END IF;
      END IF;
      IF VoResaccedeCre = 'NO' THEN
        i := i + 1;
        RAfiliacion(i) .Ser_Codigo := CSer.Codigo;
        RAfiliacion(i) .Nombre := CSer.Nombre;
        RAfiliacion(i) .NId := NId;
      END IF;
    END LOOP;
    IF NVL(i, 0) = 0 THEN
      BoExito := FALSE;
      IF VoMsjUsu IS NULL THEN
        VoMsjUsu := 'El cliente ya posee servicios de afiliación efectivos o en trámite';
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      Pks_Garantia_Real.CASE_EXCEPTION('PKS_PROC_SOLICITUDES.Serafiliacion',
                                       'Error no fue posible cargar los servicios de afiliacion',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjusu := 'Se presento un error tecnico ser afiliación, verifique el LOG';
  END SerAfiliacion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 30/03/2004 09:47:00 a.m.
  -- Este funcion devuelve las cuotas de los servicios requeridos de afiliacion
  -- que una persona posee  con la cooperativa
  PROCEDURE ValoresCuotasAfiliacion(ViSer        IN VARCHAR2, --Consecutivo del servicio
                                    NiPerId      IN NUMBER, --Id de la persona
                                    RoSerExig    OUT Pks_Proc_Solicitudes.RSerExig, --Vector con los servicios de afiliacion que posee la persona
                                    NoNroSerExig OUT NUMBER --Cantidad de servicios de afiliacion que posee la persona
                                    ) IS

    CURSOR CEstados IS
      SELECT Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01') Est,
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') Efec,
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.05') Apro
        FROM DUAL;
    RcEst CEstados%ROWTYPE;

    CURSOR CASeSer(ViServ IN VARCHAR2) IS
      SELECT Ase.Ser_Codigo, Ase.Cuota_Periodo_Pago,
             Ase.Afforma_Amortizacion
        FROM TS_ASE_SERVICIOS ASE, TS_ASESORIAS A
       WHERE SUBSTR(Ser_Codigo, 1, 6) = ViServ AND
             ASE.TIPO_ESTADO_DECISION IN
             (RcEst.Est, RcEst.Efec, RcEst.Apro) AND -- servicio efectivo,  en estudio, aprobado
             A.CONSECUTIVO_ASESORIA = ASE.ASE_CONSECUTIVO_ASESORIA AND
             A.PER_ID = NiPerId
       ORDER BY Ase.Ser_Codigo;

    i PLS_INTEGER := 0;

  BEGIN
    OPEN CEstados;
    FETCH CEstados
      INTO RcEst;
    CLOSE CEstados;
    FOR CdcAseSer IN CAseSer(ViSer) LOOP
      i := i + 1;
      RoSerExig(i) .Codigo := CdcAseSer.Ser_Codigo;
      RoSerExig(i) .Cuota := CdcAseSer.Cuota_Periodo_Pago;
      RoSerExig(i) .Famortizar := CdcAseSer.Afforma_Amortizacion;
    END LOOP;
    NoNroSerExig := i;
  END ValoresCuotasAfiliacion;

  FUNCTION AutorizacionAsesoria(NiConsecAsesor IN TS_ASESORIAS.Consecutivo_Asesoria%TYPE)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: AutorizacionAsesoria

      PROPOSITO:
      Permite determinar si es posible o no modificar la asesoria

      PARAMETROS:
      niConsecAsesor IN ts_asesorias.consecutivo_asesorias%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      05/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa
      09/12/2006            JSIERRA                    2. Actualización para los procesos de modificación de afiliaciones
      03/01/2007            JGLEN                      3. Actualización para permitir modificar servicios aplazados en la oficina

      REQUISITOS:
      Que la asesoría ya se le haya asignado el consecutivo

      NOTAS:+
      es necesario que tenga almacenado el usuario y la dependencia

    ****************************************************************************************************/

    --
    -- Fecha: 05/05/2004 03:16:31 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si se puede modificar o no la asesoría
    -- Actualización: se adiciono el campo numero_renovacion
    -- Fecha actualización: 09-12-2006
    -- Autor actualización: JSIERRA
    --
    CURSOR CAsesoria(NConsecAsesor IN NUMBER) IS
      SELECT Ase.Aestcon_Codigo, Ase.Acredir_Id, ase.numero_renovacion, ase.numero_radicado-- 29537 (A) HOSSA - 20130131 agrega ase.numero_radicado
        FROM TS_ASESORIAS Ase
       WHERE Ase.Consecutivo_Asesoria = NConsecAsesor;

    --
    -- Fecha: 05/05/2004 03:35:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar que no se encuentren servicios efectivos en la solicitud
    --
    CURSOR CSerEfectivos(NConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.15'))
      UNION
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor AND
             Aservic.Numero_Servicio IS NOT NULL AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.02'
      UNION
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic, TS_ASESORIAS Ase
       WHERE Aservic.Ase_Consecutivo_Asesoria = Ase.Consecutivo_Asesoria AND
             Ase.Consecutivo_Asesoria = NConsecAsesor AND
             Ase.Numero_Radicado IS NOT NULL AND
             SUBSTR(Aservic.Ser_Codigo, 1, 9) = '001.02.02';

    --
    -- Fecha: 05/05/2004 03:08:08 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para identificar el rol del usuario
    --
    --29789 JNARANJO 20130601
    --CURSOR CRolesUsuario(ViUsuario IN VARCHAR2, ViRol1 IN VARCHAR2, ViRol2 IN VARCHAR2 DEFAULT 'NINGUNO', ViRol3 IN VARCHAR2 DEFAULT 'NINGUNO') IS
    CURSOR CRolesUsuario(ViUsuario IN VARCHAR2, ViRol1 IN VARCHAR2, ViRol2 IN VARCHAR2 DEFAULT 'NINGUNO', ViRol3 IN VARCHAR2 DEFAULT 'NINGUNO', ViRol4 IN VARCHAR2 DEFAULT 'NINGUNO',ViRol5 IN VARCHAR2 DEFAULT 'NINGUNO') IS    
    --29789 JNARANJO Fin modificación
      SELECT 1
        FROM CON_USUARIO_GRUPOS Cusugru
       WHERE Cusugru.Usuario = ViUsuario AND
             --29789 JNARANJO 20130601
             --(Cusugru.Grupo = ViRol1 OR Cusugru.Grupo = ViRol2 OR Cusugru.Grupo = ViRol3);
             (Cusugru.Grupo = ViRol1 OR Cusugru.Grupo = ViRol2 OR Cusugru.Grupo = ViRol3 OR Cusugru.Grupo = ViRol4 OR Cusugru.Grupo = ViRol5);
             --29789 JNARANJO Fin modificación

    --
    -- Fecha: 05/05/2004 03:35:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar las condiciones que se deben cumplir por servicio
    --
    CURSOR CServicios(NConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor AND
             Aservic.Tipo_Estado_Decision <>
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) <> '001.02'
      UNION
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor AND
             Aservic.Tipo_Estado_Decision <>
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') AND
             Aservic.Numero_Servicio IS NULL AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.02';


        --
    -- Fecha: 03-01-2007 03:35:01 p.m.
    -- Usuario: Jacqueline Glen
    -- Recupera Informacion para determinar si hay servicios aplazados
    --

    --03/01/2007            JGLEN                      3. Actualización para permitir modificar servicios aplazados en la oficina
    CURSOR CAplazados(NConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor AND
             Aservic.Tipo_Estado_Decision = Pk_Conseres.Sec_Tipo_Codigo(1264, '2.12');

    --
    -- Fecha: 06/05/2004 10:35:41 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determiar si posee algun servicio de crédito
    --
    CURSOR CCreditos(NConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor;

    --
    -- Fecha: 06/05/2004 10:39:41 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si la asesoria ha pasado por estado de control solicitud en analisis
    --
    CURSOR CAnalisis(NConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_ESTADO_CONTROLES Aestcon
       WHERE Aestcon.Aestcon_Codigo = '1.05' AND
             Aestcon.Ase_Consecutivo_Asesoria = NConsecAsesor;

    --
    -- Fecha: 06/05/2004 10:59:40 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si el usuario esta autorizado para modificar la asesoria
    --
    nVrSMMLV NUMBER := NVL(Pk_Conseres.Param_General('GSMLMV'),0);-- 25732 (M) 11/05/2011 JNARANJO                 
    CURSOR CAutorizado(NConsecAsesor IN NUMBER, ViUsuario IN VARCHAR2) IS
      SELECT 1
        FROM VS_SER_CRE_APROBACIONES Vcreapr, TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConsecasesor AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             ((Aservic.Ser_Codigo = Vcreapr.Ser_Codigo AND
             SYSDATE >= Vcreapr.Saplica_Fecha_Inicial AND
             Aservic.Crtipo_Credito = Vcreapr.Tipo_Credito AND
             -- 25732 (M) 11/05/2011 JNARANJO
             --Aservic.Crvalor_Credito BETWEEN (Vcreapr.Monto_Inicial * Pk_Conseres.Param_General('GSMLMV')) AND (Vcreapr.Monto_Final * Pk_Conseres.Param_General('GSMLMV')) AND
             ( ( Aservic.Crvalor_Credito BETWEEN (Vcreapr.Monto_Inicial * nVrSMMLV) AND (Vcreapr.Monto_Final * nVrSMMLV) AND Aservic.Aservic_Type = 'ACREDIT' ) OR
               ( Aservic.Rovalor_Cupo BETWEEN (Vcreapr.Monto_Inicial * nVrSMMLV) AND (Vcreapr.Monto_Final * nVrSMMLV) AND Aservic.Aservic_Type = 'AROTATI' )  ) AND
             -- 25732 11/05/2011 Fin modificación 
             EXISTS
              (SELECT 1
                  FROM CON_USUARIOS u, CON_USUARIO_GRUPOS g,
                       CON_USUARIO_GRUPOS Grupos, CON_USUARIOS Nivel
                 WHERE u.Usuario = ViUsuario AND u.Tipo = 'U' AND
                       u.Usuario = g.Usuario AND g.Grupo = Grupos.Usuario AND
                       Grupos.Grupo = Nivel.Usuario AND Nivel.Tipo = 'N' AND
                       Nivel.Usuario = Vcreapr.Usuario)) OR
             -- 25732 (M) 11/05/2011 JNARANJO
             --NVL(Aservic.Crvalor_Credito, 0) = 0);
             ( (NVL(Aservic.Crvalor_Credito, 0) = 0 AND Aservic.Aservic_Type != 'ACREDIT') OR 
               (NVL(Aservic.Rovalor_Cupo, 0) = 0 AND Aservic.Aservic_Type != 'AROTATI') )  );
             -- 25732 11/05/2011 Fin modificación 
             

    --
    -- Fecha: 15/09/04 11:53:21 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el usuario al que se le asigno la solicitud al apasar por el estado de analisis o verificación
    --
    CURSOR CEstado(NiConsec_Asesor IN NUMBER, ViEstados IN VARCHAR2) IS
      SELECT Aestcon.Usu_Usuario
        FROM TS_ASE_ESTADO_CONTROLES Aestcon
       WHERE Aestcon.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             Aestcon.Aestcon_Codigo IN ViEstados
       ORDER BY f_Creacion DESC;

    VResult         VARCHAR2(4);
    VUsuario        VARCHAR2(40) := 'NULL'; -- Utilizada para identificar el usuario asesor
    VUsuFase        VARCHAR2(50); -- Para manejar el usuario de la fase
    VAestcon_Codigo VARCHAR2(6) := 'NULL'; -- Utilizada para determinar el estado de control de la solicitud
    NCont           NUMBER := 0; -- Utilizada para determinar el número de registros retornados
    NAcredirId      NUMBER; -- utilizada para guardar si la asesoria pertenece a un desembolso directo
    nNumeroRenovacion NUMBER; -- Almacenar el número de renovación de la asesoria - JSIERRA - 09-12-2006
    nNumeroRadicado NUMBER; -- Almacenar el número de radicado de la asesoria - HOSSA - 10-04-2013
    --29789 JNARANJO 20130601
    CURSOR cExiste IS
      SELECT 'SI' existe
      FROM ts_ase_servicios aservic,ts_servicios s
      WHERE aservic.ase_consecutivo_asesoria = NiConsecAsesor
        AND aservic.ser_codigo = s.codigo
        AND s.modalidad_cartera IN ('HIPOTE','MICROC','COMERC');
    rExiste cExiste%ROWTYPE;
    vCooVivMicCom VARCHAR2(30);
    vComVivMicCom VARCHAR2(30);
    --29789 JNARANJO Fin modificación
    ---49484 JMarquez 20180621 (A) Identificar si el servicio es un crédito preaprobado NO se puede modificar en asesoria
    CURSOR cPreaprobado IS
      SELECT 'NO'
        FROM ts_ase_cre_pre_detalles cpd, ts_ase_cre_preaprobados cp, ts_ase_Servicios s
       WHERE cp.acta = cpd.acrepre_acta
         AND cpd.aservic_consecutivo_servicio = s.consecutivo_servicio
         AND s.ase_consecutivo_asesoria = NiConsecAsesor
         AND cp.ser_codigo LIKE '001.03.01.%';
    ---FIN 49484 JMarquez 20180621 (A)
  BEGIN
    ---49484 JMarquez 20180621 (A) Identificar si el servicio es un crédito preaprobado NO se puede modificar en asesoria
     OPEN cPreaprobado;
    FETCH cPreaprobado INTO VResult;
    CLOSE cPreaprobado;

    IF VResult = 'NO' THEN
      RETURN VResult;
    END IF;
    ---FIN 49484 JMarquez 20180621 (A)    
    --29789 JNARANJO 20130601
    OPEN cExiste;
    FETCH cExiste INTO rExiste;
    CLOSE cExiste;
    IF rExiste.existe = 'SI' THEN
      vCooVivMicCom := 'G_COORD_VIVI_MICROC_COMERCIAL';
      vComVivMicCom := 'G_COMITE_CREDITOS_VIV_MICR_COM';
    END IF;
    --29789 JNARANJO Fin modificación
    NCont   := 0;
    VResult := NULL;
    OPEN CSerEfectivos(NiConsecAsesor);
    FETCH CSerEfectivos INTO NCont;
    CLOSE CSerEfectivos;
    IF NVL(NCont, 0) > 0 THEN
      RETURN(VResult);
    END IF;
    VUsuario := USER;
    --
    -- Modificacion: recibir numero_renovacion - JSIERRA - 09-12-2006
    --
    OPEN CAsesoria(NiConsecAsesor);
    FETCH CAsesoria INTO VAestcon_Codigo, NAcredirId, nNumeroRenovacion, nNumeroRadicado;-- 29537 (A) HOSSA - 20130131 agrega nNumeroRadicado
    CLOSE CAsesoria;

    -- 29537 (A) HOSSA - 20130131 -- Se adiciona condición para que sólo permita las asesorias que se procesan por SIC
    OPEN CAplazados(NiConsecAsesor);
    FETCH CAplazados INTO NCont;
    CLOSE CAplazados;
    IF NVL(NCont, 0) = 0 AND
       pks_asesorias.oficinaEnBpm(PKS_SESSIONES.RETORNAR_CODDEPENDENCIA) = 'SI'
       -- 43054 JMarquez 20161005 (M) El sistema de gestión esta entre GPC y SIC
       --AND pks_asesorias.obtenerSistemaGestion(NiConsecAsesor) = 'NO' 
       AND pks_asesorias.obtenerSistemaGestion(NiConsecAsesor) = 'GPC'
       -- FIN 43054 JMarquez 20161005 (M)
       AND nNumeroRadicado IS NOT NULL THEN    
       -- 29537 (A) JACEVEDO 20130627 - Se verifica si la asesoria fue aplazada en BPM y se debe permitir su modificación
       IF AplazadaBPM(NiConsecAsesor) = 'SI' THEN
          RETURN 'RUD';
       END IF;
       -- 29537 Linea Final       
       RETURN 'R';
    END IF; 

    --
    -- Modificacion: verificar numero_renovacion - JSIERRA - 09-12-2006
    --
    IF NAcredirId IS NOT NULL OR
       nNumeroRenovacion IS NOT NULL THEN
      RETURN 'R';
    END IF;
    IF VAestcon_Codigo IS NULL THEN
      RETURN 'CRUD';
    END IF;
    IF VUsuario IS NOT NULL THEN
      -- 1
      NCont := 0;
      --29789 JNARANJO 20130601
      --OPEN CRolesUsuario(VUsuario, 'G_ASESOR_COMERCIAL');
      OPEN CRolesUsuario(VUsuario, 'G_ASESOR_COMERCIAL','G_PRACTICANTES_OFICINA');
      --29789 JNARANJO Fin modificación
      FETCH CRolesUsuario INTO NCont;
      CLOSE CRolesUsuario;
      IF NVL(NCont, 0) > 0 THEN
        -- Si es asesor comercial
        IF VAestcon_Codigo = '1.01' THEN
          -- Si la solicitud esta radicada
          NCont := 0;
          OPEN CServicios(NiConsecAsesor);
          FETCH CServicios INTO NCont;
          CLOSE CServicios;
          IF NVL(NCont, 0) > 0 THEN
            VResult := 'CRUD';
            RETURN(VResult);
          END IF;
    --03/01/2007            JGLEN                      3. Actualización para permitir modificar servicios aplazados en la oficina
        ELSIF VAestcon_Codigo = '1.17' THEN
          NCont := 0;
          OPEN CAplazados(NiConsecAsesor);
          FETCH CAplazados INTO NCont;
          CLOSE CAplazados;
          IF NVL(NCont, 0) > 0 THEN
            VResult := 'RUS';
            RETURN(VResult);
          END IF;
        END IF;
      END IF; -- Fin si es asesor comercial
      NCont := 0;
      OPEN CRolesUsuario(VUsuario, 'G_ANALISTAS_CO', 'G_ANALISTAS_CO_AH');
      FETCH CRolesUsuario INTO NCont;
      CLOSE CRolesUsuario;
      IF NVL(NCont, 0) > 0 THEN
        -- Si es analista
        --
        -- Fecha modificación : 10/11/04 04:31:30 p.m.
        -- Usuario modifica   : Alberto Eduardo Sánchez B.
        -- Causa modificación : Para la salida a producción se requiere que temporalmente las solicitudes que se encuentran en el estado de control 1.03 se puedan modificar por los analistas del centro de operaciones
        --
        IF VAestcon_Codigo IN ('1.05', '1.06', '1.03') THEN
          NCont := 0;
          OPEN CEstado(NiConsecasesor, '1.05');
          FETCH CEstado INTO VUsuFase;
          CLOSE CEstado;
          IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
            VResult := 'U';
            RETURN(VResult);
          END IF;
          NCont := 0;
          OPEN CEstado(NiConsecasesor, '1.06');
          FETCH CEstado INTO VUsuFase;
          CLOSE CEstado;
          IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
            VResult := 'U';
            RETURN(VResult);
          END IF;
        ELSIF VAestcon_Codigo IN ('1.40', '1.11') THEN
          NCont := 0;
          OPEN CCreditos(NiConsecAsesor);
          FETCH CCreditos INTO NCont;
          CLOSE CCreditos;
          IF NVL(NCont, 0) = 1 THEN
            NCont := 0;
            OPEN CAutorizado(NiConsecAsesor, VUsuario);
            FETCH CAutorizado INTO NCont;
            CLOSE CAutorizado;
            IF NVL(NCont, 0) > 0 THEN
              VResult := 'U';
              RETURN(VResult);
            END IF;
          ELSE
            NCont := 0;
            OPEN CEstado(NiConsecasesor, '1.06');
            FETCH CEstado INTO VUsuFase;
            CLOSE CEstado;
            IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
              VResult := 'U';
              RETURN(VResult);
            END IF;
          END IF;
        END IF;
      END IF; -- Fin Si es analista
      NCont := 0;
      OPEN CRolesUsuario(VUsuario,
                         'G_VERIFICADOR_CO',
                         'G_VERIFICADOR_CO_AH');
      FETCH CRolesUsuario
        INTO NCont;
      CLOSE CRolesUsuario;
      IF NVL(NCont, 0) > 0 THEN
        -- Si es verificador
        IF VAestcon_Codigo IN ('1.06') THEN
          NCont := 0;
          OPEN CEstado(NiConsecasesor, '1.06');
          FETCH CEstado
            INTO VUsuFase;
          CLOSE CEstado;
          IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
            OPEN CAnalisis(NiConsecasesor);
            FETCH CAnalisis
              INTO NCont;
            CLOSE CAnalisis;
            IF NVL(NCont, 0) > 0 THEN
              VResult := 'U';
              RETURN(VResult);
            END IF;
          END IF;
        ELSIF VAestcon_Codigo IN ('1.40', '1.11') THEN
          NCont := 0;
          OPEN CCreditos(NiConsecAsesor);
          FETCH CCreditos
            INTO NCont;
          CLOSE CCreditos;
          IF NVL(NCont, 0) = 1 THEN
            NCont := 0;
            OPEN CAutorizado(NiConsecAsesor, VUsuario);
            FETCH CAutorizado
              INTO NCont;
            CLOSE CAutorizado;
            IF NVL(NCont, 0) > 0 THEN
              OPEN CAnalisis(NiConsecasesor);
              FETCH CAnalisis
                INTO NCont;
              CLOSE CAnalisis;
              IF NVL(NCont, 0) > 0 THEN
                VResult := 'U';
                RETURN(VResult);
              END IF;
            END IF;
          ELSE
            NCont := 0;
            OPEN CEstado(NiConsecasesor, '1.06');
            FETCH CEstado
              INTO VUsuFase;
            CLOSE CEstado;
            IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
              OPEN CAnalisis(NiConsecasesor);
              FETCH CAnalisis
                INTO NCont;
              CLOSE CAnalisis;
              IF NVL(NCont, 0) > 0 THEN
                VResult := 'U';
                RETURN(VResult);
              END IF;
            END IF;
          END IF;
        END IF;
      END IF; -- Fin Si es verificador
    END IF;
    --
    -- Fecha modificación : 10/11/04 04:12:33 p.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Los niveles correspondientes podrán modificar las solicitudes en
    -- estado  SOLICITUD REMITIDA al nivel respectivo
    -- (coordinador, comité, Gerencia general, solicitud remitida) siempre y
    -- cuando el servicio no se encuentre efectivo.
    --
    NCont := 0;
    --29789 JNARANJO 20130601
    --OPEN CRolesUsuario(VUsuario,'G_COORDINADOR_CO','G_GERENTE_GENERAL','G_COMITE_CREDITOS');
    OPEN CRolesUsuario(VUsuario,'G_COORDINADOR_CO','G_GERENTE_GENERAL','G_COMITE_CREDITOS',vCooVivMicCom,vComVivMicCom);
    --29789 JNARANJO Fin modificación
    FETCH CRolesUsuario INTO NCont;
    CLOSE CRolesUsuario;
    IF NVL(NCont, 0) > 0 THEN
      --
      -- Req. 20069
      -- Fecha modificación : 08/05/2008 11:45:15 a.m.
      -- Usuario modifica   : Alberto Eduardo Sánchez B.
      -- Causa modificación : Se modifica para permitir que se modifique la asesoria si el estado de control es 1.30
      IF VAestcon_Codigo IN ('1.07','1.30') THEN
        -- Si la solicitud esta remitida
        NCont := 0;
        OPEN CServicios(NiConsecAsesor);
        FETCH CServicios
          INTO NCont;
        CLOSE CServicios;
        IF NVL(NCont, 0) > 0 THEN
          VResult := 'RUD';
          RETURN(VResult);
        END IF;
      ELSIF VAestcon_Codigo IN ('1.40') THEN
        NCont := 0;
        OPEN CCreditos(NiConsecAsesor);
        FETCH CCreditos
          INTO NCont;
        CLOSE CCreditos;
        IF NVL(NCont, 0) = 1 THEN
          NCont := 0;
          OPEN CAutorizado(NiConsecAsesor, VUsuario);
          FETCH CAutorizado
            INTO NCont;
          CLOSE CAutorizado;
          IF NVL(NCont, 0) > 0 THEN
            VResult := 'U';
            RETURN(VResult);
          END IF;
        END IF;
      END IF;
      --
      -- Fin Req. 20069
    END IF; -- Fin Coordinador
    IF VAestcon_Codigo IN ('1.40') THEN
      -- Evaluada sin crédito
      NCont := 0;
      OPEN CCreditos(NiConsecAsesor);
      FETCH CCreditos
        INTO NCont;
      CLOSE CCreditos;
      IF NVL(NCont, 0) = 0 THEN
        NCont := 0;
        OPEN CRolesUsuario(VUsuario, 'G_ANALISTAS_CO', 'G_ANALISTAS_CO_AH');
        FETCH CRolesUsuario
          INTO NCont;
        CLOSE CRolesUsuario;
        IF NVL(NCont, 0) > 0 THEN
          -- Si es analista
          VResult := 'U';
          RETURN(VResult);
        ELSE
          NCont := 0;
          OPEN CRolesUsuario(VUsuario,
                             'G_VERIFICADOR_CO',
                             'G_VERIFICADOR_CO_AH');
          FETCH CRolesUsuario
            INTO NCont;
          CLOSE CRolesUsuario;
          IF NVL(NCont, 0) > 0 THEN
            -- Si es verificador
            VResult := 'U';
            RETURN(VResult);
          END IF;
        END IF;
      END IF;
    END IF; -- Fin Evaluada sin crédito

    RETURN(VResult);
  END AutorizacionAsesoria;

  FUNCTION AutorizacionServicio(NiConsecServic IN NUMBER) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: AutorizacionAsesoria

      PROPOSITO:
      Permite determinar si es posible o no modificar la asesoria

      PARAMETROS:
      niConsecAsesor IN ts_asesorias.consecutivo_asesorias%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      05/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa
      09/12/2006            JSIERRA                    2. Actualización para modificación de afiliaciones
      03/01/2007            JGLEN                      3. Actualización para permitir modificar servicios aplazados en la oficina

      REQUISITOS:
      Que la asesoría ya se le haya asignado el consecutivo

      NOTAS:
      es necesario que tenga almacenado el usuario y la dependencia

    ****************************************************************************************************/

    --
    -- Fecha: 05/05/2004 03:16:31 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si se puede modificar o no la asesoría
    -- Modificacion: se agrego el campo número de renovación - JSIERRA - 09-12-2006
    --
    CURSOR CAsesoria(NConsecAsesor IN NUMBER) IS
      SELECT Ase.Aestcon_Codigo, Ase.Numero_Radicado, Ase.Acredir_Id, ase.numero_renovacion
        FROM TS_ASESORIAS Ase
       WHERE Ase.Consecutivo_Asesoria = NConsecAsesor;

    RAsesoria CAsesoria%ROWTYPE;

    --
    -- Fecha: 05/05/2004 03:35:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar que no se encuentren servicios efectivos en la solicitud
    --
    CURSOR CAseEfectivos(NConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.15'))
      UNION
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor AND
             Aservic.Numero_Servicio IS NOT NULL AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.02'
      UNION
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic, TS_ASESORIAS Ase
       WHERE Aservic.Ase_Consecutivo_Asesoria = Ase.Consecutivo_Asesoria AND
             Ase.Consecutivo_Asesoria = NConsecAsesor AND
             Ase.Numero_Radicado IS NOT NULL AND
             SUBSTR(Aservic.Ser_Codigo, 1, 9) = '001.02.02';

    --
    -- Fecha: 05/05/2004 03:35:01 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar que no se encuentren servicios efectivos en la solicitud
    --
    CURSOR CSerEfectivos(NConsecServic IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Consecutivo_Servicio = NConsecServic AND
             Aservic.Tipo_Estado_Decision <>
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08');

   -- Fecha: 03-01-2007 04:23:01 p.m.
   -- Usuario: Jacqueline Glen.
   -- Recupera Informacion para determinar si el servicio se encuentra aplazado

    CURSOR CSerAplazados(NConsecServic IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Consecutivo_Servicio = NConsecServic AND
             Aservic.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.12');

    --
    -- Fecha: 05/05/2004 03:08:08 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para identificar el rol del usuario
    --

    --29789 JNARANJO 20130601
    --CURSOR CRolesUsuario(ViUsuario IN VARCHAR2, ViRol1 IN VARCHAR2, ViRol2 IN VARCHAR2 DEFAULT 'NINGUNO', ViRol3 IN VARCHAR2 DEFAULT 'NINGUNO') IS
    CURSOR CRolesUsuario(ViUsuario IN VARCHAR2, ViRol1 IN VARCHAR2, ViRol2 IN VARCHAR2 DEFAULT 'NINGUNO', ViRol3 IN VARCHAR2 DEFAULT 'NINGUNO', ViRol4 IN VARCHAR2 DEFAULT 'NINGUNO', ViRol5 IN VARCHAR2 DEFAULT 'NINGUNO') IS    
    --29789 JNARANJO Fin modificación
      SELECT 1
        FROM CON_USUARIO_GRUPOS Cusugru
       WHERE Cusugru.Usuario = ViUsuario AND
             --29789 JNARANJO 20130601
             --(Cusugru.Grupo = ViRol1 OR Cusugru.Grupo = ViRol2 OR Cusugru.Grupo = ViRol3);
             (Cusugru.Grupo = ViRol1 OR Cusugru.Grupo = ViRol2 OR Cusugru.Grupo = ViRol3 OR Cusugru.Grupo = ViRol4 OR Cusugru.Grupo = ViRol5);
             --29789 JNARANJO Fin modificación

    --
    -- Fecha: 06/05/2004 10:35:41 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determiar si posee algun servicio de crédito
    --
    CURSOR CCreditos(NConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             Aservic.Ase_Consecutivo_Asesoria = NConsecAsesor;

    --
    -- Fecha: 06/05/2004 10:39:41 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si la asesoria ha pasado por estado de control solicitud en analisis
    --
    CURSOR CAnalisis(NConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_ESTADO_CONTROLES Aestcon
       WHERE Aestcon.Aestcon_Codigo = '1.05' AND
             Aestcon.Ase_Consecutivo_Asesoria = NConsecAsesor;

    --
    -- Fecha: 06/05/2004 10:59:40 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si el usuario esta autorizado para modificar la asesoria
    nVrSMMLV NUMBER := NVL(Pk_Conseres.Param_General('GSMLMV'),0);-- 25732 (M) 11/05/2011 JNARANJO      
    CURSOR CAutorizado(NiConsecAsesor IN NUMBER, ViUsuario IN VARCHAR2) IS
      SELECT 1
        FROM VS_SER_CRE_APROBACIONES Vcreapr, TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConsecasesor AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             ((Aservic.Ser_Codigo = Vcreapr.Ser_Codigo AND
             SYSDATE >= Vcreapr.Saplica_Fecha_Inicial AND
             Aservic.Crtipo_Credito = Vcreapr.Tipo_Credito AND
             -- 25732 (M) 11/05/2011 JNARANJO
             --Aservic.Crvalor_Credito BETWEEN (Vcreapr.Monto_Inicial * Pk_Conseres.Param_General('GSMLMV')) AND (Vcreapr.Monto_Final * Pk_Conseres.Param_General('GSMLMV')) AND
             ( ( Aservic.Crvalor_Credito BETWEEN (Vcreapr.Monto_Inicial * nVrSMMLV) AND (Vcreapr.Monto_Final * nVrSMMLV) AND Aservic.Aservic_Type = 'ACREDIT' ) OR
               ( Aservic.Rovalor_Cupo BETWEEN (Vcreapr.Monto_Inicial * nVrSMMLV) AND (Vcreapr.Monto_Final * nVrSMMLV) AND Aservic.Aservic_Type = 'AROTATI' )  ) AND
             -- 25732 11/05/2011 Fin modificación              
             EXISTS
              (SELECT 1
                  FROM CON_USUARIOS u, CON_USUARIO_GRUPOS g,
                       CON_USUARIO_GRUPOS Grupos, CON_USUARIOS Nivel
                 WHERE u.Usuario = ViUsuario AND u.Tipo = 'U' AND
                       u.Usuario = g.Usuario AND g.Grupo = Grupos.Usuario AND
                       Grupos.Grupo = Nivel.Usuario AND Nivel.Tipo = 'N' AND
                       Nivel.Usuario = Vcreapr.Usuario)) OR
             -- 25732 (M) 11/05/2011 JNARANJO
             --NVL(Aservic.Crvalor_Credito, 0) = 0);
             ( (NVL(Aservic.Crvalor_Credito, 0) = 0 AND Aservic.Aservic_Type != 'ACREDIT') OR 
               (NVL(Aservic.Rovalor_Cupo, 0) = 0 AND Aservic.Aservic_Type != 'AROTATI') )  );
             -- 25732 11/05/2011 Fin modificación 

    --
    -- Fecha: 15/09/04 11:53:21 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el usuario al que se le asigno la solicitud al apasar por el estado de analisis o verificación
    --
    CURSOR CEstado(NiConsec_Asesor IN NUMBER, ViEstados IN VARCHAR2) IS
      SELECT Aestcon.Usu_Usuario
        FROM TS_ASE_ESTADO_CONTROLES Aestcon
       WHERE Aestcon.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             Aestcon.Aestcon_Codigo IN ViEstados
       ORDER BY f_Creacion DESC;

    --
    -- Fecha modificación : 03/08/04 11:16:25 a.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Adicionar dias de vigencia de la asesoría
    --
    NDiasVigencia NUMBER(3) := Pk_Conseres.Param('GDIASVIGENCIA'); -- Días de vigencia para la actualización de la asesoría

    --
    -- Fecha modificación : 03/08/04 11:19:35 a.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Cursor para consultar si el servicio cumple con los días para poderla modificar
    --
    CURSOR CDiasAsesorias(NiConsecAsesor IN NUMBER) IS
      SELECT 1
        FROM TS_ASESORIAS Ase
       WHERE Ase.Numero_Radicado IS NULL AND
             Ase.Fecha_Asesoria BETWEEN (SYSDATE - Ndiasvigencia) AND
             SYSDATE AND Consecutivo_Asesoria = NiConsecAsesor;

    VResult         VARCHAR2(4);
    VUsuario        VARCHAR2(40) := 'NULL'; -- Utilizada para identificar el usuario asesor
    VUsuFase        VARCHAR2(50); -- Para manejar el usuario de la fase
    VAestcon_Codigo VARCHAR2(6) := 'NULL'; -- Utilizada para determinar el estado de control de la solicitud
    NCont           NUMBER := 0; -- Utilizada para determinar el número de registros retornados
    NiConsecAsesor  NUMBER; -- Consecutivo de la asesoria
    --29789 JNARANJO 20130601
    CURSOR cExiste IS
      SELECT 'SI' existe
      FROM ts_ase_servicios aservic,ts_servicios s
      WHERE aservic.ase_consecutivo_asesoria = NiConsecAsesor
        AND aservic.ser_codigo = s.codigo
        AND s.modalidad_cartera IN ('HIPOTE','MICROC','COMERC');
    rExiste cExiste%ROWTYPE;
    vCooVivMicCom VARCHAR2(30);
    vComVivMicCom VARCHAR2(30);
    --29789 JNARANJO Fin modificación
    
    ---49484 JMarquez 20180621 (A) Identificar si el servicio es un crédito preaprobado NO se puede modificar en asesoria
    CURSOR cPreaprobado IS
      SELECT 'NO'
        FROM ts_ase_cre_pre_detalles cpd, ts_ase_cre_preaprobados cp
       WHERE cp.acta = cpd.acrepre_acta
         AND cpd.aservic_consecutivo_servicio = NiConsecServic
         AND cp.ser_codigo LIKE '001.03.01.%';
    ---FIN 49484 JMarquez 20180621 (A)
  BEGIN
    ---49484 JMarquez 20180621 (A) Identificar si el servicio es un crédito preaprobado NO se puede modificar en asesoria    
     OPEN cPreaprobado;
    FETCH cPreaprobado INTO VResult;
    CLOSE cPreaprobado;

    IF VResult = 'NO' THEN
      RETURN VResult;
    END IF;
    ---FIN 49484 JMarquez 20180621 (A)    
    --29789 JNARANJO 20130601
    OPEN cExiste;
    FETCH cExiste INTO rExiste;
    CLOSE cExiste;
    IF rExiste.existe = 'SI' THEN
      vCooVivMicCom := 'G_COORD_VIVI_MICROC_COMERCIAL';
      vComVivMicCom := 'G_COMITE_CREDITOS_VIV_MICR_COM';
    END IF;
    --29789 JNARANJO Fin modificación
    NCont          := 0;
    VResult        := 'NO';
    VUsuario       := USER;
    NiConsecAsesor := ParamAseSer(NiConsecServic,
                                  'ase_consecutivo_asesoria');

      
    -- 29537 (A) HOSSA - 20130131
    OPEN CAsesoria(NiConsecAsesor);
    FETCH CAsesoria INTO RAsesoria;
    CLOSE CAsesoria;
      
    -- 29537 (A) HOSSA - 20130131 -- Se adiciona condición para que sólo permita las asesorias que se procesan por SIC
    OPEN CSerAplazados(NiConsecAsesor);
    FETCH CSerAplazados INTO NCont;
    CLOSE CSerAplazados;
    IF NVL(NCont, 0) = 0 then--AND
      IF pks_asesorias.oficinaEnBpm(PKS_SESSIONES.RETORNAR_CODDEPENDENCIA) = 'SI' THEN
      --IF pks_asesorias.oficinaEnBpm(1) = 'SI' THEN
        -- 43054 JMarquez 20161005 (M) El sistema de gestión esta entre GPC y SIC
        --IF pks_asesorias.obtenerSistemaGestion(NiConsecAsesor) = 'NO' THEN
        IF pks_asesorias.obtenerSistemaGestion(NiConsecAsesor) = 'GPC' THEN
        -- FIN 43054 JMarquez 20161005 (M)  
          IF RAsesoria.Numero_Radicado IS NOT NULL THEN    
             -- 29537 (M) JACEVEDO 20130627 - Se verifica si la asesoria fue aplazada en BPM y se debe permitir su modificación
             --  VResult := 'NO';
             IF AplazadaBPM(NiConsecAsesor) = 'SI' THEN
                 VResult := 'SI';
             ELSE
                 VResult := 'NO';
             END IF;
             -- 29537 Linea Final
            RETURN(VResult);
          END IF;   
        END IF;   
      END IF;   
    END IF;

    /* 28-03-2006 JGLEN Puesto que la pantalla de cuotas extras se va a llamar desde gestión de cartera
    se debe validar que la asesoría exista para poder deshabilitar el ingreso de cuotas extras. Se agrega la
    condición validando que la asesoría no esté nula */
    IF NiConsecAsesor IS NULL THEN
      VResult := 'NO';
    ELSE
      -- 29537 (A) HOSSA - 20130131
      /*OPEN CAsesoria(NiConsecAsesor);
      FETCH CAsesoria INTO RAsesoria;
      CLOSE CAsesoria;*/
      -- 29537 (A) HOSSA - Fin modificación
      --
      -- Modificación: verificar que exista un número de renovación - JSIERRA - 09-12-2006
      --
      IF RAsesoria.Acredir_Id IS NOT NULL OR
         RAsesoria.numero_renovacion IS NOT NULL THEN
        RETURN(VResult);
      END IF;
      IF RAsesoria.Aestcon_Codigo IS NULL THEN
        VResult := 'SI';
        /* 12-07-2006 JGLEN No se puede devolver inmediatamente este resultado sin validar el parámetro días de
        vigencia de la asesoría. Por tanto, se pone en comentario el return */
        -- RETURN(VResult);
      ELSE
        VAestcon_Codigo := RAsesoria.Aestcon_Codigo;
      END IF;
      IF RAsesoria.Numero_Radicado IS NULL THEN
        OPEN CDiasAsesorias(NiConsecAsesor);
        FETCH CDiasAsesorias
          INTO NCont;
        CLOSE CDiasAsesorias;
        IF NVL(NCont, 0) = 0 THEN
          /* 12-07-2006 JGLEN Debe asignarse el valor NO cuando la asesoría sobrepasa los días de vigencia */
          VResult := 'NO';
          RETURN(VResult);
        END IF;
      END IF;
      NCont := 0;
      OPEN CASeEfectivos(NiConsecAsesor);
      FETCH CAseEfectivos
        INTO NCont;
      CLOSE CAseEfectivos;
      IF NVL(NCont, 0) > 0 THEN
        RETURN(VResult);
      END IF;
      IF VUsuario IS NOT NULL THEN
        -- 1
        NCont := 0;
        --29789 JNARANJO 20130601
        --OPEN CRolesUsuario(VUsuario, 'G_ASESOR_COMERCIAL');
        OPEN CRolesUsuario(VUsuario, 'G_ASESOR_COMERCIAL','G_PRACTICANTES_OFICINA');
        --29789 JNARANJO Fin modificación
        FETCH CRolesUsuario
          INTO NCont;
        CLOSE CRolesUsuario;
        IF NVL(NCont, 0) > 0 THEN
          -- Si es asesor comercial
          IF VAestcon_Codigo = '1.01' THEN
            -- Si la solicitud esta radicada
            NCont := 0;
            OPEN CSerEfectivos(NiConsecServic);
            FETCH CSerEfectivos
              INTO NCont;
            CLOSE CSerEfectivos;
            IF NVL(NCont, 0) > 0 THEN
              VResult := 'SI';
              RETURN(VResult);
            END IF;
          --03/01/2007            JGLEN                      3. Actualización para permitir modificar servicios aplazados en la oficina
          ELSIF VAestcon_Codigo = '1.17' THEN
          -- Si la solicitud está recibida en la oficina devuelta por el centro de operaciones
            NCont := 0;
            OPEN CSerAplazados(NiConsecServic);
            FETCH CSerAplazados
              INTO NCont;
            CLOSE CSerAplazados;
            IF NVL(NCont, 0) > 0 THEN
              VResult := 'SI';
              RETURN(VResult);
            END IF;
          END IF;
        END IF; -- Fin si es asesor comercial
        NCont := 0;
        OPEN CRolesUsuario(VUsuario, 'G_ANALISTAS_CO', 'G_ANALISTAS_CO_AH');
        FETCH CRolesUsuario
          INTO NCont;
        CLOSE CRolesUsuario;
        IF NVL(NCont, 0) > 0 THEN
          -- Si es analista
          --
          -- Fecha modificación : 10/11/04 04:31:30 p.m.
          -- Usuario modifica   : Alberto Eduardo Sánchez B.
          -- Causa modificación : Para la salida a producción se requiere que temporalmente las solicitudes que se encuentran en el estado de control 1.03 se puedan modificar por los analistas del centro de operaciones
          --
          IF VAestcon_Codigo IN ('1.05', '1.06', '1.03') THEN
            NCont := 0;
            OPEN CEstado(NiConsecasesor, '1.05');
            FETCH CEstado
              INTO VUsuFase;
            CLOSE CEstado;
            IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
              VResult := 'SI';
              RETURN(VResult);
            END IF;
            NCont := 0;
            OPEN CEstado(NiConsecasesor, '1.06');
            FETCH CEstado
              INTO VUsuFase;
            CLOSE CEstado;
            IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
              VResult := 'SI';
              RETURN(VResult);
            END IF;
          ELSIF VAestcon_Codigo IN ('1.40', '1.11') THEN
            NCont := 0;
            OPEN CCreditos(NiConsecAsesor);
            FETCH CCreditos
              INTO NCont;
            CLOSE CCreditos;
            IF NVL(NCont, 0) = 1 THEN
              NCont := 0;
              OPEN CAutorizado(NiConsecAsesor, VUsuario);
              FETCH CAutorizado
                INTO NCont;
              CLOSE CAutorizado;
              IF NVL(NCont, 0) > 0 THEN
                VResult := 'SI';
                RETURN(VResult);
              END IF;
            ELSE
              NCont := 0;
              OPEN CEstado(NiConsecasesor, '1.06');
              FETCH CEstado
                INTO VUsuFase;
              CLOSE CEstado;
              IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
                VResult := 'SI';
                RETURN(VResult);
              END IF;
            END IF;
          END IF;
        END IF; -- Fin Si es analista
        NCont := 0;
        OPEN CRolesUsuario(VUsuario,
                           'G_VERIFICADOR_CO',
                           'G_VERIFICADOR_CO_AH');
        FETCH CRolesUsuario
          INTO NCont;
        CLOSE CRolesUsuario;
        IF NVL(NCont, 0) > 0 THEN
          -- Si es verificador
          IF VAestcon_Codigo IN ('1.06') THEN
            NCont := 0;
            OPEN CEstado(NiConsecasesor, '1.06');
            FETCH CEstado
              INTO VUsuFase;
            CLOSE CEstado;
            IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
              OPEN CAnalisis(NiConsecasesor);
              FETCH CAnalisis
                INTO NCont;
              CLOSE CAnalisis;
              IF NVL(NCont, 0) > 0 THEN
                VResult := 'SI';
                RETURN(VResult);
              END IF;
            END IF;
          ELSIF VAestcon_Codigo IN ('1.40', '1.11') THEN
            NCont := 0;
            OPEN CCreditos(NiConsecAsesor);
            FETCH CCreditos
              INTO NCont;
            CLOSE CCreditos;
            IF NVL(NCont, 0) = 1 THEN
              NCont := 0;
              OPEN CAutorizado(NiConsecasesor, VUsuario);
              FETCH CAutorizado
                INTO NCont;
              CLOSE CAutorizado;
              IF NVL(NCont, 0) > 0 THEN
                OPEN CAnalisis(NiConsecasesor);
                FETCH CAnalisis
                  INTO NCont;
                CLOSE CAnalisis;
                IF NVL(NCont, 0) > 0 THEN
                  VResult := 'SI';
                  RETURN(VResult);
                END IF;
              END IF;
            ELSE
              NCont := 0;
              OPEN CEstado(NiConsecasesor, '1.06');
              FETCH CEstado
                INTO VUsuFase;
              CLOSE CEstado;
              IF NVL(VUsuFase, 'VACIO') = VUsuario THEN
                OPEN CAnalisis(NiConsecasesor);
                FETCH CAnalisis
                  INTO NCont;
                CLOSE CAnalisis;
                IF NVL(NCont, 0) > 0 THEN
                  VResult := 'SI';
                  RETURN(VResult);
                END IF;
              END IF;
            END IF;
          END IF;
        END IF; -- Fin Si es verificador
      END IF;
      --
      -- Fecha modificación : 10/11/04 04:12:33 p.m.
      -- Usuario modifica   : Alberto Eduardo Sánchez B.
      -- Causa modificación : Los niveles correspondientes podrán modificar las solicitudes en
      -- estado  SOLICITUD REMITIDA al nivel respectivo
      -- (coordinador, comité, Gerencia general, solicitud remitida) siempre y
      -- cuando el servicio no se encuentre efectivo.
      --
      NCont := 0;
      --29789 JNARANJO 20130601
      --OPEN CRolesUsuario(VUsuario,'G_COORDINADOR_CO','G_GERENTE_GENERAL','G_COMITE_CREDITOS');
      OPEN CRolesUsuario(VUsuario,'G_COORDINADOR_CO','G_GERENTE_GENERAL','G_COMITE_CREDITOS',vCooVivMicCom,vComVivMicCom);
      --29789 JNARANJO Fin modificación
      FETCH CRolesUsuario INTO NCont;
      CLOSE CRolesUsuario;
      IF NVL(NCont, 0) > 0 THEN
        --
        -- Req. 20069
        -- Fecha modificación : 08/05/2008 11:45:15 a.m.
        -- Usuario modifica   : Alberto Eduardo Sánchez B.
        -- Causa modificación : Se modifica para permitir que se modifique la asesoria si el estado de control es 1.30

        IF VAestcon_Codigo IN ('1.07','1.30') THEN
          -- Si la solicitud esta remitida
          NCont := 0;
          OPEN CSerEfectivos(NiConsecServic);
          FETCH CSerEfectivos
            INTO NCont;
          CLOSE CSerEfectivos;
          IF NVL(NCont, 0) > 0 THEN
            VResult := 'SI';
            RETURN(VResult);
          END IF;
        ELSIF VAestcon_Codigo IN ('1.40') THEN
          NCont := 0;
          OPEN CCreditos(NiConsecAsesor);
          FETCH CCreditos
            INTO NCont;
          CLOSE CCreditos;
          IF NVL(NCont, 0) = 1 THEN
            NCont := 0;
            OPEN CAutorizado(NiConsecAsesor, VUsuario);
            FETCH CAutorizado
              INTO NCont;
            CLOSE CAutorizado;
            IF NVL(NCont, 0) > 0 THEN
              VResult := 'SI';
              RETURN(VResult);
            END IF;
          END IF;
        END IF;
       --
       -- Fin Req. 20069
      END IF; -- Fin si es asesor comercial
      IF VAestcon_Codigo IN ('1.40') THEN
        -- Evaluada sin crédito
        NCont := 0;
        OPEN CCreditos(NiConsecAsesor);
        FETCH CCreditos
          INTO NCont;
        CLOSE CCreditos;
        IF NVL(NCont, 0) = 0 THEN
          NCont := 0;
          OPEN CRolesUsuario(VUsuario,
                             'G_ANALISTAS_CO',
                             'G_ANALISTAS_CO_AH');
          FETCH CRolesUsuario
            INTO NCont;
          CLOSE CRolesUsuario;
          IF NVL(NCont, 0) > 0 THEN
            -- Si es analista
            VResult := 'SI';
            RETURN(VResult);
          ELSE
            NCont := 0;
            OPEN CRolesUsuario(VUsuario,
                               'G_VERIFICADOR_CO',
                               'G_VERIFICADOR_CO_AH');
            FETCH CRolesUsuario
              INTO NCont;
            CLOSE CRolesUsuario;
            IF NVL(NCont, 0) > 0 THEN
              -- Si es verificador
              VResult := 'SI';
              RETURN(VResult);
            END IF;
          END IF;
        END IF;
      END IF; -- Fin Evaluada sin crédito
    END IF;

    RETURN(VResult);
  END AutorizacionServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 21/04/2004 10:02:00 p.m.
  -- Calulcar el procentaje de endeudamiento

  FUNCTION CalcularEndeudamiento(NiConsec_Asesor TS_ASESORIAS.Consecutivo_Asesoria%TYPE,
                                 ViTipo_Pers     TS_ASE_PERSONAS.Aperson_Type%TYPE,
                                 Viactualiza     IN VARCHAR2 DEFAULT 'SI')
    RETURN VARCHAR2 IS

    CURSOR CInfFinanciera IS
      SELECT Aperson.*, Aservic.Consecutivo_Servicio, Ase.Fecha_Asesoria
        FROM TS_ASE_SERVICIOS Aservic, TS_ASE_PERSONAS Aperson,
             TS_ASESORIAS Ase
       WHERE Aservic.Consecutivo_Servicio =
             Aperson.Aservic_Consecutivo_Servicio(+) AND
             Aservic.Ase_Consecutivo_Asesoria = Ase.Consecutivo_Asesoria AND
             NVL(Aperson.Ingresos_Basicos, 0) > 0 AND
             Ase.Consecutivo_Asesoria = Niconsec_Asesor
      --AND aperson.aperson_type = viTipo_pers -- Esta condicion se destapa ya que debe tener en cuenta a la persona 14-02-2005
       ORDER BY Aservic.Ser_Codigo DESC,
                NVL(Aperson.Ingresos_Basicos, 0) DESC;
    --
    -- Req. 19874
    -- Fecha modificación : 17/04/2008 02:46:48 p.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : la  forma de amortización de pago único ya que esta afectando el porcentaje de endeudamiento para los asociados
    CURSOR CCuotaActSer IS
      SELECT SUM(DECODE(SUBSTR(Aservic.Ser_Codigo, 1, 6),
                         '001.03',
                         NVL(Aservic.Crcuota_Total_Pago, 0),
                         NVL(Aservic.Cuota_Periodo_Pago, 0))) Cuota_Periodo_Pago
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             NVL(aservic.crtipo_amortizacion,'OTRO') = 'PAGOUN';
    --
    -- Fin Req. 19874

--             NVL(Aservic.Afforma_Amortizacion, 'OTRO') = 'UNICO';

    --
    -- Fecha: 06/05/2004 08:55:33 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para deteminar el item de cotrafa pago personal
    --

    CURSOR CDivisor IS
      SELECT Peringr.Numero_Dias --periodos_liquidacion_mensual
        FROM TS_ASESORIAS Seringr, TS_ADM_TIPO_PERIODOS Peringr
       WHERE Seringr.Atipper_Pago = Peringr.Nombre AND
             Seringr.Consecutivo_Asesoria = NiConsec_Asesor;

    --
    -- Fecha: 19-10-2004 01:11:08 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el valor de las cuotas de los créditos a refinanciar o restructurar
    --
    -- Fecha: 14-09-2005 18:30:08 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Se adiciona parametro de forma de pago para que tome los servicios por nomina solo cuando el servicio sea por nomina
    --

    /*Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo cursor
    CURSOR CRestruc(ViForma_Pago IN VARCHAR2) IS
      --Modifica Jorge Naranjo 20090828 Requerimiento 21437
      SELECT SUM(( NVL(Restruc.Crcuota_Total_Pago,NVL(Restruc.Cuota_Periodo_Pago, 0)) / Peringr.Numero_Dias ) * Pernuev.Numero_Dias) Cuota_Periodo_Pago
      --SELECT SUM(((DECODE(SUBSTR(Restruc.Ser_Codigo, 1, 6),
      --                     '001.03',
      --                     NVL(Restruc.Crcuota_Total_Pago, 0),
      --                     NVL(Restruc.Cuota_Periodo_Pago, 0))) /
      --            Peringr.Numero_Dias) * Pernuev.Numero_Dias) Cuota_Periodo_Pago
        FROM TS_ASE_SERVICIOS Aservic, TS_ASE_CRE_REESTRUCTURACIONES Acreres,
             TS_ASE_SERVICIOS Restruc, TS_ADM_TIPO_PERIODOS Peringr,
             TS_ADM_TIPO_PERIODOS Pernuev
       WHERE Aservic.Consecutivo_Servicio =
             Acreres.Aservic_Consecutivo_Servicio AND
             Restruc.Consecutivo_Servicio =
             Acreres.Arefinanciada_Reestructurada_a AND
             Esta es la parte adicionada 14-09-2005
             ((NVL(ViForma_Pago, 'CAJA') = 'NOMINA' AND
             Restruc.Aforpag_Nombre = 'NOMINA') OR
             NVL(ViForma_Pago, 'CAJA') <> 'NOMINA') AND
            ----------------------------------------
             Restruc.Atipper_Pago = Peringr.Nombre AND
             Aservic.Atipper_Pago = Pernuev.Nombre AND
             --Modifica Jorge Naranjo 20090918 Requerimiento 22473
             --No debe tener en cuenta las cuotas de los créditos con forma de amortización Pago Único, que refinancian o reestructuran otros créditos,
             --ya que éste valor está contenido en el cursor CCuotaActSer para crtipo_amortizacion = PAGOUN
             NVL(Aservic.crtipo_amortizacion,'OTRO') != 'PAGOUN' AND
             --Fin modificación 20090918 Requerimiento 22473
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;
    */

    --No debe tener en cuenta las cuotas de los créditos con forma de amortización Pago Único, que refinancian o reestructuran otros créditos,
    --ya que éste valor está contenido en el cursor CCuotaActSer para crtipo_amortizacion = PAGOUN
    --NIVEL 1 cuotas créditos refinanciar o reestructurar que sean por el mismo convenio y del mismo titular (no las que asuma donde es codeudor)
    --NIVEL 2 cuotas créditos refinanciar o reestructurar que sean del mismo titular (no las que asuma donde es codeudor)
    CURSOR CReestructuracion( viNivel IN VARCHAR2 ) IS
      SELECT SUM(pks_cpl0031.convertir_valor(NVL(restruc.crcuota_total_pago,NVL(restruc.cuota_periodo_pago, 0)),restruc.atipper_pago,aservic.atipper_pago)) Cuota_Periodo_Pago
        FROM ts_ase_servicios aservic, ts_ase_cre_reestructuraciones acreres,ts_ase_servicios restruc
       WHERE aservic.consecutivo_servicio = acreres.aservic_consecutivo_servicio
         AND restruc.consecutivo_servicio = acreres.arefinanciada_reestructurada_a
         AND aservic.per_id = restruc.per_id
         AND NVL(aservic.crtipo_amortizacion,'OTRO') != 'PAGOUN'
         AND ( ( viNivel = '1' AND restruc.aforpag_nombre = 'NOMINA'
                               AND restruc.pperocu_id IN ( SELECT pperocu.id
                                                           FROM ts_per_persona_ocupaciones pperocu,ts_per_persona_ocupaciones pperocuase
                                                           WHERE pperocu.con_deduccion_nomina = pperocuase.con_deduccion_nomina
                                                             AND pperocuase.id = aservic.pperocu_id ) )
               OR
               ( viNivel = '2' )
             )
         AND aservic.ase_consecutivo_asesoria = NiConsec_Asesor;
    --Fin modificación 20091123 Requerimiento 22760

    -- Alberto Eduardo sánchez B.
    -- 26/08/2004 11:06
    -- Se adiciono el siguiente cursor para la línea de ahorros Luz Edith García

    CURSOR CCuotaAct_Ahorr(NPer_Men IN NUMBER) IS
      SELECT SUM(NVL(NVL(Aservic.Crcuota_Total_Pago,
                          Aservic.Cuota_Periodo_Pago),
                      0) / NVL(Atipper.Numero_Dias, 0) * (NPer_Men))
        FROM TS_ASE_SERVICIOS Aservic, TS_ADM_TIPO_PERIODOS Atipper
       WHERE Aservic.Atipper_Pago = Atipper.Nombre AND
             Aservic.Aforpag_Nombre = 'NOMINA' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.02' AND
             NVL(Aservic.Tipo_Estado_Decision, 0) <>
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.15') AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    -- Alberto Eduardo sánchez B.
    -- 26/08/2004 11:06
    -- Se adiciono el siguiente cursor para determinar las cuotas de los ahorros por caja y debito

    CURSOR CCuotaCaj_Ahorr(NPer_Men IN NUMBER, NPer_Id IN NUMBER) IS
      SELECT SUM(NVL(NVL(Aservic.Crcuota_Total_Pago,
                          Aservic.Cuota_Periodo_Pago),
                      0) / NVL(Atipper.Numero_Dias, 0) * (NPer_Men))
        FROM TS_ASE_SERVICIOS Aservic, TS_ADM_TIPO_PERIODOS Atipper
       WHERE Aservic.Atipper_Pago = Atipper.Nombre AND
             Aservic.Aforpag_Nombre <> 'NOMINA' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.02'
            --          AND NVL(aservic.tipo_estado_decision,0) <> PK_CONSERES.sec_tipo_codigo(1264,'2.08')
            /* J.Glen 24/02/2005 Se valida que la condicion esté igual a la de la pantalla de asesoria */
             AND (Aservic.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') OR
             (Aservic.Numero_Servicio IS NOT NULL AND
             Aservic.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'))) AND
             Aservic.Per_Id = Nper_Id;

    NPer_Men NUMBER;

    RInfFin Cinffinanciera%ROWTYPE;
    ---Trae información de los ingresos del cliente
    RegIngresos TIngresos;
    --regendeudamiento      rendeudamiento;
    FteIngresos TIngresos;
    --    grAseServicioCre      Pks_proc_solicitudes.rAseServicioCre;
    VIncluyeAhorro       VARCHAR2(2);
    NCuotaAhorro         NUMBER(13);
    NNivel1Actual        NUMBER;
    NNivel2Actual        NUMBER;
    NNivel1Nuevo         NUMBER;
    NNivel2Nuevo         NUMBER;
    NNuevasCuota         NUMBER;
    NCuotaMaxima         NUMBER;
    NValorAdicional      NUMBER;
    --NValorRest           NUMBER;
    nVrReestrucNivel1    NUMBER;
    nVrReestrucNivel2    NUMBER;
    NCuotasActuales      NUMBER; -- Cuotas actuales de ahorro
    NCuotasActualesSolic NUMBER; -- Cuotas actuales de ahorro
    NCuotasAho           NUMBER;
    --Modifica Jorge Naranjo 20090825 Requerimiento 21437
    vMsjTecnico VARCHAR2(2000);--mensaje técnico
    vMsjUsuario VARCHAR2(2000);--mensaje de usuario
    bExitoInvocado BOOLEAN;--Éxito de la invocación del programa
    trCuotaServicio pks_cpl0031.trCuotaServicios;--Valores de las cuotas para cada servicio
    vMensaje VARCHAR2(2000);--Mensaje de cambio de cuotas
    nIngSalario NUMBER;--Valor obtenido para realizar el cálculo de la cuota
    eProceso EXCEPTION;--Excepción de proceso invocado
    nPosicion NUMBER;--Posición de recorrodo del record
    nVrCuotaServAfilia NUMBER := 0;--Almacena el valor de las cuotas de los servicios de afiliación
    nVrCuotaServAfiNueva NUMBER := 0;--Almacena el valor de las cuotas de los servicios de afiliación
    nServAfilia NUMBER;--Consecutivo del servicio de afiliación
    nVrTotalSerAfi NUMBER := 0;--Almacena el valor total de las cuotas de afiliación que van a cambiar
    nVrTotalSerAfiNueva NUMBER := 0;--Almacena el valor total de las cuotas de afiliación nuevas
    nVrTotalAfiNomina NUMBER := 0;--Valor diferencia por Nómina
    nVrTotalAfiDebito NUMBER := 0;--Valor diferencia por Débito
    nVrDifCuotasAfi NUMBER := 0;--Almacena el valor de la diferencia en las cuotas de los servicios de afiliación según los ingresos básicos de la asesoría
    --Fin modificación 20090825 Requerimiento 21437
    --Modifica Jorge Naranjo 20090915 Requerimiento 21437
    CURSOR cServiciosVerifica IS
      SELECT s.ser_codigo,ap.cuota_maxima,ap.endeudamiento_actual_i,ap.endeudamiento_actual_ii,ap.endeudamiento_nuevo_i,ap.endeudamiento_nuevo_ii,ap.aservic_consecutivo_servicio,ap.per_id
      FROM ts_asesorias ase,ts_ase_servicios s,ts_ase_personas ap
      WHERE ase.consecutivo_asesoria = s.ase_consecutivo_asesoria
        AND ap.aservic_consecutivo_servicio = s.consecutivo_servicio
        AND ase.Consecutivo_Asesoria = Niconsec_Asesor
        AND ap.aperson_type = ViTipo_Pers
      ORDER BY DECODE(SUBSTR(s.ser_codigo,1,6),'001.03',1,'001.01',2,'001.04',3,'001.02',4,5);
    rServVerifica cServiciosVerifica%ROWTYPE;
    --Fin modificación 20090915 Requerimiento 21437
    -- 25402 (A) 30/03/2011 JNARANJO
    vCumpleBasico VARCHAR2(2) := 'SI';
    vCumpleGlobal VARCHAR2(2) := 'SI';
    -- 25402 30/03/2011 Fin modificación
    -- 24717 (A) 07/04/2011 JNARANJO 
    pliEstRechazado PLS_INTEGER := pk_conseres.sec_tipo_codigo(1264, '2.13');
    pliEstAnulado PLS_INTEGER := pk_conseres.sec_tipo_codigo(1264, '2.22');
    --Identifica si existe un servicio rotativo en la asesoría
    CURSOR cServicioRotativo IS
      SELECT aservic.consecutivo_servicio,aservic.rocuota_base_solicitada,aservic.per_id,
             ( SELECT aperson.cuota_maxima
               FROM ts_ase_personas aperson 
               WHERE aperson.aservic_consecutivo_servicio = aservic.consecutivo_servicio
                 AND aperson.per_id = aservic.per_id
                 AND aperson.fecha_final IS NULL ) cuota_maxima
      FROM ts_ase_servicios aservic
      WHERE aservic.ase_consecutivo_asesoria = NiConsec_Asesor
        AND aservic.aservic_type = 'AROTATI'
        AND SUBSTR(aservic.ser_codigo,1,9) = '001.03.04'
        AND NVL(aservic.tipo_estado_decision,0) NOT IN ( pliEstRechazado,pliEstAnulado );
    rServicioRotativo cServicioRotativo%ROWTYPE;
    nVrCuotaBaseSugerida NUMBER;
    nVrCupoSugerido NUMBER;
    --41208 JNARANJO 20170405
    CURSOR cPeriodos ( vPeriodo VARCHAR2 ) IS
      SELECT peringr.numero_dias
      FROM ts_adm_tipo_periodos peringr
      WHERE peringr.nombre = vPeriodo;    
    rPeriodos cPeriodos%ROWTYPE;
  BEGIN
    rastro('INICIA  asesoria:'||NiConsec_Asesor,'ENDEUDA1');
    OPEN CDivisor;
    FETCH CDivisor INTO NPer_Men;
    CLOSE CDivisor;
    OPEN CCuotaAct_Ahorr(NPer_Men);
    FETCH CCuotaAct_Ahorr INTO NCuotasActualesSolic;
    CLOSE CCuotaAct_Ahorr;
    NCuotasActualesSolic := NVL(NCuotasActualesSolic, 0);
    OPEN CCuotaActSer;
    FETCH CCuotaActSer INTO NValorAdicional;
    CLOSE CCuotaActSer;
    NValorAdicional := NVL(NValorAdicional, 0);
    --Modifica Jorge Naranjo 20090825 Requerimiento 21437
    --Calcula las nuevas cuotas para los servicios de afiliación
    pks_cpl0031.verificar_cuota_solicitud( NiConsec_Asesor,
                                           vMensaje,
                                           trCuotaServicio,
                                           nIngSalario,
                                           vMsjTecnico,
                                           vMsjUsuario,
                                           bExitoInvocado );
    IF NOT bExitoInvocado THEN
      RAISE eProceso;
    END IF;
    --Verifica si existe diferencia en las cuotas
    IF NVL(vMensaje,'NO') != 'NO' THEN
      FOR nPosicion IN 1 .. trCuotaServicio.COUNT LOOP
        nServAfilia := trCuotaServicio(nPosicion).nConsServicio;
        nVrCuotaServAfilia := trCuotaServicio(nPosicion).nVrCuotaServicio;
        nVrCuotaServAfiNueva := trCuotaServicio(nPosicion).nVrCuota;
        nVrTotalSerAfi := nVrTotalSerAfi + (NVL(nVrTotalSerAfi,0) + NVL(nVrCuotaServAfilia,0));
        nVrTotalSerAfiNueva := nVrTotalSerAfiNueva + (NVL(nVrTotalSerAfiNueva,0) + NVL(nVrCuotaServAfiNueva,0));
        IF trCuotaServicio(nPosicion).vFormaPago = 'NOMINA' THEN
          nVrTotalAfiNomina := nVrTotalAfiNomina + (nVrCuotaServAfiNueva - nVrCuotaServAfilia);
        ELSE
          nVrTotalAfiDebito := nVrTotalAfiDebito + (nVrCuotaServAfiNueva - nVrCuotaServAfilia);
        END IF;
      END LOOP;
    END IF;
    nVrDifCuotasAfi := nVrTotalSerAfiNueva - nVrTotalSerAfi;
    --Fin modificación 20090825 Requerimiento 21437
    FOR RInfFin IN CInfFinanciera LOOP
      --41208 JNARANJO 20170327 
      IF RInfFin.Aperson_Type != 'SOLICI' THEN
        RInfFin.Modelo := 'NOMINA';
      END IF;
      --41208 JNARANJO 20170327 Fin modificación
      /* JGLEN 22/02/2005 Verificar calculos que aplican sólo al solicitante */
      NCuotaAhorro := 0;
      NCuotasAho   := 0;
      NNuevasCuota := 0;
      --NValorRest   := 0;Modifica Jorge Naranjo 20091123 Requerimiento 22760
      nVrReestrucNivel1 := 0;
      nVrReestrucNivel2 := 0;
      IF RInfFin.Aperson_Type = 'SOLICI' THEN
        NCuotasActuales := NCuotasActualesSolic;
      ELSE
        NCuotasActuales := 0;
      END IF;
      /* JGLEN */
      rastro('RInfFin.Consecutivo_Servicio:'||RInfFin.Consecutivo_Servicio,'CALCENDEU');
      --Modifica Jorge Naranjo 20090825 Requerimiento 21437
      /*IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
        GrAseServicioCre := ParGenAseServicio(RInfFin.Consecutivo_Servicio);
      END IF;*/
      GrAseServicioCre := ParGenAseServicio(RInfFin.Consecutivo_Servicio);
      --Fin modificación 20090825 Requerimiento 21437
      --Modifica Jorge Naranjo 20100526 Requerimiento 23314
      /*IF GrAseServicioCre.Aforpag_Nombre <> 'NOMINA' THEN
        OPEN CCuotaCaj_Ahorr(NPer_Men, RInfFin.Per_Id);
        FETCH CCuotaCaj_Ahorr INTO NCuotasAho;
        CLOSE CCuotaCaj_Ahorr;
      END IF;*/
      --Para la forma de pago DEBITO y modelo DEBITO, se debe tener en cuenta para el cálculo del endeudamiento
      IF NOT (GrAseServicioCre.Aforpag_Nombre = 'DEBITO' AND RInfFin.modelo = 'DEBITO') THEN
        OPEN CCuotaCaj_Ahorr(NPer_Men, RInfFin.Per_Id);
        FETCH CCuotaCaj_Ahorr INTO NCuotasAho;
        CLOSE CCuotaCaj_Ahorr;
      END IF;
      --Fin modificación 20100526
      NNivel1Actual := 0;
      NNivel1Nuevo  := 0;
      NNivel2Actual := 0;
      NNivel2Nuevo  := 0;
      --        grAseServicioCre:= Pks_proc_solicitudes.ParGenAseServicio(rInfFin.consecutivo_servicio);
      --Modifica Jorge Naranjo 20100222 Requerimiento 23107
      RegIngresos.Consecutivo_Servicio := RInfFin.Consecutivo_Servicio;
      RegIngresos.PerId :=                RInfFin.Per_Id;--40784 JNARANJO 20160412  
      RegIngresos.FechaInicial :=         RInfFin.Fecha_Inicial;--40784 JNARANJO 20160412  
      --Fin modificación 20100222
      RegIngresos.Ingresos_Basicos     := NVL(RInfFin.Ingresos_Basicos, 0);
      RegIngresos.Conceptos_Fijos      := NVL(RInfFin.Conceptos_Fijos, 0);
      RegIngresos.Otro_Ingresos        := NVL(RInfFin.Otro_Ingresos, 0);
      RegIngresos.Ingresos_Especiales  := NVL(RInfFin.Ingresos_Especiales,0);
      --Modifica Jorge Naranjo 20090825 Requerimiento 21437
      --RegIngresos.Deduccion_Nomina     := NVL(RInfFin.Deduccion_Nomina, 0);
      --RegIngresos.Deduccion_Personal   := NVL(RInfFin.Deduccion_Personal, 0);
      IF RInfFin.Aperson_Type = 'SOLICI' THEN --41208 JNARANJO 20170405 Agrego IF
        RegIngresos.Deduccion_Nomina     := NVL(RInfFin.Deduccion_Nomina,0) + NVL(nVrTotalAfiNomina,0);
        RegIngresos.Deduccion_Personal   := NVL(RInfFin.Deduccion_Personal,0) + NVL(nVrTotalAfiDebito,0);
      ELSE
        RegIngresos.Deduccion_Nomina     := NVL(RInfFin.Deduccion_Nomina,0);
        RegIngresos.Deduccion_Personal   := NVL(RInfFin.Deduccion_Personal,0);        
      END IF;
      --Fin modificación 20090825 Requerimiento 21437
      RegIngresos.Otras_Deducciones    := NVL(RInfFin.Otras_Deducciones, 0);
      RegIngresos.Gastos               := NVL(RInfFin.Gastos, 0);
      RegIngresos.Aforpag_Nombre       := GrAseServicioCre.Aforpag_Nombre;
      RegIngresos.Periodo_Colilla_Pago := RInfFin.Periodo_Colilla_Pago;
      IF NVL(RegIngresos.Ingresos_Basicos, 0) > 0 AND
         RegIngresos.Ingresos_Basicos <> FteIngresos.Ingresos_Basicos THEN
        FteIngresos := RegIngresos;
      END IF;

      --Calcula el valor para las reestructuraciones Nivel 1 y Nivel 2
      --Modifica Jorge Naranjo 20091123 Requerimiento 22760
      OPEN CReestructuracion('1');
      FETCH CReestructuracion INTO nVrReestrucNivel1;
      CLOSE CReestructuracion;
      OPEN CReestructuracion('2');
      FETCH CReestructuracion INTO nVrReestrucNivel2;
      CLOSE CReestructuracion;
      --Modifica Jorge Naranjo 20100525 Requerimiento 23314
      --IF GrAseServicioCre.Aforpag_Nombre != 'NOMINA' THEN
      IF RInfFin.modelo != 'NOMINA' THEN
      --Fin modificación 20100525
        nVrReestrucNivel1 := nVrReestrucNivel2;
      END IF;
      rastro('nVrReestrucNivel1'||nVrReestrucNivel1||' '||'nVrReestrucNivel2'||nVrReestrucNivel2,'ENDEUDA');
      --Fin modificación 20091123 Requerimiento 22760

      IF RInfFin.Consecutivo_Servicio = RInfFin.Aservic_Consecutivo_Servicio THEN
        IF SUBSTR(GrAseServicioCre.Ser_Codigo, 1, 6) IN ('001.01', '001.02', '001.03', '001.04') THEN
          rastro('VIncluyeAhorro cnvded:'||GrAseServicioCre.Con_Deduccion_Nomina||'  aforpag:'||GrAseServicioCre.Aforpag_Nombre,'CALCEND');
          IF GrAseServicioCre.Con_Deduccion_Nomina IS NOT NULL AND GrAseServicioCre.Aforpag_Nombre = 'NOMINA' THEN
            VIncluyeAhorro := NVL(Pks_Convenios.ParConvenioIncAhorros(GrAseServicioCre.Con_Deduccion_Nomina),'NO');            
            nIncluyeSegSocial := NVL(IncluyeSegSocial(GrAseServicioCre.Con_Deduccion_Nomina),0);--40784 JNARANJO 20160412            
            rastro('VIncluyeAhorro ParCnv '||VIncluyeAhorro,'CALCEND');
            IF VIncluyeAhorro = 'SI' THEN
              NCuotasActuales := 0;
            END IF;
          ELSE
            VIncluyeAhorro := 'NO';
            nIncluyeSegSocial := 0;--40784 JNARANJO 20160412
            rastro('VIncluyeAhorro ELSE ParCnv '||VIncluyeAhorro,'CALCEND');
          END IF;
          IF GrAseServicioCre.Aforpag_Nombre <> 'NOMINA' THEN
            VIncluyeAhorro := 'NO';
            nIncluyeSegSocial := 0;--40784 JNARANJO 20160412
            rastro('VIncluyeAhorro NOMINA '||VIncluyeAhorro,'CALCEND');
          END IF;
          --41208 JNARANJO 20170405
          IF RInfFin.Aperson_Type = 'CRECOD' THEN
            nIncluyeSegSocial := -1;
          END IF;
          --41208 JNARANJO 20170405 Fin modificación
          --
          -- Fecha modificación : 04-10-2004 09:25:04 a.m.
          -- Usuario modifica   : Alberto Eduardo Sánchez B.
          -- Causa modificación : Si no incluyen las cuotas de ahorro segun el convenio, se calculan para restarlas en la formula
          --
          IF RInfFin.Aperson_Type = 'SOLICI' THEN
            --Modifica Jorge Naranjo 20100219 Requerimiento 23107 Dejo en comentario IF VIncluyeAhorro = 'NO'
            --IF VIncluyeAhorro = 'NO' THEN
              --se hallan el total de las cuotas actuales de ahorro que tenga el solicitante
              NCuotaAhorro := NVL(Totalcuotasservicio(GrAseServicioCre.Per_Id,
                                                      '001.02',
                                                      NPer_Men,
                                                      GrAseServicioCre.Pperocu_Id), 0);
            --END IF;
            --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
            NNuevasCuota := NVL(ValorCuotaSolicitud(NiConsec_Asesor),0) - NVL(NValorAdicional, 0);
            /*Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo cursor ubico al inicio
            OPEN CRestruc(GrAseServicioCre.Aforpag_Nombre);
            FETCH CRestruc INTO NValorRest;
            CLOSE CRestruc;
            --Fin modificación 20091123 Requerimiento 22760*/

          --41208 JNARANJO 20170405
          ELSIF RInfFin.Aperson_Type = 'CRECOD' THEN
            OPEN cPeriodos(RInfFin.Atipper_Nombre);
            FETCH cPeriodos INTO rPeriodos;
            CLOSE cPeriodos;                   
            NCuotaAhorro := NVL(pks_proc_solicitudes.totalcuotasservicio(RInfFin.Per_Id,'001.02',rPeriodos.numero_dias),0);
          
          --41208 JNARANJO 20170405 Fin modificación
          END IF;
          rastro('VIncluyeAhorro '||VIncluyeAhorro||' '||'NCuotaAhorro '||NCuotaAhorro||' '||'NCuotasAho '||NCuotasAho||' '||'nVrDifCuotasAfi'||nVrDifCuotasAfi||'  numero_dias:'||rPeriodos.numero_dias,'ENDEUDA');
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
          NNivel1Actual := Nivel1Actual(RegIngresos,
                                        NVL(NCuotaAhorro,0), --  + NVL(NCuotasAho,0), JMarquez 20100527 
                                        GrAseServicioCre.Aforpag_Nombre,
                                        VIncluyeAhorro,
                                        0);
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs

          NNivel2Actual := Nivel2Actual(RegIngresos,
                                        NVL(NCuotaAhorro,0), -- + NVL(NCuotasAho,0), JMarquez 20100527
                                        0);
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
          --Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo NValorRest por nVrReestrucNivel1 o nVrReestrucNivel2 según el caso
          NNivel1Nuevo := Nivel1Actual(RegIngresos,
                                       NVL(NCuotaAhorro,0), -- + NVL(NCuotasAho,0), JMarquez 20100527
                                       GrAseServicioCre.Aforpag_Nombre,
                                       VIncluyeAhorro,
                                       NVL(NNuevasCuota,0) - NVL(nVrReestrucNivel1, 0) -
                                       NVL(NCuotasActuales, 0));
          --
          -- Fecha: 9-00-2005 18:30:08 p.m.
          -- Usuario: Alberto Eduardo Sánchez B.
          -- Reuqerimiento 7079
          -- Se adiciona parametro de forma de pago para que tome los servicios por nomina solo cuando el servicio sea por nomina
          --
          /*Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo cursor ubico al inicio
          IF RInfFin.Aperson_Type = 'SOLICI' THEN
            OPEN CRestruc(NULL);
            FETCH CRestruc INTO NValorRest;
            CLOSE CRestruc;
          END IF;
          --Fin modificación 20091123 Requerimiento 22760*/
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
          --Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo NValorRest por nVrReestrucNivel1 o nVrReestrucNivel2 según el caso

          NNivel2Nuevo := Nivel2Actual(RegIngresos,
                                       NVL(NCuotaAhorro,0), -- + NVL(NCuotasAho,0), JMarquez 20100527
                                       NVL(NNuevasCuota,0) - NVL(nVrReestrucNivel2, 0) -
                                       NVL(NCuotasActuales, 0));
          --Modifica Jorge Naranjo 20090813 Requerimiento 21437 Agrego parámetro RInfFin.modelo al llamado de CuotaMaxima
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
          NCuotaMaxima := CuotaMaxima(GrAseServicioCre.Consecutivo_Servicio,
                                      GrAseServicioCre.Con_Deduccion_Nomina,
                                      RegIngresos,
                                      NVL(NCuotaAhorro,0),
                                      GrAseServicioCre.Aforpag_Nombre,
                                      RInfFin.modelo);
        END IF;
        IF Viactualiza = 'SI' THEN
          rastro('NNivel1Actual  '||NNivel1Actual,'CALCEND');
          UPDATE TS_ASE_PERSONAS
             SET Endeudamiento_Actual_i = NNivel1Actual,
                 Endeudamiento_Actual_Ii = NNivel2Actual,
                 Endeudamiento_Nuevo_i = NNivel1Nuevo,
                 Endeudamiento_Nuevo_Ii = NNivel2Nuevo,
                 Cuota_Maxima = NCuotaMaxima, u_Actualizacion = USER,
                 f_Actualizacion = SYSDATE
           WHERE Aservic_Consecutivo_Servicio =
                 RInfFin.Consecutivo_Servicio AND Per_Id = RInfFin.Per_Id AND
                 Fecha_Final IS NULL;
          --AND aperson_type = viTipo_pers;-- Se agrega para actualizar el endeudamiento donde es debido 14-02-2005
          IF RInfFin.Aperson_Type = ViTipo_Pers THEN
            Regendeudamiento.ENDEUDAMIENTO_NUEVO_II := NNivel2Nuevo;
            Regendeudamiento.ENDEUDAMIENTO_NUEVO_I  := NNivel1Nuevo;
          END IF;
        ELSE
          IF RInfFin.Aperson_Type = ViTipo_Pers THEN
            Regendeudamiento.ENDEUDAMIENTO_NUEVO_II := NNivel2Nuevo;
            Regendeudamiento.ENDEUDAMIENTO_NUEVO_I  := NNivel1Nuevo;
          END IF;
        END IF;
      ELSE
        RegIngresos := FteIngresos;
        IF SUBSTR(GrAseServicioCre.Ser_Codigo, 1, 6) IN ('001.01', '001.03') THEN
          IF GrAseServicioCre.Con_Deduccion_Nomina IS NOT NULL THEN
            VIncluyeAhorro := NVL(Pks_Convenios.ParConvenioIncAhorros(GrAseServicioCre.Con_Deduccion_Nomina),'NO');
            nIncluyeSegSocial := NVL(IncluyeSegSocial(GrAseServicioCre.Con_Deduccion_Nomina),0);--40784 JNARANJO 20160412
            rastro('VIncluyeAhorro ParCnv ELSE '||VIncluyeAhorro,'CALCEND');
            IF VIncluyeAhorro = 'SI' THEN
              NCuotasActuales := 0;
            END IF;
          ELSE
            VIncluyeAhorro := 'NO';
            nIncluyeSegSocial := 0;--40784 JNARANJO 20160412
            rastro('VIncluyeAhorro ParCnv ELSE ELSE '||VIncluyeAhorro,'CALCEND');
          END IF;

          IF GrAseServicioCre.Aforpag_Nombre <> 'NOMINA' THEN
            VIncluyeAhorro := 'NO';
            nIncluyeSegSocial := 0;--40784 JNARANJO 20160412
            rastro('VIncluyeAhorro ParCnv ELSE NOMINA '||VIncluyeAhorro,'CALCEND');
          END IF;
          --41208 JNARANJO 20170405
          IF RInfFin.Aperson_Type = 'CRECOD' THEN
            nIncluyeSegSocial := -1;
          END IF;
          --41208 JNARANJO 20170405 Fin modificación          
          
          --
          -- Fecha modificación : 04-10-2004 09:25:04 a.m.
          -- Usuario modifica   : Alberto Eduardo Sánchez B.
          -- Causa modificación : Si no incluyen las cuotas de ahorro segun el convenio, se calculan para restarlas en la formula
          --
          --Modifica Jorge Naranjo 20100219 Requerimiento 23107 Dejo en comentario IF VIncluyeAhorro = 'NO'
          --IF VIncluyeAhorro = 'NO' THEN
          --Fin modificación 20100219
            --se hallan el total de las cuotas actuales de ahorro que tenga el solicitante
            NCuotaAhorro := NVL(Totalcuotasservicio(GrAseServicioCre.Per_Id,
                                                    '001.02',
                                                    NPer_Men), 0);
          --END IF;
          NNuevasCuota := ValorCuotaSolicitud(NiConsec_Asesor) - NVL(NValorAdicional, 0);
          /*Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo cursor ubico al inicio
          OPEN CRestruc(GrAseServicioCre.Aforpag_Nombre);
          FETCH CRestruc INTO NValorRest;
          CLOSE CRestruc;
          --Fin modificación 20091123 Requerimiento 22760*/
          rastro('VIncluyeAhorro '||VIncluyeAhorro||' '||'NCuotaAhorro '||NCuotaAhorro||' '||'NCuotasAho '||NCuotasAho||' '||'nVrDifCuotasAfi'||nVrDifCuotasAfi,'ENDEUDA');
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
          NNivel1Actual := Nivel1Actual(RegIngresos,
                                        NVL(NCuotaAhorro,0) + NVL(NCuotasAho,0),
                                        GrAseServicioCre.Aforpag_Nombre,
                                        VIncluyeAhorro,
                                        0);
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs

          NNivel2Actual := Nivel2Actual(RegIngresos,
                                        NVL(NCuotaAhorro,0) + NVL(NCuotasAho,0),
                                        0);
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
          --Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo NValorRest por nVrReestrucNivel1 o nVrReestrucNivel2 según el caso
          NNivel1Nuevo := Nivel1Actual(RegIngresos,
                                       NVL(NCuotaAhorro,0) + NVL(NCuotasAho,0),
                                       GrAseServicioCre.Aforpag_Nombre,
                                       VIncluyeAhorro,
                                       NVL(NNuevasCuota,0) - NVL(nVrReestrucNivel1, 0));
          --
          -- Fecha: 9-00-2005 18:30:08 p.m.
          -- Usuario: Alberto Eduardo Sánchez B.
          -- Reuqerimiento 7079
          -- Se adiciona parametro de forma de pago para que tome los servicios por nomina solo cuando el servicio sea por nomina
          --
          /*Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo cursor ubico al inicio
          IF RInfFin.Aperson_Type = 'SOLICI' THEN
            OPEN CRestruc(NULL);
            FETCH CRestruc INTO NValorRest;
            CLOSE CRestruc;
          END IF;
          --Fin modificación 20091123 Requerimiento 22760*/
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
          --Modifica Jorge Naranjo 20091123 Requerimiento 22760 Reemplazo NValorRest por nVrReestrucNivel1 o nVrReestrucNivel2 según el caso

          NNivel2Nuevo := Nivel2Actual(RegIngresos,
                                       NVL(NCuotaAhorro,0) + NVL(NCuotasAho,0),
                                       NVL(NNuevasCuota,0) - NVL(nVrReestrucNivel2, 0));
          --Modifica Jorge Naranjo 20090813 Requerimiento 21437 Agrego parámetro RInfFin.modelo al llamado de CuotaMaxima
          --Modifica Jorge Naranjo 20090817 Requerimiento 21437 Agrego NVLs
          NCuotaMaxima := CuotaMaxima(GrAseServicioCre.Consecutivo_Servicio,
                                      GrAseServicioCre.Con_Deduccion_Nomina,
                                      RegIngresos,
                                      NVL(NCuotaAhorro,0),
                                      GrAseServicioCre.Aforpag_Nombre,
                                      RInfFin.modelo);
        END IF;
        rastro('INSERT NNivel1Actual  '||NNivel1Actual,'CALCEND');
        INSERT INTO TS_ASE_PERSONAS
          (Aperson_Type, Fecha_Inicial, Ingresos_Basicos, Conceptos_Fijos,
           Ingresos_Especiales, Otro_Ingresos, Deduccion_Nomina,
           Deduccion_Personal, Gastos, Otras_Deducciones, Per_Id,
           Aservic_Consecutivo_Servicio, Cuota_Maxima,
           Endeudamiento_Actual_i, Endeudamiento_Actual_Ii,
           Endeudamiento_Nuevo_i, Endeudamiento_Nuevo_Ii,
           Periodo_Colilla_Pago, Fecha_Final, u_Actualizacion,
           f_Actualizacion, Atipper_Nombre)
        VALUES
          (ViTipo_Pers, RInfFin.Fecha_Asesoria,
           FteIngresos.Ingresos_Basicos, FteIngresos.Conceptos_Fijos,
           FteIngresos.Ingresos_Especiales, FteIngresos.Otro_Ingresos,
           FteIngresos.Deduccion_Nomina, FteIngresos.Deduccion_Personal,
           FteIngresos.Gastos, FteIngresos.Otras_Deducciones,
           GrAseServicioCre.Per_Id, RInfFin.Consecutivo_Servicio,
           NCuotaMaxima, NNivel1Actual, NNivel1Nuevo, NNivel2Actual,
           NNivel2Nuevo, RInfFin.Periodo_Colilla_Pago, NULL, NULL, NULL,
           GrAseServicioCre.Atipper_Pago);
      END IF;
    END LOOP;
    --Modifica Jorge Naranjo 20090915 Requerimiento 21437
    OPEN cServiciosVerifica;
    FETCH cServiciosVerifica INTO rServVerifica;
    CLOSE cServiciosVerifica;
    IF rServVerifica.aservic_consecutivo_servicio IS NOT NULL THEN
      UPDATE ts_ase_personas aperson
      SET aperson.cuota_maxima = rServVerifica.cuota_maxima,
          aperson.endeudamiento_actual_i = rServVerifica.endeudamiento_actual_i,
          aperson.endeudamiento_actual_ii = rServVerifica.endeudamiento_actual_ii,
          aperson.endeudamiento_nuevo_i = rServVerifica.endeudamiento_nuevo_i,
          aperson.endeudamiento_nuevo_ii = rServVerifica.endeudamiento_nuevo_ii
      WHERE aperson.aperson_type = ViTipo_Pers
        AND aperson.aservic_consecutivo_servicio != rServVerifica.aservic_consecutivo_servicio
        AND aperson.aservic_consecutivo_servicio IN ( SELECT aservic.consecutivo_servicio
                                                      FROM ts_ase_servicios aservic
                                                      WHERE aservic.ase_consecutivo_asesoria = Niconsec_Asesor );
    END IF;
    --Fin modificación 20090915 Requerimiento 21437

    -- 25402 (A) 30/03/2011 JNARANJO
    vCumpleBasico := pks_proc_solicitudes.VerificarEndeudNivel1(NiConsec_Asesor);
    vCumpleGlobal := pks_proc_solicitudes.VerificarEndeudNivel2(NiConsec_Asesor);   
    UPDATE ts_ase_personas aperson
    SET aperson.cumple_basico = DECODE(vCumpleBasico,'NO','SI','NO'),
        aperson.cumple_global = DECODE(vCumpleGlobal,'NO','SI','NO') 
    WHERE aperson.aservic_consecutivo_servicio IN ( SELECT aservic.consecutivo_servicio
                                                    FROM ts_ase_servicios aservic
                                                    WHERE aservic.ase_consecutivo_asesoria = NiConsec_Asesor );        
    -- 25402 30/03/2011 Fin modificación    
    
    -- 24717 (A) 07/04/2011 JNARANJO Identifica si existe un servicio de crédito rotativo en la asesoría
    OPEN cServicioRotativo;
    FETCH cServicioRotativo INTO rServicioRotativo;
    CLOSE cServicioRotativo;
    --Si existe debe calcular la cuota base sugerida y el cupo sugerido
    IF rServicioRotativo.consecutivo_servicio IS NOT NULL THEN
      nVrCuotaBaseSugerida := pks_creditos_rotativos.cuota_base_sugerida( niPerId => rServicioRotativo.per_id,
                                                                          niConsServicio => rServicioRotativo.consecutivo_servicio,
                                                                          niCuotaMaxima => NVL(rServicioRotativo.cuota_maxima,0),
                                                                          niRoCuotaBaseSolicitada => NVL(rServicioRotativo.rocuota_base_solicitada,0) );
    --24717 2011/09/17 JNARANJO Se separan las dos actualizaciones de ts_ase_servicios debido a que el cupo sugerido se 
    --calcula según los datos de la base de datos para la cuota base sugerida
      UPDATE ts_ase_servicios aservic
      SET aservic.rocuota_base_sugerida = nVrCuotaBaseSugerida
      WHERE aservic.consecutivo_servicio = rServicioRotativo.consecutivo_servicio;
      nVrCupoSugerido := pks_creditos_rotativos.cupo_sugerido( niPerId => rServicioRotativo.per_id,
                                                               niConsServicio => rServicioRotativo.consecutivo_servicio );
      UPDATE ts_ase_servicios aservic
      SET aservic.rocupo_sugerido = nVrCupoSugerido
      WHERE aservic.consecutivo_servicio = rServicioRotativo.consecutivo_servicio;
    END IF;
    -- 24717 Fin modificación
    --24717 2011/09/17 Fin modificación

    RETURN 'SI';
  EXCEPTION
    WHEN eProceso THEN
         rastro(SQLCODE||'-'||SQLERRM,'CalcularEndeudamiento');
         rastro('eProceso NiConsec_Asesor:'||NiConsec_Asesor||' NNivel1Actual:'||NNivel1Actual||' NNivel2Actual:'||NNivel2Actual||' NNivel1Nuevo:'||NNivel1Nuevo||' NNivel2Nuevo'||NNivel2Nuevo||' NCuotaMaxima'||NCuotaMaxima);
         RETURN 'NO';
    WHEN OTHERS THEN
         rastro(SQLCODE||'-'||SQLERRM,'CalcularEndeudamiento');
         rastro('NiConsec_Asesor:'||NiConsec_Asesor||' NNivel1Actual:'||NNivel1Actual||' NNivel2Actual:'||NNivel2Actual||' NNivel1Nuevo:'||NNivel1Nuevo||' NNivel2Nuevo'||NNivel2Nuevo||' NCuotaMaxima'||NCuotaMaxima);
         RETURN 'NO';
  END CalcularEndeudamiento;

  FUNCTION CambioConvenio(NiId TS_PERSONAS.Id%TYPE) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_CambioConvenio

      PROPOSITO:
      Permite verificar si el cambio de convenio afecta el período de pago o
      el período de liquidación de los servicios que posee el cliente

      PARAMETROS:
      niId ts_personas.id%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      28/04/04            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      Poseer servicios en estado efectivo

      NOTAS:
      Para los servicios en estudio es necesario que el usuario se remita a
       la forma de servicios y realice las validaciones desde allí, además las
       modiifaciones a los servicios se realizaran desde Foxprow y se actualizaran mediante la interface

    ****************************************************************************************************/
    VResult  VARCHAR2(2);
    NCont    NUMBER; -- Utilizada para controlar la cantidad de registros que no cumplen la condición
    VPerPago TS_CNV_DEDUCCIONES.Atipper_Nombre_Libranza%TYPE; -- Utilizada para determinar el período de pago del convenio
    VPerLiqu TS_CNV_DEDUCCIONES.Atipper_Nombre_Liquidado%TYPE; -- Utilizada para determinar el período de liquidación del convenio

    --
    -- Fecha: 28/04/04 09:57:46 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion de servicios que no poseen el período de pago del nuevo convenio
    --
    CURSOR CPerPagos IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Atipper_Pago <> VPerPago AND
             Aservic.Atipper_Liquidacion <> VPerLiqu AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08')) AND
             Aservic.Per_Id = NiId;

    --
    -- Fecha: 28/04/04 10:21:06 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el período de pago del convenio
    --
    CURSOR CInfConvenio IS
      SELECT Cdeduci.Atipper_Nombre_Libranza,
             Cdeduci.Atipper_Nombre_Liquidado
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu, TS_CNV_DEDUCCIONES Cdeduci
       WHERE Pperocu.Con_Deduccion_Nomina = Cdeduci.Con_Id AND
             Pperocu.Per_Id = NiId;

  BEGIN
    OPEN CInfConvenio;
    FETCH CInfConvenio
      INTO VPerPago, VPerLiqu;
    CLOSE CInfConvenio;
    OPEN CPerPagos;
    FETCH CPerPagos
      INTO NCont;
    CLOSE CPerPagos;
    IF NVL(NCont, 0) > 0 THEN
      VResult := 'SI';
    ELSE
      VResult := 'NO';
    END IF;
    RETURN(VResult);
  END CambioConvenio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 28/04/2004 08:47:00 a.m.
  -- Verificar si por el cambio de convenio
  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 09:47:00 a.m.
  -- Verifica el número de crédtios por la misma línea.

  FUNCTION CreditosLinea(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    NCont   NUMBER(3) := 0;
    NCon    NUMBER(3) := 0;
    VSerCod TS_ASE_SERVICIOS.Ser_Codigo%TYPE;
    NPer_Id TS_ASE_SERVICIOS.Per_Id%TYPE;

    CURSOR CCreditos IS
      SELECT Aservic.Ser_Codigo, Aservic.Per_Id
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic;

    CURSOR CServicios IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ser_Codigo = VSerCod AND Aservic.Per_Id = NPer_Id AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08')) AND NOT EXISTS
       (SELECT 1
                FROM TS_ASE_CRE_REESTRUCTURACIONES r
               WHERE r.Arefinanciada_Reestructurada_a =
                     Aservic.Consecutivo_Servicio AND
                     r.Aservic_Consecutivo_Servicio = NiConsec_Servic);

    CURSOR CPoliticas IS
      SELECT 1
        FROM VS_SER_CREDITOS Vsercre
       WHERE (Vsercre.Ser_Codigo = VserCod OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 9) OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 6)) AND
             Vsercre.Vigentes_Igual_Line > NCont
       ORDER BY Vsercre.Ser_Codigo, SUBSTR(Vsercre.Ser_Codigo, 1, 9),
                SUBSTR(Vsercre.Ser_Codigo, 1, 6);
    -- 24717 (A) 23/05/2011 JNARANJO
    CURSOR cPoliticasRot IS
      SELECT 1
        FROM Vs_Ser_Cre_Rotativos Vsercre
       WHERE (Vsercre.Ser_Codigo = VserCod OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 9) OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 6)) AND
             Vsercre.Vigentes_Igual_Line > NCont
       ORDER BY Vsercre.Ser_Codigo, SUBSTR(Vsercre.Ser_Codigo, 1, 9),
                SUBSTR(Vsercre.Ser_Codigo, 1, 6);
    -- 24717 23/05/2011 Fin modificación

  BEGIN
    OPEN CCreditos;
    FETCH CCreditos
    INTO VSerCod, NPer_Id;
    CLOSE CCreditos;
    IF SUBSTR(VSerCod, 1, 6) = '001.03' THEN
      OPEN CServicios;
      FETCH CServicios INTO NCont;
      CLOSE CServicios;
      -- 24717 (A) 23/05/2011 JNARANJO
      IF SUBSTR(VSerCod,1,9) != '001.03.04' THEN
        OPEN CPoliticas;
        FETCH CPoliticas INTO NCon;
        CLOSE CPoliticas;
      ELSE
        OPEN cPoliticasRot;
        FETCH cPoliticasRot INTO NCon;
        CLOSE cPoliticasRot;        
      END IF;
      -- 24717 23/05/2011 Fin modificación
      IF NVL(NCon, 0) = 0 THEN
        RETURN 'SI';
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END CreditosLinea;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 09:47:00 a.m.
  -- Verifica el número de créditos por otra línea.

  FUNCTION CreditosOtrasLinea(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    NCont   NUMBER(3);
    NCon    NUMBER(3);
    VSerCod TS_ASE_SERVICIOS.Ser_Codigo%TYPE;
    NPer_Id TS_ASE_SERVICIOS.Per_Id%TYPE;

    CURSOR CCreditos IS
      SELECT Aservic.Ser_Codigo, Aservic.Per_Id
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic;

    CURSOR CServicios IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ser_Codigo <> VSerCod AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 9) <> '001.03.02' AND
             Aservic.Per_Id = NPer_Id AND
             Aservic.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') AND NOT EXISTS
       (SELECT 1
                FROM TS_ASE_CRE_REESTRUCTURACIONES r
               WHERE r.Arefinanciada_Reestructurada_a =
                     Aservic.Consecutivo_Servicio AND
                     r.Aservic_Consecutivo_Servicio = NiConsec_Servic);

    -- Amartelo
    -- 17/11/2004
    -- Se quita de la validación los servicios en estudio
    -- Por solicitud de Sandra Vargas Orozco
    /* IN (PK_CONSERES.sec_tipo_codigo(1264,'2.01'),
    PK_CONSERES.sec_tipo_codigo(1264,'2.08'));*/

    CURSOR CPoliticas IS
      SELECT COUNT(1)
        FROM VS_SER_CREDITOS Vsercre
       WHERE (Vsercre.Ser_Codigo = VserCod OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 9) OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 6)) AND
             Vsercre.Vigentes_Otras_Line < NCont
       ORDER BY Vsercre.Ser_Codigo, SUBSTR(Vsercre.Ser_Codigo, 1, 9),
                SUBSTR(Vsercre.Ser_Codigo, 1, 6);

  BEGIN
    OPEN CCreditos;
    FETCH CCreditos
      INTO VSerCod, NPer_Id;
    CLOSE CCreditos;
    IF SUBSTR(VSerCod, 1, 6) = '001.03' THEN
      OPEN CServicios;
      FETCH CServicios
        INTO NCont;
      CLOSE CServicios;
      OPEN CPoliticas;
      FETCH CPoliticas
        INTO NCon;
      CLOSE CPoliticas;
      IF NVL(NCon, 0) > 0 THEN
        RETURN 'SI';
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END CreditosOtrasLinea;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 30/03/2004 09:47:00 a.m.
  -- Determina el total de los compromisos que tiene un cliente  en la cooperativa
  -- con una determinada forma de pago, pero teniendo en cuenta el tipo de periodo de pago
  -- en cuyo caso se debe convertir si es diferente al del parametro.

  FUNCTION CompromisosCooperativa(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE,
                                  ViTipo_Per      TS_ASE_PERSONAS.Aperson_Type%TYPE)
    RETURN VARCHAR2 IS

    NPerId        NUMBER; --Id de la persona
    NDeduccionesN NUMBER; --Valor de las deducciones por nomina
    VTipoPer      TS_ASE_SERVICIOS.Atipper_Pago%TYPE;

    CURSOR CServicios IS
      SELECT Aservic.Per_Id, Aservic.Atipper_Pago, Aperson.Deduccion_Nomina
        FROM TS_ASE_SERVICIOS Aservic, TS_ASE_PERSONAS Aperson
       WHERE Aservic.Consecutivo_Servicio =
             Aperson.Aservic_Consecutivo_Servicio AND
             SYSDATE BETWEEN Aperson.Fecha_Inicial AND
             NVL(Aperson.Fecha_Final, SYSDATE) AND
             Aperson.Aperson_Type = ViTipo_Per AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

    --Se buscan todos los compromisos que tenga la persona con la cooperativa,
    --Con las condicines: forma Pago nomina, Estado Efectivo
    CURSOR CCompromisos(NEstado IN NUMBER) IS
      SELECT S.Atipper_Pago, s.Cuota_Periodo_Pago
        FROM Vs_Aser a, TS_ASE_SERVICIOS s
       WHERE s.Aforpag_Nombre = 'NOMINA' AND
             s.Consecutivo_Servicio = a.Consecutivo_Servicio AND
             a.Per_Id = Nperid AND a.AESTCON_CODIGO = NEstado;

    NTotCompromisos NUMBER := 0; -- Total de los compromisos en la cooperativa
    NCuota          NUMBER := 0;
    NCodEstado      NUMBER := 0;

  BEGIN
    OPEN CServicios;
    FETCH CServicios
      INTO NPerId, VTipoPer, NDeduccionesN;
    CLOSE CServicios;
    NCodEstado      := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08');
    NTotCompromisos := 0;
    FOR CdCompromisos IN CCompromisos(NCodEstado) LOOP
      IF CdCompromisos.Atipper_Pago <> VTipoPer THEN
        NCuota          := F118(NiConsec_Servic,
                                CdCompromisos.Cuota_Periodo_Pago,
                                CdCompromisos.Atipper_Pago);
        NTotCompromisos := NTotCompromisos + NCuota;
      ELSE
        NTotCompromisos := NTotCompromisos +
                           CdCompromisos.Cuota_Periodo_Pago;
      END IF;
    END LOOP;
    IF NVL(NDeduccionesN, 0) = 0 AND NVL(NTotCompromisos, 0) = 0 THEN
      RETURN('NO');
    ELSE
      IF NDeduccionesN >= NVL(NTotCompromisos, 0) THEN
        RETURN('NO');
      ELSE
        RETURN('SI');
      END IF;
    END IF;
  END CompromisosCooperativa;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 30/03/2004 09:47:00 a.m.
  -- Determina el total de los comporomisos que tiene un cliente  en la cooperativa
  -- con una determinada forma de pago, pero teniendo en cuenta el tipo de periodo de pago
  -- en cuyo caso se debe convertir si es diferente al del parametro.

  FUNCTION TotalCompromisos(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE,
                            ViTipo_Per      TS_ASE_PERSONAS.Aperson_Type%TYPE)
    RETURN NUMBER IS

    NPerId        NUMBER; --Id de la persona
    NDeduccionesN NUMBER; --Valor de las deducciones por nomina
    VTipoPer      TS_ASE_SERVICIOS.Atipper_Pago%TYPE;

    CURSOR CServicios IS
      SELECT Aservic.Per_Id, Aservic.Atipper_Pago, Aperson.Deduccion_Nomina
        FROM TS_ASE_SERVICIOS Aservic, TS_ASE_PERSONAS Aperson
       WHERE Aservic.Consecutivo_Servicio =
             Aperson.Aservic_Consecutivo_Servicio AND
             SYSDATE BETWEEN Aperson.Fecha_Inicial AND
             NVL(Aperson.Fecha_Final, SYSDATE) AND
             Aperson.Aperson_Type = ViTipo_Per AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

    --Se buscan todos los compromisos que tenga la persona con la cooperativa,
    --Con las condicines: forma Pago nomina, Estado Efectivo
    CURSOR CCompromisos(NEstado IN NUMBER) IS
      SELECT S.Atipper_Pago, s.Cuota_Periodo_Pago
        FROM Vs_Aser a, TS_ASE_SERVICIOS s
       WHERE s.Aforpag_Nombre = 'NOMINA' AND
             s.Consecutivo_Servicio = a.Consecutivo_Servicio AND
             a.Per_Id = Nperid AND a.AESTCON_CODIGO = NEstado;

    NTotCompromisos NUMBER := 0; -- Total de los compromisos en la cooperativa
    NCuota          NUMBER := 0;
    NServicio       NUMBER := 0;
    NCodEstado      NUMBER := 0;

  BEGIN
    OPEN CServicios;
    FETCH CServicios
      INTO NPerId, VTipoPer, NDeduccionesN;
    CLOSE CServicios;
    NCodEstado      := Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08');
    NTotCompromisos := 0;
    FOR CdCompromisos IN CCompromisos(NCodEstado) LOOP
      IF CdCompromisos.Atipper_Pago <> VTipoPer THEN
        NCuota          := F118(NServicio,
                                CdCompromisos.Cuota_Periodo_Pago,
                                CdCompromisos.Atipper_Pago);
        NTotCompromisos := NTotCompromisos + NCuota;
      ELSE
        NTotCompromisos := NTotCompromisos +
                           CdCompromisos.Cuota_Periodo_Pago;
      END IF;
    END LOOP;
    IF NVL(NTotCompromisos, 0) > 0 THEN
      RETURN(NTotCompromisos);
    ELSE
      RETURN(0);
    END IF;
  END TotalCompromisos;

  FUNCTION ConceptoFijos(NiId IN TS_ASE_SERVICIOS.Pperocu_Id%TYPE)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_ConceptoFijos

      PROPOSITO:
      Verificar si la empresa posee conceptos fijos

      PARAMETROS:
      NiId ts_ase_servicios.pperocu_id

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      10/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      que poseea convenio de deducción denómina




    ****************************************************************************************************/
    Result  VARCHAR2(1000);
    VNombre TS_PERSONAS.Nombre_Completo%TYPE; -- Utilizada para determinar el nombre completo de la empresa

    --
    -- Fecha: 10/05/2004 08:17:59 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si la entidad tiene conceptos fijos
    --
    CURSOR CConceptoFijo IS
      SELECT Per.Conceptos_Fijos, Per.Nombre_Completo
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu, TS_PERSONAS Per,
             TS_CNV_DEDUCCIONES Cdeducc
       WHERE Pperocu.Con_Deduccion_Nomina = Cdeducc.Con_Id AND
             Pperocu.Per_Id_Vinculado_a = Per.Id AND Pperocu.Id = NiId;

  BEGIN
    OPEN CConceptoFijo;
    FETCH CConceptoFijo
      INTO Result, VNombre;
    CLOSE CConceptoFijo;
    IF Result IS NOT NULL THEN
      Result := 'Sólo aplican para la empresa ' || VNombre ||
                ' los siguientes conceptos fijos: ' || Result;
      RETURN(Result);
    ELSE
      RETURN 'NO';
    END IF;
  END ConceptoFijos;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/04/2004 02:47:00 p.m.
  -- Verifica si el cliente esta asociado a un convenio de deducción de nómina
  -- en estado APROBADO

  FUNCTION ConvenioAprobado(NiId IN TS_CONVENIOS.Id%TYPE -- Id del convenio
                            ) RETURN VARCHAR2 IS

    CURSOR CConvenio IS
      SELECT Con.Estado_Actual
        FROM TS_CONVENIOS Con
       WHERE Con.Id = NiId;

    VEstado TS_CONVENIOS.ESTADO_ACTUAL%TYPE;

  BEGIN
    OPEN CConvenio;
    FETCH CConvenio
      INTO VEstado;
    CLOSE CConvenio;
    IF VEstado IS NOT NULL THEN
      IF VEstado IN ('APROBA') THEN
        RETURN 'NO';
      ELSE
        RETURN 'SI';
      END IF;
    END IF;
  END ConvenioAprobado;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 30/03/2004 09:47:00 a.m.
  -- Retorna el nombre del convenio
  -- teniendo el id de la ocupación.

  FUNCTION ConvenioServicio(NiId IN TS_PER_PERSONA_OCUPACIONES.ID%TYPE -- Id del convenio
                            ) RETURN VARCHAR2 IS

    CURSOR CConvenio IS
      SELECT Con.Nombre
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu, TS_CONVENIOS Con
       WHERE Pperocu.Con_Deduccion_Nomina = Con.Id AND
             SYSDATE BETWEEN Pperocu.Fecha_Inicial AND
             NVL(Pperocu.Fecha_Final, SYSDATE) AND Pperocu.Id = NiId;

    VConvenio TS_CONVENIOS.NOMBRE%TYPE;

  BEGIN
    OPEN CConvenio;
    FETCH CConvenio
      INTO VConvenio;
    CLOSE CConvenio;
    IF VConvenio IS NOT NULL THEN
      RETURN VConvenio;
    ELSE
      RETURN NULL;
    END IF;
  END ConvenioServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 12:00:00 p.m.
  -- Esta funcion convierte los ingresos basicos   a ingresos basicos mensuales
  -- si el periodo de pago es diferente a mensual
  -- Formula 3 cpl0129
  -- Valor salario mensual = Salario enviado / Numero de dias del periodo con el cual se envio la informacion * 30
  -- Se paso para este paquete ya que es aqui donde se referencia la formula tres -- Moav Agosto 19 de 2003

  FUNCTION ConvIngBasico(ViTipoPer       IN TS_ADM_TIPO_PERIODOS.Nombre%TYPE,
                         NiIngresoBasico IN NUMBER) RETURN NUMBER IS

    CURSOR CDiasPer IS
      SELECT Tp.Numero_Dias
        FROM TS_ADM_TIPO_PERIODOS Tp
       WHERE Tp.Nombre = ViTipoPer;

    NDiasPer NUMBER;

  BEGIN
    OPEN CDiasPer;
    FETCH CDiasPer
      INTO NDiasPer;
    CLOSE CDiasPer;
    IF NDiasPer IS NOT NULL THEN
      RETURN(NiIngresoBasico / NDiasPer * 30);
    ELSE
      RETURN(0);
    END IF;
  END ConvIngBasico;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion funcion convierte un salario basico mensual en su
  -- equivalente a una base de SMLMV

  FUNCTION ConvSMLMV(NiIngMensual IN NUMBER, NiBase IN NUMBER) RETURN NUMBER IS

  BEGIN
    RETURN(NiIngMensual / NiBase);
  END;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Calcula la cuota maxima segun cpl0031
  --Modifica Jorge Naranjo 20090813 Requerimiento 21437 Agrego parámetro viModelo el cual contiene el modelo seleccionado para la asesoría (Nómina o Débito)
  FUNCTION CuotaMaxima(NiConsec_Servic IN NUMBER,
                       NiConvenio      IN NUMBER,
                       RiIngresos      IN Pks_Proc_Solicitudes.TIngresos,
                       NiCuotaAhorro   IN NUMBER,
                       VFormaPago      IN VARCHAR2,
                       viModelo IN VARCHAR2 ) RETURN NUMBER IS

    NCuotaMaxima   NUMBER;
    NPEndeudaMax   NUMBER;
    VIncluyeAhorro VARCHAR2(2);

    --Modifica Jorge Naranjo 20090723 Requerimiento 21437
    -- Si la forma de pago es por nómina
    --   Si según el convenio no se deben incluir los ahorros para el análisis de la capacidad de endeudamiento
    --     Cuota máxima = redondear((Ingresos básicos + conceptos fijos) * porcentaje endeudamiento máximo / 100  (el menor entre el general, el convenio y la línea de crédito) - Deducciones por nómina + cuotas actuales ahorros)
    --   Si no
    --     Cuota máxima = redondear((Ingresos básicos + conceptos fijos) * porcentaje endeudamiento máximo / 100  (el menor entre el general, el convenio y la línea de crédito) - Deducciones por nómina)
    -- Si no
    --   Cuota máxima = redondear((Ingresos básicos + conceptos fijos + otros ingresos + ingresos especiales) * porcentaje endeudamiento máximo / 100 (el menor entre el general y la línea de crédito) - (Deducciones por nómina + Deducciones en Cotrafa con pago personal + Otras deducciones + Gastos) + cuotas actuales ahorros)
    --   En este caso el % de endeudamiento máximo es el 100%

    -- Nota: Se debe calcular la cuota máxima considerando los compromisos que posee en otras entidades (nivel 2). Si ésta es menor a la calculada con el nivel 1, se asume ésta como cuota máxima.

    -- Cuota máxima (Nivel 2) = redondear((Ingresos básicos + conceptos fijos + otros ingresos + ingresos especiales) * 100% - (Deducciones por nómina + Deducciones en Cotrafa con pago personal + Otras deducciones + Gastos) + cuotas actuales ahorros)
    nCuotaMaxGlobal NUMBER;
    nPorcEndGlobal NUMBER;
  --Modifica Jorge Naranjo 20100219 Requerimiento 23107
  CURSOR cAperson( niConsServicio IN NUMBER ) IS
    SELECT aperson.modelo
    FROM ts_ase_personas aperson
    WHERE aperson.aservic_consecutivo_servicio = niConsServicio
      AND aperson.modelo IS NOT NULL;
  rAperson cAperson%ROWTYPE;
  --Fin modificación 20100219
  -------------------------------------
    -- 25211 (M) 08/03/2011 JNARANJO 
    CURSOR cRiesgoEndeudamiento( niArieend IN NUMBER ) IS
      SELECT arieend.endeudamiento_global
      FROM ts_adm_riesgo_endeudamientos arieend
      WHERE arieend.id = niArieend;
    rRiesgoEndeudamiento cRiesgoEndeudamiento%ROWTYPE;
    trResultadoRiesgos pks_cpl0436.VResulRiesgos;
    nCantVariables PLS_INTEGER;
    nArieendId PLS_INTEGER;
  --------------------------------------
  --Modifica Jorge Naranjo 20100526 Requerimiento 23314
  nCuotaAhorro NUMBER := NVL(NiCuotaAhorro,0);
  --Fin modificación 20100526

  --26470 2012/04/16 JNARANJO
  CURSOR cCreditosReestrRefina IS
    SELECT SUM(pks_cpl0031.convertir_valor(NVL(restruc.crcuota_total_pago,NVL(restruc.cuota_periodo_pago, 0)),restruc.atipper_pago,aservic.atipper_pago)) Cuota_Periodo_Pago
    FROM ts_ase_servicios aservic, ts_ase_cre_reestructuraciones acreres,ts_ase_servicios restruc
    WHERE aservic.consecutivo_servicio = acreres.aservic_consecutivo_servicio
      AND restruc.consecutivo_servicio = acreres.arefinanciada_reestructurada_a
      AND aservic.per_id = restruc.per_id
      AND aservic.consecutivo_servicio = NiConsec_Servic;
  rCreditosReestrRefina cCreditosReestrRefina%ROWTYPE;
  --26470 2012/04/16 Fin modificación         

  BEGIN
    --Modifica Jorge Naranjo 20100219 Requerimiento 23107
    OPEN cAperson(RiIngresos.Consecutivo_Servicio);
    FETCH cAperson INTO rAperson;
    CLOSE cAperson;
    --Fin modificación 20100219

    --se determina el pocercentaje de endeudamiento que se debe aplicar para la cuota maxima
    --Modifica Jorge Naranjo 20090813 Requerimiento 21437
    --NPEndeudaMax := PorcentajeEndeudamiento(NiConsec_Servic);
    --Nivel 1:

    rastro('Deduccion_Nomina '||NVL(RiIngresos.Deduccion_Nomina,0) || CHR(13) ||
           'NiCuotaAhorro '||NVL(NiCuotaAhorro, 0) || CHR(13) ||
           'Ingresos_Basicos '||NVL(RiIngresos.Ingresos_Basicos, 0) || CHR(13) ||
           'Conceptos_Fijos '||NVL(RiIngresos.Conceptos_Fijos, 0) || CHR(13) ||
           'Deduccion_Personal '||NVL(RiIngresos.Deduccion_Personal, 0) || CHR(13) ||
           'otras_deducciones '||NVL(RiIngresos.otras_deducciones, 0) || CHR(13) ||
           'gastos '||NVL(RiIngresos.gastos, 0) || CHR(13) ||
           'Otro_Ingresos '||NVL(RiIngresos.Otro_Ingresos, 0) || CHR(13) ||
           'Ingresos_Especiales '||NVL(RiIngresos.Ingresos_Especiales, 0)   
           ,'CUOMAX');

  ----------------------------------
    NPEndeudaMax := PorcentajeEndeudamiento(NiConsec_Servic,viModelo);
    --Modifica Jorge Naranjo 20090707 Requerimiento 21437
/*    GrAseServicioCre := ParGenAseServicio(NiConsec_Servic);*/
    IF NiConsec_Servic IS NOT NULL THEN

      pks_cpl0436.obtener_variables( NiSer => NiConsec_Servic,
                                     ViSistema =>  'ASESOR', 
                                     VoResulRiesgos =>  trResultadoRiesgos,
                                     NoNroVar =>  nCantVariables );
      IF NVL(nCantVariables,0) > 0 THEN
        IF trResultadoRiesgos(1).NIdRiesgo IS NOT NULL THEN
          pks_cpl0436.verificar_politica_endeuda( niConsServicio => NiConsec_Servic,
                                                  niArieanaId => trResultadoRiesgos(1).NIdRiesgo,
                                                  noArieendId => nArieendId );
        END IF;
      END IF;
      IF nArieendId IS NOT NULL THEN
        OPEN cRiesgoEndeudamiento(nArieendId);
        FETCH cRiesgoEndeudamiento INTO rRiesgoEndeudamiento;
        CLOSE cRiesgoEndeudamiento;
        nPorcEndGlobal := rRiesgoEndeudamiento.endeudamiento_global;
      ELSE
        IF viModelo = 'NOMINA' THEN
         nPorcEndGlobal := Pk_Conseres.Param('GPORENDGLOBAL');
        ELSIF viModelo = 'DEBITO' THEN
          nPorcEndGlobal := Pk_Conseres.Param('GPORENDGLOBALPAGOPER');
        END IF;        
      END IF;
    END IF;
  
  ----------------------------------
  
    --Fin modificación 20090813 Requerimiento 21437
    --se determina si el convenio incluye los ahorros
    VIncluyeAhorro := Pks_Convenios.ParConvenioIncAhorros(NiConvenio);
    rastro('VFormaPago:'||VFormaPago||' VIncluyeAhorro:'||VIncluyeAhorro||' NPEndeudaMax:'||NPEndeudaMax||' nArieendId:'||nArieendId||' nPorcEndGlobal:'||nPorcEndGlobal,'CUOMAX');
    
    IF UPPER(VFormaPago) = 'NOMINA' THEN
      --(1)
    --40784 JNARANJO 20160412 Adiciono nIncluyeSegSocial para determinar si se incluye o no la seguridad social       
      IF VIncluyeAhorro = 'NO' THEN
        --(2)        
        NCuotaMaxima := ROUND((NVL(RiIngresos.Ingresos_Basicos, 0) +
                              NVL(RiIngresos.Conceptos_Fijos, 0) + (nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) ) *
                              (NVL(NPendeudaMax, 0) / 100) -
      --40784 JNARANJO 20160412 Adiciono nIncluyeSegSocial para determinar si se incluye o no la seguridad social 
                              --NVL(RiIngresos.Deduccion_Nomina, 0) +
                              --41208 JNARANJO 20170309 Cambio -(nIncluyeSegSocial) por +ABS(nIncluyeSegSocial)
                              --NVL(RiIngresos.Deduccion_Nomina, 0)-(nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) +
                              NVL(RiIngresos.Deduccion_Nomina, 0)+ABS((nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial)) +                              
                              NVL(NiCuotaAhorro, 0));
      ELSE
        NCuotaMaxima := ROUND((NVL(RiIngresos.Ingresos_Basicos, 0) +
                              NVL(RiIngresos.Conceptos_Fijos, 0) + (nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) ) *                              
                              (NVL(NPEndeudaMax, 0) / 100) -
      --40784 JNARANJO 20160412 Adiciono nIncluyeSegSocial para determinar si se incluye o no la seguridad social                               
                              --NVL(RiIngresos.Deduccion_Nomina, 0));
                              --41208 JNARANJO 20170309 Cambio -(nIncluyeSegSocial) por +ABS(nIncluyeSegSocial)
                              --NVL(RiIngresos.Deduccion_Nomina, 0)-(nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial));
                              NVL(RiIngresos.Deduccion_Nomina, 0)+ABS((nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial)));                              
      END IF; --(2)
    ELSE
    --Modifica Jorge Naranjo 20090723 Requerimiento 21437
      --NCuotaMaxima := ROUND((NVL(RiIngresos.Ingresos_Basicos, 0) +
      --                      NVL(RiIngresos.Conceptos_Fijos, 0) +
      --                      NVL(RiIngresos.Otro_Ingresos, 0) +
      --                      NVL(RiIngresos.Ingresos_Especiales, 0)) *
      --                      (NVL(NPendeudaMax, 0) / 100) -
      --                      ( NVL(RiIngresos.Deduccion_Nomina, 0) +
      --                        NVL(RiIngresos.Deduccion_Personal, 0)) +
      --                        NVL(NiCuotaAhorro, 0));

      --Modifica Jorge Naranjo 20100219 Requerimiento 23107 Agrego condición NVL(rAperson.Modelo,'NULO') = 'NOMINA'
      --Modifica Jorge Naranjo 20100527 Requerimiento 23384
      IF NVL(rAperson.Modelo,'NULO') = 'NOMINA' THEN
        /*NCuotaMaxima := ROUND((NVL(RiIngresos.Ingresos_Basicos, 0) +
                              NVL(RiIngresos.Conceptos_Fijos, 0)) *
                              (NVL(NPEndeudaMax, 0) / 100) -
                              NVL(RiIngresos.Deduccion_Nomina, 0));*/
        NCuotaMaxima := ROUND((NVL(RiIngresos.Ingresos_Basicos, 0) +
                              NVL(RiIngresos.Conceptos_Fijos, 0)) *
                              (NVL(NPEndeudaMax, 0) / 100) -
                              (NVL(RiIngresos.Deduccion_Nomina, 0)) + NVL(NiCuotaAhorro, 0));
      --Fin modificación 20100527
      --Fin modificación 20100219
      ELSE
        --Modifica Jorge Naranjo 20100526 Requerimiento 23314 Dejo en comentarios el de abajo y los quito al siguiente
        NCuotaMaxima := ROUND( ( NVL(RiIngresos.Ingresos_Basicos, 0) +
                                 NVL(RiIngresos.Conceptos_Fijos, 0) +
                                 NVL(RiIngresos.Otro_Ingresos, 0) +
                                 NVL(RiIngresos.Ingresos_Especiales, 0) ) *
                               ( NVL(NPendeudaMax, 0) / 100 ) -
                               ( NVL(RiIngresos.Deduccion_Nomina, 0) +
                                 NVL(RiIngresos.otras_deducciones, 0) +
                                 NVL(RiIngresos.gastos, 0) +
                                 NVL(RiIngresos.Deduccion_Personal, 0) ) +
                                NVL(NiCuotaAhorro, 0));
        /*NCuotaMaxima := ROUND( ( NVL(RiIngresos.Ingresos_Basicos, 0) +
                                 NVL(RiIngresos.Conceptos_Fijos, 0) +
                                 NVL(RiIngresos.Otro_Ingresos, 0) +
                                 NVL(RiIngresos.Ingresos_Especiales, 0) ) *
                               ( NVL(NPendeudaMax, 0) / 100 ) -
                               ( NVL(RiIngresos.Deduccion_Nomina, 0) +
                                 NVL(RiIngresos.otras_deducciones, 0) +
                                 NVL(RiIngresos.gastos, 0) +
                                 NVL(RiIngresos.Deduccion_Personal, 0) ));*/
      END IF;
      --Fin modificación 20090723 Requerimiento 21437
    END IF; --(1)

    --Modifica Jorge Naranjo 20090909 Requerimiento 21437
    --Nivel 2:
    --Modifica Jorge Naranjo 20100527 Requerimiento 23384
    --IF viModelo = 'NOMINA' THEN
    --26398 2011/08/11 JNARANJO 
    /*IF viModelo = 'NOMINA' OR VFormaPago = 'NOMINA' THEN
    --Fin modificación 20100527
     nPorcEndGlobal := Pk_Conseres.Param('GPORENDGLOBAL');
    ELSIF viModelo = 'DEBITO' THEN
      nPorcEndGlobal := Pk_Conseres.Param('GPORENDGLOBALPAGOPER');
    END IF;*/  
    --nPorcEndGlobal := PorcentajeEndeudamiento( NiConsec_Servic,
     --                                          viModelo );
    --26398 2011/08/11 Fin modificación
    --Modifica Jorge Naranjo 20100526 Requerimiento 23314
    /*IF viModelo = 'DEBITO' AND UPPER(VFormaPago) = 'DEBITO' THEN
      nCuotaAhorro := 0;
    END IF;*/
    --Fin modificación 20100526
    nCuotaMaxGlobal := ROUND( ( NVL(RiIngresos.Ingresos_Basicos, 0) +
                               NVL(RiIngresos.Conceptos_Fijos, 0) +
                               NVL(RiIngresos.Otro_Ingresos, 0) +
                               NVL(RiIngresos.Ingresos_Especiales, 0) +
                               --41208 JNARANJO 20170309
                               (nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) ) *
                               --41208 JNARANJO 20170309 Fin modificación
                             ( NVL(nPorcEndGlobal, 0) / 100 ) -                            
                             --( NVL(RiIngresos.Deduccion_Nomina, 0) +
                             ( NVL(RiIngresos.Deduccion_Nomina, 0) +
                               NVL(RiIngresos.otras_deducciones, 0) +
                               NVL(RiIngresos.gastos, 0) +
                               NVL(RiIngresos.Deduccion_Personal, 0) ) +
                               --41208 JNARANJO 20170309
                               ABS((nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial)) +
                               --41208 JNARANJO 20170309 Fin modificación
                              NVL(NiCuotaAhorro, 0));/*--Modifica Jorge Naranjo 20100526 Requerimiento 23314
                              NVL(nCuotaAhorro, 0));--Reemplazo NiCuotaAhorro por nCuotaAhorro 20100526 */
    rastro('VFormaPago:'||VFormaPago||' nPorcEndGlobal:'||nPorcEndGlobal||' viModelo:'||viModelo||' nCuotaMaxGlobal:'||NVL(nCuotaMaxGlobal,0)||' NCuotaMaxima:'||NVL(NCuotaMaxima,0),'CUOMAX');
    --26470 2012/04/16 JNARANJO
    OPEN cCreditosReestrRefina;
    FETCH cCreditosReestrRefina INTO rCreditosReestrRefina;
    CLOSE cCreditosReestrRefina; 
    NCuotaMaxima := NCuotaMaxima + NVL(rCreditosReestrRefina.Cuota_Periodo_Pago,0);  
    nCuotaMaxGlobal := nCuotaMaxGlobal + NVL(rCreditosReestrRefina.Cuota_Periodo_Pago,0);  
    rastro('Cuota_Periodo_Pago '||NVL(rCreditosReestrRefina.Cuota_Periodo_Pago,0)||'  nCuotaMaxGlobal '||nCuotaMaxGlobal,'CUOMAX');
    --26470 2012/04/16 Fin modificación
    IF NVL(nCuotaMaxGlobal,0) < NVL(NCuotaMaxima,0) THEN
      NCuotaMaxima := NVL(nCuotaMaxGlobal,0);
    END IF;
    --Fin modificación 20090909 Requerimiento 21437
    --Modifica Jorge Naranjo 20090909 Requerimiento 21437
    IF NCuotaMaxima < 0 THEN
      NCuotaMaxima := 0;
    END IF;
    --Fin modificación 20090909 Requerimiento 21437
    RETURN(ROUND(NCuotaMaxima));
  END CuotaMaxima;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- calcula el valor de la cuota segun el periodo de pago de la persona
  -- con base a una cuota mensual, segun formula 5 del cpl0031

  FUNCTION CuotaPerPeriodoPagoMensual(ViTipoPer IN TS_ADM_TIPO_PERIODOS.Nombre%TYPE,
                                      NiCuota   IN NUMBER) RETURN NUMBER IS

    NPer NUMBER;
  BEGIN
    NPer := Pks_Generales.Paramperiodo(ViTipoPer, 'periodos_mensuales');
    IF NPer IS NOT NULL THEN
      RETURN(ROUND((NiCuota / NPer)));
    ELSE
      RETURN(0);
    END IF;
  END CuotaPerPeriodoPagoMensual;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 30/03/2004 10:02:00 a.m.
  -- Retorna la cuenta de ahorros desde donde se débita
  -- teniendo el consecutivo del servicio

  FUNCTION DebitoServicio(NiConsecutivo_Servicio IN TS_ASE_SERVICIOS.CONSECUTIVO_SERVICIO%TYPE -- Consecutivo del servicio
                          ) RETURN VARCHAR2 IS

    CURSOR CDebito IS
      SELECT Aservic.Numero_Servicio
        FROM TS_ASE_DEBITO_AUTOMATICOS Adebaut, TS_ASE_SERVICIOS Aservic
       WHERE Adebaut.Aservic_Cons_Debitado_Desde =
             Aservic.Consecutivo_Servicio AND
             Adebaut.Aservic_Consecutivo_Servicio = NiConsecutivo_Servicio;

    NCuenta TS_ASE_SERVICIOS.NUMERO_SERVICIO%TYPE;

  BEGIN
    OPEN CDebito;
    FETCH CDebito
      INTO NCuenta;
    CLOSE CDebito;
    IF NCuenta IS NOT NULL THEN
      RETURN NCuenta;
    ELSE
      RETURN NULL;
    END IF;
  END DebitoServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 29/03/2004 01:00:00 p.m.
  -- Retorna la dirección completa de envío de correspondencia del servicio
  -- teniendo los datos de la dirección previos.

  FUNCTION DireccionServicio(ViTipo_Via                IN TS_PER_DIRECCIONES.TIPO_VIA%TYPE, -- Tipo vía
                             ViNumero_Via              IN TS_PER_DIRECCIONES.NUMERO_VIA%TYPE, -- Número de vía
                             ViLetra_Via               IN TS_PER_DIRECCIONES.LETRA_VIA%TYPE, -- Letra de la vía
                             ViNombre_Via              IN TS_PER_DIRECCIONES.NOMBRE_VIA%TYPE, -- Nombre de la vía
                             ViPosicion_Via            IN TS_PER_DIRECCIONES.POSICION_VIA%TYPE, -- Posición de la vía
                             ViNumero_Interseccion     IN TS_PER_DIRECCIONES.NUMERO_INTERSECCION%TYPE, -- Número de intersección
                             ViLetra_Interseccion      IN TS_PER_DIRECCIONES.LETRA_INTERSECCION%TYPE, -- Letra de intersección
                             ViPosicion_Interseccion   IN TS_PER_DIRECCIONES.POSICION_INTERSECCION%TYPE, -- Posición de intersección
                             ViComponente_Interseccion IN TS_PER_DIRECCIONES.COMPONENTE_INTERSECCION%TYPE, -- Componente de intersección
                             ViLugar_Ubicacion         IN TS_PER_DIRECCIONES.LUGAR_UBICACION%TYPE, -- Lugar de ubicación
                             ViNumero_Lugar            IN TS_PER_DIRECCIONES.NUMERO_LUGAR%TYPE, -- Número de lugar
                             ViTipo_Ubicacion          IN TS_PER_DIRECCIONES.TIPO_UBICACION%TYPE, -- Tipo de ubicación
                             ViNumero_Ubicacion        IN TS_PER_DIRECCIONES.NUMERO_UBICACION%TYPE -- Número de ubicación
                             ) RETURN VARCHAR2 IS

    VDirCompleta VARCHAR2(200);
    VCaracter1   VARCHAR2(3);
    VCaracter2   VARCHAR2(3);
    VCaracter3   VARCHAR2(3);
    VCaracter4   VARCHAR2(3);
  BEGIN
    IF ViNUMERO_INTERSECCION IS NULL THEN
      VCaracter1 := NULL;
    ELSE
      VCaracter1 := ' # ';
    END IF;
    IF ViNUMERO_LUGAR IS NULL THEN
      VCaracter2 := NULL;
    ELSE
      VCaracter2 := ' # ';
    END IF;
    IF ViNUMERO_UBICACION IS NULL THEN
      VCaracter3 := NULL;
    ELSE
      VCaracter3 := ' # ';
    END IF;
    IF ViCOMPONENTE_INTERSECCION IS NULL THEN
      VCaracter4 := NULL;
    ELSE
      VCaracter4 := ' - ';
    END IF;
    VDirCompleta := LTRIM(RTRIM(Vitipo_Via || ' ' || Vinumero_Via || '  ' ||
                                Viletra_Via || ' ' || Vinombre_Via || ' ' ||
                                Viposicion_Via || Vcaracter1 ||
                                Vinumero_Interseccion || ' ' ||
                                Viletra_Interseccion || ' ' ||
                                Viposicion_Interseccion || Vcaracter4 ||
                                Vicomponente_Interseccion || ' ' ||
                                Vilugar_Ubicacion || Vcaracter2 ||
                                Vinumero_Lugar || ' ' || Vitipo_Ubicacion ||
                                Vcaracter3 || Vinumero_Ubicacion));
    RETURN(VDirCompleta);
  END DireccionServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Si cumple la edad requerida para acceder a creditos y SI esta entre la edad maxima
  -- y minima permitida por la linea
  --Modifica Jorge Naranjo 20061127 Se agrega parámetro viAplicacion debido a que se debe controlar si es
  --de ACCESO o DURANT
  FUNCTION EdadAccedeServicios( NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE,
                                viAplicacion IN VARCHAR2 DEFAULT 'ACCESO' )
    RETURN VARCHAR2 IS

    NEdadMinima    VS_SER_EDADES.Edad_Minima%TYPE;
    NEdadMaxima    VS_SER_EDADES.Edad_Maxima%TYPE;
    NCumpleEdadReq NUMBER;
    VoResaccedeCre VARCHAR2(2);
    VSerCodigo     TS_ASE_SERVICIOS.Ser_Codigo%TYPE;
    NPer_Id        TS_ASE_SERVICIOS.Per_Id%TYPE;

    CURSOR CServicio IS
      SELECT Aservic.Per_Id, Aservic.Ser_Codigo
        FROM TS_ASE_SERVICIOS Aservic, TS_PERSONAS Per
       WHERE Per.Per_Type = 'PNATUR' AND Aservic.Per_Id = Per.Id AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;
    --Modifica Jorge Naranjo 20061127 Para traer las edades permitidas dependiendo del
    --tipo de APLICACION 'ACCESO' o 'DURANT'
    CURSOR cSerEdad ( viSercodigo IN VARCHAR2 ) IS
      SELECT vseredad.edad_minima,vseredad.edad_maxima
      FROM VS_SER_EDADES vseredad
      WHERE vseredad.aplicacion = viAplicacion
        AND vseredad.ser_codigo = ( SELECT MAX(vseredad2.Ser_Codigo)
                                    FROM VS_SER_EDADES vseredad2
                                    WHERE viSercodigo LIKE vseredad2.Ser_Codigo || '%');
  BEGIN
    OPEN CServicio;
    FETCH CServicio
      INTO NPer_Id, VSerCodigo;
    CLOSE CServicio;
    NCumpleEdadReq := F114(NPer_Id);
    OPEN cSerEdad ( VSerCodigo );
    FETCH cSerEdad INTO NEdadMinima,NEdadMaxima;
    CLOSE cSerEdad;

  rastro('viAplicacion:'||viAplicacion||' VSerCodigo:'||VSerCodigo,'EDAD');
    --
    -- Fecha modificación : 16/09/04 08:05:45 a.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Quitado por sugerencia de german y marylu
    --
    --     IF nCumpleEdadReq <> 0 THEN
/*Modifica Jorge Naranjo 20061127 No tiene en cuenta el momento de "APLICACION"
    NEdadMinima := Pks_Servicios.Parametro(VSerCodigo,
                                           'VS_SER_EDADES',
                                           'EDAD_MINIMA',
                                           SYSDATE);
    NEdadMaxima := Pks_Servicios.Parametro(VSerCodigo,
                                           'VS_SER_EDADES',
                                           'EDAD_MAXIMA',
                                           SYSDATE);
*/
    IF NEdadMinima IS NOT NULL AND NEdadMaxima IS NOT NULL THEN
      IF NCumpleEdadReq BETWEEN NEdadMinima AND NEdadMaxima THEN
        VoResaccedeCre := 'NO';
      ELSE
        -- se debe reportar
        VoResaccedeCre := 'SI';
      END IF;
    ELSE
      VoResaccedeCre := 'NO';
    END IF;
    --     ELSE voResaccedeCre := 'NO';
    --     END IF;
    RETURN(VoResaccedeCre);
  END EdadAccedeServicios;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 30/03/2004 07:00:00 a.m.
  -- Retorna el empleador del cliente asociado a un servicio
  -- teniendo el id de la ocupación.

  FUNCTION EmpleadorServicio(NiId IN TS_PER_PERSONA_OCUPACIONES.ID%TYPE -- Id de la ocupación
                             ) RETURN VARCHAR2 IS

    CURSOR CEmpleador IS
      SELECT Per.Nombre_Completo
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu, TS_PERSONAS Per
       WHERE Pperocu.Per_Id_Vinculado_a = Per.Id AND Pperocu.Id = NiId;

    CURSOR CActividad IS
      SELECT Aacteco.Nombre
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu, TS_ADM_CIIUS Aciius,
             TS_ADM_ACTIVIDAD_ECONOMICAS Aacteco
       WHERE Pperocu.Id = NiId AND Aciius.Ciiu = Pperocu.Aciiu_Ciiu AND
             Aacteco.Id = Aciius.Aacteco_Id;
    --29153 JNARANJO 20121124
    CURSOR cOcupacion IS
      SELECT atipocu.nombre
      FROM ts_per_persona_ocupaciones pperocu,ts_adm_tipo_ocupaciones atipocu
      WHERE pperocu.id = NiId
        AND pperocu.atipocu_codigo = atipocu.codigo
        AND pperocu.atipocu_codigo = '5';
    --29153 JNARANJO 20121124

    VEmpleador VARCHAR2(2000);

  BEGIN
    OPEN CEmpleador;
    FETCH CEmpleador
      INTO VEmpleador;
    CLOSE CEmpleador;
    IF VEmpleador IS NOT NULL THEN
      RETURN VEmpleador;
    ELSE
      OPEN CActividad;
      FETCH CActividad
        INTO VEmpleador;
      CLOSE CActividad;
      IF VEmpleador IS NOT NULL THEN
        RETURN VEmpleador;
      ELSE
        --29153 JNARANJO 20121124
        --RETURN NULL;
        OPEN cOcupacion;
        FETCH cOcupacion INTO VEmpleador;
        CLOSE cOcupacion;
        IF VEmpleador IS NOT NULL THEN
          RETURN VEmpleador;
        ELSE
          RETURN NULL;
        END IF;
        --29153 JNARANJO 20121124        
      END IF;
    END IF;
  END EmpleadorServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 01:02:00 p.m.
  -- Retorna el nombre del estado de control
  -- teniendo el codigo

  FUNCTION EstadoControl(ViCodigo IN TS_ADM_ESTADO_CONTROLES.CODIGO%TYPE -- Codigo del estado
                         ) RETURN VARCHAR2 IS

    CURSOR CEstados IS
      SELECT Aestcon.Descripcion
        FROM TS_ADM_ESTADO_CONTROLES Aestcon
       WHERE Aestcon.Codigo = ViCodigo;

    VEstados TS_ADM_ESTADO_CONTROLES.DESCRIPCION%TYPE;

  BEGIN
    OPEN CEstados;
    FETCH CEstados
      INTO VEstados;
    CLOSE CEstados;
    IF VEstados IS NOT NULL THEN
      RETURN VEstados;
    ELSE
      RETURN NULL;
    END IF;
  END EstadoControl;

  FUNCTION ExcepcionConvenio(NiConsec IN TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_ExcepcionConvenio

      PROPOSITO:
      Determinar si el servicio no es admitido para el convenio

      PARAMETROS:
      niConsecutivoServicio IN ts_ase_servicios.consecutivo_servicio%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      31/05/04            SIC      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      tener un convenio

      NOTAS:
      Numeral 2b del Cpl0026

    ****************************************************************************************************/
    NCont NUMBER := 0; -- Utilizada para determinar el número de registros retornados

    --
    -- Fecha: 31/05/04 01:18:31 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si el servicio no es admitido para el convenio
    --
    CURSOR CSerconvenio IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic, TS_PER_PERSONA_OCUPACIONES Pperocu,
             TS_CNV_EXCEPCION_SERVICIOS Cexcser
       WHERE Aservic.Ser_Codigo = Cexcser.Ser_Codigo AND
             Cexcser.Con_Id = Pperocu.Con_Deduccion_Nomina AND
             Aservic.Pperocu_Id = Pperocu.Id AND
             Aservic.Consecutivo_Servicio = NiConsec;

  BEGIN
    OPEN CSerconvenio;
    FETCH CSerconvenio
      INTO NCont;
    CLOSE CSerconvenio;
    IF NVL(NCont, 0) > 0 THEN
      RETURN('SI');
    ELSE
      RETURN('NO');
    END IF;
  END ExcepcionConvenio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 12:00:00 p.m.
  -- Formula 114  ?Edad en a?os?  Entero(((fecha actual) - fecha de nacimiento)/365)

  FUNCTION F114(NiIdPer IN TS_PERSONAS.Id%TYPE) RETURN NUMBER IS

    CURSOR CPer IS
      SELECT p.Fecha_Nacimiento
        FROM TS_PERSONAS p
       WHERE p.Id = NiIdPer;

    DFechaNac DATE;
    Nedad     NUMBER;

  BEGIN
    OPEN CPer;
    FETCH CPer
      INTO DFEchaNac;
    IF NOT CPer%NOTFOUND THEN
      Nedad := FLOOR((SYSDATE - NVL(DFechaNac, SYSDATE)) / 365.25);
    ELSE
      Nedad := 0;
    END IF;
    CLOSE CPer;
    RETURN(NVL(Nedad, 0));
  END F114;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 12:00:00 p.m.
  -- calcula el valor de la cuota segun el periodo de pago de la persona
  -- Se puede pasar como parametro el consecutivo del servicio y
  -- Se toma el valor de la cuota y el tipo de peroiodo de pago y liquidado,
  -- que se hallan grabado en el servicio, si no se ingresa el servicio
  -- se toma la cuota y el periodo de pago ingresado como parametros

  FUNCTION F118(NiNroSer IN NUMBER, --Consecutivo del servicio
                NiCuota  IN NUMBER, --Valor de la cuota
                ViTipoPP IN VARCHAR2 --Tipo periodo de pago
                ) RETURN NUMBER IS

    CURSOR CCredito IS
      SELECT Ase.Atipper_Liquidacion, Ase.Cuota_Periodo_Pago
        FROM TS_ASE_SERVICIOS Ase
       WHERE Ase.Consecutivo_Servicio = NiNroSer;

    VTipoPer TS_ADM_TIPO_PERIODOS.Nombre%TYPE;
    NCuota   NUMBER;
    NPer     NUMBER;
    NValor   NUMBER;
    /* JGLEN 13-02-2006 El paquete pks_formulas guarda los valores de todos los servicios en la misma posición
    así que normalmente esta función devuelve falso. No se considera útil y por tanto se elimina */

    -- VValor   VARCHAR2(30);

  BEGIN
    IF NiNroSer IS NOT NULL THEN
      /* JGLEN 13-02-2006 El paquete pks_formulas guarda los valores de todos los servicios en la misma posición
      así que normalmente esta función devuelve falso. No se considera útil y por tanto se elimina */
      /*IF NOT Pks_Formulas.Esta_Calculada(NiNroSer, 'F118', VValor) THEN*/
      OPEN CCredito;
      FETCH CCredito
        INTO VTipoPer, NCuota;
      IF NOT CCredito%NOTFOUND THEN
        NPer := TO_NUMBER(Pks_Generales.ParamPeriodo(VTipoPer,
                                                     'periodos_liquidacion_mensual'));
        IF NPer IS NOT NULL THEN
          NValor := ROUND((NCuota / NPer));
        ELSE
          NValor := 0;
        END IF;
      END IF;
      --se crea el valor de la variable al vector
      /*Pks_Formulas.Guardar_Valor(NiNroSer, 'F118', 'NU', TO_CHAR(NValor));*/
      CLOSE CCredito;
      /* JGLEN 13-02-2006 El paquete pks_formulas guarda los valores de todos los servicios en la misma posición
      así que normalmente esta función devuelve falso. No se considera útil y por tanto se elimina */

      /*ELSE
        NValor := TO_NUMBER(VValor);
      END IF;*/
    ELSE
      NPer   := TO_NUMBER(Pks_Generales.ParamPeriodo(ViTipoPP,
                                                     'periodos_liquidacion_mensual'));
      NValor := ROUND((NiCuota / NPer));
    END IF;
    RETURN(NVL(NValor, 0));
  END F118;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 08:02:00 a.m.
  -- Verifica si la persona firmo centrales de riesgo

  FUNCTION FirmaCentralesRiesgo(NiPer_Id IN TS_PERSONAS.Id%TYPE)
    RETURN VARCHAR2 IS
    CURSOR CPersonas IS
      SELECT Per.Firma_Centrales_Riesgo
        FROM TS_PERSONAS Per
       WHERE Per.Id = NiPer_Id;

    VFirma_Centrales TS_PERSONAS.Firma_Centrales_Riesgo%TYPE;

  BEGIN
    OPEN CPersonas;
    FETCH CPersonas
      INTO VFirma_Centrales;
    CLOSE CPersonas;
    IF VFirma_Centrales IS NULL THEN
      RETURN 'NO';
    ELSE
      RETURN VFirma_Centrales;
    END IF;
  END FirmaCentralesRiesgo;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Retorna si cumple con el ingreso minimo
  -- teniendo el codigo del servicio

  FUNCTION IngresosBasicosSolicitante(NiSecServicio IN NUMBER --id del Servicio solicitado
                                      ) RETURN VARCHAR2 IS

    --
    -- Fecha: 26-10-2004 05:19:22 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar los ingresos a comparar
    --
    CURSOR CIngresos(NiConsec_Servic IN NUMBER) IS
      SELECT ROUND((NVL(Aperson.Ingresos_Basicos, 0) / Peringr.Numero_Dias) *
                    Pernuev.Numero_Dias) Ingresos
        FROM TS_ASE_SERVICIOS Aservic, TS_ASE_PERSONAS Aperson,
             TS_ADM_TIPO_PERIODOS Peringr, TS_ADM_TIPO_PERIODOS Pernuev
       WHERE Aperson.Fecha_Final IS NULL AND
             Aperson.Per_Id = Aservic.Per_Id AND
             Aservic.Consecutivo_Servicio =
             Aperson.Aservic_Consecutivo_Servicio AND
             Aservic.Atipper_Pago = Peringr.Nombre AND
             Pernuev.Nombre = 'MENSUAL' AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

    NIngresosMiniLinea NUMBER;
    NSalBasico         NUMBER;
    VSer               VARCHAR2(20);
    VFormaPago         VARCHAR2(10);
    VResp              VARCHAR2(2) := 'NO';
  BEGIN
    VSer               := ParamAseSer(NiSecServicio, 'Ser_codigo');
    VFormaPago         := ParamAseSer(NiSecServicio, 'aforpag_nombre');
    NIngresosMiniLinea := NVL(SerIngresoMinimo(VSer, VFormaPago), 0);
    OPEN CIngresos(NiSecServicio);
    FETCH CIngresos
      INTO NSalBasico;
    CLOSE CIngresos;
    --      nSalBasico:= NVL(to_number(ParGenAsePersonas(niSecServicio,'ingresos_basicos')),0);
    IF SUBSTR(VSer, 1, 6) IN ('001.03', '001.01') THEN
      IF SUBSTR(VSer, 1, 6) = '001.03' AND --- Servicio de Credito
         NSalbasico <= NIngresosMiniLinea THEN
        IF NSalBasico <= Pk_Conseres.Param('GSMLMV_DEUDORES') THEN
          VResp := 'NO';
        ELSE
          VResp := 'SI';
        END IF;
      END IF;
      IF SUBSTR(VSer, 1, 6) = '001.01' AND --Servicio de afiliacion
         NSalbasico <= NIngresosMiniLinea THEN
        VResp := 'NO';
      ELSE
        VResp := 'SI';
      END IF;
    ELSE
      VResp := 'NO';
    END IF;
    RETURN(VResp);
  END IngresosBasicosSolicitante;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Retorna si cumple con el ingreso minimo
  -- teniendo el codigo del servicio

  FUNCTION IngresosBasicosAfiliacion(NiSecServicio IN NUMBER --id del Servicio solicitado
                                     ) RETURN VARCHAR2 IS

    NIngresosMiniLinea NUMBER;
    NSalBasico         NUMBER;
    VSer               VARCHAR2(20);
    VFormaPago         VARCHAR2(10);
    VResp              VARCHAR2(50) := 'NO';

    --
    -- Fecha: 26-10-2004 05:19:22 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar los ingresos a comparar
    --
    CURSOR CIngresos(NiConsec_Servic IN NUMBER) IS
      SELECT ROUND((NVL(Aperson.Ingresos_Basicos, 0) / Peringr.Numero_Dias) *
                    Pernuev.Numero_Dias) Ingresos
        FROM TS_ASE_SERVICIOS Aservic, TS_ASE_PERSONAS Aperson,
             TS_ADM_TIPO_PERIODOS Peringr, TS_ADM_TIPO_PERIODOS Pernuev
       WHERE Aperson.Fecha_Final IS NULL AND
             Aperson.Per_Id = Aservic.Per_Id AND
             Aservic.Consecutivo_Servicio =
             Aperson.Aservic_Consecutivo_Servicio AND
             Aservic.Atipper_Pago = Peringr.Nombre AND
             Pernuev.Nombre = 'MENSUAL' AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

  BEGIN
    VSer               := ParamAseSer(NiSecServicio, 'Ser_codigo');
    VFormaPago         := ParamAseSer(NiSecServicio, 'aforpag_nombre');
    NIngresosMiniLinea := NVL(SerIngresoMinimo(VSer, VFormaPago), 0);
    OPEN CIngresos(NiSecServicio);
    FETCH CIngresos
      INTO NSalBasico;
    CLOSE CIngresos;
    --      nSalBasico:= NVL(to_number(ParGenAsePersonas(niSecServicio,'ingresos_basicos')),0);
    IF SUBSTR(VSer, 1, 6) IN ('001.01', '001.03', '001.04') THEN
      IF SUBSTR(VSer, 1, 6) IN ('001.01', '001.03', '001.04') AND --Servicio de afiliacion
         NSalbasico < NIngresosMiniLinea THEN
        VResp := '(' || TO_CHAR(NIngresosMiniLinea) || ')';
      ELSE
        VResp := 'NO';
      END IF;
    ELSE
      VResp := 'NO';
    END IF;
    RETURN(VResp);
  END IngresosBasicosAfiliacion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Retorna si cumple con el ingreso minimo
  -- teniendo el codigo del servicio

  FUNCTION IngresosBasicos(NiConsecAsesor IN NUMBER) RETURN VARCHAR2 IS

    NIngresosMiniLinea NUMBER;
    NSalBasico         NUMBER;
    VSer               VARCHAR2(20);
    VFormaPago         VARCHAR2(10);
    VResp              VARCHAR2(50) := 'NO';

    --
    -- Fecha: 25/08/04 03:05:59 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar los servicios de la asesoria
    --
    CURSOR CServicios IS
      SELECT Aservic.Consecutivo_Servicio, Aservic.Ser_Codigo,
             Aservic.Aforpag_Nombre, Aservic.Per_Id, Ser.Nombre
        FROM TS_ASE_SERVICIOS Aservic, TS_SERVICIOS Ser
       WHERE Aservic.Ser_Codigo = Ser.Codigo AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsecAsesor;

    --
    -- Fecha: 26-10-2004 05:19:22 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar los ingresos a comparar
    --
    CURSOR CIngresos(NiConsec_Servic IN NUMBER) IS
      SELECT ROUND((NVL(Aperson.Ingresos_Basicos, 0) / Peringr.Numero_Dias) *
                    Pernuev.Numero_Dias) Ingresos
        FROM TS_ASE_SERVICIOS Aservic, TS_ASE_PERSONAS Aperson,
             TS_ADM_TIPO_PERIODOS Peringr, TS_ADM_TIPO_PERIODOS Pernuev
       WHERE Aperson.Fecha_Final IS NULL AND
             Aperson.Per_Id = Aservic.Per_Id AND
             Aservic.Consecutivo_Servicio =
             Aperson.Aservic_Consecutivo_Servicio AND
             Aservic.Atipper_Pago = Peringr.Nombre AND
             Pernuev.Nombre = 'MENSUAL' AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

  BEGIN
    VResp := 'NO';
    FOR CSer IN CServicios LOOP
      VSer               := CSer.Ser_Codigo;
      VFormaPago         := CSer.Aforpag_Nombre;
      NIngresosMiniLinea := NVL(SerIngresoMinimo(VSer, VFormaPago), 0);
      OPEN CIngresos(CSer.Consecutivo_Servicio);
      FETCH CIngresos
        INTO NSalBasico;
      CLOSE CIngresos;
      --      nSalBasico:= NVL(to_number(ParGenAsePersonas(cSer.consecutivo_servicio,'ingresos_basicos')),0);
      IF SUBSTR(VSer, 1, 6) IN ('001.01', '001.03', '001.04') THEN
        IF SUBSTR(VSer, 1, 6) IN ('001.01', '001.03', '001.04') AND
           NVL(NSalbasico, 0) < NVL(NIngresosMiniLinea, 0) THEN
          IF VResp = 'NO' THEN
            VResp := ' ' || CSer.Nombre || '->' ||
                     TO_CHAR(NIngresosMiniLinea) || ' ';
          ELSE
            VResp := VResp || CSer.Nombre || '->' ||
                     TO_CHAR(NIngresosMiniLinea) || ' ';
          END IF;
        END IF;
      END IF;
    END LOOP;
    RETURN(VResp);
  END IngresosBasicos;

  FUNCTION Lineas_Ahorro_Simultaneas(NiConsecServic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_Lineas_ahorro_simultaneas

      PROPOSITO:
      Permite verificar si el solicitante posee el número de cuentas de ahorro permitidas

      PARAMETROS:
      niConsecServic ts_ase_servicios.consecutivo_servicio%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      18/06/04            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      Cuenta de ahorro

      NOTAS:
      En la forma de ahorros se evalua esta condición para el autorizado

    ****************************************************************************************************/
    Result      VARCHAR2(2);
    NPer_Id     NUMBER(10, 0) := 0; -- Utilizada para determinar el id de la persona
    VSer_Codigo VARCHAR2(30) := ''; -- Utilizada para determina el servicio

    --
    -- Fecha: 18/06/04 02:59:19 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar la información del servicio a ser evaluado
    --
    CURSOR CServicio IS
      SELECT Per_Id, Ser_Codigo
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Consecutivo_Servicio = NiConsecServic;

  BEGIN
    OPEN CServicio;
    FETCH CServicio
      INTO NPer_Id, VSer_Codigo;
    CLOSE CServicio;
    IF SUBSTR(VSer_Codigo, 1, 6) = '001.02' THEN
      Result := Pks_Ahorros.Permite_Lineas_Simultaneas(NPer_Id, VSer_Codigo);
      IF Result = 'NO' THEN
        Result := 'SI';
      ELSE
        Result := 'NO';
      END IF;
    ELSE
      Result := 'NO';
    END IF;

    RETURN(Result);
  END Lineas_Ahorro_Simultaneas;

  FUNCTION LibranzaPendiente(NiPerId TS_PERSONAS.Id%TYPE) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_LibranzaPendiente

      PROPOSITO:
      Verifica si el cliente tiene una libranza pendiente por actualizar

      PARAMETROS:
      niPerId ts_personas.id%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      31/05/04            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      El id de la persona

      NOTAS:
      Numeral 13 del Cpl0105

    ****************************************************************************************************/
    Result VARCHAR2(2);

    --
    -- Fecha: 31/05/04 12:01:55 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si el cliente posee libranzas pendientes por actualizar
    --
    --
    -- Fecha: 31/05/04 12:01:55 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si el cliente posee libranzas pendientes por actualizar
    --
    CURSOR CDocumentos IS
      SELECT Adocume.Recibido_Asesor
        FROM TS_ASE_DOCUMENTOS Adocume, TS_DOCUMENTOS Doc,
             TS_PER_PERSONA_OCUPACIONES Pperocu
       WHERE Doc.Codigo = 9 AND Doc.Doc_Type = 'DENTRE' AND
             Adocume.Doc_Id = Doc.Id AND Adocume.Pperocu_Id = Pperocu.Id AND
             Pperocu.Con_Deduccion_Nomina IS NOT NULL AND
             Adocume.Per_Id = NiPerId;

    --
    -- Fecha modificación : 09/09/04 09:11:30 a.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Se modifica por sugerencia de marylu debido a el mensaje de libranza pendiente se genera para un usuario que no tiene ninguna vinculación por nómina (Asesoría 412468)
    --
    /* SELECT adocume.recibido_asesor
     FROM ts_ase_documentos adocume,
          ts_documentos     doc
    WHERE doc.codigo = 9
      AND doc.doc_type = 'DENTRE'
      AND adocume.doc_id = doc.id
      AND adocume.per_id = niPerId;*/

  BEGIN
    OPEN CDocumentos;
    FETCH CDocumentos
      INTO Result;
    CLOSE CDocumentos;
    IF Result = 'NO' THEN
      RETURN('SI');
    ELSE
      RETURN('NO');
    END IF;
  END LibranzaPendiente;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Calcula la capacidad de endeudamiento con nuevas cuotas

  FUNCTION Nivel1Actual(RiIngresos     IN Pks_Proc_Solicitudes.TIngresos,
                        NCuotaAhorro   IN NUMBER,
                        VFormaPago     IN VARCHAR2,
                        VIncluyeAhorro IN VARCHAR2,
                        NiNuevasCuotas IN NUMBER) RETURN NUMBER IS

    NPorEndeudaAct NUMBER;
  --Modifica Jorge Naranjo 20090723 Requerimiento 21437
  --Nivel 1 ACTUAL
  -- Si la forma de pago para el servicio solicitado es deducción de nómina
  --   Si según el convenio no se deben incluir los ahorros para el análisis de la capacidad de endeudamiento
  --     Porcentaje endeudamiento actual = (Deducciones por nómina - cuotas actuales ahorros) / (Ingresos básicos + conceptos fijos) * 100
  --   Si no
  --     Porcentaje endeudamiento actual =  (Deducciones por nómina / (Ingresos básicos + conceptos fijos)) * 100
  -- Si no
  --  Porcentaje endeudamiento actual = % endeudamiento actual nivel 2

  --Nivel 2 NUEVAS CUOTAS
  -- Si la forma de pago es por nómina
  --   Si según el convenio no se deben incluir los ahorros para el análisis de la capacidad de endeudamiento
  --     Porcentaje endeudamiento final = ((Deducciones por nómina - cuotas actuales de ahorros + Nuevas cuotas sin incluir ahorros) / (Ingresos básicos + conceptos fijos)) * 100
  --   Si no
  --     Porcentaje endeudamiento final = ((Deducciones por nómina + Nuevas cuotas) / (Ingresos básicos + conceptos fijos)) * 100
  -- Si no
  --   Porcentaje endeudamiento final = % endeudamiento final nivel 2.
  --Modifica Jorge Naranjo 20100219 Requerimiento 23107
  CURSOR cAperson( niConsServicio IN NUMBER ) IS
    SELECT aperson.modelo,s.per_id perIdTitular--41208 JNARANJO 20170327
    FROM ts_ase_personas aperson,ts_ase_servicios s--41208 JNARANJO 20170327 Adiciono ts_ase_servicios
    WHERE aperson.aservic_consecutivo_servicio = niConsServicio
      AND s.consecutivo_servicio = aperson.aservic_consecutivo_servicio--41208 JNARANJO 20170327 condición
      AND aperson.modelo IS NOT NULL;
  rAperson cAperson%ROWTYPE;
  --Fin modificación 20100219
  vIncAhorro VARCHAR2(2) := VIncluyeAhorro;--41208 JNARANJO 20170405
  nNuevasCuotas NUMBER := NiNuevasCuotas;
  nVrCuotaAhorro NUMBER := NCuotaAhorro;
  vPago VARCHAR2(10) := VFormaPago;--41208 JNARANJO 20170327
  BEGIN
    --Modifica Jorge Naranjo 20100219 Requerimiento 23107
    OPEN cAperson(RiIngresos.Consecutivo_Servicio);
    FETCH cAperson INTO rAperson;
    CLOSE cAperson;
    --Fin modificación 20100219
    
    --41208 JNARANJO 20170327
    IF rAperson.PerIdTitular != RiIngresos.PerId THEN
      vPago := 'NOMINA';
      nIncluyeSegSocial := -1;
      vIncAhorro := 'NO';
      nNuevasCuotas := 0;
      nVrCuotaAhorro := 0;
    END IF;
    --41208 JNARANJO 20170327 Fin 
    rastro('Deduccion_Nomina: '||NVL(RiIngresos.Deduccion_Nomina,0) || CHR(13) ||
           ' NCuotaAhorro: '||NVL(nVrCuotaAhorro, 0) || CHR(13) ||
           ' NiNuevasCuotas: '||NVL(nNuevasCuotas, 0) || CHR(13) ||
           ' Ingresos_Basicos: '||NVL(RiIngresos.Ingresos_Basicos, 0) || CHR(13) ||
           ' Conceptos_Fijos: '||NVL(RiIngresos.Conceptos_Fijos, 0) || CHR(13) ||
           ' Deduccion_Personal: '||NVL(RiIngresos.Deduccion_Personal, 0) || CHR(13) ||
           ' otras_deducciones: '||NVL(RiIngresos.otras_deducciones, 0) || CHR(13) ||
           ' gastos: '||NVL(RiIngresos.gastos, 0) || CHR(13) ||
           ' Otro_Ingresos: '||NVL(RiIngresos.Otro_Ingresos, 0) || CHR(13) ||
           ' Ingresos_Especiales: '||NVL(RiIngresos.Ingresos_Especiales, 0) || CHR(13) ||
           ' VIncluyeAhorro: '||vIncAhorro || CHR(13) ||
           ' nIncluyeSegSocial: '||(nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) || CHR(13) ||    
           ' Modelo: '||NVL(rAperson.Modelo,'NULO') ||'  VFormaPago: '||VFormaPago ||'  vPago: '||vPago || CHR(13) ||
           ' perId:'||RiIngresos.PerId
           ,'NIVEL1ACT');
    
    IF UPPER(vPago) = 'NOMINA' THEN
      --(1)
      IF vIncAhorro = 'NO' THEN
        --(2)
        --40784 JNARANJO 20160412 Adiciono nIncluyeSegSocial para determinar si se incluye o no la seguridad social
        --NPorEndeudaAct := (NVL(RiIngresos.Deduccion_Nomina, 0) -        
        --41208 JNARANJO 20170307 Agrego ROUND y nRedondeoNivel
        NPorEndeudaAct := ROUND((NVL(RiIngresos.Deduccion_Nomina, 0)+(nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) -
                          NVL(nVrCuotaAhorro, 0) + NVL(nNuevasCuotas, 0)) /
                          (NVL(RiIngresos.Ingresos_Basicos, 0) + 
                          (nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial)+
                          NVL(RiIngresos.Conceptos_Fijos, 0)),nRedondeoNivel) * 100;
      ELSE
        --40784 JNARANJO 20160412 Adiciono nIncluyeSegSocial para determinar si se incluye o no la seguridad social
        --NPorEndeudaAct := (NVL(RiIngresos.Deduccion_Nomina, 0) +
        --41208 JNARANJO 20170307 Agrego ROUND y nRedondeoNivel
        NPorEndeudaAct := ROUND((NVL(RiIngresos.Deduccion_Nomina, 0)+(nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) +
                          NVL(nNuevasCuotas, 0)) /
                          (NVL(RiIngresos.Ingresos_Basicos, 0) +
                          (nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial)+
                          NVL(RiIngresos.Conceptos_Fijos, 0)),nRedondeoNivel) * 100;
      END IF; --(2)
    ELSE
      -- la forma de pagos es diferente a mensual, o no se conoce todavia
      --Modifica Jorge Naranjo 20090723 Requerimiento 21437
      /*NPorEndeudaAct := ((NVL(RiIngresos.Deduccion_Nomina, 0) -
                          NVL(NCuotaAhorro, 0) +
                          NVL(RiIngresos.Deduccion_Personal, 0) +
                          NVL(NiNuevasCuotas, 0)) /
                        (NVL(RiIngresos.Ingresos_Basicos, 0) +
                         NVL(RiIngresos.Conceptos_Fijos, 0) +
                         NVL(RiIngresos.Otro_Ingresos, 0) +
                         NVL(RiIngresos.Ingresos_Especiales, 0)) ) * 100;*/
      --Modifica Jorge Naranjo 20100219 Requerimiento 23107 Agrego condición NVL(rAperson.Modelo,'NULO') = 'NOMINA'
      IF NVL(rAperson.Modelo,'NULO') = 'NOMINA' THEN
        --Modifica Jorge Naranjo 20100527 Requerimiento 23384
        /*NPorEndeudaAct := (NVL(RiIngresos.Deduccion_Nomina, 0) + NVL(NiNuevasCuotas, 0)) /
                          (NVL(RiIngresos.Ingresos_Basicos, 0) +
                          NVL(RiIngresos.Conceptos_Fijos, 0)) * 100;*/
        --41208 JNARANJO 20170307 Agrego ROUND y nRedondeoNivel
        NPorEndeudaAct := ROUND((NVL(RiIngresos.Deduccion_Nomina, 0) + NVL(nNuevasCuotas, 0) - NVL(nVrCuotaAhorro, 0) ) /
                          (NVL(RiIngresos.Ingresos_Basicos, 0) +
                          NVL(RiIngresos.Conceptos_Fijos, 0)),nRedondeoNivel) * 100;
        --
      --Fin modificación 20100219
      ELSE
        --Modifica Jorge Naranjo 20100219 Requerimiento 23107
        --41208 JNARANJO 20170307 Agrego ROUND y nRedondeoNivel
        NPorEndeudaAct := ROUND(((NVL(RiIngresos.Deduccion_Nomina, 0) -
                            NVL(nVrCuotaAhorro, 0) +
                            NVL(RiIngresos.Deduccion_Personal, 0) +
                            NVL(RiIngresos.otras_deducciones, 0) +
                            NVL(RiIngresos.gastos, 0) +
                            NVL(nNuevasCuotas, 0)) /
                          (NVL(RiIngresos.Ingresos_Basicos, 0) +
                           NVL(RiIngresos.Conceptos_Fijos, 0) +
                           NVL(RiIngresos.Otro_Ingresos, 0) +
                           NVL(RiIngresos.Ingresos_Especiales, 0)) ),nRedondeoNivel) * 100;
      --Fin modificación 20100219

      --Fin modificación 20090723 Requerimiento 21437
      END IF;
    END IF; --(1);

    --pks_cpl0031.valor_servicios_ahorros(:ASERVIC.CONSECUTIVO_SERVICIO)
    --pks_cpl0031.incluye_cuotas_ahorros(:ASERVIC.CONSECUTIVO_SERVICIO)
    rastro('NPorEndeudaAct:'||NPorEndeudaAct||' perId:'||RiIngresos.PerId,'NIVEL1ACT');
    RETURN(NPorEndeudaAct);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END Nivel1Actual;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Calcula el porcentaje de endeudamiento actual nivel 2, con nuevas cuotas

  FUNCTION Nivel2Actual(RiIngresos   IN Pks_Proc_Solicitudes.TIngresos,
                        NCuotaAhorro IN NUMBER,
                        NNuevasCuota IN NUMBER) RETURN NUMBER IS

    NPorEndeudaAct NUMBER;
  --Modifica Jorge Naranjo 20100219 Requerimiento 23107
  CURSOR cAperson( niConsServicio IN NUMBER ) IS
    SELECT aperson.modelo,s.per_id perIdTitular--41208 JNARANJO 20170327 Adiciono perIdTitular 
    FROM ts_ase_personas aperson,ts_ase_servicios s--41208 JNARANJO 20170327 Adiciono ts_ase_servicios
    WHERE aperson.aservic_consecutivo_servicio = niConsServicio
      AND s.consecutivo_servicio = aperson.aservic_consecutivo_servicio--41208 JNARANJO 20170327 Adiciono condición
      AND aperson.modelo IS NOT NULL;
  rAperson cAperson%ROWTYPE;
  nVrNuevasCuotas NUMBER := NNuevasCuota;
  vPago VARCHAR2(10);--41208 JNARANJO 20170327
  --Fin modificación 20100219
  --Modifica Jorge Naranjo 20090723 Requerimiento 21437
  --Nivel 2 ACTUAL
  -- Porcentaje endeudamiento actual = (Deducciones por nómina - cuotas actuales ahorros  + Deducciones en Cotrafa con pago personal + Otras deducciones + Gastos) /
  --                                   (Ingresos básicos + conceptos fijos + otros ingresos + ingresos especiales) * 100
  --Nivel 2 NUEVAS CUOTAS
  -- Porcentaje endeudamiento final = ((Deducciones por nómina - cuotas actuales de ahorros para el convenio incluidas en el nivel 1 +
  --                                    Deducciones en Cotrafa con pago personal + Otras deducciones + Gastos + Nuevas cuotas sin incluir ahorros) /
  --                                  (Ingresos básicos + conceptos fijos + otros ingresos + ingresos especiales)) * 100
  BEGIN
    --Modifica Jorge Naranjo 20100219 Requerimiento 23107
    OPEN cAperson(RiIngresos.Consecutivo_Servicio);
    FETCH cAperson INTO rAperson;
    CLOSE cAperson;
    --Fin modificación 20100219
    
    --41208 JNARANJO 20170327
    vPago := RiIngresos.Aforpag_Nombre;
    IF rAperson.PerIdTitular != RiIngresos.PerId THEN
      vPago := 'NOMINA';
      nIncluyeSegSocial := -1;
      nVrNuevasCuotas := 0;  
    END IF;
    --41208 JNARANJO 20170327 Fin modificación    

    rastro('Deduccion_Nomina: '||NVL(RiIngresos.Deduccion_Nomina,0) || CHR(13) ||
           ' NCuotaAhorro: '||NVL(NCuotaAhorro, 0) || CHR(13) ||
           ' NNuevasCuota: '||NVL(nVrNuevasCuotas, 0) || CHR(13) ||
           ' Ingresos_Basicos: '||NVL(RiIngresos.Ingresos_Basicos, 0) || CHR(13) ||
           ' Conceptos_Fijos: '||NVL(RiIngresos.Conceptos_Fijos, 0) || CHR(13) ||
           ' Deduccion_Personal: '||NVL(RiIngresos.Deduccion_Personal, 0) || CHR(13) ||
           ' otras_deducciones: '||NVL(RiIngresos.otras_deducciones, 0) || CHR(13) ||
           ' gastos: '||NVL(RiIngresos.gastos, 0) || CHR(13) ||
           ' Otro_Ingresos: '||NVL(RiIngresos.Otro_Ingresos, 0) || CHR(13) ||
           ' Ingresos_Especiales: '||NVL(RiIngresos.Ingresos_Especiales, 0) || CHR(13) ||
           ' nIncluyeSegSocial: '||(nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) || CHR(13) ||
           ' FormaPago: '||RiIngresos.Aforpag_Nombre||'  vPago:'||vPago||'  modelo: '||rAperson.Modelo || CHR(13) ||   
           ' perId:'||RiIngresos.PerId            
           ,'NIVEL2ACT');


    --Modifica Jorge Naranjo 20100219 Requerimiento 23107 Agrego condición NVL(rAperson.Modelo,'NULO') = 'NOMINA'
    IF vPago = 'NOMINA' THEN
      --NPorEndeudaAct := (NVL(RiIngresos.Deduccion_Nomina, 0) -
      --41208 JNARANJO 20170307 Agrego ROUND y nRedondeoNivel
      NPorEndeudaAct := ROUND((NVL(RiIngresos.Deduccion_Nomina, 0) -      
                         NVL(NCuotaAhorro, 0) +
                         --41208 JNARANJO 20170307
                         (nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial)+
                         --41208 JNARANJO 20170307 Fin modificación
                         NVL(RiIngresos.Deduccion_Personal, 0) +
                         NVL(RiIngresos.Otras_Deducciones, 0) +
                         NVL(RiIngresos.Gastos, 0) + NVL(nVrNuevasCuotas, 0)) /
                         (NVL(RiIngresos.Ingresos_Basicos, 0) +
                         NVL(RiIngresos.Conceptos_Fijos, 0) +
                         NVL(RiIngresos.Otro_Ingresos, 0) +
                         NVL(RiIngresos.Ingresos_Especiales, 0) +
                         --41208 JNARANJO 20170307
                         (nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial)
                         --41208 JNARANJO 20170307 Fin modificación                         
                         ),nRedondeoNivel) * 100;
    ELSIF vPago = 'DEBITO' AND rAperson.Modelo = 'DEBITO' THEN
      --41208 JNARANJO 20170307 Agrego ROUND y nRedondeoNivel
      NPorEndeudaAct := ROUND((NVL(RiIngresos.Deduccion_Nomina, 0) -
                         NVL(NCuotaAhorro, 0) +
                         NVL(RiIngresos.Deduccion_Personal, 0) +
                         NVL(RiIngresos.Otras_Deducciones, 0) +
                         NVL(RiIngresos.Gastos, 0) + NVL(nVrNuevasCuotas, 0)) /
                         (NVL(RiIngresos.Ingresos_Basicos, 0) +
                         NVL(RiIngresos.Conceptos_Fijos, 0) +
                         NVL(RiIngresos.Otro_Ingresos, 0) +
                         NVL(RiIngresos.Ingresos_Especiales, 0)),nRedondeoNivel) * 100;
    ELSE
      --Modifica Jorge Naranjo 20100527 Requerimiento 23384
      /*NPorEndeudaAct := (NVL(RiIngresos.Deduccion_Nomina, 0) +
                         NVL(RiIngresos.Deduccion_Personal, 0) +
                         NVL(RiIngresos.Otras_Deducciones, 0) +
                         NVL(RiIngresos.Gastos, 0) + NVL(NNuevasCuota, 0)) /
                         (NVL(RiIngresos.Ingresos_Basicos, 0) +
                         NVL(RiIngresos.Conceptos_Fijos, 0) +
                         NVL(RiIngresos.Otro_Ingresos, 0) +
                         NVL(RiIngresos.Ingresos_Especiales, 0)) * 100;  */
      --41208 JNARANJO 20170307 Agrego ROUND y nRedondeoNivel
      NPorEndeudaAct := ROUND((NVL(RiIngresos.Deduccion_Nomina, 0) +
                         NVL(RiIngresos.Deduccion_Personal, 0) +
                         NVL(RiIngresos.Otras_Deducciones, 0) +
                         NVL(RiIngresos.Gastos, 0) + NVL(nVrNuevasCuotas, 0) - NVL(NCuotaAhorro, 0) ) /
                         (NVL(RiIngresos.Ingresos_Basicos, 0) +
                         NVL(RiIngresos.Conceptos_Fijos, 0) +
                         NVL(RiIngresos.Otro_Ingresos, 0) +
                         NVL(RiIngresos.Ingresos_Especiales, 0)),nRedondeoNivel) * 100;
      --Fin modificación 20100527
    END IF;
    rastro('NPorEndeudaAct:'||NPorEndeudaAct||' perId:'||RiIngresos.PerId,'NIVEL2ACT');
    RETURN(NPorEndeudaAct);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END Nivel2Actual;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 12/04/2004 11:58:00 p.m.
  -- Retorna el nombre del convenio
  FUNCTION NombreConvenio(NIid TS_CONVENIOS.Id%TYPE) RETURN VARCHAR2 IS
    CURSOR CConvenios IS
      SELECT Con.Nombre
        FROM TS_CONVENIOS Con
       WHERE Con.Id = NIid;

    VNom TS_CONVENIOS.Nombre%TYPE;

  BEGIN
    OPEN CConvenios;
    FETCH CConvenios
      INTO VNom;
    CLOSE CConvenios;
    IF VNom IS NOT NULL THEN
      RETURN VNom;
    END IF;
  END;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 12/04/2004 11:58:00 p.m.
  -- Retorna el nombre del servicio

  FUNCTION NombreServicio(ViCodigo IN TS_SERVICIOS.Codigo%TYPE)
    RETURN VARCHAR2 IS

    CURSOR CServicios IS
      SELECT Ser.Nombre Nombre
        FROM TS_SERVICIOS Ser
       WHERE SYSDATE BETWEEN Ser.Fecha_Inicio AND
             NVL(Ser.Fecha_Final, SYSDATE) AND Ser.Codigo = ViCodigo;

    VServicio TS_SERVICIOS.Nombre%TYPE;

  BEGIN
    OPEN CServicios;
    FETCH CServicios
      INTO VServicio;
    CLOSE CServicios;
    IF VServicio IS NULL THEN
      RETURN(NULL);
    ELSE
      RETURN(VServicio);
    END IF;
  END NombreServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 29/03/2004 01:00:00 p.m.
  -- Esta funcion obtiene el empleador de una persona, por medio del
  -- convenio que registro en la asesoria,

  FUNCTION ObtenerEmpleador(NiPerId IN NUMBER, NCvoId IN NUMBER)
    RETURN NUMBER IS

    CURSOR CEmple IS
      SELECT c.Per_Id
        FROM TS_PER_PERSONA_OCUPACIONES Peo, TS_CONVENIOS c
       WHERE c.Id = NCvoId AND Peo.Per_Id_Vinculado_a = c.Per_Id AND
             Peo.Per_Id = NiPerId AND Peo.Fecha_Final IS NULL AND
             c.Tipo_Convenio = 'DEDNOM';

    NEmpleador NUMBER;

  BEGIN
    OPEN CEmple;
    FETCH CEmple
      INTO NEmpleador;
    IF NOT CEmple%NOTFOUND THEN
      RETURN(NEmpleador);
    ELSE
      RETURN(NULL);
    END IF;
    CLOSE CEmple;

  END ObtenerEmpleador;

  FUNCTION ConfigServicio(ViSer_Codigo   IN VARCHAR2,
                          ViNomVista     IN VARCHAR2,
                          ViNomParam     IN VARCHAR2,
                          ViTipoBusqueda IN VARCHAR2) -- Determina el tipo de búsqueda
   RETURN NUMBER IS
    /****************************************************************************************************
     OBJETO: FS_ConfigServicio

     PROPOSITO:
     Permite verificar la configuración de un servicio

     PARAMETROS:
     viSer_Codigo   IN VARCHAR2  -- Codigo del servicios a buscar
     viNomVista     IN VARCHAR2  -- Nombre de la vista en donde se realiza la búsqueda
     viNomParam     IN VARCHAR2  -- Condiciones adicionales
     vTipoBusqueda  IN VARCHAR2  -- Tipo de busqueda
     A : Si el hijo tiene mas datos no busque mas.
     B : Si el hijo tiene mas registros pero no tiene la condición siga buscando

     HISTORIAL
     Fecha                 Usuario      Version      Descripcion
     11/05/2004            SIC      1.0.0        1. Creacion de la unidad de programa

     REQUISITOS:
     el código del servicio y el consecutivo del servicio
    ****************************************************************************************************/

    VSql     VARCHAR2(2000);
    VSq2     VARCHAR2(2000);
    NValor   NUMBER(5);
    VCodPapa VARCHAR2(20);

    TYPE RcGen IS REF CURSOR;
    CGen RcGen;

  BEGIN
    VSql := 'SELECT COUNT(1)' || ' FROM ' || ViNomVista || ' vista ' ||
            ' WHERE ' || ' SER_CODIGO = ' || ViSer_Codigo;
    IF ViNomParam IS NOT NULL THEN
      VSql := VSql || ' AND ' || ViNomParam;
    END IF;
    OPEN CGen FOR VSql;
    FETCH CGen
      INTO NValor;
    CLOSE CGen;
    IF NVL(NValor, 0) > 0 THEN
      RETURN(NValor);
    ELSE
      IF ViTipoBusqueda = 'A' THEN
        VSq2 := 'SELECT COUNT(1) ' || ' FROM ' || ViNomVista || ' WHERE ' ||
                ' SER_CODIGO = ' || ViSer_Codigo;
        OPEN CGen FOR VSq2;
        FETCH CGen
          INTO NValor;
        CLOSE CGen;
        IF NVL(NValor, 0) > 0 THEN
          RETURN(NULL);
        END IF;
      END IF;
      VCodPapa := Pks_Servicios.Papa(TRIM('''' FROM ViSer_Codigo));
      IF VCodPapa IS NOT NULL THEN
        NValor := ConfigServicio('''' || VCodPapa || '''',
                                 ViNomVista,
                                 ViNomParam,
                                 ViTipoBusqueda);
        RETURN(NValor);
      ELSE
        RETURN(NULL);
      END IF;
    END IF;

  END ConfigServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- Esta funcion trae el valor vigente de un parametro de una asesoria solicitada
  -- por un cliente.
  -- se ingresan como parametros los siguientes consecutivo de la asesoria
  -- nombre del parametro o columna

  FUNCTION ParGenAsesorias(NiConsecAse IN NUMBER, ViNomParam IN VARCHAR2)
    RETURN VARCHAR2 IS
    VSql   VARCHAR2(2000);
    VValor VARCHAR2(250);

    TYPE RcGen IS REF CURSOR;
    CGen RcGen;

  BEGIN
    VSql := 'SELECT ' || ViNomParam ||
            ' FROM ts_asesorias  WHERE  consecutivo_asesoria = ' ||
            NiConsecAse;
    OPEN CGen FOR VSql;
    FETCH CGen
      INTO VValor;
    CLOSE CGen;
    RETURN(VValor);
  END ParGenAsesorias;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- Esta funcion trae el valor vigente de un parametro de una asesoria solicitada
  -- por un cliente.
  -- se ingresan como parametros los siguientes consecutivo del servicio
  -- nombre del parametro o columna

  FUNCTION ParGenAsePersonas(NiConsecSer IN NUMBER, ViNomParam IN VARCHAR2)
    RETURN NUMBER IS
    VSql   VARCHAR2(2000);
    VValor NUMBER;

    TYPE RcGen IS REF CURSOR;
    CGen RcGen;

  BEGIN
    VSql := 'SELECT ' || ViNomParam ||
            ' FROM ts_ase_personas  WHERE aservic_consecutivo_servicio = ' ||
            NiConsecSer || ' AND aperson_type = ' || '''SOLICI''';
    OPEN CGen FOR VSql;
    FETCH CGen
      INTO VValor;
    CLOSE CGen;
    RETURN(VValor);
  END ParGenAsePersonas;
  --
  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion devuelve los datos de una asesoria servicio
  -- de una persona en un registro
  -- Modificado por: Juan Esteban Sierra
  -- Fecha de modificacion: 2006-11-10
  -- Modifica Jorge Naranjo 20061124 Adiciono tipo_destino al record
  FUNCTION ParGenAseServicio(NiConsecServicio IN NUMBER)
    RETURN RAseServicioCre IS

    CURSOR CASeSer IS
      SELECT Aservic.Consecutivo_Servicio,
             NULL Adepen_Asignada,
             Aservic.Aforpag_Nombre,
             Aservic.Ase_Consecutivo_Asesoria,
             Aservic.Atipper_Pago,
             Aservic.Atipper_Liquidacion,
             Pperocu.Con_Deduccion_Nomina,
             Aservic.Crtipo_Credito,
             Aservic.Crcompromete_Cesantias,
             Aservic.Crvalor_Credito,
             Aservic.Crvalor_Desembolso,
             Aservic.Crtipo_Amortizacion,
             Aservic.Crcuota_Total_Liquidacion,
             Aservic.Crcuota_Total_Pago,
             Aservic.Crperiodos_Gracia_Capital,
             Aservic.Crperiodos_Sin_Intereses,
             Aservic.Crfecha_Minima_Desembolso,
             Aservic.Cuota_Periodo_Pago,
             Aservic.Cuota_Periodo_Liquidacion,
             Aservic.Es_Viable,
             Aservic.Fecha_Corte,
             Aservic.Fecha_Efectivo,
             Aservic.Per_Id,
             Aservic.Pdirec_Id,
             Aservic.Pperocu_Id,
             Aservic.Plazo,
             Aservic.Reclama_Personal,
             Aservic.Saldo,
             Aservic.Ser_Codigo,
             Aservic.Tipo_Estado_Decision,
             Aservic.Plazo_Liquidacion,
             Aservic.Modalidad_Interes,
             Aservic.Aservic_Type,
             Aservic.Tipo_Tasa_Interes,
             Aservic.Rodia_Corte,
             Aservic.Rovalor_Cupo,
             Aservic.Fecha_Primera_Cuota,
             Aservic.Tasa_Efectiva,
             Aservic.Tasa_Base_Interes,
             Aservic.Crcuota_Liquidacion_Empleado,
             Aservic.Crcuota_Total_Liquidacion_Empl,
             Aservic.Aservic_Desembolso_Parcial,
             Aservic.Fecha_Vencimiento,
             Aservic.Crsaldo_Pendiente_Liquidar,
             Aservic.Crinteres_Por_Cobrar,
             Aservic.Crinteres_Anticipado,
             Aservic.Numero_Servicio,
             Aservic.Crtotal_Refinanciacion,
             Aservic.Crtipo_Garantia,
             Aservic.Primera_Cuota_Automatica,
             Aservic.Ahacepta_Apertura_Automatica,
             Aservic.ahdestino_interes,
             Aservic.aservic_renovacion,
             Aservic.ahdestino_cancelacion,
             Aservic.aservic_sobregiro,
             Aservic.Tipo_Destino,
             Aservic.Afforma_Amortizacion,
             Aservic.Afmodalidad_Cobro,
             Aservic.Ahcuotas_Proyectadas,
             aservic.crcar_modalidad,
            -- 26/12/2008
            -- John Arley Marín Valencia
            -- Se crea este campo nuevo en ts_as_servicios
            -- INICIO 21202
            aservic.nombre_cuenta
            -- FIN 21202
            
            -- 10/06/2010
            -- John Arley Marín Valencia
            -- Guarda la tasa que aplica al servicio.  
            -- Si es tasa fija almacena Tasa_Efectiva
            -- Si es tasa variable almacena el valor de la tasa base más los puntos de Tasa_Efectiva
            -- INICIO 23421        
            , Aservic.Tasa_Efectiva tasa_real
            -- fin 23421    
        FROM TS_ASE_SERVICIOS Aservic, TS_PER_PERSONA_OCUPACIONES Pperocu
       WHERE Aservic.Pperocu_Id = Pperocu.Id(+) AND
             Aservic.Consecutivo_Servicio = NiConsecServicio;

    -- 10/06/2010
    -- John Arley Marín Valencia
    -- Consulta el valor de la tasa base a una fecha determinada
    -- Inicio req. 23421
    CURSOR cValorTasaBase(niTasaBaseInteres in number) IS
      SELECT Tbv.Tasa_Efectiva
        FROM Con_Tipos                 Ct,
             Ts_Adm_Tasas_Base_Valores Tbv
       WHERE Ct.Sec         = Tbv.Tasa_Interes_Base
         AND SYSDATE BETWEEN Tbv.Fecha_Inicio 
                         AND NVL(Tbv.Fecha_Final,SYSDATE)
         AND Clasificacion_Credito IS NULL
         AND Ct.Sec                = niTasaBaseInteres;
         
    nValorTasaBase NUMBER(11,8);     
    -- Fin req. 23421  
    
  BEGIN
    --
    -- Limpiar el registro
    --
    GrAseServicioCre := rAseServicioCreEMPTY;
    OPEN CASeSer;
    FETCH CASeSer INTO GrAseServicioCre;
    CLOSE CASeSer;
    
    -- 10/06/2010
    -- John Arley Marín Valencia
    -- Guarda la tasa que aplica al servicio.  
    -- Si es tasa fija almacena Tasa_Efectiva
    -- Si es tasa variable almacena el valor de la tasa base más los puntos de Tasa_Efectiva
    -- INICIO 23421        
      IF GrAseServicioCre.Tipo_Tasa_Interes = 'VARIAB' THEN    
        nValorTasaBase := 0;      
        OPEN cValorTasaBase(GrAseServicioCre.tasa_base_interes);
        FETCH cValorTasaBase INTO nValorTasaBase;
        CLOSE cValorTasaBase;
        --Modifica Jorge Naranjo 20101103 Requerimiento 24547 Para sumar las tasas se debe utilizar la fórmula
        --GrAseServicioCre.tasa_real := NVL(GrAseServicioCre.Tasa_Efectiva,0) + NVL(nValorTasaBase,0);
        GrAseServicioCre.tasa_real := pks_calcular_cuota.calcular_tasa( NVL(nValorTasaBase,0),'EA',
                                                                        NVL(GrAseServicioCre.Tasa_Efectiva, 0),'EA',
                                                                        '+','DTF_SPREAD',NULL ); 
        --Fin modificación 20101103        
      ELSE     
        GrAseServicioCre.tasa_real := GrAseServicioCre.Tasa_Efectiva;
      END IF;    
    -- fin 23421    
    RETURN(GrAseServicioCre);
  END ParGenAseServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion trae los parametros que tiene un servicio de afiliacion
  -- se pasa como parametro el consecutivo del sevicio

  FUNCTION ParGenSerAfiliacion(NiConsecSer IN NUMBER) RETURN TAseServicioAfi IS

    CURSOR CParAfil IS
      SELECT Consecutivo_Servicio, Es_Viable, Reclama_Personal, Pdirec_Id,
             Aforpag_Nombre, Tipo_Estado_Decision, Ase_Consecutivo_Asesoria,
             Atipper_Pago, Per_Id, Atipper_Liquidacion, Afforma_Amortizacion,
             Afmodalidad_Cobro, Tasa_Base_Interes, Afconsecutivo,
             Cuota_Periodo_Liquidacion, Cuota_Periodo_Pago
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Consecutivo_Servicio = NiConsecSer;

    RtAseServicioAfi TAseServicioAfi;
    RegcParAfi       CParAfil%ROWTYPE;

  BEGIN
    OPEN CParAfil;
    FETCH CParAfil
      INTO RegcParAfi;
    CLOSE CParAfil;
    RtAseServicioAfi.Consecutivo_Servicio      := RegcParAfi.Consecutivo_Servicio;
    RtAseServicioAfi.Es_Viable                 := RegcParAfi.Es_Viable;
    RtAseServicioAfi.Reclama_Personal          := RegcParAfi.Reclama_Personal;
    RtAseServicioAfi.Pdirec_Id                 := RegcParAfi.Pdirec_Id;
    RtAseServicioAfi.Aforpag_Nombre            := RegcParAfi.Aforpag_Nombre;
    RtAseServicioAfi.Tipo_Estado_Decision      := RegcParAfi.Tipo_Estado_Decision;
    RtAseServicioAfi.Ase_Consecutivo_Asesoria  := RegcParAfi.Ase_Consecutivo_Asesoria;
    RtAseServicioAfi.Per_Id                    := RegcParAfi.Per_Id;
    RtAseServicioAfi.Afforma_Amortizacion      := RegcParAfi.Afforma_Amortizacion;
    RtAseServicioAfi.Afmodalidad_Cobro         := RegcParAfi.Afmodalidad_Cobro;
    RtAseServicioAfi.Tasa_Base_Interes         := RegcParAfi.Tasa_Base_Interes;
    RtAseServicioAfi.Afconsecutivo             := RegcParAfi.Afconsecutivo;
    RtAseServicioAfi.Cuota_Periodo_Liquidacion := RegcParAfi.Cuota_Periodo_Liquidacion;
    RtAseServicioAfi.Cuota_Periodo_Pago        := RegcParAfi.Cuota_Periodo_Pago;
    RETURN(RtAseServicioAfi);
  END ParGenSerAfiliacion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion determina el porcentaje de endeudamiento la linea
  --Modifica Jorge Naranjo 20090821 Requerimiento 21437 Agrego parámetro viFormaPago IN VARCHAR2
  FUNCTION ParLineaPorEndeudamiento( ViSerCodigo IN VARCHAR2,
                                     viFormaPago IN VARCHAR2 ) RETURN NUMBER IS

    --Modifica Jorge Naranjo 20090821 Requerimiento 21437
    /*CURSOR CParEndLinea IS
      SELECT Saficon.Max_Endeudamiento
        FROM VS_SER_AFI_CONDICIONES Saficon
       WHERE Saficon.Ser_Codigo = ViSerCodigo
      UNION
      SELECT Screcon.Max_Endeudamiento
        FROM VS_SER_CRE_CONDICIONES Screcon
       WHERE Screcon.Ser_Codigo = ViSerCodigo;*/
    CURSOR CParEndLinea IS
      SELECT Saficon.Max_Endeudamiento
        FROM VS_SER_AFI_CONDICIONES Saficon,vs_ser_forma_pagos vsfp
       WHERE Saficon.Ser_Codigo = ViSerCodigo
         AND Saficon.Svalor_Id = vsfp.id
         AND vsfp.Forma_Pago = viFormaPago
      UNION
      SELECT Screcon.Max_Endeudamiento
        FROM VS_SER_CRE_CONDICIONES Screcon,vs_ser_forma_pagos vsfp
       WHERE Screcon.Ser_Codigo = ViSerCodigo
         AND Screcon.Svalor_Id = vsfp.id
         AND vsfp.Forma_Pago = viFormaPago;
   --Fin modificación  20090821 Requerimiento 21437

    NPorEndeu NUMBER;

  BEGIN
    --obtiene el porcentaje general de la linea
    OPEN CParEndLinea;
    FETCH CParEndLinea
      INTO NPorEndeu;
    CLOSE CParEndLinea;
    IF NVL(NPorEndeu, 0) = 0 THEN
      NPorEndeu := 0;
    END IF;

    RETURN(NPorEndeu);
  END ParLineaPorEndeudamiento;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion trae el valor vigente de un parametro de un servicio solicitado
  -- por un cliente se ingresan como parametros los siguientes consecutivo del servicio
  -- nombre del parametro o columna

  FUNCTION ParamAseSer(NiConsecSer IN NUMBER, -- Consecutivo del servicio
                       ViNomParam  IN VARCHAR2 -- Nombre del parametro o columna
                       ) RETURN VARCHAR IS
    VSql   VARCHAR2(2000);
    VValor VARCHAR2(250);

    TYPE RcGen IS REF CURSOR;
    CGen RcGen;

  BEGIN
    VSql := 'SELECT ' || ViNomParam ||
            ' FROM ts_ase_servicios  WHERE  consecutivo_servicio = ' ||
            NiConsecSer;
    OPEN CGen FOR VSql;
    FETCH CGen
      INTO VValor;
    -- Autor Modificacion : Jorge Alberto Marquez Florez
    -- Creado : 12/03/2005 14:45:00

    /*        --- se elimina este bloque de codigo pues retorna el valor correspondiente antes de cerrar el cursor

    IF cGen%FOUND THEN
       RETURN(vValor);
     ELSE
         RETURN(NULL);
     END IF;
     CLOSE cGen;   */

    -- Se agrega el codigo que resuelve el problema

    CLOSE CGen;

    RETURN(VValor);
  END ParamAseSer;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 08:02:00 a.m.
  -- Retorna el período de pago del convenio, cuando la forma
  -- de pago es deducción de nómina
  -- teniendo el id de la ocupación

  FUNCTION PeriodoPago(NiId IN TS_PER_PERSONA_OCUPACIONES.ID%TYPE -- Id del período de pago
                       ) RETURN VARCHAR2 IS

    CURSOR CConvenio IS
      SELECT Cdeduci.Atipper_Nombre_Libranza
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu, TS_CNV_DEDUCCIONES Cdeduci
       WHERE Pperocu.Con_Deduccion_Nomina = Cdeduci.Con_Id AND
             SYSDATE BETWEEN Pperocu.Fecha_Inicial AND
             NVL(Pperocu.Fecha_Final, SYSDATE) AND
             SYSDATE BETWEEN Cdeduci.Fecha_Inicio AND
             NVL(Cdeduci.Fecha_Final, SYSDATE) AND Pperocu.Id = NiId;

    VTipper TS_CNV_DEDUCCIONES.ATIPPER_NOMBRE_LIBRANZA%TYPE;

  BEGIN
    OPEN CConvenio;
    FETCH CConvenio
      INTO VTipper;
    CLOSE CConvenio;
    IF VTipper IS NOT NULL THEN
      RETURN VTipper;
    ELSE
      RETURN NULL;
    END IF;
  END PeriodoPago;

  FUNCTION PlazoEstandar(NiConsecServic IN TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_PlazoEstandar

      PROPOSITO:
      Identificar el número de días de que disponde un cliente para evitar la renovación del ahorro a término

      PARAMETROS:
      niConsecServic IN ts_ase_servicios.consecutivo_servicio%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      14/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      Consecutivo del servicio de ahorro a término




    ****************************************************************************************************/
    Result VARCHAR2(30);
    NDias  NUMBER(3, 0) := 0; -- Utilizada para determinar le número de días habiles

    --
    -- Fecha: 14/05/2004 11:03:59 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el número de días habiles para la cancelación del ahorro a término
    --
    CURSOR CDiasHabiles IS
      SELECT Dias_Postvencimient
        FROM TS_ASE_SERVICIOS Aservic, VS_SER_AHO_CANCELACIONES Sahocan
       WHERE (Sahocan.Ser_Codigo = Aservic.Ser_Codigo OR
             SUBSTR(Sahocan.Ser_Codigo, 1, 9) =
             SUBSTR(Aservic.Ser_Codigo, 1, 9)) AND
             Aservic.Consecutivo_Servicio = NiConsecServic
       ORDER BY LENGTH(Sahocan.Ser_Codigo) DESC;

  BEGIN
    OPEN CDiasHabiles;
    FETCH CDiasHabiles
      INTO NDias;
    CLOSE CDiasHabiles;
    IF NVL(NDias, 0) > 0 THEN
      Result := NDias || ' dias';
    ELSE
      Result := 'NO';
    END IF;
    RETURN(Result);
  END PlazoEstandar;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 08:02:00 a.m.
  -- Esta funcion determina el porcentaje de endeudamiento que se debe aplicar
  -- para calular la cuota maxima que puede tener un cliente,
  -- con base al parametro general, el parametro de la linea, o el paramentro del
  -- convenio.
  --Modifica Jorge Naranjo 20090707 Requerimiento 21437 Adiciono parámetro viModelo
  FUNCTION PorcentajeEndeudamiento( NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE,
                                    viModelo IN VARCHAR2 DEFAULT 'DEBITO')
    RETURN NUMBER IS

    NPorcCon      NUMBER; --porcentaje del convenio
    NPorcGenCoope NUMBER; -- procentaje endeudamiento general para pago nomina  o pago personal de la cooperativa
    NPorcLin      NUMBER; -- procentaje endeudamiento general para pago nomina o pago perosnal de la linea
    NMenorValor   NUMBER;
    NiConvenio    NUMBER;
    ViFormaPago   VARCHAR2(10);
    ViSerCodigo   VARCHAR2(20);
    --  grAseServicioCre  Pks_proc_solicitudes.rAseServicioCre;
    -- 25211 (M) 08/03/2011 JNARANJO 
    CURSOR cRiesgoEndeudamiento( niArieend IN NUMBER ) IS
      SELECT arieend.endeudamiento_basico
      FROM ts_adm_riesgo_endeudamientos arieend
      WHERE arieend.id = niArieend;
    rRiesgoEndeudamiento cRiesgoEndeudamiento%ROWTYPE;
    trResultadoRiesgos pks_cpl0436.VResulRiesgos;
    nCantVariables PLS_INTEGER;
    nArieendId PLS_INTEGER;
    -- 25211 08/03/2011 Fin modificación
    -- 25402 (A) 30/03/2011 JNARANJO 
    CURSOR cServicioCredito( niConsAsesoria IN NUMBER ) IS
      SELECT aservic.consecutivo_servicio
      FROM ts_ase_servicios aservic
      WHERE aservic.ase_consecutivo_asesoria = niConsAsesoria 
        AND aservic.aservic_type = 'ACREDIT'
        AND SUBSTR(aservic.ser_codigo,1,6) = '001.03';
    rServicioCredito cServicioCredito%ROWTYPE;
    -- 25402 30/03/2011 Fin modificación 
    
  BEGIN
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(NiConsec_Servic);
    END IF;
    -- 25402 (A) 30/03/2011 JNARANJO 
    OPEN cServicioCredito(GrAseServicioCre.ase_consecutivo_asesoria);
    FETCH cServicioCredito INTO rServicioCredito;
    CLOSE cServicioCredito;
    -- 25402 30/03/2011 Fin modificación 
    
    --  grAseServicioCre:= Pks_proc_solicitudes.ParGenAseServicio(niConsec_servic);
    NiConvenio  := GrAseServicioCre.Con_Deduccion_Nomina;
    ViFormaPago := GrAseServicioCre.Aforpag_Nombre;
    ViSerCodigo := GrAseServicioCre.Ser_Codigo;
    --se determina el porcentaje de endeudamiento de la linea
    --Modifica Jorge Naranjo 20090821 Requerimiento 21437
    --NPorcLin := ParLineaPorEndeudamiento(ViSerCodigo);
    NPorcLin := ParLineaPorEndeudamiento(ViSerCodigo,ViFormaPago);
    --Fin modificación 20090821 Requerimiento 21437
    -- 25211 (M) 08/03/2011 JNARANJO 
    pks_cpl0436.obtener_variables( NiSer => rServicioCredito.Consecutivo_Servicio,
                                   ViSistema =>  'ASESOR', 
                                   VoResulRiesgos =>  trResultadoRiesgos,
                                   NoNroVar =>  nCantVariables );
    IF NVL(nCantVariables,0) > 0 THEN
      IF trResultadoRiesgos(1).NIdRiesgo IS NOT NULL THEN
        pks_cpl0436.verificar_politica_endeuda( niConsServicio => rServicioCredito.Consecutivo_Servicio,
                                                niArieanaId => trResultadoRiesgos(1).NIdRiesgo,
                                                noArieendId => nArieendId );
      END IF;
    END IF;
    -- 25211 08/03/2011 Fin modificación    
    --Modifica Jorge Naranjo 20090707 Requerimiento 21437
    --IF UPPER(ViFormaPago) = 'NOMINA' THEN
    IF UPPER(ViFormaPago) = 'NOMINA' OR viModelo = 'NOMINA' THEN
    --Fin modificación 20090707 Requerimiento 21437
      -- se obtiene el procentaje de endeudamiento maximo del convenio
      NPorcCon := Pks_Convenios.ParConvenioPorEdndeudamiento(NiConvenio);
      --obtiene el porcentanje de endeudamiento global para pago por nomina de la cooperativa
      -- 25211 (M) 08/03/2011 JNARANJO 
      IF nArieendId IS NULL THEN
        NPorcGenCoope := Pk_Conseres.Param('GENDEUDA_MAXIMO');
      ELSE
        OPEN cRiesgoEndeudamiento(nArieendId);
        FETCH cRiesgoEndeudamiento INTO rRiesgoEndeudamiento;
        CLOSE cRiesgoEndeudamiento;
        NPorcGenCoope := rRiesgoEndeudamiento.Endeudamiento_Basico;
      END IF;
      -- 25211 08/03/2011 Fin modificación
      --obtiene el menor valor de los tres
      rastro('NPorcGenCoope:'||NPorcGenCoope||'  NPorcLin:'||NPorcLin||'  NPorcCon:'||NPorcCon,'PORCEND');
      IF NVL(NPorcCon, 0) > 0 THEN
        IF NVL(NPorcGenCoope, 0) > 0 THEN
          IF NVL(NPorcLin, 0) > 0 THEN
            NMenorValor := LEAST(NPorcCon, NPorcGenCoope, NPorcLin);
          ELSE
            NMenorValor := LEAST(NPorcCon, NPorcGenCoope);
          END IF;
        ELSE
          NMenorValor := NPorcCon;
        END IF;
      ELSE
        IF NVL(NPorcGenCoope, 0) > 0 THEN
          IF NVL(NPorcLin, 0) > 0 THEN
            NMenorValor := LEAST(NPorcGenCoope, NPorcLin);
          ELSE
            NMenorValor := NPorcGenCoope;
          END IF;
        END IF;
      END IF;
    ELSE
      --obtiene el porcentanje de endeudamiento global para pago personal de la cooperativa
      -- 25211 (M) 08/03/2011 JNARANJO 
      IF nArieendId IS NULL THEN      
        NPorcGenCoope := Pk_Conseres.Param('GPORENDMAXPERS');
      ELSE
        OPEN cRiesgoEndeudamiento(nArieendId);
        FETCH cRiesgoEndeudamiento INTO rRiesgoEndeudamiento;
        CLOSE cRiesgoEndeudamiento;
        NPorcGenCoope := rRiesgoEndeudamiento.Endeudamiento_Basico;      
      END IF;
      -- 25211 08/03/2011 Fin modificación
      --obtiene el menor valor de los dos
      rastro('NPorcGenCoope:'||NPorcGenCoope||'  NPorcLin:'||NPorcLin,'PORCEND');
      IF NVL(NPorcGenCoope, 0) > 0 THEN
        IF NVL(NPorcLin, 0) > 0 THEN
          NMenorValor := LEAST(NPorcGenCoope, NPorcLin);
        ELSE
          NMenorValor := NPorcGenCoope;
        END IF;
      ELSE
        NMenorValor := 0;
      END IF;
    END IF;
    RETURN(NMenorValor);
  END PorcentajeEndeudamiento;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 04:45:00 p.m.
  -- Si posee contactos según documento cpl0027 cuyo objetivo haya sido la gestión de cobro

  FUNCTION PoseeContactos(NiPerId IN NUMBER) RETURN VARCHAR2 IS
    CURSOR CContactos IS
      SELECT COUNT(1)
        FROM TS_PER_CONTACTO_PERSONAS a
       WHERE a.Tipo_Objetivo = Pk_Conseres.Sec_Tipo_Codigo(789, '1') AND
             a.Per_Id = NiPerId;

    NResultado   NUMBER;
    VResContacto VARCHAR2(2);
  BEGIN
    OPEN CContactos;
    FETCH CContactos
      INTO NResultado;
    CLOSE CContactos;
    IF NVL(NResultado, 0) > 0 THEN
      VResContacto := 'SI';
    ELSE
      -- se debe reportar
      VResContacto := 'NO';
    END IF;

    RETURN(VResContacto);
  END;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/04/2004 11:02:00 a.m.
  -- Verificar si requiere aprobación de un usuario autorizado para
  -- los puntos adicionales si el servicio es ahorro a termino

  FUNCTION PuntosAhorroTermino(NiConsec_Asesor TS_ASESORIAS.Consecutivo_Asesoria%TYPE)
    RETURN VARCHAR2 IS

    VUsuario       TS_ASE_SERVICIOS.Usu_Autoriza_Puntos%TYPE;
    NDiferencia    NUMBER;
    NTasa_Sugerida NUMBER;
    NTasa_Efectiva NUMBER;
    NDepend        TS_ASE_SERVICIOS.Adepen_Asignada%TYPE;
    NDepen_Usu     TS_ASE_SERVICIOS.Adepen_Asignada%TYPE;
    VOficina       VARCHAR2(2);
    VCodigo        VARCHAR2(30);

    CURSOR CAhorro IS
      SELECT Aservic.Usu_Autoriza_Puntos,
             NVL(Aservic.Ahtasa_Sugerida, 0) Tasa_Sugerida,
             NVL(Aservic.Tasa_Efectiva, 0) Tasa_Efectiva,
             Aservic.Adepen_Asignada, Aservic.Ser_Codigo
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ahtasa_Sugerida > 0 AND
             SUBSTR(Aservic.Ser_Codigo, 1, 9) = '001.02.02' AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    CURSOR CPuntos IS
      SELECT Vahoint.Otras_Oficinas
        FROM VS_SER_AHO_INTERES_NEGOCIOS Vahoint
       WHERE (Vahoint.Ser_Codigo = VCodigo OR
             SUBSTR(Vahoint.Ser_Codigo, 1, 9) = SUBSTR(VCodigo, 1, 9) OR
             SUBSTR(Vahoint.Ser_Codigo, 1, 6) = SUBSTR(VCodigo, 1, 6)) AND
             Vahoint.Puntos_Max_Adiciona >= NDiferencia AND
             (Vahoint.Usuario IN
             (SELECT Grupo
                 FROM CON_USUARIO_GRUPOS a
                WHERE LEVEL = 2
                START WITH Usuario = VUsuario
               CONNECT BY PRIOR a.Grupo = a.Usuario))
       ORDER BY VCodigo, SUBSTR(VCodigo, 1, 6), SUBSTR(VCodigo, 1, 9);
    --Modifica Jorge Naranjo 20100804 Requerimiento 23877     
    /*CURSOR CDepend IS
      SELECT Adepusu.Adepen_Id
        FROM TS_ADM_DEPENDENCIA_USUARIOS Adepusu
       WHERE Adepusu.Usu_Usuario = VUsuario AND     
             SYSDATE BETWEEN Adepusu.Fecha_Inicio AND NVL(Adepusu.Fecha_Final, SYSDATE);*/
    --Fin modificación 23877       
  BEGIN
    OPEN CAhorro;
    FETCH CAhorro
      INTO VUsuario, NTasa_Sugerida, NTasa_Efectiva, NDepend, VCodigo;
    CLOSE CAhorro;
    IF NTasa_Efectiva > NTasa_Sugerida THEN
      NDiferencia := ABS(NTasa_Efectiva - NTasa_Sugerida);
      OPEN CPuntos;
      FETCH CPuntos INTO VOficina;
      CLOSE CPuntos;
      IF VOficina IS NOT NULL THEN
        IF VOficina = 'SI' THEN
          RETURN 'NO';
        ELSE          
          --Modifica Jorge Naranjo 20100804 Requerimiento 23877                    
          /*OPEN CDepend;
          FETCH CDepend INTO NDepen_Usu;
          CLOSE CDepend;
          IF NDepen_Usu = NDepend THEN*/
          IF TO_NUMBER(pks_sessiones.retornar_coddependencia) = NDepend THEN            
          --Fin modificación 23877       
            RETURN 'NO';
          ELSE
            RETURN 'SI';
          END IF;
        END IF;
      ELSE
        RETURN 'SI';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END PuntosAhorroTermino;

  FUNCTION ReclamoPersonal(NiConsecAsesor IN TS_ASESORIAS.Consecutivo_Asesoria%TYPE)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_ReclamoPersonal

      PROPOSITO:
      Permite identificar si reclamara la información personalmente o se le enviará a una dirección

      PARAMETROS:
      niConsecAsesor IN ts_asesorias.consecutivo_asesoria%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      07/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

    ****************************************************************************************************/
    Result VARCHAR2(500);

    --
    -- Fecha: 07/05/2004 05:53:37 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si reclama personalmente o no
    --
    CURSOR CReclama IS
      SELECT '. Para el servicio ' || Ser.Nombre
        FROM TS_ASE_SERVICIOS Aservic, TS_SERVICIOS Ser
       WHERE Aservic.Ser_Codigo = Ser.Codigo AND
             (Aservic.Reclama_Personal = 'NO' AND Aservic.Pdirec_Id IS NULL) AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsecAsesor;

  BEGIN
    OPEN CReclama;
    FETCH CReclama
      INTO Result;
    CLOSE CReclama;
    IF Result IS NULL THEN
      Result := 'NO';
    END IF;
    RETURN(Result);
  END ReclamoPersonal;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 28/08/2004 11:02:00 a.m.
  -- Verifica si el empleador tiene restringidos los créditos con pago personal
  -- para la fecha de asesoría

  FUNCTION Pago_Personal_Restringido(NiConsec_Asesor IN NUMBER)
    RETURN VARCHAR2 IS

    CURSOR CPagos IS
      SELECT COUNT(1)
        FROM TS_PER_PAGO_PERSONALES Ppagper,
             TS_PER_PERSONA_OCUPACIONES Pperocu, TS_ASE_SERVICIOS Aservic
       WHERE Pperocu.Per_Id_Vinculado_a = Ppagper.Per_Id AND
             (SYSDATE BETWEEN Ppagper.Fecha_Inicial AND
             NVL(Ppagper.Fecha_Final, SYSDATE)) AND
             Aservic.Aforpag_Nombre <> 'NOMINA' AND
             Pperocu.Id = Aservic.Pperocu_Id AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    NCont NUMBER(2);

  BEGIN
    OPEN CPagos;
    FETCH CPagos
      INTO NCont;
    CLOSE CPagos;
    IF NVL(NCont, 0) > 0 THEN
      RETURN 'SI';
    ELSE
      RETURN 'NO';
    END IF;
  END Pago_Personal_Restringido;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/04/2004 11:02:00 a.m.
  -- Verifica si el empleador tiene restringidos los créditos con pago personal
  -- para la fecha de asesoría

  FUNCTION RestriccionPagoPers(NiId IN TS_PERSONAS.Id%TYPE) RETURN VARCHAR2 IS

    CURSOR CPagos IS
      SELECT COUNT(1)
        FROM TS_PER_PAGO_PERSONALES Ppagper
       WHERE (SYSDATE BETWEEN Ppagper.Fecha_Inicial AND
             NVL(Ppagper.Fecha_Final, SYSDATE)) AND Ppagper.Per_Id = NiId;

    NCont NUMBER(2);

  BEGIN
    OPEN CPagos;
    FETCH CPagos
      INTO NCont;
    CLOSE CPagos;
    IF NVL(NCont, 0) > 0 THEN
      RETURN 'SI';
    ELSE
      RETURN 'NO';
    END IF;
  END RestriccionPagoPers;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- Esta funcion obtiene el ingreso basico mensual de una persona

  FUNCTION SalarioBasico(NiPerId IN NUMBER, NEmpleador IN NUMBER)
    RETURN NUMBER IS

    CURSOR CSal IS

      SELECT NVL(Ie.Ingresos_Basicos, 0)
        FROM TS_PER_OCU_INGRESO_EGRESOS Ie, TS_PER_PERSONA_OCUPACIONES Po
       WHERE Ie.Pperocu_Id = Po.Id AND Ie.Fecha_Final IS NULL AND
             Po.Per_Id = NiPerId AND Po.Per_Id_Vinculado_a = Nempleador;

    NIngresos NUMBER;

  BEGIN
    OPEN CSal;
    FETCH CSal
      INTO NIngresos;
    RETURN(Ningresos);
    CLOSE CSal;
  END;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- Esta funcion  obtiene el ingreso basico que un codeudor reporta en una asesoria

  FUNCTION SalBasCodeudor(NiCodeudor IN NUMBER, NiConseServicio IN NUMBER)
    RETURN NUMBER IS

    CURSOR CSalBas IS
      SELECT Ac.Ingresos_Basicos, Ac.Atipper_Nombre
        FROM TS_ASE_PERSONAS Ac
       WHERE Ac.Aservic_Consecutivo_Servicio = NiConseServicio AND
             Ac.Aperson_Type = 'CODEUD' AND Ac.Per_Id = NiCodeudor;

    NSalBas  NUMBER;
    VTipoPer VARCHAR2(10);

  BEGIN
    OPEN CSalBas;
    FETCH CSalBas
      INTO NSalBas, VTipoPer;
    IF VTipoPer <> 'MENSUAL' THEN
      NSalBas := ConvIngBasico(VTipoPer, NSalBas);
    END IF;
    CLOSE CSalBas;
    RETURN(NSalBas);
  END;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- Esta funcion debuelve los salarios basicos que respalda un codeudor

  FUNCTION SalBasSMLMVResCodeudor(NiCodeudor IN NUMBER) RETURN NUMBER IS

    NTDeudasAct           NUMBER;
    NTAportes             NUMBER;
    NMaxSMLMV             NUMBER;
    NTotalCredRespaldados NUMBER;

  BEGIN
    NTDeudasAct           := TotalCreditosPer(NiCodeudor);
    NTAportes             := SerSaldo(NiCodeudor, '01.03.01');
    NTotalCredRespaldados := TotalCredCodeudor(NiCodeudor);
    NMaxSMLMV             := (NTDeudasAct + NTotalCredRespaldados -
                             NTAportes) / Pk_Conseres.Param('GSMLMV');
    RETURN(NMaxSMLMV);
  END SalBasSMLMVResCodeudor;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- Esta funcion debuelve los salarios basicos que respalda un codeudor

  FUNCTION SalBasResCodeudor(NiCodeudor      IN NUMBER,
                             NiConseServicio IN NUMBER) RETURN NUMBER IS

    NTDeudasAct           NUMBER;
    NTAportes             NUMBER;
    NMaxSMLMV             NUMBER;
    NTotalCredRespaldados NUMBER;

  BEGIN
    NTDeudasAct           := TotalCreditosPer(NiCodeudor);
    NTAportes             := SerSaldo(NiCodeudor, '01.03.01');
    NTotalCredRespaldados := TotalCredCodeudor(NiCodeudor);
    NMaxSMLMV             := (NTDeudasAct + NTotalCredRespaldados -
                             NTAportes) /
                             SalBasCodeudor(NiCodeudor, NiConseServicio);
    RETURN(NMaxSMLMV);
  END SalBasResCodeudor;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 04:45:00 p.m.
  -- determina el ingreso minimo exgido para una linea, segun la forma de pago
  -- a una fecha dada.

  FUNCTION SerIngresoMinimo(ViSer_Codigo IN VARCHAR2, -- Codigo del servicio
                            ViFormaPago  IN VARCHAR2 -- Forma de pago
                            ) RETURN NUMBER IS

    CURSOR CSerCre IS
      SELECT Vsercre.Ingresos_Minimos
        FROM VS_SER_CRE_CONDICIONES Vsercre, VS_SER_FORMA_PAGOS Fp
       WHERE (Vsercre.Ser_Codigo = ViSer_Codigo OR
             Vsercre.Ser_Codigo = SUBSTR(ViSer_Codigo, 1, 9) OR
             Vsercre.Ser_Codigo = SUBSTR(ViSer_Codigo, 1, 6)) AND
             Fp.Forma_Pago = ViFormaPago AND
             Fp.Ser_Codigo = Vsercre.Ser_Codigo
       ORDER BY Vsercre.Ser_Codigo, SUBSTR(Vsercre.Ser_Codigo, 1, 9),
                SUBSTR(Vsercre.Ser_Codigo, 1, 6);

    CURSOR CSerAfi IS
      SELECT Vsercre.Ingresos_Minimos
        FROM VS_SER_AFI_CONDICIONES Vsercre, VS_SER_FORMA_PAGOS Fp
       WHERE (Vsercre.Ser_Codigo = ViSer_Codigo OR
             Vsercre.Ser_Codigo = SUBSTR(ViSer_Codigo, 1, 9) OR
             Vsercre.Ser_Codigo = SUBSTR(ViSer_Codigo, 1, 6)) AND
             Fp.Forma_Pago = ViFormaPago AND
             Fp.Ser_Codigo = Vsercre.Ser_Codigo
       ORDER BY Vsercre.Ser_Codigo, SUBSTR(Vsercre.Ser_Codigo, 1, 9),
                SUBSTR(Vsercre.Ser_Codigo, 1, 6);

    NIngMin NUMBER;

  BEGIN
    IF SUBSTR(ViSer_Codigo, 1, 6) = '001.01' THEN
      OPEN CSerAfi;
      FETCH CSErAfi
        INTO NIngMin;
      CLOSE CSerAfi;
    ELSIF SUBSTR(ViSer_Codigo, 1, 6) = '001.03' THEN
      OPEN CSerCre;
      FETCH CSerCre
        INTO NIngMin;
      CLOSE CSerCre;
    END IF;
    --30833 JNARANJO 20130923
    --RETURN(NIngMin);
    RETURN(NVL(NIngMin,0)*NVL(Pk_Conseres.Param_Gnral('GSMLMV'),0));
    --30833 JNARANJO Fin modificación
  END SerIngresoMinimo;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 12:00:00 p.m.
  -- Busca los servivios exigidos para una linea

  FUNCTION SerExigidos(NiConsec_Servic IN TS_ASE_SERVICIOS.CONSECUTIVO_SERVICIO%TYPE)
    RETURN VARCHAR2 IS

    VSer           TS_ASE_SERVICIOS.SER_CODIGO%TYPE;
    NPer_Id        TS_ASE_SERVICIOS.Per_Id%TYPE;
    I              NUMBER;
    NCont          NUMBER(2);
    VNomSer        VARCHAR2(100);
    VNomAna        VARCHAR2(100);
    VResultado     VARCHAR2(2000);
    VSer_Codigo    TS_ASE_SERVICIOS.SER_CODIGO%TYPE;
    NConsec_Asesor TS_ASESORIAS.Consecutivo_Asesoria%TYPE;
    NNum           NUMBER(1);

    CURSOR CServicios IS
      SELECT Aservic.Ser_Codigo, Aservic.Per_Id,
             Aservic.Ase_Consecutivo_Asesoria
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic;

    CURSOR CSer IS
      SELECT MAX(T.SAPLICA_FECHA_INICIAL), T.SER_CODIGO_ADIC SERADIC
        FROM VS_SER_SERVICIO_EXIGIDOS t
       WHERE SER_CODIGO = VSer OR SER_CODIGO = SUBSTR(VSer, 1, 9) OR
             SER_CODIGO = SUBSTR(VSer, 1, 6)
       GROUP BY T.SER_CODIGO_ADIC
       ORDER BY T.SER_CODIGO_ADIC DESC;

    CURSOR CNomServicio IS
      SELECT Ser.Nombre
        FROM TS_SERVICIOS Ser
       WHERE Ser.Codigo = VSer_Codigo;

    CURSOR CNomAnalizado IS
      SELECT Ser.Nombre
        FROM TS_SERVICIOS Ser
       WHERE Ser.Codigo = VSer;

    CURSOR CSer_Exig IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.05')) AND
             Aservic.Per_Id = NPer_Id AND Aservic.Ser_Codigo = VSer_Codigo;

    --
    -- Fecha: 25/08/04 12:44:58 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si el servicio exigido se encuentra en la misma asesoría
    --
    CURSOR CAsesoria IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ser_Codigo = VSer_Codigo AND
             Aservic.Ase_Consecutivo_Asesoria = NConsec_Asesor;

  BEGIN
    OPEN CServicios;
    FETCH CServicios
      INTO VSer, NPer_Id, NConsec_Asesor;
    CLOSE CServicios;
    i := 0;
    FOR CdcSer IN CSer LOOP
      I           := I + 1;
      VSer_Codigo := CdcSer.SERADIC;
      OPEN CSer_Exig;
      FETCH CSer_Exig
        INTO NCont;
      CLOSE CSer_Exig;
      OPEN CAsesoria;
      FETCH CAsesoria
        INTO NNum;
      CLOSE CAsesoria;
      IF NVL(NCont, 0) = 0 AND NVL(NNum, 0) = 0 THEN
        OPEN CNomAnalizado;
        FETCH CNomAnalizado
          INTO VNomAna;
        CLOSE CNomAnalizado;
        OPEN CNomServicio;
        FETCH CnoMservicio
          INTO VNomSer;
        CLOSE CNomServicio;
        IF VResultado IS NULL THEN
          VResultado := 'El servicio ' || VNomAna || ' requiere: ' ||
                        VNomSer;
        ELSE
          VResultado := VResultado || ', ' || VNomSer;
        END IF;
      END IF;
    END LOOP;
    IF VResultado IS NULL THEN
      RETURN 'NO';
    ELSE
      RETURN VResultado;
    END IF;
  END SerExigidos;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion devuelve el saldo de un servicio de una persona
  FUNCTION SerSaldo(NiPerId IN NUMBER, VSerCodigo IN VARCHAR2) RETURN NUMBER IS

    CURSOR CSaldo IS
      SELECT NVL(Ase.Saldo, 0)
        FROM TS_ASE_SERVICIOS Ase
       WHERE Ase.Ser_Codigo = VSerCodigo AND Ase.Per_Id = NiPerid AND
             Ase.Tipo_Estado_Decision = '2.08';
    NSaldo NUMBER;

  BEGIN
    OPEN CSaldo;
    FETCH CSaldo
      INTO NSaldo;
    CLOSE CSaldo;
    RETURN(NSaldo);
  END SerSaldo;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 11:02:00 a.m.
  -- Procedimieto para verificar si la línea es exclusiva para empleados

  FUNCTION ServicioEmpleado(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    NCont NUMBER(2);

    CURSOR Cpersonas IS
      SELECT COUNT(1)
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu, TS_ASE_SERVICIOS Aservic,
             TS_PERSONAS Per
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic AND
             Aservic.Pperocu_Id = Pperocu.Id AND
             Per.Identificacion IN
             ('8909011763', '8110170243', '890901176', '811017024') AND
             Pperocu.Per_Id_Vinculado_a = Per.Id;

    --     grAseServicioCre      Pks_proc_solicitudes.rAseServicioCre;

  BEGIN
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(NiConsec_Servic);
    END IF;

    --        grAseServicioCre:= Pks_proc_solicitudes.ParGenAseServicio(niConsec_Servic);
    NCont := ConfigServicio('''' || GrAseServicioCre.Ser_Codigo || '''',
                            'VS_SER_CREDITOS',
                            'vista.exclusivo_empleados = ' || '''SI''',
                            'A');
    IF NVL(NCont, 0) = 0 THEN
      NCont := ConfigServicio('''' || GrAseServicioCre.Ser_Codigo || '''',
                              'VS_SER_AHORROS',
                              'vista.exclusivo_empleados = ' || '''SI''',
                              'A');
    END IF;
    IF NVL(NCont, 0) = 0 THEN
      NCont := ConfigServicio('''' || GrAseServicioCre.Ser_Codigo || '''',
                              'VS_SER_AFILIACIONES',
                              'vista.exclusivo_empleados = ' || '''SI''',
                              'A');
    END IF;
    IF NVL(NCont, 0) > 0 THEN
      OPEN CPersonas;
      FETCH CPersonas
        INTO NCont;
      CLOSE CPersonas;
      IF NVL(NCont, 0) > 0 THEN
        RETURN 'NO';
      ELSE
        RETURN 'SI'; --No cumple la condición
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END ServicioEmpleado;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 03:48:00 p.m.
  -- Determina si una persona ha sido excluida de un servicio, indicando cuales

  FUNCTION ServicioSuspendido(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    CURSOR CPerServ IS
      SELECT Pserest.Fecha_Inicial, Pserest.Fecha_Final, Ser.Nombre,
             Pserest.Estado_Servicio
        FROM TS_ASE_SERVICIOS Aservic, TS_SERVICIOS Ser,
             TS_PER_SERVICIO_ESTADOS Pserest
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic AND
             Aservic.Ser_Codigo = Ser.Codigo AND
             Pserest.Ser_Codigo =
             SUBSTR(Aservic.Ser_Codigo, 1, LENGTH(Pserest.Ser_Codigo)) AND
             Pserest.Per_Id_Definido_Por = Aservic.Per_Id AND
             SYSDATE BETWEEN Pserest.Fecha_Inicial AND
             NVL(Pserest.Fecha_Final, SYSDATE) AND
             Pserest.Estado_Servicio IN ('SUSPEN', 'EXCLUI');

    VFec_Ini VARCHAR2(20);
    VFec_Fin VARCHAR2(20);
    VNombre  VARCHAR2(500);
    VResult  VARCHAR2(1000);
    VEst     VARCHAR2(15);
    VEstado  VARCHAR2(6);
    VFecha   VARCHAR2(30);

  BEGIN
    OPEN CPerServ;
    FETCH CPerServ
      INTO VFec_Ini, VFec_Fin, VNombre, VEstado;
    CLOSE CPerServ;
    IF Vfec_Ini IS NOT NULL THEN

      IF VEstado = 'EXCLUI' THEN
        VEst := 'Excluido';
      ELSE
        VEst := 'Suspendido';
      END IF;
      IF VFec_Fin IS NULL THEN
        VFecha := NULL;
      ELSE
        VFecha := ' hasta ' || VFec_Fin;
      END IF;
      VResult := ' ' || VEst || ' para el servicio ' || VNombre ||
                 ' desde ' || VFec_Ini || VFecha;
    ELSE
      VResult := 'NO';
    END IF;
    RETURN(VResult);
  END ServicioSuspendido;

  FUNCTION ServicRestringidosConv(NiConsecServic IN TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_ServicRestringidosConv

      PROPOSITO:
      Verificar si el servicio se encuentra restringido para el convenio

      PARAMETROS:
      niConsecServic IN ts_ase_servicios.consecutivo_servicio%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      12/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      Tener un convenio seleccionado




    ****************************************************************************************************/
    Result VARCHAR2(2);
    NCont  NUMBER(2);
    --
    -- Fecha: 12/05/2004 12:08:11 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si el servicio esta restringido para el convenio
    --
    CURSOR CSerRestring IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic, TS_PER_PERSONA_OCUPACIONES Pperocu,
             TS_CNV_EXCEPCION_SERVICIOS Cexcser
       WHERE Cexcser.Ser_Codigo = Aservic.Ser_Codigo AND
             Pperocu.Con_Deduccion_Nomina = Cexcser.Con_Id AND
             Aservic.Pperocu_Id = Pperocu.Id AND
             Aservic.Consecutivo_Servicio = NiConsecservic;

  BEGIN
    OPEN CSerRestring;
    FETCH CSerRestring
      INTO NCont;
    CLOSE CSerRestring;
    IF NVL(NCont, 0) > 0 THEN
      Result := 'SI';
    ELSE
      Result := 'NO';
    END IF;
    RETURN(Result);
  END ServicRestringidosConv;

  FUNCTION ServicioBloqueado(NiPerid IN TS_PERSONAS.Id%TYPE) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_ServicioBloqueado

      PROPOSITO:
      Verificar si el cliente posee servicios bloqueados

      PARAMETROS:
      niPerid IN ts_personas.id%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      14/05/2004            SIC      1.0.0        1. Creacion de la unidad de programa

    ****************************************************************************************************/
    Result VARCHAR2(2);
    NCont  NUMBER(2, 0) := 0; -- Utilizada para determinar cuantos servicios estan bloqueados

    --
    -- Fecha: 14/05/2004 11:39:59 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si posee servicios bloqueados
    --
    CURSOR CSerBloqueos IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ahesta_Bloqueado = 'SI' AND Aservic.Per_Id = NiPerid;

  BEGIN
    OPEN CSerBloqueos;
    FETCH CSerBloqueos
      INTO NCont;
    CLOSE CSerBloqueos;
    IF NVL(NCont, 0) > 0 THEN
      Result := 'SI';
    ELSE
      Result := 'NO';
    END IF;
    RETURN(Result);
  END ServicioBloqueado;

  FUNCTION ServicioEmbargado(NiPerid IN TS_PERSONAS.Id%TYPE) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_ServicioEmbargado

      PROPOSITO:
      Verificar si el cliente posee servicios embargados

      PARAMETROS:
      niPerid IN ts_personas.id%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      14/05/2004            SIC      1.0.0        1. Creacion de la unidad de programa

    ****************************************************************************************************/
    Result VARCHAR2(2);
    NCont  NUMBER(2, 0) := 0; -- Utilizada para determinar cuantos servicios estan embargados

    --
    -- Fecha: 14/05/2004 11:39:59 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si posee servicios embargados
    --
    CURSOR CSerBloqueos IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ahesta_Embargado = 'SI' AND Aservic.Per_Id = NiPerid;

  BEGIN
    OPEN CSerBloqueos;
    FETCH CSerBloqueos
      INTO NCont;
    CLOSE CSerBloqueos;
    IF NVL(NCont, 0) > 0 THEN
      Result := 'SI';
    ELSE
      Result := 'NO';
    END IF;
    RETURN(Result);
  END ServicioEmbargado;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 22/04/2004 12:00:00 p.m.
  -- Sumatoria de la cuota de pago por asesoria

  FUNCTION SumarCuotaPago(NiConsec_Asesor IN NUMBER) RETURN NUMBER IS
    
    CURSOR CServicios IS
      -- 24717 (M) 07/04/2011 JNARANJO 
      --El campo rocuota_base_solicitada contempla los servicios rotativos, crcuota_total_pago los servicios de créditos
      -- 24717 (M) 2011/04/07 JNARANJO 
      --SELECT SUM(DECODE(SUBSTR(Ase.Ser_Codigo, 1, 6),'001.03',NVL(Ase.Crcuota_Total_Pago, 0),NVL(Ase.Cuota_Periodo_Pago, 0))) Cuota_Periodo_Pago
      SELECT SUM(DECODE(aservic.aservic_type,'AROTATI', NVL(aservic.rocuota_base_solicitada,0),
                                             'ACREDIT', NVL(aservic.crcuota_total_pago,0),NVL(aservic.cuota_periodo_pago,0))) Cuota_Periodo_Pago
      -- 24717 (M) 2011/04/07 Fin modificación                
        FROM ts_ase_servicios aservic
       WHERE aservic.ase_consecutivo_asesoria = NiConsec_Asesor;

    NTotal NUMBER(15);

  BEGIN
    OPEN CServicios;
    FETCH CServicios INTO NTotal;
    CLOSE CServicios;
    RETURN NTotal;
  -- 24717 (M) 2011/04/07 JNARANJO 
  EXCEPTION
    WHEN OTHERS THEN
      pks_case_exception.raise_app_exception(vi_msj => 'Error al calcular las cuotas para el cliente.'||SQLCODE||' '||SQLERRM);
  -- 24717 (M) 2011/04/07 JNARANJO Fin modificación
  END SumarCuotaPago;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 03:48:00 p.m.
  -- Verifica si cumple con el tiempo minimo para tener un credito despés de la afiliación

  FUNCTION TiempoMinimoCredito(NiPerId        IN NUMBER, --Id de la persona
                               NiConsServicio IN NUMBER --Consecutivo del servicio solicitado
                               ) RETURN VARCHAR2 IS

    CURSOR CAfiliacion IS
      SELECT MAX(a.Fecha_Efectivo)
        FROM TS_ASE_SERVICIOS a
       WHERE a.Per_Id = NiPerId AND Ser_Codigo = '001.01.00.001' AND
             a.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08');

    NTiempoDesdeAfilia VS_SER_CREDITOS.Tiempo_Desde_Afilia%TYPE;
    DAfiliacion        DATE;
    NMeses             NUMBER;
    VResAfiliacion     VARCHAR2(2);
    VSerCodigo         VARCHAR2(20);

  BEGIN
    VSerCodigo := ParamAseSer(NiConsServicio, 'Ser_codigo');
    IF SUBSTR(VSerCodigo, 1, 6) = '001.03' THEN
      -- si existe un tiempo minimo para otorgar un credito
      NTiempoDesdeAfilia := Pks_Servicios.Parametro(VSerCodigo,
                                                    'VS_SER_CREDITOS',
                                                    'TIEMPO_DESDE_AFILIA',
                                                    SYSDATE);
    END IF;
    IF NVL(NTiempoDesdeAfilia, 0) > 0 THEN
      OPEN CAfiliacion;
      FETCH CAfiliacion
        INTO DAfiliacion;
      IF CAfiliacion%NOTFOUND THEN
        DAfiliacion := NULL;
      END IF;
      CLOSE CAfiliacion;
      IF DAfiliacion IS NOT NULL THEN
        NMeses := TRUNC(MONTHS_BETWEEN(SYSDATE, DAfiliacion));
        IF NMeses < NTiempoDesdeAfilia THEN
          -- se debe reportar
          VResAfiliacion := 'SI';
        ELSE
          VResAfiliacion := 'NO';
        END IF;
      ELSE
        VResAfiliacion := 'SI'; -- Se debe reportar porque no tiene afiliación
      END IF;
    ELSE
      VResAfiliacion := 'NO';
    END IF;
    RETURN(VResAfiliacion);
  END TiempoMinimoCredito;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion devuelve el total de los creditos que posee una persona

  FUNCTION TotalCreditosPer(NiPerId IN NUMBER) RETURN NUMBER IS

    CURSOR CCreditos IS
      SELECT NVL(SUM(Ase.Crvalor_Credito), 0)
        FROM TS_ASE_SERVICIOS Ase
       WHERE Ase.Per_Id = NiPerId AND
             SUBSTR(Ase.Ser_Codigo, 1, 6) = '001.03' AND
             (Ase.Tipo_Estado_Decision = '2.08' OR
             (Ase.Tipo_Estado_Decision = '2.15' AND NVL(Ase.Saldo, 0) > 0));

    NTotalCred NUMBER;

  BEGIN
    OPEN CCreditos;
    FETCH CCreditos
      INTO NTotalCred;
    CLOSE CCreditos;
    RETURN(NTotalCred);
  END TotalCreditosPer;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion devuelve el valor total de los creditos que respala un codeudor

  FUNCTION TotalCredCodeudor(NiCodeudor IN NUMBER) RETURN NUMBER IS

    CURSOR CCreAct(NiPerId IN NUMBER) IS
      SELECT Ase.Crvalor_Credito
        FROM TS_ASE_PERSONAS c, TS_ASE_SERVICIOS Ase
       WHERE c.Per_Id = NiCodeudor AND c.Aperson_Type = 'CODEUD' AND
             c.Aservic_Consecutivo_Servicio = Ase.Consecutivo_Servicio;

    NCoduedor    NUMBER;
    NTotCreditos NUMBER;

  BEGIN

    NTotCreditos := 0;
    FOR CdcCreAct IN CCreAct(NCoduedor) LOOP
      NTotCreditos := NTotCreditos + CdcCreAct.Crvalor_Credito;
    END LOOP;
    RETURN(NTotCreditos);
  END TotalCredCodeudor;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Este funcion obtiene el total de las cuotas actuales de un servicio
  -- el vTipoEstado indica si toma solamente los efectivos o incluye
  -- tambien los que no han sido aprobados

  FUNCTION TotalCuotasServicio(NiPerId    IN NUMBER,
                               VSerCodigo IN VARCHAR2,
                               NPer_Men   IN NUMBER DEFAULT 0,
                               NiPerocuId IN NUMBER DEFAULT NULL)
    RETURN NUMBER IS

    CURSOR CCuotaActSer IS
      SELECT SUM(Aservic.Cuota_Periodo_Pago)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.05'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08')) AND
             Aservic.Per_Id = NiPerId AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = VSerCodigo;

    -- Alberto Eduardo sánchez B.
    -- 26/08/2004 11:06
    -- Se adiciono el siguiente cursor para la línea de ahorros Luz Edith García

    CURSOR CCuota_Ahorr IS
      SELECT SUM(NVL(NVL(Aservic.Crcuota_Total_Pago,
                          Aservic.Cuota_Periodo_Pago),
                      0) / NVL(Atipper.Numero_Dias, 0) * (NPer_Men))
        FROM TS_ASE_SERVICIOS Aservic, TS_ADM_TIPO_PERIODOS Atipper
       WHERE Aservic.Atipper_Pago = Atipper.Nombre AND
             Aservic.Numero_Servicio IS NOT NULL AND
             Aservic.Aforpag_Nombre = 'NOMINA' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.02'
            --AND aservic.tipo_estado_decision = PK_CONSERES.sec_tipo_codigo(1264,'2.15')
             AND (Aservic.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') OR
             (Aservic.Numero_Servicio IS NOT NULL AND
             Aservic.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'))) AND
             Aservic.Per_Id = NiPerId AND
             (Aservic.Pperocu_Id = NiPerocuId OR NiPerocuId IS NULL) AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = VSerCodigo;

    --
    -- Fecha modificación : 12/10/04 03:46:58 p.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Deben incluirse las cutoas que sean por deduccion de nómina por que solo se consideran las deducciones por nómina
    -- Marylu
    --

    NTotalCuota NUMBER;

  BEGIN
    IF VSerCodigo = '001.02' THEN

      OPEN CCuota_Ahorr;
      FETCH CCuota_Ahorr
        INTO NTotalCuota;
      CLOSE CCuota_Ahorr;
    ELSE
      OPEN CCuotaActSer;
      FETCH CCuotaActSer
        INTO NTotalCuota;
      CLOSE CCuotaActSer;
    END IF;
    RETURN(CEIL(NTotalCuota));
  END TotalCuotasServicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 27/04/2004 03:48:00 p.m.
  -- Verifica si es posible anular la asesoria

  FUNCTION ValidaAnulacion(NiConsec_Asesor TS_ASESORIAS.Consecutivo_Asesoria%TYPE)
    RETURN VARCHAR2 IS

    CURSOR CServicios IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE (Aservic.Numero_Servicio IS NOT NULL OR
             Aservic.Tipo_Estado_Decision <>
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01')) AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    NResultado NUMBER;

  BEGIN
    OPEN CServicios;
    FETCH CServicios
      INTO NResultado;
    CLOSE CServicios;
    IF NVL(NResultado, 0) > 0 THEN
      RETURN 'SI'; -- se debe reportar
    ELSE
      RETURN 'NO';
    END IF;
  END ValidaAnulacion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- la actividad ecoNOmica o CIIU del cliente
  -- NO debe estar entre los NO permitidos para la linea

  FUNCTION ValidaCiiuCliente(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    CURSOR CCiiu IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic, TS_PERSONAS Per,
             VS_SER_CIIU_RESTRINGIDOS Sciires
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic AND
             Aservic.Per_Id = Per.Id AND
             (Sciires.Ser_Codigo = Aservic.Ser_Codigo OR
             SUBSTR(Sciires.Ser_Codigo, 1, 9) =
             SUBSTR(Aservic.Ser_Codigo, 1, 9) OR
             SUBSTR(Sciires.Ser_Codigo, 1, 6) =
             SUBSTR(Aservic.Ser_Codigo, 1, 6)) AND
             Sciires.Ciiu = Per.Aciiu_Ciiu
       ORDER BY Aservic.Ser_Codigo DESC;

    NResultado NUMBER;
    VoResCiiu  VARCHAR2(2);

  BEGIN
    OPEN CCiiu;
    FETCH CCiiu
      INTO NResultado;
    CLOSE CCiiu;
    IF NVL(NResultado, 0) > 0 THEN
      VoResCiiu := 'SI'; -- se debe reportar
    ELSE
      VoResCiiu := 'NO';
    END IF;
    RETURN(VoResCiiu);
  END ValidaCiiuCliente;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Validar que el contrato sea permitido por la linea
  -- devuelve si, en caso de que el tipo de contrato sea permitido

  FUNCTION ValidaContrato(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    CURSOR CContrato IS
      SELECT Pperocu.Tipo_Contrato_Laboral, Pperocu.Vencimiento_Contrato
        FROM TS_ASE_SERVICIOS Aservic, TS_PER_PERSONA_OCUPACIONES Pperocu
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) IN ('001.01', '001.03') AND
             Aservic.Pperocu_Id = Pperocu.Id AND Pperocu.Atipocu_Codigo = 1; -- se valida solo para empleados

    VResp      VARCHAR2(100);
    NTipoCont  NUMBER;
    DFecha_Ven DATE;
    --    grAseServicioCre      Pks_proc_solicitudes.rAseServicioCre;
    NCont NUMBER(2);

  BEGIN
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(NiConsec_Servic);
    END IF;

    --      grAseServicioCre:= Pks_proc_solicitudes.ParGenAseServicio(niConsec_Servic);
    OPEN CContrato;
    FETCH CContrato
      INTO NTipoCont, DFecha_Ven;
    CLOSE CContrato;
    IF NTipoCont IS NOT NULL THEN
      NCont := ConfigServicio('''' || GrAseServicioCre.Ser_Codigo || '''',
                              'VS_SER_CONTRATOS',
                              'vista.tipo_contrato = ' || NTipoCont,
                              'A');
      IF NVL(NCont, 0) = 0 THEN
        VResp := 'SI'; -- se debe reportar
      ELSE
        IF DFecha_Ven IS NULL THEN
          VResp := 'NO';
        ELSE
          IF TRUNC(DFecha_Ven) >= TRUNC(SYSDATE) THEN
            VResp := 'NO';
          ELSE
            VResp := ' El contrato venció el ' || TO_CHAR(DFecha_Ven);
          END IF;
        END IF;
      END IF;
    ELSE
      VResp := 'NO';
    END IF;
    RETURN(VResp);
  END ValidaContrato;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion valida  que la ocupacion del cliente
  -- este entre las permitidas por la linea
  -- Si resp es si, la ocupacion es valida, de lo contrario esta no es permitida.

  FUNCTION ValidaOcupacionPersona(ViSerCodigo  IN VARCHAR2,
                                  NiPperocu_Id IN NUMBER) RETURN VARCHAR2 IS

    CURSOR CSerOcupa IS

      SELECT 1
        FROM VS_SER_OCUPACIONES Vserocu, TS_PER_PERSONA_OCUPACIONES Pperocu
       WHERE Vserocu.Ser_Codigo = ViSerCodigo AND Pperocu.Id = NiPperocu_Id AND
             Pperocu.Atipocu_Codigo = Vserocu.Ocupacion;

    NOcupacion NUMBER;
    Vresp      VARCHAR2(2);

  BEGIN
    OPEN CSerOcupa;
    FETCH CSerOcupa
      INTO NOcupacion;
    IF CSerOcupa%NOTFOUND THEN
      Vresp := 'NO';
    ELSE
      Vresp := 'SI';
    END IF;
    CLOSE CSerOcupa;
    RETURN(Vresp);
  END ValidaOcupacionPersona;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion valida  que la ocupacion del cliente
  -- no este entre entre las restringidas por la linea

  FUNCTION Ocupacion_Restringida(NiConsecServic IN NUMBER,
                                 NiPperocu_Id   IN NUMBER) RETURN VARCHAR2 IS

    CURSOR CSerOcupa IS
      SELECT 1
        FROM VS_SER_OCUPACIONES Vserocu, TS_PER_PERSONA_OCUPACIONES Pperocu,
             TS_ASE_SERVICIOS Aservic
       WHERE Vserocu.Ser_Codigo = Aservic.Ser_Codigo AND
             Pperocu.Atipocu_Codigo = Vserocu.Ocupacion AND
             Pperocu.Id = Aservic.Pperocu_Id AND
             Aservic.Consecutivo_Servicio = NiConsecServic;

    CURSOR CSinOcupa IS
      SELECT 1
        FROM VS_SER_OCUPACIONES Vserocu, TS_PER_PERSONA_OCUPACIONES Pperocu,
             TS_ASE_SERVICIOS Aservic
       WHERE Vserocu.Ser_Codigo = Aservic.Ser_Codigo AND
             Pperocu.Atipocu_Codigo = Vserocu.Ocupacion AND
             Pperocu.Per_Id = Aservic.Per_Id AND
             Aservic.Consecutivo_Servicio = NiConsecServic;

    NOcupacion NUMBER;
    Vresp      VARCHAR2(2);

  BEGIN
    IF NiPperocu_Id IS NULL THEN
      OPEN CSinOcupa;
      FETCH CSinOcupa
        INTO NOcupacion;
      CLOSE CSinOcupa;
    ELSE
      OPEN CSerOcupa;
      FETCH CSerOcupa
        INTO NOcupacion;
      CLOSE CSerOcupa;
    END IF;
    IF NVL(NOcupacion, 0) = 0 THEN
      Vresp := 'NO';
    ELSE
      Vresp := 'SI';
    END IF;
    RETURN(Vresp);
  END Ocupacion_Restringida;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- validar que el contrato sea permitido por la linea
  -- y hallar el tiempo minimo laborado exigido por la linea para ese tipo de contrato
  -- Devuelve si, si se cumple con el tiempo minimo en labores
  -- Las dos siguientes letras, si el tiempo minimo de labores es permitido

  FUNCTION ValidaTiempoLabores(NiConsec_Aservic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    --
    -- Fecha: 06/05/2004 08:28:46 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar el tiempo minimo de labores
    --
    CURSOR CContrato IS
      SELECT Pperocu.Fecha_Inicial
        FROM TS_ASE_SERVICIOS Aservic, TS_PER_PERSONA_OCUPACIONES Pperocu
       WHERE Aservic.Pperocu_Id = Pperocu.Id AND Pperocu.Atipocu_Codigo = 1 AND
             Aservic.Consecutivo_Servicio = NiConsec_Aservic
       ORDER BY Aservic.Ser_Codigo DESC;

    VResp         VARCHAR2(4);
    NTiempoLab    NUMBER;
    DFechaIngreso DATE;

    --    grAseServicioCre      Pks_proc_solicitudes.rAseServicioCre;
    NCont NUMBER(2);

  BEGIN
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(NiConsec_Aservic);
    END IF;

    --      grAseServicioCre:= Pks_proc_solicitudes.ParGenAseServicio(niConsec_Aservic);
    OPEN CContrato;
    FETCH CContrato
      INTO DfechaIngreso;
    CLOSE CContrato;
    IF DFechaIngreso IS NOT NULL THEN
      NTiempoLab := ((TRUNC(SYSDATE) - TRUNC(DFechaIngreso)) / 30);
    END IF;
    IF NTiempoLab IS NOT NULL THEN
      NCont := ConfigServicio('''' || GrAseServicioCre.Ser_Codigo || '''',
                              'VS_SER_CONTRATOS',
                              'vista.tiempo_minimo_labor > to_number(''' ||
                              NTiempoLab || ''')',
                              --'vista.tiempo_minimo_labor > '||nTiempoLab,
                              'A');
      IF NCont > 0 THEN
        VResp := 'SI';

      ELSE
        VResp := 'NO';
      END IF;
    ELSE
      VResp := 'NO';
    END IF;
    RETURN(VResp);
  END ValidaTiempoLabores;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Si la ocupacion es independiente y posee
  -- una empresa, establecimiento comercial, se valida
  -- que el tiempo transcurrido en meses entre
  -- la fecha de constritucion de la empresa y la
  -- fecha actual sea mayor o igual al parametro Cotrafa

  FUNCTION ValidarConstitucion(NiPerId IN NUMBER) RETURN VARCHAR2 IS

    CURSOR CConstitucion IS
      SELECT b.Fecha_Constitucion
        FROM TS_PER_PERSONA_OCUPACIONES a, TS_PERSONAS b
       WHERE a.Per_Id = NiPerId AND a.Per_Id_Vinculado_a = b.Id AND
             a.Atipocu_Codigo = '4' AND a.Fecha_Final IS NULL;

    DConstitucion    TS_PERSONAS.Fecha_Constitucion%TYPE;
    NMeses           NUMBER;
    VResConstitucion VARCHAR(2);
  BEGIN
    OPEN CConstitucion;
    FETCH CConstitucion
      INTO DConstitucion;
    IF CConstitucion%NOTFOUND THEN
      VResConstitucion := 'NO';
    ELSE
      IF DConstitucion IS NOT NULL THEN
        NMeses := TRUNC(MONTHS_BETWEEN(SYSDATE, DConstitucion));
        -- pk_conseres.param_gnral('GMESES_CONSTITUCION')
        -- se cambia por pk_conseres.param_gnral('GTIEMPO_CONSTITUCION')
        -- modificado por la unificación de los paramgetros generales

        IF NMeses < Pk_Conseres.Param_Gnral('GTIEMPO_CONSTITUCION') THEN
          -- se debe reportar
          VResConstitucion := 'SI';
        ELSE
          VResConstitucion := 'NO';
        END IF;
      ELSE
        VResConstitucion := 'NO';
      END IF;
    END IF;
    CLOSE CConstitucion;
    RETURN(VResConstitucion);
  END ValidarConstitucion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion determina el valor minimo que debe recibir de la nomina el cliente

  FUNCTION ValMinimoNominaCliente(RiIngresos     IN Pks_Proc_Solicitudes.TIngresos,
                                  NCuotaAhorro   IN NUMBER,
                                  VIncluyeAhorro IN VARCHAR2) RETURN NUMBER IS

    NValMinimo NUMBER;

  BEGIN

    IF VIncluyeAhorro = 'NO' THEN
      NValMinimo := (RiIngresos.Ingresos_Basicos +
                    RiIngresos.Conceptos_Fijos) -
    --40784 JNARANJO 20160412 Adiciono nIncluyeSegSocial para determinar si se incluye o no la seguridad social                    
                    --(RiIngresos.Deduccion_Nomina - NCuotaAhorro);
                    (RiIngresos.Deduccion_Nomina+(nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial) - NCuotaAhorro);
    ELSE
      NValMinimo := (RiIngresos.Ingresos_Basicos +
                    RiIngresos.Conceptos_Fijos) -
                    --40784 JNARANJO 20160412 Adiciono nIncluyeSegSocial para determinar si se incluye o no la seguridad social
                    --(RiIngresos.Deduccion_Nomina);
                    (RiIngresos.Deduccion_Nomina+(nIncluyeSegSocial)*calcSeguridadSocial(RiIngresos.PerId,RiIngresos.Consecutivo_Servicio,RiIngresos.FechaInicial));
    END IF;
    RETURN(ROUND(NValMinimo));
  END ValMinimoNominaCliente;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Esta funcion obtiene el total de las cuotas de una solicitud

  FUNCTION ValorCuotaSolicitud(NiConseAsesoria IN NUMBER) RETURN NUMBER IS

    CURSOR CCuota IS
      
      -- 24717 (M) 07/04/2011 JNARANJO 
      --SELECT SUM(DECODE(SUBSTR(Ase.Ser_Codigo, 1, 6),'001.03',NVL(Ase.Crcuota_Total_Pago, 0),NVL(Ase.Cuota_Periodo_Pago, 0))) Cuota_Periodo_Pago
      SELECT SUM(DECODE(aservic.aservic_type,'AROTATI', NVL(aservic.rocuota_base_solicitada,0),
                                             'ACREDIT', NVL(aservic.crcuota_total_pago,0),NVL(aservic.cuota_periodo_pago,0))) Cuota_Periodo_Pago
      -- 24717 (M) 07/04/2011 Fin modificación 
        FROM TS_ASE_SERVICIOS aservic
       WHERE aservic.Ase_Consecutivo_Asesoria = NiConseAsesoria;

    --
    -- Fecha modificación : 03/09/04 04:05:56 p.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Modificado por sugerencia de marylu, ya que debe tomar todos los servicios de la asesoría
    --
    /*
               AND  ase.tipo_estado_decision IN (PK_CONSERES.sec_tipo_codigo(1264,'2.01'),
                                                 PK_CONSERES.sec_tipo_codigo(1264,'2.05'),
                                                 PK_CONSERES.sec_tipo_codigo(1264,'2.08'));
    */

    NTotalCuota NUMBER;

  BEGIN
    OPEN CCuota;
    FETCH CCuota
      INTO NTotalCuota;
    CLOSE CCuota;
    RETURN(ROUND(NVL(NTotalCuota, 0)));
  END ValorCuotaSolicitud;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 30/03/2004 11:02:00 a.m.
  -- Verifica si el cliente esta asociado a un convenio de deducción de nómina
  -- en estado APROBADO o SUSPENDIDO
  -- teniendo el id de la persona o id del convenio

  FUNCTION VerificarConvenio(NiPer_Id IN TS_PERSONAS.ID%TYPE -- Id de la persona
                             ) RETURN VARCHAR2 IS

    CURSOR CConvenio IS
      SELECT Con.Estado_Actual
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu, TS_CONVENIOS Con
       WHERE Pperocu.Con_Deduccion_Nomina = Con.Id AND
             SYSDATE BETWEEN Pperocu.Fecha_Inicial AND
             NVL(Pperocu.Fecha_Final, SYSDATE) AND
             Pperocu.Per_Id = NiPer_Id;

    VEstado TS_CONVENIOS.ESTADO_ACTUAL%TYPE;

  BEGIN
    OPEN CConvenio;
    FETCH CConvenio
      INTO VEstado;
    CLOSE CConvenio;
    IF VEstado IS NOT NULL THEN
      IF VEstado IN ('APROBA', 'SUSPEN') THEN
        RETURN 'SI';
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END VerificarConvenio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- verificar endeudamiento nivel 1

  --Modifica Jorge Naranjo 20090707 Requerimiento 21437 Cambio nombre de parámetro
  --FUNCTION VerificarEndeudNivel1(NiConsec_Servic IN NUMBER) RETURN VARCHAR2 IS
  FUNCTION VerificarEndeudNivel1( niConsAsesoria IN NUMBER) RETURN VARCHAR2 IS
    NPEndeudaMax       NUMBER;
    NNivelEndeudActual NUMBER;
    NNivelEndeudNuevo  NUMBER;
    --    grAseServicioCre      Pks_proc_solicitudes.rAseServicioCre;

    --Modifica Jorge Naranjo 20090707 Requerimiento 21437
    CURSOR cServicios IS
      SELECT aservic.consecutivo_servicio,aservic.aforpag_nombre,aperson.modelo
      FROM ts_ase_servicios aservic,ts_ase_personas aperson
      WHERE aservic.ase_consecutivo_asesoria = niConsAsesoria
        AND aservic.consecutivo_servicio = aperson.aservic_consecutivo_servicio
        AND aperson.aperson_type = 'SOLICI'
        AND ( (SUBSTR(aservic.ser_codigo,1,6) IN ('001.01', '001.03', '001.04')) OR
              (SUBSTR(aservic.ser_codigo,1,9) IN ('001.02.02')) )
      -- 25732 (M) 11/05/2011 JNARANJO
      --ORDER BY DECODE(aservic.aservic_type,'ACREDIT',1,'AAFILIA',2,'ARECAUD',3,'AAHORRO',4,5) DESC ;
      ORDER BY DECODE(aservic.aservic_type,'ACREDIT',1,'AROTATI',2,'AAFILIA',3,'ARECAUD',4,'AAHORRO',5,6) DESC ;
      -- 25732 11/05/2011 Fin modificación       
    nConsServicio NUMBER;--Consecutivo de servicio
    vModelo VARCHAR2(10);--Almacena el modelo utilizado
    vFormaPago VARCHAR2(10);--Almacena la forma de pago
    --Fin modificación 20090707 Requerimiento 21437
  BEGIN
    --Modifica Jorge Naranjo 20090707 Requerimiento 21437
    OPEN cServicios;
    FETCH cServicios INTO nConsServicio,vFormaPago,vModelo;
    CLOSE cServicios;
    GrAseServicioCre := ParGenAseServicio(nConsServicio);
    IF nConsServicio IS NOT NULL THEN
    /*
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(NiConsec_Servic);
    END IF;
    --      grAseServicioCre:= Pks_proc_solicitudes.ParGenAseServicio(niConsec_servic);
    IF SUBSTR(GrAseServicioCre.Ser_Codigo, 1, 6) IN ('001.01', '001.03', '001.04') THEN
    */
    --Fin modificación 20090707 Requerimiento 21437
      --se determina el pocercentaje de endeudamiento que se debe aplicar para la cuota maxima
      /*NPEndeudaMax       := PorcentajeEndeudamiento(NiConsec_Servic);
      NNivelEndeudActual := ParGenAsePersonas(NiConsec_Servic,
                                              'endeudamiento_actual_i');
      NNivelEndeudNuevo  := ParGenAsePersonas(NiConsec_Servic,
                                              'endeudamiento_nuevo_i');
      */
      NPEndeudaMax       := PorcentajeEndeudamiento( nConsServicio,
                                                     vModelo );
      NNivelEndeudActual := ParGenAsePersonas( nConsServicio,
                                              'endeudamiento_actual_i' );
      NNivelEndeudNuevo  := ParGenAsePersonas( nConsServicio,
                                              'endeudamiento_nuevo_i' );
      --Fin modificación 20090707 Requerimiento 21437
      --rastro('vModelo:'||vModelo||'  NPEndeudGlobal:'||NPEndeudGlobal,'ENDNIVEL2');

      -- 25402 (M) 30/03/2011 JNARANJO Sólo debe comparar contra el nuevo endeudamiento
      /*IF (NNivelEndeudActual > NPEndeudaMax) OR
         (NNivelEndeudNuevo > NPEndeudaMax) THEN*/
      IF NNivelEndeudNuevo > NPEndeudaMax THEN
      -- 25402 30/03/2011 Fin modificación
        RETURN 'SI';
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END VerificarEndeudNivel1;


  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 05/04/2004 03:48:00 p.m.
  -- Verificar endeudamiento Básico
  --Modifica Jorge Naranjo 20090707 Requerimiento 21437 Cambio nombre de parámetro
  --FUNCTION VerificarEndeudNivel2(NiConsec_Servic IN NUMBER) RETURN VARCHAR2 IS
  FUNCTION VerificarEndeudNivel2( niConsAsesoria IN NUMBER) RETURN VARCHAR2 IS

    NNivelEndeudActual NUMBER;
    NNivelEndeudNuevo  NUMBER;
    NPEndeudGlobal     NUMBER := Pk_Conseres.Param('GPORENDGLOBAL');

    --    grAseServicioCre      Pks_proc_solicitudes.rAseServicioCre;
    --Modifica Jorge Naranjo 20090707 Requerimiento 21437
    CURSOR cServicios IS
      SELECT aservic.consecutivo_servicio,aservic.aforpag_nombre,aperson.modelo
      FROM ts_ase_servicios aservic,ts_ase_personas aperson
      WHERE aservic.ase_consecutivo_asesoria = niConsAsesoria
        AND aservic.consecutivo_servicio = aperson.aservic_consecutivo_servicio
        AND aperson.aperson_type = 'SOLICI'
        AND ( (SUBSTR(aservic.ser_codigo,1,6) IN ('001.01', '001.03', '001.04')) OR
              (SUBSTR(aservic.ser_codigo,1,9) IN ('001.02.02')) )
      -- 25732 (M) 11/05/2011 JNARANJO
      --ORDER BY DECODE(aservic.aservic_type,'ACREDIT',1,'AAFILIA',2,'ARECAUD',3,'AAHORRO',4,5) DESC ;
      ORDER BY DECODE(aservic.aservic_type,'ACREDIT',1,'AROTATI',2,'AAFILIA',3,'ARECAUD',4,'AAHORRO',5,6) DESC ;
      -- 25732 11/05/2011 Fin modificación 
    nConsServicio NUMBER;--Consecutivo de servicio
    vModelo VARCHAR2(10);--Almacena el modelo utilizado
    vFormaPago VARCHAR2(100);
    --Fin modificación 20090707 Requerimiento 21437
    -- 25211 (M) 08/03/2011 JNARANJO 
    CURSOR cRiesgoEndeudamiento( niArieend IN NUMBER ) IS
      SELECT arieend.endeudamiento_global
      FROM ts_adm_riesgo_endeudamientos arieend
      WHERE arieend.id = niArieend;
    rRiesgoEndeudamiento cRiesgoEndeudamiento%ROWTYPE;
    trResultadoRiesgos pks_cpl0436.VResulRiesgos;
    nCantVariables PLS_INTEGER;
    nArieendId PLS_INTEGER;
    -- 25211 08/03/2011 Fin modificación
    -- 25402 (A) 30/03/2011 JNARANJO 
    CURSOR cServicioCredito( niConsAsesoria IN NUMBER ) IS
      SELECT aservic.consecutivo_servicio
      FROM ts_ase_servicios aservic
      WHERE aservic.ase_consecutivo_asesoria = niConsAsesoria 
        AND aservic.aservic_type = 'ACREDIT'
        AND SUBSTR(aservic.ser_codigo,1,6) = '001.03';
    rServicioCredito cServicioCredito%ROWTYPE;
    -- 25402 30/03/2011 Fin modificación     
  BEGIN
    --Modifica Jorge Naranjo 20090707 Requerimiento 21437
    OPEN cServicios;
    FETCH cServicios INTO nConsServicio,vFormaPago,vModelo;
    CLOSE cServicios;
    GrAseServicioCre := ParGenAseServicio(nConsServicio);
    IF nConsServicio IS NOT NULL THEN
      -- 25402 (A) 30/03/2011 JNARANJO 
      OPEN cServicioCredito(GrAseServicioCre.ase_consecutivo_asesoria);
      FETCH cServicioCredito INTO rServicioCredito;
      CLOSE cServicioCredito;
      -- 25402 30/03/2011 Fin modificación 
      -- 25211 (M) 08/03/2011 JNARANJO 
      pks_cpl0436.obtener_variables( NiSer => rServicioCredito.Consecutivo_Servicio,
                                     ViSistema =>  'ASESOR', 
                                     VoResulRiesgos =>  trResultadoRiesgos,
                                     NoNroVar =>  nCantVariables );
      IF NVL(nCantVariables,0) > 0 THEN
        IF trResultadoRiesgos(1).NIdRiesgo IS NOT NULL THEN
          pks_cpl0436.verificar_politica_endeuda( niConsServicio => rServicioCredito.Consecutivo_Servicio,
                                                  niArieanaId => trResultadoRiesgos(1).NIdRiesgo,
                                                  noArieendId => nArieendId );
        END IF;
      END IF;
      -- 25211 08/03/2011 Fin modificación   

      -- 25211 (M) 08/03/2011 JNARANJO 
      IF nArieendId IS NOT NULL THEN
        OPEN cRiesgoEndeudamiento(nArieendId);
        FETCH cRiesgoEndeudamiento INTO rRiesgoEndeudamiento;
        CLOSE cRiesgoEndeudamiento;
        NPEndeudGlobal := rRiesgoEndeudamiento.endeudamiento_global;
      ELSE
        IF vModelo = 'NOMINA' THEN
         NPEndeudGlobal := Pk_Conseres.Param('GPORENDGLOBAL');
        ELSIF vModelo = 'DEBITO' THEN
          NPEndeudGlobal := Pk_Conseres.Param('GPORENDGLOBALPAGOPER');
        END IF;        
      END IF;
      -- 25211 08/03/2011 Fin modificación
    /*
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(NiConsec_Servic);
    END IF;

    --      grAseServicioCre:= Pks_proc_solicitudes.ParGenAseServicio(niConsec_servic);
    IF SUBSTR(GrAseServicioCre.Ser_Codigo, 1, 6) IN
       ('001.01', '001.03', '001.04') THEN
      --se determina el pocercentaje de endeudamiento que se debe aplicar para la cuota maxima
      NNivelEndeudActual := ParGenAsePersonas(NiConsec_Servic,
                                              'endeudamiento_actual_ii');
      NNivelEndeudNuevo  := ParGenAsePersonas(NiConsec_Servic,
                                              'endeudamiento_nuevo_ii');
      */
      NNivelEndeudActual := ParGenAsePersonas( nConsServicio,
                                               'endeudamiento_actual_ii' );
      NNivelEndeudNuevo  := ParGenAsePersonas( nConsServicio,
                                               'endeudamiento_nuevo_ii' );
      --Fin modificación 20090707 Requerimiento 21437
      rastro('vModelo:'||vModelo||' NPEndeudGlobal:'||NPEndeudGlobal||' NNivelEndeudActual:'||NNivelEndeudActual||' NNivelEndeudNuevo'||NNivelEndeudNuevo,'ENDNIVEL2');
      -- 25402 (M) 30/03/2011 JNARANJO Sólo debe comparar contra el nuevo endeudamiento
      /*IF (NNivelEndeudActual > NPEndeudGlobal) OR
         (NNivelEndeudNuevo > NPEndeudGlobal) THEN*/
      IF NNivelEndeudNuevo > NPEndeudGlobal THEN
      -- 25402 30/03/2011 Fin modificación
        RETURN 'SI';
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END VerificarEndeudNivel2;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 15/04/2004 11:02:00 a.m.
  -- Verifica si el cliente se ha retidado como asociado de la cooperativa

  FUNCTION VerificarRetiro(NiPer_Id IN TS_PERSONAS.ID%TYPE) RETURN VARCHAR2 IS
    --
    -- Fecha: 25/08/04 10:04:31 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar cual es el último servicio de afiliación
    --
    CURSOR CAfiliacion IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Tipo_Estado_Decision =
             Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') AND
             Aservic.Ser_Codigo = '001.01.00.001' AND
             Aservic.Per_Id = NiPer_Id;

    CURSOR CRetirado IS
      SELECT Per.f_Acepta_Reingreso
        FROM TS_PERSONAS Per
       WHERE Per.Id = NiPer_Id;

    CURSOR CTipoRetiro(NiTipoRetiro NUMBER) IS
      SELECT Nombre
        FROM CON_TIPOS
       WHERE Sec = NiTipoRetiro;

    DFReingreso DATE;
    NDias       NUMBER(15);
    NTipoRet    NUMBER(5);
    DFecRetiro  DATE;
    VResultado  VARCHAR2(1000);

  BEGIN
    NDias      := 0;
    VResultado := 'NO';

    OPEN CAfiliacion;
    FETCH CAfiliacion
      INTO NDias;
    CLOSE CAfiliacion;

    IF NDias = 0 THEN
      -- Si no tiene servicio de afiliacion vigente.
      OPEN CRetirado;
      FETCH CRetirado
        INTO DFReingreso;
      CLOSE CRetirado;

      Pks_Genpersonas.UltimoRetiro(NiPer_Id, DFecRetiro, NTipoRet);

      IF DFecRetiro IS NOT NULL THEN
        -- Si se encuentran retiros
        NDias := Pks_Generales.DiaSuspension(NTipoRet);

        IF DFReingreso IS NULL THEN
          DFReingreso := DFecRetiro + NDias;
        END IF;

        OPEN CTipoRetiro(NTipoRet);
        FETCH CTipoRetiro
          INTO VResultado;
        CLOSE CTipoRetiro;

        VResultado := 'Presenta retiro tipo ' || VResultado ||
                      ', puede reingresar desde el ' ||
                      TO_CHAR(DFReingreso, 'DD-MM-YYYY');
      END IF;
    END IF;

    RETURN VResultado;
  END VerificarRetiro;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 26/03/2004 01:30:00 p.m.
  -- Esta funcion determina si la calificacion de la empresa
  -- es aceptada por la cooperativa

  FUNCTION VerificaCalificacion(NiIdPer IN NUMBER) RETURN VARCHAR2 IS

    CURSOR CPer IS
      SELECT Per.Calificacion
        FROM TS_PERSONAS Per, TS_PER_PERSONA_OCUPACIONES Pperocu
       WHERE Per.Id = Pperocu.Per_Id_Vinculado_a AND
             TRUNC(SYSDATE) BETWEEN Pperocu.Fecha_Inicial AND
             NVL(Pperocu.Fecha_Final, TRUNC(SYSDATE))
            --           AND TRUNC(SYSDATE) <= NVL(pperocu.vencimiento_contrato,TRUNC(SYSDATE))
             AND Pperocu.Per_Id = NiIdPer AND
             Pperocu.Atipocu_Codigo IN ('1', '2', '3')
      UNION
      SELECT Per.Calificacion
        FROM TS_PERSONAS Per
       WHERE Per.Per_Type = 'PJURID' AND Per.Id = NiIdPer;

    NCal NUMBER;

  BEGIN
    OPEN CPer;
    FETCH Cper
      INTO NCal;
    CLOSE CPer;

    IF NCal < Pk_Conseres.Param_Gnral('GCALMINIMAEMP') THEN
      RETURN 'SI';
    ELSE
      RETURN 'NO';
    END IF;

  END VerificaCalificacion;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 19/04/2004 11:02:00 a.m.
  -- Verifica si el empleador tiene restringidos los créditos con pago personal
  -- para la fecha de asesoría

  FUNCTION VerificarCreRestringidos(NiConsSevic IN TS_ASE_SERVICIOS.CONSECUTIVO_SERVICIO%TYPE -- Consecutivo del Servicio
                                    ) RETURN VARCHAR2 IS

    CURSOR CPagos IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic, TS_PER_PAGO_PERSONALES Ppagper,
             TS_PER_PERSONA_OCUPACIONES Pperocu
       WHERE (SYSDATE BETWEEN Ppagper.Fecha_Inicial AND
             NVL(Ppagper.Fecha_Final, SYSDATE)) AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             Aservic.Aforpag_Nombre <> 'NOMINA' AND
             Pperocu.Per_Id_Vinculado_a = Ppagper.Per_Id AND
             Pperocu.Id = Aservic.Pperocu_Id AND
             Aservic.Consecutivo_Servicio = NiConsSevic;

    --
    -- Fecha modificación : 04-10-2004 10:02:13 a.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Se adiciono la restricción de créditos, ya que la validación la hacia para cualquier servicio (Maria Eugenia Jaramillo)
    --
    NCont NUMBER(2);

  BEGIN
    OPEN CPagos;
    FETCH CPagos
      INTO NCont;
    CLOSE CPagos;
    IF NVL(NCont, 0) > 0 THEN
      RETURN 'SI';
    ELSE
      RETURN 'NO';
    END IF;
  END VerificarCreRestringidos;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 19/04/2004 08:02:00 a.m.
  -- Se verifica si el cliente posee solicitudes de crédito en
  -- estado de decisión diferente a "SERVICIO EFECTIVO", "SERVICIO ANULADO" ó "SERVICIO RECHAZADO"

  FUNCTION VerificarEstadoCredito(NiPerId         IN NUMBER,
                                  NiConsec_Asesor IN NUMBER) RETURN VARCHAR2 IS
    CURSOR CSolicitudCredito IS
      SELECT 1
        FROM TS_ASE_SERVICIOS a
       WHERE a.Tipo_Estado_Decision NOT IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.22'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.13')) AND
             a.Tipo_Estado_Decision IS NOT NULL AND
             SUBSTR(a.Ser_Codigo, 1, 6) = '001.03' AND
             a.Consecutivo_Servicio <> NiConsec_Asesor AND
             a.Per_Id = NiPerId;

    NResultado    NUMBER;
    VResSolicitud VARCHAR2(2);
  BEGIN
    OPEN CSolicitudCredito;
    FETCH CSolicitudCredito
      INTO NResultado;
    IF CSolicitudCredito%NOTFOUND THEN
      VResSolicitud := 'NO';
    ELSE
      -- se debe reportar
      VResSolicitud := 'SI';
    END IF;
    CLOSE CSolicitudCredito;
    RETURN(VResSolicitud);
  END;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 19/04/2004 08:02:00 a.m.
  -- Verifica si la línea cumple para la forma de pago

  -- Autor      : Fredy Sanchez Castro
  -- Modificado : 11-Jul-2005
  -- Se agrega la validacion para el recaudo a tercero

  FUNCTION VerificaFormaPago(NiConsec_Servic TS_ASE_SERVICIOS.CONSECUTIVO_SERVICIO%TYPE -- Consecutivo servicio
                             ) RETURN VARCHAR2 IS

    NCont NUMBER(2);

    CURSOR c_Forma_Pago_Convenio IS
      SELECT COUNT(1)
        FROM TS_CNV_FORMA_PAGOS
       WHERE Aforpag_Nombre = GrAseServicioCre.Aforpag_Nombre AND
             Con_Id IN
             (SELECT Con_Id
                FROM TS_SERVICIOS
               WHERE Codigo = GrAseServicioCre.Ser_Codigo);

  BEGIN
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(NiConsec_Servic);
    END IF;

    IF SUBSTR(GrAseServicioCre.Ser_Codigo, 1, 9) NOT IN
       ('001.02.02', '001.02.01', '001.02.04') THEN
      -- Ahorro a termino
      IF SUBSTR(GrAseServicioCre.Ser_Codigo, 1, 6) = '001.04' THEN
        OPEN c_Forma_Pago_Convenio;
        FETCH c_Forma_Pago_Convenio
          INTO NCont;
        CLOSE c_Forma_Pago_Convenio;
        IF NVL(NCont, 0) > 0 THEN
          RETURN 'NO'; -- Si cumple con la condición
        ELSE
          RETURN 'SI'; -- No cumple con la condición
        END IF;
      ELSE
        NCont := ConfigServicio('''' || GrAseServicioCre.Ser_Codigo || '''',
                                'VS_SER_FORMA_PAGOS',
                                'vista.forma_pago = ' || '''' ||
                                GrAseServicioCre.Aforpag_Nombre || '''',
                                'A');
        IF NVL(NCont, 0) > 0 THEN
          RETURN 'NO'; -- Si cumple con la condición
        ELSE
          RETURN 'SI'; -- No cumple con la condición
        END IF;
      END IF;
    ELSE
      RETURN 'NO'; -- Si cumple con la condición
    END IF;
  END VerificaFormaPago;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 15/04/2004 11:02:00 a.m.
  -- Verifica si posee Cartera castigada

  FUNCTION VerCarteraCastigada(NiPer_Id IN TS_PERSONAS.ID%TYPE -- Id de la persona
                               ) RETURN VARCHAR2 IS

    CURSOR CPersonas IS
      SELECT Ainterf.Cartera_Castigada
        FROM TS_ASE_INTERFAZ Ainterf
       WHERE Ainterf.Per_Id = NiPer_Id;

    VResult VARCHAR2(2);

  BEGIN
    OPEN CPersonas;
    FETCH CPersonas
      INTO VResult;
    CLOSE CPersonas;
    IF VResult IS NULL THEN
      RETURN 'NO';
    ELSIF VResult = 'SI' THEN
      RETURN 'SI';
    ELSE
      RETURN 'NO';
    END IF;
  END VerCarteraCastigada;

  FUNCTION FecReingreso(NiConsecServic IN TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_FecReingreso

      PROPOSITO:
      Verifica la fecha de reingreso

      PARAMETROS:
      niConsecServic IN ts_ase_servicios.consecutivo_servicio%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      14/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

      REQUISITOS:
      Servicio de Aportes sociales


    ****************************************************************************************************/
    --
    -- Fecha: 14/05/2004 12:38:18 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar la fecha de reingreso a la cooperativa
    --
    CURSOR CIdPersona IS
      SELECT Per_Id PId, Ser_Codigo
        FROM TS_ASE_SERVICIOS
       WHERE Consecutivo_Servicio = NiConsecServic;

    CURSOR CFecReingreso(NIdPersona NUMBER) IS
      SELECT Per.f_Acepta_Reingreso
        FROM TS_PERSONAS Per
       WHERE Per.Id = NIdPersona;

    VResultado    VARCHAR2(2) := 'NO';
    DFecReingreso DATE; -- Utilizada para guardar la fecha de reingreso
    DFecRetiro    DATE; -- Utilizada para guardar la fecha de retiro
    RIdPersona    CIdPersona%ROWTYPE;
    NTipRetiro    NUMBER(5);
    NDias         NUMBER(15);

  BEGIN
    OPEN CIdPersona;
    FETCH CIdPersona
      INTO RIdPersona;
    CLOSE CIdPersona;

    OPEN CFecReingreso(RIdPersona.PId);
    FETCH CFecReingreso
      INTO DFecReingreso;
    CLOSE CFecReingreso;

    IF DFecReingreso IS NULL THEN
      Pks_Genpersonas.UltimoRetiro(RIdPersona.PId, DFecRetiro, NTipRetiro);

      IF DFecRetiro IS NOT NULL THEN
        NDias := Pks_Generales.DiaSuspension(NTipRetiro);

        DFecReingreso := DFecRetiro + NDias;
      END IF;
    END IF;
    --
    -- Req. 10433
    -- Fecha modificación : 28/03/2006 07:23:59 p.m.
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : modifica para que tome el trunc de la fecha de retiro, ya que
    -- si el retiro se hace el mismo dia no permite el reingreso inmediato
    IF DFecReingreso IS NOT NULL AND TRUNC(SYSDATE) < TRUNC(DFecReingreso) AND
       RIdPersona.Ser_Codigo = '001.01.00.001' THEN
      VResultado := 'SI';
    END IF;
    --
    -- Fin Req. 10433

    -- Esta funcion trabaja con logica inversa por ser utilizada como condicion.

    RETURN VResultado;
  END FecReingreso;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 15/04/2004 11:02:00 a.m.
  -- Verifica si posee Proceso jurídicos

  FUNCTION ProcesoJuridico(NiPer_Id IN TS_PERSONAS.ID%TYPE -- Id de la persona
                           ) RETURN VARCHAR2 IS

    CURSOR CPersonas IS
      SELECT Ainterf.Proceso_Juridico
        FROM TS_ASE_INTERFAZ Ainterf
       WHERE Ainterf.Per_Id = NiPer_Id;

    VResult VARCHAR2(2);

  BEGIN
    OPEN CPersonas;
    FETCH CPersonas
      INTO VResult;
    CLOSE CPersonas;
    IF VResult IS NULL THEN
      RETURN 'NO';
    ELSIF VResult = 'SI' THEN
      RETURN 'SI';
    ELSE
      RETURN 'NO';
    END IF;
  END ProcesoJuridico;

  FUNCTION VerificarSalario(NiId               IN TS_PER_PERSONA_OCUPACIONES.Id%TYPE,
                            NiIngresos_Basicos IN NUMBER,
                            ViPeriodoPago      IN VARCHAR2 DEFAULT 'MENSUAL',
                            viFormaPago IN VARCHAR2 DEFAULT NULL)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_VerificarSalario

      PROPOSITO:
      Determinar si los ingresos básicos son iguales al salario de la empresa seleccionada

      PARAMETROS:
      niPer_id IN ts_personas.id%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      06/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa
      31-10-2006  JHGUTIER  1.1.1 Debe recibir el parámetro de la forma de pago
                                  para evaluar como ingresos básicos la suma de
                                  todos los ingresos vigentes si no es nómina y
                                  sólo el que se pasa de PPEROCU si el nómina

      REQUISITOS:
      Tener el per_id de la empresa en la cual labora el cliente




    ****************************************************************************************************/
    Result VARCHAR2(2);
    NCont  NUMBER(2, 0) := 0; -- Utilizada para determinar si el salario es igual al ingresado

    -- Obtención del número de días del período de pago deseado
    CURSOR cuAtipper IS
      SELECT atipper.numero_dias
      FROM TS_ADM_TIPO_PERIODOS atipper
      WHERE atipper.nombre = ViPeriodoPago;
    nNumeroDias NUMBER;

    -- Rango de ingresos para usar en el cursor principal
    nIngresoBasicoIni NUMBER;
    nIngresoBasicoFin NUMBER;

/*  JHGUTIER 31-10-2006. Obtiene el per_id de la ocupación pasada */
    CURSOR cuPperocu IS
      SELECT pperocu.per_id, pperocu.con_deduccion_nomina
      FROM TS_PER_PERSONA_OCUPACIONES pperocu
      WHERE pperocu.id = nIid ;
    nPerId NUMBER;
    nConded NUMBER;
    --
    -- Fecha: 06/05/2004 11:50:13 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si los ingresos básicos son iguales
    -- al salario matrículado
    --
    CURSOR CVerificarSalCnv IS
      SELECT COUNT(1)
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu,
             TS_PER_OCU_INGRESO_EGRESOS Pocuing
       WHERE Pocuing.Fecha_Final IS NULL AND
             Pperocu.Id = Pocuing.Pperocu_Id AND Pperocu.Id = NiId AND
             Pocuing.Ingresos_Basicos NOT BETWEEN nIngresoBasicoIni
               AND nIngresoBasicoFin;

    --
    -- Fecha: 31-10-2006
    -- Usuario: JHGUTIER
    -- Determina el ingreso básico de todas las ocupaciones vigentes
    --
    CURSOR CVerificarSalOcu IS
      SELECT 1
      FROM TS_PER_PERSONA_OCUPACIONES pperocu
        , TS_PER_OCU_INGRESO_EGRESOS pocuing
      WHERE Pocuing.Fecha_Final IS NULL
        AND Pperocu.Id = Pocuing.Pperocu_Id
        AND Pperocu.per_Id = nPerId
      HAVING SUM(Pocuing.Ingresos_Basicos) NOT BETWEEN nIngresoBasicoIni
        AND nIngresoBasicoFin;

BEGIN
    /*  JHGUTIER 31-10-2006. Mejora de legibiliodad. Estos se calculan antes y se
        pasan al cursor            */
    OPEN cuAtipper;
      FETCH cuAtipper INTO nNumeroDias;
    CLOSE cuAtipper;
    nIngresoBasicoIni := ((NiIngresos_Basicos / nNumeroDias) * 30) - 10;
    nIngresoBasicoFin := ((NiIngresos_Basicos / nNumeroDias) * 30) + 10;

    -- Determinar si la ocupación pasada a la función tiene convenio asociado
    IF viFormaPago IS NULL OR viFormaPago != 'NOMINA' THEN
      OPEN cuPperocu;
        FETCH cuPperocu INTO nPerId, nConded;
      CLOSE cuPperocu;
    END IF;

    -- JHGUTIER 31-10-006. Evalúa cuál cursor lanza de acuerdo a forma de pago

    -- Fecha modificación : 10/12/2006 15:25:13
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Solo debe tener en cuenta un solo salario si es por nomina y tiene convenio

    IF viFormaPago IS NOT NULL AND viFormaPago = 'NOMINA' AND NVL(nConded,0) <> 0 THEN
      OPEN CVerificarSalCnv;
      FETCH CVerificarSalCnv
        INTO NCont;
      CLOSE CVerificarSalCnv;
    ELSE
      OPEN CVerificarSalOcu;
      FETCH CVerificarSalOcu
        INTO NCont;
      CLOSE CVerificarSalOcu;
    END IF;

    IF NVL(NCont, 0) > 0 THEN
      Result := 'SI';
    ELSE
      Result := 'NO';
    END IF;
    RETURN(Result);
  END VerificarSalario;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 15/04/2004 11:02:00 a.m.
  -- Verifica si el cliente tiene servicios en estado de decision aprobado

  FUNCTION VerificarServiciosAprobados(NiPer_Id IN TS_PERSONAS.ID%TYPE -- Id de la persona
                                       ) RETURN VARCHAR2 IS

    CURSOR CAprobados IS
      SELECT Ser.Nombre
        FROM TS_ASE_SERVICIOS Aservic, Vs_Tipo_Estado_Decision Testdec,
             TS_SERVICIOS Ser
       WHERE Aservic.Per_Id = NiPer_Id AND Aservic.Ser_Codigo = Ser.Codigo AND
             Aservic.Tipo_Estado_Decision = Testdec.Sec AND
             Testdec.Codigo = '2.05'; --Estado aprobado;

    VServicio VARCHAR2(50);

  BEGIN
    OPEN CAprobados;
    FETCH CAprobados
      INTO VServicio;
    CLOSE CAprobados;
    IF VServicio IS NOT NULL THEN
      RETURN 'SI';
    ELSE
      RETURN 'NO';
    END IF;
  END VerificarServiciosAprobados;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 19/04/2004 11:02:00 a.m.
  -- Funcion para determinar que servicios pueden ir dentro de una solicitud

  FUNCTION ValidaServicSol(NiConsec_Asesor IN NUMBER --Consecutivo de la asesoria
                           ) RETURN VARCHAR2 IS

    --- Verifica si hay servicios que no se encuentran configurados en ningún grupo

    CURSOR CSin_Grupo IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservi1
       WHERE Aservi1.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             NOT EXISTS
       (SELECT Asolser.Ser_Codigo
                FROM TS_ADM_SOLICITUD_SERVICIOS Asolser,
                     Vs_Tipo_Grupo_Solicitudes Tgrusol
               WHERE Tgrusol.Sec = Asolser.Tipo_Grupo_Solicitud AND
                     Tgrusol.Esta_Activo = 'S' AND
                     INSTR(Aservi1.Ser_Codigo, Asolser.Ser_Codigo, 1, 1) <> 0);

    -- Grupos actuales

    CURSOR CGrupos IS
      SELECT DISTINCT Asolser.Tipo_Grupo_Solicitud Grupo
        FROM TS_ADM_SOLICITUD_SERVICIOS Asolser,
             Vs_Tipo_Grupo_Solicitudes Tgrusol
       WHERE Tgrusol.Sec = Asolser.Tipo_Grupo_Solicitud AND
             Tgrusol.Esta_Activo = 'S';

    -- Numero de servicios que contiene la asesoria que pertenece al grupo
    -- y número maximo de servicios configurado

    CURSOR CCantidad(VGrupo IN NUMBER) IS
      SELECT Asolser.Ser_Codigo, Asolser.Tipo_Grupo_Solicitud Grupo,
             Asolser.Numero_Maximo, COUNT(1) Cantidad
        FROM TS_ADM_SOLICITUD_SERVICIOS Asolser,
             Vs_Tipo_Grupo_Solicitudes Tgrusol, TS_ASE_SERVICIOS Aservic
       WHERE Tgrusol.Sec = Asolser.Tipo_Grupo_Solicitud AND
             Tgrusol.Esta_Activo = 'S' AND
             INSTR(Aservic.Ser_Codigo, Asolser.Ser_Codigo, 1, 1) <> 0 AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             Asolser.Tipo_Grupo_Solicitud = VGrupo
       GROUP BY Asolser.Ser_Codigo, Asolser.Tipo_Grupo_Solicitud,
                Asolser.Numero_Maximo;

    --- Verifica si hay servicios que no se encuentran configurados en el grupo

    CURSOR CFuera_Grupo(VGrupo IN NUMBER) IS
      SELECT 1
        FROM TS_ASE_SERVICIOS Aservi1
       WHERE Aservi1.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             NOT EXISTS
       (SELECT Asolser.Ser_Codigo
                FROM TS_ADM_SOLICITUD_SERVICIOS Asolser,
                     Vs_Tipo_Grupo_Solicitudes Tgrusol
               WHERE Tgrusol.Sec = Asolser.Tipo_Grupo_Solicitud AND
                     Tgrusol.Esta_Activo = 'S' AND
                     Asolser.Tipo_Grupo_Solicitud = VGrupo AND
                     INSTR(Aservi1.Ser_Codigo, Asolser.Ser_Codigo, 1, 1) <> 0);

    VResp VARCHAR2(2);
    NCont NUMBER;

  BEGIN
    VResp := 'SI';
    OPEN CSin_Grupo;
    FETCH CSin_Grupo
      INTO NCont;
    CLOSE CSin_Grupo;
    IF NVL(NCont, 0) > 0 THEN
      VResp := 'SI';
    ELSE
      FOR RGrupo IN CGrupos LOOP
        NCont := 0;
        OPEN CFuera_Grupo(RGrupo.Grupo);
        FETCH CFuera_Grupo
          INTO NCont;
        CLOSE CFuera_Grupo;
        IF NVL(NCont, 0) = 0 THEN
          FOR RCant IN CCantidad(RGrupo.Grupo) LOOP
            IF Rcant.Numero_Maximo < RCant.Cantidad THEN
              VResp := 'SI';
              EXIT;
            END IF;
            VResp := 'NO';
          END LOOP;
        END IF;
      END LOOP;
    END IF;
    RETURN(VResp);
  END ValidaServicSol;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 27/04/2004 01:02:00 p.m.
  -- Verificar si la asignación del título es por oficina,
  -- es un documento controlado utilizado para el control de título
  -- y no se poseen documentos en estado "Documento asignado
  -- a la oficina"

  FUNCTION VerificarDctoControlado(NiConsec_Asesor IN TS_ASESORIAS.Consecutivo_Asesoria%TYPE -- Id de la persona
                                   ) RETURN VARCHAR2 IS

    NCont NUMBER;

    CURSOR CServicios IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE SUBSTR(Aservic.Ser_Codigo, 1, 6) <> '001.02' AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    CURSOR CControl IS
      SELECT COUNT(1)
        FROM VS_SER_DOCUMENTOS Sdocume, TS_DOCUMENTOS Doc,
             TS_ASE_SERVICIOS Aservic
       WHERE Doc.Controlado = 'SI' AND Sdocume.Doc_Id = Doc.Id AND
             Sdocume.Ser_Codigo = Aservic.Ser_Codigo AND
             Sdocume.Control_Titulo = 'SI' AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

    CURSOR CDocto IS
      SELECT COUNT(1)
        FROM VS_SER_DOCUMENTOS Sdocume, TS_DOCUMENTOS Doc,
             TS_DOC_CONTROLADOS Dcontro, TS_DOC_CONT_UNIDADES Dconuni,
             TS_ASE_SERVICIOS Aservic
       WHERE Dconuni.Adepen_Id = Aservic.Adepen_Asignada AND
             Dconuni.Tipo_Estado_Doc =
             Pk_Conseres.Sec_Tipo_Codigo('791', 'ASIOFI') AND
             Dcontro.Doc_Id = Dconuni.Dcontr_Doc_Id AND
             Dcontro.Nro_Cupones = Dconuni.Dcontr_Nro_Cupones AND
             Doc.Id = Dcontro.Doc_Id AND Doc.Controlado = 'SI' AND
             Sdocume.Doc_Id = Doc.Id AND
             Sdocume.Ser_Codigo = Aservic.Ser_Codigo AND
             Sdocume.Control_Titulo = 'SI' AND
             Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor;

  BEGIN
    OPEN CServicios;
    FETCH CServicios
      INTO NCont;
    CLOSE CServicios;
    IF NVL(NCont, 0) = 0 THEN
      OPEN CControl;
      FETCH CControl
        INTO NCont;
      CLOSE CControl;
      IF NVL(NCont, 0) > 0 THEN
        OPEN CDocto;
        FETCH CDocto
          INTO NCont;
        CLOSE CDocto;
        IF NVL(NCont, 0) = 0 THEN
          RETURN 'SI';
        ELSE
          RETURN 'NO';
        END IF;
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END VerificarDctoControlado;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 15/04/2004 11:02:00 a.m.
  -- Verifica si el sexo corresponde al configurado por el numero de identificacion

  FUNCTION VerificarSexo(NiPer_Id IN TS_PERSONAS.ID%TYPE -- Id de la persona
                         ) RETURN VARCHAR2 IS

    CURSOR CPersonas IS
      SELECT Per.Identificacion, Per.Tipo_Identificacion, Per.Sexo
        FROM TS_PERSONAS Per
       WHERE Per.Id = NiPer_Id;

    VIdent  TS_PERSONAS.Identificacion%TYPE;
    VTipid  TS_PERSONAS.Tipo_Identificacion%TYPE;
    VSex    TS_PERSONAS.Sexo%TYPE;
    VNueSex TS_PERSONAS.Sexo%TYPE;
    VResult VARCHAR2(2000);
    VSexo1  VARCHAR2(15);
    VSexo2  VARCHAR2(15);
  BEGIN
    OPEN CPersonas;
    FETCH CPersonas
      INTO VIdent, VTipid, VSex;
    CLOSE CPersonas;
    IF VIdent IS NOT NULL THEN
      VNueSex := Pks_Genpersonas.ObtenerSexoPerNatural(VTipid, VIdent);
      IF VNueSex <> VSex THEN
        IF VSex = 'FEMENI' THEN
          VSexo1 := 'Femenino';
        ELSE
          VSexo1 := 'Masculino';
        END IF;
        IF VNuesex = 'FEMENI' THEN
          VSexo2 := 'Femenino';
        ELSE
          VSexo2 := 'Masculino';
        END IF;
        VResult := 'Sexo actual ' || VSexo1 || ' sexo asignado ' || VSexo2;
        RETURN VResult;
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END VerificarSexo;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 11:02:00 a.m.
  -- Verifica si el tipo de persona es permitida para el servicio

  FUNCTION VerificarTipoPersona(NiConsec_Servic TS_ASE_SERVICIOS.CONSECUTIVO_SERVICIO%TYPE)
    RETURN VARCHAR2 IS

    VTipoPersona VARCHAR2(240);
    VNombreVista VARCHAR2(50);
    VTipoPer     VARCHAR2(6);
    VServicio    VARCHAR2(20);

    CURSOR CPersonas IS
      SELECT Aservic.Ser_Codigo, Per.Per_Type
        FROM TS_ASE_SERVICIOS Aservic, TS_PERSONAS Per
       WHERE Aservic.Per_Id = Per.Id AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

  BEGIN

    OPEN CPersonas;
    FETCH CPersonas
      INTO VServicio, VTipoPer;
    CLOSE CPersonas;
    IF VServicio IS NULL THEN
      RETURN 'NO';
    ELSE
      IF SUBSTR(VServicio, 1, 6) IN ('001.02', '001.03', '001.01') THEN
        --se verfica que el tipo de persona la linea lo permita
        -- 24717 (M) JNARANJO 12/04/2011 Se adiciona comportamiento para servicio Rotativo
        --IF SUBSTR(VServicio, 1, 6) = '001.03' THEN
        IF SUBSTR(VServicio, 1, 9) = '001.03.04' THEN
          VNombreVista := 'VS_SER_CRE_ROTATIVOS';
        ELSIF SUBSTR(VServicio, 1, 6) = '001.03' THEN
        -- 24717 12/04/2011 
          VNombreVista := 'VS_SER_CREDITOS';
        ELSIF SUBSTR(VServicio, 1, 6) = '001.02' THEN
          VNombreVista := 'VS_SER_AHORROS';
        ELSIF SUBSTR(VServicio, 1, 6) = '001.01' THEN
          VNombreVista := 'VS_SER_AFILIACIONES';
        END IF;
        VTipoPersona := Pks_Servicios.Parametro(VServicio,
                                                VNombreVista,
                                                'TIPO_PERSONA',
                                                SYSDATE);
        IF VTipoPersona <> 'AMBAS' THEN
          IF VTipoPersona <> VTipoPer THEN
            RETURN 'SI'; -- no cumple la condición
          ELSE
            RETURN 'NO';
          END IF;
        ELSE
          RETURN 'NO';
        END IF;
      ELSE
        RETURN 'NO';
      END IF;
    END IF;
  END VerificarTipoPersona;

  PROCEDURE Borrar_Servicio(NiConsec_Servic IN NUMBER, -- Consecutivo del servicio a ser borrado
                            VoMsjTec        OUT VARCHAR2,
                            VoMsjUsu        OUT VARCHAR2,
                            BoExito         OUT BOOLEAN) IS
    /****************************************************************************************************
      OBJETO: FS_Borrar_servicio

      PROPOSITO:
      Borrar todos los hijos de un sevicio

      PARAMETROS:
      niConsec_servic IN NUMBER -- Consecutivo del servicio a ser borrado

      HISTORIAL
             Fecha                          Usuario                 Versión           Descripción
      13/07/04 08:49:16 a.m.        Alberto Eduardo Sánchez B.       1.0.0       1. Creacion de Borrar_servicio

      REQUISITOS:
      Que el servicio se encuentre en un estado diferente a efectivo




    ****************************************************************************************************/

    -- Variables locales utilizadas en el proceso
    VEst_Servicio VARCHAR2(5); -- Utilizada para determinar el estado del servicio
    VEst_Asesoria VARCHAR2(5); -- Utilizada para determinar el estado de la asesoria
    nConsAsesoria PLS_INTEGER;--41208 JNARANJO 20170407
    vModalidad VARCHAR2(50);--41208 JNARANJO 20170407
    --
    -- Fecha: 13/07/04 08:53:13 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar que se puedan borrar los hijos del servicio
    --
    CURSOR CServicio IS
      SELECT Aservic.Tipo_Estado_Decision, Ase.Aestcon_Codigo,
        aservic.ase_consecutivo_asesoria,--41208 JNARANJO 20170407
        (SELECT ser.modalidad_cartera FROM ts_servicios ser WHERE ser.codigo = aservic.ser_codigo) modalidad --41208 JNARANJO 20170407
        FROM TS_ASE_SERVICIOS Aservic, TS_ASESORIAS Ase
       WHERE Aservic.Ase_Consecutivo_Asesoria = Ase.Consecutivo_Asesoria AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;
  --41208 JNARANJO 20170407
  CURSOR cAperson ( niConsAse IN NUMBER ) IS
    SELECT s.consecutivo_servicio
    FROM ts_ase_servicios s
    WHERE s.ase_consecutivo_asesoria = niConsAse;
  --41208 JNARANJO 20170407
  
  BEGIN
    OPEN CServicio;
    FETCH CServicio
      INTO VEst_Servicio, VEst_Asesoria, nConsAsesoria, vModalidad; --41208 JNARANJO 20170407 agrego nConsAsesoria, vModalidad
    CLOSE CServicio;
    --Modifica Jorge Naranjo 20101001 Requerimiento 24357
    --IF VEst_Asesoria = '1.01' THEN
    IF VEst_Asesoria IS NOT NULL THEN
      -- Radicada
      --VoMsjUsu := 'No es posible borrar servicios de una solicitud rádicada';
      VoMsjUsu := 'No es posible borrar servicios de una solicitud con estado de control.';
      --Fin modificación 20101001
      BoExito  := FALSE;
    ELSE
      --Modifica Jorge Naranjo 20101001 Requerimiento 24357
      --IF VEst_Servicio = Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08') THEN
      --  VoMsjUsu := 'No es posible borrar servicios que se encuentran efectivos';
      IF VEst_Servicio IS NOT NULL THEN
        VoMsjUsu := 'No es posible borrar servicios con estado de decisión.';
      --Fin modificación 20101001
        BoExito  := FALSE;
      ELSE
        UPDATE TS_DOC_CONT_UNIDADES
           SET Apaservic_Consecutivo_Servicio = NULL, Aperson_Per_Id = NULL,
               Aperson_Fecha_Inicial = NULL,
               Tipo_Estado_Doc = Pk_Conseres.Sec_Tipo_Codigo('791', 'ASIOFI')
         WHERE Apaservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_DOCUMENTOS
         WHERE Apaservic_Consecutivo_Servicio = NiConsec_Servic;
        --Modifica Jorge Naranjo 20090606 Requerimiento 21437
        DELETE FROM ts_ase_persona_conceptos apercon
        WHERE apercon.apaservic_consecutivo_servicio = NiConsec_Servic;
        --Fin modificación 20090606 Requerimiento 21437
        --41208 JNARANJO 20170407
        IF NVL(vModalidad,'NULO') = 'HIPOTE' THEN
          FOR rAperson IN cAperson( nConsAsesoria ) LOOP
            UPDATE TS_ASE_PERSONAS ap
            SET ap.deudor_solidario = 'NO'
            WHERE ap.aservic_consecutivo_servicio = rAperson.Consecutivo_Servicio;
          END LOOP;
        END IF;
        --41208 JNARANJO 20170407 Fin modificación
        DELETE FROM TS_ASE_PERSONAS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_DECISIONES
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_AHO_PAGO_NOMINAS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_AHO_PROYECCIONES
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_AHO_SOBREGIROS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_AHO_INTERESES
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_CONDICION_CUOTAS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        /*Modifica Jorge Naranjo 20100202 Requerimiento 23005 DEbe borrarse despues de TS_ASE_COMPONENTE_CUOTAS
        DELETE FROM TS_ASE_CRE_COBRO_ADICIONALES
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
         Fin modificación 20100202 Requerimiento 23005*/
        DELETE FROM TS_ASE_CRE_COD_NUMEROS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_CRE_CUOTA_EXTRAS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_CRE_DESEMBOLSOS
         WHERE Aservic_Consecutivo_Credito = NiConsec_Servic;
        DELETE FROM TS_ASE_CRE_DES_RESTRICTIVOS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_CRE_REESTRUCTURACIONES
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_COMPONENTE_CUOTAS
         WHERE Acaservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_CUOTAS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        /*Modifica Jorge Naranjo 20100202 Requerimiento 23005 DEbe borrarse despues de TS_ASE_COMPONENTE_CUOTAS*/
        DELETE FROM TS_ASE_CRE_COBRO_ADICIONALES
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        --Fin modificación 20100202 Requerimiento 23005
        /*Modifica Jorge Naranjo 20100202 Requerimiento 23005 DEbe borrarse TS_ASE_DEBITO_MODIFICACIONES*/
        DELETE FROM TS_ASE_DEBITO_MODIFICACIONES dm
        WHERE dm.adaservic_consecutivo_servicio = NiConsec_Servic;
        --Fin modificación 20100202 Requerimiento 23005
        DELETE FROM TS_ASE_DEBITO_AUTOMATICOS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_NOTA_CREDITOS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_NO_VIABLES
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        --DELETE FROM TS_ASE_REC_AUX_BENEFICIARIOS
        --    WHERE aservic_consecutivo_servicio = niConsec_servic;
        DELETE FROM TS_ASE_ROTATIVO_CUPOS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_ASE_SER_RESTRINGIDOS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_GAR_ESTUDIOS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_SEGUROS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        DELETE FROM TS_SEG_VEHICULOS
         WHERE Aservic_Consecutivo_Servicio = NiConsec_Servic;
        --Modifica Jorge Naranjo 20101110 Requerimiento 24547 La tabla no tenía foreign key
        DELETE FROM TS_ASE_SARC_VARIABLES asarcva
        WHERE asarcva.aservic_consecutivo_servicio = NiConsec_Servic;
        --Fin modificación 20101110
      END IF;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      Vomsjtec := SQLERRM;
      Pks_Garantia_Real.CASE_EXCEPTION('Borrar_servicio',
                                       'Error en Borrar_servicio',
                                       SQLCODE,
                                       Vomsjtec,
                                       Boexito);
      VoMsjUsu := 'Se presentó problemas en Borrar_servicio, verifique el LOG';
  END Borrar_Servicio;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 27/04/2004 03:48:00 p.m.
  -- Verifica si el crédito requiere codeudor

  FUNCTION Requierecodeudor(NiConsec_Servic IN NUMBER) RETURN VARCHAR2 IS

    CURSOR CServicios IS
      SELECT 1
        FROM VS_SER_CREDITOS Vsercre, TS_ASE_SERVICIOS Aservic
       WHERE Vsercre.Requiere_Codeudor = 'SI' AND
             Vsercre.Ser_Codigo = Aservic.Ser_Codigo AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

    NResultado NUMBER;

  BEGIN
    OPEN CServicios;
    FETCH CServicios
      INTO NResultado;
    CLOSE CServicios;
    IF NVL(NResultado, 0) > 0 THEN
      RETURN 'SI'; -- se debe reportar
    ELSE
      RETURN 'NO';
    END IF;
  END Requierecodeudor;

  FUNCTION Linea_Activa(NiConsec_Asesor IN NUMBER) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: Linea activa

      PROPOSITO:
      Determinar si al momento de radicar se presenta una línea que ya no se encuentra vigente, se detiene el proceso y se informa al usuario

      PARAMETROS:
      niConsec_asesor IN NUMBER

      AUTOR:
      Alberto Eduardo Sánchez B.
      26/08/04 02:50:56 p.m.

      HISTORIAL
      Fecha               Usuario       Version      Descripcion
      26/08/04            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa


    ****************************************************************************************************/
    Result VARCHAR2(200);

    --
    -- Fecha: 26/08/04 02:52:19 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar la vigencia de una linea
    --
    CURSOR CLinea IS
      SELECT Ser.Nombre Nombre
        FROM TS_SERVICIOS Ser, TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConsec_Asesor AND
             Ser.Codigo = Aservic.Ser_Codigo AND
             SYSDATE NOT BETWEEN Ser.Fecha_Inicio AND
             NVL(Ser.Fecha_Final, SYSDATE);

  BEGIN
    FOR CLin IN CLinea LOOP
      IF CLin.Nombre IS NOT NULL THEN
        Result := Result || ' ' || CLin.Nombre;
      END IF;
    END LOOP;
    IF Result IS NULL THEN
      Result := 'NO';
    END IF;
    RETURN(Result);
  END Linea_Activa;

  FUNCTION Estado_Servicio(NiPer_Id IN NUMBER) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: Estado_Servicio

      PROPOSITO:
      Recupera información para determinar el estado del servicio

      PARAMETROS:
      niPer_id IN NUMBER

      AUTOR:
      Alberto Eduardo Sánchez B.
      27/08/04 02:22:27 p.m.

      HISTORIAL
      Fecha               Usuario       Version      Descripcion
      27/08/04            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa


    ****************************************************************************************************/
    VResult VARCHAR2(2000);

    --
    -- Fecha: 27/08/04 02:23:34 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si posee servicios en estados determinados
    --
    CURSOR CServicios IS
      SELECT Ser.Nombre Servicio, Ctipos.Nombre Estado
        FROM TS_ASE_SERVICIOS Aservic, TS_SERVICIOS Ser, CON_TIPOS Ctipos
       WHERE Aservic.Tipo_Estado_Decision = Ctipos.Sec AND
             Aservic.Ser_Codigo = Ser.Codigo AND Aservic.Per_Id = NiPer_Id AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.05'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.12'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.09'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.24'))
       ORDER BY 2;

     --
     -- Req. 19723
     -- Fecha modificación : 03/04/2008 02:42:20 p.m.
     -- Usuario modifica   : Alberto Eduardo Sánchez B.
     -- Causa modificación : Se modifica para que no incluya en la validación las cuentas inactivas o inactivas trasladadas al tesoro nacional
     --         Pk_Conseres.Sec_Tipo_Codigo(1264, '2.20'),
     --         Pk_Conseres.Sec_Tipo_Codigo(1264, '2.21'),
     --
     -- Fin Req. 19723

  BEGIN
    FOR CSer IN CServicios LOOP
      IF CSer.Servicio IS NOT NULL THEN
        IF VResult IS NULL THEN
          VResult := 'El cliente posee solicitudes con estado ' ||
                     CSer.Estado || ' para ' || CSer.Servicio;
        ELSE
          VResult := VResult || ' ' || CSer.Estado || ' para ' ||
                     CSer.Servicio;
        END IF;
      END IF;
    END LOOP;
    RETURN(VResult);
  END Estado_Servicio;

  FUNCTION Codeudor_Radicar(NiConsec_Servic IN NUMBER) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: Codeudor_Radicar

      PROPOSITO:
      Realizar las validaciones necesarias sobre los codeudores al momento de radicar

      PARAMETROS:
      niConsec_servic IN NUMBER

      AUTOR:
      Alberto Eduardo Sánchez B.
      30/08/04 03:59:20 p.m.

      HISTORIAL
      Fecha               Usuario       Version      Descripcion
      30/08/04            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa


    ****************************************************************************************************/
    Result        VARCHAR2(2);
    BDetener_Proc BOOLEAN;
    VMsjtec       VARCHAR2(2000);
    VMsjUsu       VARCHAR2(2000);
    BExito        BOOLEAN;
    BCumple       BOOLEAN;

    --
    -- Fecha: 30/08/04 04:00:48 p.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para identificar los codeudores que intervienen en un crédito
    --
    CURSOR CCodeudor IS
      SELECT Aperson.Per_Id, Aservic.Consecutivo_Servicio,
             Aservic.Ase_Consecutivo_Asesoria
        FROM TS_ASE_PERSONAS Aperson, TS_ASE_SERVICIOS Aservic
       WHERE Aperson.Aperson_Type = 'CRECOD' AND
             Aperson.Aservic_Consecutivo_Servicio =
             Aservic.Consecutivo_Servicio AND
             Aservic.Consecutivo_Servicio = NiConsec_Servic;

  BEGIN
    FOR CCod IN CCodeudor LOOP
      EvaluarCondicion('170',
                       CCod.Per_Id,
                       CCod.Ase_Consecutivo_Asesoria,
                       BDetener_Proc,
                       VMsjtec,
                       VMsjUsu,
                       BExito,
                       BCumple,
                       CCod.Consecutivo_Servicio);
      IF BDetener_Proc THEN
        Result := 'SI';
      ELSE
        Result := 'NO';
      END IF;
    END LOOP;
    RETURN(Result);
  END Codeudor_Radicar;

  FUNCTION Datos_Convenio(NiConsec_Asesor IN NUMBER) RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: Datos_Convenio

      PROPOSITO:
      validación al radicar, cuando la persona no presenta código interno ni centro de costos y se está asesorando un servicio por un convenio que los requiere

      PARAMETROS:
      Name IN type, Name IN type, ...

      AUTOR:
      Alberto Eduardo Sánchez B.
      31/08/04 03:09:25 p.m.

      HISTORIAL
      Fecha               Usuario       Version      Descripcion
      31/08/04            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa

    ****************************************************************************************************/
    VResult VARCHAR2(2);
    NCont   NUMBER;

    --
    -- Req. 7597
    -- Fecha modificación : 09/11/2005 11:11:11
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Esta intentando evaluar el centro de costos y debe ser el centro de costos de la empresa

    CURSOR CDeduccion IS
      SELECT 1
        FROM TS_CNV_DEDUCCIONES Cdeduci, TS_PER_PERSONA_OCUPACIONES Pperocu,
             TS_ASESORIAS Ase
       WHERE SYSDATE BETWEEN Cdeduci.Fecha_Inicio AND
             NVL(Cdeduci.Fecha_Final, SYSDATE) AND
             ((Cdeduci.Requiere_Registro_Interno = 'SI' AND
             Pperocu.Registro IS NULL) OR
             (Cdeduci.Centro_Costo = 'SI' AND
             Pperocu.Centro_Costo_Empresa IS NULL)) -- pperocu.centro_costo
             AND Pperocu.Con_Deduccion_Nomina = Cdeduci.Con_Id AND
             Ase.Pperocu_Id = Pperocu.Id AND Ase.Aforpag_Nombre = 'NOMINA' AND
             Ase.Consecutivo_Asesoria = NiConsec_Asesor;

    --
    -- Fin Req. 7597

  BEGIN
    OPEN CDeduccion;
    FETCH CDeduccion
      INTO NCont;
    CLOSE CDeduccion;
    IF NVL(NCont, 0) > 0 THEN
      VResult := 'SI';
    ELSE
      VResult := 'NO';
    END IF;
    RETURN(VResult);
  END Datos_Convenio;

  FUNCTION Ver_Salario(NiId               IN TS_PER_PERSONA_OCUPACIONES.Id%TYPE,
                       NiIngresos_Basicos IN NUMBER,
                       ViPeriodoPago      IN VARCHAR2 DEFAULT 'MENSUAL',
                       viFormaPago IN VARCHAR2 DEFAULT NULL)
    RETURN VARCHAR2 IS
    /****************************************************************************************************
      OBJETO: FS_VerificarSalario

      PROPOSITO:
      Determinar si los ingresos básicos son iguales al salario de la empresa seleccionada

      PARAMETROS:
      niPer_id IN ts_personas.id%TYPE

      HISTORIAL
      Fecha                 Usuario      Version      Descripcion
      06/05/2004            ASANCHEZ      1.0.0        1. Creacion de la unidad de programa
      26/10/2006  JHGUTIER  1.1.1   Si la ocupación pasada como parámetros está
                                    adscrita a un convenio se obtiene el salario
                                    de esta ocupación. Si no tiene convenio se
                                    suman todas las ocupaciones del cliente.
      31/10/2006  JHGUTIER  1.1.2   Para evaluar si la ocupación pasada era la
                                    única que se iba a tomar para los ingresos
                                    se evalúa la forma de pago. Si ésta es
                                    NOMINA entonces se toma sólo esa sino se
                                    suman todas las ocupaciones del cliente.


      20090601  JNARANJO
      Si la forma de pago del servicio es por deducción de NOMINA se registra el ingreso que posee el cliente en la colilla según el periodo de pago. Se verifica
      que el ingreso básico ingresado por el usuario (convertido a mensual para la comparación), sea igual al que posee el cliente en la hoja de vida por la
      ocupación seleccionada (se encuentra almacenado en término mensuales en ts_per_ocu_ingreso_egresos), en caso contrario se alerta al usuario
      "El ingreso básico que acaba de registrar es diferente al registrado en la hoja de vida ($999,999,999)".

      Si la forma de pago del servicio es por CAJA o DEBITO se realiza la misma validación con respecto al salario, pero tomando todos los salarios que posea
      el cliente en sus ocupaciones vigentes. En caso contrario se alerta al usuario "El ingreso básico que acaba de registrar es diferente a la sumatoria de
      los salarios vigentes que tiene registrados en la hoja de vida ($999,999,999)".

     --Según la función si el salario registrado en la hoja de vida es igual, devuelve 'NO'; si es diferente devuelve el valor del salario. En el llamado desde
     --la forma de asesorías, si se ha ingresado un valor menor a 10 se asigna al campo ingresos_basicos el valor del salario, de lo contrario se deja el valor ingresado

      REQUISITOS:
      Tener el per_id de la empresa en la cual labora el cliente
    ****************************************************************************************************/
    Result VARCHAR2(20);
    NCont  NUMBER(20) := 0; -- Utilizada para determinar si el salario es igual al ingresado

    --Modifica Jorge Naranjo 20090813 Requerimiento 21437
    -- Obtención del número de días del período de pago deseado
    CURSOR cuAtipper IS
      SELECT atipper.numero_dias
      FROM TS_ADM_TIPO_PERIODOS atipper
      WHERE atipper.nombre = ViPeriodoPago;
    nNumeroDias NUMBER;
    --Fin modificación 20090813 Requerimiento 21437

    -- Rango de ingresos para usar en el cursor principal
    nIngresoBasicoIni NUMBER;
    nIngresoBasicoFin NUMBER;

/*  JHGUTIER 31-10-2006. Obtiene el per_id de la ocupación pasada */
    CURSOR cuPperocu IS
      SELECT pperocu.per_id, pperocu.con_deduccion_nomina
      FROM TS_PER_PERSONA_OCUPACIONES pperocu
      WHERE pperocu.id = nIid
        AND NVL(pperocu.fecha_final,SYSDATE+1) > SYSDATE;--Modifica Jorge Naranjo 20090529 Requerimiento 21437 Adiciono condición NVL(pperocu.fecha_final,SYSDATE+1) > SYSDATE
    nPerId NUMBER;
    nConded NUMBER;

    --
    -- Fecha: 06/05/2004 11:50:13 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si los ingresos básicos son iguales al salario matrículado
    -- Modificación: JHGUTIER 27-10-206 Si la ocupación está conectada a un
    -- convenio se toman los ingresos de dicha ocupación solamente
    CURSOR CVerificarSalSiCNV IS
      SELECT Pocuing.Ingresos_Basicos
        FROM TS_PER_PERSONA_OCUPACIONES Pperocu,
             TS_PER_OCU_INGRESO_EGRESOS Pocuing
       WHERE SYSDATE BETWEEN pocuing.fecha_inicial AND NVL(pocuing.fecha_final, SYSDATE)
         AND Pperocu.Id = Pocuing.Pperocu_Id
         AND Pperocu.Id = NiId
         AND NVL(pperocu.fecha_final,SYSDATE+1) > SYSDATE AND--Modifica Jorge Naranjo 20090529 Requerimiento 21437 Adiciono condición NVL(pperocu.fecha_final,SYSDATE+1) > SYSDATE
             Pocuing.Ingresos_Basicos NOT BETWEEN nIngresoBasicoIni AND nIngresoBasicoFin;

/*  JHGUTIER 27-10-2006. Mejora de legibiliodad. Estos se calculan antes y se
    pasan al cursor
             (((NiIngresos_Basicos /
             (SELECT Atipper.Numero_Dias
                   FROM TS_ADM_TIPO_PERIODOS Atipper
                  WHERE Atipper.Nombre = ViPeriodoPago)) * 30) - 10) AND
             (((NiIngresos_Basicos /
             (SELECT Atipper.Numero_Dias
                   FROM TS_ADM_TIPO_PERIODOS Atipper
                  WHERE Atipper.Nombre = ViPeriodoPago)) * 30) + 10);*/

    /* JHGUTIER 27-10-206 Si la ocupación no está conectada a un convenio se
       suman los ingresos de todas las ocupaciones vigentes */
    CURSOR CVerificarSalNoCNV IS
      SELECT SUM(pocuing.ingresos_basicos)
      FROM TS_PER_PERSONA_OCUPACIONES pperocu
        , TS_PER_OCU_INGRESO_EGRESOS pocuing
      WHERE pperocu.per_id = nPerId
        AND SYSDATE BETWEEN pperocu.fecha_inicial AND NVL(pperocu.fecha_final, SYSDATE)
        AND SYSDATE BETWEEN pocuing.fecha_inicial AND NVL(pocuing.fecha_final, SYSDATE)
        AND pperocu.Id = pocuing.pperocu_Id
        AND Pperocu.Id = NiId ---50368 JMarquez 20180705 (A) Solo tomar la ocupación del servicio
        AND NVL(pperocu.fecha_final,SYSDATE+1) > SYSDATE--Modifica Jorge Naranjo 20090529 Requerimiento 21437 Adiciono condición NVL(pperocu.fecha_final,SYSDATE+1) > SYSDATE
      HAVING SUM(pocuing.ingresos_basicos) NOT BETWEEN ningresobasicoini AND nIngresoBasicoFin;

    -- Para ver si se van a sumar las ocupaciones o se toma la que se pasa en
    -- nIid
  BEGIN
    --Modifica Jorge Naranjo 20090813 Requerimiento 21437
    /*  JHGUTIER 27-10-2006. Mejora de legibiliodad. Estos se calculan antes y se pasan al cursor            */
    /*OPEN cuAtipper;
    FETCH cuAtipper INTO nNumeroDias;
    CLOSE cuAtipper;
    nIngresoBasicoIni := ((NiIngresos_Basicos / nNumeroDias) * 30) - 10;
    nIngresoBasicoFin := ((NiIngresos_Basicos / nNumeroDias) * 30) + 10;*/
    nIngresoBasicoIni := NVL(ROUND(pks_cpl0031.convertir_valor(NiIngresos_Basicos,ViPeriodoPago,'MENSUAL')),0) - 10;
    nIngresoBasicoFin := NVL(ROUND(pks_cpl0031.convertir_valor(NiIngresos_Basicos,ViPeriodoPago,'MENSUAL')),0) + 10;
    --Fin modificación 20090813 Requerimiento 21437

    -- Determinar si la ocupación pasada a la función tiene convenio asociado
    OPEN cuPperocu;
      FETCH cuPperocu INTO nPerId, nConded;
    CLOSE cuPperocu;

    -- Fecha modificación : 10/12/2006 15:25:13
    -- Usuario modifica   : Alberto Eduardo Sánchez B.
    -- Causa modificación : Solo debe tener en cuenta un solo salario si es por nomina y tiene convenio
    --Modifica Jorge Naranjo 20090601 Requerimiento 21437 Remplazo condición viFormaPago IS NOT NULL
    --IF viFormaPago IS NOT NULL AND viFormaPago = 'NOMINA' AND NVL(nConded,0) <> 0 THEN
    IF viFormaPago = 'NOMINA' AND NVL(nConded,0) != 0 THEN
      OPEN CVerificarSalSiCNV;
      FETCH CVerificarSalSiCNV INTO NCont;
      CLOSE CVerificarSalSiCNV;
    ELSE
      OPEN CVerificarSalNoCNV;
      FETCH CVerificarSalNoCNV INTO NCont;
      CLOSE CVerificarSalNoCNV;
    END IF;
    rastro('nPerId:'||nPerId||'  viFormaPago:'||viFormaPago||'  nConded:'||NVL(nConded,0)||'  NCont:'||NVL(NCont, 0),'VERSALARIO');
    IF NVL(NCont, 0) > 0 THEN
      Result := TO_CHAR(NCont);
    ELSE
      Result := 'NO';
    END IF;
    RETURN(Result);
  END Ver_Salario;


 -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 01/09/2004 03:48:00 p.m.
  -- Esta funcion verifica diferencias en cuotas de afiliacion

  FUNCTION Verificar_Cuota_Sol(NiConseAsesoria IN NUMBER,
                               NiIngresos      IN NUMBER) RETURN VARCHAR2 IS

    -- Autor : Alberto Eduardo Sánchez B.
    -- Creado : 01/09/2004 03:48:00 p.m.
    -- Este cursor retorna la información del solicitante

    CURSOR CInf_Actual IS
      SELECT Ase.Pperocu_Id, Ase.Atipper_Pago
        FROM TS_ASESORIAS Ase
       WHERE Ase.Consecutivo_Asesoria = NiConseAsesoria;

    RInf_Actual CInf_Actual%ROWTYPE;

    -- Autor : Alberto Eduardo Sánchez B.
    -- Creado : 01/09/2004 03:48:00 p.m.
    -- Este cursor retorna la información del servicio de afiliacion

    CURSOR CSer_Afiliacion(NiperocuId IN NUMBER) IS
      SELECT Aservic.Consecutivo_Servicio, Aservic.Ser_Codigo,
             Aservic.Cuota_Periodo_Pago, Aservic.Saldo, Ser.Nombre Servicio,
             Aservic.Per_Id, Aservic.Atipper_Pago, Per.Per_Type,
             Aservic.Fecha_Efectivo
        FROM TS_ASE_SERVICIOS Aservic, TS_PERSONAS Per, TS_ASESORIAS Ase,
             TS_SERVICIOS Ser
       WHERE Ase.Per_Id = Per.Id AND Ser.Codigo = Aservic.Ser_Codigo AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.01' AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.05'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08')) AND
             Aservic.Per_Id = Ase.Per_Id AND
             Ase.Consecutivo_Asesoria = NiConseAsesoria AND
             Aservic.Pperocu_Id = NiperocuId;

    -- Autor : Alberto Eduardo Sánchez B.
    -- Creado : 01/09/2004 03:48:00 p.m.
    -- Este cursor retorna la información de configuración de la línea

    CURSOR CConfiguracion(VCodigo IN VARCHAR2, VTipoPer IN VARCHAR2, NPer_Id IN NUMBER, DFec_Efectivo IN DATE) IS
      SELECT 1
        FROM VS_SER_AFI_CUOTAS Saficuo
       WHERE DFec_Efectivo BETWEEN Saficuo.f_Inicio_Afiliacion AND
             NVL(Saficuo.f_Final_Afiliacion, SYSDATE) AND
             Saficuo.Base_Porcentaje = 'SALARI' AND Ser_Codigo = VCodigo AND
             (NVL(Saficuo.Tipo_Persona, 'AMBAS') = VTipoPer OR
             NVL(Saficuo.Tipo_Persona, 'AMBAS') = 'AMBAS') AND
             (NVL(Saficuo.Tipo_Entidad, 'NULA'), NVL(Saficuo.Origen, 'NULA'),
              NVL(Saficuo.Radio_Accion, 'NULA'),
              NVL(Saficuo.Animo_Lucro, 'NULA'),
              NVL(Saficuo.Cooperativa, 'NULA')) =
             (SELECT NVL(Per.Tipo_Entidad, 'NULA'), NVL(Per.Origen, 'NULA'),
                     NVL(Per.Radio_Accion, 'NULA'),
                     NVL(Per.Animo_Lucro, 'NULA'),
                     NVL(Per.Cooperativa, 'NULA')
                FROM TS_PERSONAS Per
               WHERE Per.Id = NPer_Id);

    --
    -- Fecha: 01/09/04 11:24:58 a.m.
    -- Usuario: Alberto Eduardo Sánchez B.
    -- Recupera Informacion para determinar si el salod del cliente supera el maximo configurado
    --
    CURSOR CSaldo(NiCodigo IN VARCHAR2, NiSaldo IN NUMBER) IS
      SELECT 1
        FROM VS_SER_AFILIACIONES Safilia
       WHERE Safilia.Ser_Codigo = NiCodigo AND
             Safilia.Saldo_Maximo < NVL(Nisaldo, 0);

    CURSOR CPeriodo(ViPeriodoPago IN VARCHAR2) IS
      SELECT Atipper.Numero_Dias
        FROM TS_ADM_TIPO_PERIODOS Atipper
       WHERE Atipper.Nombre = ViPeriodoPago;

    CURSOR CPeriodo_Pago IS
      SELECT Aservic.Atipper_Pago
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ase_Consecutivo_Asesoria = NiConseAsesoria;

    NCont          NUMBER;
    GrAseServicios TS_ASE_SERVICIOS%ROWTYPE;
    NCuotaAfil     NUMBER := 0; -- Acumular las cutoas de afiliación con el nuevo salario
    VConsecutivos  VARCHAR2(2000); -- Consecutivos de afiliación a excluir de la suma total
    VResultado     VARCHAR2(2); -- verificar si posee el mismo salario
    NIngSal        NUMBER; --- Salario mensual
    NDias          NUMBER; -- Dias del período
    VPerpago       VARCHAR2(15);
    NDiasNuevo     NUMBER;

  BEGIN
    OPEN CInf_Actual;
    FETCH CInf_Actual
      INTO RInf_Actual;
    CLOSE CInf_Actual;
    VResultado := VerificarSalario(RInf_Actual.Pperocu_Id,
                                   NiIngresos,
                                   RInf_Actual.Atipper_Pago);
    NCuotaAfil := 0;
    IF VResultado = 'SI' THEN
      VConsecutivos := 'NO';
      FOR CSer IN CSer_Afiliacion(RInf_Actual.Pperocu_Id) LOOP
        NCont := 0;
        OPEN CSaldo(CSer.Ser_Codigo, CSer.Saldo);
        FETCH CSaldo
          INTO NCont;
        CLOSE CSaldo;
        IF NVL(NCont, 0) > 0 THEN
          VConsecutivos := 'El saldo del servicio ' || CSer.Servicio ||
                           ' supera el saldo maximo configurado';
        ELSE
          NCont := 0;
          OPEN CConfiguracion(CSer.Ser_Codigo,
                              CSer.Per_Type,
                              CSer.Per_Id,
                              CSer.Fecha_Efectivo);
          FETCH CConfiguracion
            INTO NCont;
          CLOSE CConfiguracion;
          IF NVL(NCont, 0) > 0 THEN
            OPEN CPeriodo(CSer.Atipper_Pago);
            FETCH CPeriodo
              INTO NDias;
            CLOSE CPeriodo;
            NIngSal := (NiIngresos / NDias) * 30;
            Pks_Afiliacion.Set_Salario_Asesoria(FALSE, NIngSal);
            --Modifica Jorge Naranjo 20100515 Requerimiento 23315
            --Pks_Afiliacion.Calcular_Cuota(CSer.Consecutivo_Servicio);
            Pks_Afiliacion.Calcular_Cuota(CSer.Consecutivo_Servicio,'SI');
            --Fin modificación 20100515
            GrAseServicios := Pks_Afiliacion.Get_Record;
            OPEN CPeriodo_Pago;
            FETCH CPeriodo_Pago
              INTO VPerpago;
            CLOSE CPeriodo_Pago;
            IF CSer.Atipper_Pago <> VPerpago THEN
              OPEN CPeriodo(VPerpago);
              FETCH CPeriodo
                INTO NDiasNuevo;
              CLOSE CPeriodo;
              NCuotaAfil := CEIL((NVL(GrAseServicios.Cuota_Periodo_Pago, 0) /
                                 NDiasNuevo) * NDias);
            ELSE
              NCuotaAfil := NVL(GrAseServicios.Cuota_Periodo_Pago, 0);
            END IF;
            IF NVL(CSer.Cuota_Periodo_Pago, 0) <> NVL(NCuotaAfil, 0) THEN
              --                      nCuotaAfil := NVL(cSer.cuota_periodo_pago,0) - NVL(grAseServicios.cuota_periodo_pago,0);
              IF VConsecutivos = 'NO' THEN
                --
                -- Fecha modificación : 28-09-2004 02:49:23 p.m.
                -- Usuario modifica   : Alberto Eduardo Sánchez B.
                -- Causa modificación : Sugerencia dada por Sandra Vargas para cambiar el mensaje
                --
                VConsecutivos := 'El servicio ' || CSer.Servicio ||
                                 ' cambiaría su cuota de $' ||
                                 TO_CHAR(NVL(CSer.Cuota_Periodo_Pago, 0) ||
                                         ' a $' ||
                                         TO_CHAR(NVL(NCuotaAfil, 0)));
                --                       vConsecutivos := 'El servicio de '||cSer.servicio||' presenta una diferencia de '||TO_CHAR(nCuotaAfil);
              ELSE
                --                       vConsecutivos := vConsecutivos||','||' el servicio de '||cSer.servicio||' presenta una diferencia de '||TO_CHAR(nCuotaAfil);
                VConsecutivos := VConsecutivos || ',' || 'El servicio ' ||
                                 CSer.Servicio ||
                                 ' cambiaría su cuota de $' ||
                                 TO_CHAR(NVL(CSer.Cuota_Periodo_Pago, 0) ||
                                         ' a $' ||
                                         TO_CHAR(NVL(NCuotaAfil, 0)));
              END IF;
            END IF;
          END IF;
        END IF;
      END LOOP;
    ELSE
      VConsecutivos := 'NO';
    END IF;
    RETURN(VConsecutivos);
  END Verificar_Cuota_Sol;


  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 28/04/2004 08:47:00 a.m.
  -- Verificar si por el cambio de convenio
  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 09:47:00 a.m.
  -- Verifica el número de crédtios por la misma línea incluyebdo restructurados y refinanciados.

  FUNCTION CreditosLineaRestr(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    NCont   NUMBER(3) := 0;
    NCon    NUMBER(3) := 0;
    VSerCod TS_ASE_SERVICIOS.Ser_Codigo%TYPE;
    NPer_Id TS_ASE_SERVICIOS.Per_Id%TYPE;
    NRest   NUMBER;

    CURSOR CCreditos IS
      SELECT Aservic.Ser_Codigo, Aservic.Per_Id
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic;

    CURSOR CServicios IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ser_Codigo = VSerCod AND Aservic.Per_Id = NPer_Id AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 9) <> '001.03.02' AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'));

    CURSOR CRestruc IS
      SELECT COUNT(1)
        FROM TS_ASE_CRE_REESTRUCTURACIONES Acreres, TS_ASE_SERVICIOS Aservic,
             TS_ASE_SERVICIOS Sactual
       WHERE Sactual.Consecutivo_Servicio = NiConsec_Servic AND
             Acreres.Aservic_Consecutivo_Servicio =
             Aservic.Consecutivo_Servicio AND Aservic.Ser_Codigo = VSerCod AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 9) <> '001.03.02' AND
             Aservic.Ase_Consecutivo_Asesoria =
             Sactual.Ase_Consecutivo_Asesoria AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'));

    CURSOR CPoliticas IS
      SELECT 1
        FROM VS_SER_CREDITOS Vsercre
       WHERE (Vsercre.Ser_Codigo = VserCod OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 9) OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 6)) AND
             Vsercre.Vigentes_Igual_Line > NCont
       ORDER BY Vsercre.Ser_Codigo, SUBSTR(Vsercre.Ser_Codigo, 1, 9),
                SUBSTR(Vsercre.Ser_Codigo, 1, 6);

  BEGIN
    OPEN CRestruc;
    FETCH CRestruc
      INTO NRest;
    CLOSE CRestruc;
    OPEN CCreditos;
    FETCH CCreditos
      INTO VSerCod, NPer_Id;
    CLOSE CCreditos;
    IF SUBSTR(VSerCod, 1, 6) = '001.03' THEN
      OPEN CServicios;
      FETCH CServicios
        INTO NCont;
      CLOSE CServicios;
      NCont := NCont - NVL(NRest, 0);
      OPEN CPoliticas;
      FETCH CPoliticas
        INTO NCon;
      CLOSE CPoliticas;
      IF NVL(NCon, 0) = 0 THEN
        RETURN 'SI';
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END CreditosLineaRestr;

  -- Autor : Alberto Eduardo Sánchez B.
  -- Creado : 20/04/2004 09:47:00 a.m.
  -- Verifica el número de créditos por otra línea incluyendo restructuración y refinanciación.

  FUNCTION CreditosOtrasLineaRestr(NiConsec_Servic TS_ASE_SERVICIOS.Consecutivo_Servicio%TYPE)
    RETURN VARCHAR2 IS

    NCont   NUMBER(3);
    NCon    NUMBER(3);
    VSerCod TS_ASE_SERVICIOS.Ser_Codigo%TYPE;
    NPer_Id TS_ASE_SERVICIOS.Per_Id%TYPE;
    NRest   NUMBER;

    CURSOR CCreditos IS
      SELECT Aservic.Ser_Codigo, Aservic.Per_Id
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Consecutivo_Servicio = NiConsec_Servic;

    CURSOR CServicios IS
      SELECT COUNT(1)
        FROM TS_ASE_SERVICIOS Aservic
       WHERE Aservic.Ser_Codigo <> VSerCod AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 9) <> '001.03.02' AND
             Aservic.Per_Id = NPer_Id AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'));

    CURSOR CRestruc IS
      SELECT COUNT(1)
        FROM TS_ASE_CRE_REESTRUCTURACIONES Acreres, TS_ASE_SERVICIOS Aservic,
             TS_ASE_SERVICIOS Sactual
       WHERE Sactual.Consecutivo_Servicio = NiConsec_Servic AND
             Acreres.Aservic_Consecutivo_Servicio =
             Aservic.Consecutivo_Servicio AND Aservic.Ser_Codigo <> VSerCod AND
             SUBSTR(Aservic.Ser_Codigo, 1, 6) = '001.03' AND
             SUBSTR(Aservic.Ser_Codigo, 1, 9) <> '001.03.02' AND
             Aservic.Ase_Consecutivo_Asesoria =
             Sactual.Ase_Consecutivo_Asesoria AND
             Aservic.Tipo_Estado_Decision IN
             (Pk_Conseres.Sec_Tipo_Codigo(1264, '2.01'),
              Pk_Conseres.Sec_Tipo_Codigo(1264, '2.08'));

    CURSOR CPoliticas IS
      SELECT COUNT(1)
        FROM VS_SER_CREDITOS Vsercre
       WHERE (Vsercre.Ser_Codigo = VserCod OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 9) OR
             Vsercre.Ser_Codigo = SUBSTR(VserCod, 1, 6)) AND
             Vsercre.Vigentes_Otras_Line < NCont
       ORDER BY Vsercre.Ser_Codigo, SUBSTR(Vsercre.Ser_Codigo, 1, 9),
                SUBSTR(Vsercre.Ser_Codigo, 1, 6);

  BEGIN
    OPEN CRestruc;
    FETCH CRestruc
      INTO NRest;
    CLOSE CRestruc;
    OPEN CCreditos;
    FETCH CCreditos
      INTO VSerCod, NPer_Id;
    CLOSE CCreditos;
    IF SUBSTR(VSerCod, 1, 6) = '001.03' THEN
      OPEN CServicios;
      FETCH CServicios
        INTO NCont;
      CLOSE CServicios;
      NCont := NCont - NVL(NRest, 0);
      OPEN CPoliticas;
      FETCH CPoliticas
        INTO NCon;
      CLOSE CPoliticas;
      IF NVL(NCon, 0) > 0 THEN
        RETURN 'SI';
      ELSE
        RETURN 'NO';
      END IF;
    ELSE
      RETURN 'NO';
    END IF;
  END CreditosOtrasLineaRestr;

  -- autor: Angela Martelo Zapata
  -- Fecha : 03/05/2004
  -- Función Validar la fecha de la primera cuota
  -- Parametros
  -- viFormaPago  --   Forma de Pago DEBITO, NOMINA, PAGO UNICO
  -- viTipoLiquidacion  QUINCENAL MENSUAL
  FUNCTION Validar_Fecha_Pago(ViFormaPago       TS_ASE_SERVICIOS.Aforpag_Nombre%TYPE,
                              ViTipoLiquidacion TS_ASE_SERVICIOS.Atipper_Liquidacion%TYPE,
                              Nipperocuid       TS_ASE_SERVICIOS.Pperocu_Id%TYPE)
    RETURN BOOLEAN IS

    VModificaFecha   VARCHAR2(2);
    Nperiodosinteres TS_CNV_DEDUCCIONES.Periodos_Intereses_Anticipados%TYPE;

    CURSOR CDeducciones(Nipperocuid IN NUMBER) IS
      SELECT NVL(Periodos_Intereses_Anticipados, 0)
        FROM TS_CNV_DEDUCCIONES Cdeduci, TS_PER_PERSONA_OCUPACIONES Pperocu
       WHERE Cdeduci.Con_Id = Pperocu.Con_Deduccion_Nomina AND
             Pperocu.Id = Nipperocuid;

  BEGIN

    IF ViFormaPago = 'DEBITO' OR Viformapago = 'CAJA' THEN
      VModificaFecha := UPPER(Pk_Conseres.Param_Gnral('GMODFECPRIMCUO'));
      IF VModificaFecha = 'SI' AND ViTipoLiquidacion = 'MENSUAL' THEN
        RETURN TRUE;
      ELSE
        RETURN FALSE;
      END IF;
    ELSIF ViFormaPago = 'NOMINA' THEN
      OPEN CDeducciones(Nipperocuid);
      FETCH CDeducciones
        INTO Nperiodosinteres;
      CLOSE CDeducciones;
      IF Nperiodosinteres > 0 THEN
        RETURN TRUE;
      ELSE
        RETURN FALSE;
      END IF;
    END IF;
  END;

  FUNCTION Causales_Incumplimiento(p_PerId    TS_PERSONAS.Id%TYPE,
                                   p_Asesoria TS_ASESORIAS.Consecutivo_Asesoria%TYPE)
    RETURN VARCHAR2 IS

    -- cursor para detectar si el codeudor ha tenido una causal de incumplimiento en los últimos 60 días
    CURSOR c_Causales(p_PerId TS_PERSONAS.Id%TYPE) IS
      SELECT s.Afase_Ase_Consecutivo_Asesoria, UPPER(p.Nombre) Fase,
             UPPER(Pk_Conseres.Nombre_Tipo(s.Tipo_Causal_Incumplimiento)) Causal,
             MAX(s.f_Creacion)
        FROM TS_ASE_RECHAZO_SOLICITUDES s, TS_ADM_PROCESOS p
       WHERE s.f_Creacion >= (SYSDATE - 60) AND
             s.Afase_Aproces_Codigo = p.Codigo AND s.Per_Id = p_PerId
       GROUP BY p.Nombre, s.Tipo_Causal_Incumplimiento,
                s.Afase_Ase_Consecutivo_Asesoria;
    c_Causal c_Causales%ROWTYPE;

    -- cursor para obtener el nombre del solicitante de la asesoría que presenta una causal de incumplimiento en los
    -- últimos 60 días
    CURSOR c_Solicitantes(p_Asesoria TS_ASESORIAS.Consecutivo_Asesoria%TYPE) IS
      SELECT DISTINCT p.Identificacion, Pe.Per_Id,
                      Pk_Conseres.Nombre_Tipo(s.Tipo_Estado_Decision) Estado_Actual
        FROM TS_PERSONAS p, TS_ASE_PERSONAS Pe, TS_ASE_SERVICIOS s
       WHERE p.Id = Pe.Per_Id AND Pe.Aperson_Type = 'SOLICI' AND
             s.Ase_Consecutivo_Asesoria = p_Asesoria AND
             s.Consecutivo_Servicio = Pe.Aservic_Consecutivo_Servicio;
    c_Solicitante  c_Solicitantes%ROWTYPE;
    VMensajeCausal VARCHAR2(4000);

  BEGIN
    VMensajeCausal := 'NO';
    -- Autor: Adriana M. Alzate V., Ideas Aplicadas S.A.
    -- Fecha: 24-09-2004
    -- valida la siguiente nota del diseño de módulos (módulo pantalla principal)
    -- Al ingresar un deudor o codeudor se debe validar si en los últimos 60 días ha tenido incumplimiento de alguna fase
    -- de evaluación de la solicitud (último resultado), caso en el cual, se debe indicar el nombre de la fase,
    -- la causal de incumplimiento, el nombre completo del solicitante del servicio y el estado de decisión del servicio.
    -- Se verifica en la tabla ASE RECHAZO SOLICITUD por intermedio de la relación que tiene con PERSONA.
    OPEN c_Causales(p_PerId);
    FETCH c_Causales
      INTO c_Causal;
    IF c_Causales%FOUND THEN
      VMensajeCausal := 'En los últimos 60 días incumplió la fase de evaluación: ' ||
                        c_Causal.Fase || ' por causal: ' || c_Causal.Causal || '. ';
      OPEN c_Solicitantes(c_Causal.Afase_Ase_Consecutivo_Asesoria);
      FETCH c_Solicitantes
        INTO c_Solicitante;
      IF c_Solicitantes%FOUND THEN
        IF c_Solicitante.Per_Id <> p_PerId THEN
          -- Muestra el nombre del solicitante solo cuando es diferente al codeudor que está siendo evaluado en la asesoría actual
          --vMensajeCausal := vMensajeCausal ||'Solicitante : '||c_solicitante.identificacion||'. ';
          NULL;
        END IF;
        VMensajeCausal := VMensajeCausal || 'Solicitante : ' ||
                          c_Solicitante.Identificacion || '. ';
        IF c_Solicitante.Estado_Actual IS NOT NULL THEN
          VMensajeCausal := VMensajeCausal || 'Estado actual: ' ||
                            c_Solicitante.Estado_Actual;
        END IF;
      END IF;
      CLOSE c_Solicitantes;
      RETURN VMensajeCausal;
    END IF;
  END Causales_Incumplimiento;

  FUNCTION Get_Record RETURN TS_ASE_PERSONAS%ROWTYPE IS
    /****************************************************************************************************
      OBJETO: get_record

      PROPOSITO:
      Se implemento esta funcion para ser utilizada en FORMS, debido a que no se puede
      tener acceso direcctamente.
      Retorna el registro variable
    ****************************************************************************************************/

  BEGIN
    RETURN Regendeudamiento;
  END;
  --
  -- Esta función determina si un servicio encuentra en un estado determinado
  -- Parametros
  --  niCosecutivoServicio: parámetro de entrada que contiene el consecutivo de
  --                        servicio para el cual se desea ejecutar la validación
  --  viCodigosTipoEstados: parametro de entrada que contiene los código del tipo de
  --                        estados de decisión que se desean validar.
  --  viValidarNroServicio: parámetro de entrada que determina si se desea validar
  --                        la existencia del número de servicio
  --  viValidarServicioProcesado: parámetro de entrada que determina si se desea validar
  --                              el servicio que se esta procesando u otro diferente
  -- Retorno: valor tipo texto que determina si el servicio se encuentra en el estado
  --          a verificar, es decir retornara:
  --          SI si el tipo de estado de decisión corresponde al código especificado
  --          NO si el tipo de estado de decisión no carresponde el código especificado
  -- Autor: Juan Esteban Sierra P.
  -- Fecha de creación: 23-10-2006
  --
  FUNCTION validar_estado_servicio(niCosecutivoServicio IN TS_ASE_SERVICIOS.consecutivo_servicio%TYPE,
                                   viCodigosTipoEstados IN VARCHAR2,
                                   viValidarNroServicio IN VARCHAR2,
                                   viValidarServicioProcesado IN VARCHAR2 DEFAULT 'SI')
                                   RETURN VARCHAR2 IS
    --
    -- Obtiene información del tipo de estado de decisión definido para el servicio
    --
    CURSOR cTipoEstado (nSecTipo  CON_TIPOS.sec%TYPE) IS
      SELECT tp.codigo
      FROM CON_TIPOS tp
      WHERE tp.sec = nSecTipo;

    rTipoEstado cTipoEstado%ROWTYPE;
    --
    -- Obtener información del servicio
    --
    CURSOR cAseServicio IS
      SELECT ase.consecutivo_servicio,
             ase.numero_servicio,
             ase.tipo_estado_decision, ase.ahesta_bloqueado
      FROM TS_ASE_SERVICIOS ase
      WHERE ase.consecutivo_servicio = niCosecutivoServicio;

    rAseServicio cAseServicio%ROWTYPE;
    vResultado   VARCHAR2(2);
    vSQL         VARCHAR2(1000);
    TYPE query_curtype IS REF CURSOR;
    rcTemp query_curtype;

  BEGIN
    IF (viValidarServicioProcesado = 'SI') THEN
      --
      -- Obtener la información del servicio procesado
      --
      IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
        GrAseServicioCre := ParGenAseServicio(niCosecutivoServicio);
      END IF;
      rAseServicio.consecutivo_servicio := GrAseServicioCre.consecutivo_servicio;
      rAseServicio.numero_servicio := GrAseServicioCre.numero_servicio;
      rAseServicio.tipo_estado_decision := GrAseServicioCre.tipo_estado_decision;
    ELSE
      --
      -- Obtener la información del servicio
      --
      OPEN cAseServicio;
      FETCH cAseServicio
      INTO rAseServicio;
      CLOSE cAseServicio;
    END IF;

    --
    -- Obtener información del tipo de estado de decisión del servicio
    --
    OPEN cTipoEstado(rAseServicio.Tipo_Estado_Decision);
    FETCH cTipoEstado
    INTO rTipoEstado;
    CLOSE cTipoEstado;
    --
    -- Construir el query de validacion
    --
    vSQL := 'SELECT ''SI'' '||
            'FROM dual '||
            'WHERE '''||NVL(rTipoEstado.codigo, '##')||''' IN ('||viCodigosTipoEstados||') '||
            'AND DECODE('''||NVL(viValidarNroServicio, 'NO')||''', '||
                        '''SI'', '||
                        'DECODE('''||NVL(rAseServicio.Numero_Servicio, 'NULL')||''', '||
                                '''NULL'', '||
                                'NULL, '||
                                ''''||NVL(rAseServicio.Numero_Servicio, 'NULL')||''' ), '||
                        '''0000'') IS NOT NULL ';
    DBMS_OUTPUT.put_line('SQL:'||SUBSTR(vSQL,1, 250));
    Rastro(viMsj => 'SQL:'||vSQL,vinombre => 'REEPLAZAR');
    --
    -- Evaluar si el estado de decisión asociado al servicio corresponde
    -- a alguno de los estados especificados
    --
    OPEN rcTemp
    FOR vSQL;
    FETCH rcTemp
    INTO vResultado;
    CLOSE rcTemp;

    RETURN NVL(vResultado, 'NO');

  END validar_estado_servicio;
  --
  -- Esta función determina si un servicio encuentra bloqueado
  -- Parametros
  --  niCosecutivoServicio: parámetro de entrada que contiene el consecutivo de
  --                        servicio para el cual se desea ejecutar la validación
  -- Retorno: valor tipo texto que determina si el servicio se encuentra bloqueado
  --          , es decir retornara:
  --          SI si el servicio esta bloqueado
  --          NO si el servicio no esta bloqueado
  -- Autor: Juan Esteban Sierra P.
  -- Fecha de creación: 25-10-2006
  --
  FUNCTION validar_servicio_bloqueado(niCosecutivoServicio IN TS_ASE_SERVICIOS.consecutivo_servicio%TYPE)
                                      RETURN VARCHAR2 IS
    --
    -- Obtener información del servicio
    --
    CURSOR cAseServicio IS
      SELECT ase.consecutivo_servicio,
             ase.ahesta_bloqueado
      FROM TS_ASE_SERVICIOS ase
      WHERE ase.consecutivo_servicio = niCosecutivoServicio;

    rAseServicio cAseServicio%ROWTYPE;
    vResultado   VARCHAR2(2);

  BEGIN
    --
    -- Obtener la información del servicio
    --
    OPEN cAseServicio;
    FETCH cAseServicio
    INTO rAseServicio;
    CLOSE cAseServicio;
    --
    -- Determina si la cuenta esta bloqueada
    --
    vResultado := rAseServicio.ahesta_bloqueado;

    RETURN NVL(vResultado, 'NO');

  END validar_servicio_bloqueado;
  --
  -- Esta función determina si un servicio encuentra embargado
  -- Parametros
  --  niCosecutivoServicio: parámetro de entrada que contiene el consecutivo de
  --                        servicio para el cual se desea ejecutar la validación
  -- Retorno: valor tipo texto que determina si el servicio se encuentra embargado
  --          , es decir retornara:
  --          SI si el servicio esta embargada
  --          NO si el servicio no esta embargada
  -- Autor: Juan Esteban Sierra P.
  -- Fecha de creación: 25-10-2006
  --
  FUNCTION validar_servicio_embargado(niCosecutivoServicio IN TS_ASE_SERVICIOS.consecutivo_servicio%TYPE)
                                      RETURN VARCHAR2 IS
    --
    -- Obtener información del servicio
    --
    CURSOR cAseServicio IS
      SELECT ase.consecutivo_servicio,
             ase.ahesta_embargado
      FROM TS_ASE_SERVICIOS ase
      WHERE ase.consecutivo_servicio = niCosecutivoServicio;

    rAseServicio cAseServicio%ROWTYPE;
    vResultado   VARCHAR2(2);

  BEGIN
    --
    -- Obtener la información del servicio
    --
    OPEN cAseServicio;
    FETCH cAseServicio
    INTO rAseServicio;
    CLOSE cAseServicio;
    --
    -- Determina si la cuenta esta bloqueada
    --
    vResultado := rAseServicio.ahesta_embargado;

    RETURN NVL(vResultado, 'NO');

  END validar_servicio_embargado;
  --
  -- Esta función determina si el destino definido para un servicio corresponde al o los
  -- tipo de destino especificados
  -- Parametros
  --  niCosecutivoServicio: parámetro de entrada que contiene el consecutivo de
  --                        servicio para el cual se desea ejecutar la validación
  --  viCodigosTipoDestino: parámetro de entrada que especifica los tipos de destino que
  --                 se desean validar, se debe pasar como un string separados por comas
  -- Retorno: valor tipo texto que determina si el servicio corresponde a alguno de los
  --          tipos de destino especificados, es decir retornara:
  --          SI si el destino del servicio corresponde
  --          NO si el destino del servicio no corresponde
  -- Autor: Juan Esteban Sierra P.
  -- Fecha de creación: 25-10-2006
  --
  FUNCTION validar_destinacion_servicio(niCosecutivoServicio IN TS_ASE_SERVICIOS.consecutivo_servicio%TYPE,
                                        viCodigosTipoDestino IN VARCHAR2)
                                        RETURN VARCHAR2 IS
    --
    -- Obtener información del servicio
    --
    CURSOR cAseServicio IS
      SELECT ase.consecutivo_servicio,
             tp.codigo codigo_tipo_destino
      FROM TS_ASE_SERVICIOS ase,
           CON_TIPOS tp
      WHERE tp.sec(+) = ase.tipo_destino
      AND ase.consecutivo_servicio = niCosecutivoServicio;

    rAseServicio cAseServicio%ROWTYPE;
    vResultado   VARCHAR2(2);

    TYPE query_curtype IS REF CURSOR;
    rcTemp query_curtype;

  BEGIN
    --
    -- Obtener la información del servicio
    --
    OPEN cAseServicio;
    FETCH cAseServicio
    INTO rAseServicio;
    CLOSE cAseServicio;
    --
    -- Evaluar si el estado de decisión asociado al servicio corresponde
    -- a alguno de los estados especificados
    --
    OPEN rcTemp
    FOR 'SELECT ''SI'' '||
        'FROM dual '||
        'WHERE '''||NVL(rAseServicio.codigo_tipo_destino, '##')||''' IN ('||viCodigosTipoDestino||') ';
    FETCH rcTemp
    INTO vResultado;
    CLOSE rcTemp;

    RETURN vResultado;
  END validar_destinacion_servicio;
  --
  -- Esta función valida la calificación de la empresa del convenio
  -- Parámetros
  --  niConId: parámetro de entrada que contine el identificador único del convenio
  --           a validar
  -- Retorno: valor tipo texto que determina si la ejecución fue exitosa o no
  -- Autor: Juan Esteban Sierra Pérez.
  -- Fecha de creación: 24-10-2006.
  --
  FUNCTION validar_calificacion_emp_con(niConId IN TS_CONVENIOS.id%TYPE)
                                        RETURN VARCHAR2 IS
    --
    -- Obtiene la información del convenio
    --
    CURSOR cConvenio IS
      SELECT con.per_id per_id_empresa
      FROM TS_CONVENIOS con
      WHERE con.id = niConId;

    rConvenio   cConvenio%ROWTYPE;
    vResultado   VARCHAR2(2000);
  BEGIN
    --
    -- Obtener la información del convenio
    --
    OPEN cConvenio;
    FETCH cConvenio
    INTO rConvenio;
    --
    -- Validar que exista un convenio
    --
    IF (cConvenio%FOUND) THEN
      CLOSE cConvenio;
      vResultado := VerificaCalificacion(rConvenio.per_id_empresa);
    ELSE
      CLOSE cConvenio;
    END IF;
    RETURN NVL(vResultado, 'NO');

  END validar_calificacion_emp_con;
  --
  -- Esta función valida si la empresa del convenio se encuentra reportada por  -- una central de riesgo
  -- Parámetros
  --  niConId: parámetro de entrada que contine el identificador único del convenio
  --           a validar
  --  viCodigoTipoPerEsp: parámetro de entrada que determina el tipo de central de riesgo que se desea validar
  -- Retorno: valor tipo texto que determina si la ejecución fue exitosa o no
  -- Autor: Juan Esteban Sierra Pérez.
  -- Fecha de creación: 24-10-2006.
  --
  FUNCTION validar_reporte_riesgo_emp_con(niConId IN TS_CONVENIOS.id%TYPE,
                                          viCodigoTipoPerEsp IN VARCHAR2 DEFAULT NULL)
                                          RETURN VARCHAR2 IS

    --
    -- Obtiene la información del convenio
    --
    CURSOR cConvenio IS
      SELECT con.per_id per_id_empresa
      FROM TS_CONVENIOS con
      WHERE con.id = niConId;

    rConvenio   cConvenio%ROWTYPE;
    vResultado   VARCHAR2(2000);
  BEGIN
    --
    -- Obtener la información del convenio
    --
    OPEN cConvenio;
    FETCH cConvenio
    INTO rConvenio;
    --
    -- Validar que exista un convenio
    --
    IF (cConvenio%FOUND) THEN
      CLOSE cConvenio;
      vResultado := Pks_Validagenpersonas.VerificaReportado(NiId => rConvenio.per_id_empresa,
                                                            viCodigoTipoPerEsp => viCodigoTipoPerEsp);

    ELSE
      CLOSE cConvenio;
    END IF;
    RETURN vResultado;

  END validar_reporte_riesgo_emp_con;
  --
  -- Esta función valida si la empresa se encuentra en una situación diferente
  -- a operación normal
  -- Parámetros
  --  niConId: parámetro de entrada que contine el identificador único del convenio
  --           a validar
  -- Retorno: valor tipo texto que determina si la ejecución fue exitosa o no
  -- Autor: Juan Esteban Sierra Pérez.
  -- Fecha de creación: 24-10-2006.
  --
  FUNCTION validar_situacion_emp_con(niConId IN TS_CONVENIOS.id%TYPE)
                                     RETURN VARCHAR2 IS

    --
    -- Obtiene la información del convenio
    --
    CURSOR cConvenio IS
      SELECT con.per_id per_id_empresa
      FROM TS_CONVENIOS con
      WHERE con.id = niConId;

    rConvenio   cConvenio%ROWTYPE;
    vResultado   VARCHAR2(2000);
  BEGIN
    --
    -- Obtener la información del convenio
    --
    OPEN cConvenio;
    FETCH cConvenio
    INTO rConvenio;
    --
    -- Validar que exista un convenio
    --
    IF (cConvenio%FOUND) THEN
      CLOSE cConvenio;
      vResultado := Pks_Validagenpersonas.VerficaSituacionEmpresa(NiId => rConvenio.per_id_empresa);
    ELSE
      CLOSE cConvenio;
    END IF;
    RETURN vResultado;

  END validar_situacion_emp_con;
  --
  -- Esta función valida si la empresa se encuentra en estado vetado o fallecido
  -- Parámetros
  --  niConId: parámetro de entrada que contine el identificador único del convenio
  --           a validar
  -- Retorno: valor tipo texto que determina si la ejecución fue exitosa o no
  -- Autor: Juan Esteban Sierra Pérez.
  -- Fecha de creación: 24-10-2006.
  --
  FUNCTION validar_estado_emp_con(niConId IN TS_CONVENIOS.id%TYPE)
                                  RETURN VARCHAR2 IS
    --
    -- Obtiene la información del convenio
    --
    CURSOR cConvenio IS
      SELECT con.per_id per_id_empresa
      FROM TS_CONVENIOS con
      WHERE con.id = niConId;

    rConvenio   cConvenio%ROWTYPE;
    vResultado   VARCHAR2(2000);
  BEGIN
    --
    -- Obtener la información del convenio
    --
    OPEN cConvenio;
    FETCH cConvenio
    INTO rConvenio;
    --
    -- Validar que exista un convenio
    --
    IF (cConvenio%FOUND) THEN
      CLOSE cConvenio;
      vResultado := Pks_Validagenpersonas.VerificaEstado(NiId => rConvenio.per_id_empresa);
    ELSE
      CLOSE cConvenio;
    END IF;
    RETURN vResultado;

  END validar_estado_emp_con;
  --
  -- Esta funcion retorna el registro del servicio contenida en la varible global
  -- Retorno: registro que contiene la informacion del servicio
  -- Autor: Juan Esteban Sierra Pérez
  -- Fecha de creacion: 20-10-2006
  --
  FUNCTION obtener_registro_aseServicio RETURN RAseServicioCre IS
  BEGIN
    RETURN grAseServicioCre;
  END obtener_registro_aseServicio;
  --
  -- Este procedimiento asigna un registro del servicio a la varible global
  -- Parametros
  --  riAseServicio: parámetro de entrada que contiene el registro a asignar
  --                 variable global
  -- Autor: Juan Esteban Sierra Pérez
  -- Fecha de creacion: 20-10-2006
  --
  PROCEDURE asignar_registro_aseServicio (riAseServicio IN RAseServicioCre) IS
  BEGIN
    grAseServicioCre := riAseServicio;
  END asignar_registro_aseServicio;


  -- Funcion   validar_fecha_vencimiento
  --
  -- Esta función determina si la fecha de venciemiento es válida
  -- Parametros
  --  niCosecutivoServicio: parámetro de entrada que contiene el consecutivo de
  --                        servicio para el cual se desea ejecutar la validación
  --  diFechaVencimiento:   parametro de entrada que contiene la fecha de vencimiento
  --                        que desea validar
  -- Retorno: valor tipo texto que determina si la fecha de vencimiento ingresada es
  --          valida para actualizar el servicio
  --          SI si el tipo de estado de decisión corresponde al código especificado
  --          NO si el tipo de estado de decisión no carresponde el código especificado
  -- Autor: Fredy Sanchez Castro
  -- Fecha de creación: 03-11-2006
  --
  FUNCTION validar_fecha_vencimiento(niCosecutivoServicio IN TS_ASE_SERVICIOS.consecutivo_servicio%TYPE)
  RETURN VARCHAR2 IS
    --
    -- Cursor para obtener el vencimiento de la línea
    --
    CURSOR cLinea IS
      SELECT id
        FROM vs_ser_aho_plazos_h a
       WHERE a.ser_codigo = GrAseServicioCre.ser_codigo
         AND a.tipo_plazo = 'VENCIM'
         AND GrAseServicioCre.fecha_efectivo BETWEEN a.saplica_fecha_inicial AND NVL(a.saplica_fecha_final, SYSDATE)
         AND a.ser_codigo = (SELECT MAX(b.ser_codigo)
                               FROM vs_ser_aho_plazos_h b
                              WHERE b.tipo_plazo = 'VENCIM'
                                AND GrAseServicioCre.ser_codigo LIKE b.ser_codigo||'%'
                                AND GrAseServicioCre.fecha_efectivo BETWEEN b.saplica_fecha_inicial AND NVL(b.saplica_fecha_final, SYSDATE));
    --
    -- Cursor para obtener la minima fecha de vencimiento configurada para la linea en el momente de la apertura del servicio
    --
    CURSOR cFecMinima (nId    vs_ser_aho_plazos_h.id%TYPE) IS
      SELECT MIN(b.fecha_vencimiento)
        FROM vs_ser_aho_plazo_definicione_h b
       WHERE b.svalor_id = nId;
    --
    vResultado            VARCHAR2(2);
    --
    nid                   vs_ser_aho_plazos_h.id%TYPE;
    dfechaminima          vs_ser_aho_plazo_definicione_h.fecha_vencimiento%TYPE;
    --
  BEGIN
    --
    -- Obtener la información del servicio procesado
    --
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(niCosecutivoServicio);
    END IF;
    --
    -- Se obtiene la linea definida al momento de la apertura del servicio
    --
    OPEN cLinea;
    FETCH cLinea INTO nId;
    CLOSE cLinea;
    --
    -- Se halla la mínima fecha de vencimineto configurada para la linea al momento de
    -- la apertura del servicio
    --
    IF nId IS NOT NULL THEN
      OPEN cFecMinima(nId);
      FETCH cFecMinima INTO dfechaminima;
      CLOSE cFecMinima;
    END IF;
    --
    -- Se valida que la fecha ingresada sea posterior a la fecha actual
    --
    IF (GrAseServicioCre.Fecha_Vencimiento < SYSDATE) OR (GrAseServicioCre.Fecha_Vencimiento < dfechaminima) THEN
      vResultado := 'SI';
    END IF;
    --
    --
    RETURN NVL(vResultado, 'NO');
    --
  END validar_fecha_vencimiento;
  --
  -- Funcion   validar_Apertura_Automatica
  --
  -- Esta función determina si la fecha de venciemiento es válida
  -- Parametros
  --  niCosecutivoServicio: parámetro de entrada que contiene el consecutivo de
  --                        servicio para el cual se desea ejecutar la validación
  --  viAceptaApertura:     parametro de entrada que contiene la aceptacion de la
  --                        apertura automatica que desea validar
  --  viformapago:          parametro de entrada que contiene la forma de pago
  --                        asociada que se desea validar
  -- Retorno: valor tipo texto que determina si la fecha de vencimiento ingresada es
  --          valida para actualizar el servicio
  --          SI si el tipo de estado de decisión corresponde al código especificado
  --          NO si el tipo de estado de decisión no carresponde el código especificado
  -- Autor: Fredy Sanchez Castro
  -- Fecha de creación: 03-11-2006
  --
  FUNCTION validar_Apertura_Automatica(niCosecutivoServicio IN TS_ASE_SERVICIOS.consecutivo_servicio%TYPE,
                                       viAceptaAperturaAuto IN TS_ASE_SERVICIOS.ahacepta_apertura_automatica%TYPE,
                                       viformapago          IN TS_ASE_SERVICIOS.aforpag_nombre%TYPE)
  RETURN VARCHAR2 IS
    --
    -- Cursor para obtener el vencimiento de la línea
    --
    CURSOR cLinea IS
      SELECT autorizacion_client
        FROM VS_SER_AHO_ADMINISTRACION  a,
             VS_SER_FORMA_PAGOS      b
       WHERE a.svalor_id             = b.svalor_id
         AND a.ser_codigo            = b.ser_codigo
         AND a.saplica_fecha_inicial = b.saplica_fecha_inicial
         AND b.forma_pago            = viformapago
         AND a.apertura_automatica   = viAceptaAperturaAuto;
    --
    vAutorizacion         VS_SER_AHO_ADMINISTRACION.autorizacion_client%TYPE;
    --
  BEGIN
    --
    -- Obtener la información del servicio procesado
    --
    IF GrAseServicioCre.Consecutivo_Servicio IS NULL THEN
      GrAseServicioCre := ParGenAseServicio(niCosecutivoServicio);
    END IF;
    --
    -- Se obtiene la linea definida al momento de la apertura del servicio
    --
    OPEN cLinea;
    FETCH cLinea INTO vAutorizacion;
    CLOSE cLinea;
    --
    --
    RETURN NVL(vAutorizacion, 'NO');
  END validar_Apertura_Automatica;


END Pks_Proc_Solicitudes;
