public class CampoConsulta{
	public String alias;
	public Expresion expresion;
	public CampoConsulta(){
		alias=null;
		expresion=null;
	}
	public String toString(){
		if(alias==null){
			return expresion+"";
		}else{
			return expresion+" "+alias;
		}
	}
}