CREATE OR REPLACE PROCEDURE       pr_estudio_tecnico_to (
   w_pedido   IN       fnx_pedidos.pedido_id%TYPE,
   w_resp     IN OUT   fnx_caracteristica_solicitudes.valor%TYPE
)
IS
-- HISTORIA DE MODIFICACIONES
-- Fecha       Autor        Observaciones
-- --------    ---------   ----------------------------------------------------------
-- 2008-01-22  Jrincong    Evaluación de características de Telefonia IP ( 3805, 3806, 3807 )
-- 2008-02-13  Jrincong    Determinar enrutamiento de Estudio Técnico del producto con base en el Municipio
--                         de servicio de la solicitud.
-- 2008-02-28  Jrincong    Implementación transacciones para cambio y retiro de servicios de Telefonia IP
-- 2008-04-10  Jrincong    Cambio de Número de VoIP.  Verificación de linea TOIP portada.  En
--                         tal caso se debe asignar como nuevo número, el número VOIP que soporta
--                         la portabilidad ( 3827 )
-- 2008-04-19  Jrincong    Cambio Tecnologia.  Verificar sí hay cambio de tecnologia IP a TDMA
-- 2008-04-19  Jrincong    SECCION.  Transacción Cambio de Tecnologia de IP a TDMA.  Provisión red de
--                         trámite e inserción de puentes.
-- 2008-04-19  Jrincong    Consideración de nuevos servicios de TOIP para una linea que ya es TOIP
--                         p_peticion_toip  AND  p_telefonia_ip
-- 2008-04-24  Jrincong    Creación de solicitud de retiro de TOIP ( acceso de la línea de voz ip )
-- 2008-04-24  Jrincong    Verificación para creación de puente de retiro ( sí es necesario )
-- 2008-04-25  Jrincong    Cambio de concepto de creación de solicitud de retiro de TOIP
--                         Inicialmente se crea en PSERV y luego se cambia a PORDE
-- 2008-05-03  Jrincong    Cambio de tecnologia IP a TDMA. Inserción de puente IB cuando la
--                         tecnologia del acceso TOIP es HFC.
-- 2008-05-03  Jrincong    Cambio de tecnología de IP a TDMA.  Creación de solicitud de retiro
--                         del componente TOIP
-- 2008-05-06  Jrincong    Cambio de tecnologia de IP a TDMA.  Solicitud de TOIP se deja en concepto
--                         PINSC, para paso a PORDE cuando se cumpla la solicitud de TO.
-- 2009-01-20  Jrincong    Determinar el número de solicitudes de TOIP que se deben crear, con base en la tecnología
--                         definida como base, para el acceso.
-- 2009-01-20  Jrincong    Creación de las solicitudes de TOIP, con base en el número determinado por
--                         la variable v_total_solicitud_toip
-- 2009-01-21  Jrincong    Actualización de solicitudes de TO a estado y concepto ORDEN-PSERV.
-- 2009-01-29  Jrincong    Cambio en lógica para calculo de variable v_total_solicitud_toip
-- 2009-02-07  Jrincong    Buscar el uso del servicio, para determinar el número de solicitudes de EQURED
--                         que se deben crear.
-- 2009-02-07  Jrincong    Crear solicitudes EQURED para cada una de las líneas TO
-- 2009-02-10  Jrincong    Verificar el uso del servicio de la línea.  En el caso de que sea comercial, no se
--                         debe crear una solicitud de retiro del acceso ( TOIP )
-- 2009-02-24  Mmedinac    Actualización de la lógica para cuando el pedido queda en FACTU - PLECT actualice la
--                         fecha de instalacion en FNX_REPORTES_INSTALACION
-- 2009-03-02  Jrincong    Lógica para calcular líneas de acceso.  Se hace antes de verificar el enrutamiento
--                         de estudio técnico ( PR_RUTAS_PROV_ET ).
-- 2009-03-02  Jrincong    En caso de alguna inconsistencia se debe salir del ciclo para que no se realicen otras
--                         tareas
-- 2009-03-26  WMendozaL   Se deben verificar las condiciones para la asignación de la línea TOIP adicional.
--                         para esto, se hace llamado al procedimiento PR_PROV_VERIF_ACCESO_TOIP.
-- 2009-03-27  WMendozaL   Si el procedimiento PR_PROV_VERIF_ACCESO_TOIP, retorna un identificador TOIP, se llama al procedimiento
--                         PR_PROV_ACCESO_PTO_LIBRE, el cual retorna, marca, referencia, equipo,id,
--                         terminal.
-- 2009-03-28  WMendozaL   Si ya existe un equipo con capacidad en los puertos, se debe actualizar las caracteristicas
--                         de marca, referencia, equipo y terminal.
-- 2009-04-03  WMendozaL   Lògica para enrutamiento de solicitudes de TO con tecnologia IP, consideradas
--                         como lineas adicionales, cuando se tiene un acceso.
-- 2009-04-04  Jrincong    Se quita cursor c_total_solic_to para calcular solicitudes de TO.  El calculo se hace
--                         con base en la variable v_total_solic_to_ip
-- 2009-04-04  Jrincong    Para el caso de Telefonia IP nueva, se incrementa la variable que controla
--                         el nùmero de solicitudes de TO con IP.
-- 2009-04-04  Jrincong    Se quita condiciòn que evaluaba sì el enrutamiento es directamente al estado de Ordenes,
--                         sin asignaciòn de red ( p_sin_asignacion_red ) y en el cual se consideraba como telefonìa IP.
--                         Con este cambio se evitan errores en el caso de que se ofrezca telefonia normal en otras
--                         ciudades
-- 2009-04-04  Jrincong    Actualizaciòn de solicitud base a estado ORDEN y PSERV y caracterìstica 4093. Se
--                         quita actualizaciòn de solicitudes de TO en PETEC, pues la actualizaciòn se
--                         hace dentro del ciclo
-- 2009-04-04  WmendozaL   Busqueda de uso de servicio cuando es un servicio de Telefonia IP Nuevo. v_uso_servicio
-- 2009-04-06  WmendozaL   Cursor para obtener los identificadores de TO asociados al
--                         identificador de acceso TOIP.
-- 2009-04-06  WmendozaL   Declaración de Variables tabla pl para llevar una relación de
--                         Los identificadores asociados a cada acceso TOIP.
-- 2009-04-06  WmendozaL   Se comienza a recorrer la tabla pl para el identificador
--                         TOIP Asociado
-- 2009-04-07  WmendozaL   Se debe llenar la otra tabla pl para realizar la comparación
-- 2009-04-07  WmendozaL   Se comienzan a recorrer las tablas pl con el fin de realizar
--                         La comparación del identificador. Esta tarea se realiza con la
--                         funcion pkg_telefip_util.fn_buscar_identif_valor
-- 2009-04-07  WmendozaL   Cursor para obtener los datos asociados a las terminales de
--                         los equipos que se van a retirar
-- 2009-04-08  Jrincong    Lògica para verificar los equipos asociados a los identificadores que se retiran y determinar
--                         cuales equipos se retiran y cuales solo se libera uno de los puertos ( cambian )
-- 2009-04-11  Jrincong    Se quita lógica de actualización de solicitud.  Se maneja de manera centralizada
--                         en la sección de actualización de la solicitud.
-- 2009-05-07  Jrincong    Seccion Inserciòn Solicitudes Cambio EQURED.  Procedimiento PR_SOLICITUD_CAMBI_EQURED
--                         Cambio de concepto de solicitud a POPTO.  La solicitud no genera Orden. Antes PORDE
-- 2009-05-12  Jrincong    Retiro de accesos TOIP.  Verificaciòn de tipo de Tecnologìa REDCO para buscar sì se
--                         debe generar el regitro de puente
-- 2009-05-13  Jrincong    Consideraciòn de linea portada. Solicitud de servicios IP para una lìnea con tecnologia tradicional
-- 2009-05-23  Jrincong    Retiro. Seccion Generacion Puentes de Red.  Adiciòn de condiciòn < AND NOT p_peticion_toip >
--                         para no generar puente de Red.
-- 2009-05-23  Jrincong    Cambio de Numero.  Se debe evaluar sì el cambio de nùmero se hace para lìnea portada y asi
--                         poder establecer que es una peticiòn IP.
-- 2009-05-28  Jrincong    Unificación de Modelo TOIP.  Modificaciones para no considerar uso de servicio en secciones de:
--                         Uso del servicio para salirse del proceso y crear una solicitud de Retiro
--                         Uso de servicio para determinar el nùmero de equipos de red a crear ( v_total_solic_equred  )
-- 2009-06-27  Jrincong    Para el caso de la linea portada, se considera el nùmero de solicitudes Nuevas en el pedido
--                         màs una solicitud que corresponde a la línea portada. Antes se consideraba solo 1 solicitud
--                         y fallaba para el escenario de Linea Portada màs una lìnea adicional
-- 2009-06-27  Jrincong    En la invocaciòn del proc. PR_SOLICITUD_NUEVA_TOIP se cambia el valor del parametro tipo_elemento_id
--                         por ATA ( Antes EQURED )
-- 2009-06-27  Jrincong    Para el caso de tecnologìa INALA ( WIMAX ) se agrega una solicitud de Antena Wimax.
-- 2009-07-23  Jrincong    Uso de variable v_tipo_central para manejo de valor IP u otro tipo
-- 2009-07-23  Jrincong    Prov. PR_TRAER_ID_MUN_PROD. Invocacion con nuevo paràmetro v_plataforma_numeracion
-- 2009-07-24  Jrincong    Uso de funciòn para obtener la plataforma de Numeraciòn que tipifica las series de numeraciòn
--                         que se deben utilizar
-- 2009-07-24  Jrincong    Obtener el dato de tipo Central asociado a la plataforma de Numeraciòn
-- 2009-07-27  Jrincong    Sì la solicitud no trae servicios de Telefonia IP, se debe evaluar sì el grupo de
--                         proveedor, municipio, producto y uso de servicio, tiene la configuraciòn que indica sì se
--                         deben crear solicitudes IP o no.
-- 2009-07-27  Jrincong    Se considera la variable b_crear_solic_nueva_ip como criterio para asignar los valores de las
--                         variable v_subpedido_TO y v_solicitud_TO_base para crear las solicitudes nuevas del pedido
--                         ( ejm. EQURED y TOIP. )
-- 2009-07-27  Jrincong    Secciòn para calculo de solicitudes de Acceso que se deben crear cuando se solicitan
--                         servicios de Nueva IP.  Esta secciòn se saca del ciclo principal para no hacer el calculo
--                         por cada solicitud.
-- 2009-08-25  Jrincong    Se utiliza funciòn para determinar cual es la plataforma de numeraciòn del identificador
--                         actual y asì poder determinar la plataforma del nuevo número.
-- 2009-09-25  Jrincong    Verificar sì la solicitud tiene la caracterìstica 4110 ( Tipo Telefonia IP ) con el valor IP
--                         En tal caso, el servicio de Telefonia IP fue solicitado de manera expresa y por tanto se
--                         realiza el proceso de creación de solicitudes de TOIP. Tal caracteristica se puede utilizar
--                         para casos en los que el cliente solicita Telefonia IP
-- 2009-12-10  Jrincong    Búsqueda de valor de característica 3870 para determinar el proveedor de Servicio y definir
--                         si se deben crear solicitudes para Telefonia IP
-- 2009-12-10  Jrincong    Sì la variable v_proveedor_servicio es nula, se asigna el valor del campo EMPRESA_ID de
--                         la solicitud, como valor por defecto
-- 2009-12-10  Jrincong    Cambio de variable v_solic.empresa_id por v_proveedor_servicio PR_RUTAS_MUNIC_PRODUCTO
-- 2010-01-07  Wmendoza    Para el caso de la migración a HUAWEI, se debe verificar si viene la 4110 y si en trabajos viene un cambio
-- 2010-01-07  Wmendoza    Migración Huawei. Para el caso en que sea un cambio a HUAWEI se insertan dos solicitudes
--                         de equipos EQURED EQACCP.
-- 2010-01-07  Wmendoza    Para este caso, la solicitud de TO, debe pasar a PORDE
-- 2010-01-08  Wmendoza    Se debe verificar la tecnología de acuerdo al valor que tiene en la tabla de identificadores
-- 2010-01-26  Wmendoza    Para el tema de migración a HUAWEI, se debe insertar las caracteristicas 3860 y 3861
-- 2010-01-26  Wmendoza    Verificaciòn de variable provision_tarjeta_ip  de acuerdo con la plataforma de Numeraciòn
--                         para inserciòn de caracterìstica 3861 <Tarjeta Plataforma IP>
-- 2010-02-03  Aacosta     Consideracion de la peticion de Cambio de número para la migracion de TOIP.
--                         Se debe validar si viene en los trabajos de la solicitud un cambio de número
--                         con valor M. Para este caso se debe prender una booleana para poder insertar
--                         la solicitud del EQACCP y DEL EQURED.
-- 2010-02-03  Aacosta     Se modifica la condicion para cambio de huawei para incluir el tema de cambio de identificador
--                         para la migracion de toip.  IF p_cambio_huawei por IF p_cambio_huawei OR b_chg_numero_mig_toip
-- 2010-02-12  Jrincong    Cambio de Nùmero.  Verificaciòn de secciones para cambio de número en tecnologìa POTS
-- 2010-02-12  Jrincong    Cambio de Nùmero.  Parametrización de cursores c_cilo,  c_ttarjeta, c_trabajos, c_serie, c_starj
--                         c_ractiva, c_rtramite
-- 2010-02-12  Jrincong    Cambio de Nùmero.  Cursores c_cilo,  c_ttarjeta, c_ident, c_serie, c_starj. Selecciòn
--                         solo de campos necesarios. Se quita select *
-- 2010-02-12  Jrincong    Cambio de Número.  Parametrizaciòn de cursores c_trabaj, c_parametros, c_ractiva
-- 2010-02-12  Jrincong    Verificaciòn de variable de circuito_logico_id para el caso de las TO de REDCO
--                         con tecnologia POTS tradicional.
-- 2010-02-12  Jrincong    Verificaciòn de variable de circuito_logico_id para el caso de las TO de REDCO
--                         con tecnologia POTS tradicional.
-- 2010-02-15  Jrincong    Determinar el nodo de numeraciòn con base en el subtipo de Tecnologìa.
-- 2010-02-15  Jrincong    Validaciones para establecer sì la red de cobre activa es vàlida para la transacciòn
--                         de Cambio de Número
-- 2010-02-15  Jrincong    Inserciòn de registro de Eqmul Trámite para el caso que el Identificador anterior
--                         tenga el subtipo de tecnologia POTS.
-- 2010-03-02  Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caracteres porque los pedidos
--                         se quedan en petec.
-- 2010-03-15  Jrincong    Invocar procedimiento PR_PROV_PUENTES_ORIGEN para inserciòn de puentes de retiro.
-- 2010-03-15  Jrincong    Logica generaciòn puentes de Interconexiones, se traslada para proc. PR_PROV_PUENTES_ORIGEN
-- 2010-04-27  Etachej     Se cambia la aperura del cursor c_identificador para el tipo trabajo RETIR, para que siempre sea consultado
--                         independientemente si tiene o no una peticion de TOIP.
-- 2010-05-05  Lgonzalg    Uso de procedimiento para generar puentes de configuraciòn / desconfiguracion cuando
--                         se retira un servicio de TOIP a través de una TO. El procedimiento verifica condiciones de Multiservicio.
-- 2010-05-05  Lgonzalg    Se quitan variables v_internet_banda_ancha , v_television_adsl ya que se dejan de usar.
-- 2010-05-10  Lgonzalg    Agregar cursor c_rutasprov para consultar la variable CONTROL_PUENTES.
-- 2010-05-10  Lgonzalg    Verificar si se deben generar puentes. Se consulta la variable CONTROL_PUENTES de rutas provisión.
-- 2010-09-21  Wmendoza    En adelante se utilizará el procedimiento PR_VERIFICAR_CICLO_CORRERIA
--                         para realizar las verificaciones y saber si un pedido se coloca en concepto PUMED.
-- 2010-09-22  Jrincong    Invocación de procedimiento PR_RUTAS_MUNIC_PRODUCTO. Cambio de variable vo_tecnologia_acceso
--                         por vo_tecnologia_base
-- 2010-09-22  Jrincong    Cursor c_rutmunpro_tecnologia para buscar la tecnologia asociada a la configuraciòn de un proveedor, municipio,
--                         producto y el uso de servicio  ( para el caso de TO )
-- 2010-09-22  Jrincong    Cursor c_rutmunpro_tecnologia. Buscar la tecnologìa base asociada al proveedor,
--                         municipio, producto y servicio.
-- 2010-09-22  Jrincong    Se quita uso de proc. PR_RUTAS_PROV_TECNOLOGIA debido a que el valor de vo_tecnologia_base se
--                         obtiene previamente.
-- 2010-09-23  Jrincong    Mostrar valores de variables en búsqueda de datos para Municipio - Proveedor - Uso Servicio
--                         y generar inconsistencia en variables w_mensaje_error y w_inconsistencias
-- 2010-09-23  Jrincong    Condicion p_telefonia_ip AND p_nueva.
--                         Buscar la tecnologìa base asociada al proveedor, municipio, producto y servicio.
-- 2010-09-23  Jrincong    Condicion p_telefonia_ip AND p_nueva.
--                         Buscar la tecnologìa base asociada al proveedor, municipio, producto y servicio.
-- 2010-09-23  Wmendoza    Se actualiza la solicitud a estado PUMED y se sale del ciclo para evitar que se creen
--                         las demás solicitudes.
-- 2010-09-23  Wmendoza    Se actualiza la solicitud a estado PUMED y se sale del ciclo.
-- 2010-09-28  Wmendoza    Se agrega la condición de que no sea un pedido de INGRE-PUMED.
-- 2010-09-29  Wmendoza    Se agrega el campo identificador_id_nuevo.
-- 2011-03-14  Wmendoza    Si viene un tipo de trabajo en nuevo para la caracteristica 49
--                         entonces, coloco en true la variable b_citofonia
-- 2011-03-14  Wmendoza    Consideraciones a tener en cuenta para cuando se trata
--                         de citofonia:
--                         1) Si el identificador, no tiene valor en el campo agrupador de la tabla FNX_IDENTIFICADORES y el tipo de solicitud
--                            en la tabla FNX_REPORTES_INSTALACION es de tipo CAMBI,la solicitud debe quedar en ORDEN - PORDE.
--                         2) Por otro lado, si el identificador tiene agrupador, se debe llamar al procedimiento PR_CUMPLIDO_PETICIONES.
--                            Si ese procedimiento, no devuelve ningún error, las solicitud debe quedar en FACTU-PFACT.
-- 2011-03-15  Wmendoza    Se agrega la condición de que se retire la caracteristica 49.
-- 2011-03-30  Jaristz     En adiciones de TO en TOIP la TO debe pasar a concepto PORDE para la generracion de Ordenes.
-- 2011-03-31  Jaristz     En adiciones de TO en TOIP luego de que la TO pase a PSERV se procede a pasar a PORDE para que permita generar
--                         la numeración.
-- 2011-08-10  Jrincong    Portabilidad POTS-NGN.  Modificación para considerar cambio de circuito.  Sí es cambio de
--                         circuito, se evalua sí el identificador tiene la característica 4114 configurada con el
--                         valor de manera correcta.
-- 2011-08-11  Jrincong    Transacción de adiciòn de servicios para una linea que tiene portabilidad NGN.
-- 2011-08-12  Jrincong    Portabilidad NGN. Cambio de uso de proc. PR_RED_EXISTENTE por PR_INSERT_RED_TRAMITE.
-- 2011-09-23  JGallg      Cola CLIPS: generar los trabajos de la solicitud (característica 56) con el municipio de referencia (MUNICIPIO_ID).
-- 2012-09-20  Sbuitrab    Se realiza modificación para el tema de ciclo correria, puesto que antes de pasar a estado PORDE se debe verificar
--                         que el pedido si cumpla con los requisitos para la correcta facturación
-- 2013-02-26  Yhernana    Lógica con generación de solicitud de recuperación de equipo
--                         Si se retira TOIP, se debe realizar Desaprovisionamiento de Equipos.
--                         Lógica basada en el PR_ESTUDIO_TECNICO_TELEF_IP.
-- 2013-03-07  ETachej     REQ_GeneNuevNumePedi - Ampliar campo pedido
-- 2013-05-07  Wmendoza    Cursor para el tema de nuevo retiro.
-- 2013-05-07  Wmendoza    Lógica para el tema de traslado con retiro y nuevo
--                         se inserta el registro con la caracteristica 4938 para el EQACCP.
-- 2013-05-07  Wmendoza    Lógica para el tema de traslado con retiro y nuevo
--                         se inserta el registro con la caracteristica 4938 para el EQURED. 
-- 2013-09-02  Sbuitrab    ITPAM 19886 Configuracion de una Segunda Línea TOIP para HOGARES        

   w_respuesta_servicios         VARCHAR2 (3);
   w_inconsistencias             BOOLEAN;
   w_mensaje_error               VARCHAR2 (100);
   p_nueva                       BOOLEAN;
   p_retiro                      BOOLEAN;
   p_traslado                    BOOLEAN;
   p_cambio_numero               BOOLEAN;
   p_clip                        BOOLEAN;
   p_cambio_circuito             BOOLEAN;
   p_cambio_tecnologia           BOOLEAN;
   b_cambio_tecno_ip_tdma        BOOLEAN;
   b_redco_valida                BOOLEAN;
   b_encontrado_retiro           BOOLEAN;
   p_cambio_huawei               BOOLEAN;
   b_ingre_pumed                 BOOLEAN;
   b_citofonia                   BOOLEAN;
   p_camb_fetex                  BOOLEAN;

   CURSOR c_parametros
     ( p_parametro          IN   FNX_PARAMETROS.parametro_id%TYPE )
   IS
      SELECT valor,3+4*5,(a+b*c)*(a+b) d,qwer qwer,(select x,3*4 from dual) u
        FROM fnx_parametros,tres f,(select * from tal) o
       WHERE modulo_id = 'PRV_SERV'
       AND   parametro_id = p_parametro AND (select * from dual)=1 AND EXISTS (select r from tax);

   v_param                       c_parametros%ROWTYPE;
   w_parametro                   NUMBER (3);

   w_reserva_identificador       NUMBER (3);
   w_caracteristica              fnx_tipos_caracteristicas.caracteristica_id%TYPE;
   w_caracteristica_aux          fnx_tipos_caracteristicas.caracteristica_id%TYPE;

--2010-09-29 Se agrega el campo identificador_id_nuevo

   CURSOR c_solic
   IS
      SELECT     pedido_id, subpedido_id, solicitud_id, identificador_id,identificador_id_nuevo,
                 municipio_id, empresa_id, servicio_id, producto_id
            FROM fnx_solicitudes
           WHERE pedido_id = w_pedido
             AND servicio_id = 'TELRES'
             AND producto_id = 'TO'
             AND tipo_elemento_id = 'TO'
             AND tipo_elemento = 'CMP'
             AND estado_id = 'TECNI'
             AND concepto_id = 'PETEC'
             AND estado_bloqueo = 'N'
-- AND      identificador_id  IS NOT NULL
      FOR UPDATE;

   v_solic                       c_solic%ROWTYPE;

   CURSOR c_ractiva
     ( p_identificador           IN FNX_IDENTIFICADORES.identificador_id%TYPE )
   IS
      SELECT nodo_conmutador_id, armario_id, strip_id, par_primario_id,
             caja_id, par_secundario_id,
             nodo_conmutador_eqd, circuito_logico_id,
             posicion_fisica_id,  circuito_publico_id,
             subtipo_tecnologia_id
        FROM fnx_inf_redes_activas
       WHERE identificador_id = p_identificador;

   v_ractiva                     c_ractiva%ROWTYPE;

   -- Cursor c_ractiva_subtipo.  
   -- Utilizado para obtener sólo los campos nodo_conmutador_id y subtipo_tecnologia.
   CURSOR c_ractiva_subtipo
     ( p_identificador           IN FNX_IDENTIFICADORES.identificador_id%TYPE )
   IS
      SELECT nodo_conmutador_id, subtipo_tecnologia_id
        FROM fnx_inf_redes_activas
       WHERE identificador_id = p_identificador;

    v_ractiva_subtipo           c_ractiva_subtipo%ROWTYPE;
    
   CURSOR c_cilo
     ( p_nodo_conmutador         IN  FNX_NODOS_CONMUTADORES.nodo_conmutador_id%TYPE,
       p_circuito_logico         IN  FNX_TARJETAS_CIRCUITOS_LOGICOS.circuito_logico_id%TYPE
     )
   IS
      SELECT tipo_tarjeta_id
        FROM fnx_tarjetas_circuitos_logicos
       WHERE nodo_conmutador_id = p_nodo_conmutador
         AND circuito_logico_id = p_circuito_logico;

   v_tipo_tarjeta                fnx_tipos_tarjetas.tipo_tarjeta_id%TYPE;

   p_fec_inic                    DATE      := TO_DATE ('19691204', 'YYYYMMDD');

   CURSOR c_trabajos
      ( p_pedido                IN  FNX_PEDIDOS.pedido_id%TYPE,
        p_subpedido             IN  FNX_SUBPEDIDOS.subpedido_id%TYPE,
        p_solicitud             IN  FNX_SOLICITUDES.solicitud_id%TYPE
      )
   IS
      SELECT a.caracteristica_id
        FROM fnx_trabajos_solicitudes a, fnx_tipos_caracteristicas b
       WHERE a.pedido_id    = p_pedido
         AND a.subpedido_id = p_subpedido
         AND a.solicitud_id = p_solicitud
         AND a.tipo_trabajo = 'NUEVO'
         AND a.valor IS NOT NULL
         AND a.caracteristica_id = b.caracteristica_id
         AND b.indicador_tipo = 'S_ESP';


   CURSOR c_ttarjeta
     ( p_tipo_tarjeta          IN FNX_TIPOS_TARJETAS.tipo_tarjeta_id%TYPE )
   IS
      SELECT tipo_central_id
        FROM fnx_tipos_tarjetas
       WHERE tipo_tarjeta_id = p_tipo_tarjeta;


   CURSOR c_starj
      ( p_tipo_tarjeta         IN  FNX_SERVICIOS_TARJETAS.tipo_tarjeta_id%TYPE,
        p_caracteristica       IN  FNX_TIPOS_CARACTERISTICAS.caracteristica_id%TYPE
      )
   IS
      SELECT caracteristica_id
        FROM fnx_servicios_tarjetas
       WHERE tipo_tarjeta_id   = p_tipo_tarjeta
         AND caracteristica_id = p_caracteristica;

   CURSOR c_serie
     ( p_nodo_conmutador           IN   FNX_NODOS_CONMUTADORES.nodo_conmutador_id%TYPE,
       p_tipo_central              IN   FNX_TIPOS_CENTRALES.tipo_central_id%TYPE
     )
   IS
      SELECT b.serie_id, b.numero_inicial, b.numero_final
        FROM fnx_rangos_productos a, fnx_rangos_series b
       WHERE a.nodo_conmutador_id = p_nodo_conmutador
         AND a.nodo_conmutador_id = b.nodo_conmutador_id
         AND a.serie_id           = b.serie_id
         AND a.numero_inicial     = b.numero_inicial
         AND a.numero_final       = b.numero_final
         AND a.servicio_id        = 'TELRES'
         AND a.producto_id        = 'TO'
         AND b.tipo_central_id    = p_tipo_central
         AND a.disponible         = 'S'
         AND b.estado             = 'S';

    v_serie                       c_serie%ROWTYPE;

   -- Cursor para buscar un identificador libre en una serie
   CURSOR c_ident_libre
     ( p_serie              IN   FNX_SERIES.serie_id%TYPE,
       p_numero_inicial     IN   FNX_RANGOS_SERIES.numero_inicial%TYPE,
       p_numero_final       IN   FNX_RANGOS_SERIES.numero_final%TYPE,
       p_meses_reserva      IN   NUMBER
     )
   IS
      SELECT identificador_id
        FROM fnx_identificadores
       WHERE serie_id          = p_serie
         AND identificador_id BETWEEN
              TO_CHAR (p_serie)|| LPAD (p_numero_inicial, 4, 0)
         AND  TO_CHAR (p_serie)|| LPAD (p_numero_final,   4, 0)
         AND NVL (servicio_id, 'P') NOT IN ('DATOS', 'ENTRE')
         AND estado = 'LIB'
         AND NVL (fecha_estado, p_fec_inic) <   ADD_MONTHS (SYSDATE, p_meses_reserva)
         AND ROWNUM = 1;

   v_rtramite                    fnx_inf_redes_tramites%ROWTYPE;

   CURSOR c_rtramite
     ( p_pedido                 IN   FNX_PEDIDOS.pedido_id%TYPE,
       p_subpedido              IN   FNX_SUBPEDIDOS.subpedido_id%TYPE,
       p_solicitud              IN   FNX_SOLICITUDES.solicitud_id%TYPE
     )
   IS
      SELECT     *
            FROM fnx_inf_redes_tramites
           WHERE pedido_id    = p_pedido
             AND subpedido_id = p_subpedido
             AND solicitud_id = p_solicitud
      FOR UPDATE;

   v_inter                       fnx_interconexiones_activas%ROWTYPE;


   CURSOR c_inter
      ( p_identificador         IN  FNX_IDENTIFICADORES.identificador_id%TYPE )
   IS
      SELECT *
        FROM fnx_interconexiones_activas
       WHERE identificador_id = p_identificador;


   CURSOR c_trabaj
     ( p_pedido                 IN   FNX_PEDIDOS.pedido_id%TYPE,
       p_subpedido              IN   FNX_SUBPEDIDOS.subpedido_id%TYPE,
       p_solicitud              IN   FNX_SOLICITUDES.solicitud_id%TYPE
     )
   IS
      SELECT caracteristica_id, tipo_trabajo, valor
        FROM fnx_trabajos_solicitudes
       WHERE pedido_id    = p_pedido
         AND subpedido_id = p_subpedido
         AND solicitud_id = p_solicitud;

   v_trabaj                      c_trabaj%ROWTYPE;

   w_encontro_numero             BOOLEAN                              := FALSE;
   w_soporta_servicios           BOOLEAN                              := FALSE;

   w_identificador               fnx_identificadores.identificador_id%TYPE;
   v_caracteristica_insert       FNX_TIPOS_CARACTERISTICAS.caracteristica_id%TYPE;
   v_equipment_id                VARCHAR2(32);
   v_tarjeta_ip                  FNX_PROV_TARJETASIP_MUNIC.tarjeta_numero%TYPE;
   v_valor_random                NUMBER(9);
   v_mensaje_insupd              VARCHAR2(100);

   CURSOR c_subpedido
   IS
      SELECT agrupador
        FROM fnx_subpedidos
       WHERE pedido_id = v_solic.pedido_id
         AND subpedido_id = v_solic.subpedido_id;

   v_subpedido                   c_subpedido%ROWTYPE;

   CURSOR c_identificador
   IS
      SELECT estado, tecnologia_id
        FROM fnx_identificadores
       WHERE identificador_id = v_solic.identificador_id;

   v_identificador               c_identificador%ROWTYPE;
   w_tipo_puente                 fnx_puentes.tipo_puente%TYPE;

   p_empaqueta_linea             BOOLEAN;
   p_cambio_linea_auxiliar       BOOLEAN;
   p_telefonia_ip                BOOLEAN;
   p_peticion_toip               BOOLEAN;
   p_serv_toip_cambio            BOOLEAN;
   p_serv_toip_retir             BOOLEAN;
   b_linea_portada               BOOLEAN;
   v_cliente                     fnx_clientes.cliente_id%TYPE;
   v_agrupador                   fnx_identificadores.identificador_id%TYPE;
   v_identificador_paquete       fnx_caracteristica_solicitudes.valor%TYPE;

-- Cursor para buscar el identificador de paquete en el pedido.
   CURSOR c_paquete_identificador (p_pedido IN fnx_pedidos.pedido_id%TYPE)
   IS
      SELECT identificador_id_nuevo
        FROM fnx_solicitudes
       WHERE pedido_id = p_pedido
         AND servicio_id = 'PQUETE'
         AND producto_id = 'PQUETE'
         AND tipo_elemento_id = 'PQUETE'
         AND tipo_elemento = 'SRV'
         AND estado_id IN ('ORDEN', 'CUMPL');

   v_paquete_identificador       fnx_solicitudes.identificador_id_nuevo%TYPE;
   v_tipo_venta                  fnx_caracteristica_solicitudes.valor%TYPE;
   v_ident_reinstalacion         fnx_caracteristica_solicitudes.valor%TYPE;
   v_estado_identificador        fnx_identificadores.estado%TYPE;
   v_estado_servicio             fnx_configuraciones_identif.valor%TYPE;
   v_estado_solicitud            fnx_solicitudes.estado_id%TYPE;
   v_concepto_solicitud          fnx_solicitudes.concepto_id%TYPE;
   b_actualizar_solicitud        BOOLEAN;
   b_prepago_desconect           BOOLEAN;
   v_observacion                 VARCHAR2 (100);
   v_observacion_aislados        fnx_aislados.observacion%TYPE;
   v_usuario                     fnx_usuarios.usuario_id%TYPE;
   p_mensaje                     VARCHAR2 (50);

-- Variables para manejo de creación de solicitudes de TOIP
   v_subpedido_to                fnx_solicitudes.subpedido_id%TYPE;
   v_solicitud_to_base           fnx_solicitudes.solicitud_id%TYPE;
   v_tecnologia_solicitud        fnx_solicitudes.tecnologia_id%TYPE;
   v_identificador_nuevo         fnx_identificadores.identificador_id%TYPE;

   v_identificador_retir         fnx_solicitudes.identificador_id_nuevo%TYPE;
   v_solicitud_nueva             fnx_solicitudes.solicitud_id%TYPE;
   v_mensaje_creacion            VARCHAR2 (150);
   v_municipio_servicio          fnx_caracteristica_solicitudes.valor%TYPE;
   v_munic_salida_int            fnx_municipios.mun_salida_internet%TYPE;
   vo_enrutar_et                 fnx_rutas_provision.control_est_tecnico%TYPE;
   vo_tecnologia_base            fnx_rutas_provision.tecnologia_id%TYPE;
   v_total_lineas_xacceso        NUMBER (3);
   v_total_solic_to_ip           NUMBER (3);
   v_total_solic_equred          NUMBER (3);
   v_total_solicitud_toip        NUMBER (3);
   v_uso_servicio                fnx_caracteristica_solicitudes.valor%TYPE;
   v_tipo_tecno_telefonia        fnx_caracteristica_solicitudes.valor%TYPE;
   v_tecnologia_identificador    fnx_configuraciones_identif.valor%TYPE;
   w_uno                         VARCHAR2 (1);
   w_dos                         VARCHAR2 (1);
   w_tres                        VARCHAR2 (1);
   v_municipio_numeracion        fnx_municipios.municipio_numeracion%TYPE;
   w_identificador_toip          fnx_identificadores.identificador_id%TYPE;
   b_error_actualizando          BOOLEAN;
   v_linea_voip_portada          fnx_configuraciones_identif.valor%TYPE;
   b_portada_ip_tdma             BOOLEAN                              := FALSE;
   v_identificador_acceso_toip   fnx_configuraciones_identif.valor%TYPE;
   v_tecnologia_acceso_toip      fnx_identificadores.tecnologia_id%TYPE;
   v_identif_internet_ba         fnx_configuraciones_identif.valor%TYPE;
   v_identif_telev_adsl          fnx_configuraciones_identif.valor%TYPE;
   b_provisionar_red             BOOLEAN                              := FALSE;
   v_red_provisionada            VARCHAR2 (15);
   b_inconsistencia              BOOLEAN                              := FALSE;
   v_mensaje_prov_red            VARCHAR2 (200);
   puente_red                    fnx_puentes.tipo_puente%TYPE;
   v_parametro_est_to            NUMBER (3);
   v_uso_comercial               fnx_caracteristica_solicitudes.valor%TYPE;
   v_pagina_servicio             fnx_caracteristica_solicitudes.valor%TYPE;
   v_acceso_toip                 fnx_identificadores.identificador_id%TYPE;
   v_mensaje_toip                VARCHAR2 (200);
   v_mensaje_enrutam_to          VARCHAR2 (200);
   v_marca                       fnx_terminales_equipos.marca_id%TYPE;
   v_referencia                  fnx_terminales_equipos.referencia_id%TYPE;
   v_equipo_id                   fnx_terminales_equipos.equipo_id%TYPE;
   v_terminal                    fnx_terminales_equipos.terminal_id%TYPE;
   v_mensaje_pto_libre           VARCHAR2 (200);
   b_nuevo_acceso                BOOLEAN;
   b_equipo_nuevo                BOOLEAN;
   b_crear_solic_nueva_ip        BOOLEAN                              := FALSE;
   b_crear_solicip_retiro        BOOLEAN                              := FALSE;
   b_nueva_telefonia_ip          BOOLEAN                              := FALSE;
   v_solicitud_equred            fnx_solicitudes.solicitud_id%TYPE;
   v_serial_equipo               fnx_caracteristica_solicitudes.valor%TYPE;
   v_marca_equipo                fnx_caracteristica_solicitudes.valor%TYPE;
   v_referencia_equipo           fnx_caracteristica_solicitudes.valor%TYPE;
   v_terminal_asociado           fnx_caracteristica_solicitudes.valor%TYPE;
   v_solicitud_eqaccp            fnx_solicitudes.solicitud_id%TYPE;
   v_identificador_toip          fnx_identificadores.identificador_id%TYPE;
   v_agrupador_to                fnx_identificadores.agrupador_id%TYPE;
   w_estado_id                   fnx_solicitudes.estado_id%TYPE;
   w_concepto                    fnx_solicitudes.estado_id%TYPE;
   w_obs_pumed                   VARCHAR2 (40);
   v_identificador_pumed         FNX_IDENTIFICADORES.identificador_id%TYPE:=NULL;

-- 2009-04-06 Cursor para obtener los identificadores de TO asociados al identificador de acceso TOIP
   CURSOR c_ident_to_asociados (
      p_identificador_toip   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT a.identificador_id, a.valor
        FROM fnx_configuraciones_identif a, fnx_identificadores b
       WHERE a.caracteristica_id = 4093
         AND a.valor = p_identificador_toip
         AND a.identificador_id = b.identificador_id
         AND b.servicio_id = 'TELRES'
         AND b.producto_id = 'TO'
         AND b.tipo_elemento_id = 'TO'
         AND b.tipo_elemento = 'CMP';

-- Cursor para obtener los datos asociados a la plataforma de numeraciòn
   CURSOR c_plataforma_datos (
      p_plataforma   IN   fnx_prov_plataformas.plataforma_id%TYPE
   )
   IS
      SELECT tipo_central_id
        FROM fnx_prov_plataformas
       WHERE plataforma_id = p_plataforma;

-- Cursor para migracion HUAWEI, Tema de actualización de caracteristica 200 en trabajos.
CURSOR c_equipos_migracion (p_tipo_elemento_id fnx_identificadores.tipo_elemento_id%TYPE,
                            p_identificador_toip fnx_identificadores.identificador_id%TYPE)
   IS
   SELECT EQUIPO_ID
   FROM   FNX_EQUIPOS
   WHERE  identificador_id = p_identificador_toip
   AND    tipo_elemento_id = p_tipo_elemento_id
   AND    estado           = 'OCU';

   -- Cursor para obtener los datos asociados a la plataforma de numeraciòn
   CURSOR c_plataforma_huawei
     ( p_plataforma           IN  FNX_PROV_PLATAFORMAS.plataforma_id%TYPE )
   IS
   SELECT tipo_central_id, provision_cuenta, provision_login
         ,provision_equipment_id, provision_tarjeta_ip, prefijo_equipment_id
   FROM   FNX_PROV_PLATAFORMAS
   WHERE  plataforma_id  = p_plataforma;

   vr_plataf_datos_huawei             c_plataforma_huawei%ROWTYPE;

-- 2009-04-06 Declaración de Variables tabla pl para logica de retiro de lineas TO con accesos TOIP.
   indice                        NUMBER                                   := 0;
   indice_retiro                 NUMBER                                   := 0;
   n_conteo_retiros              NUMBER                                   := 0;
   vt_identif_enacceso           pkg_telefip_util.t_identif_config;
   vt_accestoip_retiro           pkg_telefip_util.t_identif_config;
   vt_identif_aretirar           pkg_telefip_util.t_identif_config;
   vt_equipos_retirar            pkg_telefip_util.tt_term_equipo;
   vt_equipos_cambiar            pkg_telefip_util.tt_term_equipo;
   b_toip_en_tabla               BOOLEAN                              := FALSE;
   b_retiro_acceso               BOOLEAN                              := FALSE;
   b_retiro_equipo               BOOLEAN                              := FALSE;
   b_retcam_equipo               BOOLEAN                              := FALSE;
   v_solicitud_estado            fnx_estados.estado_id%TYPE;
   v_solicitud_concepto          fnx_conceptos.concepto_id%TYPE;
   v_tipo_central                fnx_tipos_centrales.tipo_central_id%TYPE;
   v_plataforma_numeracion       fnx_prov_plataformas.plataforma_id%TYPE;
   e_plataforma_nula             EXCEPTION;
   e_tarjetaip_nula              EXCEPTION;

   vo_control_est_tecnico        fnx_rutas_munic_producto.control_est_tecnico%TYPE;
   vo_crear_solic_ip             fnx_rutas_munic_producto.crear_solicitudes%TYPE;
   vo_mensaje                    VARCHAR2 (200);
   v_proveedor_servicio          fnx_caracteristica_solicitudes.valor%TYPE;

   v_chg_numero_mig_toip         fnx_caracteristica_solicitudes.valor%TYPE:=NULL;
   b_chg_numero_mig_toip         BOOLEAN;

   v_nodo_numeracion            FNX_NODOS_CONMUTADORES.nodo_conmutador_id%TYPE;
   e_nodo_numerac_nulo          EXCEPTION;
   e_muni_numerac_nulo          EXCEPTION;

   b_provisionar_tarjd          BOOLEAN;
   v_mensaje_prov_adsl          VARCHAR2(200);
   
    -- 2013-05-07 Cursor para el tema de nuevo retiro
	CURSOR c_valor_4936 (p_pedido_is FNX_SOLICITUDES.pedido_id%TYPE) IS
	SELECT VALOR
	  FROM FNX_CARACTERISTICA_SOLICITUDES
	 WHERE PEDIDO_ID         = p_pedido_is
	   AND caracteristica_id = 4936
       AND valor             = 'DNR'
       AND rownum            = 1;
	  
	v_valor_4936            c_valor_4936%ROWTYPE;

   -- Cursor para buscar el nodoo de numeraciòn en una serie activa (S) asociadas al municipio y tipo de central
   -- que se pasan comos paràmetros.
   CURSOR c_nodo_numeracion
     ( p_municipio_numeracion   IN   FNX_MUNICIPIOS.municipio_id%TYPE,
       p_tipo_central           IN   FNX_TIPOS_CENTRALES.tipo_central_id%TYPE
     )
   IS
   SELECT nodo_conmutador_id
   FROM   FNX_RANGOS_SERIES
   WHERE  municipio_id      = p_municipio_numeracion
   AND    tipo_central_id   = p_tipo_central
   AND    estado            = 'S'
   ;

   -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caracteres porque los pedidos
   --                          se quedan en petec.
   v_tamano_observacion       NUMBER(4);
   v_nueva_observacion        VARCHAR2(1000);
   v_tamano_nueva_obs         NUMBER(4);

   v_transaccion_retiro       VARCHAR2(10);


   CURSOR c_rutasprov ( p_proveedor           IN FNX_PROVEEDORES_PRODUCTOS.proveedor_id%TYPE
                       ,p_municipio_servicio  IN FNX_PROVEEDORES_PRODUCTOS.municipio_id%TYPE
                       ,p_producto            IN FNX_PROVEEDORES_PRODUCTOS.producto_id%TYPE
                       ,p_tecnologia          IN FNX_RUTAS_PROVISION.tecnologia_id%TYPE
                      ) IS
   SELECT control_puentes, control_ordenes
   FROM   FNX_RUTAS_PROVISION
   WHERE  proveedor_id   = p_proveedor
   AND    municipio_id   = p_municipio_servicio
   AND    producto_id    = p_producto
   AND    tecnologia_id  = p_tecnologia ;

   v_rutasprov     c_rutasprov%ROWTYPE;


-- Cursor para buscar la tecnologia asociada a la configuraciòn de un proveedor, municipio,
-- producto y el uso de servicio  ( para el caso de TO )
CURSOR c_rutmunpro_tecnologia
(  pc_proveedor           IN FNX_PROVEEDORES_PRODUCTOS.proveedor_id%TYPE
  ,pc_municipio           IN FNX_PROVEEDORES_PRODUCTOS.municipio_id%TYPE
  ,pc_producto            IN FNX_PROVEEDORES_PRODUCTOS.producto_id%TYPE
  ,pc_uso_servicio        IN FNX_RUTAS_PROVISION.tecnologia_id%TYPE
)
IS
SELECT tecnologia_id
FROM   FNX_RUTAS_MUNIC_PRODUCTO
WHERE  proveedor_id = pc_proveedor
AND    municipio_id = pc_municipio
AND    producto_id  = pc_producto
AND    criterio_distincion = pc_uso_servicio
;

v_tipo_solicitud       fnx_reportes_instalacion.tipo_solicitud%TYPE;

CURSOR c_solic_toip
IS
   SELECT count(1)
         FROM fnx_solicitudes
        WHERE pedido_id = w_pedido
          AND servicio_id = 'TELRES'
          AND producto_id = 'TO'
          AND tipo_elemento_id = 'TOIP';

nutoip  NUMBER:=0;

--Dojedac - 04/08/2011 - Valiable para canocer la tecnologia del nuevo numero asignado para un cambio de numero.
v_tec_numero            FNX_RANGOS_SERIES.TIPO_CENTRAL_ID%TYPE;

v_linea_portada_NGN             FNX_CONFIGURACIONES_IDENTIF.valor%TYPE;
b_linea_portada_NGN             BOOLEAN;

--Sbuitrab  20/09/2012  Variables para manejo de verificación ciclo correria
v_estado_pumed          fnx_estados.estado_id%TYPE;
v_concepto_pumed        fnx_conceptos.concepto_id%TYPE;
v_observacion_pumed     VARCHAR2(1000);
w_mensaje_pumed         BOOLEAN;
paquete_out             BOOLEAN;

    -- 2013-02-26  Yhernana     Creación variables proceso generación solicitud de recuperación de equipo.
    TYPE regMemory_t IS RECORD (
        EQUIPO_ID            FNX_EQUIPOS.EQUIPO_ID%TYPE,
        MARCA_ID             FNX_EQUIPOS.MARCA_ID%TYPE,
        REFERENCIA_ID        FNX_EQUIPOS.REFERENCIA_ID%TYPE,
        TIPO_ELEMENTO        FNX_EQUIPOS.TIPO_ELEMENTO%TYPE,
        TIPO_ELEMENTO_ID     FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
        TIPO_ELEMENTO_ID_EQU FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
        GENERAR              NUMBER(1)
                                );
    TYPE CUR_TYP IS REF CURSOR;
    c_Equipos_solic CUR_TYP;
    v_cade_Query            VARCHAR2(1000);
    reg_tipo_equipo         regMemory_t;

    v_equipo_obsoleto       VARCHAR2(1);
    v_si_obsoleto           NUMBER(2);
    v_no_obsoleto           NUMBER(2);
    v_equip_obsol           FNX_SOLICITUDES.OBSERVACION%TYPE;
    b_genera_solic_equipo   BOOLEAN;

    -- 2013-02-26  Yhernana     Creación función interna para validar si se genera o no la solicitud de recuperación de equipo.
    FUNCTION fni_generar_solic_equipo (
        p_identificador_id   fnx_solicitudes.identificador_id%TYPE,
        p_tecnologia         fnx_solicitudes.tecnologia_id%TYPE,
        prg_equipo           regMemory_t,
        p_pedido_id             fnx_pedidos.pedido_id%TYPE
    ) RETURN BOOLEAN IS
        b_genera           BOOLEAN;
        v_identif_comparte fnx_solicitudes.identificador_id%TYPE;
        v_equipo_comodato  fnx_configuraciones_equipos.valor%TYPE;
        v_cantidad         NUMBER;

        CURSOR c_caract_equipo (
            prg_datos_equipo     regMemory_t,
            pc_caracteristica_id fnx_tipos_caracteristicas.caracteristica_id%type
        ) IS
            SELECT valor
            FROM fnx_configuraciones_equipos
            WHERE marca_id          = prg_datos_equipo.marca_id
              AND referencia_id     = prg_datos_equipo.referencia_id
              AND tipo_elemento_id  = prg_datos_equipo.tipo_elemento_id
              AND tipo_elemento     = prg_datos_equipo.tipo_elemento
              AND equipo_id         = prg_datos_equipo.equipo_id
              AND caracteristica_id = pc_caracteristica_id;
    BEGIN
        -- Validar infraestructura compartida
        IF prg_equipo.tipo_elemento_id IN ('CABLEM', 'CPE') THEN
            IF p_tecnologia = 'INALA' THEN
                b_genera := TRUE;
            ELSIF p_tecnologia = 'FIBRA' THEN
                b_genera := TRUE;
            ELSIF p_tecnologia = 'HFC' THEN
                v_identif_comparte := fn_comparte_infra_activa (p_identificador_id, p_tecnologia, 'INTER', 'ACCESP', NULL);
                IF v_identif_comparte IS NOT NULL THEN
                    b_genera := FALSE;

                    --Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo tiene infraestructura compartida
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-Infra Comp;' || observacion,1,200)
                        WHERE pedido_id     = p_pedido_id;
                       EXCEPTION
                        WHEN OTHERS THEN
                            NULL;
                    END;
                ELSE
                  b_genera := TRUE;
                END IF;

            ELSIF p_tecnologia = 'REDCO' THEN
                v_identif_comparte := fn_comparte_infra_activa (p_identificador_id, p_tecnologia, 'TELEV', 'INSIP', NULL);
                IF v_identif_comparte IS NULL THEN
                    v_identif_comparte := fn_comparte_infra_activa (p_identificador_id, p_tecnologia, 'INTER', 'ACCESP', NULL);
                END IF;

                IF v_identif_comparte IS NOT NULL THEN
                    b_genera := FALSE;

                    --Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo tiene infraestructura compartida
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-Infra Comp;' || observacion,1,200)
                        WHERE pedido_id     = p_pedido_id;
                       EXCEPTION
                        WHEN OTHERS THEN
                            NULL;
                    END;

                ELSE
                  b_genera := TRUE;
                END IF;
            END IF;

            IF b_genera THEN
                -- Validar equipo compartido
                IF p_tecnologia IN ('REDCO', 'HFC') THEN
                    v_cantidad := fn_comparte_equipo (prg_equipo.marca_id, prg_equipo.referencia_id, prg_equipo.tipo_elemento_id, prg_equipo.equipo_id);

                    IF v_cantidad = 0 THEN
                        b_genera := TRUE;
                    ELSE
                        b_genera := FALSE;

                        --Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es compartido
                        BEGIN
                            UPDATE FNX_PEDIDOS
                            SET observacion =  SUBSTR ('Desap-Equi Comp:' || prg_equipo.equipo_id ||';'|| observacion,1,200)
                            WHERE pedido_id     = p_pedido_id;
                           EXCEPTION
                            WHEN OTHERS THEN
                                NULL;
                        END;
                    END IF;
                ELSE
                    b_genera := TRUE;
                END IF;
            END IF;
        ELSE
            b_genera := TRUE;
        END IF;

        IF b_genera THEN
            -- Validar propiedad del equipo
            OPEN c_caract_equipo (prg_equipo, 1093);
            FETCH c_caract_equipo INTO v_equipo_comodato;
            CLOSE c_caract_equipo;
            v_equipo_comodato := NVL(v_equipo_comodato, 'CD');

            IF v_equipo_comodato IN ('CD', 'AL') THEN
                b_genera := TRUE;
            ELSE
                b_genera := FALSE;

                --Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es propiedad del cliente
                BEGIN
                    UPDATE FNX_PEDIDOS
                    SET observacion =  SUBSTR ('Desap-Equi Client;' || observacion,1,200)
                    WHERE pedido_id     = p_pedido_id;
                   EXCEPTION
                    WHEN OTHERS THEN
                        NULL;
                END;
            END IF;
        END IF;

        RETURN(b_genera);
    END;
    
    -- 2013-02-26  Yhernana     Creación Procedimiento interno para paso de solicitud a ORDEN - PORDE.
    PROCEDURE pri_paso_orden_porde (
        p_pedido_id        fnx_solicitudes.pedido_id%type,
        p_subpedido_id     fnx_solicitudes.subpedido_id%type,
        p_solicitud_id     fnx_solicitudes.solicitud_id%type,
        p_observ_obsol     fnx_solicitudes.observacion%type,
        p_tecnologia       fnx_solicitudes.tecnologia_id%type,
        p_mensaje          out varchar2
    ) IS
    BEGIN
        UPDATE FNX_SOLICITUDES
        SET estado_id     = 'ORDEN',
            concepto_id   = 'PORDE',
            estado_soli   = 'DEFIN',
            tecnologia_id = p_tecnologia,
            observacion = SUBSTR ( p_observ_obsol || observacion,1,2000)
        WHERE pedido_id     = p_pedido_id
          AND subpedido_id  = p_subpedido_id
          AND solicitud_id  = p_solicitud_id;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := 'PR_ESTUDIO_TECNICO_TO:  No puso solicitud en ORDEN-PORDE.  mensaje: '||sqlerrm||'  ('||to_char(sqlcode)||').';
    END;


BEGIN
   -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caracteres porque los pedidos
   --                          se quedan en petec.
   w_parametro             := 1;
   v_tamano_observacion    := NVL(FN_valor_parametros ( 'ESTUD_TEC', w_parametro ), 1000);


   w_soporta_servicios := FALSE;
   w_encontro_numero := FALSE;
   v_total_solic_to_ip := 0;
   w_inconsistencias := FALSE;

   OPEN c_solic;

   LOOP

      FETCH c_solic INTO v_solic;

      EXIT WHEN c_solic%NOTFOUND OR w_inconsistencias;
      p_nueva                       := FALSE;
      p_retiro                      := FALSE;
      p_traslado                    := FALSE;
      p_cambio_numero               := FALSE;
      p_clip                        := FALSE;
      p_cambio_circuito             := FALSE;
      p_cambio_tecnologia           := FALSE;
      p_empaqueta_linea             := FALSE;
      p_telefonia_ip                := FALSE;
      b_linea_portada               := FALSE;
      p_peticion_toip               := FALSE;
      p_serv_toip_cambio            := FALSE;
      p_serv_toip_retir             := FALSE;
      p_cambio_linea_auxiliar       := FALSE;
      b_actualizar_solicitud        := FALSE;
      b_prepago_desconect           := FALSE;
      v_observacion                 := NULL;
      p_cambio_huawei               := FALSE;
      b_chg_numero_mig_toip         := FALSE;
      b_ingre_pumed                 := FALSE;
      b_citofonia                   := FALSE;
      p_camb_fetex                  := FALSE;

      OPEN c_trabaj ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id );

      LOOP
         FETCH c_trabaj INTO v_trabaj;

         EXIT WHEN c_trabaj%NOTFOUND;

         IF v_trabaj.caracteristica_id = 1
            AND                                                -- Instalación nueva
               v_trabaj.tipo_trabajo = 'NUEVO'
         THEN
            p_nueva := TRUE;
         ELSIF     v_trabaj.caracteristica_id = 1
               AND                                             -- Retiro
                   v_trabaj.tipo_trabajo = 'RETIR'
         THEN
            p_retiro := TRUE;
         ELSIF     v_trabaj.caracteristica_id = 1
               AND                                              -- Cambio Número
                   v_trabaj.tipo_trabajo = 'CAMBI'
         THEN
            p_cambio_numero := TRUE;
            -- 2010-02-03  Aacosta
            -- Consideracion de la peticion de Cambio de número para la migracion de TOIP.
            -- Se debe validar si viene en los trabajos de la solicitud un cambio de número
            -- con valor M. Para este caso se debe prender una booleana para poder insertar
            -- la solicitud del EQACCP y DEL EQURED.

            v_chg_numero_mig_toip := PKG_SOLICITUDES.fn_valor_caracteristica
                                        (v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,1);

            IF NVL(v_chg_numero_mig_toip,'NING') = 'M' THEN
               b_chg_numero_mig_toip := TRUE;
            END IF;
         ELSIF     v_trabaj.caracteristica_id IN (34, 35, 38)
               AND                                              -- Traslado
                   v_trabaj.tipo_trabajo = 'CAMBI'
         THEN
            p_traslado := TRUE;
         ELSIF     v_trabaj.caracteristica_id = 28
               AND                                              -- Adicion CLIP
                   v_trabaj.tipo_trabajo = 'NUEVO'
         THEN
            p_clip := TRUE;
         --Dojedac. 04-08/2011 - Cambio de numero para numero FTX
         ELSIF v_trabaj.caracteristica_id = 28 AND v_trabaj.tipo_trabajo = 'CAMBI' THEN
            p_camb_fetex := TRUE;
         ELSIF     v_trabaj.caracteristica_id = 56
               AND                                              -- Cambio de circuito
                   v_trabaj.tipo_trabajo = 'CAMBI'
         THEN
            p_cambio_circuito := TRUE;
         ELSIF     v_trabaj.caracteristica_id = 33
               AND                                              -- Cambio tecnologia
                   v_trabaj.tipo_trabajo = 'CAMBI'
         THEN
            p_cambio_tecnologia := TRUE;

            -- 2008-04-19
            -- Verificar sí hay cambio de tecnologia IP a TDMA
            IF v_trabaj.valor = 'IP'
            THEN
               b_cambio_tecno_ip_tdma := TRUE;
            END IF;
         ELSIF     v_trabaj.caracteristica_id = 2623
               AND                                    -- Cambio línea auxiliar
                   v_trabaj.tipo_trabajo = 'CAMBI'
         THEN
            p_cambio_linea_auxiliar := TRUE;
         ELSIF     v_trabaj.caracteristica_id = 2879
               AND                                               -- Empaquetar
                   v_trabaj.tipo_trabajo = 'CAMBI'
         THEN
            p_empaqueta_linea := TRUE;
         ELSIF     v_trabaj.caracteristica_id IN (3805, 3806, 3807)
               AND v_trabaj.tipo_trabajo = 'NUEVO'
         THEN
            -- La solicitud requiere nuevos servicios de Telefonia IP
            p_telefonia_ip := TRUE;
         ELSIF     v_trabaj.caracteristica_id IN (3805, 3806, 3807)
               AND v_trabaj.tipo_trabajo = 'CAMBI'
         THEN
            -- La solicitud requiere cambio de un servicio de Telefonia IP
            p_serv_toip_cambio := TRUE;
         ELSIF     v_trabaj.caracteristica_id IN (3805, 3806, 3807)
               AND v_trabaj.tipo_trabajo = 'RETIR'
         THEN
            -- La solicitud requiere retiro de servicio de Telefonia IP
            p_serv_toip_retir := TRUE;

         --2010-01-07 Para el caso de la migración a HUAWEI, se debe verificar si viene la 4110 y
         -- si en trabajos viene un cambio
         ELSIF     v_trabaj.caracteristica_id IN (4110)
               AND v_trabaj.tipo_trabajo = 'CAMBI'
         THEN
            p_cambio_huawei := TRUE;
         --2011-03-14 Si viene un tipo de trabajo en nuevo para la caracteristica 49
         -- entonces, coloco en true la variable b_citofonia
         -- 2011-03-15 Se agrega la condición de que se retire la caracteristica 49.
         ELSIF     v_trabaj.caracteristica_id =49
               AND v_trabaj.tipo_trabajo IN ('NUEVO','RETIR')
         THEN
            b_citofonia := TRUE;
         END IF;
      END LOOP;

      CLOSE c_trabaj;

      -- 2011-08-10
      -- Sí la solicitud llega con un cambio de circuito o cambio de circuito CLIP, se debe verificar sí 
      -- el cambio es para una linea que tenga Portabilidad NGN con el subtipo de tecnologia POTS.  
      -- En tal caso, la solicitud
      -- debe pasar al concepto PORDE, pues no es necesario realizar asignación de un nuevo circuito lògico
      -- debido a que la linea ZTE soporta el nuevo servicio.             
      IF p_cambio_circuito OR p_clip THEN
         -- Buscar sí el identificador tiene configurada la característica de Linea Portada NGN.
         v_linea_portada_NGN  := PKG_IDENTIFICADORES.fn_valor_configuracion ( v_solic.identificador_id, 4114 );
         
         IF NVL(v_linea_portada_NGN, 'NOT') = '*8'||v_solic.identificador_id THEN            
            b_linea_portada_NGN := TRUE;
            DBMS_OUTPUT.PUT_LINE('<Identificador con portabilidad NGN: '||v_solic.identificador_id);  
         ELSE
            b_linea_portada_NGN := FALSE;   
         END IF; 
      END IF;
        
      -- Seccion comercial INGRE-PUMED
      IF p_traslado OR p_nueva THEN
      -- 2010-09-21 En adelante se utilizará el procedimiento PR_VERIFICAR_CICLO_CORRERIA
      -- para realizar las verificaciones y saber si un pedido se coloca en concepto PUMED.
              IF  p_nueva THEN
                    v_identificador_pumed := v_solic.identificador_id_nuevo;
              ELSIF p_traslado THEN
                    v_identificador_pumed := v_solic.identificador_id;
              END IF;
             dbms_output.put_line('v_identificador_pumed '||v_identificador_pumed);
               PR_VERIFICAR_CICLO_CORRERIA(v_solic.pedido_id,
                                           v_solic.subpedido_id,
                                           v_solic.solicitud_id,
                                           v_identificador_pumed,
                                           w_estado_id,
                                           w_concepto,
                                           w_obs_pumed);

           IF NVL(w_obs_pumed,'N') <> 'N' THEN
              --2010-09-23 Se actualiza la solicitud a estado PUMED y se sale del ciclo.
                  BEGIN
                        UPDATE fnx_solicitudes
                           SET estado_soli   = 'PENDI',
                               estado_id     = w_estado_id,
                               concepto_id   = w_concepto,
                               observacion   =  '<# ET-TO -> [' || w_concepto || '] #> '||observacion
                         WHERE pedido_id     = v_solic.pedido_id
                           AND subpedido_id  = v_solic.subpedido_id
                           AND solicitud_id  = v_solic.solicitud_id;

                  EXCEPTION
                  WHEN OTHERS THEN
                           DBMS_OUTPUT.put_line
                                       (   '<%ET-TO> Error actualizando solicitud_id PUMED '
                                        || SUBSTR (SQLERRM, 1, 80)
                                        );
                           NULL;
                   END;
                     b_ingre_pumed:=TRUE;
                   EXIT;
           END IF;
        END IF;

      -- SECCIÓN MIGRACIÓN HUAWEI

      -- 2010-01-07 Migración Huawei. Para el caso en que sea un cambio a huawei
      -- insertan dos solicitudes de equipos. EQURED EQACCP.
      -- 2010-02-03 Aacosta
      -- Se modifica la condicion para cambio de huawei para incluir el tema de cambio de identificador
      -- para la migracion de toip.  IF p_cambio_huawei por IF p_cambio_huawei OR b_chg_numero_mig_toip

      IF p_cambio_huawei OR b_chg_numero_mig_toip
      THEN
      DBMS_OUTPUT.put_line ('Pasa FCCU 27 **************** 1' );

         v_solicitud_eqaccp := NULL;
         v_cliente := NULL;

         v_identificador_toip :=
            pkg_identificadores.fn_valor_configuracion
                                                   (v_solic.identificador_id,
                                                    4093
                                                   );
         v_tecnologia_solicitud :=
                 pkg_identificadores.fn_tecnologia (v_identificador_toip);

         pr_solicitud_equipo_crear (v_solic.pedido_id,
                                    v_solic.subpedido_id,
                                    v_solicitud_eqaccp,
                                    v_solic.servicio_id,
                                    v_solic.producto_id,
                                    'EQACCP',
                                    'EQU',
                                    v_tecnologia_solicitud,
                                    v_cliente,
                                    v_identificador_toip,
                                    v_solic.municipio_id,
                                    v_solic.empresa_id,
                                    p_mensaje
                                   );

         IF p_mensaje IS NULL  THEN

            IF v_solicitud_eqaccp IS NOT NULL
            THEN
               UPDATE fnx_solicitudes
                  SET estado_id = 'ORDEN',
                      concepto_id = 'POPTO',
                      estado_soli = 'DEFIN'
                WHERE pedido_id = v_solic.pedido_id
                  AND subpedido_id = v_solic.subpedido_id
                  AND solicitud_id = v_solicitud_eqaccp;
            END IF;

            OPEN  c_equipos_migracion('ATA',v_identificador_toip);
            FETCH c_equipos_migracion
            INTO  v_serial_equipo;
            CLOSE c_equipos_migracion;

           -- v_serial_equipo   :=   PKG_SOLICITUDES.fn_valor_caracteristica ( v_solic.subpedido_id, v_solic.subpedido_id, v_solic.solicitud_id, 200);

            v_solicitud_equred := v_solicitud_eqaccp + 1;
            pr_solicitud_cambi_equred (v_solic.pedido_id,
                                       v_solic.subpedido_id,
                                       v_solic.solicitud_id,
                                       v_solic.servicio_id,
                                       v_solic.producto_id,
                                       'EQURED',
                                       'EQU',
                                       v_identificador_toip,
                                       v_identificador_toip,
                                       v_serial_equipo,
                                       v_marca_equipo,
                                       v_referencia_equipo,
                                       v_terminal_asociado,
                                       v_tecnologia_solicitud,
                                       'TECNI',
                                       'PETEC',
                                       v_solic.municipio_id,
                                       v_solic.empresa_id,
                                       v_solicitud_equred,
                                       p_mensaje
                                      );

            IF p_mensaje IS NULL
            THEN

               IF v_solicitud_equred IS NOT NULL
               THEN
                  UPDATE fnx_solicitudes
                     SET identificador_id_nuevo = v_solic.identificador_id,
                         estado_soli   = 'DEFIN',
                         estado_id     = 'ORDEN',
                         concepto_id   = 'PORDE'
                   WHERE pedido_id     = v_solic.pedido_id
                     AND subpedido_id  = v_solic.subpedido_id
                     AND solicitud_id  = v_solicitud_equred;
               END IF;

                --2010-01-26 Para el tema de migración a HUAWEI, se debe insertar las caracteristicas 3860 y 3861
               OPEN  c_plataforma_huawei ('HUAWEI');
               FETCH c_plataforma_huawei INTO vr_plataf_datos_huawei;
               CLOSE c_plataforma_huawei;

               DBMS_OUTPUT.put_line ('Pasa FCCU 27 **************** 2' );
               IF NVL(vr_plataf_datos_huawei.provision_tarjeta_ip, 'N') = 'S' THEN

                  v_municipio_numeracion := PKG_PROV_INTER_UTIL.fn_munic_numeracion (v_solic.municipio_id);
                  -- Obtener el valor de la tarjeta de plataforma con la cual se debe provisionar el nùmero.
                  v_tarjeta_ip  := PKG_PROV_TARJETAS_IP.FN_NUMERO_TARJETA
                            ( 'HUAWEI', v_municipio_numeracion, v_tecnologia_solicitud, v_solic.pedido_id );

                  IF v_tarjeta_ip IS NULL THEN
                      RAISE e_tarjetaip_nula;
                  END IF;

               END IF;

               IF NVL(vr_plataf_datos_huawei.provision_equipment_id, 'N') = 'S' THEN

                    -- Obtener el valor de login Equipment ID con base en pedido y valor randòmico
                    DBMS_RANDOM.initialize(v_solic.pedido_id);
                    v_valor_random := DBMS_RANDOM.value(1, 9999);
                    v_equipment_id := vr_plataf_datos_huawei.prefijo_equipment_id||v_solic.pedido_id||LPAD(v_valor_random, 4, '0');

                    v_caracteristica_insert := 3860;

                    PKG_SOLCAR_UTIL.pr_insupd_caracteristica
                  ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                    'TELRES', 'TO', 'CMP', 'TO', v_caracteristica_insert,
                    v_solic.municipio_id, v_solic.empresa_id,
                    v_equipment_id, v_mensaje_insupd);

               END IF;

               -- 2010-01-26
               -- Verificaciòn de variable provision_tarjeta_ip  de acuerdo con la plataforma de Numeraciòn
               -- para inserciòn de caracterìstica 3861 <Tarjeta Plataforma IP>

               IF NVL(vr_plataf_datos_huawei.provision_tarjeta_ip, 'N') = 'S' THEN

                     v_caracteristica_insert := 3861;

                     PKG_SOLCAR_UTIL.pr_insupd_caracteristica
                        ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                         'TELRES', 'TO', 'CMP', 'TO', v_caracteristica_insert,
                         v_solic.municipio_id, v_solic.empresa_id,
                         v_tarjeta_ip, v_mensaje_insupd);

                     -- Actualizar el valor de Cantidad_Tramite para la plataforma y tarjeta
                     PKG_PROV_TARJETAS_IP.pr_sumar_tramites ( v_plataforma_numeracion, v_tarjeta_ip, 1 );
               END IF;
            ELSE
               w_inconsistencias := FALSE;
            END IF;

            IF (v_mensaje_insupd IS NOT NULL)
            THEN
               w_inconsistencias := FALSE;
            END IF;

         ELSE
            w_inconsistencias := FALSE;
         END IF;
      END IF;

      --  FIN SECCION MIGRACIÓN HUAWEI

      -- 2008-02-27
      -- Verificar sí la transaccion involucra un telefono IP.
      v_tecnologia_identificador :=
         pkg_identificadores.fn_valor_configuracion (v_solic.identificador_id,
                                                     33);

      IF v_tecnologia_identificador = 'IP'
      THEN
         p_peticion_toip := TRUE;
         DBMS_OUTPUT.put_line ('TOIP**************** 1' );
      END IF;

      -- 2009-05-23
      -- Cambio de Numero.  Se debe evaluar sì el cambio de nùmero se hace para lìnea portada y asi
      -- poder establecer que es una peticiòn IP.
      IF p_cambio_numero AND v_tecnologia_identificador = 'REDCO'
      THEN
         v_linea_voip_portada :=
            pkg_identificadores.fn_valor_configuracion
                                                   (v_solic.identificador_id,
                                                    3827
                                                   );

         IF v_linea_voip_portada IS NOT NULL
         THEN
            p_peticion_toip := TRUE;
         END IF;
      END IF;

      -- 2008-02-13
      -- Determinar enrutamiento de Estudio Técnico del producto con base en el Municipio
      -- de servicio de la solicitud.
      v_municipio_servicio :=
         pkg_solicitudes.fn_valor_caracteristica (v_solic.pedido_id,
                                                  v_solic.subpedido_id,
                                                  v_solic.solicitud_id,
                                                  34
                                                 );

      IF v_municipio_servicio IS NULL
      THEN
         v_municipio_servicio :=
            pkg_identificadores.fn_valor_configuracion
                                                   (v_solic.identificador_id,
                                                    34
                                                   );
      END IF;

      IF v_municipio_servicio IS NOT NULL
      THEN
         -- Buscar el municipio de salida internet asociado al Municipio de servicio.
         v_munic_salida_int :=
                   pkg_prov_inter_util.fn_munic_salida (v_municipio_servicio);

         IF v_munic_salida_int IS NULL
         THEN
            w_mensaje_error :=
                  '<ET-TO> No se encontró municipio internet para ['
               || v_municipio_servicio
               || ']';
            w_inconsistencias := TRUE;
         END IF;
      ELSE
         w_mensaje_error :=
                        '<ET-TO> El municipio de servicio no debe ser nulo. ';
         w_inconsistencias := TRUE;
      END IF;

      -- 2009-03-02
      -- En caso de alguna inconsistencia en la busqueda del municipio de servicio o del municipio
      -- de salida Internet se debe salir del ciclo para que no se realicen otras tareas
      IF w_inconsistencias
      THEN
         EXIT;
      END IF;

      IF p_nueva
      THEN

         -- 2009-12-10
         -- Búsqueda de valor de característica 3870 para determinar el proveedor de Servicio y definir si
         -- se deben crear solicitudes para Telefonia IP
         v_proveedor_servicio :=
            fn_valor_caract_subpedido (v_solic.pedido_id,
                                       v_solic.subpedido_id,
                                       3870
                                      );

         -- 2009-12-10
         -- Sì la variable v_proveedor_servicio es nula, se asigna el valor del campo EMPRESA_ID de la solicitud,
         -- como valor por defecto
         IF v_proveedor_servicio IS NULL
         THEN
            v_proveedor_servicio := v_solic.empresa_id;
         END IF;

      END IF;

      -- 2009-04-04
      -- Para el caso de Telefonia IP nueva, se incrementa la variable que controla
      -- el nùmero de solicitudes de TO con IP.
      IF (p_telefonia_ip AND p_nueva)
      THEN
         v_total_solic_to_ip := v_total_solic_to_ip + 1;
         b_crear_solic_nueva_ip := TRUE;
         -- Busqueda uso del servicio
         v_uso_servicio :=
            NVL
               (pkg_solicitudes.fn_valor_caracteristica (v_solic.pedido_id,
                                                         v_solic.subpedido_id,
                                                         v_solic.solicitud_id,
                                                         2
                                                        ),
                'COM'
               );

         b_nueva_telefonia_ip := TRUE;

         -- 2010-09-23
         -- Condicion p_telefonia_ip AND p_nueva.
         -- Buscar la tecnologìa base asociada al proveedor, municipio, producto y servicio.
         OPEN  c_rutmunpro_tecnologia ( v_proveedor_servicio, v_munic_salida_int, 'TO', v_uso_servicio);
         FETCH c_rutmunpro_tecnologia INTO vo_tecnologia_base;
         IF c_rutmunpro_tecnologia%NOTFOUND THEN
            vo_tecnologia_base  := 'HFC';
         END IF;
         CLOSE c_rutmunpro_tecnologia;

      ELSIF p_nueva
      THEN
        
         -- 2009-09-25
         -- Verificar sì la solicitud tiene la caracterìstica 4110 ( Tipo Telefonia IP ) con el valor IP
         -- En tal caso, el servicio de Telefonia IP fue solicitado de manera expresa y por tanto se
         -- realiza el proceso de creación de solicitudes de TOIP.
         v_tipo_tecno_telefonia :=
            pkg_solicitudes.fn_valor_caracteristica (v_solic.pedido_id,
                                                     v_solic.subpedido_id,
                                                     v_solic.solicitud_id,
                                                     4110
                                                    );
         -- Busqueda uso del servicio
         v_uso_servicio :=
            NVL
               (pkg_solicitudes.fn_valor_caracteristica (v_solic.pedido_id,
                                                         v_solic.subpedido_id,
                                                         v_solic.solicitud_id,
                                                         2
                                                        ),
                'COM'
               );

         IF NVL (v_tipo_tecno_telefonia, 'NOT') <> 'IP'
         THEN
            -- 2009-07-27
            -- Sì la solicitud no trae servicios de Telefonia IP o no se solicita de manera expresa
            -- Telefonia IP, se debe evaluar sì el grupo de proveedor, municipio, producto y uso de servicio,
            -- tiene la configuraciòn que indica sì se deben crear solicitudes IP o no.

            -- 2010-09-22
            -- Invocación de procedimiento PR_RUTAS_MUNIC_PRODUCTO. Cambio de variable vo_tecnologia_acceso
            -- por vo_tecnologia_base

            -- 2009-12-10
            -- Cambio de variable v_solic.empresa_id por v_proveedor_servicio PR_RUTAS_MUNIC_PRODUCTO
            pr_rutas_munic_producto (v_proveedor_servicio,
                                     v_munic_salida_int,
                                     'TO',
                                     v_uso_servicio,
                                     vo_tecnologia_base,
                                     vo_control_est_tecnico,
                                     vo_crear_solic_ip,
                                     vo_mensaje
                                    );

         ELSE
            -- En este caso, se entiende que la solicitud tiene el valor de la caracterìstica tipo tecnologia
            -- con el valor IP y por tanto se deben crear las solicitudes
            vo_crear_solic_ip     := 'S';

            -- 2010-09-22
            -- Cursor c_rutmunpro_tecnologia. Buscar la tecnologìa base asociada al proveedor, municipio, producto y servicio.
            OPEN  c_rutmunpro_tecnologia ( v_proveedor_servicio, v_munic_salida_int, 'TO', v_uso_servicio);
            FETCH c_rutmunpro_tecnologia INTO vo_tecnologia_base;
            IF c_rutmunpro_tecnologia%NOTFOUND THEN
               vo_tecnologia_base  := 'HFC';
            END IF;
            CLOSE c_rutmunpro_tecnologia;
         END IF;

         -- Si la variable vo_crear_solic_ip tiene el valor S, se deben crear las solicitudes que corresponden al
         -- servicio de Telefonia IP.
         IF vo_crear_solic_ip = 'S'
         THEN
            v_total_solic_to_ip := v_total_solic_to_ip + 1;
            b_crear_solic_nueva_ip := TRUE;
            b_nueva_telefonia_ip := TRUE;
            -- Se cambia el valor de la variable p_telefonia_ip a TRUE para que se trate la solicitud
            -- como una solicitud de Telefonia IP.
            p_telefonia_ip := TRUE;
         ELSE
            IF vo_mensaje IS NOT NULL
            THEN
               w_mensaje_error :=
                     '<ET-TO> No se encontró ruta Municipio-Producto '
                  || v_munic_salida_int;
               w_inconsistencias := TRUE;
               EXIT;
            END IF;
         END IF;
      END IF;

      -- 2010-09-23
      -- Mostrar valores de variables en búsqueda de datos para Municipio - Proveedor - Uso Servicio
      -- y generar inconsistencia en variables w_mensaje_error y w_inconsistencias
      IF p_nueva THEN

         IF vo_tecnologia_base IS NOT NULL THEN

            DBMS_OUTPUT.PUT_LINE('<ET-TO> ['||vo_tecnologia_base||'] Tecnologia Base para: '||NVL(v_proveedor_servicio, 'R-PROV')||'-'||
                                     NVL(v_munic_salida_int, 'R-MUN')||'-'||NVL(v_uso_servicio, 'R-USO') );

            DBMS_OUTPUT.PUT_LINE('<ET-TO> Control ET ['||NVL(vo_control_est_tecnico, 'R')||'] * Crear Solic IP ['
                                    ||NVL(vo_crear_solic_ip, 'R')||']');
         ELSE

            w_mensaje_error := '<ET-TO> Ver ruta TO ['|| v_munic_salida_int||'*'||v_proveedor_servicio||']';
            w_inconsistencias := TRUE;

            DBMS_OUTPUT.PUT_LINE('<ET-TO> No se encontró tecnología para: '||NVL(v_proveedor_servicio, 'R-PROV')||'-'||
                                     NVL(v_munic_salida_int, 'R-MUN')||'-'||NVL(v_uso_servicio, 'R-USO') );

            EXIT;

         END IF;
      END IF;

      -- 2009-05-13
      -- Consideraciòn de linea portada. Solicitud de servicios IP para una lìnea con tecnologia tradicional
      IF (p_telefonia_ip AND NOT p_peticion_toip AND NOT p_nueva)
      THEN
         b_linea_portada := TRUE;
         b_crear_solic_nueva_ip := TRUE;
         -- 2009-04-04
         -- Esta asignaciòn se hace para la creaciòn de solicitud de una solicitud de TOIP en el caso de
         -- linea portada
         v_total_solicitud_toip := 1;
      END IF;

      -- 2009-07-27
      -- Se considera la variable b_crear_solic_nueva_ip como criterio para asignar los valores de las
      -- variable v_subpedido_TO y v_solicitud_TO_base para crear las solicitudes nuevas del pedido
      -- ( ejm. EQURED y TOIP. )
      IF b_crear_solic_nueva_ip
      THEN
         v_subpedido_to := v_solic.subpedido_id;
         v_solicitud_to_base := v_solic.solicitud_id;
      END IF;

      -- 2006-10-02
      -- Verificación de paquete en la solución, para actualizar identificador
      -- de paquete en la característica 2879.  Caso de Adición de subpedidos
      OPEN c_paquete_identificador (v_solic.pedido_id);

      FETCH c_paquete_identificador
       INTO v_paquete_identificador;

      IF c_paquete_identificador%NOTFOUND
      THEN
         v_paquete_identificador := NULL;
      END IF;

      CLOSE c_paquete_identificador;

      IF v_paquete_identificador IS NOT NULL
      THEN
         UPDATE fnx_caracteristica_solicitudes
            SET valor = v_paquete_identificador
          WHERE pedido_id = v_solic.pedido_id
            AND subpedido_id = v_solic.subpedido_id
            AND solicitud_id = v_solic.solicitud_id
            AND caracteristica_id = 2879;
      END IF;

      -- SECCION IF Principal para evaluar Tipos de Trabajo.
      IF     p_cambio_numero
         AND NOT p_peticion_toip
         AND NOT p_nueva
         AND NOT p_retiro
         AND NOT p_traslado
         AND NOT p_clip
         AND NOT p_cambio_circuito
         AND NOT p_cambio_tecnologia
      THEN

         -- SECCION CN#.  Evaluación Cambio de Número

         -- 2010-02-12  Jrincong
         -- Cambio de Nùmero.  Verificaciòn de secciones para cambio de número en tecnologìa POTS

         -- Buscar sí el identificador actual tiene infraestructura de red activa
         OPEN  c_ractiva ( v_solic.identificador_id );
         FETCH c_ractiva INTO v_ractiva;
         IF c_ractiva%NOTFOUND
         THEN
            DBMS_OUTPUT.PUT_LINE('<ET-TO> No se encontró REDCO para <'||v_solic.identificador_id||'>');
            b_redco_valida := FALSE;
         ELSE
            -- 2010-02-15
            -- Validaciones para establecer sì la red de cobre activa es vàlida para la transacciòn
            -- de Cambio de Número
            IF NVL (v_ractiva.nodo_conmutador_id, 0) = NVL (v_ractiva.nodo_conmutador_eqd, 0) AND
                v_ractiva.circuito_logico_id IS NOT NULL
            THEN
               b_redco_valida := TRUE;
            ELSIF v_ractiva.nodo_conmutador_id IS NOT NULL AND
                  v_ractiva.subtipo_tecnologia_id = 'POTS'
            THEN
               b_redco_valida := TRUE;
            ELSE
               DBMS_OUTPUT.PUT_LINE('<ET-TO> CXN. Verificar red de cobre <'||v_solic.identificador_id||'>');
               b_redco_valida := FALSE;
            END IF;
         END IF;
         CLOSE c_ractiva;


         IF b_redco_valida  THEN

            -- 2010-02-12
            -- Verificaciòn de variable de circuito_logico_id para el caso de las TO de REDCO
            -- con tecnologia POTS tradicional.
            IF NVL(v_ractiva.subtipo_tecnologia_id, 'REDCO') = 'REDCO'
            THEN
                -- Buscar el tipo de tarjeta del circuito lògico actual
                OPEN  c_cilo ( v_ractiva.nodo_conmutador_eqd, v_ractiva.circuito_logico_id );
                FETCH c_cilo INTO v_tipo_tarjeta;
                CLOSE c_cilo;

            ELSIF v_ractiva.subtipo_tecnologia_id = 'POTS' THEN
               v_tipo_tarjeta  := 'POTSNG';
            END IF;

            -- Buscar el tipo de central para el tipo de tarjeta asociada al circuito.
            OPEN  c_ttarjeta ( v_tipo_tarjeta );
            FETCH c_ttarjeta INTO v_tipo_central;
            CLOSE c_ttarjeta;

            -- 2010-02-15
            -- Determinar el nodo de numeraciòn con base en el subtipo de Tecnologìa.
            IF NVL(v_ractiva.subtipo_tecnologia_id, 'REDCO') = 'REDCO' THEN

               v_nodo_numeracion  := v_ractiva.nodo_conmutador_id;

            ELSIF v_ractiva.subtipo_tecnologia_id = 'POTS' THEN

               -- Determinar el municipio de numeraciòn asociado al municipio de servicio.
               v_municipio_numeracion := PKG_PROV_INTER_UTIL.fn_munic_numeracion (v_municipio_servicio);

               IF v_municipio_numeracion IS NULL THEN
                  RAISE e_muni_numerac_nulo;
               END IF;

               -- Buscar un nodo de numeraciòn de una plataforma que soporta servicios de POTS-NGN.
               -- y del tipo de central determinado.
               OPEN  c_nodo_numeracion ( v_municipio_numeracion, v_tipo_central );
               FETCH c_nodo_numeracion INTO v_nodo_numeracion;
               CLOSE c_nodo_numeracion;

               IF NVL(v_nodo_numeracion, 0) = 0 THEN
                  RAISE e_nodo_numerac_nulo;
               END IF;
            ELSE
               NULL;
            END IF;

            -- Ciclo para verificar los trabajos de categorías y servicios especiales que se deben
            -- configurar para el nuevo número.
            w_soporta_servicios := TRUE;
            OPEN c_trabajos ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id );
            LOOP
               FETCH c_trabajos INTO w_caracteristica;
               EXIT WHEN c_trabajos%NOTFOUND OR NOT w_soporta_servicios;

               -- Verificar si la característica del servicio, es soportada por el tipo de tarjeta.
               OPEN  c_starj ( v_tipo_tarjeta, w_caracteristica );
               FETCH c_starj INTO w_caracteristica_aux;

               IF c_starj%NOTFOUND
               THEN
                  w_soporta_servicios := FALSE;
               END IF;
               CLOSE c_starj;
            END LOOP;
            CLOSE c_trabajos;

            IF w_soporta_servicios
            THEN

               -- Parámetro para el periódo de meses de reserva para el identificador
               w_parametro := 1;
               OPEN  c_parametros ( w_parametro );
               FETCH c_parametros INTO v_param;
               CLOSE c_parametros;

               BEGIN
                  w_reserva_identificador := -1 * TO_NUMBER (NVL (v_param.valor, 6));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     w_reserva_identificador := -6;
               END;

               -- Ciclo para buscar una serie en el nodo de numeraciòn y un número libre asociado a la
               -- serie .
               w_encontro_numero := FALSE;
               OPEN c_serie ( v_nodo_numeracion, v_tipo_central );
               LOOP

                  FETCH c_serie INTO v_serie;
                  EXIT WHEN c_serie%NOTFOUND OR w_encontro_numero;

                  OPEN  c_ident_libre ( v_serie.serie_id, v_serie.numero_inicial, v_serie.numero_final
                                       ,w_reserva_identificador );
                  FETCH c_ident_libre INTO v_identificador_nuevo;

                  IF c_ident_libre%FOUND
                  THEN
                     w_encontro_numero := TRUE;
                  END IF;

                  CLOSE c_ident_libre;
               END LOOP;
               CLOSE c_serie;
            END IF;

            -- Presupuestar el número encontrado
            IF w_encontro_numero
            THEN

                -- Presupuestar red
                BEGIN
                    UPDATE fnx_identificadores
                       SET estado = 'PRE'
                     WHERE identificador_id = v_identificador_nuevo;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                       w_mensaje_error :=
                          '<ET-TO> Error presup. ID <'
                          || SUBSTR(SQLERRM, 1, 30);
                       w_inconsistencias := TRUE;
                END;

                BEGIN
                    UPDATE fnx_identificadores
                       SET estado = 'XLI'
                     WHERE identificador_id = v_solic.identificador_id;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                       NULL;
                END;

            END IF;

            -- SECCION Inserciòn de registro de red de Cobre en Trámite
            IF w_encontro_numero AND NOT w_inconsistencias
            THEN

                OPEN  c_rtramite ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id) ;
                FETCH c_rtramite INTO v_rtramite;

                IF c_rtramite%NOTFOUND
                THEN

                    BEGIN
                       INSERT INTO fnx_inf_redes_tramites
                                  (pedido_id,
                                   subpedido_id,
                                   solicitud_id, agrupador_id,
                                   identificador_id,
                                   nodo_conmutador_id,
                                   armario_id,
                                   strip_id,
                                   par_primario_id,
                                   caja_id,
                                   par_secundario_id,
                                   nodo_conmutador_eqd,
                                   circuito_logico_id,
                                   posicion_fisica_id,
                                   circuito_publico_id,
                                   subtipo_tecnologia_id
                                  )
                       VALUES    ( v_solic.pedido_id,
                                   v_solic.subpedido_id,
                                   v_solic.solicitud_id, NULL,
                                   v_identificador_nuevo,
                                   v_ractiva.nodo_conmutador_id,
                                   v_ractiva.armario_id,
                                   v_ractiva.strip_id,
                                   v_ractiva.par_primario_id,
                                   v_ractiva.caja_id,
                                   v_ractiva.par_secundario_id,
                                   v_ractiva.nodo_conmutador_eqd,
                                   v_ractiva.circuito_logico_id,
                                   v_ractiva.posicion_fisica_id,
                                   v_ractiva.circuito_publico_id,
                                   v_ractiva.subtipo_tecnologia_id
                                 );
                    EXCEPTION
                       WHEN OTHERS
                        THEN
                           w_mensaje_error := '<ET-TO> INS-TRAM'||SUBSTR(SQLERRM, 1,30);
                           w_inconsistencias := TRUE;
                    END;

                ELSE

                   BEGIN
                      UPDATE fnx_inf_redes_tramites
                         SET agrupador_id          = NULL,
                             identificador_id      = v_identificador_nuevo,
                             nodo_conmutador_id    = v_ractiva.nodo_conmutador_id,
                             armario_id            = v_ractiva.armario_id,
                             strip_id              = v_ractiva.strip_id,
                             par_primario_id       = v_ractiva.par_primario_id,
                             caja_id               = v_ractiva.caja_id,
                             par_secundario_id     = v_ractiva.par_secundario_id,
                             nodo_conmutador_eqd   = v_ractiva.nodo_conmutador_eqd,
                             circuito_logico_id    = v_ractiva.circuito_logico_id,
                             posicion_fisica_id    = v_ractiva.posicion_fisica_id,
                             circuito_publico_id   = v_ractiva.circuito_publico_id,
                             subtipo_tecnologia_id = v_ractiva.subtipo_tecnologia_id
                       WHERE CURRENT OF c_rtramite;
                   EXCEPTION
                      WHEN OTHERS
                      THEN
                         w_mensaje_error :=
                               '<ET-TO> UPD-TRAM'||SUBSTR(SQLERRM, 1, 30);
                         w_inconsistencias := TRUE;
                   END;

                END IF;
                CLOSE c_rtramite;

                -- Insertar registros de cables de Interconexio (si aplica)
                OPEN c_inter ( v_solic.identificador_id );
                LOOP
                   FETCH c_inter  INTO v_inter;

                      EXIT WHEN c_inter%NOTFOUND;

                      BEGIN
                         INSERT INTO fnx_interconexiones_tramites
                                      (pedido_id,
                                       subpedido_id,
                                       solicitud_id,
                                       nodo_conmutador_id_red,
                                       nodo_conmutador_id_nro,
                                       strip_red,
                                       strip_numero,
                                       par_de_interconexion_id,
                                       identificador_id,
                                       secuencia, instalado
                                      )
                         VALUES      ( v_solic.pedido_id,
                                       v_solic.subpedido_id,
                                       v_solic.solicitud_id,
                                       v_inter.nodo_conmutador_id_red,
                                       v_inter.nodo_conmutador_id_nro,
                                       v_inter.strip_red,
                                       v_inter.strip_numero,
                                       v_inter.par_interconexion_id,
                                       v_identificador_nuevo,
                                       v_inter.secuencia, 'N'
                                      );
                      EXCEPTION
                          WHEN OTHERS THEN
                             NULL;
                      END;
                END LOOP;
                CLOSE c_inter;

            END IF;

            -- 2010-02-15
            -- SECCION Inserciòn de registro de Eqmul Trámite para el caso que el Identificador anterior
            -- tenga el subtipo de tecnologia POTS.
            IF w_encontro_numero AND NOT w_inconsistencias AND
               v_ractiva.subtipo_tecnologia_id = 'POTS'
            THEN

                -- Inserción registro de red ADSL para la solicitud
                PKG_PROVISION_TARJDATO.pr_insert_adsl_tramite
                  ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
                   ,v_solic.identificador_id
                   ,v_identificador_nuevo
                   ,b_provisionar_tarjd, v_red_provisionada
                   ,w_inconsistencias,   v_mensaje_prov_adsl );

               IF w_inconsistencias  THEN
                   DBMS_OUTPUT.put_line ('<ET-TO> Error provisionando Red EQMUL');
                   w_mensaje_error  := '<ET-TO> '||v_mensaje_prov_adsl;
               END IF;

            END IF;


            -- Actualizaciòn de caracterìsticas de solicitud y cambio de estado y concepto de la solicitud
            IF w_encontro_numero AND NOT w_inconsistencias
            THEN

                -- Actualización de la característica identificador
                BEGIN
                   UPDATE fnx_caracteristica_solicitudes
                   SET    valor        = v_identificador_nuevo
                   WHERE  pedido_id    = v_solic.pedido_id
                   AND    subpedido_id = v_solic.subpedido_id
                   AND    solicitud_id = v_solic.solicitud_id
                   AND    caracteristica_id = 1;
                EXCEPTION
                   WHEN OTHERS
                      THEN
                         w_mensaje_error :=
                             '<ET-TO> ACT-CAR-SOL.'||SUBSTR(SQLERRM, 1, 30);
                        w_inconsistencias := TRUE;
                END;

                -- Actualización de Fecha de Estudio Técnico
                BEGIN
                   UPDATE fnx_reportes_instalacion
                      SET fecha_est_tecnico = SYSDATE
                    WHERE pedido_id    = v_solic.pedido_id
                      AND subpedido_id = v_solic.subpedido_id
                      AND solicitud_id = v_solic.solicitud_id;
                EXCEPTION
                   WHEN OTHERS
                   THEN
                      NULL;
                END;

                -- 2010-03-02
                --  Aobregon  Se modifica la actualizacion de las observaciones para que no superen los 1000 caracteres
                --- porque los pedidos se quedan en petec.
                v_nueva_observacion := ' <# ET-TO [PORDE](AA)>';
                v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                --   Actualización de la solicitud
                BEGIN
                   UPDATE fnx_solicitudes
                      SET concepto_id    = 'PORDE',
                          estado_id      = 'ORDEN',
                          estado_soli    = 'DEFIN',
                          estado_bloqueo = 'N',
                          usuario_id     = NULL,
                          fecha_estado   = SYSDATE,
                          identificador_id_nuevo = v_identificador_nuevo,
                          tecnologia_id  = 'REDCO',
                          observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                    WHERE CURRENT OF c_solic;
                EXCEPTION
                   WHEN OTHERS
                   THEN

                      -- 2010-03-02
                      -- Aobregon    Se modifica la actualizacion de las observaciones para que no superen los
                      -- 1000 caracteres porque los pedidos se quedan en petec.
                      v_nueva_observacion := '<# ET-TO [APROB] (AA)>';
                      v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                      BEGIN
                         UPDATE fnx_solicitudes
                            SET concepto_id    = 'APROB',
                                estado_bloqueo = 'N',
                                usuario_id     = NULL,
                                estado_id      = 'ORDEN',
                                fecha_estado   = SYSDATE,
                                estado_soli    = 'DEFIN',
                                identificador_id_nuevo = v_identificador_nuevo,
                                tecnologia_id  = 'REDCO',
                                observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                          WHERE CURRENT OF c_solic;
                      EXCEPTION
                         WHEN OTHERS
                         THEN
                            w_mensaje_error := '<ET-TO> Error solicitud a APROB.'|| SUBSTR(SQLERRM, 1, 50);
                            w_inconsistencias := TRUE;
                      END;
                END;

                /* Llenado de Servicios Categorias para órdenes de trabajo */
                BEGIN
                   w_respuesta_servicios := NULL;
                   pr_configurar_nuevo_identif
                                           (v_solic.pedido_id,
                                            v_solic.subpedido_id,
                                            v_solic.solicitud_id,
                                            v_solic.identificador_id,
                                            w_respuesta_servicios
                                           );
                EXCEPTION
                   WHEN OTHERS
                   THEN
                      NULL;
                END;

            END IF;

            --DOJEDAC - se inserta la caracteristica 56 si la tecnologia del nuevo numerio es FTX
            -- JGallg - 2011-11-25 - Paso a producción Cola CLIPS
            IF p_camb_fetex THEN
                BEGIN
                    SELECT b.tipo_central_id
                      INTO v_tec_numero
                      FROM fnx_rangos_series b, fnx_tipos_centrales c
                     WHERE c.tipo_central_id = b.tipo_central_id
                       AND b.serie_id        = SUBSTR(v_identificador_nuevo,1,3)
                       AND SUBSTR(v_identificador_nuevo,4,4) BETWEEN numero_inicial AND numero_final;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_tec_numero := NULL;
                    WHEN TOO_MANY_ROWS THEN
                        v_tec_numero := NULL;
                END;

                IF v_tec_numero = 'FTX' THEN
                    -- JGallg - 2011-09-23 - Cola CLIPS: Generar el trabajo con base en el municipio de referencia y no en el de servicio.
                    PKG_PEDIDOS_UTIL.PR_Crear_Trabajo (v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                                                       'TELRES', 'TO', 'CMP', 'TO', 56, 'S', 'CAMBI',
                                                       v_solic.municipio_id, 'UNE', p_mensaje);

                    IF p_mensaje IS NULL THEN
                        UPDATE FNX_TRABAJOS_SOLICITUDES
                           SET VALOR = 'S'
                         WHERE PEDIDO_ID         = v_solic.pedido_id
                           AND SUBPEDIDO_ID      = v_solic.subpedido_id
                           AND SOLICITUD_ID      = v_solic.solicitud_id
                           AND CARACTERISTICA_ID = 56;
                    END IF;
                END IF;
            END IF;
            -- Dojedac fin asignación FETEX
         END IF;            -- FIN SECCION CN#.  Evaluación Cambio de Número.
      ELSIF     p_cambio_numero
            AND p_peticion_toip
            AND NOT p_nueva
            AND NOT p_retiro
            AND NOT p_traslado
            AND NOT p_clip
            AND NOT p_cambio_circuito
            AND NOT p_cambio_tecnologia
      THEN
         DBMS_OUTPUT.put_line
                         ('<%EST-TO%> <TRACK 05> Cambio Número Telefonía IP ');
         -- 2008-04-03
         -- Búsqueda de tecnología del identificador, para ser asignada la solicitud.
         v_identificador_acceso_toip :=
            pkg_identificadores.fn_valor_configuracion
                                                   (v_solic.identificador_id,
                                                    4093
                                                   );
         v_tecnologia_acceso_toip :=
               pkg_identificadores.fn_tecnologia (v_identificador_acceso_toip);
         v_tecnologia_solicitud := NVL (v_tecnologia_acceso_toip, 'REDCO');
         DBMS_OUTPUT.put_line (   '<%EST-TO%> Identificador TOIP     : '
                               || v_identificador_acceso_toip
                              );
         DBMS_OUTPUT.put_line (   '<%EST-TO%> Tecnologia acceso TOIP : '
                               || v_tecnologia_acceso_toip
                              );
         DBMS_OUTPUT.put_line (   '<%EST-TO%> Tecnologia Solicitud   : '
                               || v_tecnologia_solicitud
                              );
         -- 2008-04-10
         -- Buscar sí el número actual corresponden a una línea portada.  En tal caso,
         -- se debe asignar el identificador de TOIP actual que sirve para realizar
         -- la portabilidad de TDMA a VoIP.
         v_linea_voip_portada :=
            pkg_identificadores.fn_valor_configuracion
                                                    (v_solic.identificador_id,
                                                     3827
                                                    );

         IF v_linea_voip_portada IS NOT NULL
         THEN
            DBMS_OUTPUT.put_line (   '<%EST-TO%> Linea Portada <TO - '
                                  || v_solic.identificador_id
                                  || '> Linea VoIP'
                                  || v_linea_voip_portada
                                 );
            w_identificador_toip := v_linea_voip_portada;
         ELSE
            -- Sí el cambio de número no es para una linea portada, se busca un
            -- nuevo número de las series de numeración de VoIp.
            v_municipio_numeracion :=
               pkg_prov_inter_util.fn_munic_numeracion (v_municipio_servicio);
            v_uso_servicio :=
               pkg_identificadores.fn_valor_configuracion
                                                   (v_solic.identificador_id,
                                                    2
                                                   );
            -- 2009-07-24
            -- FN_INFRA_PLATAF_NUMERACION. Esta funciòn se debe utilizar cuando se defina bien la
            -- transacción de Cambio de Número entre plataformas. ( SER->HUAWEI )

            -- 2009-08-25
            -- Se utiliza funciòn para determinar cual es la plataforma de numeraciòn del identificador
            -- actual y asì poder determinar la plataforma del nuevo número.
            v_plataforma_numeracion :=
               fn_rango_serie_plataforma (v_municipio_servicio,
                                          v_solic.identificador_id,
                                          'TO',
                                          'IP',
                                          'S'
                                         );

            IF v_plataforma_numeracion IS NULL
            THEN
               RAISE e_plataforma_nula;
            END IF;

            -- 2009-07-24
            -- Obtener el dato de tipo Central asociado a la plataforma de Numeraciòn
            OPEN c_plataforma_datos (v_plataforma_numeracion);

            FETCH c_plataforma_datos
             INTO v_tipo_central;

            CLOSE c_plataforma_datos;

            -- Búsqueda de identificador libre para presupuestarlo
            --2013-03-07 ETachej - REQ_GeneNuevNumePedi - Ampliar campo pedido
            w_uno  := SUBSTR (w_pedido, length(w_pedido), 1);
            w_dos  := SUBSTR (w_pedido, length(w_pedido)-1, 1);
            w_tres := SUBSTR (w_pedido, length(w_pedido)-2, 1);
            -- Buscar un identificador de telefonía IP.
            pr_traer_id_mun_prod (v_municipio_numeracion,
                                  'TELRES',
                                  'TO',
                                  NULL,
                                  v_tipo_central,
                                  v_plataforma_numeracion,
                                  w_uno || w_dos || w_tres,
                                  w_identificador_toip,
                                  w_mensaje_error
                                 );
         END IF;

         IF w_identificador_toip IS NULL
         THEN
            DBMS_OUTPUT.put_line ('<%EST-TO%> identificador TOIP es nulo ');
            DBMS_OUTPUT.put_line (w_mensaje_error);
            DBMS_OUTPUT.put_line (NVL (v_municipio_numeracion, 'NMN'));
         END IF;

         IF w_identificador_toip IS NOT NULL
         THEN
            w_encontro_numero := TRUE;

            UPDATE fnx_caracteristica_solicitudes
               SET valor = w_identificador_toip
             WHERE pedido_id = v_solic.pedido_id
               AND subpedido_id = v_solic.subpedido_id
               AND solicitud_id = v_solic.solicitud_id
               AND caracteristica_id = 1;

            DBMS_OUTPUT.put_line (   '<%EST-TO%> ID encontrado (M1) '
                                  || w_identificador
                                 );
            v_estado_solicitud := 'ORDEN';
            v_concepto_solicitud := 'PORDE';
         ELSE
            w_encontro_numero := FALSE;
            v_estado_solicitud := 'TECNI';
            v_concepto_solicitud := '21';
         END IF;

         -- 2010-03-02
         -- Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caracteres
         -- porque los pedidos se quedan en petec.
         v_nueva_observacion := '<# ET-TO ['|| v_concepto_solicitud|| '] * '
                      || v_observacion
                      || ' #> ';
         v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

         -- Actualizar datos de las solicitud
         
-- 2012-09-20  Sbuitrab      Se realiza modificación para el tema de ciclo correria, puesto que antes de pasar a estado PORDE se debe verificar
--                           que el pedido si cumpla con los requisitos para la correcta facturación             
        IF   v_concepto_solicitud = 'PORDE' THEN
            pr_estudio_tecnico_pumed(v_solic.pedido_id,v_estado_pumed,v_concepto_pumed,v_observacion_pumed,w_mensaje_pumed,paquete_out);
                IF w_mensaje_pumed THEN
                    v_estado_solicitud:=   v_estado_pumed;
                    v_concepto_solicitud:= v_concepto_pumed;
                    v_nueva_observacion:=  v_observacion_pumed;
                    v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);
                END IF;
        END IF;
        
         BEGIN
            UPDATE fnx_solicitudes
               SET estado_soli            = 'DEFIN',
                   estado_id              = v_estado_solicitud,
                   concepto_id            = v_concepto_solicitud,
                   tecnologia_id          = v_tecnologia_solicitud,
                   identificador_id_nuevo = w_identificador_toip,
                   observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
             WHERE CURRENT OF c_solic;

            b_error_actualizando := FALSE;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line (   '<%EST-TO%> CNIP <ORA> '
                                     || SUBSTR (SQLERRM, 1, 100)
                                    );
               b_error_actualizando := TRUE;
         END;

         IF b_error_actualizando
         THEN

            -- 2010-03-02
            -- Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000
            -- caracteres porque los pedidos se quedan en petec.
            v_nueva_observacion := ' <# ET-TO ['
                         || v_concepto_solicitud
                         || '] * '
                         || v_observacion
                         || ' #> ';
            v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

            -- Actualizar datos de las solicitud
            BEGIN
               UPDATE FNX_SOLICITUDES
                  SET estado_soli    = 'DEFIN',
                      estado_id      = 'ORDEN',
                      concepto_id    = 'APROB',
                      tecnologia_id  = 'REDCO',
                      identificador_id_nuevo = w_identificador_toip,
                      observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                WHERE CURRENT OF c_solic;
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line (   '<%EST-TO%> CNIP APROB <ORA> '
                                        || SUBSTR (SQLERRM, 1, 100)
                                       );
                  NULL;
            END;
         END IF;

         -- Actualizacion de fecha de estudio técnico
         UPDATE fnx_reportes_instalacion
            SET fecha_est_tecnico = SYSDATE
          WHERE pedido_id = v_solic.pedido_id
            AND subpedido_id = v_solic.subpedido_id
            AND solicitud_id = v_solic.solicitud_id;
      ELSIF p_serv_toip_retir OR p_serv_toip_cambio
      THEN
         DBMS_OUTPUT.put_line
                  ('<%EST-TO%> <TRACK 05> Cambio o Retiro de servicios TO IP');

         OPEN c_identificador;

         FETCH c_identificador
          INTO v_identificador;

         CLOSE c_identificador;

         b_actualizar_solicitud := TRUE;
         v_estado_solicitud := 'ORDEN';
         v_concepto_solicitud := 'PORDE';
      ELSIF p_peticion_toip AND p_traslado
      THEN
         DBMS_OUTPUT.put_line ('<%EST-TO%> <TRACK 05> Traslado de TO IP');

         OPEN c_identificador;

         FETCH c_identificador
          INTO v_identificador;

         CLOSE c_identificador;

         b_actualizar_solicitud := TRUE;
         v_estado_solicitud := 'ORDEN';
         v_concepto_solicitud := 'PSERV';

      ELSIF p_peticion_toip AND p_telefonia_ip
      THEN
         DBMS_OUTPUT.put_line ('<%EST-TO%> <TRACK 05> Traslado de TO IP');

         OPEN c_identificador;

         FETCH c_identificador
          INTO v_identificador;

         CLOSE c_identificador;

         b_actualizar_solicitud := TRUE;
         v_estado_solicitud := 'ORDEN';
         v_concepto_solicitud := 'PORDE';
      -- ELSIF 2. Evaluación de retiro de servicio
      ELSIF p_retiro
      THEN
         OPEN c_subpedido;

         FETCH c_subpedido
          INTO v_subpedido;

         CLOSE c_subpedido;

         v_uso_servicio :=
            pkg_identificadores.fn_valor_configuracion
                                                   (v_solic.identificador_id,
                                                    2
                                                   );

         IF p_peticion_toip
         THEN
            v_subpedido_to := v_solic.subpedido_id;
            v_solicitud_to_base := v_solic.solicitud_id;
            v_identificador_acceso_toip :=
               pkg_identificadores.fn_valor_configuracion
                                                   (v_solic.identificador_id,
                                                    4093
                                                   );
            -- 2009-04-07 Se debe llenar la otra tabla pl para realizar la comparación
            indice_retiro := indice_retiro + 1;
            vt_identif_aretirar (indice_retiro).identificador_id :=
                                                      v_solic.identificador_id;
            vt_identif_aretirar (indice_retiro).valor :=
                                                   v_identificador_acceso_toip;
            -- Buscar sì el identificador de acceso TOIP se encuentra ya cargado en la tabla PL/SQL
            -- vt_identif_enAcceso
            b_toip_en_tabla :=
               pkg_telefip_util.fn_buscar_valor (v_identificador_acceso_toip,
                                                 vt_identif_enacceso
                                                );

            IF NOT b_toip_en_tabla
            THEN
               -- Se comienza a recorrer la tabla pl para el identificador TOIP Asociado
               FOR reg IN c_ident_to_asociados (v_identificador_acceso_toip)
               LOOP
                  indice := indice + 1;
                  vt_identif_enacceso (indice) := reg;
               END LOOP;

               b_crear_solicip_retiro := TRUE;

               -- COMMENT.
               -- Las siguientes instrucciones se utilizan para verificaciòn de la tabla
               -- carga
               FOR ind_tabla IN 1 .. vt_identif_enacceso.COUNT
               LOOP
                  DBMS_OUTPUT.put_line
                          (   '<ET-TO> TOIP <'
                           || vt_identif_enacceso (ind_tabla).valor
                           || '>
                           Identificador: '
                           || vt_identif_enacceso (ind_tabla).identificador_id
                          );
               END LOOP;
            ELSE
               DBMS_OUTPUT.put_line
                     ('<ET-TO> TOIP Encontrado en tabla: vt_identif_enAcceso');
            END IF;
         END IF;


         -- 2010-04-27  Se cambia la aperura del cursor c_identificador para el tipo trabajo RETIR, para que siempre sea consultado
         --             independientemente si tiene o no una peticion de TOIP.

         OPEN  c_identificador;
         FETCH c_identificador INTO v_identificador;
         CLOSE c_identificador;

         -- 2009-05-23
         -- Retiro. Seccion Generacion Puentes de Red.  Adiciòn de condiciòn < AND NOT p_peticion_toip >
         -- para no generar puente de Red.

         -- SECCION.  Logica de Generaciòn de PUENTES de Red
         --           Se evalua sí el agrupador es nulo, para identificar que no sea cambio de Producto
         IF v_subpedido.agrupador IS NULL AND NOT p_peticion_toip
         THEN


            IF    NVL (v_identificador.tecnologia_id, 'REDCO') = 'REDCO'
            THEN
               v_transaccion_retiro  := 'RETRCO';
            ELSIF NVL (v_identificador.tecnologia_id, 'REDCO') = 'INALA'
            THEN
               v_transaccion_retiro  := 'RETINA';
            ELSIF NVL (v_identificador.tecnologia_id, 'REDCO') = 'GANPA'
            THEN
               v_transaccion_retiro  := 'RETGPA';
            ELSE
               v_transaccion_retiro  := NULL;    -- Este en la realidad no debe pasar
            END IF;

            -- 2010-03-15
            -- Invocar procedimiento PR_PROV_PUENTES_ORIGEN para inserciòn de puentes de retiro.
            IF v_transaccion_retiro IS NOT NULL THEN

                FENIX.PR_PROV_PUENTES_ORIGEN
                  ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                    'TO', v_transaccion_retiro, v_solic.identificador_id );
            END IF;

            BEGIN
               INSERT INTO FNX_TRABAJOS_ORDENES
                           (pedido_id, subpedido_id,
                            solicitud_id, estado_orden, tipo_instalacion,
                            rpuente, estado_bloqueo
                           )
                    VALUES (v_solic.pedido_id, v_solic.subpedido_id,
                            v_solic.solicitud_id, 'APR', 'N',
                            'N', 'N'
                           );

            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     UPDATE FNX_TRABAJOS_ORDENES
                        SET estado_orden = 'APR',
                            tipo_instalacion = 'N',
                            rpuente = 'N',
                            estado_bloqueo = 'N'
                      WHERE pedido_id = v_solic.pedido_id
                        AND subpedido_id = v_solic.subpedido_id
                        AND solicitud_id = v_solic.solicitud_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
            END;

            -- 2010-03-15
            -- Logica generaciòn puentes de Interconexiones, se traslada para proc. PR_PROV_PUENTES_ORIGEN

         END IF;

         -- FIN SECCION.  Logica de Generaciòn de Puentes

         -- SECCION.  Determinar estado y concepto de la solicitud de Retiro
         IF v_subpedido.agrupador IS NULL AND NOT w_inconsistencias
         THEN
            BEGIN
               UPDATE FNX_REPORTES_INSTALACION
                  SET fecha_est_tecnico = SYSDATE
                WHERE pedido_id = v_solic.pedido_id
                  AND subpedido_id = v_solic.subpedido_id
                  AND solicitud_id = v_solic.solicitud_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            BEGIN
               UPDATE FNX_IDENTIFICADORES
                  SET estado = 'XLI'
                WHERE identificador_id = v_solic.identificador_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            b_actualizar_solicitud := TRUE;
            v_estado_solicitud     := 'ORDEN';
            v_concepto_solicitud   := 'PORDE';

         -- 2009-04-11
         -- Se quita lógica de actualización de solicitud.  Se maneja de manera centralizada
         -- en la sección de actualización de la solicitud.

         END IF;

         -- SECCION.  Evaluación de agrupador para identificar sí es CAMBIO DE PRODUCTO
         IF v_subpedido.agrupador IS NOT NULL
         THEN
            -- Agrupador subpedido no es nulo. Normalmente una solicitud de Cambio de Producto.
            BEGIN
               UPDATE fnx_reportes_instalacion
                  SET fecha_est_tecnico = SYSDATE
                WHERE pedido_id = v_solic.pedido_id
                  AND subpedido_id = v_solic.subpedido_id
                  AND solicitud_id = v_solic.solicitud_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            -- 2009-04-11
            -- Se quita lógica de actualización de solicitud.  Se maneja de manera centralizada
            -- en la sección de actualización de la solicitud.
            b_actualizar_solicitud := TRUE;
            v_estado_solicitud := 'ORDEN';
            v_concepto_solicitud := 'PCPRO';

            BEGIN
               UPDATE fnx_solicitudes
                  SET identificador_id = v_solic.identificador_id
                WHERE pedido_id = v_solic.pedido_id
                  AND subpedido_id <> v_solic.subpedido_id
                  AND subpedido_id IN (
                         SELECT subpedido_id
                           FROM fnx_subpedidos
                          WHERE pedido_id = v_solic.pedido_id
                            AND agrupador = v_subpedido.agrupador)
                  AND identificador_id IS NULL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;
         END IF;
      -- FIN SECCION.  Evaluación de agrupador para identificar sí es CAMBIO DE PRODUCTO
      ELSIF     p_empaqueta_linea
            AND p_cambio_linea_auxiliar
            AND NOT p_cambio_numero
            AND NOT p_nueva
            AND NOT p_retiro
            AND NOT p_traslado
            AND NOT p_clip
            AND NOT p_cambio_circuito
            AND NOT p_cambio_tecnologia
      THEN
         -- Configurar identificador con características de solicitud.
         pr_configurar_identificador (v_solic.pedido_id,
                                      v_solic.subpedido_id,
                                      v_solic.solicitud_id,
                                      v_solic.identificador_id,
                                      v_solic.identificador_id,
                                      v_cliente,
                                      v_agrupador,
                                      'CAMBI',
                                      v_solic.municipio_id,
                                      v_solic.empresa_id
                                     );
         -- Configurar Caracteristica 2623 Identificador de Paquete
         v_identificador_paquete :=
            fn_valor_caracteristica_sol (v_solic.pedido_id,
                                         v_solic.subpedido_id,
                                         v_solic.solicitud_id,
                                         2879
                                        );

         IF v_identificador_paquete IS NOT NULL
         THEN
            pr_insertar_conf_ident (v_identificador_paquete,
                                    2624,
                                    v_solic.identificador_id
                                   );
         END IF;

         b_actualizar_solicitud := TRUE;
         v_estado_solicitud := 'FACTU';
         v_concepto_solicitud := 'PLECT';
      -- 2006-12-19
      -- Verificación transacción de Nueva TO con característica de PrePagos
      -- Desconectado.
      ELSIF     NOT p_cambio_numero
            AND p_nueva
            AND NOT p_telefonia_ip
            AND NOT p_retiro
            AND NOT p_traslado
            AND NOT p_clip
            AND NOT p_cambio_circuito
            AND NOT p_cambio_tecnologia
      THEN
         v_tipo_venta :=
            pkg_solicitudes.fn_valor_caracteristica (v_solic.pedido_id,
                                                     v_solic.subpedido_id,
                                                     v_solic.solicitud_id,
                                                     1339
                                                    );

         IF v_tipo_venta = 'PRED'
         THEN
            -- Buscar el identificador de reinstalación en la solicitud. Car 3041
            v_ident_reinstalacion :=
               pkg_solicitudes.fn_valor_caracteristica (v_solic.pedido_id,
                                                        v_solic.subpedido_id,
                                                        v_solic.solicitud_id,
                                                        3041
                                                       );

            IF v_ident_reinstalacion IS NOT NULL
            THEN
               -- Verificar estado de identificador de reinstalacion
               v_estado_identificador :=
                        pkg_identificadores.fn_estado (v_ident_reinstalacion);
               v_estado_servicio :=
                  pkg_identificadores.fn_valor_configuracion
                                                      (v_ident_reinstalacion,
                                                       132
                                                      );

               IF     NVL (v_estado_identificador, 'REV') = 'OCU'
                  AND NVL (v_estado_servicio, 'REV') = 'DCOB'
               THEN
                  b_actualizar_solicitud := TRUE;
                  b_prepago_desconect := TRUE;
                  v_estado_solicitud := 'FACTU';
                  v_concepto_solicitud := 'PLECT';
                  v_observacion := 'PRE-DCNX';
               END IF;
            END IF;
         ELSE
            DBMS_OUTPUT.put_line ('<%EST-TO%>;NUEVA TO;PROVISION');
         END IF;
      -- 2008-04-19
      -- SECCION.  Transacción Cambio de Tecnologia de IP a TDMA.  Provisión red de
      --           trámite e inserción de puentes.
      ELSIF     NOT p_cambio_numero
            AND NOT p_nueva
            AND NOT p_retiro
            AND NOT p_traslado
            AND NOT p_clip
            AND NOT p_cambio_circuito
            AND p_cambio_tecnologia
            AND b_cambio_tecno_ip_tdma
      THEN
         DBMS_OUTPUT.put_line (   '<%EST-TO%> Cambio Tecnologia IP a TDMA '
                               || v_solic.identificador_id
                              );
         -- Verificar sí la línea es una linea de TDMA portada a TOIP
         v_linea_voip_portada :=
            pkg_identificadores.fn_valor_configuracion
                                                    (v_solic.identificador_id,
                                                     3827
                                                    );

         IF v_linea_voip_portada IS NOT NULL
         THEN
            DBMS_OUTPUT.put_line
               ('<%EST-TO%> La linea actual es una linea portada que soporta TOIP'
               );
            b_portada_ip_tdma := TRUE;
            -- Buscar el identificador de acceso TOIP-abcx asociado a la linea
            v_identificador_acceso_toip :=
               pkg_identificadores.fn_valor_configuracion
                                                    (v_solic.identificador_id,
                                                     4093
                                                    );
            v_tecnologia_acceso_toip :=
               pkg_identificadores.fn_tecnologia (v_identificador_acceso_toip);
            -- Buscar sí el acceso de TOIP tiene asociado un identificador de internet banda ancha
            v_identif_internet_ba :=
               pkg_identificadores.fn_valor_configuracion
                                                    (v_solic.identificador_id,
                                                     2093
                                                    );
            -- Buscar sí el acceso de TOIP tiene asociado un identificador de TV ADSL
            v_identif_telev_adsl :=
               pkg_identificadores.fn_valor_configuracion
                                                    (v_solic.identificador_id,
                                                     2087
                                                    );
            DBMS_OUTPUT.put_line (   '<%EST-TO%> Identificador TOIP     : '
                                  || v_identificador_acceso_toip
                                 );
            DBMS_OUTPUT.put_line (   '<%EST-TO%> Tecnologia acceso TOIP : '
                                  || v_tecnologia_acceso_toip
                                 );

            IF v_identif_internet_ba IS NOT NULL
            THEN
               DBMS_OUTPUT.put_line
                  (   '<%EST-TO%> Identificador TOIP tiene Internet asociado '
                   || v_identif_internet_ba
                  );
            ELSE
               DBMS_OUTPUT.put_line
                  ('<%EST-TO%> Identificador TOIP no tiene Internet asociado '
                  );
            END IF;

            IF v_identif_telev_adsl IS NOT NULL
            THEN
               DBMS_OUTPUT.put_line
                  (   '<%EST-TO%> Identificador TOIP tiene televisión ADSL asociada '
                   || v_identif_internet_ba
                  );
            ELSE
               DBMS_OUTPUT.put_line
                  ('<%EST-TO%> Identificador TOIP no tiene televisión ADSL asociada '
                  );
            END IF;

            -- SECCION. RED TRAMITE
            -- Invocar rutina para provisionar Red de cobre con base en la red actual
            PKG_PROVISION_BAREDCO.pr_red_existente (v_solic.pedido_id,
                                                    v_solic.subpedido_id,
                                                    v_solic.solicitud_id,
                                                    v_solic.identificador_id,
                                                    v_solic.identificador_id,
                                                    b_provisionar_red,
                                                    v_red_provisionada,
                                                    b_inconsistencia,
                                                    v_mensaje_prov_red
                                                   );
            DBMS_OUTPUT.put_line (   '<%EST-TO%> RED-REDCO>'
                                  || v_solic.pedido_id
                                  || ';'
                                  || v_mensaje_prov_red
                                 );

            -- SECCION: DEFINIR ESTADO Y CONCEPTO DE LA SOLICITUD
            IF b_provisionar_red AND v_red_provisionada = 'COMPLETA'
            THEN
               v_estado_solicitud := 'ORDEN';
               v_concepto_solicitud := 'PORDE';
               b_actualizar_solicitud := TRUE;
               v_observacion :=
                   ' (Cambio tecnologia IP-REDCO - Asignación Red Anterior )';
            ELSIF b_provisionar_red AND v_red_provisionada = 'PARCIAL'
            THEN
               v_estado_solicitud := 'TECNI';
               v_concepto_solicitud := 'PETEC';
               b_actualizar_solicitud := TRUE;
               v_observacion :=
                         ' (Cambio tecnologia IP-REDCO) - Sin asignación Red';
            END IF;

            -- SECCION.  GENERACION DE PUENTES
            -- Se deben generar de acuerdo a la tecnología del acceso actual
            -- ( identificador TOIP ) y sí el servicio tiene banda ancha o TV ADSL
            IF     b_provisionar_red
               AND v_red_provisionada = 'COMPLETA'
               AND v_tecnologia_acceso_toip = 'REDCO'
            THEN
               IF    v_identif_internet_ba IS NOT NULL
                  OR v_identif_telev_adsl IS NOT NULL
               THEN
                  -- Generar un registro IN para restablecer el puente del DSLAM Conmutado al
                  -- distribuidor vertical
                  puente_red := 'IN';
                  pkg_prov_puentes.pr_insertar_puente (v_solic.pedido_id,
                                                       v_solic.subpedido_id,
                                                       v_solic.solicitud_id,
                                                       puente_red,
                                                       NULL
                                                      );
               ELSE
                  -- Generar un registro IB para restablecer el puente del cable normal.
                  -- Del distribuidor vertical al strip
                  puente_red := 'IB';
                  pkg_prov_puentes.pr_insertar_puente (v_solic.pedido_id,
                                                       v_solic.subpedido_id,
                                                       v_solic.solicitud_id,
                                                       puente_red,
                                                       NULL
                                                      );
                  -- Generar un registro RM para retirar el puente del dedicado del DSLAM
                  -- al strip
                  puente_red := 'RM';
                  pkg_prov_puentes.pr_insertar_puente (v_solic.pedido_id,
                                                       v_solic.subpedido_id,
                                                       v_solic.solicitud_id,
                                                       puente_red,
                                                       NULL
                                                      );
               END IF;
            ELSIF     b_provisionar_red
                  AND v_red_provisionada = 'COMPLETA'
                  AND v_tecnologia_acceso_toip = 'HFC'
            THEN
               -- Generar un registro IB para restablecer el puente del cable normal
               puente_red := 'IB';
               pkg_prov_puentes.pr_insertar_puente (v_solic.pedido_id,
                                                    v_solic.subpedido_id,
                                                    v_solic.solicitud_id,
                                                    puente_red,
                                                    NULL
                                                   );
            END IF;
         ELSE
            DBMS_OUTPUT.put_line
                           ('<%EST-TO%> La linea actual es una linea de TOIP');
            DBMS_OUTPUT.put_line
                          ('<%EST-TO%> TRANSACCION EN PROCESO DE DEFINICION ');
         END IF;
      -- 2009-04-04
      -- Consideraciòn de estado y concepto ORDEN - PSERV para actualizar la solicitud de TO
      -- nueva con servicios de telefonia IP.
      ELSIF p_telefonia_ip AND p_nueva
      THEN
         b_actualizar_solicitud := TRUE;
         v_estado_solicitud     := 'ORDEN';
         v_concepto_solicitud   := 'PSERV';
         DBMS_OUTPUT.put_line ('Pasa FCCU 27 **************** 3' );

         /*-- 2011-03-30 En adiciones de TO en TOIP la TO debe pasar a concepto PORDE para la generracion de Ordenes.
         nutoip:=0;
         OPEN c_solic_toip;
         FETCH c_solic_toip INTO nutoip;
         CLOSE c_solic_toip;

         IF nutoip < 1
         THEN
             v_estado_solicitud     := 'ORDEN';
             v_concepto_solicitud   := 'PORDE';
         END IF;*/

      ELSIF p_telefonia_ip AND NOT p_peticion_toip
      THEN
        DBMS_OUTPUT.put_line ('Pasa FCCU 27 **************** 5' );
         b_actualizar_solicitud := TRUE;
         v_estado_solicitud := 'ORDEN';
         v_concepto_solicitud := 'PSERV';
      -- 2010-01-07 Para este caso, la solicitud de TO, debe pasar a PORDE
      ELSIF p_cambio_huawei
      THEN
        DBMS_OUTPUT.put_line ('Pasa FCCU 27 **************** 4' );
         b_actualizar_solicitud := TRUE;
         v_estado_solicitud := 'ORDEN';
         v_concepto_solicitud := 'PORDE';
         v_tecnologia_solicitud:=pkg_identificadores.fn_tecnologia (v_solic.identificador_id);
      -- 2011-03-14  Consideraciones a tener en cuenta para cuando se trata
      --             de citofonia:
      --             1) Si el identificador, no tiene valor en el campo agrupador de la tabla FNX_IDENTIFICADORES y el tipo de solicitud
      --                en la tabla FNX_REPORTES_INSTALACION es de tipo CAMBI,la solicitud debe quedar en ORDEN - PORDE.
      --             2) Por otro lado, si el identificador tiene agrupador, se debe llamar al procedimiento PR_CUMPLIDO_PETICIONES.
      --                Si ese procedimiento, no devuelve ningún error, las solicitud debe quedar en FACTU-PFACT.
      ELSIF b_citofonia THEN
            v_agrupador_to:=FN_AGRUPADOR_IDENTIFICADOR(v_solic.identificador_id);

            v_tipo_solicitud:=FN_TIPO_SOLICITUD(v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id);


            IF NVL(v_agrupador_to,'N')='N' AND NVL(v_tipo_solicitud,'N')='CAMBI' THEN
                   b_actualizar_solicitud := TRUE;
                   v_estado_solicitud     := 'ORDEN';
                   v_concepto_solicitud   := 'PORDE';
                   v_tecnologia_solicitud :=  pkg_identificadores.fn_tecnologia (v_solic.identificador_id);
            ELSIF NVL(v_agrupador_to,'N')<>'N'
              AND NVL(v_tipo_solicitud,'N')='CAMBI' THEN
                  pr_cumplido_peticiones (v_solic.pedido_id,
                                          v_solic.subpedido_id,
                                          v_solic.solicitud_id,
                                          SYSDATE,
                                          p_mensaje);
                 IF p_mensaje IS NULL THEN
                    b_actualizar_solicitud := TRUE;
                    v_estado_solicitud     := 'FACTU';
                    v_concepto_solicitud   := 'PFACT';
                    v_tecnologia_solicitud :=  pkg_identificadores.fn_tecnologia (v_solic.identificador_id);
                  END IF;

            END IF;
            
      ELSIF b_linea_portada_NGN THEN

         -- 2011-08-11
         -- Transacción de adiciòn de servicios para una linea que tiene portabilidad NGN.
         -- Buscar sí el identificador actual tiene infraestructura de red activa.
         
         -- Buscar el subtipo de tecnologia del identificador para determinar sí efectivamente se
         -- trata de una linea POTS-NGN  
         OPEN  c_ractiva_subtipo ( v_solic.identificador_id );
         FETCH c_ractiva_subtipo INTO v_ractiva_subtipo;
         IF c_ractiva_subtipo%NOTFOUND
         THEN
            DBMS_OUTPUT.PUT_LINE('<ET-TO> No se encontró REDCO para <'||v_solic.identificador_id||'>');
            b_redco_valida := FALSE;
         ELSE
            IF v_ractiva_subtipo.nodo_conmutador_id IS NOT NULL AND v_ractiva_subtipo.subtipo_tecnologia_id = 'POTS'
            THEN
               b_redco_valida := TRUE;
            ELSE
               DBMS_OUTPUT.PUT_LINE('<ET-TO> . Verificar red de cobre. Tecnologia POTS <'||v_solic.identificador_id||'>');
               b_redco_valida := FALSE;
            END IF;
         END IF;
         CLOSE c_ractiva_subtipo;
         
         IF b_redco_valida THEN
            DBMS_OUTPUT.PUT_LINE('<Portabilidad NGN> Provisión Red de Cobre'); 
            -- Invocar rutina para provisionar Red de cobre con base en la red actual
            PKG_PROVISION_BAREDCO.pr_insert_red_tramite (v_solic.pedido_id,
                                                    v_solic.subpedido_id,
                                                    v_solic.solicitud_id,
                                                    v_solic.identificador_id,
                                                    v_solic.identificador_id,
                                                    b_provisionar_red,
                                                    v_red_provisionada,
                                                    b_inconsistencia,
                                                    v_mensaje_prov_red
                                                   );
                                                   
            DBMS_OUTPUT.put_line (   '<%EST-TO%> RED-REDCO>'|| v_solic.pedido_id|| ';'|| v_mensaje_prov_red);

            IF b_provisionar_red
            THEN
                DBMS_OUTPUT.PUT_LINE('<Portabilidad NGN> Provisión Tarjeta Puerto');
                -- Inserción registro de red ADSL para la solicitud
                PKG_PROVISION_TARJDATO.pr_insert_adsl_tramite
                  ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
                   ,v_solic.identificador_id
                   ,v_solic.identificador_id
                   ,b_provisionar_tarjd, v_red_provisionada
                   ,w_inconsistencias,   v_mensaje_prov_adsl );

               IF w_inconsistencias  THEN
                   DBMS_OUTPUT.put_line ('<ET-TO> Error provisionando Red EQMUL');
                   w_mensaje_error  := '<ET-TO> '||v_mensaje_prov_adsl;
               END IF;
               
               -- Sí se pudo realizar la inserción de la red en trámite, la solicitud
               -- debe pasar al estado de ORDENES.
               IF b_provisionar_tarjd  AND v_red_provisionada = 'COMPLETA'
               THEN
                   v_estado_solicitud := 'ORDEN';
                   v_concepto_solicitud := 'PORDE';
                   b_actualizar_solicitud := TRUE;
                   v_observacion := '(Adicion SE - PortabNGN )';                            
               END IF;
            END IF;
                     
         END IF;
                     
      ELSE
         DBMS_OUTPUT.put_line
                             ('<%EST-TO%>;TO;TRABAJOS REQUIEREN ASIGNACIONES');
      END IF;

      -- FIN IF Principal para evaluar Tipos de Trabajo.
      IF p_telefonia_ip AND p_nueva
      THEN
         v_tecnologia_solicitud := NULL;
      --2010-01-08 Se debe verificar la tecnología de acuerdo al valor que tiene en la tabla de identificadores
      ELSIF p_cambio_huawei
      THEN
        v_tecnologia_solicitud:=pkg_identificadores.fn_tecnologia (v_solic.identificador_id);
      ELSE
         v_tecnologia_solicitud :=
                                 NVL (v_identificador.tecnologia_id, 'REDCO');
      END IF;

      -- SECCION.  Actualizaciòn de la solicitud con el estado y concepto definido en cada
      --           transaccion.
      -- 2010-09-28 Se agrega la condición de que no sea un pedido de INGRE-PUMED.
      IF b_actualizar_solicitud AND NOT b_ingre_pumed
      THEN
         DBMS_OUTPUT.put_line
                             ('<%EST-TO%> [TRACK 11] Actualizando solicitud ');

         -- 2010-03-02
         -- Aobregon. Se modifica la actualizacion de las observaciones para que no superen los 1000 caracteres
         -- porque los pedidos se quedan en petec.
         v_nueva_observacion := ' <# ET-TO ['
                      || v_concepto_solicitud
                      || '] * '
                      || v_observacion
                      || ' #>';
         v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);
         
         
-- 2012-09-20  Sbuitrab      Se realiza modificación para el tema de ciclo correria, puesto que antes de pasar a estado PORDE se debe verificar
--                           que el pedido si cumpla con los requisitos para la correcta facturación             
        IF   v_concepto_solicitud = 'PORDE' THEN
            pr_estudio_tecnico_pumed(v_solic.pedido_id,v_estado_pumed,v_concepto_pumed,v_observacion_pumed,w_mensaje_pumed,paquete_out);
                IF w_mensaje_pumed THEN
                    v_estado_solicitud:=   v_estado_pumed;
                    v_concepto_solicitud:= v_concepto_pumed;
                    v_nueva_observacion:=  v_observacion_pumed;
                    v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);
                END IF;
        END IF;
        
        DBMS_OUTPUT.put_line ('Actualizacion FCCU********' );
        DBMS_OUTPUT.PUT_LINE ( 'v_estado_solicitud = ' || v_estado_solicitud );
        DBMS_OUTPUT.PUT_LINE ( 'v_concepto_solicitud = ' || v_concepto_solicitud );
        DBMS_OUTPUT.PUT_LINE ( 'v_tecnologia_solicitud = ' || v_tecnologia_solicitud );
        DBMS_OUTPUT.PUT_LINE ( 'v_nueva_observacion|' || v_nueva_observacion );
        
         BEGIN
            UPDATE fnx_solicitudes
               SET estado_soli   = 'DEFIN',
                   estado_id     = v_estado_solicitud,
                   concepto_id   = v_concepto_solicitud,
                   tecnologia_id = v_tecnologia_solicitud,
                   observacion   =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
             WHERE CURRENT OF c_solic;
         EXCEPTION
            WHEN OTHERS
            THEN
               IF v_concepto_solicitud = 'PORDE'
               THEN

                   -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen
                  -- los 1000 caracteres porque los pedidos se quedan en petec.
                   v_nueva_observacion := ' <# ET-TO ['
                               || v_concepto_solicitud
                               || '] * '
                               || v_observacion
                               || ' #>';
                   v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                  BEGIN
                     UPDATE fnx_solicitudes
                        SET estado_soli    = 'DEFIN',
                            estado_id      = 'ORDEN',
                            concepto_id    = 'APROB',
                            tecnologia_id  = v_tecnologia_solicitud,
                            observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                      WHERE CURRENT OF c_solic;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        DBMS_OUTPUT.put_line
                               (   '<%EST-TO%> Error actualizando solicitud '
                                || 'ORDEN'
                                || '-'
                                || 'PORDE'
                               );
                        NULL;
                  END;
               ELSE
                  DBMS_OUTPUT.put_line
                               (   '<%EST-TO%> Error actualizando solicitud '
                                || v_estado_solicitud
                                || '-'
                                || v_concepto_solicitud
                               );
                  NULL;
               END IF;
         END;

         -- 2009-02-24
         -- Actualización de la lógica para cuando el pedido queda en FACTU - PLECT actualice la
         -- fecha de instalacion en FNX_REPORTES_INSTALACION
         IF     b_actualizar_solicitud
            AND v_estado_solicitud = 'FACTU'
            AND v_concepto_solicitud = 'PLECT'
         THEN
            UPDATE fnx_reportes_instalacion
               SET fecha_instalacion = SYSDATE,
                   fecha_est_tecnico = SYSDATE
             WHERE pedido_id = v_solic.pedido_id
               AND subpedido_id = v_solic.subpedido_id
               AND solicitud_id = v_solic.solicitud_id;
         ELSE
            -- Actualizacion de fecha de estudio técnico
            UPDATE fnx_reportes_instalacion
               SET fecha_est_tecnico = SYSDATE
             WHERE pedido_id = v_solic.pedido_id
               AND subpedido_id = v_solic.subpedido_id
               AND solicitud_id = v_solic.solicitud_id;
         END IF;
      END IF;                                       -- Si actualizar solicitud

      -- Sí es una reinstalacion de prepago desconectado activo, se actualiza
      -- el identificador nuevo de la solicitud y se debe dar cumplido
      -- a la solicitud.
      IF b_actualizar_solicitud AND b_prepago_desconect
      THEN
         -- Actualización de estado y concepto de solicitud
         UPDATE fnx_solicitudes
            SET identificador_id_nuevo = v_ident_reinstalacion
          WHERE CURRENT OF c_solic;

         UPDATE fnx_caracteristica_solicitudes
            SET valor = v_ident_reinstalacion
          WHERE pedido_id = v_solic.pedido_id
            AND subpedido_id = v_solic.subpedido_id
            AND solicitud_id = v_solic.solicitud_id
            AND caracteristica_id = 1;

         -- Actualizacion de fecha de instalación
         UPDATE fnx_reportes_instalacion
            SET fecha_instalacion = SYSDATE
          WHERE pedido_id = v_solic.pedido_id
            AND subpedido_id = v_solic.subpedido_id
            AND solicitud_id = v_solic.solicitud_id;

         -- Configurar identificador con características de solicitud.
         pr_configurar_identificador (v_solic.pedido_id,
                                      v_solic.subpedido_id,
                                      v_solic.solicitud_id,
                                      v_ident_reinstalacion,
                                      v_ident_reinstalacion,
                                      v_cliente,
                                      v_agrupador,
                                      'NUEVO',
                                      v_solic.municipio_id,
                                      v_solic.empresa_id
                                     );

         -- 2006-12-26
         -- PrePago Desconectado.   Actualización de estado de identificador
         -- en tabla Fnx_Aislados.
         BEGIN
            v_observacion_aislados := 'Activo por ET-TO. PrePago-Desconectado';
            v_usuario := fn_usuario;

            UPDATE FNX_AISLADOS
               SET estado          = 'ACTI',
                   fecha_estado    = SYSDATE,
                   hora_estado     = TO_CHAR (SYSDATE, 'HH:MI'),
                   usua_usuario_id = v_usuario,
                   observacion    = v_observacion_aislados
             WHERE iden_identificador_id = v_ident_reinstalacion;

            IF SQL%NOTFOUND
            THEN
               pr_insertar_conf_ident (v_ident_reinstalacion, 132, 'ACTI');
               pr_insertar_conf_ident (v_ident_reinstalacion,
                                       224,
                                       TO_CHAR (SYSDATE, 'YYYYMMDD')
                                      );
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END IF;                                        -- Si b_prepago_desconect

   END LOOP CICLO_PPAL_SOLICITUDES;
   CLOSE c_solic;

-- SECCION TOIP EMPRESARIAL.  ADICION DE LINEA
-- 2009-03-26   Se deben verificar las condiciones para la asignación de la línea TOIP adicional.
--              para esto, se hace llamado al procedimiento PR_PROV_VERIF_ACCESO_TOIP.
-- 2009-03-27   Si el procedimiento PR_PROV_VERIF_ACCESO_TOIP, retorna un identificador TOIP, se llama al procedimiento
--              PR_PROV_ACCESO_PTO_LIBRE, el cual retorna, marca, referencia, equipo,id,
--              terminal.
   IF b_nueva_telefonia_ip
   THEN
      -- Inicialmente se asume que es necesario crear un nuevo acceso para telefonia IP. La verificaciòn
      -- con el procedimiento PR_PROV_VERIF_ACCESO_TOIP, se encarga de revisar sì ya existe un acceso con
      -- capacidad disponible en la dirección de servicio y para el mismo cliente y en tal caso,
      -- la variable b_nuevo_acceso se asigna a FALSE.
      b_nuevo_acceso := TRUE;
      b_equipo_nuevo := TRUE;
      w_caracteristica := 38;
      v_pagina_servicio :=
         pkg_solicitudes.fn_valor_caracteristica (v_solic.pedido_id,
                                                  v_solic.subpedido_id,
                                                  v_solic.solicitud_id,
                                                  w_caracteristica
                                                 );

      IF v_pagina_servicio IS NOT NULL
      THEN
         v_cliente := fn_cliente_pedido (v_solic.pedido_id);
         pr_prov_verif_acceso_toip ('TOIP',
                                    'CMP',
                                    v_pagina_servicio,
                                    w_caracteristica,
                                    v_cliente,
                                    v_total_solic_to_ip,
                                    v_acceso_toip,
                                    v_mensaje_toip
                                   );
         DBMS_OUTPUT.put_line
                            (   '<ET-TO> Identificador Acceso en direccion:  '
                             || v_mensaje_toip
                            );
                            
            -- 2013-09-02  Sbuitrab    ITPAM 19886 Configuracion de una Segunda Línea TOIP para HOGARES 
            IF NVL (v_uso_servicio, 'COM') = 'RES'
            THEN
               v_acceso_toip := NULL;
            END IF;

         IF v_acceso_toip IS NOT NULL
         THEN
            -- Existe un acceso con capacidad disponible para soportar las nuevas lìneas
            b_nuevo_acceso := FALSE;
            pr_prov_acceso_pto_libre (v_acceso_toip,
                                      v_total_solic_to_ip,
                                      v_marca,
                                      v_referencia,
                                      v_equipo_id,
                                      v_terminal,
                                      v_mensaje_pto_libre
                                     );

            -- Sì el
            IF v_equipo_id IS NOT NULL
            THEN
               b_equipo_nuevo := FALSE;
            ELSE
               b_equipo_nuevo := TRUE;
            END IF;

            IF b_equipo_nuevo
            THEN
               DBMS_OUTPUT.put_line ('<ET-TO> Utilizar un nuevo equipo ');
            ELSE
               DBMS_OUTPUT.put_line
                                 (   '<ET-TO> Utilizar un equipo existente: '
                                  || v_equipo_id
                                 );
            END IF;
         ELSE
            -- En este caso, puede darse que:
            -- A.  No existe un acceso en la direcciòn de servicio
            -- B.  Puede existir un acceso, pero no tiene la capacidad suficiente para soportar las
            --     lìneas adicionales
            b_nuevo_acceso := TRUE;
            b_equipo_nuevo := TRUE;
         END IF;
      END IF;
   END IF;

-- FIN SECCION TOIP EMPRESARIAL.  ADICION DE LINEA

   -- 2009-04-07
-- SECCION.  TOIP EMPRESARIAL.  TRANSACCION RETIRO LINEA.
-- Para el caso de retiro de lineas IP se hacen las verificaciones para buscar sì se debe retirar el
-- acceso asociado < n_conteo_retiros = vt_identif_enAcceso.COUNT> y las verificaciones para buscar
-- los equipos saociados a las lìneas que se retiran <PR_ET_ACCESO_EQUIPOS_RETCAM>
   IF b_crear_solicip_retiro AND NOT w_inconsistencias
   THEN
      b_retiro_acceso := FALSE;
      b_retiro_equipo := FALSE;
      b_retcam_equipo := FALSE;

      FOR cont IN 1 .. vt_identif_enacceso.COUNT
      LOOP
         b_encontrado_retiro :=
            pkg_telefip_util.fn_buscar_identif_valor
                                 (vt_identif_enacceso (cont).identificador_id,
                                  vt_identif_enacceso (cont).valor,
                                  vt_identif_aretirar
                                 );

         IF b_encontrado_retiro
         THEN
            -- Si la combinaciòn de identificador y valor, se encuentran en la tabla de retiros,
            -- se incrementa el contador de retiro
            n_conteo_retiros := n_conteo_retiros + 1;
         END IF;
      END LOOP;

      IF n_conteo_retiros = vt_identif_enacceso.COUNT
      THEN
         b_retiro_acceso := TRUE;
         DBMS_OUTPUT.put_line ('<ET-TO> Se debe retirar el acceso');
      ELSE
         b_retiro_acceso := FALSE;
         DBMS_OUTPUT.put_line ('<ET-TO> No se debe retirar el acceso');
      END IF;

      -- Verificar los equipos asociados a los identificadores que se retiran y determinar cuales
      -- equipos se retiran y cuales solo se libera uno de los puertos ( cambian )
      IF NOT w_inconsistencias AND NOT b_retiro_acceso
      THEN
         pr_et_acceso_equipos_retcam (v_identificador_acceso_toip,
                                      vt_identif_aretirar,
                                      vt_equipos_retirar,
                                      vt_equipos_cambiar
                                     );

         -- Las variables vt_equipos_retirar y vt_equipos_cambiar son tablas PL que contienen
         -- registros de equipos.  Posteriormente estas variables sirven para la creaciòn de las
         -- solicitudes de retiro o cambio de equipos.
         IF vt_equipos_retirar.EXISTS (1)
         THEN
            b_retiro_equipo := TRUE;
         END IF;

         IF vt_equipos_cambiar.EXISTS (1)
         THEN
            b_retcam_equipo := TRUE;
         END IF;

         DBMS_OUTPUT.put_line
            ('<ET-TO> <TABLA PLSQL>  Equipos Retiro que se devuelve ------------------ '
            );

         FOR j IN 1 .. vt_equipos_retirar.COUNT
         LOOP
            DBMS_OUTPUT.put_line (   '<>;'
                                  || vt_equipos_retirar (j).marca
                                  || ';'
                                  || vt_equipos_retirar (j).referencia
                                  || ';'
                                  || vt_equipos_retirar (j).equipo
                                  || ';'
                                  || vt_equipos_retirar (j).identificador
                                  || ';'
                                  || vt_equipos_retirar (j).agrupador
                                  || ';'
                                  || vt_equipos_retirar (j).terminal
                                  || ';'
                                 );
         END LOOP;

         DBMS_OUTPUT.put_line
            ('<ET-TO> <TABLA PLSQL>  Equipos Cambio que se devuelve ------------------ '
            );

         FOR k IN 1 .. vt_equipos_cambiar.COUNT
         LOOP
            DBMS_OUTPUT.put_line (   '<>;'
                                  || vt_equipos_cambiar (k).marca
                                  || ';'
                                  || vt_equipos_cambiar (k).referencia
                                  || ';'
                                  || vt_equipos_cambiar (k).equipo
                                  || ';'
                                  || vt_equipos_cambiar (k).identificador
                                  || ';'
                                  || vt_equipos_cambiar (k).agrupador
                                  || ';'
                                  || vt_equipos_cambiar (k).terminal
                                  || ';'
                                 );
         END LOOP;
      END IF;
   END IF;

-- FIN SECCION.  TOIP EMPRESARIAL.  TRANSACCION RETIRO LINEA.

   -- 2009-07-27
-- Secciòn para calculo de solicitudes de Acceso que se deben crear cuando se solicitan servicios de Nueva
-- IP.  Esta secciòn se saca del ciclo principal para no hacer el calculo por cada solicitud.
   IF b_crear_solic_nueva_ip AND NOT w_inconsistencias
   THEN
      -- 2009-01-20
      -- Determinar el número de solicitudes de TOIP que se deben crear, con base en la tecnología
      -- definida como base, para el acceso.

      -- 2010-09-22
      -- Se quita uso de proc. PR_RUTAS_PROV_TECNOLOGIA debido a que el valor de vo_tecnologia_base se
      -- obtiene previamente.

      IF NVL (vo_tecnologia_base, 'NOT') = 'REDCO'
      THEN
         v_parametro_est_to := 1;
         v_total_lineas_xacceso :=
             NVL (fn_valor_parametros ('ESTTEC_TO', v_parametro_est_to), 'N');
      ELSIF NVL (vo_tecnologia_base, 'NOT') = 'HFC'
      THEN
         v_parametro_est_to := 5;
         v_total_lineas_xacceso :=
             NVL (fn_valor_parametros ('ESTTEC_TO', v_parametro_est_to), 'N');
      ELSIF NVL (vo_tecnologia_base, 'NOT') = 'INALA'
      THEN
         v_parametro_est_to := 10;
         v_total_lineas_xacceso :=
             NVL (fn_valor_parametros ('ESTTEC_TO', v_parametro_est_to), 'N');
      ELSE
         v_total_lineas_xacceso := 5;
         w_mensaje_error :=
               '<ET-TO> No se encontró tecnología para municipio '
            || v_munic_salida_int;
         w_inconsistencias := TRUE;
      END IF;

      IF v_total_solic_to_ip <= v_total_lineas_xacceso
      THEN
         v_total_solicitud_toip := 1;
      ELSIF v_total_solic_to_ip > v_total_lineas_xacceso
      THEN
         IF MOD (v_total_solic_to_ip, v_total_lineas_xacceso) = 0
         THEN
            v_total_solicitud_toip :=
                                 v_total_solic_to_ip / v_total_lineas_xacceso;
            DBMS_OUTPUT.put_line (   '<ET-TO> <Ingreso Residuo igual a 0: '
                                  || v_total_solicitud_toip
                                 );
         ELSE
            v_total_solicitud_toip :=
                      TRUNC (v_total_solic_to_ip / v_total_lineas_xacceso)
                      + 1;
            DBMS_OUTPUT.put_line (   '<ET-TO> <Ingreso Residuo mayor a 0: '
                                  || v_total_solicitud_toip
                                 );
         END IF;
      END IF;

      DBMS_OUTPUT.put_line ('<Municipio>              :' || v_munic_salida_int);
      DBMS_OUTPUT.put_line ('<Tecnologia>             :' || vo_tecnologia_base);
      DBMS_OUTPUT.put_line (   '<Total Lineas por Acceso :'
                            || v_total_lineas_xacceso
                           );
      DBMS_OUTPUT.put_line ('<Total TOs en pedido     :'
                            || v_total_solic_to_ip
                           );
      DBMS_OUTPUT.put_line (   '<Total TOIP              :'
                            || v_total_solicitud_toip
                           );
   END IF;

-- 2008-01-22
-- SECCION TOIP, Insercion 3 solicitudes adicionales al pedido ( TOIP, EQURED, CPE ).
-- 2010-09-28 Se agrega la condición de que no sea un pedido de INGRE-PUMED.
   IF     b_crear_solic_nueva_ip
      AND (b_nuevo_acceso OR b_linea_portada)
      AND NOT w_inconsistencias
      AND NOT b_ingre_pumed
   THEN
      v_tecnologia_solicitud := NULL;
      v_identificador_nuevo := NULL;
      v_solicitud_nueva := NULL;
    
   DBMS_OUTPUT.put_line ('v_total_solicitud_toip: ' || v_total_solicitud_toip );
      -- 2009-01-20
      -- Creación de las solicitudes de TOIP, con base en el número determinado por
      -- la variable v_total_solicitud_toip
      FOR reg IN 1 .. v_total_solicitud_toip
      LOOP
         -- Crear la solicitud del componente TOIP
         pr_solicitud_nueva_toip (w_pedido,
                                  v_subpedido_to,
                                  v_solicitud_to_base,
                                  'TELRES',
                                  'TO',
                                  'TOIP',
                                  'CMP',
                                  v_identificador_nuevo,
                                  v_tecnologia_solicitud,
                                  'TECNI',
                                  'PETEC',
                                  v_solic.municipio_id,
                                  v_solic.empresa_id,
                                  v_solicitud_nueva,
                                  v_mensaje_creacion
                                 );
         DBMS_OUTPUT.put_line (   '<%EST-TO%> Creacion solicitud TOIP: '
                               || v_mensaje_creacion
                              );
         
         -- Crear la solicitud del equipo EQACCP
         v_solicitud_nueva := v_solicitud_nueva + 1;
         pr_solicitud_nueva_toip (w_pedido,
                                  v_subpedido_to,
                                  v_solicitud_to_base,
                                  'TELRES',
                                  'TO',
                                  'EQACCP',
                                  'EQU',
                                  v_identificador_nuevo,
                                  v_tecnologia_solicitud,
                                  'RUTAS',
                                  'PRUTA',
                                  v_solic.municipio_id,
                                  v_solic.empresa_id,
                                  v_solicitud_nueva,
                                  v_mensaje_creacion
                                 );
         DBMS_OUTPUT.put_line (   '<%EST-TO%> Creacion solicitud CPE: '
                               || v_mensaje_creacion
                              );
         
        -- 2013-05-07 Lógica para el tema de traslado con retiro y nuevo
        -- se inserta el registro con la caracteristica 4938 para el EQACCP.
        OPEN c_valor_4936(w_pedido);
        FETCH c_valor_4936 INTO v_valor_4936;
        CLOSE c_valor_4936;
        IF NVL(v_valor_4936.valor,'N')='DNR' THEN
            -- Insertar la caracteristica de 4938 para indicar sí es una linea IP
            pkg_pedidos_util.pr_insupd_caracteristica (w_pedido,
                                                       v_subpedido_to,
                                                       v_solicitud_nueva,
                                                       'TELRES',
                                                       'TO',
                                                       'EQU',
                                                       'EQACCP',
                                                       4938,
                                                       NULL,
                                                       v_solic.municipio_id,
                                                       v_solic.empresa_id,
                                                       v_mensaje_creacion
                                                      );
        END IF;                     

         -- 2009-06-27
         -- Para el caso de tecnologìa INALA ( WIMAX ) se agrega una solicitud de Antena Wimax.
         IF vo_tecnologia_base = 'INALA'
         THEN
            v_solicitud_nueva := v_solicitud_nueva + 1;
            pr_solicitud_nueva_toip (w_pedido,
                                     v_subpedido_to,
                                     v_solicitud_to_base,
                                     'TELRES',
                                     'TO',
                                     'ANTENA',
                                     'EQU',
                                     v_identificador_nuevo,
                                     v_tecnologia_solicitud,
                                     'RUTAS',
                                     'PRUTA',
                                     v_solic.municipio_id,
                                     v_solic.empresa_id,
                                     v_solicitud_nueva,
                                     v_mensaje_creacion
                                    );
         END IF;

         v_solicitud_nueva := v_solicitud_nueva + 1;
      END LOOP;

      DBMS_OUTPUT.PUT_LINE ( 'v_mensaje_creacion = ' || v_mensaje_creacion );
      --  FIN Ciclo Creación de Solicitudes Nuevas de TOIP
      IF v_mensaje_creacion IS NOT NULL
      THEN
         w_inconsistencias := TRUE;
      END IF;
   END IF;

-- 2009-03-27
-- SECCION.  Creaciòn de solicitudes de EQURED
-- 2010-09-28 Se agrega la condición de que no sea un pedido de INGRE-PUMED.

   IF b_crear_solic_nueva_ip  AND NOT b_ingre_pumed AND NOT w_inconsistencias
   THEN
    DBMS_OUTPUT.put_line ('********* entra 1.0' );
      -- 2009-02-07
      -- Buscar el uso del servicio, para determinar el número de solicitudes de EQURED
      -- que se deben crear.
      v_uso_comercial :=
         pkg_solicitudes.fn_valor_caracteristica (w_pedido,
                                                  v_subpedido_to,
                                                  v_solicitud_to_base,
                                                  2
                                                 );

      IF NOT b_linea_portada
      THEN
         v_total_solic_equred := v_total_solic_to_ip;
      ELSE
         -- 2009-05-30. Unificacion Modelo TOIP.
         -- 2009-06-27.  Para el caso de la linea portada, se considera el nùmero de solicitudes Nuevas en el pedido
         -- màs una solicitud que corresponde a la línea portada. Antes se consideraba solo 1 solicitud y fallaba
         -- para el escenario de Linea Portada màs una lìnea adicional
         v_total_solic_equred := v_total_solic_to_ip + 1;
      END IF;

      -- 2009-02-07
      -- SECCION.  Crear solicitudes EQURED para cada una de las líneas TO

      -- 2009-06-27
      -- En la invocaciòn del proc. PR_SOLICITUD_NUEVA_TOIP se cambia el valor del parametro tipo_elemento_id
      -- por ATA ( Antes EQURED )
      
      DBMS_OUTPUT.PUT_LINE ( 'v_total_solic_equred = ' || v_total_solic_equred );
      FOR reg IN 1 .. v_total_solic_equred
      LOOP
         -- Crear la solicitud del equipo EQURED
         pr_solicitud_nueva_toip (w_pedido,
                                  v_subpedido_to,
                                  v_solicitud_to_base,
                                  'TELRES',
                                  'TO',
                                  'ATA',
                                  'EQU',
                                  v_identificador_nuevo,
                                  v_tecnologia_solicitud,
                                  'RUTAS',
                                  'PRUTA',
                                  v_solic.municipio_id,
                                  v_solic.empresa_id,
                                  v_solicitud_nueva,
                                  v_mensaje_creacion
                                 );
         DBMS_OUTPUT.put_line (   '<%EST-TO%> Creacion solicitud EQURED-ATA: '
                               || v_mensaje_creacion
                              );

         IF NOT b_nuevo_acceso
         THEN
         DBMS_OUTPUT.PUT_LINE ( 'v_total_solic_equred = ' || v_total_solic_equred );
            -- Insertar la caracteristica de 3807 para indicar sí es una linea IP
            pkg_pedidos_util.pr_insupd_caracteristica (w_pedido,
                                                       v_subpedido_to,
                                                       v_solicitud_nueva,
                                                       'TELRES',
                                                       'TO',
                                                       'EQU',
                                                       'EQURED',
                                                       90,
                                                       v_acceso_toip,
                                                       v_solic.municipio_id,
                                                       v_solic.empresa_id,
                                                       v_mensaje_creacion
                                                      );
            DBMS_OUTPUT.put_line ('********* entra 1.1');
         END IF;
         
                 
        OPEN c_valor_4936(w_pedido);
        FETCH c_valor_4936 INTO v_valor_4936;
        CLOSE c_valor_4936;
        -- 2013-05-07 Lógica para el tema de traslado con retiro y nuevo
        -- se inserta el registro con la caracteristica 4938 para el EQURED.
        IF NVL(v_valor_4936.valor,'N')='DNR' THEN
            -- Insertar la caracteristica de 4938 para indicar sí es una linea IP
            pkg_pedidos_util.pr_insupd_caracteristica (w_pedido,
                                                       v_subpedido_to,
                                                       v_solicitud_nueva,
                                                       'TELRES',
                                                       'TO',
                                                       'EQU',
                                                       'EQURED',
                                                       4938,
                                                       NULL,
                                                       v_solic.municipio_id,
                                                       v_solic.empresa_id,
                                                       v_mensaje_creacion
                                                      );
        END IF;
         v_solicitud_nueva := v_solicitud_nueva + 1;
      END LOOP;
      
       DBMS_OUTPUT.PUT_LINE ( 'v_mensaje_creacion101 = ' || v_mensaje_creacion );
      --  FIN Ciclo Creación de Solicitudes Nuevas de EQURED
      IF v_mensaje_creacion IS NOT NULL
      THEN
         w_inconsistencias := TRUE;
      END IF;

      -- 2009-03-28
      -- Si ya existe un equipo con capacidad en los puertos, se debe actualizar las caracteristicas
      -- de marca, referencia, equipo y terminal.
      IF NOT b_equipo_nuevo
      THEN
         -- Actualizar caracterìstica Serial de Equipo ( 200 )
         UPDATE fnx_caracteristica_solicitudes
            SET valor = v_equipo_id
          WHERE pedido_id = w_pedido
            AND servicio_id = 'TELRES'
            AND producto_id = 'TO'
            AND tipo_elemento_id = 'EQURED'
            AND tipo_elemento = 'EQU'
            AND caracteristica_id = 200;

         -- Actualizar caracterìstica marca del Equipo ( 960 )
         UPDATE fnx_caracteristica_solicitudes
            SET valor = v_marca
          WHERE pedido_id = w_pedido
            AND servicio_id = 'TELRES'
            AND producto_id = 'TO'
            AND tipo_elemento_id = 'EQURED'
            AND tipo_elemento = 'EQU'
            AND caracteristica_id = 960;

         -- Actualizar caracterìstica Referencia del Equipo ( 982 )
         UPDATE fnx_caracteristica_solicitudes
            SET valor = v_referencia
          WHERE pedido_id = w_pedido
            AND servicio_id = 'TELRES'
            AND producto_id = 'TO'
            AND tipo_elemento_id = 'EQURED'
            AND tipo_elemento = 'EQU'
            AND caracteristica_id = 982;
      END IF;
   END IF;

-- FIN SECCION.  Creaciòn de solicitudes de EQURED

   -- SECCION.  Actualizaciòn de solicitud base a estado ORDEN y PSERV y caracterìstica 4093
   IF b_crear_solic_nueva_ip AND NOT b_nuevo_acceso AND NOT w_inconsistencias
   THEN
      UPDATE fnx_caracteristica_solicitudes
         SET valor = v_acceso_toip
       WHERE pedido_id = w_pedido
         AND subpedido_id = v_subpedido_to
         AND servicio_id = 'TELRES'
         AND producto_id = 'TO'
         AND tipo_elemento_id = 'TO'
         AND tipo_elemento = 'CMP'
         AND caracteristica_id = 4093;
         DBMS_OUTPUT.put_line ('Actualizo ******************' );
   END IF;

-- 2009-04-03
-- SECCION. Lògica para enrutamiento de solicitudes de TO con tecnologia IP, consideradas
-- como lineas adicionales
   IF p_telefonia_ip AND p_nueva AND NOT b_nuevo_acceso
      AND NOT w_inconsistencias
   THEN
   
     DBMS_OUTPUT.put_line ('Por aca pase' );
      v_tecnologia_acceso_toip :=
                            pkg_identificadores.fn_tecnologia (v_acceso_toip);
      pr_estudiot_to_telef_ip (w_pedido,
                               v_subpedido_to,
                               v_acceso_toip,
                               v_total_lineas_xacceso,
                               v_tecnologia_acceso_toip,
                               v_municipio_servicio,
                               v_uso_servicio,
                               v_mensaje_enrutam_to
                              );
      DBMS_OUTPUT.put_line (   '<PR_ESTUDIOT_TO_TELEF_IP> '
                            || v_mensaje_enrutam_to
                           );

         -- 2011-03-31 Hacer un cursor para verificar que en el pedido no hayan solicitudes de TOIP.
         -- se procede a pasar a PORDE la solicitud de TO.
         nutoip:=0;
         OPEN c_solic_toip;
         FETCH c_solic_toip INTO nutoip;
         CLOSE c_solic_toip;

         DBMS_OUTPUT.PUT_LINE ( '******v_concepto_solicitud****** = ' || v_concepto_solicitud );
         IF nutoip < 1 AND v_concepto_solicitud = 'PSERV'
         THEN
             v_estado_solicitud     := 'ORDEN';
             v_concepto_solicitud   := 'PORDE';
         END IF;

         DBMS_OUTPUT.PUT_LINE ( ' *****ENTRO TO-PORDE*****' );

         v_nueva_observacion := ' <# ET-TO ['
                      || v_concepto_solicitud
                      || '] * '
                      || v_observacion
                      || ' #>';
         v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

         
-- 2012-09-20  Sbuitrab      Se realiza modificación para el tema de ciclo correria, puesto que antes de pasar a estado PORDE se debe verificar
--                           que el pedido si cumpla con los requisitos para la correcta facturación             
        IF   v_concepto_solicitud = 'PORDE' THEN
            pr_estudio_tecnico_pumed(v_solic.pedido_id,v_estado_pumed,v_concepto_pumed,v_observacion_pumed,w_mensaje_pumed,paquete_out);
                IF w_mensaje_pumed THEN
                    v_estado_solicitud:=   v_estado_pumed;
                    v_concepto_solicitud:= v_concepto_pumed;
                    v_nueva_observacion:=  v_observacion_pumed;
                    v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);
                END IF;
        END IF;         
                  
         BEGIN
            UPDATE fnx_solicitudes
               SET estado_soli   = 'DEFIN',
                   --estado_id     = v_estado_solicitud,
                   concepto_id   = v_concepto_solicitud,
                   --tecnologia_id = v_tecnologia_solicitud,
                   observacion   =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
             WHERE pedido_id = w_pedido
             AND servicio_id = 'TELRES'
             AND producto_id = 'TO'
             AND tipo_elemento_id = 'TO'
             AND tipo_elemento = 'CMP'
             AND estado_id = 'ORDEN'
             AND concepto_id = 'PSERV';
         EXCEPTION
            WHEN OTHERS
            THEN
               IF v_concepto_solicitud = 'PORDE'
               THEN
                   v_nueva_observacion := ' <# ET-TO ['
                               || v_concepto_solicitud
                               || '] * '
                               || v_observacion
                               || ' #>';
                   v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                  BEGIN
                     UPDATE fnx_solicitudes
                        SET /*estado_soli    = 'DEFIN',
                            estado_id      = 'ORDEN',*/
                            concepto_id    = 'APROB',
                            --tecnologia_id  = v_tecnologia_solicitud,
                            observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                     WHERE pedido_id = w_pedido
                     AND servicio_id = 'TELRES'
                     AND producto_id = 'TO'
                     AND tipo_elemento_id = 'TO'
                     AND tipo_elemento = 'CMP'
                     AND estado_id = 'ORDEN'
                     AND concepto_id = 'PSERV';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        DBMS_OUTPUT.put_line
                               (   '<%EST-TO1%> Error actualizando solicitud '
                                || 'ORDEN'
                                || '-'
                                || 'PORDE'
                               );
                        NULL;
                  END;
               ELSE
                  DBMS_OUTPUT.put_line
                               (   '<%EST-TO2%> Error actualizando solicitud '
                                || v_estado_solicitud
                                || '-'
                                || v_concepto_solicitud
                               );
                  NULL;
               END IF;
         END;

   END IF;

-- SECCION.  RETIRO LINEAS IP EMPRESARIAL
-- SECCION.  Creacion de solicitud de retiro de Acceso TOIP, para el caso en el que se retiran todas las lìneas
--          asociadas en un acceso de TOIP Empresarial.

   -- SECCION.  Retiro de accesos TOIP
   IF NOT w_inconsistencias AND b_retiro_acceso
   THEN
      indice_retiro := 1;

      -- Armar tabla PL con identificadores de Retiro.  Se quitan los identificadores TOIP
      -- duplicados
      FOR cont IN 1 .. vt_identif_enacceso.COUNT
      LOOP
         b_encontrado_retiro := FALSE;

         FOR lin IN 1 .. vt_accestoip_retiro.COUNT
         LOOP
            IF vt_identif_enacceso (cont).valor =
                                              vt_accestoip_retiro (lin).valor
            THEN
               b_encontrado_retiro := TRUE;
            END IF;
         END LOOP;

         IF NOT b_encontrado_retiro
         THEN
            vt_accestoip_retiro (indice_retiro) := vt_identif_enacceso (cont);
            indice_retiro := indice_retiro + 1;
         END IF;
      END LOOP;

      v_solicitud_estado := 'ORDEN';
      v_solicitud_concepto := 'PORDE';
      DBMS_OUTPUT.put_line ('<ET-TO> Ingreso secciòn Retiro de Accesos');

      FOR solret IN 1 .. vt_accestoip_retiro.COUNT
      LOOP
         v_identificador_retir := vt_accestoip_retiro (solret).valor;
         v_tecnologia_solicitud := pkg_identificadores.fn_tecnologia(vt_accestoip_retiro(solret).valor);

         -- Crear la solicitud de retiro del componente TOIP
         pr_solicitud_retir_toip (w_pedido,
                                  v_subpedido_to,
                                  v_solicitud_to_base,
                                  'TELRES',
                                  'TO',
                                  'TOIP',
                                  'CMP',
                                  v_identificador_retir,
                                  v_tecnologia_solicitud,
                                  'TECNI',
                                  'PETEC',
                                  v_solic.municipio_id,
                                  v_solic.empresa_id,
                                  v_solicitud_nueva,
                                  v_mensaje_creacion
                                 );

         -- 2009-05-12
         -- Verificaciòn de tipo de Tecnologìa REDCO para buscar sì se debe generar el regitro
         -- de puente
         IF v_tecnologia_solicitud = 'REDCO'
         THEN

            -- 2010-05-10
            -- Verificar si se deben generar puentes. Se consulta la variable CONTROL_PUENTES de rutas provisión.

            -- Buscar el municipio de servicio asociado al identificador que se retira
            v_municipio_servicio :=  pkg_identificadores.fn_valor_configuracion(v_identificador_retir,34);

            IF v_municipio_servicio IS NOT NULL THEN
              DBMS_OUTPUT.put_line('<TRACK 03> Municipio Servicio ->'||v_municipio_servicio);

              -- Buscar el municipio de salida internet asociado al Municipio  de servicio.
              v_munic_salida_int := PKG_PROV_INTER_UTIL.fn_munic_salida (v_municipio_servicio);

                IF v_munic_salida_int IS NOT NULL THEN
                    DBMS_OUTPUT.put_line('<TRACK 03> Municipio Salida ->'||v_munic_salida_int);
                    v_rutasprov.control_puentes := NULL;
                    v_rutasprov.control_ordenes := NULL;
                    -- Buscar la ruta de provisión de servicio asociada al proveedor, municipio y servicio
                    OPEN  c_rutasprov ( v_solic.empresa_id, v_munic_salida_int, 'TO', 'REDCO');
                    FETCH c_rutasprov INTO v_rutasprov;

                    IF c_rutasprov%NOTFOUND THEN
                       DBMS_OUTPUT.put_line('<TRACK 05> Error: Ruta de provisión no configurada. No se generaron puentes.'||v_solic.empresa_id||'-'||v_munic_salida_int);
                    END IF;
                    CLOSE c_rutasprov;

                    IF NVL(v_rutasprov.control_puentes,'N') = 'S'
                       AND NVL(v_rutasprov.control_ordenes,'N') = 'S'
                    THEN
                        -- 2010-05-05
                        -- Uso de procedimiento para generar puentes de configuraciòn / desconfiguracion cuando
                        -- se retira un servicio de TOIP a través de una TO. El procedimiento verifica condiciones de
                        -- Multiservicio.

                        PR_PROV_PUENTES_ORIGEN
                            ( p_pedido              => w_pedido
                            ,p_subpedido            => v_subpedido_to
                            ,p_solicitud            => v_solicitud_nueva
                            ,p_tipo_elemento_id     => 'DATBA'
                            ,p_tipo_transaccion     => 'RETIR'
                            ,p_identificador_actual => v_identificador_retir
                            );

                    END IF;

                ELSE
                    DBMS_OUTPUT.put_line('<TRACK 04> Error: Municipio de salida NO encontrado. No se generaron puentes.');
                END IF;
            ELSE
               DBMS_OUTPUT.put_line('<TRACK 03> Error: Municipio Servicio NO encontrado. No se generaron puentes.');
            END IF;

         END IF;

         -- Actualización del estado y concepto a ORDEN-PORDE de la solicitud de retiro de Acceso.
         pkg_pedidos_util.pr_estado_concepto (w_pedido,
                                              v_subpedido_to,
                                              v_solicitud_nueva,
                                              v_solicitud_estado,
                                              v_solicitud_concepto,
                                              'DEFIN'
                                             );
         v_solicitud_nueva := v_solicitud_nueva + 1;
      END LOOP;
      
      -- 2013-02-26  Yhernana     Lógica con generación de solicitud de recuperación de equipo
      --                          Si se retira TOIP, se debe realizar Desaprovisionamiento de Equipos.
      --                          Lógica basada en el PR_ESTUDIO_TECNICO_TELEF_IP.
      IF v_mensaje_creacion IS NULL THEN
      
            v_cade_Query := 'SELECT A.EQUIPO_ID, A.MARCA_ID, A.REFERENCIA_ID, A.TIPO_ELEMENTO, A.TIPO_ELEMENTO_ID, ' ||
                            '        DECODE(A.TIPO_ELEMENTO_ID, ''CPE'', ''EQACCP'', ''CPEWIM'', ''EQACCP'', ''CABLEM'', ''EQACCP'', ''EQURED'') TIPO_ELEMENTO_ID_EQU, ' ||
                            '       CASE WHEN B.NUM_MTA = 2 AND A.TIPO_ELEMENTO_ID IN (''CABLEM'') THEN ' ||
                            '              1 ' ||
                            '            WHEN B.NUM_MTA = 2 AND A.TIPO_ELEMENTO_ID IN (''ATA'') THEN ' ||
                            '              0 ' ||
                            '            ELSE ' ||
                            '              1 ' ||
                            '       END GENERAR ' ||
                            'FROM FNX_EQUIPOS A, (SELECT MARCA_ID, REFERENCIA_ID, COUNT(TIPO_ELEMENTO_ID) NUM_MTA ' ||
                            '                     FROM FNX_MARCAS_REFERENCIAS ' ||
                            '                     GROUP BY MARCA_ID, REFERENCIA_ID) B ' ||
                            'WHERE A.MARCA_ID = B.MARCA_ID ' ||
                            '  AND A.REFERENCIA_ID = B.REFERENCIA_ID ' ||
                            '  AND A.IDENTIFICADOR_ID IN ('''|| v_solic.identificador_id || ''', ''' || v_identificador_retir || ''')' ||
                            '  AND A.ESTADO = ''OCU'' ';
                        
            v_equipo_obsoleto := 'N';
            v_si_obsoleto := 0;
            v_no_obsoleto := 0;
            v_equip_obsol := 'EQUIPOS OBSOLETOS ';
            
            OPEN c_Equipos_solic FOR v_cade_Query;
            LOOP
                FETCH c_Equipos_solic INTO reg_tipo_equipo;
                EXIT WHEN c_Equipos_solic%NOTFOUND;

                v_equipo_obsoleto := fn_consultar_equipo_obsoleto (reg_tipo_equipo.marca_id,         reg_tipo_equipo.referencia_id,
                                                                   reg_tipo_equipo.tipo_elemento_id, reg_tipo_equipo.tipo_elemento);
                IF v_equipo_obsoleto = 'N' THEN
                    v_no_obsoleto := v_no_obsoleto + 1;
                    v_equip_obsol := '; Equipo a recuperar: Marca: ' || reg_tipo_equipo.marca_id || ', Referencia: ' || reg_tipo_equipo.referencia_id || ', Serial: '||reg_tipo_equipo.equipo_id;
                ELSE
                    v_si_obsoleto := v_si_obsoleto + 1;
                    v_equip_obsol := '; EQUIPO OBSOLETO: Marca: ' || reg_tipo_equipo.marca_id || ', Referencia: ' || reg_tipo_equipo.referencia_id || ', Serial: '||reg_tipo_equipo.equipo_id;
                END IF;

                IF reg_tipo_equipo.generar = 1 THEN
                    
                    b_genera_solic_equipo := fni_generar_solic_equipo (v_solic.identificador_id, v_tecnologia_solicitud, reg_tipo_equipo, w_pedido);
                    
                    IF b_genera_solic_equipo THEN
                        v_solicitud_eqaccp := NULL;

                        PR_SOLICITUD_RETIR_EQUIPO (v_solic.pedido_id,v_solic.subpedido_id,v_solicitud_eqaccp,v_solic.servicio_id,
                                                   v_solic.producto_id,reg_tipo_equipo.tipo_elemento_id_equ, 'EQU',v_tecnologia_solicitud,
                                                   NULL,v_identificador_retir,v_solic.municipio_id, v_solic.empresa_id,
                                                   reg_tipo_equipo.equipo_id, 'S',v_mensaje_creacion);

                        IF v_mensaje_creacion IS NULL THEN
                            IF v_solicitud_eqaccp IS NOT NULL THEN
                                
                                pri_paso_orden_porde (v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_eqaccp, v_equip_obsol, v_tecnologia_solicitud, v_mensaje_creacion);
                            END IF;

                            v_solicitud_nueva := v_solicitud_nueva + 1;

                        END IF;
                    END IF;
                ELSE
                    
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-NoGeneraSoli RecupEquip' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                        WHERE pedido_id     = w_pedido;
                    EXCEPTION
                        WHEN OTHERS THEN
                            NULL;
                    END;
                END IF;

                IF v_si_obsoleto >= 1 THEN
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-Equi Obsolet' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                        WHERE pedido_id     = w_pedido;
                    EXCEPTION
                        WHEN OTHERS THEN
                            NULL;
                    END;
                END IF;

            END LOOP;
            CLOSE c_Equipos_solic;
      END IF;
      
   END IF;

-- SECCION.  Inserciòn solicitudes de CAMBio de equipos asociados a lìneas Telefonicas IP
--           que se retiran
   IF NOT w_inconsistencias AND b_retcam_equipo
   THEN
      DBMS_OUTPUT.put_line
                     ('<ET-TO> Inserciòn de solicitudes de cambio de equipos');
      -- 2009-05-07  Cambio de concepto de solicitud a POPTO.  La solicitud no genera Orden. Antes era PORDE
      v_solicitud_estado := 'ORDEN';
      v_solicitud_concepto := 'POPTO';

      FOR solcam IN 1 .. vt_equipos_cambiar.COUNT
      LOOP
         v_tecnologia_solicitud :=
            pkg_identificadores.fn_tecnologia
                                    (vt_equipos_cambiar (solcam).identificador
                                    );
         pr_solicitud_cambi_equred (w_pedido,
                                    v_subpedido_to,
                                    v_solicitud_to_base,
                                    'TELRES',
                                    'TO',
                                    'EQURED',
                                    'EQU',
                                    vt_equipos_cambiar (solcam).identificador,
                                    vt_equipos_cambiar (solcam).identificador,
                                    vt_equipos_cambiar (solcam).equipo,
                                    vt_equipos_cambiar (solcam).marca,
                                    vt_equipos_cambiar (solcam).referencia,
                                    vt_equipos_cambiar (solcam).terminal,
                                    v_tecnologia_solicitud,
                                    'TECNI',
                                    'PETEC',
                                    v_solic.municipio_id,
                                    v_solic.empresa_id,
                                    v_solicitud_nueva,
                                    v_mensaje_creacion
                                   );
         -- Actualización del estado y concepto a ORDEN-PORDE.  Se hace
         -- después de insertar la solicitud de cambio para que se genere la orden respectiva
         pkg_pedidos_util.pr_estado_concepto (w_pedido,
                                              v_subpedido_to,
                                              v_solicitud_nueva,
                                              v_solicitud_estado,
                                              v_solicitud_concepto,
                                              'DEFIN'
                                             );
         v_solicitud_nueva := v_solicitud_nueva + 1;
      END LOOP;
   END IF;

-- SECCION.  RETIRO LINEAS IP EMPRESARIAL

   -- SECCION.  Creacion de solicitud de retiro de EQURED, para el caso en el que se retiran los equipos.
   IF NOT w_inconsistencias AND b_retiro_equipo
   THEN
      DBMS_OUTPUT.put_line
                        ('<ET-TO> Inserciòn solicitudes de retiro de equipos');
      v_solicitud_estado := 'ORDEN';
      v_solicitud_concepto := 'PORDE';

      FOR solreteq IN 1 .. vt_equipos_retirar.COUNT
      LOOP
         v_tecnologia_solicitud :=
            pkg_identificadores.fn_tecnologia
                                  (vt_equipos_retirar (solreteq).identificador
                                  );
         -- Crear la solicitud de retiro del componente EQURED
         pr_solicitud_retir_equred
                                  (w_pedido,
                                   v_subpedido_to,
                                   v_solicitud_to_base,
                                   'TELRES',
                                   'TO',
                                   'EQURED',
                                   'EQU',
                                   vt_equipos_retirar (solreteq).identificador,
                                   vt_equipos_retirar (solreteq).identificador,
                                   vt_equipos_retirar (solreteq).equipo,
                                   v_tecnologia_solicitud,
                                   'TECNI',
                                   'PETEC',
                                   v_solic.municipio_id,
                                   v_solic.empresa_id,
                                   v_solicitud_nueva,
                                   v_mensaje_creacion
                                  );
         -- Actualización del estado y concepto a ORDEN-PORDE.  Se hace después de insertar la solicitud de retiro
         -- para que se genere la orden respectiva
         pkg_pedidos_util.pr_estado_concepto (w_pedido,
                                              v_subpedido_to,
                                              v_solicitud_nueva,
                                              v_solicitud_estado,
                                              v_solicitud_concepto,
                                              'DEFIN'
                                             );
         v_solicitud_nueva := v_solicitud_nueva + 1;
      END LOOP;
   END IF;

-- 2008-05-03
-- Cambio de tecnología de IP a CDMA.  Creación de solicitud de retiro del componente
-- TOIP
   IF     NOT w_inconsistencias
      AND b_cambio_tecno_ip_tdma
      AND p_peticion_toip
      AND v_identificador_acceso_toip IS NOT NULL
   THEN
      DBMS_OUTPUT.put_line ('<%EST-TO%> Cambio tecnología en línea IP');
      v_subpedido_to := v_solic.subpedido_id;
      v_solicitud_to_base := v_solic.solicitud_id;
      v_tecnologia_solicitud := NVL (v_tecnologia_acceso_toip, 'LOGIC');
      v_solicitud_nueva := NULL;
      -- Crear la solicitud de retiro del componente TOIP
      pr_solicitud_retir_toip (w_pedido,
                               v_subpedido_to,
                               v_solicitud_to_base,
                               'TELRES',
                               'TO',
                               'TOIP',
                               'CMP',
                               v_identificador_acceso_toip,
                               v_tecnologia_solicitud,
                               'TECNI',
                               'PETEC',
                               v_solic.municipio_id,
                               v_solic.empresa_id,
                               v_solicitud_nueva,
                               v_mensaje_creacion
                              );
      DBMS_OUTPUT.put_line (   '<%EST-TO%> Solicitud Base:                '
                            || w_pedido
                            || NVL (v_subpedido_to, 0)
                            || '-'
                            || NVL (v_solicitud_to_base, 0)
                           );
      DBMS_OUTPUT.put_line (   '<%EST-TO%> Nueva solicitud retiro TOIP:    '
                            || v_solicitud_nueva
                           );
      DBMS_OUTPUT.put_line (   '<%EST-TO%> Creacion solicitud retiro TOIP: '
                            || v_mensaje_creacion
                           );

      IF v_mensaje_creacion IS NULL
      THEN
         BEGIN
            v_solicitud_estado := 'ORDEN';
            v_solicitud_concepto := 'PINSC';
            -- Actualización del estado y concepto a ORDEN-PINSC.  Cuando se cumpla
            -- la solicitud de TO, la solicitud de TOIP pasa a PORDE
            pkg_pedidos_util.pr_estado_concepto (w_pedido,
                                                 v_subpedido_to,
                                                 v_solicitud_nueva,
                                                 v_solicitud_estado,
                                                 v_solicitud_concepto,
                                                 'DEFIN'
                                                );
         EXCEPTION
            WHEN OTHERS
            THEN
               BEGIN
                  v_solicitud_estado := 'ORDEN';
                  v_solicitud_concepto := 'APROB';
                  pkg_pedidos_util.pr_estado_concepto (w_pedido,
                                                       v_subpedido_to,
                                                       v_solicitud_nueva,
                                                       v_solicitud_estado,
                                                       v_solicitud_concepto,
                                                       'DEFIN'
                                                      );
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     w_mensaje_error :=
                        '<%EST-TO%> Error cambiando estado concepto solicitud TO ';
                     DBMS_OUTPUT.put_line (   '<%EST-TO%>'
                                           || SUBSTR (SQLERRM, 1, 70)
                                          );
               END;
         END;


         -- 2010-03-02   Aobregon
         -- Se modifica la actualizacion de las observaciones para que no superen los 1000 caracteres porque los pedidos
         -- se quedan en petec.
         -- Adición de comentario en observación de solicitudes
         BEGIN
            UPDATE fnx_solicitudes
               SET observacion  =  '<# ET-TO -> [' || concepto_id || '] #>'||
                                   SUBSTR(observacion, 1, v_tamano_observacion - length('<# ET-TO -> [' || concepto_id || '] #>') - 1)
             WHERE pedido_id    = w_pedido
               AND subpedido_id = v_subpedido_to
               AND solicitud_id = v_solicitud_nueva;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line
                               (   '<%ET-TO> Error actualizando observacion '
                                || SUBSTR (SQLERRM, 1, 80)
                               );
               NULL;
         END;
      END IF;
   END IF;

-- Borrar la informaciòn de las tablas PL utilizadas
   vt_identif_enacceso.DELETE;
   vt_identif_aretirar.DELETE;
   vt_equipos_retirar.DELETE;
   vt_equipos_cambiar.DELETE;

   IF NOT w_inconsistencias
   THEN
      w_resp := 'S';
   ELSE
      w_resp := SUBSTR (w_mensaje_error, 1, 50);
   END IF;
EXCEPTION
   WHEN e_plataforma_nula
   THEN
      DBMS_OUTPUT.put_line ('<PR_ESTUDIO_TECNICO_TO>');
      DBMS_OUTPUT.put_line (   'Plataforma Nula ['
                            || v_municipio_numeracion
                            || ']-['
                            || v_tecnologia_acceso_toip
                            || ']-['
                            || v_uso_servicio
                           );
      p_mensaje :=
            '<ET_TO> Plataforma Nula ['
         || v_municipio_numeracion
         || ']-['
         || v_tecnologia_acceso_toip
         || ']-['
         || v_uso_servicio;
   WHEN e_tarjetaip_nula THEN
        DBMS_OUTPUT.put_line('<PROV_TOIP> Tarjeta de numeracion IP es nula: '||v_municipio_numeracion);
        p_mensaje  := '<PROV_TOIP> Tarjeta numeracion es nulo: '||v_municipio_numeracion;
   WHEN e_muni_numerac_nulo THEN
        DBMS_OUTPUT.put_line('<ET_TO> CXN. Munic. numeraciòn nulo para: '||v_municipio_servicio);
        p_mensaje   := '<ET_TO> CXN. Munic. numeraciòn nulo para: '||v_municipio_servicio;
   WHEN e_nodo_numerac_nulo THEN
        DBMS_OUTPUT.put_line('<ET_TO> CXN. Nodo numeraciòn nulo para: '||v_municipio_numeracion);
        p_mensaje  := '<ET_TO> CXN. Nodo numeraciòn nulo para: '||v_municipio_numeracion;
   WHEN OTHERS THEN
        DBMS_OUTPUT.put_line ('<PR_ESTUDIO_TECNICO_TO>');
        DBMS_OUTPUT.put_line ('<ERROR>;' || SUBSTR (SQLERRM, 1, 100));
        p_mensaje := '<ET_TO>;' || SUBSTR (SQLERRM, 1, 40);
END; 