public class Expresion{
	public String valor;
	public Expresion valor1;
	public String operador;
	public Expresion valor2;
	public Expresion(String valor){
		this.valor=valor;
		this.operador=null;
		this.valor1=null;
		this.valor2=null;
	}
	public Expresion(Expresion expresion,String operador){
		this.valor=null;
		this.valor1=expresion;
		this.operador=operador;
		this.valor2=null;
	}
	public String toString(){
		if(valor!=null){
			return valor;
		}
		if(operador!=null){
			return valor1+operador+valor2;
		}
		return "";
	}
}