import java.util.*;
public class Procedimiento{
	public String nombre;
	public ArrayList<Parametro>parametros;
	public HashMap<String,Declaracion>declaraciones;
	public Procedimiento(){
		parametros=new ArrayList<Parametro>();
		declaraciones=new HashMap<String,Declaracion>();
	}
	public String toString(){
		String resultado=nombre+"(";
		String sep="";
		for(int i=0;i<parametros.size();i++){
			resultado+=sep+parametros.get(i);
			sep=",";
		}
		resultado=resultado+")";
		return resultado;
	}
}