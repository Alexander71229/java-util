public class Declaracion{
	private String nombre;
	private String tipo;
	private Object declaracion;
	public Declaracion(){
		this.nombre="";
		this.tipo="";
	}
	public String getNombre(){
		return this.nombre;
	}
	public String getTipo(){
		return this.tipo;
	}
	public Object getDeclaracion(){
		return this.declaracion;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public void setTipo(String tipo){
		this.tipo=tipo;
	}
	public void addTipo(String tipo){
		this.tipo+=" "+tipo;
	}
	public void setDeclaracion(Object declaracion){
		this.declaracion=declaracion;
	}
	public String toString(){
		return this.nombre+this.tipo+";";
	}
}