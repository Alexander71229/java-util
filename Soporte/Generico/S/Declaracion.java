import java.util.*;
public class Declaracion extends Estructura{
	public String nombre;
	public String codigo;
	public String tipo;
	public Estructura elemento;
	public Declaracion(String token,Tokenizer fuente){
		this.codigo="";
		this.nombre="";
		this.tipo="";
		if(token.toUpperCase().equals("CURSOR")){
			tipo=token.toUpperCase();
			token=fuente.next();
		}
		if(token.toUpperCase().equals("TYPE")){
			tipo=token.toUpperCase();
			token=fuente.next();
		}
		if(token.toUpperCase().equals("FUNCTION")){
			Funcion funcion=new Funcion(fuente);
			elemento=funcion;
			this.nombre=funcion.nombre;
			this.codigo=funcion.codigo;
			return;
		}
		if(token.toUpperCase().equals("PROCEDURE")){
			Procedimiento procedimiento=new Procedimiento(fuente);
			elemento=procedimiento;
			this.nombre=procedimiento.nombre;
			this.codigo=procedimiento.codigo;
			return;
		}
		this.nombre=token.toUpperCase();
		//this.codigo=token;
		while(fuente.hasNext()){
			token=fuente.next();
			if(token.equals(";")){
				this.codigo+=";";
				break;
			}
			this.codigo+=" "+token;
		}
	}
	public String getCodigo(int nivel){
		if(elemento!=null){
			return this.elemento.getCodigo(nivel+1);
		}
		String resultado="";
		String tab=Util.tab(nivel+1);
		resultado=this.tipo.toUpperCase()+" "+this.nombre.toUpperCase()+" "+Util.format(this.codigo);
		return tab+resultado.trim()+"\n";
	}
	public String getTipo(){
		return "DECLARACIÓN";
	}
	public String desc(int nivel){
		String resultado="";
		resultado=nombre;
		if(elemento!=null){
			resultado+=" "+elemento.getTipo();
		}
		return resultado;
	}
	public String getHTML()throws Exception{
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		contenido=contenido.replace("nombre",nombre);
		contenido=contenido.replace("tipo",tipo);
		return contenido;
	}
}