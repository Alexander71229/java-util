DECLARE
    v_tipo_solicitud   fnx_reportes_instalacion.tipo_solicitud%TYPE;
    v_empresa          fnx_servicios_productos.empresa_id%TYPE;
    v_estado_oferta    fnx_caracteristica_solicitudes.valor%TYPE;

BEGIN

--Modificado por: Mary Luz Aristizábal M.   Julio 2007  (MULTI-EMPRESA)
--Modificado por: Laura Sofía Donado Pinto  Octubre 2011 (Integración CRM-FENIX)
dbms_output.put_line(':NEW.estado_id: '||:NEW.estado_id);
dbms_output.put_line(':OLD.estado_id: '|| :OLD.estado_id);
dbms_output.put_line(':NEW.concepto_id: '||:NEW.concepto_id);
dbms_output.put_line(':OLD.concepto_id: '||:OLD.concepto_id);

  -- Evalua el cambio del estado en la solicitud
  IF :NEW.estado_id <> :OLD.estado_id OR :NEW.concepto_id <> :OLD.concepto_id
  THEN

    --Crear novedad solicitud
    pr_crear_novedad_solicitud (1
                              , NULL
                              , :NEW.pedido_id
                              , :NEW.subpedido_id
                              , :NEW.solicitud_id
                              , :OLD.concepto_id
                              , :NEW.concepto_id
                              , NULL
                               );

    --Identificar UEN_Responsable
    pr_uen_resp (:NEW.pedido_id
               , :NEW.subpedido_id
               , :NEW.solicitud_id
               , :NEW.producto_id
               , :NEW.tipo_elemento_id
               , :NEW.identificador_id
               , :NEW.identificador_id_nuevo
               , :NEW.empresa_id
               , :NEW.municipio_id
                );

    --Ldonadop 11/08/2011 Integración Fenix - Siebel
    pr_estado_oferta_crm (:NEW.pedido_id,
                          :NEW.subpedido_id,
                          :NEW.solicitud_id,
                          :OLD.estado_id,
                          :NEW.estado_id,
                          v_estado_oferta
                         );

   --Scastrga 30/11/2012 Identificar cambio de estado de los pedidos
    --para el proyecto fruades Extraccion pedidos
    PR_FRA_ESTADO  ( :OLD.pedido_id,
                    :OLD.subpedido_id,
                    :OLD.servicio_id,
                    :OLD.producto_id );

    -- Si el nuevo estado es 'ORDEN' y el nuevo concepto es 'PORDE'
    -- (>> Proceso para generacion Ordenes <<)
    IF :NEW.estado_id = 'ORDEN' AND :NEW.concepto_id = 'PORDE'
    THEN
        --Si el producto es ATENC, se trae el producto de la caracteristica 2187
        IF :NEW.producto_id = 'ATENC'
        THEN
          v_empresa :=
          fn_empresa_producto (fn_valor_caracteristica_sol (:NEW.pedido_id
                                                          , :NEW.subpedido_id
                                                          , :NEW.solicitud_id
                                                          , 2187
                                                           )
                                                        , :NEW.empresa_id
                                                        , :NEW.municipio_id);
        ELSE
          v_empresa :=:NEW.empresa_id;
        END IF;


        --  Hallar el tipo de solicitud
        IF :NEW.tipo_requerimiento = 'INSTA'
        THEN
          v_tipo_solicitud := fn_tipo_solicitud (:NEW.pedido_id
                             , :NEW.subpedido_id
                             , :NEW.solicitud_id
                              );
        ELSIF :NEW.tipo_requerimiento = 'ATENC'
        THEN
          v_tipo_solicitud :=fn_tipo_solicitud_aten (:NEW.pedido_id
                                  , :NEW.subpedido_id
                                  , :NEW.solicitud_id
                                   );
        END IF;


        --DBMS_OUTPUT.put_line ('v_empresa ' || v_empresa || ' v_tipo_solicitud ' || v_tipo_solicitud);
        IF v_tipo_solicitud IN ('NUEVO', 'CAMBI', 'RETIR')
        THEN
            pr_ins_trabajos_instalacion (:NEW.pedido_id
                               , :NEW.subpedido_id
                               , :NEW.solicitud_id
                               , v_tipo_solicitud
                               , :NEW.fecha_ingreso
                               , :NEW.servicio_id
                               , :NEW.producto_id
                               , :NEW.tipo_elemento
                               , :NEW.tipo_elemento_id
                               , :NEW.identificador_id
                               , :NEW.identificador_id_nuevo
                               , :NEW.tecnologia_id
                               , :NEW.fecha_cita
                               , :NEW.hora_cita
                               , NULL
                               , :NEW.empresa_id
                               , :NEW.municipio_id
                                );
                              
            
            
        END IF;--IF v_tipo_solicitud IN ('NUEVO', 'CAMBI', 'RETIR')
         
   -- 2014-03-20 VJIMENC - Scoring Anulados Otros Modulos 
   ELSIF :NEW.ESTADO_SOLI = 'ANULA'
    THEN
        IF :NEW.CONCEPTO_ID NOT IN ('ANCMT', 'ANCNE', 'ANDUS', 'ANELE', 'ANFRU', 'ANINP', 'ANPUS', 'ANREP', 'ANSPE', 'ANTNE', 'ANVAL') THEN
           BEGIN
               UPDATE SCR_SCORING
                  SET ANULADO = 'S'
                WHERE PEDIDO_ID = :NEW.PEDIDO_ID
                  AND ANULADO = 'N';
           EXCEPTION
                 WHEN OTHERS THEN          
                      NULL;    
           END;
        ELSIF :NEW.CONCEPTO_ID IN ('ANCMT', 'ANCNE', 'ANDUS', 'ANELE', 'ANFRU', 'ANINP', 'ANPUS', 'ANREP', 'ANSPE', 'ANTNE', 'ANVAL') THEN 
           BEGIN
               UPDATE SCR_SCORING
                  SET ANULADO = 'A'
                WHERE PEDIDO_ID = :NEW.PEDIDO_ID;
            EXCEPTION
                 WHEN OTHERS THEN          
                      NULL;    
           END;    
        END IF; --IF :NEW.CONCEPTO_ID NOT IN ...  
    END IF;--IF :NEW.estado_id = 'ORDEN' AND :NEW.concepto_id = 'PORDE'
  END IF;--IF :NEW.estado_id <> :OLD.estado_id OR :NEW.concepto_id <> :OLD.concepto_id
END; 