import java.util.*;
public class Procedimiento extends Estructura{
	public String nombre;
	public String codigo;
	public Parametros parametros;
	public Bloque bloque;
	public static void comprobar(String token,Tokenizer fuente,Programa programa){
		Procedimiento procedimiento=null;
		if(token.toUpperCase().equals("PROCEDURE")){
			procedimiento=new Procedimiento(fuente);
			programa.add(procedimiento);
		}
	}
	public Procedimiento(Tokenizer fuente){
		analisis(fuente);
	}
	public void analisis(Tokenizer fuente){
		nombre=fuente.next();
		codigo="PROCEDURE "+nombre;
		parametros=new Parametros(fuente);
		codigo+=parametros.codigo;
		bloque=new Bloque(fuente);
		codigo+=bloque.codigo;
	}
	public String getCodigo(int nivel){
		String resultado=Util.tab(nivel);
		resultado+="PROCEDURE "+this.nombre.toUpperCase()+parametros.getCodigo(0)+"\n";
		resultado+=this.bloque.getCodigo(nivel);
		return resultado;
	}
	public String getTipo(){
		return "PROCEDIMIENTO";
	}
	public String desc(int nivel){
		String resultado=Util.tab(nivel);
		resultado+=this.getTipo()+" "+this.nombre+"\n";
		resultado+=bloque.desc(nivel);
		return resultado;
	}
	public String getHTML()throws Exception{
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		contenido=contenido.replace("Descripcion","PROCEDURE "+this.nombre);
		contenido=contenido.replace("Bloque",bloque.getHTML());
		return contenido;
	}
}