FUNCTION       FN_PLATAFORMA_CABLEM (
      p_pedido             IN   fnx_solicitudes.pedido_id%TYPE,
      p_subpedido          IN   fnx_solicitudes.subpedido_id%TYPE,
      p_solicitud          IN   fnx_solicitudes.solicitud_id%TYPE,
      p_requerimiento      IN   fnx_requerimientos_trabajos.requerimiento_id%TYPE,
      p_identificador      IN   fnx_solicitudes.identificador_id%TYPE,
      p_producto           IN   fnx_solicitudes.producto_id%TYPE,
      p_elemento           IN   fnx_solicitudes.tipo_elemento_id%TYPE,
      p_plataforma         IN   fnx_tareas_plataformas.plataforma%TYPE,
      p_actividad          IN   fnx_tareas_plataformas.actividad_id%TYPE,
      p_tipo_trabajo       IN   fnx_tareas_plataformas.tipo_trabajo%TYPE,
      p_tipo_intercambio   IN   fnx_tareas_plataformas.tipo_intercambio%TYPE
   )
      RETURN VARCHAR2
   IS

-- Organizacion:  EPM Telecomunicaciones S.A. - UNE.
-- Aplicacion:    Mòdulo Ordenes
-- Descripcion:   Procedimiento para el enrutamiento de los intercambios para ,
--                activacion y desactivacion de los CableModem en la plataforma
--                LDAP.
-- Fecha:         Junio 24 de 2010
-- Autor:         Edith Paola Tache Jimenez
--                MVM Ingenieria de Software S.A.

-- Historia de Modificaciones
-- Fecha       Autor        Observacion
-- ----------- ---------    ------------------------------------------------------------
-- 27-07-2010  jgallg       Adición del producto Centrex
-- 09-08-2010  jgallg       Adición del producto Troncales
-- 11-08-2010  etachej      Se modifica para controlar los NUEVOS (CREAR) que cambien equipo (CEQUI)
-- 26-08-2010  jgallg       Corrección del tratamiento de las suspensiones y reconexiones en centrex y troncales
-- 31-08-2010  etachej      Se modifica para controlar los NUEVOS (CDACL) que cambien equipo (CEQUI)
-- 27-09-2010  Jgomezve     Se genera el intercambio de CNTXIP, TRKSIP para nuevos (CREAR) por mantenimiento (CEQUI)
-- 11-10-2010  Etachej      Implementación Traslado BA
-- 15-03-2011  ETachej      Se realiza cambio para la transacción de Traslado, para que busque el equipo en FNX_SOLICITUDES.EQUIPO_ID_NUEVO,
--                          se hace llamado a la función fn_valor_equipo_anterior.
-- 03-12-2011 JPULGARB      se valian los intercambios para generar el DECM cuando los productos comparten infraestrucura
-- 19-12-2011 JPULGARB      se valian las condiciones para generar el intercambio CREAR cuando los productos comparten infraestrucura ya que en algunos casos se genera doble
-- 27-12-2011 JPULGARB      se valida la condicion para generar el intercambo CDACL ya que no se debe generar cuando entran primero las tareas del TOIP
-- 05-09-2012 YSOLARTE      Cambio de tecnología BA Pymes se cambia el tipo de trabajo para cambio de tecnología para BA para generar intercambio de retiro
-- 22-11-2013 MALVARAS      Traslado con cambio de velocidad y con cambio de equipo de Convencional a Wifi
-- 26-03-2014 dgirall       Para el producto TO y el tipo de trabajo RETIR, se valida si llega un intercambio 'CPLAN' con el fin de prender la
--                          bandera de generacion de la tarea plataforma
-- 22/05/2014 GSANTOS       ITPAM50300_NUEVO_RETIR. Se modifica la funcion para que se cambie el manejo de los
--                          intercambios con la plataforma.
--                          el cambio es que en el escenario de traslado NUEVO-RETIR, SI se reutiliza
--                          el equipo de acceso no se debe generar el intercambio de NUEVO, se debe generar
--                          el intercambio de CEQUI y si NO se reutiliza el equipo se debe generar el
--                          intercambio de NUEVO.
-- 29-05-2014 SBUITRAB      ITPAM 32258 - Transacción cambio Velocidad y Tecnologia (Control de cambios)
-- 13-08-2014 JHERREMO      ITPAM54018 Se modifica para que al instalar un CABLE MTA en una petición de NUEVO TOIP con tecnología HFC en un lugar donde
--                          ya existe un servicio de Internet con un CABLEM, el tipo_intercambio de la tabla FNX_TAREAS_PLATAFORMAS quede en CEQUI y no en CREAR
-- 27-08-2014 JHERREMO      ITPAM 54012 Se modifica para que se generen los intercambios CPLAN y CDACL en reemplazo del intercambio CREAR
-- 09-09-2014 Jrendbr ITP66474_CambVeloEqui:Si hay cambio de velocidad igual o superior a 8 megas (característica 124)
--                          y el cliente tenga instalado un equipo diferente a DOCSIS3.0,
--                          entonces se actualice la observación de la solicitud en curso
--                          adicionando  la siguiente marca: ** DOCSIS 3.0 **

      v_necesita                            varchar2(2):= 'NO';
      v_ident                               fnx_configuraciones_identif.valor%type;
      v_identificador_comparte_infra        fnx_equipos.identificador_id%type;
      v_acceso                              fnx_identificadores.identificador_id%type;
      v_servicio                            fnx_identificadores.servicio_id%type;
      v_producto                            fnx_identificadores.producto_id%type;
      v_estado_equipo                       fnx_equipos.estado%type;
      v_equipo_EMTA                         VARCHAR2(10);
      v_cequi                               BOOLEAN := FALSE;
      v_cantidad                            NUMBER (1) := 0;
      v_cantidad_2                          NUMBER (1) := 0;
      v_cantidad_3                          NUMBER (1) := 0;
      v_cantidad_4                          NUMBER (1) := 0;
      -- 03-12-2011 JPULGARB
      v_cantidad_5                          NUMBER (1) := 0;
      v_equipo_nuevo                        FNX_EQUIPOS.EQUIPO_ID%TYPE;
      v_equipo_ident                        FNX_EQUIPOS.EQUIPO_ID%TYPE;
      v_equipo_ant_ba                       FNX_EQUIPOS.EQUIPO_ID%TYPE;
      v_equipo_estado                       FNX_EQUIPOS.ESTADO%TYPE;
      v_equipo_marca                        FNX_EQUIPOS.MARCA_ID%TYPE;
      v_equipo_ref                          FNX_EQUIPOS.REFERENCIA_ID%TYPE;
      v_tipo_elemento                       FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE;
      v_ident_comp_ba_o                     FNX_IDENTIFICADORES.IDENTIFICADOR_ID%TYPE;
      v_ident_comp_toip_o                   FNX_IDENTIFICADORES.IDENTIFICADOR_ID%TYPE;
      v_equipo_ant                          FNX_EQUIPOS.EQUIPO_ID%TYPE;
      v_equipo_ba                           FNX_EQUIPOS.EQUIPO_ID%TYPE;
      v_equipo_toip                         FNX_EQUIPOS.EQUIPO_ID%TYPE;
      v_cambio_tecno                        FNX_CONFIGURACIONES_IDENTIF.VALOR%TYPE:= NULL;
      v_estado_soli                         FNX_SOLICITUDES.ESTADO_ID%TYPE:=NULL;
      v_tipo_trabajo                        fnx_trabajos_solicitudes.tipo_trabajo%TYPE;
      /*YSOLARTE 2012-09-04 Cambio de tecnología BA Pymes Variables de tecnología*/
      cambioTecno                           VARCHAR2 (2);
-- 22-11-2012 MALVARAS
      CURSOR c_trabaj (p_pedido     IN   FNX_SOLICITUDES.pedido_id%TYPE,
                    p_subpedido  IN   FNX_SOLICITUDES.subpedido_id%TYPE,
                    p_solicitud  IN   FNX_SOLICITUDES.solicitud_id%TYPE
                    ) IS
      SELECT caracteristica_id, tipo_trabajo
      FROM   fnx_trabajos_solicitudes
      WHERE  pedido_id    = p_pedido
      AND    subpedido_id = p_subpedido
      AND    solicitud_id = p_solicitud;

       v_trabaj                             c_trabaj%ROWTYPE;
       v_cambio_vel                         BOOLEAN;
       v_equipo_ant2                          FNX_EQUIPOS.EQUIPO_ID%TYPE;
 --/ 22-11-2012 MALVARAS

      CURSOR c_tarPlat(p_inter      IN   fnx_tareas_plataformas.tipo_intercambio%TYPE,
                       p_concepto   IN   fnx_tareas_plataformas.concepto_orden%TYPE)
        IS
            SELECT COUNT(1)
              FROM fnx_tareas_plataformas
             WHERE requerimiento_id in (select requerimiento_id
                                        from fnx_requerimientos_trabajos
                                        where pedido_id = p_pedido
                                        and   subpedido_id = p_subpedido

                                        and   solicitud_id = p_solicitud
                                       )
              and  tipo_intercambio =  p_inter
              and  concepto_orden =  p_concepto
              and  plataforma = p_plataforma;


      -- 03-12-2011 JPULGARB
      CURSOR c_tarPlat_InterCam_Prod(p_inter      IN   fnx_tareas_plataformas.tipo_intercambio%TYPE,
                                     p_producto   IN   fnx_requerimientos_trabajos.producto_id%type)
         IS
            SELECT COUNT(1)
              FROM fnx_tareas_plataformas
             WHERE requerimiento_id in (select requerimiento_id
                                        from fnx_requerimientos_trabajos
                                        where pedido_id = p_pedido
                                        and   producto_id = p_producto
                                       )
              and  tipo_intercambio =  p_inter
              and  plataforma = p_plataforma;


          -- Cursor que obtiene Identificador y estado del equipo
          CURSOR c_equipo_nuevo(pc_equipo IN fnx_equipos.equipo_id%type,
                                pc_tipo_elem_id  IN fnx_equipos.tipo_elemento_id%type) IS
           SELECT identificador_id, estado, tipo_elemento_id,marca_id, referencia_id
           FROM FNX_EQUIPOS
           WHERE equipo_id      = pc_equipo
           and   tipo_elemento_id = pc_tipo_elem_id;

           -- Cursor que Identificador y estado del equipo
          CURSOR c_equipo_identif(pc_identificador IN fnx_identificadores.identificador_id%type,
                                  pc_tipo_elem_id  IN fnx_equipos.tipo_elemento_id%type) IS
           SELECT equipo_id
           FROM FNX_EQUIPOS
           WHERE identificador_id = pc_identificador
           and   tipo_elemento_id = pc_tipo_elem_id
           and   estado               = 'OCU';

   -- Cursor Trazabilidad de Equipos vs Identificadores
   CURSOR c_equipo_identif_bk(pc_pedido IN fnx_solicitudes.pedido_id%type,
                              pc_subpedido IN fnx_solicitudes.subpedido_id%type,
                              pc_solicitud IN fnx_solicitudes.solicitud_id%type,
                              pc_identificador IN fnx_identificadores.identificador_id%type
                              ) IS
    select equipo_id
    from fnx_log_cambios_equipos
    where pedido_id  = pc_pedido
    and subpedido_id = pc_subpedido
    and solicitud_id = pc_solicitud
    and identificador_id = pc_identificador;

    --2014-09-09 Cursor para conocer el equipo actual del cliente
    CURSOR c_equipo (p_identificador FNX_SOLICITUDES.IDENTIFICADOR_ID%TYPE) IS
    SELECT tipo_elemento_id, equipo_id, marca_id, referencia_id
    FROM   fnx_equipos
    WHERE  identificador_id = p_identificador;


    -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
    -- caracteristica 4936 pedido actual
    v_dnr  fnx_caracteristica_solicitudes.valor%TYPE;
    -- caracteristica 200 pedido actual
    v_equipo_nuevo_dnr  fnx_caracteristica_solicitudes.valor%TYPE;
    -- caracteristica 4941 pedido actual
    v_pedido_retir  fnx_pedidos.pedido_id%TYPE;
    -- subpedido de retiro
    v_subpedido_retir  fnx_subpedidos.subpedido_id%TYPE;
    -- compara los equipos
    -- 1 equipo nuevo igual equipo retiro
    -- 2 equipo nuevo diferente equipo retiro
    v_equ_ret_nue_iguales   NUMBER(2);

    -- busca el pedido de retiro
    CURSOR c_pedido_retir(p_valor fnx_caracteristica_solicitudes.valor%TYPE)
    IS
    SELECT carsol.pedido_id, carsol.subpedido_id
      FROM fnx_caracteristica_solicitudes carsol
     WHERE carsol.caracteristica_id = 4941
       AND carsol.valor = p_valor
       AND carsol.producto_id = p_producto
       AND ROWNUM = 1;


    -- busca el equipo en el pedido de retir
    CURSOR c_equipo_retir(p_pedido fnx_pedidos.pedido_id%TYPE,
                          p_subpedido fnx_subpedidos.subpedido_id%TYPE,
                          p_valor fnx_caracteristica_solicitudes.valor%TYPE)
    IS
    SELECT COUNT(carsol.valor)
      FROM fnx_caracteristica_solicitudes carsol
     WHERE carsol.pedido_id = p_pedido
       AND carsol.subpedido_id = p_subpedido
       AND carsol.caracteristica_id = 200
       AND carsol.valor = p_valor;
    -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.


    -- 2014-09-03   Jrendbr    ITP66474_CambVeloEqui: INI declaración variables y cursores
    v_vel_tipo_docsis   NUMBER;
    v_equipo_actual     C_EQUIPO%ROWTYPE;
    v_docsis            FNX_MARCAS_REFERENCIAS.TIPO_DOCSIS%TYPE;
    v_tipo_docsis_impr  VARCHAR2(500);
    -- 2014-09-03   Jrendbr    ITP66474_CambVeloEqui: FIN declaración variables



-- 19-12-2011 JPULGARB
FUNCTION FN_COMPARTE_INFRA2 (
   p_identificador IN       fnx_solicitudes.identificador_id%type,
   p_servicio      IN       fnx_solicitudes.servicio_id%type,
   p_producto      IN       fnx_solicitudes.producto_id%type,
   p_pedido        IN       fnx_solicitudes.pedido_id%type,
   p_subpedido     IN       fnx_solicitudes.subpedido_id%type,
   p_solicitud     IN       fnx_solicitudes.solicitud_id%type)
      RETURN VARCHAR2
IS


-- 19-12-2011 JPULGARB
CURSOR c_inf_tv_activas(pc_identificador1          IN        FNX_IDENTIFICADORES.identificador_id%TYPE)
IS
SELECT  identificador_id,centro_distribucion_intermedia,
        nodo_optico_electrico_id,
        tipo_amplificador_id,amplificador_id,tipo_derivador_id,
        derivador_id,terminal_derivador_id
FROM    fnx_inf_tv_activas
WHERE   identificador_id = pc_identificador1;

-- 19-12-2011 JPULGARB
v_inf_tv_activas            c_inf_tv_activas%ROWTYPE;
v_identif_servicio          FNX_IDENTIFICADORES.servicio_id%TYPE := NULL;
v_identif_producto          FNX_IDENTIFICADORES.producto_id%TYPE := NULL;
v_identificador_comparte    FNX_IDENTIFICADORES.identificador_id%TYPE := NULL;

-- 19-12-2011 JPULGARB
CURSOR c_inf_tv_tramites(pc_identificador1          IN FNX_IDENTIFICADORES.identificador_id%TYPE,
                         pc_pedido                  IN FNX_SOLICITUDES.pedido_id%TYPE,
                         pc_subpedido               IN FNX_SOLICITUDES.subpedido_id%TYPE,
                         pc_solicitud               IN FNX_SOLICITUDES.solicitud_id%TYPE)
IS
SELECT  identificador_id,centro_distribucion_intermedia,
        nodo_optico_electrico_id,
        tipo_amplificador_id,amplificador_id,tipo_derivador_id,
        derivador_id,terminal_derivador_id
FROM    fnx_inf_tv_tramites
WHERE   identificador_id = pc_identificador1
AND     pedido_id        = pc_pedido
AND     subpedido_id     = pc_subpedido
AND     solicitud_id     = pc_solicitud
AND     NVL(INSTALADO,'N') <>'S'
;

CURSOR c_inf_tv_tramites_2 ( pc_identificador1                          IN    FNX_IDENTIFICADORES.identificador_id%TYPE,
                            pc_centro_distribucion_inter             IN     FNX_INF_TV_ACTIVAS.centro_distribucion_intermedia%TYPE,
                            pc_nodo_optico_electrico_id              IN     FNX_INF_TV_ACTIVAS.nodo_optico_electrico_id%TYPE,
                            pc_amplificador_id                         IN    FNX_INF_TV_ACTIVAS.amplificador_id%TYPE,
                            pc_tipo_amplificador_id                    IN    FNX_INF_TV_ACTIVAS.tipo_amplificador_id%TYPE,
                            pc_tipo_derivador_id                       IN    FNX_INF_TV_ACTIVAS.tipo_derivador_id%TYPE,
                            pc_derivador_id                            IN    FNX_INF_TV_ACTIVAS.derivador_id%TYPE,
                            pc_terminal_derivador_id                   IN    FNX_INF_TV_ACTIVAS.terminal_derivador_id%TYPE)
IS

SELECT   identificador_id
FROM     fnx_inf_tv_tramites
WHERE     centro_distribucion_intermedia=    pc_centro_distribucion_inter
AND nodo_optico_electrico_id            =    pc_nodo_optico_electrico_id
AND tipo_amplificador_id                =    pc_tipo_amplificador_id
AND amplificador_id                     =    pc_amplificador_id
AND tipo_derivador_id                   =    pc_tipo_derivador_id
AND derivador_id                        =    pc_derivador_id
AND terminal_derivador_id               =    pc_terminal_derivador_id
AND identificador_id                    <>     pc_identificador1
AND     NVL(INSTALADO,'N') <>'S';

CURSOR c_identif_producto ( pc_identificador1                          IN    FNX_IDENTIFICADORES.identificador_id%TYPE)
IS
SELECT  servicio_id, producto_id
FROM    FNX_IDENTIFICADORES
WHERE   identificador_id= pc_identificador1
AND     TIPO_ELEMENTO_ID <>'TO';

BEGIN

v_inf_tv_activas := NULL;
            OPEN   c_inf_tv_tramites(p_identificador,p_pedido,p_subpedido,p_solicitud);
            FETCH  c_inf_tv_tramites INTO v_inf_tv_activas;
            CLOSE  c_inf_tv_tramites;

              FOR r_inf_tv_tramites_2 IN c_inf_tv_tramites_2(p_identificador,v_inf_tv_activas.centro_distribucion_intermedia,v_inf_tv_activas.nodo_optico_electrico_id,
                           v_inf_tv_activas.amplificador_id,v_inf_tv_activas.tipo_amplificador_id,v_inf_tv_activas.tipo_derivador_id,
                           v_inf_tv_activas.derivador_id,v_inf_tv_activas.terminal_derivador_id) LOOP


               v_identif_servicio:=NULL;
               v_identif_producto:=NULL;
               OPEN c_identif_producto(r_inf_tv_tramites_2.identificador_id);
               FETCH c_identif_producto INTO v_identif_servicio,v_identif_producto;
               CLOSE c_identif_producto;

               IF (NVL(v_identif_servicio,'N') = p_servicio) AND (NVL(v_identif_producto,'N') = p_producto) THEN
                 v_identificador_comparte := r_inf_tv_tramites_2.identificador_id;
               END IF;

              END LOOP;

RETURN v_identificador_comparte;

END;


   BEGIN
      --  dbms_output.put_line('Estoy verificando CABLEM');
      v_necesita := 'NO';

    BEGIN
       SELECT tipo_trabajo
       INTO   v_tipo_trabajo
       FROM   fnx_requerimientos_trabajos
       WHERE  requerimiento_id = p_requerimiento
       AND ROWNUM =1;
    EXCEPTION
     WHEN OTHERS THEN
       v_tipo_trabajo := NULL;
    END;

        -- 29-05-2014 SBUITRAB      ITPAM 32258 - Transacción cambio Velocidad y Tecnologia (Control de cambios)
        cambioTecno := fn_verificar_cambiotecno(p_pedido, p_subpedido, p_solicitud, p_identificador, NULL, NULL);

        IF cambioTecno ='SI'
            AND p_actividad ='HACAM'
            AND NVL(fn_valor_caract_subpedido(p_pedido, p_subpedido, 33) , 'N') ='HFC'
        THEN
            v_tipo_trabajo := 'NUEVO';
        END IF;

   --Internet
    IF (p_producto ='INTER') THEN
        /*YSOLARTE 2012-09-05 Cambio de tecnología BA Pymes Generación de la orden*/
        IF p_tipo_trabajo = 'RETIR'  AND p_elemento ='ACCESP' THEN
            cambioTecno := fn_verificar_cambiotecno(p_pedido, p_subpedido, p_solicitud, p_identificador, NULL, NULL);
            IF cambioTecno = 'SI' AND fn_valor_caracteristica_sol(p_pedido, p_subpedido, p_solicitud,2931) IS NOT NULL AND v_tipo_trabajo = 'CAMBI' THEN
                v_tipo_trabajo := 'RETIR';
            END IF;
        END IF;

          IF p_elemento = 'CUENTA'
          THEN
             v_acceso := fn_valor_caract_identif (p_identificador, 2093);
          ELSE
             v_acceso := p_identificador;
          END IF;

        --Verifica si comparte infraestructura con un TOIP
        v_servicio :='TELRES';
        v_producto :='TO';
        v_identificador_comparte_infra := NULL;

        v_identificador_comparte_infra := NVL(
        FN_COMPARTE_INFRA (v_acceso, 'HFC', v_servicio,v_producto,p_pedido,p_subpedido,p_solicitud),
        FN_COMPARTE_INFRA2(v_acceso, v_servicio,v_producto,p_pedido,p_subpedido,p_solicitud));

        --Tipo Trabajo NUEVO - Actividad HACAM
        IF (v_tipo_trabajo='NUEVO' AND p_actividad='HACAM') THEN


             OPEN c_tarPlat('CREAR','CUMPL');
             FETCH c_tarPlat INTO v_cantidad;
             CLOSE c_tarPlat;

             OPEN c_tarPlat('CPLAN','CUMPL');
             FETCH c_tarPlat INTO v_cantidad_2;
             CLOSE c_tarPlat;

              OPEN c_tarPlat('CDACL','CUMPL');
             FETCH c_tarPlat INTO v_cantidad_3;
             CLOSE c_tarPlat;


            -- 19-12-2011 JPULGARB
             OPEN c_tarPlat_InterCam_Prod('CREAR','TO');
             FETCH c_tarPlat_InterCam_Prod INTO v_cantidad_5;
             CLOSE c_tarPlat_InterCam_Prod;


            -- 19-12-2011 JPULGARB
            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CREAR' AND v_identificador_comparte_infra IS NOT NULL AND (v_cantidad_5 + v_cantidad) =0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                --v_necesita := 'SI';
                -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                    v_necesita := 'SI';
                    -- 27-08-2014 JHERREMO ITPAM 54012 INICIO CAMBIO1
                    BEGIN
                         select decode((select count(1)
                           from fnx_marcas_referencias
                          where multiservicio_id=2
                            and (marca_id, referencia_id) in (select marca_id, referencia_id
                                                                from fnx_equipos e
                                                               where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200)
                                                             )
                            and TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           into v_equipo_EMTA
                           from dual;
                    EXCEPTION
                      when others then
                        v_equipo_EMTA := null;
                     END;

                    IF (NVL(v_equipo_EMTA,'N') ='EMTA') THEN
                        BEGIN
                            select estado
                            into v_estado_equipo
                            from fnx_equipos e
                            where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                        EXCEPTION
                            when others then
                                v_estado_equipo := null;
                        END;

                        IF (nvl(v_estado_equipo,'N') = 'OCU') THEN
                          v_necesita := 'NO';
                        END IF;
                    END IF;
                    -- 27-08-2014 JHERREMO ITPAM 54012 FIN CAMBIO1
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_pedido_retir := NULL;
                    OPEN c_pedido_retir(p_pedido);
                    FETCH c_pedido_retir INTO v_pedido_retir, v_subpedido_retir;
                    CLOSE c_pedido_retir;
                    IF NVL(v_pedido_retir,'X') != 'X' THEN
                        v_equipo_nuevo_dnr := NULL;
                        v_equipo_nuevo_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'200');
                        IF NVL(v_equipo_nuevo_dnr,'X') != 'X' THEN
                            v_equ_ret_nue_iguales := 0;
                            OPEN c_equipo_retir(v_pedido_retir, v_subpedido_retir, v_equipo_nuevo_dnr);
                            FETCH c_equipo_retir INTO v_equ_ret_nue_iguales;
                            CLOSE c_equipo_retir;
                        END IF;
                        IF v_equ_ret_nue_iguales = 0 AND v_cantidad_3 = 0 THEN
                            --se envia si el BA no comparte infraestructura con  un TOIP
                            v_necesita := 'SI';
                        ELSIF v_equ_ret_nue_iguales = 1 AND v_cantidad_3 > 0 THEN
                            v_necesita := 'NO';
                        END IF;
                    END IF;
                END IF;
                -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            END IF;

            IF (p_tipo_intercambio = 'CPLAN' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_5 >0  AND v_cantidad_2 = 0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

             -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            IF (p_tipo_intercambio = 'CPLAN' AND v_cantidad_5 = 0  AND v_cantidad_2 = 0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                --v_necesita := 'SI';
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                    v_necesita := 'NO';
                    -- 27-08-2014 JHERREMO ITPAM 54012 INICIO CAMBIO2
                    IF v_identificador_comparte_infra IS NOT NULL THEN
                        BEGIN
                         select decode((select count(1)
                           from fnx_marcas_referencias
                          where multiservicio_id=2
                            and (marca_id, referencia_id) in (select marca_id, referencia_id
                                                                from fnx_equipos e
                                                               where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200)
                                                             )
                            and TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           into v_equipo_EMTA
                           from dual;
                        EXCEPTION
                            when others then
                                v_equipo_EMTA := null;
                        END;

                        IF (NVL(v_equipo_EMTA,'N') ='EMTA') THEN
                            BEGIN
                                select estado
                                into v_estado_equipo
                                from fnx_equipos e
                                where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                            EXCEPTION
                                when others then
                                    v_estado_equipo := null;
                            END;

                            IF (nvl(v_estado_equipo,'N') = 'OCU') THEN
                                v_necesita := 'SI';
                            END IF;
                        END IF;
                    END IF;
                    -- 27-08-2014 JHERREMO ITPAM 54012 FIN CAMBIO2
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_pedido_retir := NULL;
                    OPEN c_pedido_retir(p_pedido);
                    FETCH c_pedido_retir INTO v_pedido_retir, v_subpedido_retir;
                    CLOSE c_pedido_retir;
                    IF NVL(v_pedido_retir,'X') != 'X' THEN
                        v_equipo_nuevo_dnr := NULL;
                        v_equipo_nuevo_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'200');
                        IF NVL(v_equipo_nuevo_dnr,'X') != 'X' THEN
                            v_equ_ret_nue_iguales := 0;
                            OPEN c_equipo_retir(v_pedido_retir, v_subpedido_retir, v_equipo_nuevo_dnr);
                            FETCH c_equipo_retir INTO v_equ_ret_nue_iguales;
                            CLOSE c_equipo_retir;
                        END IF;
                        IF v_equ_ret_nue_iguales = 0 THEN
                            --se envia si el BA no comparte infraestructura con  un TOIP
                            v_necesita := 'NO';
                        ELSIF v_equ_ret_nue_iguales = 1 THEN
                            v_necesita := 'SI';
                        END IF;
                    END IF;
                END IF;
            END IF;
            -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.

            IF (p_tipo_intercambio = 'CDACL' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_5 >0  AND v_cantidad_3 = 0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

            -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            IF (p_tipo_intercambio = 'CDACL' AND v_cantidad = 0 AND v_cantidad_2 = 0 AND  v_cantidad_5 =0  AND v_cantidad_3 = 0) THEN
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                    v_necesita := 'NO';
                    -- 27-08-2014 JHERREMO ITPAM 54012 FIN CAMBIO3
                    IF v_identificador_comparte_infra IS NOT NULL THEN
                        BEGIN
                         select decode((select count(1)
                           from fnx_marcas_referencias
                          where multiservicio_id=2
                            and (marca_id, referencia_id) in (select marca_id, referencia_id
                                                                from fnx_equipos e
                                                               where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200)
                                                             )
                            and TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           into v_equipo_EMTA
                           from dual;
                        EXCEPTION
                            when others then
                                v_equipo_EMTA := null;
                        END;

                        IF (NVL(v_equipo_EMTA,'N') ='EMTA') THEN
                            BEGIN
                                select estado
                                into v_estado_equipo
                                from fnx_equipos e
                                where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                            EXCEPTION
                                when others then
                                    v_estado_equipo := null;
                            END;

                            IF (nvl(v_estado_equipo,'N') = 'OCU') THEN
                                v_necesita := 'SI';
                            END IF;
                        END IF;
                    END IF;
                    -- 27-08-2014 JHERREMO ITPAM 54012 FIN CAMBIO3
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_pedido_retir := NULL;
                    OPEN c_pedido_retir(p_pedido);
                    FETCH c_pedido_retir INTO v_pedido_retir, v_subpedido_retir;
                    CLOSE c_pedido_retir;
                    IF NVL(v_pedido_retir,'X') != 'X' THEN
                        v_equipo_nuevo_dnr := NULL;
                        v_equipo_nuevo_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'200');
                        IF NVL(v_equipo_nuevo_dnr,'X') != 'X' THEN
                            v_equ_ret_nue_iguales := 0;
                            OPEN c_equipo_retir(v_pedido_retir, v_subpedido_retir, v_equipo_nuevo_dnr);
                            FETCH c_equipo_retir INTO v_equ_ret_nue_iguales;
                            CLOSE c_equipo_retir;
                        END IF;
                        IF v_equ_ret_nue_iguales = 0 THEN
                            --se envia si el BA no comparte infraestructura con  un TOIP
                            v_necesita := 'NO';
                        ELSIF v_equ_ret_nue_iguales = 1 THEN
                            v_necesita := 'SI';
                        END IF;
                    END IF;
                END IF;
            END IF;
            -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.


            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CREAR' AND v_identificador_comparte_infra IS NULL AND v_cantidad=0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                --v_necesita := 'SI';
                -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                    v_necesita := 'SI';
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_pedido_retir := NULL;
                    OPEN c_pedido_retir(p_pedido);
                    FETCH c_pedido_retir INTO v_pedido_retir, v_subpedido_retir;
                    CLOSE c_pedido_retir;
                    IF NVL(v_pedido_retir,'X') != 'X' THEN
                        v_equipo_nuevo_dnr := NULL;
                        v_equipo_nuevo_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'200');
                        IF NVL(v_equipo_nuevo_dnr,'X') != 'X' THEN
                            v_equ_ret_nue_iguales := 0;
                            OPEN c_equipo_retir(v_pedido_retir, v_subpedido_retir, v_equipo_nuevo_dnr);
                            FETCH c_equipo_retir INTO v_equ_ret_nue_iguales;
                            CLOSE c_equipo_retir;
                        END IF;
                        IF v_equ_ret_nue_iguales = 0 AND v_cantidad_3 = 0 THEN
                            --se envia si el BA no comparte infraestructura con  un TOIP
                            v_necesita := 'SI';
                        ELSIF v_equ_ret_nue_iguales = 1 AND v_cantidad_3 > 0 THEN
                            v_necesita := 'NO';
                        END IF;
                    END IF;
                END IF;
                -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            END IF;

            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NULL AND v_cantidad>0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

            -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NULL AND v_cantidad=0 AND v_cantidad_3 > 0) THEN
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                    v_necesita := 'NO';
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_necesita := 'SI';
                END IF;
            END IF;
            -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.


              -- Intercambio CEQUI - Cambia Equipo - Si se está cumpliendo con un equipo LIB - EMTA
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_2 > 0 AND v_cantidad_3 > 0) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_cequi := FALSE;
                    begin
                         select decode((select count(1)
                            from fnx_marcas_referencias
                            where multiservicio_id=2
                            and (marca_id, referencia_id) in (select marca_id, referencia_id
                                                           from fnx_equipos e
                                                           where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200)
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           into v_equipo_EMTA
                          from dual;
                    exception
                      when others then
                        v_equipo_EMTA := null;
                     end;

                    IF (NVL(v_equipo_EMTA,'N') ='EMTA') THEN
                      begin
                        select estado
                        into v_estado_equipo
                        from fnx_equipos e
                        where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                       exception
                      when others then
                        v_estado_equipo := null;
                      end;

                        IF (nvl(v_estado_equipo,'N') = 'LIB') THEN
                            v_cequi := TRUE;
                             v_necesita := 'SI';
                        END IF;
                    END IF;
             END IF;

             -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
              -- Intercambio CEQUI - Cambia Equipo - Si se está cumpliendo con un equipo LIB - EMTA
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad=0 AND v_cantidad_2 = 0 AND v_cantidad_3 > 0) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_cequi := FALSE;
                    begin
                         select decode((select count(1)
                            from fnx_marcas_referencias
                            where multiservicio_id=2
                            and (marca_id, referencia_id) in (select marca_id, referencia_id
                                                           from fnx_equipos e
                                                           where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200)
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           into v_equipo_EMTA
                          from dual;
                    exception
                      when others then
                        v_equipo_EMTA := null;
                     end;

                    IF (NVL(v_equipo_EMTA,'N') ='EMTA') THEN
                      begin
                        select estado
                        into v_estado_equipo
                        from fnx_equipos e
                        where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                       exception
                      when others then
                        v_estado_equipo := null;
                      end;

                        IF (nvl(v_estado_equipo,'N') = 'LIB') THEN
                            v_cequi := TRUE;
                            v_necesita := 'SI';
                            v_dnr := NULL;
                            v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                            IF NVL(v_dnr,'X') != 'DNR' THEN
                               --se envia si el BA no comparte infraestructura con  un TOIP
                               v_necesita := 'NO';
                            ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                               v_necesita := 'SI';
                            END IF;
                        END IF;
                    END IF;
             END IF;
             -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.

        END IF;

        --Tipo Trabajo CAMBI - Actividad HACAM - Cambio de Equipo
        IF (v_tipo_trabajo='CAMBI' AND p_actividad='HACAM' and p_elemento ='CABLEM') THEN
            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI') THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;

        --Tipo Trabajo CAMBI - Actividad HACAM - Cambio de Plan
        IF (v_tipo_trabajo='CAMBI' AND p_actividad='HACAM' and p_elemento ='ACCESP') THEN
           -- Intercambio CPLAN - Cambio Plan
            IF (p_tipo_intercambio = 'CPLAN' ) THEN
                v_necesita := 'SI';

                -- 09-09-2014 Jrendbr ITP66474_CambVeloEqui:Si hay cambio de velocidad igual o superior a 8 megas (característica 124)
                --                          y el cliente tenga instalado un equipo diferente a DOCSIS3.0,
                --                          entonces se actualice la observación de la solicitud en curso
                --                          adicionando  la siguiente marca: ** DOCSIS 3.0 **
                BEGIN
                    v_vel_tipo_docsis:= NVL(TO_NUMBER(fn_valor_parametros('DOCSIS',1)),8000);

                    DBMS_OUTPUT.PUT_LINE ( 'v_vel_tipo_docsis = ' || v_vel_tipo_docsis );

                    IF TO_NUMBER(FN_valor_caracteristica_sol( p_pedido,p_subpedido,p_solicitud,124)) >= v_vel_tipo_docsis AND
                       TO_NUMBER(FN_valor_caracteristica_sol( p_pedido, p_subpedido,p_solicitud,124)) >
                       TO_NUMBER(FN_valor_caracteristica_trab( p_pedido,p_subpedido,p_solicitud,124)) THEN


                       OPEN c_equipo(p_identificador);
                       FETCH c_equipo INTO v_equipo_actual;
                       IF c_equipo%FOUND THEN
                            v_docsis:= fn_consultar_tipo_docsis (v_equipo_actual.marca_id,
                                                                 v_equipo_actual.referencia_id,
                                                                 v_equipo_actual.tipo_elemento_id);
                       END IF;
                       CLOSE c_equipo;

                       v_tipo_docsis_impr:= NVL(fn_valor_parametros('DOCSIS',2),'DOCSIS3.0');

                       IF NVL(v_docsis, 'N') <> v_tipo_docsis_impr THEN

                            v_necesita := 'NO';
                       ELSE
                            v_necesita := 'SI';

                       END IF;
                    END IF;
                EXCEPTION
                WHEN OTHERS THEN
                  DBMS_OUTPUT.put_line
                                        (   '<% FN_PLATAFORMA_CABLEM-> Error comparando velocidad inter '|| SUBSTR (SQLERRM, 1, 80));
                  NULL;
                END;


            END IF;

            -- 09-09-2014 Jrendbr ITP66474_CambVeloEqui:Si hay cambio de velocidad igual o superior a 8 megas (característica 124)
            --                          y el cliente tenga instalado un equipo diferente a DOCSIS3.0,
            --                          entonces se actualice la observación de la solicitud en curso
            --                          adicionando  la siguiente marca: ** DOCSIS 3.0 **
            IF (p_tipo_intercambio = 'CEQUI' ) THEN

                BEGIN
                    v_vel_tipo_docsis:= NVL(TO_NUMBER(fn_valor_parametros('DOCSIS',1)),8000);

                    DBMS_OUTPUT.PUT_LINE ( 'v_vel_tipo_docsis = ' || v_vel_tipo_docsis );

                    IF TO_NUMBER(FN_valor_caracteristica_sol( p_pedido,p_subpedido,p_solicitud,124)) >= v_vel_tipo_docsis AND
                       TO_NUMBER(FN_valor_caracteristica_sol( p_pedido, p_subpedido,p_solicitud,124)) >
                       TO_NUMBER(FN_valor_caracteristica_trab( p_pedido,p_subpedido,p_solicitud,124)) THEN

                       OPEN c_equipo(p_identificador);
                       FETCH c_equipo INTO v_equipo_actual;
                       IF c_equipo%FOUND THEN
                            v_docsis:= fn_consultar_tipo_docsis (v_equipo_actual.marca_id,
                                                                 v_equipo_actual.referencia_id,
                                                                 v_equipo_actual.tipo_elemento_id);
                       END IF;
                       CLOSE c_equipo;

                       v_tipo_docsis_impr:= NVL(fn_valor_parametros('DOCSIS',2),'DOCSIS3.0');

                       IF NVL(v_docsis, 'N') <> v_tipo_docsis_impr THEN
                            v_necesita := 'SI';
                       ELSE
                            v_necesita := 'NO';

                       END IF;
                    END IF;
                EXCEPTION
                WHEN OTHERS THEN
                  DBMS_OUTPUT.put_line
                                        (   '<% FN_PLATAFORMA_CABLEM-> Error comparando velocidad inter '|| SUBSTR (SQLERRM, 1, 80));
                  NULL;
                END;
            END IF;

        END IF;

        --Tipo Trabajo RETIR - Actividad DECAM
        IF (v_tipo_trabajo='RETIR' AND p_actividad='DECAM') THEN
            -- Intercambio DECM - Deshabilitar Cable Modem:
            IF (p_tipo_intercambio = 'DECM' AND v_identificador_comparte_infra IS NULL) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;


            -- 03-12-2011 JPULGARB
            OPEN c_tarPlat_InterCam_Prod('CDACL','TO');
             FETCH c_tarPlat_InterCam_Prod INTO v_cantidad_5;
             CLOSE c_tarPlat_InterCam_Prod;

            IF v_cantidad_5 = 0 THEN

            -- Intercambio CPLAN - Cambio Plan
            IF (p_tipo_intercambio = 'CPLAN' AND v_identificador_comparte_infra IS NOT NULL) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CDACL - Cambia datos del Cliente
            IF (p_tipo_intercambio = 'CDACL' AND v_identificador_comparte_infra IS NOT NULL) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
             END IF;

            ELSE

             IF (p_tipo_intercambio = 'DECM' AND v_identificador_comparte_infra IS NOT NULL) THEN
                 --se envia si el BA comparte infraestructura con un TOIP
                 v_necesita := 'SI';
             END IF;

            END IF;

        END IF;

        --Tipo Trabajo SUSPE - Actividad SUSPE
        IF (v_tipo_trabajo='SUSPE' AND p_actividad='DECAM') THEN
            -- Intercambio SUSPE - Deshabilitar Cable Modem:
            IF (p_tipo_intercambio = 'SUSPE' AND v_identificador_comparte_infra IS NULL) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CPLAN - Cambio Plan
            IF (p_tipo_intercambio = 'CPLAN' AND v_identificador_comparte_infra IS NOT NULL) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

        END IF;

        --Tipo Trabajo RECON - Actividad ACTIV
        IF (v_tipo_trabajo='RECON' AND p_actividad='HACAM') THEN
            -- Intercambio RECON - Habilitar Cable Modem:
            IF (p_tipo_intercambio = 'RECON' AND v_identificador_comparte_infra IS NULL) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CPLAN - Cambio Plan
            IF (p_tipo_intercambio = 'CPLAN' AND v_identificador_comparte_infra IS NOT NULL) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

        END IF;

        --Tipo Trabajo TRASL - Actividad HACAM
        IF (v_tipo_trabajo='TRASL' AND p_actividad='HACAM') THEN


             OPEN c_tarPlat('CREAR','CUMPL');
             FETCH c_tarPlat INTO v_cantidad;
             CLOSE c_tarPlat;

             OPEN c_tarPlat('CPLAN','CUMPL');
             FETCH c_tarPlat INTO v_cantidad_2;
             CLOSE c_tarPlat;

             OPEN c_tarPlat('CDACL','CUMPL');
             FETCH c_tarPlat INTO v_cantidad_3;
             CLOSE c_tarPlat;

             OPEN c_tarPlat('CEQUI','CUMPL');
             FETCH c_tarPlat INTO v_cantidad_4;
             CLOSE c_tarPlat;
            -- 22-11-2013 MALVARAS
                --Se verifica si hay cambio de plan
                  OPEN c_trabaj ( p_pedido, p_subpedido, p_solicitud);
                     LOOP
                       FETCH c_trabaj INTO v_trabaj;
                       EXIT WHEN c_trabaj%NOTFOUND;
                       IF v_trabaj.caracteristica_id IN (124) AND  v_trabaj.tipo_trabajo = 'CAMBI'
                       THEN
                          v_cambio_vel  := TRUE;
                       END IF;
                     END LOOP;
                  CLOSE c_trabaj;

            --Se verifica si es traslado
             v_equipo_nuevo := fn_valor_caract_subelem (p_pedido, p_subpedido, 'EQACCP', 200);

             IF (v_equipo_nuevo IS NULL) THEN
                 SELECT valor
                 INTO v_equipo_nuevo
                 FROM fnx_caracteristica_solicitudes
                WHERE pedido_id = p_pedido
                  AND subpedido_id = p_subpedido
                  AND caracteristica_id = 200
                  AND tipo_elemento_id = 'EQACCP'
                  AND VALOR IS NOT NULL;
             END IF;

             OPEN  c_equipo_nuevo(v_equipo_nuevo,'CABLEM');
             FETCH c_equipo_nuevo INTO v_equipo_ident,v_equipo_estado,v_tipo_elemento,v_equipo_marca,v_equipo_ref;
             CLOSE c_equipo_nuevo;

             v_ident_comp_toip_o  := FN_COMPARTE_INFRA (v_acceso, 'HFC', v_servicio,v_producto,null, null, null);

                IF(v_identificador_comparte_infra IS NOT NULL) THEN
                 OPEN  c_equipo_identif(v_identificador_comparte_infra,v_tipo_elemento);
                 FETCH c_equipo_identif INTO v_equipo_ant;
                 CLOSE c_equipo_identif;
                END IF;
            -- 22-11-2013 MALVARAS
                OPEN  c_equipo_identif(p_identificador,v_tipo_elemento);
                FETCH c_equipo_identif INTO v_equipo_ant2;
                CLOSE c_equipo_identif;
            -- 22-11-2013 MALVARAS

            --Se verifica si hay cambio de tecnologia
             v_cambio_tecno := fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33);
             --Cambio Tecnologia
             IF (v_cambio_tecno IS NOT NULL) THEN
                --Cambia de REDCO a HFC
                IF (v_cambio_tecno='REDCO') THEN
                    v_ident_comp_toip_o  := FN_COMPARTE_INFRA (v_acceso, 'REDCO', v_servicio,v_producto,null, null, null);
                    -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
                    IF (p_tipo_intercambio = 'CREAR' AND v_cantidad=0
                        AND NVL(v_equipo_estado,'N')='LIB') THEN
                        --se envia si el BA no comparte infraestructura con un TOIP Destino
                        v_necesita := 'SI';
                    END IF;
                     -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
                    IF (p_tipo_intercambio = 'CAMEQ' AND (v_cantidad>0 OR  v_cantidad_2>0 OR  v_cantidad_3>0)
                        AND NVL(v_equipo_estado,'N')='LIB') THEN
                        --se envia si el BA no comparte infraestructura con un TOIP Destino
                        v_necesita := 'SI';
                    END IF;

                     -- Intercambio CDACL -
                    IF (p_tipo_intercambio = 'CPLAN' AND v_cantidad_2 =0 AND v_identificador_comparte_infra IS NOT NULL
                        AND NVL(v_equipo_ant,'N')=v_equipo_nuevo) THEN
                        --se envia si el BA no comparte infraestructura con un TOIP Destino
                        v_necesita := 'SI';
                    END IF;

                      -- Intercambio CDACL -
                    IF (p_tipo_intercambio = 'CDACL' AND v_cantidad_3=0 AND v_identificador_comparte_infra IS NOT NULL
                        AND NVL(v_equipo_ant,'N')=v_equipo_nuevo) THEN
                        --se envia si el BA no comparte infraestructura con un TOIP Destino
                        v_necesita := 'SI';
                    END IF;

                END IF;
             END IF;

            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CREAR' AND v_identificador_comparte_infra IS NULL AND v_cantidad=0
                AND v_ident_comp_toip_o IS NOT NULL AND NVL(v_equipo_estado,'N')='LIB') THEN
                --se envia si el BA no comparte infraestructura con un TOIP Destino
                v_necesita := 'SI';
            END IF;
            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CAMEQ' AND v_identificador_comparte_infra IS NULL AND v_cantidad>0
                AND NVL(v_equipo_estado,'N')='LIB') THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_4=0
                AND NVL(v_equipo_estado,'N')='LIB' AND v_cambio_tecno IS NULL AND v_cantidad_2 = 0 AND  v_cantidad_3 = 0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NULL AND v_cantidad_4=0
                AND v_ident_comp_toip_o IS NULL AND NVL(v_equipo_estado,'N')='LIB' AND v_cambio_tecno IS NULL
                AND v_cantidad_2 = 0 AND  v_cantidad_3 = 0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CAMEQ' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_4>0
                AND NVL(v_equipo_estado,'N')='LIB' AND v_cambio_tecno IS NULL ) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CAMEQ' AND v_identificador_comparte_infra IS NULL AND v_cantidad_4>0
                AND v_ident_comp_toip_o IS NULL AND NVL(v_equipo_estado,'N')='LIB' AND v_cambio_tecno IS NULL) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CPLAN - Cambio Plan
            IF (p_tipo_intercambio = 'CPLAN' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_2 = 0
                AND NVL(v_equipo_ant,'N') = v_equipo_nuevo) THEN
                --se envia si el BA comparte infraestructura con un TOIP, Cumple equipo TOIP Destino
                v_necesita := 'SI';
            END IF;
            -- 22-11-2013 MALVARAS
             -- cuando es traslado + velocidad  igual equipo
            IF (p_tipo_intercambio = 'CPLAN'  AND v_cantidad_2 = 0  AND v_cambio_vel = TRUE
                AND NVL(v_equipo_ant2,'N') = v_equipo_nuevo ) THEN
                v_necesita := 'SI';
            END IF;
              -- cuando es traslado + velocidad y cambio de equipo
            IF (p_tipo_intercambio = 'CPLAN'  AND v_cantidad_2 = 0   AND v_cambio_vel = TRUE
                AND NVL(v_equipo_ant2,'N') <> v_equipo_nuevo  AND v_identificador_comparte_infra IS NULL ) THEN
                v_necesita := 'SI';
            END IF;
            --/ 22-11-2013 MALVARAS
            -- Intercambio CPLAN - Cambio Datos del Cliente
            IF (p_tipo_intercambio = 'CDACL' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_3 = 0
                AND NVL(v_equipo_ant,'N') = v_equipo_nuevo) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
             -- Intercambio CEQUI - Cambia Equipo - Si se está cumpliendo con un equipo LIB - EMTA
            IF (p_tipo_intercambio = 'CAMEQ' AND v_identificador_comparte_infra IS NOT NULL
                AND v_cantidad_2 > 0 AND v_cantidad_3 > 0) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
             END IF;
        END IF;

        --Tipo Trabajo TRASL - Actividad DECAM
        IF (v_tipo_trabajo='TRASL' AND p_actividad='DECAM') THEN

            v_cantidad_4 :=0;
            OPEN c_tarPlat('CEQUI','CUMPL');
            FETCH c_tarPlat INTO v_cantidad_4;
            CLOSE c_tarPlat;

             v_equipo_nuevo := fn_valor_caract_subelem (p_pedido, p_subpedido, 'EQACCP', 200);

             OPEN  c_equipo_nuevo(v_equipo_nuevo,'CABLEM');
             FETCH c_equipo_nuevo INTO v_equipo_ident,v_equipo_estado,v_tipo_elemento,v_equipo_marca,v_equipo_ref;
             CLOSE c_equipo_nuevo;

              IF(v_identificador_comparte_infra IS NOT NULL) THEN
                 OPEN  c_equipo_identif(v_identificador_comparte_infra,v_tipo_elemento);
                 FETCH c_equipo_identif INTO v_equipo_ant;
                 CLOSE c_equipo_identif;
              END IF;


              OPEN  c_equipo_identif(v_acceso,v_tipo_elemento);
              FETCH c_equipo_identif INTO v_equipo_ba;
              CLOSE c_equipo_identif;

             v_estado_soli := pkg_solicitudes.FN_ESTADO(p_pedido, p_subpedido,p_solicitud);

             IF(v_estado_soli IN ('CUMPL','FACTU')) THEN
                v_ident_comp_toip_o  := FN_COMPARTE_INFRA_RESP (v_acceso, 'HFC', v_servicio,v_producto,p_pedido, p_subpedido,p_solicitud);
                v_identificador_comparte_infra  := FN_COMPARTE_INFRA (v_acceso, 'HFC', v_servicio,v_producto,null, null, null);
                OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_identificador_comparte_infra);
                FETCH c_equipo_identif_bk INTO v_equipo_ant;
                CLOSE c_equipo_identif_bk;

                OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_acceso);
                FETCH c_equipo_identif_bk INTO v_equipo_ant_ba;
                CLOSE c_equipo_identif_bk;
             ELSE
                v_ident_comp_toip_o  := FN_COMPARTE_INFRA (v_acceso, 'HFC', v_servicio,v_producto,null, null, null);
             END IF;

            -- Intercambio CPLAN - Cambio Plan
            IF (p_tipo_intercambio = 'CPLAN' AND v_ident_comp_toip_o IS NOT NULL
                AND (NVL(v_equipo_estado,'N') = 'LIB' OR  NVL(v_equipo_ant,'N') = v_equipo_nuevo OR  NVL(v_equipo_ba,'N') = v_equipo_nuevo)
                AND NVL(v_equipo_ant_ba,'N') <> v_equipo_nuevo and NVL(fn_valor_equipo_anterior(p_pedido, p_subpedido, 'EQACCP'),'N')<>v_equipo_nuevo
                AND v_cantidad_4 =0) THEN
                --se envia si el BA comparte infraestructura con un TOIP, Cumple equipo TOIP Destino
                v_necesita := 'SI';
            END IF;

            -- Intercambio CPLAN - Cambio Datos del Cliente
            IF (p_tipo_intercambio = 'CDACL' AND v_ident_comp_toip_o IS NOT NULL
                AND (NVL(v_equipo_estado,'N') = 'LIB' OR  NVL(v_equipo_ant,'N') = v_equipo_nuevo OR  NVL(v_equipo_ba,'N') = v_equipo_nuevo)
                AND NVL(v_equipo_ant_ba,'N') <> v_equipo_nuevo and NVL(fn_valor_equipo_anterior(p_pedido, p_subpedido, 'EQACCP'),'N')<>v_equipo_nuevo
                AND v_cantidad_4 =0) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

            -- Intercambio CREAR - Crear
            IF (p_tipo_intercambio = 'CREAR' AND v_ident_comp_toip_o IS NOT NULL
                AND v_cantidad_4 >0) THEN
                --se envia si el BA comparte infraestructura con un TOIP, Cumple equipo TOIP Destino
                v_necesita := 'SI';
            END IF;

            -- Intercambio DECM - Retirar Cable Modem
            IF (p_tipo_intercambio = 'DECM' AND v_identificador_comparte_infra IS NOT NULL
                AND v_ident_comp_toip_o IS NULL AND (NVL(v_equipo_estado,'N') = 'LIB' OR  NVL(v_equipo_ba,'N') = v_equipo_nuevo
                OR  NVL(v_equipo_ant,'N') = v_equipo_nuevo OR  NVL(v_equipo_ant_ba,'N') = v_equipo_nuevo)) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

            -- Intercambio DECM - Retirar Cable Modem
            IF (p_tipo_intercambio = 'DECM' AND v_identificador_comparte_infra IS NOT NULL
                AND v_ident_comp_toip_o IS NOT NULL AND (NVL(v_equipo_ba,'N') = v_equipo_nuevo OR  NVL(v_equipo_ant_ba,'N') = v_equipo_nuevo)) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
             --Se verifica si hay cambio de tecnologia
             v_cambio_tecno := fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33);
             --DBMS_OUTPUT.PUT_LINE('v_cambio_tecno:'||v_cambio_tecno||'-v_ident_comp_toip_o:'||v_ident_comp_toip_o||'-v_cantidad_2:'||v_cantidad_2);
             --Cambio Tecnologia
             IF (v_cambio_tecno IS NOT NULL) THEN
                v_necesita := 'NO';
                --Cambia de HFC a REDCO
                IF (v_cambio_tecno='HFC') THEN
                    -- Intercambio CPLAN - Cambio Plan
                    IF (p_tipo_intercambio = 'CPLAN' AND v_ident_comp_toip_o IS NOT NULL ) THEN
                        --se envia si el BA comparte infraestructura con un TOIP, Cumple equipo TOIP Destino
                        v_necesita := 'SI';
                    END IF;
                    -- Intercambio CPLAN - Cambio Datos del Cliente
                    IF (p_tipo_intercambio = 'CDACL' AND v_ident_comp_toip_o IS NOT NULL ) THEN
                        --se envia si el BA comparte infraestructura con un TOIP
                        v_necesita := 'SI';
                    END IF;
                    IF (p_tipo_intercambio = 'DECM' AND v_ident_comp_toip_o IS NULL ) THEN
                        --se envia si el BA comparte infraestructura con un TOIP, Cumple equipo TOIP Destino
                        v_necesita := 'SI';
                    END IF;
                END IF;
                 --Cambia de REDCO a HFC
                IF (v_cambio_tecno='REDCO') THEN
                    IF (p_tipo_intercambio = 'DECM' AND v_cantidad_2 = 0) THEN
                        --se envia si el BA comparte infraestructura con un TOIP, Cumple equipo Libre
                        v_necesita := 'SI';
                    END IF;
                END IF;
             END IF;
        END IF;
    END IF;

    --TOIP
    IF (p_producto ='TO') THEN

          IF p_elemento = 'TO'
          THEN
             v_acceso := fn_valor_caract_identif (p_identificador, 4093);
          ELSE
             v_acceso := p_identificador;
          END IF;

        --Verifica si comparte infraestructura con un BA
        v_servicio :='DATOS';
        v_producto :='INTER';
        v_identificador_comparte_infra := NULL;

        v_identificador_comparte_infra := NVL(
        FN_COMPARTE_INFRA (v_acceso, 'HFC', v_servicio,v_producto,p_pedido,p_subpedido,p_solicitud),
        FN_COMPARTE_INFRA2(v_acceso, v_servicio,v_producto,p_pedido,p_subpedido,p_solicitud));


        --Tipo Trabajo NUEVO - Actividad HACAM
        IF (v_tipo_trabajo='NUEVO' AND p_actividad='HACAM') THEN


             OPEN c_tarPlat('CREAR','CUMPL');
             FETCH c_tarPlat INTO v_cantidad;
             CLOSE c_tarPlat;

             OPEN c_tarPlat('CDACL','CUMPL');
             FETCH c_tarPlat INTO v_cantidad_2;
             CLOSE c_tarPlat;

-- 19-12-2011 JPULGARB
             OPEN c_tarPlat_InterCam_Prod('CREAR','INTER');
             FETCH c_tarPlat_InterCam_Prod INTO v_cantidad_5;
             CLOSE c_tarPlat_InterCam_Prod;


            --JPULGARB2
              -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CREAR' AND v_identificador_comparte_infra IS NOT NULL AND (v_cantidad_5 + v_cantidad) =0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                --v_necesita := 'SI';
                -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                -- INICIO CAMBIO 13-08-2014 JHERREMO ITPAM54018_NuevEquiMTAsinRetiEquiConv
                    BEGIN
                        SELECT DECODE((SELECT COUNT(1)
                          FROM fnx_marcas_referencias
                         WHERE multiservicio_id=2
                           AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                               FROM fnx_equipos e
                                                              WHERE e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido,p_subpedido,'EQACCP',200))
                           AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           INTO v_equipo_EMTA
                           FROM DUAL;
                    EXCEPTION
                      WHEN OTHERS THEN
                        v_equipo_EMTA := NULL;
                     END;

                    IF (NVL(v_equipo_EMTA,'N') ='EMTA') THEN
                        BEGIN
                            SELECT estado
                              INTO v_estado_equipo
                              FROM fnx_equipos e
                             WHERE e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                        EXCEPTION
                            WHEN OTHERS THEN
                                v_estado_equipo := NULL;
                        END;

                        IF (NVL(v_estado_equipo,'N') = 'LIB') THEN
                            v_necesita := 'NO';
                        ELSE
                            v_necesita := 'SI';
                        END IF;
                    END IF;
                --v_necesita := 'SI';
                -- FIN CAMBIO 13-08-2014 JHERREMO ITPAM54018_NuevEquiMTAsinRetiEquiConv
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_pedido_retir := NULL;
                    OPEN c_pedido_retir(p_pedido);
                    FETCH c_pedido_retir INTO v_pedido_retir, v_subpedido_retir;
                    CLOSE c_pedido_retir;
                    IF NVL(v_pedido_retir,'X') != 'X' THEN
                        v_equipo_nuevo_dnr := NULL;
                        v_equipo_nuevo_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'200');
                        IF NVL(v_equipo_nuevo_dnr,'X') != 'X' THEN
                            v_equ_ret_nue_iguales := 0;
                            OPEN c_equipo_retir(v_pedido_retir, v_subpedido_retir, v_equipo_nuevo_dnr);
                            FETCH c_equipo_retir INTO v_equ_ret_nue_iguales;
                            CLOSE c_equipo_retir;
                        END IF;
                        IF v_equ_ret_nue_iguales = 0 THEN
                            --se envia si el BA no comparte infraestructura con  un TOIP
                            v_necesita := 'SI';
                        ELSIF v_equ_ret_nue_iguales = 1 THEN
                            v_necesita := 'NO';
                        END IF;
                    END IF;
                END IF;
                -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            END IF;


            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CREAR' AND v_identificador_comparte_infra IS NULL AND v_cantidad =0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                --v_necesita := 'SI';
                -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                    v_necesita := 'SI';
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_pedido_retir := NULL;
                    OPEN c_pedido_retir(p_pedido);
                    FETCH c_pedido_retir INTO v_pedido_retir, v_subpedido_retir;
                    CLOSE c_pedido_retir;
                    IF NVL(v_pedido_retir,'X') != 'X' THEN
                        v_equipo_nuevo_dnr := NULL;
                        v_equipo_nuevo_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'200');
                        IF NVL(v_equipo_nuevo_dnr,'X') != 'X' THEN
                            v_equ_ret_nue_iguales := 0;
                            OPEN c_equipo_retir(v_pedido_retir, v_subpedido_retir, v_equipo_nuevo_dnr);
                            FETCH c_equipo_retir INTO v_equ_ret_nue_iguales;
                            CLOSE c_equipo_retir;
                        END IF;
                        IF v_equ_ret_nue_iguales = 0 THEN
                            --se envia si el BA no comparte infraestructura con  un TOIP
                            v_necesita := 'SI';
                        ELSIF v_equ_ret_nue_iguales = 1 THEN
                            v_necesita := 'NO';
                        END IF;
                    END IF;
                END IF;
                -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            END IF;

            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NULL AND v_cantidad >0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

            -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NULL AND v_cantidad = 0 AND v_cantidad_2 > 0) THEN
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                    v_necesita := 'NO';
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_necesita := 'SI';
                END IF;
            END IF;
            -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.

             -- Intercambio CDACL - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
             IF (p_tipo_intercambio = 'CDACL' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_2=0 AND v_cantidad_5 > 0) THEN  -- 27-12-2011 JPULGARB
                    --se envia si el BA no comparte infraestructura con un TOIP
                    v_necesita := 'SI';
             END IF;

              -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
            IF (p_tipo_intercambio = 'CDACL' AND v_cantidad = 0 AND v_cantidad_2 = 0 AND  v_cantidad_5 =0 ) THEN
                v_dnr := NULL;
                v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                IF NVL(v_dnr,'X') != 'DNR' THEN
                    --se envia si el BA no comparte infraestructura con  un TOIP
                    v_necesita := 'NO';
                ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                    v_pedido_retir := NULL;
                    OPEN c_pedido_retir(p_pedido);
                    FETCH c_pedido_retir INTO v_pedido_retir, v_subpedido_retir;
                    CLOSE c_pedido_retir;
                    IF NVL(v_pedido_retir,'X') != 'X' THEN
                        v_equipo_nuevo_dnr := NULL;
                        v_equipo_nuevo_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'200');
                        IF NVL(v_equipo_nuevo_dnr,'X') != 'X' THEN
                            v_equ_ret_nue_iguales := 0;
                            OPEN c_equipo_retir(v_pedido_retir, v_subpedido_retir, v_equipo_nuevo_dnr);
                            FETCH c_equipo_retir INTO v_equ_ret_nue_iguales;
                            CLOSE c_equipo_retir;
                        END IF;
                        IF v_equ_ret_nue_iguales = 0 THEN
                            --se envia si el BA no comparte infraestructura con  un TOIP
                            v_necesita := 'NO';
                        ELSIF v_equ_ret_nue_iguales = 1 THEN
                            v_necesita := 'SI';
                        END IF;
                    END IF;
                END IF;
            END IF;
            -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.


             -- Intercambio CEQUI - Cambia Equipo - Si se está cumpliendo con un equipo LIB - EMTA
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_2 >0 ) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_cequi := FALSE;
                    begin
                         select decode((select count(1)
                            from fnx_marcas_referencias
                            where multiservicio_id=2
                            and (marca_id, referencia_id) in (select marca_id, referencia_id
                                                           from fnx_equipos e
                                                           where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200)
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           into v_equipo_EMTA
                          from dual;
                    exception
                      when others then
                        v_equipo_EMTA := null;
                     end;

                    IF (NVL(v_equipo_EMTA,'N') ='EMTA') THEN
                      begin
                        select estado
                        into v_estado_equipo
                        from fnx_equipos e
                        where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                       exception
                      when others then
                        v_estado_equipo := null;
                      end;

                        IF (nvl(v_estado_equipo,'N') = 'LIB') THEN
                            v_cequi := TRUE;
                             v_necesita := 'SI';
                        END IF;
                    END IF;
             END IF;

             -- 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.
              -- Intercambio CEQUI - Cambia Equipo - Si se está cumpliendo con un equipo LIB - EMTA
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad=0 AND v_cantidad_2 > 0 ) THEN
                --se envia si el BA comparte infraestructura con un TOIP
                v_cequi := FALSE;
                    begin
                         select decode((select count(1)
                            from fnx_marcas_referencias
                            where multiservicio_id=2
                            and (marca_id, referencia_id) in (select marca_id, referencia_id
                                                           from fnx_equipos e
                                                           where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200)
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           into v_equipo_EMTA
                          from dual;
                    exception
                      when others then
                        v_equipo_EMTA := null;
                     end;

                    IF (NVL(v_equipo_EMTA,'N') ='EMTA') THEN
                      begin
                        select estado
                        into v_estado_equipo
                        from fnx_equipos e
                        where e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                       exception
                      when others then
                        v_estado_equipo := null;
                      end;

                        IF (nvl(v_estado_equipo,'N') = 'LIB') THEN
                            v_cequi := TRUE;
                            v_necesita := 'SI';
                            v_dnr := NULL;
                            v_dnr := fn_valor_caract_subpedido(p_pedido, p_subpedido,'4936');
                            IF NVL(v_dnr,'X') != 'DNR' THEN
                               --se envia si el BA no comparte infraestructura con  un TOIP
                               v_necesita := 'NO';
                            ELSIF NVL(v_dnr,'X') = 'DNR' THEN
                               v_necesita := 'SI';
                            END IF;
                        END IF;
                    END IF;
             END IF;
             -- FIN 22/05/2014  GSANTOS ITPAM50300_NUEVO_RETIR.

        -- INICIO CAMBIO 13-08-2014 JHERREMO ITPAM54018_NuevEquiMTAsinRetiEquiConv
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad = 0 AND v_cantidad_2 = 0 AND v_cantidad_5 = 0) THEN
                BEGIN
                    SELECT DECODE ((SELECT   COUNT (1)
                      FROM fnx_marcas_referencias
                     WHERE multiservicio_id = 2
                       AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                           FROM fnx_equipos e
                                                          WHERE e.equipo_id = FN_VALOR_CARACT_SUBELEM (p_pedido,p_subpedido,'EQACCP',200))
                       AND TIPO_ELEMENTO_ID = 'CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                      INTO v_equipo_EMTA
                      FROM DUAL;
                EXCEPTION
                    WHEN OTHERS THEN
                        v_equipo_EMTA := NULL;
                END;

                IF (NVL (v_equipo_EMTA, 'N') = 'EMTA') THEN
                    BEGIN
                        SELECT estado
                          INTO v_estado_equipo
                          FROM fnx_equipos e
                         WHERE e.equipo_id = FN_VALOR_CARACT_SUBELEM (p_pedido,p_subpedido,'EQACCP',200);
                    EXCEPTION
                        WHEN OTHERS THEN
                           v_estado_equipo := NULL;
                    END;

                    IF (NVL (v_estado_equipo, 'N') = 'LIB') THEN
                        v_cequi := TRUE;
                        v_necesita := 'SI';
                        v_dnr := NULL;
                        v_dnr := fn_valor_caract_subpedido (p_pedido,p_subpedido,'4936');
                        IF NVL (v_dnr, 'X') != 'DNR' THEN
                            v_necesita := 'SI';
                        ELSIF NVL (v_dnr, 'X') = 'DNR' THEN
                           v_necesita := 'NO';
                        END IF;
                    END IF;
                END IF;
            END IF;
        -- FIN CAMBIO 13-08-2014 JHERREMO ITPAM54018_NuevEquiMTAsinRetiEquiConv

        END IF;

        --Tipo Trabajo RETIR - Actividad DECAM
        IF (v_tipo_trabajo='RETIR' AND p_actividad='DECAM') THEN
            -- Intercambio DECM - Deshabilitar Cable Modem:
            IF (p_tipo_intercambio = 'DECM' AND v_identificador_comparte_infra IS NULL) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

            -- 03-12-2011 JPULGARB
            OPEN c_tarPlat_InterCam_Prod('CDACL','INTER');
             FETCH c_tarPlat_InterCam_Prod INTO v_cantidad_5;
             CLOSE c_tarPlat_InterCam_Prod;

            IF v_cantidad_5 = 0 THEN
                -- Intercambio CDACL - Cambia datos del Cliente
                IF (p_tipo_intercambio = 'CDACL' AND v_identificador_comparte_infra IS NOT NULL) THEN
                    --se envia si el BA comparte infraestructura con un TOIP
                    v_necesita := 'SI';
                END IF;
                -- 26-03-2014  Se valida tambien si llega un intercambio de Actualizacion de plan (contrato)
                --  para generar la tarea plataforma para este intercambio
                IF (p_tipo_intercambio = 'CPLAN' AND v_identificador_comparte_infra IS NOT NULL) THEN
                    --se envia si el BA comparte infraestructura con un TOIP
                    v_necesita := 'SI';
                END IF;
                -- 26-03-2014 - Fin

            ELSE
                IF (p_tipo_intercambio = 'DECM' AND v_identificador_comparte_infra IS NOT NULL) THEN
                    --se envia si el BA comparte infraestructura con un TOIP
                    v_necesita := 'SI';
                END IF;

            END IF;
        END IF;

        --Tipo Trabajo SUSPE - Actividad SUSPE
        IF (v_tipo_trabajo='SUSPE' AND p_actividad='DECAM') THEN
            -- Intercambio SUSPE - Deshabilitar Cable Modem:
            IF (p_tipo_intercambio = 'SUSPE' AND v_identificador_comparte_infra IS NULL) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;

        --Tipo Trabajo RECON - Actividad ACTIV
        IF (v_tipo_trabajo='RECON' AND p_actividad='HACAM') THEN
            -- Intercambio RECON - Habilitar Cable Modem:
            IF (p_tipo_intercambio = 'RECON' AND v_identificador_comparte_infra IS NULL) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;

        --Tipo Trabajo CAMBI - Actividad HACAM - Cambio de Equipo
        IF (v_tipo_trabajo='CAMBI' AND p_actividad='HACAM' and p_elemento ='CABLEM') THEN
            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI') THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;

        --Tipo Trabajo TRASL - Actividad HACAM
        IF (v_tipo_trabajo='TRASL' AND p_actividad='HACAM') THEN
             OPEN c_tarPlat('CREAR','CUMPL');
             FETCH c_tarPlat INTO v_cantidad;
             CLOSE c_tarPlat;

             OPEN c_tarPlat('CDACL','CUMPL');
             FETCH c_tarPlat INTO v_cantidad_2;
             CLOSE c_tarPlat;

             OPEN c_tarPlat('CEQUI','CUMPL');
             FETCH c_tarPlat INTO v_cantidad_4;
             CLOSE c_tarPlat;

             v_ident_comp_ba_o  := FN_COMPARTE_INFRA (v_acceso, 'HFC', v_servicio,v_producto,null, null, null);

             v_equipo_nuevo := fn_valor_caract_subelem (p_pedido, p_subpedido, 'EQACCP', 200);


             OPEN  c_equipo_nuevo(v_equipo_nuevo,'CABLEM');
             FETCH c_equipo_nuevo INTO v_equipo_ident,v_equipo_estado,v_tipo_elemento,v_equipo_marca,v_equipo_ref;
             CLOSE c_equipo_nuevo;

             v_equipo_toip := null;
             OPEN  c_equipo_identif(v_acceso,v_tipo_elemento);
             FETCH c_equipo_identif INTO v_equipo_toip;
             CLOSE c_equipo_identif;

              IF (v_ident_comp_ba_o IS NOT NULL) THEN
                 OPEN  c_equipo_identif(v_ident_comp_ba_o,v_tipo_elemento);
                 FETCH c_equipo_identif INTO v_equipo_ba;
                 CLOSE c_equipo_identif;
              END IF;

               /*DBMS_OUTPUT.PUT_LINE('v_identificador_comparte_infra:'||v_identificador_comparte_infra);
               DBMS_OUTPUT.PUT_LINE('v_equipo_nuevo:'||v_equipo_nuevo);
               DBMS_OUTPUT.PUT_LINE('v_equipo_estado:'||v_equipo_estado);*/

            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CREAR' AND v_identificador_comparte_infra IS NULL AND v_cantidad =0
                AND NVL(v_equipo_estado,'N') = 'LIB') THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            IF (p_tipo_intercambio = 'CEQUI' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_4 =0
                AND NVL(v_equipo_estado,'N') = 'LIB') THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            /*IF (p_tipo_intercambio = 'CEQUI' AND v_ident_comp_ba_o IS NOT NULL AND v_cantidad_4 =0
                AND NVL(v_equipo_ba,'N') = v_equipo_nuevo AND v_equipo_toip IS NOT NULL) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;*/
            IF (p_tipo_intercambio = 'CPLAN' AND v_ident_comp_ba_o IS NOT NULL AND v_cantidad_4 =0
                AND NVL(v_equipo_ba,'N') = v_equipo_nuevo AND v_cantidad_2=0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            IF (p_tipo_intercambio = 'CDACL' AND v_ident_comp_ba_o IS NOT NULL AND v_cantidad_4 =0
                AND NVL(v_equipo_ba,'N') = v_equipo_nuevo AND v_cantidad_3=0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CEQUI - Cambiar Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CAMEQ' AND v_identificador_comparte_infra IS NULL AND v_cantidad >0) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            IF (p_tipo_intercambio = 'CAMEQ' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_4 >0
                AND NVL(v_equipo_estado,'N') = 'LIB') THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            IF (p_tipo_intercambio = 'CAMEQ' AND v_ident_comp_ba_o IS NOT NULL AND (v_cantidad_4 >0 OR v_cantidad_2>0
                OR v_cantidad_3>0 ) AND NVL(v_equipo_ba,'N') = v_equipo_nuevo) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

             -- Intercambio CPLAN - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
             IF (p_tipo_intercambio = 'CPLAN' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_2=0
                 AND NVL(v_equipo_estado,'N') = 'OCU' AND NVL(v_equipo_ident,'N') = v_acceso ) THEN
                    --se envia si el BA no comparte infraestructura con un TOIP
                    v_necesita := 'SI';
             END IF;

              -- Intercambio CDACL - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
             IF (p_tipo_intercambio = 'CDACL' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_2=0
                 AND NVL(v_equipo_estado,'N') = 'OCU' AND NVL(v_equipo_ident,'N') = v_acceso) THEN
                    --se envia si el BA no comparte infraestructura con un TOIP
                    v_necesita := 'SI';
             END IF;

              -- Intercambio CPLAN - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
             IF (p_tipo_intercambio = 'CAMEQ' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_2>0
                 AND NVL(v_equipo_estado,'N') = 'OCU') THEN
                    --se envia si el BA no comparte infraestructura con un TOIP
                    v_necesita := 'SI';
             END IF;


        END IF;
        --Tipo Trabajo TRASL - Actividad DECAM
        IF (v_tipo_trabajo='TRASL' AND p_actividad='DECAM') THEN

             v_estado_soli := pkg_solicitudes.FN_ESTADO(p_pedido, p_subpedido,p_solicitud);

             IF(v_estado_soli IN ('CUMPL','FACTU')) THEN
                v_ident_comp_ba_o  := FN_COMPARTE_INFRA_RESP (v_acceso, 'HFC', v_servicio,v_producto,p_pedido, p_subpedido,p_solicitud);
             ELSE
                v_ident_comp_ba_o  := FN_COMPARTE_INFRA (v_acceso, 'HFC', v_servicio,v_producto,null, null, null);
             END IF;

             v_equipo_nuevo := fn_valor_caract_subelem (p_pedido, p_subpedido, 'EQACCP', 200);

             OPEN  c_equipo_nuevo(v_equipo_nuevo,'CABLEM');
             FETCH c_equipo_nuevo INTO v_equipo_ident,v_equipo_estado,v_tipo_elemento,v_equipo_marca,v_equipo_ref;
             CLOSE c_equipo_nuevo;

             -- Intercambio DECM - Retirar Cable Modem: Para nuevos por instalacion o mantenimiento
             IF (p_tipo_intercambio = 'DECM' AND v_identificador_comparte_infra IS NOT NULL AND v_cantidad_2=0) THEN
                    --se envia si el BA no comparte infraestructura con un TOIP
                    v_necesita := 'SI';
             END IF;
              -- Intercambio DECM - Retirar Cable Modem: Para nuevos por instalacion o mantenimiento
             IF (p_tipo_intercambio = 'DECM' AND v_identificador_comparte_infra IS NULL AND v_cantidad_2=0) THEN
                    --se envia si el BA no comparte infraestructura con un TOIP
                    v_necesita := 'SI';
             END IF;
        END IF;
    END IF;

    --3PLAY
    IF (p_producto ='3PLAY') THEN

        --Tipo Trabajo NUEVO - Actividad HACAM
        IF (v_tipo_trabajo='NUEVO' AND p_actividad='HACAM') THEN

             OPEN c_tarPlat('CREAR','CUMPL');
             FETCH c_tarPlat INTO v_cantidad;
             CLOSE c_tarPlat;
            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CREAR' AND v_cantidad =0 ) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_cantidad >0 ) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;

        --Tipo Trabajo RETIR - Actividad DECAM
        IF (v_tipo_trabajo='RETIR' AND p_actividad='DECAM') THEN
            -- Intercambio DECM - Deshabilitar Cable Modem:
            IF (p_tipo_intercambio = 'DECM' ) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;

        --Tipo Trabajo SUSPE - Actividad SUSPE
        IF (v_tipo_trabajo='SUSPE' AND p_actividad='DECAM') THEN
            -- Intercambio SUSPE - Deshabilitar Cable Modem:
            IF (p_tipo_intercambio = 'SUSPE' ) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;

        END IF;

        --Tipo Trabajo RECON - Actividad ACTIV
        IF (v_tipo_trabajo='RECON' AND p_actividad='HACAM') THEN
            -- Intercambio RECON - Habilitar Cable Modem:
            IF (p_tipo_intercambio = 'RECON' ) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;

         --Tipo Trabajo CAMBI - Actividad HACAM - Cambio de Equipo
        IF (v_tipo_trabajo='CAMBI' AND p_actividad='HACAM' and p_elemento ='CABLEM') THEN
            -- Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion o mantenimiento
            IF (p_tipo_intercambio = 'CEQUI') THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;

        --Tipo Trabajo CAMBI - Actividad HACAM
        IF (v_tipo_trabajo='CAMBI' AND p_actividad='HACAM' and p_elemento ='3PLAY') THEN
            -- Intercambio CDACL - Cambio Datos del Cliente
            IF (p_tipo_intercambio = 'CDACL' ) AND (NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'3PLAY',2626),'N') <>'N') THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
            -- Intercambio CPLAN - Cambio Plan
            IF (p_tipo_intercambio = 'CPLAN' ) THEN
                --se envia si el BA no comparte infraestructura con un TOIP
                v_necesita := 'SI';
            END IF;
        END IF;
    END IF;

    -- --------------------------------------------------------------------------------------------
    -- Implementación de Activación Automática de CableModem para el producto CENTREX
    -- Autor: John Carlos Gallego - Analista Desarrollador MVM
    -- Fecha: 27 de julio de 2010
    -- --------------------------------------------------------------------------------------------
    -- Producto Centrex
    IF (p_producto ='CNTXIP') THEN
        -- Obtener el identificador de acceso para la cuenta.  La característica 1
        -- se refiere al Acceso Primario para Centrex.
        IF p_elemento = 'CNTXIP'
        THEN
            v_acceso := fn_valor_caract_identif (p_identificador, 1);
        ELSE
            v_acceso := p_identificador;
        END IF;

        -- Tipo Trabajo NUEVO - Actividad HACAM (Habilitar CableModem)
        IF (v_tipo_trabajo = 'NUEVO' AND p_actividad = 'HACAM') THEN

             OPEN c_tarPlat('CREAR','CUMPL');
             FETCH c_tarPlat INTO v_cantidad;
             CLOSE c_tarPlat;
            -- 2010-09-27 Jgomezve Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion
            IF (p_tipo_intercambio = 'CREAR' AND v_cantidad = 0) THEN
                v_necesita := 'SI';
            END IF;

            --2010-09-27 Jgomezve Se genera el intercambio para nuevos por mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_cantidad >0 ) THEN
                v_necesita := 'SI';
            END IF;

        END IF;

        -- Tipo Trabajo RETIR - Actividad DECAM (Deshabilitar CableModem)
        IF (v_tipo_trabajo = 'RETIR' AND p_actividad = 'DECAM') THEN
            -- Intercambio DECM - Deshabilitar Cable Modem
            IF (p_tipo_intercambio = 'DECM') THEN
                v_necesita := 'SI';
            END IF;
        END IF;

        -- Tipo Trabajo SUSPE - Actividad SUSPE (Suspender Servicio)
        IF (v_tipo_trabajo = 'SUSPE' AND p_actividad = 'DECAM') THEN
            -- Intercambio SUS - Deshabilitar Cable Modem
            IF (p_tipo_intercambio = 'SUSPE') THEN
                v_necesita := 'SI';
            END IF;
        END IF;

        -- Tipo Trabajo RECON - Actividad HACAM
        IF (v_tipo_trabajo = 'RECON' AND p_actividad = 'HACAM') THEN
            -- Intercambio REC - Habilitar Cable Modem:
            IF (p_tipo_intercambio = 'RECON') THEN
                v_necesita := 'SI';
            END IF;
        END IF;

        -- Tipo Trabajo CAMBI - Actividad HACAM (Para cambios de equipo)
        IF (v_tipo_trabajo = 'CAMBI' AND p_actividad = 'HACAM') THEN
            -- Intercambio CEQUI - Habilitar Cable Modem para cambio de equipo:
            IF (p_tipo_intercambio = 'CEQUI') THEN
                v_necesita := 'SI';
            END IF;

            -- El traslado se está aplicando dentro del tipo de trabajo CAMBI
        END IF;

        -- Tipo Trabajo TRASL (FALTA)

    END IF;

    -- --------------------------------------------------------------------------------------------
    -- Implementación de Activación Automática de CableModem para el producto TRONCALES
    -- Autor: John Carlos Gallego - Analista Desarrollador MVM
    -- Fecha: 09 de agosto de 2010
    -- --------------------------------------------------------------------------------------------
    -- Producto Troncales SIP
    IF (p_producto ='TRKSIP') THEN
        -- Obtener el identificador de acceso para la cuenta.  La característica 1
        -- se refiere al Acceso Primario para Troncales.
        IF p_elemento = 'TRKSIP'
        THEN
            v_acceso := fn_valor_caract_identif (p_identificador, 1);
        ELSE
            v_acceso := p_identificador;
        END IF;

        -- Tipo Trabajo NUEVO - Actividad HACAM (Habilitar CableModem)
        IF (v_tipo_trabajo = 'NUEVO' AND p_actividad = 'HACAM') THEN

            OPEN c_tarPlat('CREAR','CUMPL');
            FETCH c_tarPlat INTO v_cantidad;
            CLOSE c_tarPlat;

            -- 2010-09-27 Jgomezve Intercambio CREAR - Crear Cable Modem: Para nuevos por instalacion
            IF (p_tipo_intercambio = 'CREAR' AND v_cantidad = 0) THEN
                v_necesita := 'SI';
            END IF;

            --2010-09-27 Jgomezve Se genera el intercambio para nuevos por mantenimiento
            IF (p_tipo_intercambio = 'CEQUI' AND v_cantidad >0 ) THEN
                v_necesita := 'SI';
            END IF;

        END IF;

        -- Tipo Trabajo RETIR - Actividad DECAM (Deshabilitar CableModem)
        IF (v_tipo_trabajo = 'RETIR' AND p_actividad = 'DECAM') THEN
            -- Intercambio DECM - Deshabilitar Cable Modem
            IF (p_tipo_intercambio = 'DECM') THEN
                v_necesita := 'SI';
            END IF;
        END IF;

        -- Tipo Trabajo SUSPE - Actividad SUSPE (Suspender Servicio)
        IF (v_tipo_trabajo = 'SUSPE' AND p_actividad = 'DECAM') THEN
            -- Intercambio SUS - Deshabilitar Cable Modem
            IF (p_tipo_intercambio = 'SUSPE') THEN
                v_necesita := 'SI';
            END IF;
        END IF;

        -- Tipo Trabajo RECON - Actividad HACAM
        IF (v_tipo_trabajo = 'RECON' AND p_actividad = 'HACAM') THEN
            -- Intercambio REC - Habilitar Cable Modem:
            IF (p_tipo_intercambio = 'RECON') THEN
                v_necesita := 'SI';
            END IF;
        END IF;

        -- Tipo Trabajo CAMBI - Actividad HACAM (Para cambios de equipo)
        IF (v_tipo_trabajo = 'CAMBI' AND p_actividad = 'HACAM') THEN
            -- Intercambio CEQUI - Habilitar Cable Modem para cambio de equipo:
            IF (p_tipo_intercambio = 'CEQUI') THEN
                v_necesita := 'SI';
            END IF;
        END IF;

        -- Tipo Trabajo TRASL (FALTA)
    END IF;

     RETURN v_necesita;
     dbms_output.put_line(p_tipo_intercambio||'-'||v_necesita);
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'FN_VERIFICAR_TAREAS_CABLEM - ERRWO: '
                               || SUBSTR (SQLERRM, 1, 800)
                              );
         RETURN v_necesita;
   END;