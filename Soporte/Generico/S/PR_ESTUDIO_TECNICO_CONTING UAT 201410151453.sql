PROCEDURE       PR_ESTUDIO_TECNICO_CONTING
( w_pedido IN fnx_pedidos.pedido_id%TYPE,
  w_resp IN OUT VARCHAR2)
IS

/*
HISTORIA DE MODIFICACIONES
Fecha        Autor       Observacion
--------     ----------- ------------------------------------------------------------
2006-09-04   Jrincong    Inclusion de procedimiento para enrutarmiento de pedidos
                         de bolsa dinamica ( TELEMP-BOLSAD )
2006-09-06   Jrincong    Inclusion de procedimiento para estudio tecnico pedidos
                         Producto Larga Distancia ( TELLGD-LDIST)
2006-10-20   Ojaramip    Inclusion de procedimiento para estudio tecnico pedidos
                         Producto IDC (IDC-WEBHOS)
2006-11-18   Jrincong    El proc. pr_estudio_tecnico_largadist se ubica antes del proc.
                         pr_enrutar_componentes_aseguramiento. Se estaban quedando pendientes
                         PXSLN los componentes.
2007-02-16   Jrincong    Invocacion de estudio tecnico de Accesos Nacionales para realizar estudio
                         del acceso, antes de los componentes.
2007-05-22   Aacosta     Invocacion de estudio tecnico de Hogar Seguro (PR_ESTUDIO_TECNICO_SEGURIDAD).
2007-06-22   Jrincong    Invocacion de estudio técnico Internet Kids
2007-08-02   Ojaramip    Invocacion de estudio técnico Internet Play
2007-08-24   Jrincong    Lógica para implementar tabla PLSQL de productos del pedido.
2007-08-24   Jrincong    Implementación de función sub_verificar_producto para buscar
                         sí un producto se encuentra en la tabla PLSQL.
2007-08-24   Jrincong    Implementación para invocar solo los procedimientos relacionados
                         con los productos de la solicitud.
2007-08-24   Jrincong    Invocar procedimiento para asignación de banda ancha HFC
2007-09-13   Jrincong    Adición de parámetro PETEC en proc. pr_estudio_tecnico_BAFHC
2007-09-28   Jrincong    Revisión manejo de respuesta w_resp_accesos_internet en proc.
                         pr_estudio_accesos_nles
2007-09-28   Jrincong    Control de Invocación de pr_estudio_estudio_tecnico_internet. Solo
                         si w_resp_accesos_internet = 'S'.
2007-09-28   Jrincong    Manejo de variable w_resp_et_bahfc para respuesta en Pr_Estudio_Tecnico_BAHFC
2007-10-04   AAcosta     Parámetro para verificar mensaje de ET y registro de log.
2007-11-13   Jrincong    Proc. Pr_enrutar_paquete. Se quita condición de producto sub_verificar_producto ('PQUETE')
                         para que sea invocado para todos los productos
2008-02-12   Jrincong    Proc. pr_estudio_tecnico_to.  Se traslada despues de realizar el estudio tecnico
                         de Television y antes de realizar el ET de Banda Ancha HFC
2008-02-12   Jrincong    Invocación de nuevo procedimiento pr_estudio_tecnico_telef_ip
2008-02-12   Jrincong    Cursor c_solic_acceso.  Adición de tipo elemento TOIP
2009-02-12   Wmendozal   Invocación procedimiento para manejar el tema de precalificación de
             Aacosta     pares para el producto IPTV.
2008-03-05   Jrincong    Invocación Estudio Técnico BAREDCO para verificar sí puede estudiarse la solicitud
                         de TOIP por un BAREDCO existente.
2008-04-16   Jrincong    Seccion Estudio TOIP.  Invocar Estudio Técnico Banda Ancha HFC .
2008-05-01   Jrincong    Uso de cursor c_solic_toip, para invocar Estudio Tecnico Banda Ancha
2008-05-17   Jrincong    Invocación de procedimiento Pr_Estudio_Tecnico_BAREDCO
2008-07-16   Jrincong    Invocación de procedimiento Pr_Estudio_Tecnico_TVADSL
2008-07-23   Jrincong    Manejo de parametro 500 para control de Estudio Tecnico
2008-12-21   Jrincong    Invocar procedimiento para verificar solicitudes de retiro de TO, TELEV e INTERNET,
                         con plan de facturacion anticipada.
2009-01-23   Jrincong    Manejo de parametro 700 para control de Estudio Tecnico de Contingencia.
2009-02-12   Jgomezve    Consideración de producto TO para tener en cuenta las transacciones de Cambios de Equipo
                         para el producto de TOIP que se presenta en Cableras.
2009-03-13   Wmendoza    Verificar sí existe una solicitud en concepto PXREI.  Pendiente Retiro Inmediato
2009-03-28   Jrincong    Consideración de producto TV Empresarial para invocar procedimiento de
                         Validaciòn Pago Anticipado. pr_et_validar_pago_anticipado
2009-07-08   Jrincong    Proc. PR_ESTUDIO_TECNICO_BAHFC / PR_ESTUDIO_TECNICO_BAREDCO / PR_ESTUDIO_BAREDCO_TOIP
                         Invocaciòn. Adiciòn de paràmetro p_verificar_asigauto con valor
                         FALSE para controlar la asignaciòn a travès de valores de variables
2009-08-12   Jaristz     Consideración de producto Manos Remotas.
2009-08-25   Aacosta     Se incluye el nuevo producto internet Móvil.
2009-09-01   Aacosta     Validación asociada que sólo se debe provisionar para el producto Internet por demanda
                         por tecnología REDCO.
2009-09-10   Jaristz     Consideración de la Oferta de Conectividad.
2009-09-24   Aacosta     Invocación de nuevo procedimiento para el reuso de la red para pedidos de Banda Ancha por demanda.
2009-09-25   Jrincong    Proc. PR_ESTUDIO_TECNICO_BAHFC.  Se cambia la validación de la bùsqueda de tecnología a
                         la bùsqueda del tipo de Venta No se realiza para el tipo de venta DEM ( x Demanda )
2009-09-28   Aacosta     Se adiciona condición para validar que si el BA por demanda es en COMODATO (CD) se debe
                         hacer reuso de la red de la televisión.
2009-11-09   Aacosta     Se valida que cuando es una cambio de banda ancha fijo a por demanda ejecute los procesos de
                         asignación automática.
2009-12-14   Truizd      Lògica pàra invocar procedimiento para transacciones de Corte y Reconexiòn. Antes, la
                         lògica se encontraba en este procedimiento.
2010-08-24   Wmendoza    Se comenta esta lógica mientras se define nuevamente el tema de precalificación
2010-09-02   Jbuilesm    se adiciona la logica para el producto de linea directa
2011-01-24   Jrincong    Invocacion de estudio técnico de producto Credito Grupo EPM
2011-06-03   Wmendoza    Invocacion de estudio técnico de producto Portabilidad.
2011-11-15   Wmendoza    Invocacion de estudio técnico de producto aliados
2011-12-02   lhernans    LTE. Variable para controlar la respuesta del estudio técnico LTE.
                            LTE. Incovación Estudio Técnico LTE.
2012-01-17   lhernans    SECCIÓN. Creación de solicitudes para MOVLTE.
2012-02-28   lhernans    LTE. Lógica para soporte de Producto APN.
2012-03-13   lhernans    Se inicializan variables de respuesta en S y se cambia lógica de validación de
                               mensajes de respuesta con <> 'S', y no = 'S'. Pues se está sobremontando mensaje final.
                         Ajuste para mensaje de error. Cuando viene nulo, se hace igual a S.
2012-03-12   aagudc      Se actualiza cursor y variables para determinar si existe componente IDIOMA en el pedido, de lo contrario se
                         invoca el PKG de SERALI.
2012-02-16   Jrendbr     Invocacion de estudio técnico de producto INTTIC
2012-03-28   lhernans    LTE. Soporte para Nuevo Disco Duro Virtual con MOVLTE en portafolio.
2012-03-29   lhernans    LTE. Soporte para Nuevo VITRON con MOVLTE en portafolio.
2012-07-05   dojedac     VERTICALES. Se realiza el ajuste del llamado al PR_ESTUDIO_TECNICO_VERTICAL, ya que se esta ejecutando
                         antes del enrutamiento de corte y reconexion y realmente el llamado al pr debe realizarce despues de corte y reconexión
2012-10-17   Jrendbr     Invocacion de estudio técnico de producto IAAS
2012-10-31   aagudc      Se agrega verificaciones para al ejecutar el estudio técnico de Microtic.
2013-02-22   Mmedinac    Req21213: Se agregan los cambios necesarios para el funcionamiento del nuevo producto Fijo LTE
2013-02-28   Gsantos     Se agrega llamado al procedimiento PR_ET_VALIDAR_RETIRO_3066, para cumplir con el tema retiro 3066
2013-03-14   Gsantos     Se modifica el llamado al procedimiento PR_ET_VALIDAR_RETIRO_3066, para que no ejecute el resto del
                         estudio tecnico, se agrega variable en el procedimiento y esta indica si se debe dejar seguir con el
                         estudio tecnico o no
2013-02-12   dojedac     Se agrega verificaciones para al ejecutar el estudio técnico de Voz en la nube.
2013-04-22   Gsantos     Se modifican los llamados a los estudios tecnicos de INTER, TO, TOIP, RDSIP, RDSIB para que
                         evalue si hay un retiro 3066 y no ejecute el estudio tecnico de estos productos pero si el de los demas
                         productos que el pedido tenga
2013-04-08   Gsantos     Se modifica para que el retiro de paquetes aplique 3066
                         Se agrega en el llamado del procedimiento pr_enrutar_paquete la validacion de si debe o no entrar
                         evaluando la variable v_estudio_tec = 'S'
2013-05-17   lhernans    Invocación de procedimiento para estudio de accesos GPON.
2013-08-08   Gsantos     Se comenta el llamado del procedimiento PR_ESTUDIO_TECNICO_INTMOV (w_pedido, w_resp_intmov);
                         ya que se desarrollo otro procedimiento de provision rapida llamado PR_PROCESAR_PEDIDOS_PETEC_RAP
                         para que este sea quien los procese
2013-08-08   Eaguirrg    Invocación de estudio tecnico para produc SEREQU (Servicios y Equipos)
2013-09-18   Srojaslo    Se comentan las líneas que incluían al producto COMUNI en las validaciones para invocar
                         el procedimiento PR_ENRUTAR_EQ_RDSI, pues para este producto ya se diseñó el procedimiento PR_ESTUDIO_TECNICO_COMUNI.
20131025    Jrendbr      Invocar el procedimiento para transacciones de SMPRO.
2013-12-19  jcardega     Invocación de procedimiento para estudio técnico SERCOM
2014-02-27  eaguirrg     Se incluiye llamado a nuevo procedimiento de codigo familiar para determinar y enrutar el estudio tecnico de cada producto.
                         Se determina por cada producto si lo que hay que generar es un codigo familiar, personal o pyme. Al final del procedimjiento
                         se levantaran mediante un cursor los pedidos a los que se les realizo estudio tecnico para enrutarlos por codigo familiar pyme
                         o personal
2014-03-05   Jrincong    Implementación de lista tipo Associative Array indexada ( tipo typ_aa_producto )por producto para mejorar la evaluación que
                         se realiza para invocar los diferentes Estudios Técnicos.
2014-03-05   Jrincong    Cambio en evaluación de producto en pedido.  Se cambian las instrucciones que haccen referencia a la función
                         sub_verificar_producto('PRODUCTO')  por la verificación del producto en la tabla:  vt_productos_pedido.EXISTS ('PRODUCTO')
2014-03-05   Jrincong    Se cambia la asignación de la lista vt_tabla_productos por la asignación en la lista vt_productos_pedido
                         Antes era:  v_tabla_indice := NVL (v_tabla_indice, 0) + 1; ||  vt_tabla_productos (v_tabla_indice) := v_carga_prod;
2014-03-05   Jrincong    El procedimento PR_ENRUTAR_CMP_ASEGURAMIENTO sólo se debe invocar sí dentro del pedido viene el producto PQUETE.
2014-03-05   Jrincong    Se borra el arreglo vt_productos_pedido de Productos del pedido.
2014-03-27   lhernans    MODEQU. Módulo de equipos PYMES. Invocación Estudio técnico.
2014-04-09   jcardega    Invocación de procedimiento para estudio técnico del producto de Seguridad Gestionada (SEGURI).  REQ 29680.
2014-04-30   Jrincong    REQ 36900. GPON Hogares. Variables para manejo de lógica de tecnología propuesta por Producto.
2014-04-30   Jrincong    REQ 36900. GPON Hogares. Sección Definición de tecnología propuesta  para los productos TO, TELEV e INTER
2014-04-30   Jrincong    REQ 36900. GPON Hogares. Adición de condición v_tecno_propuesta_TO <> 'GPON' para invocar proc. PR_ESTUDIO_BAREDCO_TOIP sólo sí la tecnología
                         de propuesta Provisión es diferente de GPON
2014-04-30   Jrincong    REQ 36900. GPON Hogares. Adición de condición v_tecno_propuesta_TO <> 'GPON' para invocar proc. PR_ESTUDIO_TECNICO_BAHFC
                         sólo sí la tecnología de propuesta Provisión es diferente de GPON
2014-04-30   Jrincong    REQ 36900. GPON Hogares. Invocar proc. PR_ESTUDIO_TECNICO_TO_GPON para provisión de accesos de TOIP cuya
                         tecnología propuesta sea GPON
2014-04-30   Jrincong    REQ 36900. GPON Hogares. Adición de condición v_tecno_propuesta_INTER = 'GPON' para invocar proc. PR_ESTUDIO_TECNICO_PROV_GPON
2014-04-30   Jrincong    REQ 36900. GPON Hogares. Adición de condición v_tecno_propuesta_INTER <> 'GPON' para evitar invocar procedimientos
                         de asignación automática BAHFC y BAREDCO cuando la tecnología propuesta es GPON.
2014-05-19   Jrincong    REQ 36900. GPON Hogares. Uso de variable v_resp_prov_TELEV_gpon para respuesta de PR_ESTUDIO_TECNICO_TELEV_GPON.
2014-05-19   Jrincong    REQ 36900. GPON Hogares. Uso de variable v_resp_prov_TO_gpon para respuesta de PR_ESTUDIO_TECNICO_TO_GPON.
2014-05-19   Jrincong    REQ 36900. GPON Hogares. Verificación de valor de respuestas de Estudio Tecnico Telefonía GPON y Estudio Técnico de
                         Telefonía IP. Si el valor es diferente a S, AA o nula se interpreta como error y se asigna a la variable w_resp.
2014-09-18   AARBOB      se realiza busqueda del portafolio del pedido,y con dicho valor se debe realizar la siguiente valiación,
                         en caso que sea MEDANTCOL o CARBOLCOL, debe llamar al procedimiento PR_VALIDA_COD_FIDELIZACION


*/

   v_uen                     fnx_caracteristica_solicitudes.valor%type;--2014-02-27 eaguirrg
   v_valor_4975              fnx_caracteristica_solicitudes.valor%type;--2014-02-27 eaguirrg
   v_valor_2                 fnx_caracteristica_solicitudes.valor%type;--2014-02-27 eaguirrg
   v_usuario                 varchar2(60);  --2014-02-27 eaguirrg
   v_mensaje_est_cod         varchar2(4000);--2014-02-27 eaguirrg
   w_resp_desactivar_et       VARCHAR2 (2);
   e_et_no_ejecutado          EXCEPTION;

   w_resp_enrutar_pventa      VARCHAR2 (200);
   w_resp_enrutar             VARCHAR2 (50);
   w_resp_enrutar_rdsi        VARCHAR2 (200);
   w_resp_enrutar_3play       VARCHAR2 (200);
   w_resp_enrutar_tvemp       VARCHAR2 (200);
   w_resp_enrutar_lantla      VARCHAR2 (200);
   w_resp_cmp_internet        VARCHAR2 (200);
   w_resp_enrutar_cntxip      VARCHAR2 (200);
   w_resp_enrutar_paquete     VARCHAR2 (200);

   w_resp_cmpaseg             VARCHAR2 (200);
   w_resp_internet            VARCHAR2 (50);
   w_resp_accesos_internet    VARCHAR2 (50);
   w_resp_et_bahfc            VARCHAR2 (200);
   w_resp_et_iadsl            VARCHAR2 (200);
   w_resp_et_tvadsl           VARCHAR2 (200);

   w_resp_et_bartoip          VARCHAR2 (200);

   w_resp_clicktodial         VARCHAR2 (50);
   w_resp_comunidad_cerrada   VARCHAR2 (50);
   w_resp_asp                 VARCHAR2 (50);
   w_resp_ctaper              VARCHAR2 (50);
   w_resp_intemp              VARCHAR2 (50);
   w_resp_telev               VARCHAR2 (50);
   w_resp_tvemp               VARCHAR2 (50);
   w_resp_multinet            VARCHAR2 (50);
   w_resp_tlbvir              VARCHAR2 (50);
   w_resp_to                  VARCHAR2 (50);
   w_resp_telef_ip            VARCHAR2 (50);

   w_resp_pbx                 VARCHAR2 (50);
   w_resp_lantla              VARCHAR2 (50);
   w_resp_cntxip              VARCHAR2 (50);
   w_resp_comuni              VARCHAR2 (50);
   w_resp_conect              VARCHAR2 (50);
   w_resp_ldirec              VARCHAR2 (50);
   w_resp_manrem              VARCHAR2 (50);
   w_resp_3play               VARCHAR2 (50);
   w_resp_movilidad           VARCHAR2 (200);
   w_resp_desintegrar_pdto    VARCHAR2 (50);
   w_resp_beeper              VARCHAR2 (50);
   w_resp_tmovil              VARCHAR2 (50);
   w_resp_trk                 VARCHAR2 (50);
   w_resp_enlaces_lan         VARCHAR2 (100);
   w_resp_srvpro              VARCHAR2 (100);
   w_resp_bdyn                VARCHAR2 (200);
   w_resp_largadist           VARCHAR2 (200);

   w_resp_idc                 VARCHAR2 (200);
   w_resp_redint              VARCHAR2 (200);
   w_resp_segur               VARCHAR2 (200);
   w_resp_internet_kids       VARCHAR2 (100);
   w_resp_internet_play       VARCHAR2 (100);
   w_resp_toip                VARCHAR2 (100);
   w_resp_tsip                VARCHAR2 (200);
   w_resp_et_hfctsip          VARCHAR2 (100);
   w_resp_et_redcotsip        VARCHAR2 (100);
   w_resp_accesos_trksip      VARCHAR2 (100);
   w_resp_intmov              VARCHAR2 (100);
   w_resp_et_reuso            VARCHAR2 (100);
   w_resp_cambio_estado       FNX_MENSAJES_ERRORES.descripcion%TYPE;
   --2014-03-27 lhernans Variable para almacenar respuesta de estudio técnico MODEQU
   w_resp_modequ              VARCHAR2(100);

   w_parametro                FNX_PARAMETROS.parametro_id%TYPE;
   v_estudiot_ok              FNX_PARAMETROS.valor%TYPE;

   w_resp_vxpga               VARCHAR2 (100);
   w_resp_precal              VARCHAR2 (100);
   w_resp_etpetic             VARCHAR2 (100);
   v_soli_srv_trk             NUMBER (2) := 0;
   b_provi_trksip             BOOLEAN;
   w_resp_vxprei              VARCHAR2 (100);
   w_resp_cgepm               VARCHAR2 (100);
   w_resp_portab              VARCHAR2 (100);
   w_resp_idiomas             VARCHAR2 (100);
   --2013-05-17 lhernans Creación de variable para respuesta de procedimiento
   w_resp_prov_gpon           VARCHAR2(200) := 'S';
   --2013-08-08 Eaguirrg Creación de variable para respuesta de procedimiento estudio tecnico SEREQU
   w_resp_serequ              VARCHAR2(200);

   v_propiedad_equipo FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE
         := NULL ;
   v_departamento FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE
         := NULL ;

   w_resp_lte                 VARCHAR2 (200);
   -- 2012-01-17 Variable para controlar mensaje de PR de creación solicitudes LTE
   w_resp_crear_solic         VARCHAR2 (200);
   --2012-02-28
   w_resp_apn                 VARCHAR2 (200);
   --2012-03-28 Variable para controlar adición de producto a lista en memoria
   v_carga_prod               fnx_productos.producto_id%TYPE;

   -- 2012-10-31 aagudc Variabe de respuesta estudio técnico Microtic.
   w_resp_microtic            VARCHAR2 (200);

   -- 2013-02-12 dojedacc Variabe de respuesta estudio técnico Voz en la nube.
   w_resp_voznub              VARCHAR2 (200);

   -- 2013-02-28 Gsantos Retiro 3066
   v_retiro_3066       NUMBER (2);
   w_resp_ret3066      VARCHAR2 (100);
   v_estudio_tec       VARCHAR2 (2);

   -- 2014-09-18   AARBOB
   V_Portafolio                    FNX_SOLICITUDES.MUNICIPIO_ID%type;

   -- cursor para obtener la característica 7 Fecha Real Retiro (dd/mm/aaaa)
   CURSOR c_caract_7(p_pedido fnx_pedidos.pedido_id%TYPE)
   IS
   SELECT COUNT(1)
     FROM fnx_caracteristica_solicitudes
    WHERE PEDIDO_ID = p_pedido
      AND CARACTERISTICA_ID = 7
      AND VALOR IS NOT NULL;
   -- Fin 2013-02-28 Gsantos Retiro 3066

   -- Cursor para buscar solicitudes de Acceso en TECNI-PETEC
   CURSOR c_solic_acceso
   IS
      SELECT   subpedido_id, solicitud_id
        FROM   FNX_SOLICITUDES
       WHERE       pedido_id = w_pedido
               AND servicio_id = 'DATOS'
               AND producto_id = 'INTER'
               AND tipo_elemento_id = 'ACCESP'
               AND tipo_elemento = 'CMP'
               AND estado_id = 'TECNI'
               AND concepto_id = 'PETEC';

   -- Cursor para buscar solicitudes de SEDEIP de troncales de tecnología HFC en TECNI-PETEC
   CURSOR c_solic_acceso_tsip_hfc
   IS
      SELECT   subpedido_id, solicitud_id
        FROM   FNX_SOLICITUDES
       WHERE       pedido_id = w_pedido
               AND servicio_id = 'DATOS'
               AND producto_id = 'TRKSIP'
               AND tipo_elemento_id = 'SEDEIP'
               AND tipo_elemento = 'CMP'
               AND estado_id = 'TECNI'
               AND concepto_id = 'PETEC'
               AND PKG_SOLICITUDES.fn_valor_caracteristica (pedido_id,
                  subpedido_id, solicitud_id, 33) = 'HFC';

   -- Cursor para buscar solicitudes de SEDEIP de troncales de tecnología REDCO en TECNI-PETEC
   CURSOR c_solic_acceso_tsip_redco
   IS
      SELECT   subpedido_id, solicitud_id
        FROM   FNX_SOLICITUDES
       WHERE       pedido_id = w_pedido
               AND servicio_id = 'DATOS'
               AND producto_id = 'TRKSIP'
               AND tipo_elemento_id = 'SEDEIP'
               AND tipo_elemento = 'CMP'
               AND estado_id = 'TECNI'
               AND concepto_id = 'PETEC'
               AND PKG_SOLICITUDES.fn_valor_caracteristica (pedido_id,
                  subpedido_id, solicitud_id, 33) = 'REDCO';


   -- Cursor para buscar la solicitud de servicio NUEVA en el pedido.
   CURSOR c_solic_srv
   IS
      SELECT   COUNT ( * )
        FROM   FNX_SOLICITUDES A, FNX_REPORTES_INSTALACION B
       WHERE       a.pedido_id = w_pedido
               AND a.pedido_id = b.pedido_id
               AND a.subpedido_id = b.subpedido_id
               AND a.solicitud_id = b.solicitud_id
               AND a.tipo_elemento IN ('SRV', 'CMP')
               AND a.servicio_id = 'DATOS'
               AND a.producto_id = 'TRKSIP'
               AND a.tipo_elemento_id IN ('TRKSIP', 'SEDEIP')
               AND b.tipo_solicitud = 'NUEVO';

   -- Cursor para buscar solicitudes de TOIP en TECNI-PETEC
   CURSOR c_solic_toip
   IS
      SELECT   subpedido_id, solicitud_id
        FROM   FNX_SOLICITUDES
       WHERE       pedido_id = w_pedido
               AND servicio_id = 'TELRES'
               AND producto_id = 'TO'
               AND tipo_elemento_id = 'TOIP'
               AND tipo_elemento = 'CMP'
               AND estado_id = 'TECNI'
               AND concepto_id = 'PETEC';

   -- Cursor para buscar solicitudes de INSIP en TECNI-PETEC
   CURSOR c_solic_telev_insip
   IS
      SELECT   subpedido_id, solicitud_id
        FROM   FNX_SOLICITUDES
       WHERE       pedido_id = w_pedido
               AND servicio_id = 'ENTRE'
               AND producto_id = 'TELEV'
               AND tipo_elemento_id = 'INSIP'
               AND tipo_elemento = 'CMP'
               AND estado_id = 'TECNI'
               AND concepto_id = 'PETEC';

   -- Cursor para obtener los productos contenidos en el pedido
   CURSOR c_product_pedido
   IS
      SELECT   DISTINCT producto_id AS producto_id
        FROM   FNX_SOLICITUDES
       WHERE   pedido_id = w_pedido
               AND estado_id IN ('TECNI', 'RUTAS', 'INGRE');

   -- Cursor para buscar estado_id='TECNI' y concepto_id='PREI'
   -- dentro del requerimiento de retiro por pago anticipado

   CURSOR c_solic_estado_conc (p_estado IN FNX_ESTADOS.estado_id%TYPE,
   p_concepto IN FNX_CONCEPTOS.concepto_id%TYPE)
   IS
      SELECT   pedido_id, subpedido_id
        FROM   FNX_SOLICITUDES
       WHERE       pedido_id = w_pedido
               AND estado_id = p_estado
               AND concepto_id = p_concepto;

   vr_solic                   c_solic_estado_conc%ROWTYPE;

   v_tipo_venta_ba FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE
         := NULL ;
   v_tipo_solicitud FNX_REPORTES_INSTALACION.tipo_solicitud%TYPE
         := NULL ;

   -- 2012-03-12 aagudc Cursor para determinar si el pedido posee un componente IDIOMA.
     CURSOR c_cmp_idioma IS
        SELECT COUNT(1)
          FROM fnx_solicitudes
         WHERE pedido_id = w_pedido
            AND producto_id = 'SERALI'
            AND tipo_elemento_id = 'IDIOMA';

   --<<2014-02-27  eaguirrg Iniico
   --2014-09-18   AARBOB
   CURSOR c_pedidos_codigo_familiar
   IS
      SELECT   distinct subpedido_id, solicitud_id,tipo_elemento_id
        FROM   FNX_SOLICITUDES -- 2014-07-18 eaguirrg se elimina producto cartesiano contra cg_ref_codes
       WHERE       pedido_id   = w_pedido
               AND estado_id   <> 'TECNI'
               AND concepto_id <>'PETEC'
               AND municipio_id in ('MEDANTCOL','CARBOLCOL')
               AND tipo_elemento_id in(select distinct rv_low_value
                                       from cg_ref_codes
                                        where RV_HIGH_VALUE is null
                                          and rv_domain in('CODIGO_PERSONAL','CODIGO_PYME','CODIGO_FAMILIAR'));
   -->>2014-02-27  eaguirrg Fin
     -- 2011-12-20 aagudc Variables utilizadas para el producto Vitrina OnLine.
     v_cmp_idioma  NUMBER;
     w_resp_serali VARCHAR2(100);

    --2012-02-16  Jrendbr Variables utilizadas para el producto Verticales.
    w_resp_inttic VARCHAR2(100);

    --2012-10-17  Jrendbr Variables utilizadas para el producto IAAS.
    w_resp_vdatac VARCHAR2(100);

    -- 2013-12-19 jcardega Variable de respuesta estudio técnico SERCOM
    w_resp_SERCOM    VARCHAR2 (200);

    -- 2014-04-09 jcardega Variable de respuesta estudio técnico Seguridad Gestionada (SEGURI).
    w_resp_SEGURI    VARCHAR2 (200);

   -- 2014-03-05.  Implementación de lista del Tipo Associative Array para almacenar los productos que se tienen en el pedido y
   -- acceder más rápido a la consulta
   TYPE typ_aa_producto
   IS
      TABLE OF FNX_PRODUCTOS.producto_id%TYPE
         INDEX BY FNX_PRODUCTOS.producto_id%TYPE;

   vt_productos_pedido         typ_aa_producto;

   -- 2014-04-30.  REQ 36900. GPON Hogares. Variables para manejo de lógica de tecnología propuesta por Producto.
   vaa_producto_tecno         PKG_PROV_UTIL_TIPOS.typ_aa_producto_tecno;
   v_tecno_propuesta_TO       FNX_TECNOLOGIAS.tecnologia_id%TYPE;
   v_tecno_propuesta_TELEV    FNX_TECNOLOGIAS.tecnologia_id%TYPE;
   v_tecno_propuesta_INTER    FNX_TECNOLOGIAS.tecnologia_id%TYPE;

   -- 2014-05-19  REQ 36900. GPON Hogares.  Variables para manejo de respuestas de Estudio Técnico de Televisión y Telefonía GPON
   v_resp_prov_TO_gpon        VARCHAR2(200);
   v_resp_prov_TELEV_gpon     VARCHAR2(200);

BEGIN

   DBMS_OUTPUT.Put_Line ('<ET-CONTING> ----------- Inicio ------------------------');

   -- 2012-01-17 SECCIÓN. Creación de solicitudes para MOVLTE.

   -- Se mueve bloque para poblar tabla en memoria ya que antes de
   -- entrar al procedimiento de CYR se requiere validar si para el producto
   -- MOVLTE se requieren crear solicitudes.

   -- Poblar tabla PLSQL de productos del pedido
   -- 2014-03-03.  Poblar la tabla VT_PRODUCTOS_PEDIDO
   FOR reg IN c_product_pedido
   LOOP
      vt_productos_pedido(REG.producto_id)  := REG.producto_id;
   END LOOP;

   -- 2014-04-30. REQ 36900. GPON Hogares.
   -- Sección Definición de tecnología propuesta  para los productos TO, TELEV e INTER
   -- Invocar procedimiento para definir tecnología propuesta de Provisión.
   v_tecno_propuesta_TO    := 'XDEF';
   v_tecno_propuesta_TELEV := 'XDEF';
   v_tecno_propuesta_INTER := 'XDEF';
   IF vt_productos_pedido.EXISTS('TO') OR vt_productos_pedido.EXISTS('TELEV') OR
      vt_productos_pedido.EXISTS('INTER')
   THEN
      -- Invocar procedimiento para determinar las tecnologías propuestas para el pedido
      PR_PROV_TECNO_PROPUESTA ( w_pedido, vaa_producto_tecno);

      -- Evaluar la lista vaa_producto_tecno para determinar la tecnología propuesta para cada producto.
      IF vaa_producto_tecno.EXISTS('TO') THEN
        v_tecno_propuesta_TO  := vaa_producto_tecno('TO');
      END IF;

      IF vaa_producto_tecno.EXISTS('TELEV') THEN
         v_tecno_propuesta_TELEV := vaa_producto_tecno('TELEV');
      END IF;

      IF vaa_producto_tecno.EXISTS('INTER') THEN
         v_tecno_propuesta_INTER := vaa_producto_tecno('INTER');
      END IF;

   END IF;

   -- Invocación de procedimiento de creación de solicitudes MOVLTE.
   -- 2012-03-28
   --Se adiciona producto HDDVIR ya que puede crear solicitudes de MOVLTE
   --Se adiciona producto SERALI ya que puede crera solicitudes de MOVLTE
   IF vt_productos_pedido.EXISTS ('MOVLTE') OR vt_productos_pedido.EXISTS ('MOVLTR')
      OR vt_productos_pedido.EXISTS ('HDDVIR') OR vt_productos_pedido.EXISTS ('SERALI')
   THEN
      DBMS_OUTPUT.put_line ('<ET> PR_CREAR_SOLIC_MOVLTE');
      pr_crear_solic_movlte (w_pedido, w_resp_crear_solic, v_carga_prod);
      DBMS_OUTPUT.put_line ('<ET> w_resp_crear_solic ' || w_resp_crear_solic);

      -- 2014-03-05. Jrincong
      -- Se cambia la asignación de la lista vt_tablas_productos por la asignación en la lista vt_productos_pedido
      -- Antes era:  v_tabla_indice := NVL (v_tabla_indice, 0) + 1; ||  vt_tabla_productos (v_tabla_indice) := v_carga_prod;

      --Se valida si creó solicitud de MOVLTE para adicionar a lista en memoria.
      IF v_carga_prod IS NOT NULL THEN
         vt_productos_pedido (v_carga_prod) := v_carga_prod;
      END IF;
   END IF;
   -- FIN SECCIÓN. Creación de solicitudes para MOVLTE.

   --2013-02-22: Req21213: Inicio Se adiciona el producto Fijo LTE para que sea procesado por el procedimiento correspondiente
   IF vt_productos_pedido.EXISTS ('FIJLTE') THEN
      DBMS_OUTPUT.put_line ('<ET> PR_CREAR_SOLIC_FIJLTE');
      pr_crear_solic_fijlte (w_pedido, w_resp_crear_solic, v_carga_prod);
      DBMS_OUTPUT.put_line ('<ET> w_resp_crear_solic ' || w_resp_crear_solic);

      -- 2014-03-05. Jrincong
      -- Se cambia la asignación de la lista vt_tablas_producto por la asignación en la lista vt_productos_pedido
      -- Antes era:  v_tabla_indice := NVL (v_tabla_indice, 0) + 1; ||  vt_tabla_productos (v_tabla_indice) := v_carga_prod;

      --Se valida si creó solicitud de FIJLTE para adicionar a lista en memoria.
      IF v_carga_prod IS NOT NULL THEN
         vt_productos_pedido (v_carga_prod) := v_carga_prod;
      END IF;
   END IF;

   -- Jrendbr 20131025
   -- Invocar procedimiento para transacciones de SMPRO.
   IF vt_productos_pedido.EXISTS ('TO') OR
      vt_productos_pedido.EXISTS ('TELEV') OR
      vt_productos_pedido.EXISTS ('INTER') THEN
       DBMS_OUTPUT.put_line ('<ET> PR_ESTUDIO_TECNICO_SMPRO');
       PR_ESTUDIO_TECNICO_SMPRO (w_pedido, w_resp_cambio_estado);
   END IF;

   --2013-02-22: Req21213: Fin Se adiciona el producto Fijo LTE para que sea procesado por el procedimiento correspondiente

   -- 2009-12-14 Truizd
   -- Invocar procedimiento para transacciones de Corte y Reconexiòn.
   DBMS_OUTPUT.put_line ('<ET> entra PR_ENRUTAR_CORTEYRECON');
   PR_ENRUTAR_CORTEYRECON (w_pedido, w_resp_cambio_estado);
   DBMS_OUTPUT.put_line ('<ET> sale PR_ENRUTAR_CORTEYRECON');

   -- 2012-07-05
   -- Invocacion de estudio técnico de producto INTTIC despues del llamado a Corte y reconexión.
   -- 2012-02-16
   -- Invocacion de estudio técnico de producto INTTIC
   IF vt_productos_pedido.EXISTS ('INTTIC')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_VERTICAL');
      pr_estudio_tecnico_vertical(w_pedido, w_resp_inttic);
   END IF;

   -- 2009-01-23
   -- Uso de parámetro para desactivar estudio técnico de Contingencia.
   w_parametro := 700;
   w_resp_desactivar_et :=
      NVL (FN_valor_parametros ('PRV_SERV', w_parametro), 'N');

   IF w_resp_desactivar_et = 'S'
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> Estudio Técnico Desactivado por parámetro');
      RAISE e_et_no_ejecutado;
   END IF;

   DBMS_OUTPUT.Put_Line ('<ET> ----------- Enrutamientos ----------------');
   /* Inicializacion de variables */

   w_resp_enrutar_pventa := 'S';
   w_resp_enrutar_rdsi := 'S';
   w_resp_enrutar_3play := 'S';
   w_resp_enrutar := 'S';
   w_resp_enrutar_tvemp := 'S';
   w_resp_enrutar_lantla := 'S';
   w_resp_enrutar_cntxip := 'S';
   w_resp_enrutar_paquete := 'S';
   w_resp_cmpaseg := 'S';
   w_resp_cmp_internet := 'S';
   w_resp_internet := 'S';
   w_resp_clicktodial := 'S';
   w_resp_comunidad_cerrada := 'S';
   w_resp_asp := 'S';
   w_resp_ctaper := 'S';
   w_resp_intemp := 'S';
   w_resp_telev := 'S';
   w_resp_tvemp := 'S';
   w_resp_lantla := 'S';
   w_resp_cntxip := 'S';
   w_resp_comuni := 'S';
   w_resp_conect := 'S';
   w_resp_ldirec := 'S';
   w_resp_manrem := 'S';
   w_resp_3play := 'S';
   w_resp_movilidad := 'S';
   w_resp_desintegrar_pdto := 'S';
   w_resp_multinet := 'S';
   w_resp_tlbvir := 'S';
   w_resp_to := 'S';
   w_resp_telef_ip := 'S';
   w_resp_tsip := 'S';
   w_resp_et_hfctsip := 'S';
   w_resp_et_redcotsip := 'S';

   w_resp_pbx := 'S';
   w_resp_beeper := 'S';
   w_resp_tmovil := 'S';
   w_resp_trk := 'S';
   w_resp_enlaces_lan := 'S';
   w_resp_srvpro := 'S';
   w_resp_bdyn := 'S';
   w_resp_idc := 'S';
   w_resp_largadist := 'S';
   w_resp_redint := 'S';
   w_resp_segur := 'S';
   w_resp_internet_kids := 'S';
   w_resp_internet_play := 'S';
   w_resp_toip := 'S';
   w_resp_vxpga := 'S';
   w_resp_vxprei := 'S';
   --2012-03-13 Se cambia a 'S' el valor de w_resp_precal para estandarizar.
   w_resp_precal := 'S';
   w_resp_intmov := 'S';
   w_resp_et_reuso := 'S';
   w_resp_lte := 'S';
   w_resp_apn := 'S';
   --2012-03-13 Se asigna el valor de 'S' como valor inicial.
   w_resp_accesos_trksip := 'S';
   w_resp_inttic := 'S';
   --2012-10-17 Se asigna el valor de 'S' como valor inicial.
   w_resp_vdatac := 'S';
   -- 2012-10-31 aagudc Se asigna el valor de 'S' como valor inicial..
   w_resp_microtic := 'S';
   -- 2013-02-12 dojedac Se asigna el valor de 'S' como valor inicial..
   w_resp_voznub := 'S';
   -- 2013-02-28 Gsantos Retiro 3066
   w_resp_ret3066 := 'S';
   -- Fin 2013-02-28 Gsantos Retiro 3066
   -- 2013-08-08 Eaguirrg
   w_resp_serequ := 'S';
   -- 2013-12-19 jcardega Se asigna el valor de 'S' como valor inicial..
   w_resp_SERCOM := 'S';
   --2014-03-27 lhernans Se inicializa la variable
   w_resp_modequ := 'S';


   -- 2014-04-09 jcardega Se asigna el valor de 'S' como valor inicial..
   w_resp_SEGURI := 'S';

   /*
      Es necesario que las rutinas de enrutamiento de equipos queden antes de las rutinas
      de estudio tecnico, ya que si se invierte el orden se presentaran problemas con los
      productos empaquetados como 3Play.
      Paulo Mosquera 2003/07/02
   */

  -- 2013-02-28 Gsantos Retiro 3066
   v_estudio_tec := 'S';
   v_retiro_3066 := 0;
   OPEN c_caract_7(w_pedido);
   FETCH c_caract_7 INTO v_retiro_3066;
   CLOSE c_caract_7;
   IF v_retiro_3066 > 0 THEN
      OPEN c_solic_estado_conc ('TECNI', 'PXREI');
      FETCH c_solic_estado_conc INTO   vr_solic;
      IF c_solic_estado_conc%NOTFOUND
      THEN
         DBMS_OUTPUT.put_line ('<ET> PR_ET_VALIDAR_RETIRO_3066');
         w_resp_ret3066 := '';
         v_estudio_tec := 'S';
         pr_et_validar_retiro_3066 (w_pedido, v_estudio_tec, w_resp_ret3066);
         -- 2013-04-22   Gsantos
         --IF v_estudio_tec = 'N' THEN
         --   RAISE e_et_no_ejecutado;
         --END IF;
         -- Fin 2013-04-22   Gsantos
      END IF;
      CLOSE c_solic_estado_conc;
   END IF;
   -- Fin 2013-02-28 Gsantos Retiro 3066

   -- 2008-12-21
   -- Invocar procedimiento para verificar solicitudes de retiro de TO, TELEV e INTERNET,
   -- con plan de facturacion anticipada.
   IF    vt_productos_pedido.EXISTS ('TO')
      OR vt_productos_pedido.EXISTS ('INTER')
      OR vt_productos_pedido.EXISTS ('TELEV')
      OR vt_productos_pedido.EXISTS ('TV')
   THEN
      DBMS_OUTPUT.put_line ('<ET> PR_ET_VALIDAR_PAGO_ANTICIPADO');
      pr_et_validar_pago_anticipado (w_pedido, w_resp_vxpga);
   END IF;

   -- 2009-03-13
   -- Verificar sí existe una solicitud en concepto PXREI.  Pendiente Retiro Inmediato
   IF    vt_productos_pedido.EXISTS ('TO')
      OR vt_productos_pedido.EXISTS ('INTER')
      OR vt_productos_pedido.EXISTS ('TELEV')
   THEN
      OPEN c_solic_estado_conc ('TECNI', 'PXREI');

      FETCH c_solic_estado_conc INTO   vr_solic;

      IF c_solic_estado_conc%NOTFOUND
      THEN
         DBMS_OUTPUT.Put_Line (
            '<ET> El pedido no tiene solicitudes en TECNI- PXREI'
         );
      ELSE
         pr_genord_retir_servicio (w_pedido, 'TECNI', 'PXREI', w_resp_vxprei);

         --2012-03-13
         --Ajuste para mensaje de error. Cuando viene nulo, se hace igual a S.
         IF w_resp_vxprei IS NULL
         THEN
            w_resp_vxprei := 'S';
         END IF;
      END IF;

      CLOSE c_solic_estado_conc;
   END IF;

   -- 2009-02-12  Wmendozal/Aacosta  Invocación procedimiento para manejar el tema de precalificación de
   --                                pares para el producto IPTV.
   -- 2010-08-24 Se comenta esta lógica mientras se define nuevamente el tema de precalificación
   /*   IF vt_productos_pedido.EXISTS ('TELEV')
      THEN
           dbms_output.put_line('PRECALIFICACION ');
         PR_PRECALIFICACION_NODOS (w_pedido, w_resp_precal );
      END IF;*/

   IF vt_productos_pedido.EXISTS ('3PLAY')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_DESINTEGRAR_PRODUCTO');
      pr_desintegrar_producto (w_pedido, w_resp_desintegrar_pdto);
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_PVENTA');
      pr_enrutar_pventa (w_pedido, w_resp_enrutar_pventa);
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_3PLAY');
      pr_enrutar_3play (w_pedido, w_resp_enrutar_3play);
   END IF;

   -- 2009-02-12 Jgomezve  Se agrega la validación vt_productos_pedido.EXISTS ('TO')
   --                      para que el producto TO sean consideradas las transacciones de Cambios de Equipo
   --                      para el producto de TOIP que se presenta en Cableras.
    --2013-09-18 Srojaslo   Se comentan las líneas que incluían al producto COMUNI, pues para este no aplica
    --                      el procedimiento PR_ENRUTAR_EQ_RDSI

   IF    vt_productos_pedido.EXISTS ('RDSIB')
      OR vt_productos_pedido.EXISTS ('TRDSI')
      OR vt_productos_pedido.EXISTS ('RDSIP')
      OR vt_productos_pedido.EXISTS ('MULT')
      OR vt_productos_pedido.EXISTS ('LANTLA')
      OR vt_productos_pedido.EXISTS ('CNTXIP')
      OR vt_productos_pedido.EXISTS ('INTER')
      OR vt_productos_pedido.EXISTS ('MOVIL')
      OR vt_productos_pedido.EXISTS ('TELEV')
      OR vt_productos_pedido.EXISTS ('TO')
      --2013-09-18 Srojaslo
      -- OR vt_productos_pedido.EXISTS ('COMUNI')
      OR vt_productos_pedido.EXISTS ('MANOS')
      OR vt_productos_pedido.EXISTS ('CONECT')
      OR vt_productos_pedido.EXISTS ('LDIREC')
   THEN
      -- 2013-04-22   Gsantos
      IF (vt_productos_pedido.EXISTS ('RDSIB') OR
         vt_productos_pedido.EXISTS ('RDSIP') OR
         vt_productos_pedido.EXISTS ('INTER') OR
         vt_productos_pedido.EXISTS ('TO'))   AND
         v_estudio_tec = 'S' THEN
            DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_EQ_RDSI');
            pr_enrutar_eq_rdsi (w_pedido, w_resp_enrutar_rdsi);
      ELSIF (vt_productos_pedido.EXISTS ('TRDSI')  OR
            vt_productos_pedido.EXISTS ('MULT')   OR
            vt_productos_pedido.EXISTS ('LANTLA') OR
            vt_productos_pedido.EXISTS ('CNTXIP') OR
            vt_productos_pedido.EXISTS ('MOVIL')  OR
            vt_productos_pedido.EXISTS ('TELEV')  OR
            --2013-09-18 Srojaslo
            -- vt_productos_pedido.EXISTS ('COMUNI') OR
            vt_productos_pedido.EXISTS ('MANOS')  OR
            vt_productos_pedido.EXISTS ('CONECT') OR
            vt_productos_pedido.EXISTS ('LDIREC')) THEN
         DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_EQ_RDSI');
         pr_enrutar_eq_rdsi (w_pedido, w_resp_enrutar_rdsi);
      END IF;
      -- Fin 2013-04-22   Gsantos
   END IF;

   IF vt_productos_pedido.EXISTS ('TELEV')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_TELEV');
      pr_enrutar_telev (w_pedido, w_resp_telev);
   END IF;

   IF vt_productos_pedido.EXISTS ('TV')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_TVEMP');
      pr_enrutar_tvemp (w_pedido, w_resp_enrutar_tvemp);
   END IF;

   IF vt_productos_pedido.EXISTS ('LANTLA')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_LANTLA');
      pr_enrutar_lantla (w_pedido, w_resp_enrutar_lantla);
   END IF;


   IF vt_productos_pedido.EXISTS ('CNTXIP')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_CNTXIP');
      pr_enrutar_cntxip (w_pedido, w_resp_enrutar_cntxip);
   END IF;

   -- 2009-03-13  Aacosta  Adición del nuevo procedimiento de enrutamiento de componentes
   --                      para el producto TRONCALES SIP.
   IF vt_productos_pedido.EXISTS ('TRKSIP')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_CMP_TSIP');

      OPEN c_solic_srv;

      FETCH c_solic_srv INTO   v_soli_srv_trk;

      CLOSE c_solic_srv;

      IF v_soli_srv_trk = 0
      THEN
         b_provi_trksip := TRUE;
      ELSE
         b_provi_trksip := FALSE;
      END IF;

      PR_ENRUTAR_CMP_TSIP (w_pedido, 'LOGIC', w_resp_tsip, b_provi_trksip);

      --2012-03-13
      --Ajuste para mensaje de error. Cuando viene nulo, se hace igual a S.
      IF w_resp_tsip IS NULL
      THEN
         w_resp_tsip := 'S';
      END IF;
   END IF;

   -- 2013-04-08   Gsantos
   IF v_estudio_tec = 'S' THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_PAQUETE');
      pr_enrutar_paquete (w_pedido, w_resp_enrutar_paquete);
   END IF;
   -- Fin 2013-04-08   Gsantos

   /* Procedimientos que controlan el proceso de Scoring                            */
   /* y cambio de estados los contratos de Internet y television de 3Play  */

   IF    vt_productos_pedido.EXISTS ('MOVIL')
      OR vt_productos_pedido.EXISTS ('3PLAY')
      OR vt_productos_pedido.EXISTS ('LANTLA')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_SCORING');
      pr_scoring (w_pedido);
   END IF;

   IF vt_productos_pedido.EXISTS ('3PLAY')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_CONTROLAR_3PLAY');
      pr_controlar_3play (w_pedido);
   END IF;

   DBMS_OUTPUT.Put_Line ('<ET> ----------- Estudios Técnicos ------------');

   /* ESTUDIOS TECNICOS */
   IF vt_productos_pedido.EXISTS ('TO')
   THEN
      -- 2013-04-22   Gsantos
      IF v_estudio_tec = 'S' THEN

          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TO');
          pr_estudio_tecnico_to (w_pedido, w_resp_to);

          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TELEF_IP');
          pr_estudio_tecnico_telef_ip (w_pedido, 'PETEC', w_resp_telef_ip);

          IF w_resp_telef_ip = 'AA'
          THEN
             w_resp_telef_ip := 'S';
          END IF;

          -- 2008-03-05
          -- Invocación Estudio Técnico BAREDCO para verificar sí puede estudiarse la solicitud
          -- de TOIP por un acceso BAREDCO existente.

          -- 2014-04-30. REQ 36900.  GPON Hogares.
          -- Adición de condición v_tecno_propuesta_TO <> 'GPON' para invocar proc. PR_ESTUDIO_BAREDCO_TOIP sólo sí la tecnología
          -- de propuesta Provisión es diferente de GPON
          IF v_tecno_propuesta_TO <> 'GPON' THEN
             FOR solic IN c_solic_toip
             LOOP
                 PR_estudio_baredco_toip (w_pedido, solic.subpedido_id,
                 solic.solicitud_id, 'PETEC', FALSE, w_resp_et_bartoip);
                 DBMS_OUTPUT.put_line (
                    '<ET> BARECO_TOIP <' || w_resp_et_bartoip || '>'
                 );
             END LOOP;
          END IF;

          -- 2008-04-16
          -- Invocar Estudio Técnico Banda Ancha HFC .
          -- 2014-04-30. REQ 36900.  GPON Hogares.
          -- Adición de condición v_tecno_propuesta_TO <> 'GPON' para invocar proc. PR_ESTUDIO_TECNICO_BAHFC sólo sí la tecnología
          -- de propuesta Provisión es diferente de GPON
          IF v_tecno_propuesta_TO <> 'GPON' THEN

             DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_BAHFC');
             FOR solic IN c_solic_toip
             LOOP
                PR_estudio_tecnico_bahfc (w_pedido, solic.subpedido_id,
                 solic.solicitud_id, 'PETEC', FALSE, w_resp_et_bahfc);

                DBMS_OUTPUT.put_line ('<ET> BAHFC_TOIP <' || w_resp_et_bahfc || '>');
             END LOOP;

          END IF;

          -- 2014-04-30. REQ 36900.  GPON Hogares.
          -- Invocar proc. PR_ESTUDIO_TECNICO_TO_GPON para provisión de accesos de TOIP cuya tecnología propuesta sea GPON
          IF v_tecno_propuesta_TO = 'GPON' THEN

             -- 2014-05-19 Asignación de valor 'S' para variables de respuesta PR_ESTUDIO_TECNICO_TO_GPON
             v_resp_prov_TO_gpon  := 'S';

             DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TO_GPON');
             FOR solic IN c_solic_toip
             LOOP
                PR_ESTUDIO_TECNICO_TO_GPON (
                      w_pedido, solic.subpedido_id, solic.solicitud_id,
                      'PETEC', TRUE, v_resp_prov_TO_gpon );
             END LOOP;
          END IF;

      END IF;
      -- Fin 2013-04-22   Gsantos

   END IF;

 -- 2012-10-17
   -- Invocacion de estudio técnico de producto IAAS
   IF vt_productos_pedido.EXISTS ('IAAS')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_VDATAC');
      pr_estudio_tecnico_vdatac(w_pedido, w_resp_vdatac);
   END IF;


   IF vt_productos_pedido.EXISTS ('TELEV')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TELEV');
      pr_estudio_tecnico_telev (w_pedido, w_resp_telev);

      -- 2014-04-30. REQ 36900.  GPON Hogares.
      -- Adición de condición v_tecno_propuesta_TELEV <> 'GPON' para invocar proc. PR_ESTUDIO_TECNICO_TVADSL sólo sí la tecnología
      -- de propuesta Provisión es diferente de GPON
      IF v_tecno_propuesta_TELEV <> 'GPON' THEN

         -- 2008-07-16
         -- Invocación de estudio técnico de televisión ADSL
         DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TVADSL');

         FOR solic IN c_solic_telev_insip
         LOOP
            PR_estudio_tecnico_tvadsl (w_pedido, solic.subpedido_id,
             solic.solicitud_id, 'PETEC', w_resp_et_tvadsl);

            DBMS_OUTPUT.put_line (
               '<ET> TVADSL INSIP <' || w_resp_et_tvadsl || '>'
            );
         END LOOP;
      END IF;

      -- 2014-04-30. REQ 36900.  GPON Hogares.
      -- Invocar proc. PR_ESTUDIO_TECNICO_TELEV_GPON para provisión de accesos de INSIP cuya tecnología propuesta sea GPON
      IF v_tecno_propuesta_TELEV = 'GPON' THEN

          -- 2014-05-19 Asignación de valor 'S' para variables de respuesta PR_ESTUDIO_TECNICO_TELEV_GPON
          v_resp_prov_TELEV_gpon := 'S';

          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TELEV_GPON');
          FOR solic IN c_solic_telev_insip
          LOOP
             PR_ESTUDIO_TECNICO_TELEV_GPON (
                   w_pedido, solic.subpedido_id, solic.solicitud_id,
                   'PETEC', TRUE, v_resp_prov_TELEV_gpon );
          END LOOP;
      END IF;

   END IF;

   -- 2009-08-25  Aacosta  Se incluye el nuevo producto internet Móvil.
   IF vt_productos_pedido.EXISTS ('INTMOV')
   THEN
      PR_ESTUDIO_TECNICO_INTMOV (w_pedido, w_resp_intmov);
   END IF;

   --2013-08-08   Gsantos
   -- 2009-08-27  Aacosta  Se incluye el nuevo producto USRPOR.
   /*
   IF vt_productos_pedido.EXISTS ('USRPOR')
   THEN
      PR_ESTUDIO_TECNICO_INTMOV (w_pedido, w_resp_intmov);
   END IF;
   */
   -- Fin 2013-08-08   Gsantos

   -- 2007-02-16
   -- Invocacion de estudio tecnico de Accesos Nacionales para realizar estudio
   -- del acceso, antes de los componentes Internet.
   IF vt_productos_pedido.EXISTS ('INTER')
   THEN
      -- 2013-04-22   Gsantos
      IF v_estudio_tec = 'S' THEN
          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_ACCESOS_NLES');

          pr_estudio_accesos_nles (w_pedido, w_resp_accesos_internet);

          IF w_resp_accesos_internet IS NULL
          THEN
             w_resp_accesos_internet := 'S';
          END IF;
      END IF;
      -- Fin 2013-04-22   Gsantos
   END IF;

   -- 2009-03-13
   -- Invocacion de estudio tecnico de troncales sip para realizar estudio
   -- del servicio y las sedes asociadas, antes de los componentes adicionales.
   IF vt_productos_pedido.EXISTS ('TRKSIP')
   THEN
      FOR solic IN c_solic_acceso_tsip_hfc
      LOOP
         pr_estudio_tecnico_tsiphfc (w_pedido, solic.subpedido_id,
         solic.solicitud_id, 'PETEC', w_resp_et_hfctsip);
      END LOOP;

      FOR solic IN c_solic_acceso_tsip_redco
      LOOP
         PR_estudio_tecnico_redcotsip (w_pedido, solic.subpedido_id,
         solic.solicitud_id, 'PETEC', w_resp_et_redcotsip);
      END LOOP;

      PR_ESTUDIO_TECNICO_TSIP (w_pedido, w_resp_accesos_trksip);

      IF w_resp_accesos_trksip IS NULL
      THEN
         w_resp_accesos_trksip := 'S';
      END IF;
   END IF;

   --2013-05-17 lhernans
   --Invocar procedimiento para verificar asignación de Banda Ancha por GPON.

   -- 2014-04-30. REQ 36900.  Adición de condición v_tecno_propuesta_INTER = 'GPON' para invocar proc. PR_ESTUDIO_TECNICO_PROV_GPON
   IF vt_productos_pedido.EXISTS('INTER') AND w_resp_accesos_internet = 'S' AND
      v_tecno_propuesta_INTER = 'GPON'
   THEN

      --Apertura de cursor con datos del acceso
      FOR solic IN c_solic_acceso
      LOOP
         PR_ESTUDIO_TECNICO_PROV_GPON (
               w_pedido,
               solic.subpedido_id,
               solic.solicitud_id,
               'PETEC',
               TRUE,
               w_resp_prov_gpon
               );
      END LOOP;

   END IF;

   -- 2014-04-30. REQ 36900.  Adición de condición v_tecno_propuesta_INTER <> 'GPON' para evitar invocar procedimientos
   -- de asignación automática BAHFC y BAREDCO cuando la tecnología propuesta es GPON.

   -- Invocar procedimiento para verificar asignación de Banda Ancha HFC / ADSL
   IF vt_productos_pedido.EXISTS ('INTER') AND w_resp_accesos_internet = 'S' AND
      v_tecno_propuesta_INTER <> 'GPON'
   THEN
      -- 2013-04-22   Gsantos
      IF v_estudio_tec = 'S' THEN

          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_BAHFC');

          FOR solic IN c_solic_acceso
          LOOP
             -- 2009-09-01  Aacosta  Validación asociada que sólo se debe provisionar para el producto Internet por demanda
             --                      por tecnología REDCO.

             -- 2009-09-25
             -- Se cambia la validación de la bùsqueda de tecnología a la bùsqueda del tipo de Venta
             -- No se realiza para el tipo de venta DEM ( x Demanda )
             v_tipo_venta_ba :=
                FN_VALOR_CARACT_SUBPEDIDO (w_pedido, solic.subpedido_id, 1339);

             IF NVL (v_tipo_venta_ba, 'N') <> 'DEM'
             THEN
                PR_estudio_tecnico_bahfc (w_pedido, solic.subpedido_id,
                solic.solicitud_id, 'PETEC', FALSE, w_resp_et_bahfc);
             -- 2009-11-09  Aacosta  Se valida que cuando es una cambio de banda ancha fijo a por demanda ejecute los procesos de
             --                      asignación automática.
             ELSE
                v_tipo_solicitud :=
                   PKG_SOLICITUDES.fn_tipo_solicitud (w_pedido,
                   solic.subpedido_id, solic.solicitud_id);

                IF NVL (v_tipo_solicitud, 'N') <> 'NUEVO'
                THEN
                   PR_estudio_tecnico_bahfc (w_pedido, solic.subpedido_id,
                   solic.solicitud_id, 'PETEC', FALSE, w_resp_et_bahfc);
                END IF;
             END IF;

             DBMS_OUTPUT.put_line (
                '<ET> BAHFC ACCESP <' || w_resp_et_bahfc || '>'
             );
          END LOOP;

          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_BAREDCO');

          FOR solic IN c_solic_acceso
          LOOP
             -- 2009-09-10  Aacosta
             -- Se valida que si el tipo de bancha no es por demanda se puede hacer estudio de asignacion automática.

             v_tipo_venta_ba :=
                FN_VALOR_CARACT_SUBPEDIDO (w_pedido, solic.subpedido_id, 1339);

             IF NVL (v_tipo_venta_ba, 'N') <> 'DEM'
             THEN
                PR_estudio_tecnico_baredco (w_pedido, solic.subpedido_id,
                solic.solicitud_id, 'PETEC', FALSE, w_resp_et_iadsl);
             -- 2009-11-09  Aacosta  Se valida que cuando es una cambio de banda ancha fijo a por demanda ejecute los procesos de
             --                      asignación automática.
             ELSE
                v_tipo_solicitud :=
                   PKG_SOLICITUDES.fn_tipo_solicitud (w_pedido,
                   solic.subpedido_id, solic.solicitud_id);

                IF NVL (v_tipo_solicitud, 'N') <> 'NUEVO'
                THEN
                   PR_estudio_tecnico_baredco (w_pedido, solic.subpedido_id,
                   solic.solicitud_id, 'PETEC', FALSE, w_resp_et_iadsl);
                END IF;
             END IF;

             DBMS_OUTPUT.put_line (
                '<ET> IADSL ACCESP <' || w_resp_et_iadsl || '>'
             );
          END LOOP;

          -- 2009-09-24 Aacosta  Invocación de nuevo procedimiento para el reuso de la red para pedidos de Banda Ancha por demanda.
          FOR solic IN c_solic_acceso
          LOOP
             v_tipo_venta_ba :=
                FN_VALOR_CARACT_SUBPEDIDO (w_pedido, solic.subpedido_id, 1339);
             v_propiedad_equipo :=
                FN_VALOR_CARACT_SUBPEDIDO (w_pedido, solic.subpedido_id, 1093);
             v_departamento :=
                FN_VALOR_CARACT_SUBPEDIDO (w_pedido, solic.subpedido_id, 204);

             -- 2009-09-28  Aacosta  Se adiciona condición para validar que si el BA por demanda es en COMODATO (CD) se debe
             --                      hacer reuso de la red de la televisión.
             IF NVL (v_tipo_venta_ba, 'N') = 'DEM'
                AND NVL (v_propiedad_equipo, 'N') = 'CD'
             THEN
                pr_copiar_red_acceso_dem (w_pedido, solic.subpedido_id,
                solic.solicitud_id, w_resp_et_reuso);
             END IF;
          END LOOP;

      END IF;
      -- Fin 2013-04-22   Gsantos

   END IF;

   IF    vt_productos_pedido.EXISTS ('INTER')
      OR vt_productos_pedido.EXISTS ('3PLAY')
      OR vt_productos_pedido.EXISTS ('LANTLA')
   THEN
      -- 2013-04-22   Gsantos
      IF v_estudio_tec = 'S' THEN
          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_INTERNET');
          pr_estudio_tecnico_internet (w_pedido, w_resp_internet);
      END IF;
      -- Fin 2013-04-22   Gsantos
   END IF;

   IF vt_productos_pedido.EXISTS ('IPLAY')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_INTERNET_IPLAY');
      pr_estudio_internet_iplay (w_pedido, w_resp_internet_play);
   END IF;

   IF vt_productos_pedido.EXISTS ('KIDS')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_INTERNET_KIDS');
      pr_estudio_internet_kids (w_pedido, w_resp_internet_kids);
   END IF;

   IF vt_productos_pedido.EXISTS ('TOIP')
   THEN
      -- 2013-04-22   Gsantos
      IF v_estudio_tec = 'S' THEN
          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TOIP');

          -- pr_estudio_tecnico_toip (w_pedido, w_resp_toip);

          IF w_resp_toip = 'AA'
          THEN
             w_resp_toip := 'S';
          END IF;
      END IF;
      -- Fin 2013-04-22   Gsantos
   END IF;

   IF vt_productos_pedido.EXISTS ('INTEMP')
   THEN
      -- 2013-04-22   Gsantos
      IF v_estudio_tec = 'S' THEN
          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_INTEMP');
          pr_estudio_tecnico_intemp (w_pedido, w_resp_intemp);
      END IF;
      -- Fin 2013-04-22   Gsantos
   END IF;

   IF vt_productos_pedido.EXISTS ('LANTLA')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_CMP_INTERNET');
      pr_estudio_cmp_internet (w_pedido, w_resp_cmp_internet);
   END IF;

   IF vt_productos_pedido.EXISTS ('ASP')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_ASP');
      pr_estudio_tecnico_asp (w_pedido, w_resp_asp);
   END IF;

   IF vt_productos_pedido.EXISTS ('CTAPER')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_CTAPER');
      pr_estudio_cuenta_personal (w_pedido, w_resp_ctaper);
   END IF;

   IF vt_productos_pedido.EXISTS ('CLICTD')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_CLICKTODIAL');
      pr_estudio_tecnico_clicktodial (w_pedido, w_resp_clicktodial);
   END IF;

   IF vt_productos_pedido.EXISTS ('COMUN')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_COMUNIDAD_CERRADA');
      pr_estudio_comunidad_cerrada (w_pedido, w_resp_comunidad_cerrada);
   END IF;

   IF vt_productos_pedido.EXISTS ('TV')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TVEMP');
      pr_estudio_tecnico_tvemp (w_pedido, w_resp_tvemp);
   END IF;

   IF vt_productos_pedido.EXISTS ('LANTLA')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_ENLACES_LAN');
      pr_estudio_enlaces_lan (w_pedido, w_resp_enlaces_lan);
   END IF;

   IF vt_productos_pedido.EXISTS ('MULT')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_MULTINET');
      pr_estudio_tecnico_multinet (w_pedido, w_resp_multinet);
   END IF;

   IF vt_productos_pedido.EXISTS ('TLBVIR')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TLBVIR');
      pr_estudio_tecnico_tlbvir (w_pedido, w_resp_tlbvir);
   END IF;

   IF vt_productos_pedido.EXISTS ('PBX')
   THEN
      -- 2013-04-22   Gsantos
      IF v_estudio_tec = 'S' THEN
          DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_PBX');
          pr_estudio_tecnico_pbx (w_pedido, w_resp_pbx);
      END IF;
      -- 2013-04-22   Gsantos
   END IF;

   IF vt_productos_pedido.EXISTS ('BEEP')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_BEEPER');
      pr_estudio_tecnico_beeper (w_pedido, w_resp_beeper);
   END IF;

   IF vt_productos_pedido.EXISTS ('TMOVIL')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TMOVIL');
      pr_estudio_tecnico_tmovil (w_pedido, w_resp_tmovil);
   END IF;

   IF vt_productos_pedido.EXISTS ('TRK')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_TRK');
      pr_estudio_tecnico_trk (w_pedido, w_resp_trk);
   END IF;

   IF vt_productos_pedido.EXISTS ('SRVPRO')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_SERVPROF');
      pr_estudio_tecnico_servprof (w_pedido, w_resp_srvpro);
   END IF;

   IF vt_productos_pedido.EXISTS ('CNTXIP')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_CNTXIP');
      pr_estudio_tecnico_cntxip (w_pedido, w_resp_cntxip);
   END IF;

   IF vt_productos_pedido.EXISTS ('COMUNI')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_COMUNI');
      pr_estudio_tecnico_comuni (w_pedido, w_resp_comuni);
   END IF;

   IF vt_productos_pedido.EXISTS ('CONECT')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_CONECT');
      pr_estudio_tecnico_conect (w_pedido, w_resp_conect);
   END IF;

   IF vt_productos_pedido.EXISTS ('LDIREC')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_LDIREC');
      pr_estudio_tecnico_ldirec (w_pedido, w_resp_ldirec);
   END IF;

   IF vt_productos_pedido.EXISTS ('MANOS')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_MANREM');
      pr_estudio_tecnico_manrem (w_pedido, w_resp_manrem);
   END IF;

   IF vt_productos_pedido.EXISTS ('3PLAY')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_3PLAY');
      pr_estudio_tecnico_3play (w_pedido, w_resp_3play);
   END IF;

   IF vt_productos_pedido.EXISTS ('MOVIL')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_MOVILIDAD');
      pr_estudio_tecnico_movilidad (w_pedido, w_resp_movilidad);
   END IF;

   IF vt_productos_pedido.EXISTS ('BOLSAD')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_BDYN');
      pr_estudio_tecnico_bdyn (w_pedido, w_resp_bdyn);
   END IF;

   IF    vt_productos_pedido.EXISTS ('WEBHOS')
      OR vt_productos_pedido.EXISTS ('HOSTBD')
      OR vt_productos_pedido.EXISTS ('HDDVIR')
      OR vt_productos_pedido.EXISTS ('HOMAIL')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_IDC');
      pr_estudio_tecnico_idc (w_pedido, w_resp_idc);
   END IF;

   IF vt_productos_pedido.EXISTS ('LDIST') OR vt_productos_pedido.EXISTS ('LDISTH')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_LARGADIST');
      pr_estudio_tecnico_largadist (w_pedido, w_resp_largadist);
   END IF;

   -- 2007-05-22   Aacosta
   -- Invocacion de estudio tecnico de Hogar Seguro (PR_ESTUDIO_TECNICO_SEGURIDAD).
   IF vt_productos_pedido.EXISTS ('SEGINT')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_SEGURIDAD');
      pr_estudio_tecnico_seguridad (w_pedido, w_resp_segur);
   END IF;


   -- 2011-01-24   Jrincong
   -- Invocacion de estudio técnico de producto Credito Grupo EPM
   IF vt_productos_pedido.EXISTS ('CGEPM')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_CGEPM');
      PR_ESTUDIO_TECNICO_CGEPM (w_pedido, w_resp_cgepm);
   END IF;

   -- 2011-06-03
   -- Invocacion de estudio técnico de producto Portabilidad
   IF vt_productos_pedido.EXISTS ('PORTAB')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ESTUDIO_TECNICO_PORTAB');
      PR_ESTUDIO_TECNICO_PORTAB (w_pedido, w_resp_portab);
   END IF;

   -- 2011-11-15
   -- Invocacion de estudio técnico de producto aliados
     IF vt_productos_pedido.EXISTS('SERALI') THEN
        -- 2012-03-12 aagudc Se verifica si existe dentro del pedidio el componente IDIOMA de lo contrario se invoca el PKG del SERALI.
        OPEN c_cmp_idioma;
        FETCH c_cmp_idioma
            INTO v_cmp_idioma;
        CLOSE c_cmp_idioma;
        IF v_cmp_idioma > 0 THEN
            dbms_output.put_line('<ET> PR_ESTUDIO_TECNICO_IDIOMAS');
            pr_estudio_tecnico_idiomas(w_pedido,
                                                w_resp_idiomas);
        ELSE
         dbms_output.put_line('<ET> PKG_ESTUDIO_TECNICO_SERALI');
            pkg_estudio_tecnico_serali.pr_enrutar_serali(w_pedido,
                                                                 w_resp_serali);
        END IF;
     END IF;


   --2011-12-02 lhernans LTE. Incovación Estudio Técnico LTE.
   IF vt_productos_pedido.EXISTS ('MOVLTE') OR vt_productos_pedido.EXISTS ('MOVLTR') THEN
      IF NVL (w_resp_crear_solic, 'S') = 'S'
      THEN
         DBMS_OUTPUT.put_line ('<ET> PR_ESTUDIO_TECNICO_LTE');
         pr_estudio_tecnico_lte (w_pedido, w_resp_lte);
      ELSE
         w_resp_lte := w_resp_crear_solic;
      END IF;
   END IF;

   --2013-02-22: Mmedinac: Req21213 Se agrega llamado al procedimiento del estudio tecnico Fijo LTE
   IF  vt_productos_pedido.EXISTS ('FIJLTE')
   THEN
      IF NVL (w_resp_crear_solic, 'S') = 'S'
      THEN
         DBMS_OUTPUT.put_line ('<ET> PR_ESTUDIO_TECNICO_FIJLTE');
         pr_estudio_tecnico_fijlte (w_pedido, w_resp_lte);
      ELSE
         w_resp_lte := w_resp_crear_solic;
      END IF;
   END IF;
   --2013-02-22: Mmedinac: Req21213 Fin Se agrega llamado al procedimiento del estudio tecnico Fijo LTE

   --2012-02-28 LTE. Invocación Estudio Técnico APN.
   IF vt_productos_pedido.EXISTS ('SERAPN')
   THEN
      DBMS_OUTPUT.put_line ('<ET> PR_ESTUDIO_TECNICO_APN');
      pr_estudio_tecnico_apn (w_pedido, w_resp_apn);
   END IF;

   -- 2012-10-31 aagudc Invocar el estudio técnico de Microtic.
   IF vt_productos_pedido.EXISTS ('EQUSER')
   THEN
      DBMS_OUTPUT.put_line ('<ET> PR_ESTUDIO_TECNICO_EQUSER');
      pr_estudio_tecnico_equser (w_pedido, w_resp_microtic);
   END IF;

   -- 2013-02-12 dojedac Invocar el estudio técnico de vopz en la nube.
   IF vt_productos_pedido.EXISTS ('VOZNUB') THEN
      dbms_output.put_line('<ET> PKG_ESTUDIO_TECNICO_VOZNUB');
      pkg_estudio_tecnico_voznub.pr_enrutar_voznub(w_pedido, w_resp_voznub);
   END IF;

   -- 2013-08-08 Eaguirrg
   IF vt_productos_pedido.EXISTS ('SEREQU') THEN
      dbms_output.put_line('<ET> PR_ESTUDIO_TECNICO_SEREQU');
      pr_estudio_tecnico_serequ(w_pedido, w_resp_serequ);
   END IF;

   -- 2013-12-19  jcardega  Invocación de procedimiento para estudio técnico SERCOM
   IF vt_productos_pedido.EXISTS ('SERCOM')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_SERCOM');
      pkg_estudio_tecnico_SERCOM.pi_enrutar_SERCOM (w_pedido, w_resp_SERCOM);
   END IF;

   --2014-03-27 lhernans Invocación de estudio técnico MODEQU
   IF vt_productos_pedido.EXISTS ('MODEQU') THEN
      DBMS_OUTPUT.PUT_LINE ('<ET> PKG_ESTUDIO_TECNICO_MODEQU.pr_enrutar_modequ');
      pkg_estudio_tecnico_modequ.pr_enrutar_modequ (w_pedido, w_resp_modequ);
   END IF;

   -- 2014-04-09  jcardega  Invocación de procedimiento para estudio técnico de Seguridad Gestionada (SEGURI).
   IF vt_productos_pedido.EXISTS ('SEGURI')
   THEN
      DBMS_OUTPUT.Put_Line ('<ET> PR_ENRUTAR_SEGURI');
      pkg_estudio_tecnico_SEGURI.pr_enrutar_SEGURI (w_pedido, w_resp_SEGURI);
   END IF;

   DBMS_OUTPUT.Put_Line ('<ET> ----------- Tercera Sección --------------');

   -- IMPORTANTE
   -- Siempre debe estar de ultimo el proc. PR_ENRUTAR_CMP_ASEGURAMIENTO

   -- 2014-03-05.  Jrincong
   -- El procedimento PR_ENRUTAR_CMP_ASEGURAMIENTO sólo se debe invocar sí dentro del pedido viene el producto PQUETE.
   IF vt_productos_pedido.EXISTS('PQUETE') THEN
      pr_enrutar_cmp_aseguramiento (w_pedido, w_resp_cmpaseg);
   END IF;

   /*
      Ubica la zona geografica en la cual se encuentra el servicio
      The Return of Paulo 2006/06/15  */

   pr_definir_zona (w_pedido);
   w_resp := 'S';

   IF w_resp_internet <> 'S'
   THEN
      w_resp := w_resp_internet;
   ELSIF w_resp_accesos_internet <> 'S'
   THEN
      w_resp := w_resp_accesos_internet;
   ELSIF w_resp_clicktodial <> 'S'
   THEN
      w_resp := w_resp_clicktodial;
   ELSIF w_resp_comunidad_cerrada <> 'S'
   THEN
      w_resp := w_resp_comunidad_cerrada;
   ELSIF w_resp_asp <> 'S'
   THEN
      w_resp := w_resp_asp;
   ELSIF w_resp_ctaper <> 'S'
   THEN
      w_resp := w_resp_ctaper;
   ELSIF w_resp_intemp <> 'S'
   THEN
      w_resp := w_resp_intemp;
   ELSIF w_resp_enrutar_pventa <> 'S'
   THEN
      w_resp := w_resp_enrutar_pventa;
   ELSIF w_resp_enrutar <> 'S'
   THEN
      w_resp := w_resp_enrutar;
   ELSIF w_resp_enrutar_rdsi <> 'S'
   THEN
      w_resp := w_resp_enrutar_rdsi;
   ELSIF w_resp_enrutar_3play <> 'S'
   THEN
      w_resp := w_resp_enrutar_3play;
   ELSIF w_resp_enrutar_tvemp <> 'S'
   THEN
      w_resp := w_resp_enrutar_tvemp;
   ELSIF w_resp_enrutar_lantla <> 'S'
   THEN
      w_resp := w_resp_enrutar_lantla;
   ELSIF w_resp_enrutar_cntxip <> 'S'
   THEN
      w_resp := w_resp_enrutar_cntxip;
   ELSIF w_resp_enrutar_paquete <> 'S'
   THEN
      w_resp := w_resp_enrutar_paquete;
   ELSIF w_resp_bdyn <> 'S'
   THEN
      w_resp := w_resp_bdyn;
   ELSIF w_resp_largadist <> 'S'
   THEN
      w_resp := w_resp_largadist;
   ELSIF w_resp_cmpaseg <> 'S'
   THEN
      w_resp := w_resp_cmpaseg;
   ELSIF w_resp_cmp_internet <> 'S'
   THEN
      w_resp := w_resp_cmp_internet;
   ELSIF w_resp_telev <> 'S'
   THEN
      w_resp := w_resp_telev;
   ELSIF w_resp_tvemp <> 'S'
   THEN
      w_resp := w_resp_tvemp;
   ELSIF w_resp_multinet <> 'S'
   THEN
      w_resp := w_resp_multinet;
   ELSIF w_resp_tlbvir <> 'S'
   THEN
      w_resp := w_resp_tlbvir;
   ELSIF w_resp_to <> 'S'
   THEN
      w_resp := w_resp_to;
   ELSIF w_resp_telef_ip <> 'S'
   THEN
      w_resp := w_resp_telef_ip;
   ELSIF w_resp_pbx <> 'S'
   THEN
      w_resp := w_resp_pbx;
   ELSIF w_resp_beeper <> 'S'
   THEN
      w_resp := w_resp_beeper;
   ELSIF w_resp_tmovil <> 'S'
   THEN
      w_resp := w_resp_tmovil;
   ELSIF w_resp_trk <> 'S'
   THEN
      w_resp := w_resp_trk;
   ELSIF w_resp_enlaces_lan <> 'S'
   THEN
      w_resp := w_resp_enlaces_lan;
   ELSIF w_resp_srvpro <> 'S'
   THEN
      w_resp := w_resp_srvpro;
   ELSIF w_resp_cntxip <> 'S'
   THEN
      w_resp := w_resp_cntxip;
   ELSIF w_resp_comuni <> 'S'
   THEN
      w_resp := w_resp_comuni;
   ELSIF w_resp_conect <> 'S'
   THEN
      w_resp := w_resp_conect;
   ELSIF w_resp_ldirec <> 'S'
   THEN
      w_resp := w_resp_ldirec;
   ELSIF w_resp_manrem <> 'S'
   THEN
      w_resp := w_resp_manrem;
   ELSIF w_resp_3play <> 'S'
   THEN
      w_resp := w_resp_3play;
   ELSIF w_resp_movilidad <> 'S'
   THEN
      w_resp := w_resp_movilidad;
   ELSIF w_resp_desintegrar_pdto <> 'S'
   THEN
      w_resp := w_resp_desintegrar_pdto;
   ELSIF w_resp_idc <> 'S'
   THEN
      w_resp := w_resp_idc;
   ELSIF w_resp_redint <> 'S'
   THEN
      w_resp := w_resp_redint;
   ELSIF w_resp_segur <> 'S'
   THEN
      w_resp := w_resp_segur;
   ELSIF w_resp_internet_kids <> 'S'
   THEN
      w_resp := w_resp_internet_kids;
   ELSIF w_resp_internet_play <> 'S'
   THEN
      w_resp := w_resp_internet_play;
   --2012-03-13. Se cambia condición = 'S' por <> 'S' pues se está
   --perdiendo el mensaje de otros Estudios Técnicos. Cambios en variables:
   -- w_resp_vxpga, w_resp_precal, w_resp_et_hfctsip, w_resp_et_redcotsip,
   -- w_resp_tsip, w_resp_accesos_trksip, w_resp_vxprei, w_resp_intmov
   ELSIF w_resp_vxpga <> 'S'
   THEN
      w_resp := w_resp_vxpga;
   ELSIF w_resp_precal <> 'S'
   THEN
      w_resp := w_resp_precal;
   ELSIF w_resp_et_hfctsip <> 'S'
   THEN
      w_resp := w_resp_et_hfctsip;
   ELSIF w_resp_et_redcotsip <> 'S'
   THEN
      w_resp := w_resp_et_redcotsip;
   ELSIF w_resp_tsip <> 'S'
   THEN
      w_resp := w_resp_tsip;
   ELSIF w_resp_accesos_trksip <> 'S'
   THEN
      w_resp := w_resp_accesos_trksip;
   ELSIF w_resp_vxprei <> 'S'
   THEN
      w_resp := w_resp_vxprei;
   ELSIF w_resp_intmov <> 'S'
   THEN
      w_resp := w_resp_vxprei;
   ELSIF w_resp_intmov <> 'S'
   THEN
      w_resp := w_resp_et_reuso;
   --2012-03-13. FIN SECCIÓN.
   ELSIF w_resp_cgepm <> 'S'
   THEN
      w_resp := w_resp_cgepm;
   ELSIF w_resp_portab <> 'S'
   THEN
      w_resp := w_resp_portab;
   ELSIF w_resp_idiomas <> 'S'
   THEN
      w_resp := w_resp_idiomas;
   ELSIF w_resp_lte <> 'S'
   THEN
      w_resp := w_resp_lte;
   ELSIF w_resp_apn <> 'S'
   THEN
      w_resp := w_resp_apn;
      -- 2012-03-12 aagudc Se agrega verificación de posible error al ejecutar el estudio técnico de SERALI.
   ELSIF w_resp_serali <> 'S'
   THEN
          w_resp := w_resp_serali;
   --2012-02-16 Jrendbr  Se agrega verificación de posible error al ejecutar el estudio técnico de Verticales.
   ELSIF w_resp_inttic <> 'S' THEN
        w_resp := w_resp_inttic;
   --2012-10-17 Jrendbr  Se agrega verificación de posible error al ejecutar los estudios técnicos de IAAS.
   ELSIF w_resp_vdatac <> 'S' THEN
        w_resp := w_resp_vdatac;
   -- 2012-10-31 aagudc Se agrega verificación de posible error al ejecutar el estudio técnico de Microtic.
   ELSIF w_resp_microtic <> 'S' THEN
      w_resp := w_resp_microtic;
   -- 2013-02-28 Gsantos Retiro 3066
   ELSIF w_resp_ret3066 <> 'S' THEN
      w_resp := w_resp_ret3066;
      -- Fin 2013-02-28 Gsantos Retiro 3066
   --2013-05-17 lhernans Se agrega verificación de posible error al ejecutar el ET de accesos GPON
   ELSIF NVL(w_resp_prov_gpon, 'S') <> 'S' THEN
      w_resp := w_resp_prov_gpon;
   -- 2014-05-19 Verificación de valor de respuesta de Estudio Tecnico Telefonía GPON. Si es diferente a S, AA o nula se interpreta como error.
   ELSIF NVL(v_resp_prov_TO_gpon, 'S') NOT IN ('S', 'AA') THEN
      w_resp := v_resp_prov_TO_gpon;
   -- 2014-05-19 Verificación de variable de respuesta de Estudio Tecnico Televisión GPON. Si es diferente a S, AA o nula se interpreta como error.
   ELSIF NVL(v_resp_prov_TELEV_gpon, 'S') NOT IN ('S', 'AA') THEN
      w_resp := v_resp_prov_TELEV_gpon;
      -- 2013-08-08 Eaguirrg
   ELSIF NVL(w_resp_serequ,'S') <> 'S' THEN
      w_resp := w_resp_serequ;
   -- 2013-12-19 jcardega Se agrega verificación de posible error al ejecutar el estudio técnico de SERCOM
   ELSIF NVL(w_resp_SERCOM,'S') <> 'S' THEN
      w_resp := w_resp_SERCOM;
   --2014-03-27 lhernans Se agrega validación de error
   ELSIF NVL(w_resp_modequ, 'S') <> 'S' THEN
      w_resp := w_resp_modequ;
   END IF;

   --<<2014-02-27 Eaguirrg Inicio
   --Se realiza el llamado del procedimiento encargado de enrutar el codigo familiar por personal, familiar o pyme.
   FOR r_codigo_fam IN c_pedidos_codigo_familiar
     LOOP
       v_valor_4975 := coalesce(
                              fn_valor_caract_subpedido (w_pedido, r_codigo_fam.subpedido_id, 4975),
                              fn_valor_caract_identif (fn_identificador_subpedido(w_pedido, r_codigo_fam.subpedido_id), 4975),
                              fn_valor_caract_identif (fn_identificador_subpedido(w_pedido, r_codigo_fam.subpedido_id)||'-IC001', 4975)
                            );
       v_valor_2 := coalesce(
                           fn_valor_caract_subpedido (w_pedido, r_codigo_fam.subpedido_id, 2),
                           fn_valor_caract_identif (fn_identificador_subpedido(w_pedido, r_codigo_fam.subpedido_id), 2),
                           fn_valor_caract_identif (fn_identificador_subpedido(w_pedido, r_codigo_fam.subpedido_id)||'-IC001', 2)
                         );
       v_uen := fn_uen_calculada (w_pedido, r_codigo_fam.subpedido_id, r_codigo_fam.solicitud_id,NULL);
       if v_uen = 'C3' and nvl(v_valor_4975, 'HOG') = 'EMP' and v_valor_2 = 'RES' then
          v_uen := 'HG';
       end if;

        pr_valida_cod_fide_estudio_tec
       (
        w_pedido,
        r_codigo_fam.subpedido_id,
        r_codigo_fam.solicitud_id,
        v_uen,
        r_codigo_fam.tipo_elemento_id,
        v_mensaje_est_cod
       );


      IF v_mensaje_est_cod IS NOT NULL THEN
          --
           v_usuario := FN_USUARIO;
          --

          BEGIN

             INSERT INTO fnx_codigo_familiar_log
                            (pedido_id, tipo_elemento_id, transaccion,
                             mensaje_error, estado, fecha_estado, usuario)
                    VALUES ( w_pedido,r_codigo_fam.tipo_elemento_id,'ESTUDIO_TECNICO_CONTING',
                              v_mensaje_est_cod, 'PENDI', SYSDATE, v_usuario);

          EXCEPTION

                 WHEN DUP_VAL_ON_INDEX THEN

                     BEGIN
                         UPDATE fnx_codigo_familiar_log
                              SET   tipo_elemento_id = r_codigo_fam.tipo_elemento_id,
                                      transaccion      = 'ESTUDIO_TECNICO_CONTING',
                                      mensaje_error    = v_mensaje_est_cod,
                                      estado           = 'PENDI',
                                      fecha_estado     = SYSDATE,
                                      usuario          = v_usuario
                                WHERE pedido_id = W_pedido;

                     EXCEPTION
                        WHEN OTHERS THEN
                                DBMS_OUTPUT.PUT_LINE('<CODHOGAR> Error en pr_estudio_tecnico conting* '||sqlerrm);

                     END;

                 WHEN OTHERS THEN
                       DBMS_OUTPUT.PUT_LINE('<CODHOGAR> Error en pr_estudio_tecnico conting**'||sqlerrm);

          END;

       END IF;

     END LOOP;
   -->>2014-02-27 Eaguirrg FIN

   -- Parámetro para verificar sí se debe sobreescribir el mensaje de error.
   w_parametro := 88;
   v_estudiot_ok := NVL (FN_valor_parametros ('PRV_SERV', w_parametro), 'N');

   IF NVL (w_resp, 'N') <> 'S'
   THEN
      IF v_estudiot_ok = 'S' AND NOT vt_productos_pedido.EXISTS ('MOVIL')
      THEN
         BEGIN
            -- Registro de la inconsistencia en la tabla FNX_PEDGEN_FIAFS
            INSERT INTO FNX_PEDGEN_FIAFS (
                                             generado_id,
                                             fecha_generado,
                                             contrato,
                                             pedido_id,
                                             subpedido_id,
                                             solicitud_id,
                                             observacion,
                                             tipo_transaccion,
                                             problema
                       )
              VALUES   (
                           PEGEN_SEQ.NEXTVAL, SYSDATE, NULL, w_pedido, 1, 1,
                           w_resp, 'ET', 'S'
                       );

            w_resp := 'S';
         EXCEPTION
            WHEN OTHERS
            THEN
               w_resp := 'S';
         END;
      END IF;
   ELSE
      IF w_resp_cambio_estado <> 'S'
      THEN
         w_resp := w_resp_cambio_estado;
      END IF;
   END IF;

    -- 2014-03-05 Jrincong
    -- Se borra el arreglo vt_productos_pedido de Productos del pedido.
    vt_productos_pedido.DELETE;

EXCEPTION
   WHEN e_et_no_ejecutado
   THEN
      DBMS_OUTPUT.put_line ('Mensaje: Excepción Estudio Técnico No Ejecutado');

      -- 2014-03-05 Jrincong
      -- Se borra el arreglo vt_productos_pedido de Productos del pedido.
      vt_productos_pedido.DELETE;

      w_resp := 'S';

   WHEN OTHERS
   THEN
      -- 2014-03-05 Jrincong
      -- Se borra el arreglo vt_productos_pedido de Productos del pedido.
      vt_productos_pedido.DELETE;
      DBMS_OUTPUT.put_line ('Mensaje: When Others');
      DBMS_OUTPUT.put_line ('<MSG> ' || SUBSTR (SQLERRM, 1, 100));
END;