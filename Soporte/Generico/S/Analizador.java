import java.io.*;
import java.util.*;
public class Analizador{
	public Scanner prefuente;
	public Tokenizer fuente;
	public Analizador(File archivo) throws Exception{
		prefuente=new Scanner(archivo);
	}
	public void formatear(){
		String texto="";
		while(prefuente.hasNext()){
			String linea=prefuente.nextLine();
			int ind=linea.indexOf("&");
			if(ind>=0){
				System.out.println("El archivo tiene el caracter &");
				System.exit(0);
			}
			/*int ind=linea.indexOf("--");
			if(ind>0){
				System.out.println(linea);
			}
			if(ind>=0){
				linea=linea.substring(0,ind);
			}*/
			texto+=" "+linea+"&";
		}
		if(texto.indexOf("/*/")>0){
			System.out.println("El archivo tiene el caracter /*/");
			//System.exit(0);
		}
		prefuente=new Scanner(texto.replace("*/"," */ ").replace("/*"," /* "));
		//prefuente=new Scanner(texto.replace("/*"," /* ").replace("*/"," */ "));

		boolean ignorar=false;
		texto="";
		while(prefuente.hasNext()){
			String token=prefuente.next();
			if(token.equals("/*")){
				ignorar=true;
			}
			if(token.equals("*/")){
				ignorar=false;
				continue;
			}
			if(!ignorar){
				texto+=" "+token;
			}
		}
		//fuente=new Tokenizer(texto.replace("ELSIF","ELSE IF"));
		fuente=new Tokenizer(texto);
	}
	public Programa analizar(){
		Programa programa=new Programa();
		formatear();
		while(fuente.hasNext()){
			String token=fuente.next();
			Procedimiento.comprobar(token,fuente,programa);
			Funcion.comprobar(token,fuente,programa);
			Bloque.comprobar(token,fuente,programa);
		}
		return programa;
	}
}