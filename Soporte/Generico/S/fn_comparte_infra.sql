FUNCTION       fn_comparte_infra (
   p_identificador_busq   IN   fnx_identificadores.identificador_id%TYPE,
   p_tecnologia_busq      IN   fnx_solicitudes.tecnologia_id%TYPE,
   p_servicio             IN   fnx_solicitudes.servicio_id%TYPE,
   p_producto             IN   fnx_solicitudes.producto_id%TYPE,
   p_pedido               IN   fnx_solicitudes.pedido_id%TYPE,
   p_subpedido            IN   fnx_solicitudes.subpedido_id%TYPE,
   p_solicitud            IN   fnx_solicitudes.solicitud_id%TYPE
)
   RETURN VARCHAR2
IS
-- Organizacion     UNE EPM Telecomunicaciones S.A.
-- Descripcion:     Funci�n para retornar el identificador que comparte infraestructura, dependiendo del producto requerido.
-- Construido por:  Edith Paola Tache J - Yamily Solarte. - MVM Ingenieria de Software

-- HISTORIA DE MODIFICACIONES
-- Fecha        Autor       Observaciones
-- -----------  ----------- ------------------------------------------------------
-- 2009-08-26   Etachej    Construcci�n inicial de la funcion
--              Ysolarte
-- 2010-08-04   Etachej    Se valida que comparta infraestructura con un producto del mismo pedido que ya est� instalada
-- 2012-06-14   Jpulgarb   Se modifica el cursor c_inf_tv_activas_2 para forzar el index FNX_INF_TV_ACTIVAS_IDXFN001 (pry:Estabilizacion IP)
-- 2012-07-10   Jrendbr    Se agrega validacion para verifcar red en tramite y crear el identificador que comparte
--                         la infraestructura
-- 2012-07-27   JPULGARB   el identificador de retorno (redes activas), dando prioridad al identificador con el que comparta igual infraestructura,
--                         que se encuentre en estado OCU y que tenga la misma p�gina).
-- 2014-06-11   JGlenn     Implementaci�n de cursores para b�squeda de red activa y red en tr�mite tecnolog�a GPON
-- 2014-06-11   JGlenn     Implementaci�n de l�gica de b�squeda de infraestructura compartida para Infraestructura GPON.
-- 2014-06-18   Jrincong   Se renombran los par�metros ( p_identificador1 cambia por p_identificador_base y p_tecnologia1 cambia por p_tecnologia_busq)
-- 2014-06-18   Jrincong   Se renombra el cursor c_inf_redes_activas por c_inf_red_activa_ID
-- 2014-06-18   Jrincong   Se renombra el cursor c_inf_tv_activas por c_inf_tv_activa_ID
-- 2014-06-18   Jrincong   Se renombra el cursor c_inf_tv_activas_2 por c_inf_tv_activa_otrosID
-- 2014-06-18   Jrincong   Se renombra el cursor c_inf_redes_tramites por c_inf_red_tramite_solic
-- 2014-06-18   Jrincong   Se renombra el cursor c_inf_tv_tramites por c_inf_tv_tramite_solic
-- 2014-06-18   Jrincong   Se renombra el cursor c_inf_tv_tramites_2 por c_inf_tv_pedidos_tram
-- 2014-06-18   Jrincong   Se renombra el cursor c_inf_tv_tramites_3 por c_inf_tv_tram_EnPedido
-- 2014-06-18   Jrincong   Uso de variable v_inf_red_tramite_solic para contener registros de cursor c_inf_red_tramite_solic
-- 2014-06-18   Jrincong   Uso de variable v_inf_tv_tramite_solic para contener registros de cursor c_inf_tv_tramite_solic.
-- 2014-06-18   Jrincong   Se renombra la variable r_inf_tv_activas_2 por r_inf_tv_acti_otros
-- 2014-06-18   Jrincong   Se renombra la variable r_inf_tv_tramites_2 por r_inf_tv_tram_pedidos
-- 2014-06-19   Jrincong   REQ 36900 GPON Hogares.  En caso que se haya encontrado un identificador que comparte infraestructura,
--                         se abandona el ciclo para que en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ),
--                         el ciclo no contin�e evaluando los registros restantes.
-- 2014-06-19   Jrincong   REQ 36900 GPON Hogares. [ 001-c_inf_tv_pedidos_tram ] / [ 002-c_inf_tv_activa_otrosid ] / [ 003-c_inf_gpon_pedidos_tram ]
-- 2014-06-19   Jrincong   REQ 36900 GPON Hogares. [ 004-c_inf_gpon_activa_otrosid ] / [ 005-c_inf_gpon_tram_enpedido ] / [ 006-c_inf_redes_secundarias ]
-- 2014-06-19   Jrincong   REQ 36900 GPON Hogares.  [ 007-c_inf_tv_activa_otrosid ] / [ 008-c_inf_tv_activa_otrosid ]

   p_identificador               fnx_identificadores.identificador_id%TYPE;

   -- Cursor para buscar la red de Cobre asociada a un identificador ( Activa )
   CURSOR c_inf_red_activa_id (
      pc_identificador1   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT nodo_conmutador_id, armario_id, NVL (strip_id, 'N') strip_id,
             NVL (par_primario_id, '0') par_primario_id, caja_id,
             par_secundario_id
        FROM fnx_inf_redes_activas
       WHERE identificador_id = pc_identificador1;

   -- Cursor para buscar los identificadores asociados a los datos de una Red Primaria [Nodo, Armario, Strip y Par ] y que
   -- sean diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_redes_directas (
      pc_identificador1       IN   fnx_identificadores.identificador_id%TYPE,
      pc_nodo_conmutador_id   IN   fnx_inf_redes_activas.nodo_conmutador_id%TYPE,
      pc_armario_id           IN   fnx_inf_redes_activas.armario_id%TYPE,
      pc_strip_id             IN   fnx_inf_redes_activas.strip_id%TYPE,
      pc_par_primario_id      IN   fnx_inf_redes_activas.par_primario_id%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_redes_activas
       WHERE nodo_conmutador_id = pc_nodo_conmutador_id
         AND armario_id = pc_armario_id
         AND NVL (strip_id, 'N') = pc_strip_id
         AND NVL (par_primario_id, '0') = pc_par_primario_id
         AND identificador_id <> pc_identificador1;

   -- Cursor para buscar los identificadores asociados a los datos de una Red Completa (Primaria y Secundaria
   -- [Nodo, Armario, Strip, Par Primario, Caja y Par Secundario] y que sean diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_redes_secundarias (
      pc_identificador1       IN   fnx_identificadores.identificador_id%TYPE,
      pc_nodo_conmutador_id   IN   fnx_inf_redes_activas.nodo_conmutador_id%TYPE,
      pc_armario_id           IN   fnx_inf_redes_activas.armario_id%TYPE,
      pc_strip_id             IN   fnx_inf_redes_activas.strip_id%TYPE,
      pc_par_primario_id      IN   fnx_inf_redes_activas.par_primario_id%TYPE,
      pc_caja_id              IN   fnx_inf_redes_activas.caja_id%TYPE,
      pc_par_secundario_id    IN   fnx_inf_redes_activas.par_secundario_id%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_redes_activas
       WHERE nodo_conmutador_id = pc_nodo_conmutador_id
         AND armario_id = pc_armario_id
         AND NVL (strip_id, 'N') = pc_strip_id
         AND NVL (par_primario_id, '0') = pc_par_primario_id
         AND caja_id = pc_caja_id
         AND par_secundario_id = pc_par_secundario_id
         AND identificador_id <> pc_identificador1;

   -- Cursor para buscar la red HFC activa asociada a un identificador.
   CURSOR c_inf_tv_activa_id (
      pc_identificador1   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT identificador_id, centro_distribucion_intermedia,
             nodo_optico_electrico_id, tipo_amplificador_id, amplificador_id,
             tipo_derivador_id, derivador_id, terminal_derivador_id
        FROM fnx_inf_tv_activas
       WHERE identificador_id = pc_identificador1;

   -- Cursor para buscar los identificadores asociados a los elementos de red HFC y que sean diferentes al identificador que
   -- se pasa como par�metro.
   CURSOR c_inf_tv_activa_otrosid (
      pc_identificador1              IN   fnx_identificadores.identificador_id%TYPE,
      pc_centro_distribucion_inter   IN   fnx_inf_tv_activas.centro_distribucion_intermedia%TYPE,
      pc_nodo_optico_electrico_id    IN   fnx_inf_tv_activas.nodo_optico_electrico_id%TYPE,
      pc_amplificador_id             IN   fnx_inf_tv_activas.amplificador_id%TYPE,
      pc_tipo_amplificador_id        IN   fnx_inf_tv_activas.tipo_amplificador_id%TYPE,
      pc_tipo_derivador_id           IN   fnx_inf_tv_activas.tipo_derivador_id%TYPE,
      pc_derivador_id                IN   fnx_inf_tv_activas.derivador_id%TYPE,
      pc_terminal_derivador_id       IN   fnx_inf_tv_activas.terminal_derivador_id%TYPE
   )
   IS
-- 2012-06-14   Jpulgarb   Se modifica el cursor c_inf_tv_activa_otrosID para forzar el index FNX_INF_TV_ACTIVAS_IDXFN001 (pry:Estabilizacion IP)
      SELECT /*+ INDEX(X FNX_INF_TV_ACTIVA_IDXFN_001) */
             identificador_id
        FROM fnx_inf_tv_activas x
       WHERE centro_distribucion_intermedia = pc_centro_distribucion_inter
         AND nodo_optico_electrico_id = pc_nodo_optico_electrico_id
         AND NVL (tipo_amplificador_id, 'X') =
                                            NVL (pc_tipo_amplificador_id, 'X')
         AND NVL (amplificador_id, 'X') = NVL (pc_amplificador_id, 'X')
         AND NVL (tipo_derivador_id, 'X') = NVL (pc_tipo_derivador_id, 'X')
         AND NVL (derivador_id, 'X') = NVL (pc_derivador_id, 'X')
         AND NVL (terminal_derivador_id, 0) =
                                             NVL (pc_terminal_derivador_id, 0)
         AND identificador_id <> pc_identificador1;

   -- Cursor para buscar los datos de servicio y producto de un identificador.
   CURSOR c_identif_producto (
      pc_identificador1   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT servicio_id, producto_id
        FROM fnx_identificadores
       WHERE identificador_id = pc_identificador1 AND tipo_elemento_id <> 'TO';

   -- Cursor para busca la red de cobre en tr�mite para una solicitud determinada
   CURSOR c_inf_red_tramite_solic (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE,
      pc_pedido          IN   fnx_solicitudes.pedido_id%TYPE,
      pc_subpedido       IN   fnx_solicitudes.subpedido_id%TYPE,
      pc_solicitud       IN   fnx_solicitudes.solicitud_id%TYPE
   )
   IS
      SELECT nodo_conmutador_id, armario_id, NVL (strip_id, 'N') strip_id,
             NVL (par_primario_id, '0') par_primario_id, caja_id,
             par_secundario_id
        FROM fnx_inf_redes_tramites
       WHERE identificador_id = pc_identificador
         AND pedido_id = pc_pedido
         AND subpedido_id = pc_subpedido
         AND solicitud_id = pc_solicitud
         AND NVL (instalado, 'N') <> 'S';

   v_inf_red_tramite_solic       c_inf_red_tramite_solic%ROWTYPE;

   -- Cursor para buscar los identificadores que pueden estar asociados a pedidos en Tr�mites para una red directa determinada
   -- [nodo_conmutador - armario - strip - par primario ]. Los identificadores deben ser diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_redes_tramites_directas (
      pc_identificador        IN   fnx_identificadores.identificador_id%TYPE,
      pc_nodo_conmutador_id   IN   fnx_inf_redes_activas.nodo_conmutador_id%TYPE,
      pc_armario_id           IN   fnx_inf_redes_activas.armario_id%TYPE,
      pc_strip_id             IN   fnx_inf_redes_activas.strip_id%TYPE,
      pc_par_primario_id      IN   fnx_inf_redes_activas.par_primario_id%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_redes_tramites
       WHERE nodo_conmutador_id = pc_nodo_conmutador_id
         AND armario_id = pc_armario_id
         AND NVL (strip_id, 'N') = pc_strip_id
         AND NVL (par_primario_id, '0') = pc_par_primario_id
         AND identificador_id <> pc_identificador
         AND NVL (instalado, 'N') <> 'S';

   -- Cursor para buscar los identificadores que pueden estar asociados a pedidos en Tr�mites para una red primaria-secundaria
   -- determinada: [nodo_conmutador - armario - strip - par primario - caja - par secundario]
   -- Los identificadores deben ser diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_redes_tramites_sec (
      pc_identificador1       IN   fnx_identificadores.identificador_id%TYPE,
      pc_nodo_conmutador_id   IN   fnx_inf_redes_activas.nodo_conmutador_id%TYPE,
      pc_armario_id           IN   fnx_inf_redes_activas.armario_id%TYPE,
      pc_strip_id             IN   fnx_inf_redes_activas.strip_id%TYPE,
      pc_par_primario_id      IN   fnx_inf_redes_activas.par_primario_id%TYPE,
      pc_caja_id              IN   fnx_inf_redes_activas.caja_id%TYPE,
      pc_par_secundario_id    IN   fnx_inf_redes_activas.par_secundario_id%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_redes_tramites
       WHERE nodo_conmutador_id = pc_nodo_conmutador_id
         AND armario_id = pc_armario_id
         AND NVL (strip_id, 'N') = pc_strip_id
         AND NVL (par_primario_id, '0') = pc_par_primario_id
         AND caja_id = pc_caja_id
         AND par_secundario_id = pc_par_secundario_id
         AND identificador_id <> pc_identificador1
         AND NVL (instalado, 'N') <> 'S';

   -- Cursor para buscar la red HFC en tr�mite para una solicitud determinada
   CURSOR c_inf_tv_tramite_solic (
      pc_identificador1   IN   fnx_identificadores.identificador_id%TYPE,
      pc_pedido           IN   fnx_solicitudes.pedido_id%TYPE,
      pc_subpedido        IN   fnx_solicitudes.subpedido_id%TYPE,
      pc_solicitud        IN   fnx_solicitudes.solicitud_id%TYPE
   )
   IS
      SELECT identificador_id, centro_distribucion_intermedia,
             nodo_optico_electrico_id, tipo_amplificador_id, amplificador_id,
             tipo_derivador_id, derivador_id, terminal_derivador_id
        FROM fnx_inf_tv_tramites
       WHERE identificador_id = pc_identificador1
         AND pedido_id = pc_pedido
         AND subpedido_id = pc_subpedido
         AND solicitud_id = pc_solicitud
         AND NVL (instalado, 'N') <> 'S';

   v_inf_tv_tramite_solic        c_inf_tv_tramite_solic%ROWTYPE;

   -- Cursor para buscar los identificadores que puedan estar asociados a varios pedidos en Tr�mite de red HFC
   -- buscando a trav�s de los elementos de la red ( CDI, NOE, Amplificador, Derivador, Terminal )
   -- Los identificadores deben ser diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_tv_pedidos_tram (
      pc_identificador1              IN   fnx_identificadores.identificador_id%TYPE,
      pc_centro_distribucion_inter   IN   fnx_inf_tv_activas.centro_distribucion_intermedia%TYPE,
      pc_nodo_optico_electrico_id    IN   fnx_inf_tv_activas.nodo_optico_electrico_id%TYPE,
      pc_amplificador_id             IN   fnx_inf_tv_activas.amplificador_id%TYPE,
      pc_tipo_amplificador_id        IN   fnx_inf_tv_activas.tipo_amplificador_id%TYPE,
      pc_tipo_derivador_id           IN   fnx_inf_tv_activas.tipo_derivador_id%TYPE,
      pc_derivador_id                IN   fnx_inf_tv_activas.derivador_id%TYPE,
      pc_terminal_derivador_id       IN   fnx_inf_tv_activas.terminal_derivador_id%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_tv_tramites
       WHERE centro_distribucion_intermedia = pc_centro_distribucion_inter
         AND nodo_optico_electrico_id = pc_nodo_optico_electrico_id
         AND tipo_amplificador_id = pc_tipo_amplificador_id
         AND amplificador_id = pc_amplificador_id
         AND tipo_derivador_id = pc_tipo_derivador_id
         AND derivador_id = pc_derivador_id
         AND terminal_derivador_id = pc_terminal_derivador_id
         AND identificador_id <> pc_identificador1
         AND NVL (instalado, 'N') <> 'S';

   -- Cursor para buscar los identificadores que puedan estar asociados al MISMO pedido en Tr�mite de red HFC
   -- buscando a trav�s de los elementos de la red ( CDI, NOE, Amplificador, Derivador, Terminal )
   -- Los identificadores deben ser diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_tv_tram_enpedido (
      pc_identificador1              IN   fnx_identificadores.identificador_id%TYPE,
      pc_centro_distribucion_inter   IN   fnx_inf_tv_activas.centro_distribucion_intermedia%TYPE,
      pc_nodo_optico_electrico_id    IN   fnx_inf_tv_activas.nodo_optico_electrico_id%TYPE,
      pc_amplificador_id             IN   fnx_inf_tv_activas.amplificador_id%TYPE,
      pc_tipo_amplificador_id        IN   fnx_inf_tv_activas.tipo_amplificador_id%TYPE,
      pc_tipo_derivador_id           IN   fnx_inf_tv_activas.tipo_derivador_id%TYPE,
      pc_derivador_id                IN   fnx_inf_tv_activas.derivador_id%TYPE,
      pc_terminal_derivador_id       IN   fnx_inf_tv_activas.terminal_derivador_id%TYPE,
      pc_pedido                      IN   fnx_inf_tv_tramites.pedido_id%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_tv_tramites
       WHERE centro_distribucion_intermedia = pc_centro_distribucion_inter
         AND nodo_optico_electrico_id = pc_nodo_optico_electrico_id
         AND tipo_amplificador_id = pc_tipo_amplificador_id
         AND amplificador_id = pc_amplificador_id
         AND tipo_derivador_id = pc_tipo_derivador_id
         AND derivador_id = pc_derivador_id
         AND terminal_derivador_id = pc_terminal_derivador_id
         AND identificador_id <> pc_identificador1
         AND NVL (instalado, 'N') = 'S'
         AND pedido_id = pc_pedido;

   v_inf_redes_activas           c_inf_red_activa_id%ROWTYPE;
   v_inf_tv_activas              c_inf_tv_activa_id%ROWTYPE;
   v_identificador_comparte      fnx_identificadores.identificador_id%TYPE
                                                                       := NULL;
   v_identif_servicio            fnx_identificadores.servicio_id%TYPE := NULL;
   v_identif_producto            fnx_identificadores.producto_id%TYPE := NULL;
   b_traslado_servicio           BOOLEAN := FALSE;
   v_tipo_trabajo                fnx_trabajos_solicitudes.tipo_trabajo%TYPE;
   v_estado_identif              fnx_identificadores.estado%TYPE := NULL;
--2012-07-27   JPULGARB
   v_direccion_iden_base         fnx_configuraciones_identif.valor%TYPE
                                                                       := NULL;
   v_direccion_iden              fnx_configuraciones_identif.valor%TYPE
                                                                       := NULL;

   -- Cursor c_inf_gpon_activas para buscar la red GPON activa de un identificador
   CURSOR c_inf_gpon_activas                    -- [Antes c_inf_gpon_activas ]
                            (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT identificador_id, olt_id, armario_id, municipio_id, splitter_id,
             nap_id, hilo_id, tarjeta_id, puerto_id, pto_logico
        FROM fnx_inf_gpon_activas
       WHERE identificador_id = pc_identificador;

   -- Cursor para buscar los identificadores asociados a los elementos de red GPON.
   -- Los identificadores deben ser diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_gpon_activa_otrosid                  -- [c_inf_gpon_activas_2]
                                   (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE,
      pc_olt_id          IN   fnx_inf_gpon_activas.olt_id%TYPE,
      pc_armario_id      IN   fnx_inf_gpon_activas.armario_id%TYPE,
      pc_municipio_id    IN   fnx_inf_gpon_activas.municipio_id%TYPE,
      pc_splitter_id     IN   fnx_inf_gpon_activas.splitter_id%TYPE,
      pc_nap_id          IN   fnx_inf_gpon_activas.nap_id%TYPE,
      pc_hilo_id         IN   fnx_inf_gpon_activas.hilo_id%TYPE,
      pc_tarjeta_id      IN   fnx_inf_gpon_activas.tarjeta_id%TYPE,
      pc_puerto_id       IN   fnx_inf_gpon_activas.puerto_id%TYPE,
      pc_pto_logico      IN   fnx_inf_gpon_activas.pto_logico%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_gpon_activas
       WHERE olt_id = pc_olt_id
         AND armario_id = pc_armario_id
         AND municipio_id = pc_municipio_id
         AND splitter_id = pc_splitter_id
         AND nap_id = pc_nap_id
         AND hilo_id = pc_hilo_id
         AND tarjeta_id = pc_tarjeta_id
         AND puerto_id = pc_puerto_id
         AND pto_logico = pc_pto_logico
         AND identificador_id <> pc_identificador;

   -- Cursor para buscar la red GPON en tr�mite para una solicitud determinada
   CURSOR c_inf_gpon_tram_solic                         -- c_inf_gpon_tramites
                               (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE,
      pc_pedido          IN   fnx_solicitudes.pedido_id%TYPE,
      pc_subpedido       IN   fnx_solicitudes.subpedido_id%TYPE,
      pc_solicitud       IN   fnx_solicitudes.solicitud_id%TYPE
   )
   IS
      SELECT identificador_id, olt_id, armario_id, municipio_id, splitter_id,
             nap_id, hilo_id, tarjeta_id, puerto_id, pto_logico
        FROM fnx_inf_gpon_tramites
       WHERE identificador_id = pc_identificador
         AND pedido_id = pc_pedido
         AND subpedido_id = pc_subpedido
         AND solicitud_id = pc_solicitud
         AND NVL (instalado, 'N') <> 'S';

   -- Cursor para buscar los identificadores que puedan estar asociados a varios pedidos en Tr�mite de red GPON
   -- buscando a trav�s de los elementos de la red ( OLT, Armario, Municipio, Splitter, NAP, Hilo, Tarjeta, Puerto Fisico y Puerto L�gico )
   -- Los identificadores deben ser diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_gpon_pedidos_tram            -- Antes [c_inf_gpon_tramites_2 ]
                                 (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE,
      pc_olt_id          IN   fnx_inf_gpon_activas.olt_id%TYPE,
      pc_armario_id      IN   fnx_inf_gpon_activas.armario_id%TYPE,
      pc_municipio_id    IN   fnx_inf_gpon_activas.municipio_id%TYPE,
      pc_splitter_id     IN   fnx_inf_gpon_activas.splitter_id%TYPE,
      pc_nap_id          IN   fnx_inf_gpon_activas.nap_id%TYPE,
      pc_hilo_id         IN   fnx_inf_gpon_activas.hilo_id%TYPE,
      pc_tarjeta_id      IN   fnx_inf_gpon_activas.tarjeta_id%TYPE,
      pc_puerto_id       IN   fnx_inf_gpon_activas.puerto_id%TYPE,
      pc_pto_logico      IN   fnx_inf_gpon_activas.pto_logico%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_gpon_tramites
       WHERE olt_id = pc_olt_id
         AND armario_id = pc_armario_id
         AND municipio_id = pc_municipio_id
         AND splitter_id = pc_splitter_id
         AND nap_id = pc_nap_id
         AND hilo_id = pc_hilo_id
         AND tarjeta_id = pc_tarjeta_id
         AND puerto_id = pc_puerto_id
         AND pto_logico = pc_pto_logico
         AND identificador_id <> pc_identificador
         AND NVL (instalado, 'N') <> 'S';

   -- Cursor para buscar los identificadores que puedan estar asociados en el MISMO pedido en Tr�mite de red GPON
   -- buscando a trav�s de los elementos de la red ( OLT, Armario, Municipio, Splitter, NAP, Hilo, Tarjeta, Puerto Fisico y Puerto L�gico )
   -- Los identificadores deben ser diferentes al identificador que se pasa como par�metro.
   CURSOR c_inf_gpon_tram_enpedido          -- Antes [ c_inf_gpon_tramites_3 ]
                                  (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE,
      pc_olt_id          IN   fnx_inf_gpon_activas.olt_id%TYPE,
      pc_armario_id      IN   fnx_inf_gpon_activas.armario_id%TYPE,
      pc_municipio_id    IN   fnx_inf_gpon_activas.municipio_id%TYPE,
      pc_splitter_id     IN   fnx_inf_gpon_activas.splitter_id%TYPE,
      pc_nap_id          IN   fnx_inf_gpon_activas.nap_id%TYPE,
      pc_hilo_id         IN   fnx_inf_gpon_activas.hilo_id%TYPE,
      pc_tarjeta_id      IN   fnx_inf_gpon_activas.tarjeta_id%TYPE,
      pc_puerto_id       IN   fnx_inf_gpon_activas.puerto_id%TYPE,
      pc_pto_logico      IN   fnx_inf_gpon_activas.pto_logico%TYPE,
      pc_pedido          IN   fnx_inf_gpon_tramites.pedido_id%TYPE
   )
   IS
      SELECT identificador_id
        FROM fnx_inf_gpon_tramites
       WHERE olt_id = pc_olt_id
         AND armario_id = pc_armario_id
         AND municipio_id = pc_municipio_id
         AND splitter_id = pc_splitter_id
         AND nap_id = pc_nap_id
         AND hilo_id = pc_hilo_id
         AND tarjeta_id = pc_tarjeta_id
         AND puerto_id = pc_puerto_id
         AND pto_logico = pc_pto_logico
         AND identificador_id <> pc_identificador
         AND NVL (instalado, 'N') = 'S'
         AND pedido_id = pc_pedido;

   v_inf_gpon_activas            c_inf_gpon_activas%ROWTYPE;
   v_inf_gpon_tramites           c_inf_gpon_tram_solic%ROWTYPE;
BEGIN
   IF (p_pedido IS NOT NULL) THEN
      v_tipo_trabajo :=
         pkg_solicitudes.fn_tipo_trabajo (p_pedido,
                                          p_subpedido,
                                          p_solicitud,
                                          38
                                         );

      IF NVL (v_tipo_trabajo, 'NO') = 'CAMBI' THEN
         b_traslado_servicio := TRUE;
      END IF;
   END IF;

   IF (NOT b_traslado_servicio) OR (b_traslado_servicio AND p_pedido IS NULL) THEN
      IF p_tecnologia_busq = 'REDCO' THEN
         OPEN c_inf_red_activa_id (p_identificador_busq);

         FETCH c_inf_red_activa_id
          INTO v_inf_redes_activas;

         CLOSE c_inf_red_activa_id;

         --Se verifica si es red primaria DIRECTA
         IF INSTR (v_inf_redes_activas.armario_id, 'D') = 1 THEN

            -- S� el armario es de una Red Directa, se realiza la b�squeda con los elementos de Red Primaria
            -- [nodo_conmutador_id - armario_id - strip_id - par_primario ]

            FOR r_inf_redes_directas IN
               c_inf_redes_directas (p_identificador_busq,
                                     v_inf_redes_activas.nodo_conmutador_id,
                                     v_inf_redes_activas.armario_id,
                                     v_inf_redes_activas.strip_id,
                                     v_inf_redes_activas.par_primario_id
                                    )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto (r_inf_redes_directas.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                        r_inf_redes_directas.identificador_id;

                  -->2012-07-27 JPULGARB (Se rompe el ciclo cuando se encuentra el primer identificador con
                                         --el que se comparte infraestrcura de red Directa)
                  IF v_identificador_comparte IS NOT NULL THEN
                     EXIT;
                  END IF;
               END IF;
            END LOOP;

         ELSE

            -- S� el armario no es de una Red Directa, se realiza la b�squeda con los elementos de Red Primaria y Secundaria
            -- [nodo_conmutador_id - armario_id - strip_id - par_primario - caja_id - par_secundario ]

            FOR r_inf_redes_secundarias IN
               c_inf_redes_secundarias
                                     (p_identificador_busq,
                                      v_inf_redes_activas.nodo_conmutador_id,
                                      v_inf_redes_activas.armario_id,
                                      v_inf_redes_activas.strip_id,
                                      v_inf_redes_activas.par_primario_id,
                                      v_inf_redes_activas.caja_id,
                                      v_inf_redes_activas.par_secundario_id
                                     )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto
                                    (r_inf_redes_secundarias.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                     r_inf_redes_secundarias.identificador_id;

                  --> 2012-07-27   JPULGARB   el identificador de retorno (redes activas), dando prioridad al identificador con el que comparta igual infraestructura, que se encuentre en estado OCU y que tenga la misma p�gina).
                  v_direccion_iden_base := NULL;
                  v_direccion_iden := NULL;
                  v_direccion_iden_base :=
                           fn_valor_caract_identif (p_identificador_busq, 38);
                  v_direccion_iden :=
                     fn_valor_caract_identif
                                   (r_inf_redes_secundarias.identificador_id,
                                    38
                                   );

                  IF v_direccion_iden_base = v_direccion_iden THEN
                     BEGIN
                        SELECT estado
                          INTO v_estado_identif
                          FROM fnx_identificadores
                         WHERE identificador_id = v_identificador_comparte;

                        IF (NVL (v_estado_identif, 'N') = 'OCU') THEN
                           EXIT;
                        END IF;
                     EXCEPTION
                        WHEN OTHERS THEN
                           NULL;
                     END;
                  END IF;
               -->
               END IF;
            END LOOP;
         END IF;

         --Verifica en redes tramites, si comparte infraestructura
         IF (v_identificador_comparte IS NULL) THEN
            v_inf_red_tramite_solic := NULL;
            v_identif_servicio := NULL;
            v_identif_producto := NULL;

            OPEN c_inf_red_tramite_solic (p_identificador_busq,
                                          p_pedido,
                                          p_subpedido,
                                          p_solicitud
                                         );

            FETCH c_inf_red_tramite_solic
             INTO v_inf_red_tramite_solic;

            CLOSE c_inf_red_tramite_solic;

            --Se verifica si es red primaria DIRECTA
            IF INSTR (v_inf_red_tramite_solic.armario_id, 'D') = 1 THEN

               -- S� el armario es de una Red Directa, se realiza la b�squeda con los elementos de Red Primaria
               -- [nodo_conmutador_id - armario_id - strip_id - par_primario ]

               FOR r_inf_redes_tramites_directas IN
                  c_inf_redes_tramites_directas
                                 (p_identificador_busq,
                                  v_inf_red_tramite_solic.nodo_conmutador_id,
                                  v_inf_red_tramite_solic.armario_id,
                                  v_inf_red_tramite_solic.strip_id,
                                  v_inf_red_tramite_solic.par_primario_id
                                 )
               LOOP
                  v_identif_servicio := NULL;
                  v_identif_producto := NULL;

                  OPEN c_identif_producto
                              (r_inf_redes_tramites_directas.identificador_id);

                  FETCH c_identif_producto
                   INTO v_identif_servicio, v_identif_producto;

                  CLOSE c_identif_producto;

                  IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                     AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                     v_identificador_comparte :=
                               r_inf_redes_tramites_directas.identificador_id;

                     -->2012-07-27 JPULGARB (Se rompe el ciclo cuando se encuentra el primer identificador con
                                           --el que se comparte infraestrcura de red Directa)
                     IF v_identificador_comparte IS NOT NULL THEN
                        EXIT;
                     END IF;
                  END IF;
               END LOOP;
            ELSE

               -- S� el armario no es de una Red Directa, se realiza la b�squeda con los elementos de Red Primaria y Secundaria
               -- [nodo_conmutador_id - armario_id - strip_id - par_primario - caja_id - par_secundario ]

               FOR r_inf_redes_tramites_sec IN
                  c_inf_redes_tramites_sec
                                 (p_identificador_busq,
                                  v_inf_red_tramite_solic.nodo_conmutador_id,
                                  v_inf_red_tramite_solic.armario_id,
                                  v_inf_red_tramite_solic.strip_id,
                                  v_inf_red_tramite_solic.par_primario_id,
                                  v_inf_red_tramite_solic.caja_id,
                                  v_inf_red_tramite_solic.par_secundario_id
                                 )
               LOOP
                  v_identif_servicio := NULL;
                  v_identif_producto := NULL;

                  OPEN c_identif_producto
                                   (r_inf_redes_tramites_sec.identificador_id);

                  FETCH c_identif_producto
                   INTO v_identif_servicio, v_identif_producto;

                  CLOSE c_identif_producto;

                  IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                     AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                     v_identificador_comparte :=
                                    r_inf_redes_tramites_sec.identificador_id;
                  END IF;
               END LOOP;
            END IF;
         END IF;
      END IF;

      IF p_tecnologia_busq = 'HFC' THEN

         OPEN c_inf_tv_activa_id (p_identificador_busq);

         FETCH c_inf_tv_activa_id
          INTO v_inf_tv_activas;

         CLOSE c_inf_tv_activa_id;

         FOR r_inf_tv_acti_otros IN
            c_inf_tv_activa_otrosid
                            (p_identificador_busq,
                             v_inf_tv_activas.centro_distribucion_intermedia,
                             v_inf_tv_activas.nodo_optico_electrico_id,
                             v_inf_tv_activas.amplificador_id,
                             v_inf_tv_activas.tipo_amplificador_id,
                             v_inf_tv_activas.tipo_derivador_id,
                             v_inf_tv_activas.derivador_id,
                             v_inf_tv_activas.terminal_derivador_id
                            )
         LOOP
            v_identif_servicio := NULL;
            v_identif_producto := NULL;

            OPEN c_identif_producto (r_inf_tv_acti_otros.identificador_id);

            FETCH c_identif_producto
             INTO v_identif_servicio, v_identif_producto;

            CLOSE c_identif_producto;

            IF     (NVL (v_identif_servicio, 'N') = p_servicio)
               AND (NVL (v_identif_producto, 'N') = p_producto) THEN
               v_identificador_comparte :=
                                         r_inf_tv_acti_otros.identificador_id;
               --> 2012-07-27   JPULGARB   el identificador de retorno (redes activas), dando prioridad al identificador
               --  con el que comparta igual infraestructura, que se encuentre en estado OCU y que tenga la misma p�gina).
               v_direccion_iden_base := NULL;
               v_direccion_iden := NULL;
               v_direccion_iden_base :=
                           fn_valor_caract_identif (p_identificador_busq, 38);
               v_direccion_iden :=
                  fn_valor_caract_identif
                                       (r_inf_tv_acti_otros.identificador_id,
                                        38
                                       );

               IF v_direccion_iden_base = v_direccion_iden THEN
                  BEGIN
                     SELECT estado
                       INTO v_estado_identif
                       FROM fnx_identificadores
                      WHERE identificador_id = v_identificador_comparte;

                     IF (NVL (v_estado_identif, 'N') = 'OCU') THEN
                        EXIT;
                     END IF;
                  EXCEPTION
                     WHEN OTHERS THEN
                        NULL;
                  END;
               END IF;
            -->
            END IF;
         END LOOP;

         -- 2012-07-10 Jrendbr Se agrega validacion para verificar red en tramite y crear el identificador que comparte
         -- la infraestructura
         IF (v_identificador_comparte IS NULL) THEN
            v_inf_tv_tramite_solic := NULL;

            OPEN c_inf_tv_tramite_solic (p_identificador_busq,
                                         p_pedido,
                                         p_subpedido,
                                         p_solicitud
                                        );

            FETCH c_inf_tv_tramite_solic
             INTO v_inf_tv_tramite_solic;

            CLOSE c_inf_tv_tramite_solic;

            FOR r_inf_tv_tram_pedidos IN
               c_inf_tv_pedidos_tram
                       (p_identificador_busq,
                        v_inf_tv_tramite_solic.centro_distribucion_intermedia,
                        v_inf_tv_tramite_solic.nodo_optico_electrico_id,
                        v_inf_tv_tramite_solic.amplificador_id,
                        v_inf_tv_tramite_solic.tipo_amplificador_id,
                        v_inf_tv_tramite_solic.tipo_derivador_id,
                        v_inf_tv_tramite_solic.derivador_id,
                        v_inf_tv_tramite_solic.terminal_derivador_id
                       )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto
                                      (r_inf_tv_tram_pedidos.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                       r_inf_tv_tram_pedidos.identificador_id;

                  -- 2014-06-19.  REQ 36900 GPON Hogares.  [ 001-c_inf_tv_pedidos_tram ]
                  -- En caso que se haya encontrado un identificador que comparte infraestructura, se abandona el ciclo (EXIT) para que
                  -- en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ), el ciclo no contin�e
                  -- evaluando los registros restantes.
                  EXIT;

               END IF;
            END LOOP;
         END IF;

         --2010-08-04 Etachej Se valida que comparta infraestructura con un producto del mismo pedido que ya est� instalada
         IF (v_identificador_comparte IS NULL) THEN
            v_inf_tv_tramite_solic := NULL;

            OPEN c_inf_tv_tramite_solic (p_identificador_busq,
                                         p_pedido,
                                         p_subpedido,
                                         p_solicitud
                                        );

            FETCH c_inf_tv_tramite_solic
             INTO v_inf_tv_tramite_solic;

            CLOSE c_inf_tv_tramite_solic;

            FOR r_inf_tv_acti_otros IN
               c_inf_tv_activa_otrosid
                       (p_identificador_busq,
                        v_inf_tv_tramite_solic.centro_distribucion_intermedia,
                        v_inf_tv_tramite_solic.nodo_optico_electrico_id,
                        v_inf_tv_tramite_solic.amplificador_id,
                        v_inf_tv_tramite_solic.tipo_amplificador_id,
                        v_inf_tv_tramite_solic.tipo_derivador_id,
                        v_inf_tv_tramite_solic.derivador_id,
                        v_inf_tv_tramite_solic.terminal_derivador_id
                       )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto (r_inf_tv_acti_otros.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                         r_inf_tv_acti_otros.identificador_id;

                  -- 2014-06-19.  REQ 36900 GPON Hogares. [ 002-c_inf_tv_activa_otrosid ]
                  -- En caso que se haya encontrado un identificador que comparte infraestructura, se abandona el ciclo (EXIT) para que
                  -- en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ), el ciclo no contin�e
                  -- evaluando los registros restantes.
                  EXIT;

               END IF;
            END LOOP;
         END IF;

         --2010-08-04 Etachej Se valida que comparta infraestructura con un producto del mismo pedido que ya est� instalada
         IF (v_identificador_comparte IS NULL) THEN
            v_inf_tv_tramite_solic := NULL;

            OPEN c_inf_tv_tramite_solic (p_identificador_busq,
                                         p_pedido,
                                         p_subpedido,
                                         p_solicitud
                                        );

            FETCH c_inf_tv_tramite_solic
             INTO v_inf_tv_tramite_solic;

            CLOSE c_inf_tv_tramite_solic;

            FOR r_inf_tv_tramites_3 IN
               c_inf_tv_tram_enpedido
                       (p_identificador_busq,
                        v_inf_tv_tramite_solic.centro_distribucion_intermedia,
                        v_inf_tv_tramite_solic.nodo_optico_electrico_id,
                        v_inf_tv_tramite_solic.amplificador_id,
                        v_inf_tv_tramite_solic.tipo_amplificador_id,
                        v_inf_tv_tramite_solic.tipo_derivador_id,
                        v_inf_tv_tramite_solic.derivador_id,
                        v_inf_tv_tramite_solic.terminal_derivador_id,
                        p_pedido
                       )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto (r_inf_tv_tramites_3.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                         r_inf_tv_tramites_3.identificador_id;
               END IF;
            END LOOP;
         END IF;
      END IF;                              -- FIN IF p_tecnologia_busq = 'HFC'

      -- 2014-06-11 REQ36900 GPON Hogares.
      -- Implementaci�n de l�gica para buscar reuso de Infraestructura para tecnolog�a GPON.
      IF p_tecnologia_busq = 'GPON' THEN
         OPEN c_inf_gpon_activas (p_identificador_busq);

         FETCH c_inf_gpon_activas
          INTO v_inf_gpon_activas;

         CLOSE c_inf_gpon_activas;

         FOR r_inf_gpon_acti_otros IN
            c_inf_gpon_activa_otrosid (p_identificador_busq,
                                       v_inf_gpon_activas.olt_id,
                                       v_inf_gpon_activas.armario_id,
                                       v_inf_gpon_activas.municipio_id,
                                       v_inf_gpon_activas.splitter_id,
                                       v_inf_gpon_activas.nap_id,
                                       v_inf_gpon_activas.hilo_id,
                                       v_inf_gpon_activas.tarjeta_id,
                                       v_inf_gpon_activas.puerto_id,
                                       v_inf_gpon_activas.pto_logico
                                      )
         LOOP
            v_identif_servicio := NULL;
            v_identif_producto := NULL;

            OPEN c_identif_producto (r_inf_gpon_acti_otros.identificador_id);

            FETCH c_identif_producto
             INTO v_identif_servicio, v_identif_producto;

            CLOSE c_identif_producto;

            IF     (NVL (v_identif_servicio, 'N') = p_servicio)
               AND (NVL (v_identif_producto, 'N') = p_producto) THEN
               v_identificador_comparte :=
                                       r_inf_gpon_acti_otros.identificador_id;
               v_direccion_iden_base := NULL;
               v_direccion_iden := NULL;
               v_direccion_iden_base :=
                           fn_valor_caract_identif (p_identificador_busq, 38);
               v_direccion_iden :=
                  fn_valor_caract_identif
                                     (r_inf_gpon_acti_otros.identificador_id,
                                      38
                                     );

               IF v_direccion_iden_base = v_direccion_iden THEN
                  BEGIN
                     SELECT estado
                       INTO v_estado_identif
                       FROM fnx_identificadores
                      WHERE identificador_id = v_identificador_comparte;

                     IF (NVL (v_estado_identif, 'N') = 'OCU') THEN
                        EXIT;
                     END IF;
                  EXCEPTION
                     WHEN OTHERS THEN
                        NULL;
                  END;
               END IF;
            END IF;
         END LOOP;

         IF (v_identificador_comparte IS NULL) THEN
            v_inf_gpon_tramites := NULL;

            OPEN c_inf_gpon_tram_solic (p_identificador_busq,
                                        p_pedido,
                                        p_subpedido,
                                        p_solicitud
                                       );

            FETCH c_inf_gpon_tram_solic
             INTO v_inf_gpon_tramites;

            CLOSE c_inf_gpon_tram_solic;

            FOR r_inf_gpon_pedi_tram IN
               c_inf_gpon_pedidos_tram (p_identificador_busq,
                                        v_inf_gpon_tramites.olt_id,
                                        v_inf_gpon_tramites.armario_id,
                                        v_inf_gpon_tramites.municipio_id,
                                        v_inf_gpon_tramites.splitter_id,
                                        v_inf_gpon_tramites.nap_id,
                                        v_inf_gpon_tramites.hilo_id,
                                        v_inf_gpon_tramites.tarjeta_id,
                                        v_inf_gpon_tramites.puerto_id,
                                        v_inf_gpon_tramites.pto_logico
                                       )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto (r_inf_gpon_pedi_tram.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                        r_inf_gpon_pedi_tram.identificador_id;

                  -- 2014-06-19.  REQ 36900 GPON Hogares. [ 003-c_inf_gpon_pedidos_tram ]
                  -- En caso que se haya encontrado un identificador que comparte infraestructura, se abandona el ciclo (EXIT) para que
                  -- en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ), el ciclo no contin�e
                  -- evaluando los registros restantes.
                  EXIT;

               END IF;
            END LOOP;
         END IF;

         IF (v_identificador_comparte IS NULL) THEN
            v_inf_gpon_tramites := NULL;

            OPEN c_inf_gpon_tram_solic (p_identificador_busq,
                                        p_pedido,
                                        p_subpedido,
                                        p_solicitud
                                       );

            FETCH c_inf_gpon_tram_solic
             INTO v_inf_gpon_tramites;

            CLOSE c_inf_gpon_tram_solic;

            FOR r_inf_gpon_acti_otros IN
               c_inf_gpon_activa_otrosid (p_identificador_busq,
                                          v_inf_gpon_tramites.olt_id,
                                          v_inf_gpon_tramites.armario_id,
                                          v_inf_gpon_tramites.municipio_id,
                                          v_inf_gpon_tramites.splitter_id,
                                          v_inf_gpon_tramites.nap_id,
                                          v_inf_gpon_tramites.hilo_id,
                                          v_inf_gpon_tramites.tarjeta_id,
                                          v_inf_gpon_tramites.puerto_id,
                                          v_inf_gpon_tramites.pto_logico
                                         )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto
                                      (r_inf_gpon_acti_otros.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                       r_inf_gpon_acti_otros.identificador_id;

                  -- 2014-06-19.  REQ 36900 GPON Hogares. [ 004-c_inf_gpon_activa_otrosid ]
                  -- En caso que se haya encontrado un identificador que comparte infraestructura, se abandona el ciclo (EXIT) para que
                  -- en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ), el ciclo no contin�e
                  -- evaluando los registros restantes.
                  EXIT;

               END IF;
            END LOOP;
         END IF;

         IF (v_identificador_comparte IS NULL) THEN
            v_inf_gpon_tramites := NULL;

            OPEN c_inf_gpon_tram_solic (p_identificador_busq,
                                        p_pedido,
                                        p_subpedido,
                                        p_solicitud
                                       );

            FETCH c_inf_gpon_tram_solic
             INTO v_inf_gpon_tramites;

            CLOSE c_inf_gpon_tram_solic;

            FOR r_inf_gpon_tram_enpedi IN
               c_inf_gpon_tram_enpedido (p_identificador_busq,
                                         v_inf_gpon_tramites.olt_id,
                                         v_inf_gpon_tramites.armario_id,
                                         v_inf_gpon_tramites.municipio_id,
                                         v_inf_gpon_tramites.splitter_id,
                                         v_inf_gpon_tramites.nap_id,
                                         v_inf_gpon_tramites.hilo_id,
                                         v_inf_gpon_tramites.tarjeta_id,
                                         v_inf_gpon_tramites.puerto_id,
                                         v_inf_gpon_tramites.pto_logico,
                                         p_pedido
                                        )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto
                                     (r_inf_gpon_tram_enpedi.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                      r_inf_gpon_tram_enpedi.identificador_id;

                  -- 2014-06-19.  REQ 36900 GPON Hogares. [ 005-c_inf_gpon_tram_enpedido ]
                  -- En caso que se haya encontrado un identificador que comparte infraestructura, se abandona el ciclo (EXIT) para que
                  -- en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ), el ciclo no contin�e
                  -- evaluando los registros restantes.
                  EXIT;

               END IF;
            END LOOP;
         END IF;
      END IF;                        -- FIN IF p_tecnologia_busq = 'GPON' THEN

      -- Validaci�n del estado del identificador obtenido para indicar s� se comparte Infraestructura.
      -- Solo se considera que comparte Infraestructura en caso de que encuentra Ocupado.
      IF (NOT b_traslado_servicio) AND (v_identificador_comparte IS NOT NULL) THEN
         BEGIN
            SELECT estado
              INTO v_estado_identif
              FROM fnx_identificadores
             WHERE identificador_id = v_identificador_comparte;

            IF (NVL (v_estado_identif, 'N') <> 'OCU') THEN
               v_identificador_comparte := NULL;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               v_identificador_comparte := NULL;
            WHEN OTHERS THEN
               NULL;
         END;
      END IF;
   ELSE
/* =================================================================================================
                                           TRASLADO DEL SERVICIO
   =================================================================================================
*/
      IF (p_tecnologia_busq = 'REDCO') THEN
         v_inf_red_tramite_solic := NULL;
         v_identif_servicio := NULL;
         v_identif_producto := NULL;

         OPEN c_inf_red_tramite_solic (p_identificador_busq,
                                       p_pedido,
                                       p_subpedido,
                                       p_solicitud
                                      );

         FETCH c_inf_red_tramite_solic
          INTO v_inf_red_tramite_solic;

         CLOSE c_inf_red_tramite_solic;

         --Se verifica si es red primaria DIRECTA
         IF INSTR (v_inf_red_tramite_solic.armario_id, 'D') = 1 THEN
            FOR r_inf_redes_directas IN
               c_inf_redes_directas
                                 (p_identificador_busq,
                                  v_inf_red_tramite_solic.nodo_conmutador_id,
                                  v_inf_red_tramite_solic.armario_id,
                                  v_inf_red_tramite_solic.strip_id,
                                  v_inf_red_tramite_solic.par_primario_id
                                 )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto (r_inf_redes_directas.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                        r_inf_redes_directas.identificador_id;

                  -->2012-07-27 JPULGARB (Se rompe el ciclo cuando se encuentra el primer identificador con
                   --el que se comparte infraestrcura de red Directa)
                  IF v_identificador_comparte IS NOT NULL THEN
                     EXIT;
                  END IF;
               END IF;
            END LOOP;
         ELSE
            FOR r_inf_redes_secundarias IN
               c_inf_redes_secundarias
                                 (p_identificador_busq,
                                  v_inf_red_tramite_solic.nodo_conmutador_id,
                                  v_inf_red_tramite_solic.armario_id,
                                  v_inf_red_tramite_solic.strip_id,
                                  v_inf_red_tramite_solic.par_primario_id,
                                  v_inf_red_tramite_solic.caja_id,
                                  v_inf_red_tramite_solic.par_secundario_id
                                 )
            LOOP
               v_identif_servicio := NULL;
               v_identif_producto := NULL;

               OPEN c_identif_producto
                                    (r_inf_redes_secundarias.identificador_id);

               FETCH c_identif_producto
                INTO v_identif_servicio, v_identif_producto;

               CLOSE c_identif_producto;

               IF     (NVL (v_identif_servicio, 'N') = p_servicio)
                  AND (NVL (v_identif_producto, 'N') = p_producto) THEN
                  v_identificador_comparte :=
                                     r_inf_redes_secundarias.identificador_id;

                  -- 2014-06-19.  REQ 36900 GPON Hogares. [ 006-c_inf_redes_secundarias ]
                  -- En caso que se haya encontrado un identificador que comparte infraestructura, se abandona el ciclo (EXIT) para que
                  -- en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ), el ciclo no contin�e
                  -- evaluando los registros restantes.
                  EXIT;

               END IF;
            END LOOP;
         END IF;
      END IF;

      IF (p_tecnologia_busq = 'HFC') THEN
         v_identif_servicio := NULL;
         v_identif_producto := NULL;
         v_inf_tv_tramite_solic := NULL;

         OPEN c_inf_tv_tramite_solic (p_identificador_busq,
                                      p_pedido,
                                      p_subpedido,
                                      p_solicitud
                                     );

         FETCH c_inf_tv_tramite_solic
          INTO v_inf_tv_tramite_solic;

         CLOSE c_inf_tv_tramite_solic;

         FOR r_inf_tv_acti_otros IN
            c_inf_tv_activa_otrosid
                       (p_identificador_busq,
                        v_inf_tv_tramite_solic.centro_distribucion_intermedia,
                        v_inf_tv_tramite_solic.nodo_optico_electrico_id,
                        v_inf_tv_tramite_solic.amplificador_id,
                        v_inf_tv_tramite_solic.tipo_amplificador_id,
                        v_inf_tv_tramite_solic.tipo_derivador_id,
                        v_inf_tv_tramite_solic.derivador_id,
                        v_inf_tv_tramite_solic.terminal_derivador_id
                       )
         LOOP
            v_identif_servicio := NULL;
            v_identif_producto := NULL;

            OPEN c_identif_producto (r_inf_tv_acti_otros.identificador_id);

            FETCH c_identif_producto
             INTO v_identif_servicio, v_identif_producto;

            CLOSE c_identif_producto;

            IF     (NVL (v_identif_servicio, 'N') = p_servicio)
               AND (NVL (v_identif_producto, 'N') = p_producto) THEN
               v_identificador_comparte :=
                                         r_inf_tv_acti_otros.identificador_id;


               -- 2014-06-19.  REQ 36900 GPON Hogares. [ 007-c_inf_tv_activa_otrosid ]
               -- En caso que se haya encontrado un identificador que comparte infraestructura, se abandona el ciclo (EXIT) para que
               -- en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ), el ciclo no contin�e
               -- evaluando los registros restantes.
               EXIT;

            END IF;
         END LOOP;
      END IF;                            -- FIN IF (p_tecnologia_busq = 'HFC')

      -- 2014-06-11 REQ36900 GPON Hogares.
      -- Implementaci�n de l�gica para buscar reuso de Infraestructura para tecnolog�a GPON.
      IF (p_tecnologia_busq = 'GPON') THEN

         v_identif_servicio := NULL;
         v_identif_producto := NULL;
         v_inf_gpon_tramites := NULL;

         OPEN c_inf_gpon_tram_solic (p_identificador_busq,
                                     p_pedido,
                                     p_subpedido,
                                     p_solicitud
                                    );

         FETCH c_inf_gpon_tram_solic
          INTO v_inf_gpon_tramites;

         CLOSE c_inf_gpon_tram_solic;

         FOR r_inf_gpon_acti_otros IN
            c_inf_gpon_activa_otrosid (p_identificador_busq,
                                       v_inf_gpon_tramites.olt_id,
                                       v_inf_gpon_tramites.armario_id,
                                       v_inf_gpon_tramites.municipio_id,
                                       v_inf_gpon_tramites.splitter_id,
                                       v_inf_gpon_tramites.nap_id,
                                       v_inf_gpon_tramites.hilo_id,
                                       v_inf_gpon_tramites.tarjeta_id,
                                       v_inf_gpon_tramites.puerto_id,
                                       v_inf_gpon_tramites.pto_logico
                                      )
         LOOP
            v_identif_servicio := NULL;
            v_identif_producto := NULL;

            OPEN c_identif_producto (r_inf_gpon_acti_otros.identificador_id);

            FETCH c_identif_producto
             INTO v_identif_servicio, v_identif_producto;

            CLOSE c_identif_producto;

            IF     (NVL (v_identif_servicio, 'N') = p_servicio)
               AND (NVL (v_identif_producto, 'N') = p_producto) THEN
               v_identificador_comparte :=
                                       r_inf_gpon_acti_otros.identificador_id;

               -- 2014-06-19.  REQ 36900 GPON Hogares.  [ 008-c_inf_tv_activa_otrosid ]
               -- En caso que se haya encontrado un identificador que comparte infraestructura, se abandona el ciclo (EXIT) para que
               -- en el caso de varios registros ( Paquetes Trios que comparten la misma Infraestructura ), el ciclo no contin�e
               -- evaluando los registros restantes.
               EXIT;

            END IF;
         END LOOP;
      END IF;

   END IF;

   RETURN v_identificador_comparte;
EXCEPTION
   WHEN OTHERS THEN
      RETURN v_identificador_comparte;
END;