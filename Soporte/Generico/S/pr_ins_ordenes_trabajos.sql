PROCEDURE       pr_ins_ordenes_trabajos(p_requerimiento      IN fnx_ordenes_trabajos.requerimiento_id%TYPE,
                                                                        p_etapa              IN fnx_ordenes_trabajos.etapa_id%TYPE,
                                                                        p_actividad          IN fnx_ordenes_trabajos.actividad_id%TYPE,
                                                                        p_secuencia          IN fnx_ordenes_trabajos.secuencia%TYPE,
                                                                        p_pedido             IN fnx_solicitudes.pedido_id%TYPE,
                                                                        p_subpedido          IN fnx_solicitudes.subpedido_id%TYPE,
                                                                        p_solicitud          IN fnx_solicitudes.solicitud_id%TYPE,
                                                                        p_tipo_solicitud     IN fnx_reportes_instalacion.tipo_solicitud%TYPE,
                                                                        p_servicio           IN fnx_requerimientos_trabajos.servicio_id%TYPE,
                                                                        p_producto           IN fnx_requerimientos_trabajos.producto_id%TYPE,
                                                                        p_tipo_elemento      IN fnx_solicitudes.tipo_elemento%TYPE,
                                                                        p_tipo_elemento_id   IN fnx_solicitudes.tipo_elemento_id%TYPE,
                                                                        p_identificador_ortr IN fnx_requerimientos_trabajos.identificador_id%TYPE,
                                                                        p_tecnologia_ortr    IN fnx_solicitudes.tecnologia_id%TYPE,
                                                                        p_ident_paquete      IN fnx_solicitudes.identificador_id%TYPE,
                                                                        p_naturaleza         IN fnx_fallas.falla_id%TYPE,
                                                                        p_funcion_enruta     IN fnx_actividades_productos.funcion_enruta%TYPE,
                                                                        p_usuario            IN fnx_ordenes_trabajos.usuario%TYPE,
                                                                        p_empresa            IN fnx_servicios_productos.empresa_id%TYPE,
                                                                        p_municipio          IN fnx_servicios_productos.municipio_id%TYPE) IS
    -- 2009-06-05  Jgomezve  Se cambia el llamado del procedimiento porque se unió
    --                      pr_verificar_mail_mante y pr_verificar_mail_mante_otro
    --                      en uno solo

    -- 2009-08-24 Ojaramip  Se modifica el concepto de la orden inicial cuando la
    --                      entrega del equipo en Movilidad se hace en la oficina
    --                      para este caso se generea en AGEN.
    -- 2009-10-16 Truizd    Se agrega el tipo solicitud TRASL para traslado de pquete

    --Datos de la orden que se calculan en este PL
    -- 2010-01-04 ysolarte    Se agrega busqueda de la carcaterística 2880 para paquetes, con el fin de
    --                        de corregir zonas de influencia

    -- 2010-01-25 ysolarte    Se agrega validación para la generación de la orden de la cola encuesta

    -- 2010-02-03 CVILLARE    Se agrega cursor para verificar si la instalación del Internet BA x Demanda
    --                        viene de una instalación de IPTV ó TOIP nuevo o es un cambio y solicitud
    --                        de alguno de estos productos.
    -- 2010-07-06 DOJEDAC     Se cambia todo el codigo que tiene que ver con la función FN_CONSULTAR_UEN,
    --                        para aplicar el llamado al nueva función FN_UEN_CALCULADA.
    -- 2010-08-25 JGLENA     1. Se modifica cursor c_valor que trae identificador para enrutamiento en paquetes de modo que traiga
    --                          subpedido_id y solicitud_id para poder encontrar la tecnología que corresponde a dicho identificador
    --                          en el evento en que la misma no se encuentre registrada en fnx_configuraciones_identif
    --                       2. Se agregan variables para recibir los valores adicionados en el cursor c_valor
    --                       3. Se modifica lógica para que busque la tecnología del identificador que se va a usar en enrutamiento
    --                          en el caso de los paquetes.
    --                       4. Se modifica apertura del cursor c_valor para adecuarse a nueva definicion
    --                       5. Se modifica SELECT INTO que busca identificador de ruta en traslado de paquetes tipo 1, 2, 3 y se incluyen subpedido_id y solicitud_id
    --                          para poder encontrar la tecnología del identificador que se va a usar en enrutamiento
    --                       6. Se agrega cursor c_componente_ruta para que trate de encontrar un ACCESP cuando el producto sea PQUETE y no se pueda ubicar un TO o TOIP
    --                       7. Se agrega IF para que busque un ACCESP cuando el producto sea PQUETE y no se pueda ubicar un TO o TOIP
    -- 2010-08-27 JGLENA     1. Se agrega variable para almacenar procedimiento de enrutamiento que debe usarse en el llamado
    --                          al procedimiento PKG_RUTAS_TRABAJOS.pr_ruta_principal
    --                       2. En el caso de los paquetes, si la tecnología del elemento TO, TOIP, ACCESP, INSHFC o INSIP que se use para enrutar
    --                          es HFC se debe usar el procedimiento de enrutar que corresponde a esta tecnología
    --                       3. Se modifica el llamado a pkg_rutas_trabajos.pr_ruta_principal para pasar la variable v_funcion_enruta
    --                          en lugar de p_funcion_enruta y se agrega traza
    -- 2010-08-30 1 JGLENA   1. En el caso de los paquetes si la tecnologia del elemento TO, TOIP, ACCESP, INSHFC o INSIP
    --                          que se use para enrutar es HFC, se debe usar el procedimiento de enrutar que corresponde a
    --                          esa tecnología. Se coloca valor fijo puesto que no se autorizó creación de dominio
    --                          para almacenar el nombre del procedimiento.
    --                       2. Se agrega traza para identificar que no se encontró elemento de acceso para paquete
    --  12/04/2011  j carval  1. para los C2 en modificacion se le quita la asignacion por defecto de la cola 'ANALI' separacion de colas fase II
    -- 11/06/2011  JGallg    Desaprov-Cambio: Para la cola RECEQ dejar la orden generada en PENDI-SINTE si en el municipio donde se hará
    --                       el retiro no hay infraestrucura adecuada para recoger equipos.
    -- 09/07/2011  JGallg    Desaprov-Estabiliza: Si la orden a generar tiene asociado un equipo obsoleto, marcar en REFERENCIA_ID
    --                       con el dato RECEQ_OBSOLETO=SI
    -- 26/07/2011 DTREJOS    Mejoras CCC - Restructuración Niveles de Atención de Daños - Se realizó cambio ya que para los elementos
    --                        con tecnología REDCO no estaba quedando en el concepto PPCCC
    -- 28/09/2011  JGallg    Programar de una vez las órdenes de trabajo RECEQ asociadas a equipos obsoletos.
    --06/10/2011 DTREJOS   Validación para una CCC lógica existente
    -- 26/12/2011 DTREJOS    Cambio para la validación de la existencia de CCC lógica
    -- 2011/12/12  YSOLARTE  LTE 4G Se modifica para reemplazar el proceso que inserta en el FNX_TAREAS_PLATAFORMAS por el proceso que inserta eventos
    -- 2012/04/24 Modificación para la UEN Calculada, para que retorne la UEN sin el guion
    -- 2012-05-24  YSOLARTE  LTE 4G Se reemplaza la función fn_dominio_descripcion por la función fn_valor_adi_parametros_ord de órdenes
    --2012-10-25 MAPERADO - Se busca la cola asignada a la CCC de fnx_causas_comunes para asocial los nuevos daños a la misma cola.
    --2012-12-03 JPULGARB - Se implementa nvl en el select into ya que el campo cola_id puede llegar null
    --2013-03-11    Yhernana    Se captura la dirección de recogida de equipo en los retiros,
    --                          para adicionarlo en la observación de la orden.
    --2013-03-11 Jcardal/Jtabarc LTE 4G servicios fijos - Se agrega condición para insertar las ordenes de entrega de equipos en oficina en concepto PROG
    --2013-04-25 YSOLARTE - Proyecto GPON Adición de parametro aplicación para insertar en la tabla bts_eventos en el campo aplicacion_id en el llamado al paquete PKG_INTEGRA_FENIX_BTS
    --2013-05-03 ABETANB - Se adicion validación del nodo al momento de hacer la inserción en comandos.
    --                      v_tipo_nodo IN ('AXE', 'FTM', 'FTX', 'NEC')
    --2013-08-13 JGLENA    REQ22579_GPON Se modifica el cursor copr y la lógica del llamado del mismo en reparaciones para tener en cuenta producto, servicio y
    --                     tecnología. En caso de que no se encuentren datos se retoma el comportamiento anterior.
    --2013-10-09 EARDILAC  REQ 28628 Tangibilizacion La orden de la actividad HDECM la  coloque en  concepto PPRG
    -- 2014-01-03 DTANGAHU REQ_Nuevo_Retir_Mejora Para el pedido de Retiro (si tiene en FNX_CARACTERISTICA_SOLICITUDES la característica 85 con valor 'DNR' y
    --                      tiene el valor del pedido de NUEVO en la característica 4941), se actualizaran las ordenes plataforma al concepto O-310 en FNX_ORDENES_TRABAJOS.
    -- 2014-02-17 JRENDBR   Se cambia el codigo para incluir en el cursor c_equipo_obsoleto el tipo elemento id DPS.
    -- 2014-02-24 Jrendbr Se adiciona logica para que tenga en cuenta los esato de los equipos NIR.
    --2014-05-05 JGLENA      REQ36900_GponHoga Se adiciona tecnologia GPON y funcion de enrutamiento correspondiente
    -- 2014-07-09 GSANTOS   ITPAM50300_NUEVO_RETIR. Se debe modificar el concepto O-310 por PDNR en los pedidos de DNR
    -- 2014-07-14 JGLENA   REQ36900_GponHoga Se adiciona manejo de error E4 devuelto por la función de UEN Calculada en caso de daños REFAL.
    -- 2014-07-16 MMESAGO -  REQ49946_MejoVozNube Compensación
    -- 2014-09-01   Yhernana    ITP66474_CambVeloEqui:Si hay cambio de velocidad igual o superior a 8 megas (característica 124)
    --                          y el cliente tenga instalado un equipo diferente a DOCSIS3.0, 
    --                          entonces se actualice la observación de la orden en curso 
    --                          adicionando  la siguiente marca: ** DOCSIS 3.0 **

    v_cola                 fnx_colas_trabajos.cola_id%TYPE;
    v_cola_inicial         fnx_localizaciones_fallas.cola_inicial%TYPE;
    v_actividad            fnx_ordenes_trabajos.actividad_id%TYPE;
    v_concepto             fnx_conceptos.concepto_id%TYPE;
    v_tipo_infraestructura fnx_actividades_ordenes.tipo_infraestructura%TYPE;
    --Areas de trabajo
    v_area_operativa       fnx_areas_operativas.area_operativa_id%TYPE;
    v_subzona              fnx_subzonas.subzona_id%TYPE;
    v_area_trabajo         fnx_areas_trabajo.area_trabajo_id%TYPE;
    v_ruta                 fnx_rutas_trabajos.ruta_trabajo_id%TYPE;
    v_nodo                 fnx_influencias_trabajos.influencia_id%TYPE;
    v_influencia           fnx_influencias_trabajos.influencia_id%TYPE;
    v_equipo               fnx_ordenes_trabajos.equipo_id%TYPE;
    v_secuencia_influencia fnx_ordenes_trabajos.secuencia_influencia%TYPE;
    v_tipo_nodo            fnx_ordenes_trabajos.tipo_nodo%TYPE;
    v_secuencia_minima     fnx_ordenes_trabajos.secuencia%TYPE;
    v_uso_servicio         fnx_ordenes_trabajos.uso_servicio%TYPE;
    v_tipo_nodo_orden      fnx_ordenes_trabajos.tipo_nodo%TYPE;
    v_observacion          fnx_ordenes_trabajos.observacion%TYPE;
    v_actividad_inicial    fnx_ordenes_trabajos.actividad_id%TYPE;
    v_permanencia          fnx_caracteristica_solicitudes.valor%TYPE;
    --Causas Comunes
    v_causa_comun    fnx_solicitudes.concepto_id%TYPE;
    v_causa_activa   fnx_causas_comunes.causa_id%TYPE;
    v_requerimiento  fnx_causas_comunes.requerimiento_id%TYPE;
    v_fecha_esperada fnx_causas_comunes.elemento_id%TYPE;
    v_tipo_paquete   fnx_trabajos_solicitudes.valor%TYPE;
    v_tipo_seg       fnx_trabajos_solicitudes.valor%TYPE;
    -- 06/10/2011  DTREJOS
    v_causa_comun_logic fnx_solicitudes.concepto_id%TYPE;
    --Otras Variables
    v_tipo_cliente    fnx_ordenes_trabajos.tipo_cliente%TYPE;
    v_producto_cambio fnx_solicitudes.producto_id%TYPE;
    v_procesar_cola   fnx_colas_trabajos.categoria2%TYPE;
    v_plataforma_cola fnx_colas_trabajos.categoria2%TYPE;
    v_cambio_prd      VARCHAR2(2);
    v_plan_empleados  VARCHAR2(2);
    v_mensaje         VARCHAR2(1000);
    v_cantidad        NUMBER(3,
                                     0);
    v_comppaq         fnx_caracteristica_solicitudes.valor%TYPE;
    --se crean porque en ATENC se debe modificar
    v_servicio           fnx_solicitudes.servicio_id%TYPE;
    v_producto           fnx_solicitudes.producto_id%TYPE;
    v_tipo_elemento      fnx_solicitudes.tipo_elemento%TYPE;
    v_tipo_elemento_id   fnx_solicitudes.tipo_elemento_id%TYPE;
    v_tecnologia_ortr    fnx_solicitudes.tecnologia_id%TYPE;
    v_identificador_ortr fnx_solicitudes.identificador_id%TYPE;
    v_identif_ruta       fnx_solicitudes.identificador_id%TYPE;
    v_tecno_ruta         fnx_solicitudes.tecnologia_id%TYPE;
    v_tipo_solicitud     fnx_reportes_instalacion.tipo_solicitud%TYPE;
    v_criterio           fnx_colas_productos.criterio%TYPE;
    v_valorcriterio      fnx_caracteristica_solicitudes.valor%TYPE;
    v_depto              fnx_caracteristica_solicitudes.valor%TYPE;
    v_cambio_prod        fnx_caracteristica_solicitudes.valor%TYPE;
    v_traslado_paq       fnx_caracteristica_solicitudes.valor%TYPE;
    --2009-06-05  Jgomezve  Declaracion variables
    v_fecha_mante fnx_ordenes_trabajos.fecha_entrega%TYPE := SYSDATE;
    v_hora_mante  fnx_ordenes_trabajos.hora_entrega%TYPE := to_char(SYSDATE,
                                                                                         'HH24:MI');
    -- JGallg - Para equipos obsoletos, programar de una vez
    v_fecha_entrega fnx_ordenes_trabajos.fecha_entrega%TYPE := NULL;
    v_hora_entrega  fnx_ordenes_trabajos.hora_entrega%TYPE := NULL;

    v_estado_orden  fnx_solicitudes.estado_id%TYPE;
    v_val_tel_encue VARCHAR2(2);
    -- 2010-02-03  CVILLARE   Variable de verificación Internet BA x Demanda
    v_cambio VARCHAR2(2);
    v_dem    fnx_caracteristica_solicitudes.valor%TYPE := 'NO';

    --  JGLENA    2010-08-25-   2. Se agregan variables para recibir los valores subpedido_id y solicitud_id
    --            adicionados en el cursor c_valor
    v_subpedido_ruta fnx_solicitudes.subpedido_id%TYPE;
    v_solicitud_ruta fnx_solicitudes.subpedido_id%TYPE;
    /*2011-12-05 YSOLARTE LTE 4G Varible para consultar el tipo de tareas*/
    v_tipo_tarea             fnx_productos.tipo_tarea%TYPE;
    v_codigo_evento          fnx_caracteristica_solicitudes.valor%TYPE;
    --Cmurillg 2013-02-14 Voz en la Nube se amplia tamaño de la variable
    --v_aplicacion             VARCHAR2 (10);
    --
    -- 2014-08-01   jrojasri    -- Req ITPAM41860_INTRAWAY
                                -- SE CORRIJE EL TIPO DE DATO DE LA VARIABLE APLICACION DE VARCHAR 10 AAL TIPO FNX_PARAMETROS_ORD.VALOR_ADICIONAL%TYPE
    --
    --v_aplicacion             VARCHAR2 (10);
    v_aplicacion   FNX_PARAMETROS_ORD.VALOR_ADICIONAL%TYPE;
    -- FIN  eq ITPAM41860_INTRAWAY
    --  JGLENA    2010-08-27-   1. Se agrega variable para almacenar procedimiento de enrutamiento que debe usarse
    --                          en el llamado al procedimiento PKG_RUTAS_TRABAJOS.pr_ruta_principal
    v_funcion_enruta fnx_actividades_productos.funcion_enruta%TYPE := p_funcion_enruta;

    -- CVILLARE 06/02/2013
    -- 22605 - Sinergia Hogares - Pymes
    -- Modificación de la función para devolver HG en caso que venga valor en la
    -- característica 4975 como EMP, el uso del servicio es RES y el cálculo de
    -- la UEN es C3
    v_valor_4975              fnx_caracteristica_solicitudes.valor%type;
    v_valor_2                 fnx_caracteristica_solicitudes.valor%type;
    -- FIN 22605 - Sinergia Hogares - Pymes
    v_error_bts NUMBER;
    v_mensaje_bts VARCHAR2(500);

/* 2013-08-13 JGLENA    REQ22579_GPON Se modifica el cursor copr y la lógica del llamado del mismo en reparaciones para tener en cuenta producto, servicio y
                                      tecnología. En caso de que no se encuentren datos se retoma el comportamiento anterior.
                                      se agrega variable para condicionar comportamiento del cursor
*/
/*    CURSOR c_copr IS
        SELECT actividad_id
          FROM fnx_colas_productos
         WHERE cola_id = v_cola
            AND rownum = 1;*/

    v_condicionar varchar2(1) := 'S';

    -- JVASCO    -    20131004    Variable para almacenar las observaciones de la tabla FNX_OBSERVACIONES_GENERALES
    --                            para el requerimiento servicios adicionales de internet.
    v_obse_gene            VARCHAR2(700);
    --2014-02-24    Jrendbr Creacion de variables equipos NIR
    v_equipo_nir           VARCHAR2(1);

    v_can_tangi   NUMBER(3); --2013-11-01 EARDILAC Variable creada para tangibilizacion
    CURSOR c_copr (p_cola fnx_colas_productos.cola_id%type, p_servicio fnx_colas_productos.servicio_id%type,  p_producto fnx_colas_productos.producto_id%type, p_tecnologia_ortr fnx_colas_productos.tecnologia_id%type, p_empresa fnx_colas_productos.empresa_id%type, p_municipio fnx_colas_productos.municipio_id%type, p_condicionar varchar2) IS
    SELECT actividad_id
      FROM fnx_colas_productos
    WHERE (p_condicionar = 'S' AND cola_id = p_cola AND
           servicio_id = p_servicio AND producto_id = p_producto AND
           empresa_id = p_empresa AND tecnologia_id = p_tecnologia_ortr AND
           tipo_trabajo = 'REFAL')
        OR (p_condicionar = 'N' AND cola_id = p_cola)
            AND rownum = 1;

    CURSOR c_iden_agru IS
        SELECT nvl(identificador_id,
                      p_identificador_ortr)
          FROM fnx_identificadores
         WHERE agrupador_id = p_identificador_ortr
            AND tipo_elemento = 'CMP'
            AND tipo_elemento_id = 'INSTA'
            AND fn_valor_caract_identif(identificador_id,
                                                 35) = fn_valor_caract_identif(p_identificador_ortr,
                                                                                         35)
            AND rownum = 1;

    CURSOR c_ortr IS
        SELECT COUNT(p_requerimiento)
          FROM fnx_ordenes_trabajos
         WHERE pedido_id = p_pedido
            AND etapa_id = p_etapa
            AND actividad_id = p_actividad;

    -- 2010-02-03 CVILLARE   Declaración de cursor de verificación Internet BA x Demanda
    CURSOR c_cambio IS
        SELECT 'SI'
          FROM fnx_trabajos_solicitudes
         WHERE pedido_id = p_pedido
            AND producto_id IN ('TO',
                                      'TELEV')
            AND tipo_trabajo = 'NUEVO'
            AND caracteristica_id = 1
            AND rownum = 1;

    --  OJARAMIP  2010-04-08- corrección para identificar TO en paquetes
    --  JGLENA    2010-08-25- 1. Se modifica cursor c_valor que trae identificador para enrutamiento en paquetes de modo que traiga
    --                        subpedido_id y solicitud_id para poder encontrar la tecnología que corresponde a dicho identificador
    --                        en el evento en que la misma no se encuentre registrada en fnx_configuraciones_identif
    CURSOR c_valor(p_pedido fnx_solicitudes.pedido_id%TYPE) IS
        SELECT valor,
                 subpedido_id,
                 solicitud_id
          FROM fnx_caracteristica_solicitudes
         WHERE producto_id = 'TO'
            AND pedido_id = p_pedido
            AND tipo_elemento_id IN ('TO',
                                             'TOIP')
            AND caracteristica_id = 1
         ORDER BY tipo_elemento_id DESC;

    --  JGLENA    2010-08-25- 6. Se agrega cursor c_componente_ruta que trae identificador, subpedido_id y solicitud_id para enrutamiento en paquetes
    --                        cuando no exista un componete TO o TOIP. Estos datos se usan posteriormente para poder encontrar la tecnología
    --                        que corresponde a dicho identificador, en el evento en que la misma no se encuentre registrada en fnx_configuraciones_identif
    CURSOR c_componente_ruta(p_pedido fnx_solicitudes.pedido_id%TYPE) IS
        SELECT valor,
                 subpedido_id,
                 solicitud_id
          FROM fnx_caracteristica_solicitudes
         WHERE producto_id IN ('INTER',
                                      'TELEV')
            AND pedido_id = p_pedido
            AND tipo_elemento_id IN ('ACCESP',
                                             'INSIP',
                                             'INSHFC')
            AND caracteristica_id = 1
         ORDER BY tipo_elemento_id DESC;

    -- GSANTOS 15/06/2010
    -- PBX POTS
    -- SE ADICIONA PARA EVALUAR SI VIENE SOLA LA
    -- TRONCAL COLECTIVA(ADICION) O SI VIENE CON SEÑAL
    -- LLAMADA(NUEVO)
    CURSOR c_tc_pots IS
        SELECT MIN(secuencia) FROM fnx_ordenes_trabajos WHERE pedido_id = p_pedido;

    v_min_sec NUMBER(3);

    CURSOR c_sll_pots IS
        SELECT COUNT(*)
          FROM fnx_ordenes_trabajos
         WHERE pedido_id = p_pedido
            AND secuencia = v_min_sec
            AND concepto_id = 'CUMPL';

    v_sll_cumpl NUMBER(3);
    -- GSANTOS 26-01-2011
    -- SE UTILIZA PARA VERIFICAR SI DEBE O NO CAMBIAR LA COLA
    -- PARA LA FALLA, PRODUCTO Y TECNOLOGIA EN CUESTION
    CURSOR c_criterio_valor IS
        SELECT criterio,
                 valor
          FROM fnx_localizaciones_fallas
         WHERE servicio_id = p_servicio
            AND producto_id = p_producto
            AND falla_id = p_naturaleza
            AND tecnologia_id = p_tecnologia_ortr
            AND empresa_id = p_empresa
            AND municipio_id = p_municipio
            AND rownum = 1;

    -- JGallg - 2011-07-09 - Desaprov-Estabiliza: Cursor para determinar equipo obsoleto
    -- JGallg - 2012-03-09 - MejorasDesaprov:     Obtener dato del equipo con todos sus datos clave.
    -- 2014-02-17 JRENDBR   Se cambia el codigo para incluir en el cursor c_equipo_obsoleto el tipo elemento id DPS.
    CURSOR c_equipo_obsoleto IS
        SELECT t.equipo_id valor, decode(nvl(fn_consultar_equipo_obsoleto (t.marca_id, t.referencia_id, b.tipo_elemento_id, b.tipo_elemento), 'N'), 'N', NULL, 'RECEQ_OBSOLETO=SI') marca_obsoleto,
               t.marca_id,
               t.referencia_id,
               b.tipo_elemento_id,
               b.tipo_elemento
        FROM
        (
            SELECT (
                SELECT valor FROM fnx_trabajos_solicitudes
                WHERE pedido_id    = p_pedido
                  AND subpedido_id = p_subpedido
                  AND solicitud_id = p_solicitud
                  AND caracteristica_id = 960
            ) marca_id, (
                SELECT valor FROM fnx_trabajos_solicitudes
                WHERE pedido_id    = p_pedido
                  AND subpedido_id = p_subpedido
                  AND solicitud_id = p_solicitud
                  AND caracteristica_id = 982
            ) referencia_id, (
                SELECT valor FROM fnx_trabajos_solicitudes
                WHERE pedido_id    = p_pedido
                  AND subpedido_id = p_subpedido
                  AND solicitud_id = p_solicitud
                  AND caracteristica_id = 200
            ) equipo_id
            FROM dual
        ) t, fnx_equipos b
        WHERE t.marca_id      = b.marca_id
          AND t.referencia_id = b.referencia_id
          AND t.equipo_id     = b.equipo_id
          AND b.tipo_elemento_id IN ('CABLEM', 'CPE', 'CPEWIM', 'ANTENA', 'DECO', 'STBOX', 'ATA', 'DPS');

    rg_equipo_obsoleto c_equipo_obsoleto%ROWTYPE;  -- JGallg - Desaprov-Estabiliza

    /* 2011/12/05 LTE YSOLARTE Se agrega cursor para consultar el tipo de tarea de FNX_PRODUCTOS */
    /* Traer configuración de FNX_PRODUCTOS para determinar si inserta tareas o eventos*/
    CURSOR c_tipotarea
    IS
       SELECT tipo_tarea
         FROM fnx_productos
        WHERE producto_id = p_producto;

    v_criterio_l_fallas fnx_localizaciones_fallas.criterio%TYPE;
    v_valor_l_fallas    fnx_localizaciones_fallas.valor%TYPE;
    -- FIN GSANTOS 26-01-2011


     --2013-03-11    Yhernana    Se captura la dirección de recogida de equipo en los retiros,
     --                          para adicionarlo en la observación de la orden.
     v_direccion_recogida   fnx_caracteristica_solicitudes.valor%TYPE;

     --2013-10-04   JVASCO    -    Cursor para traer las observaciones de la tabla FNX_OBSERVACIONES_GENERALES
    CURSOR c_obsegene
    IS
       SELECT observacion
         FROM fnx_observaciones_generales
        WHERE pedido_id = p_pedido
          AND subpedido_id = p_subpedido
          AND solicitud_id = p_solicitud;

   --- 2013-11-01 EARDILAC Incio Cursor creado para que el pedido sea de tangibilizacion
    CURSOR c_tangibilizacion
    IS
       SELECT COUNT(1)
       FROM  FNX_CARACTERISTICA_SOLICITUDES
       WHERE PEDIDO_ID= p_pedido
       and caracteristica_id = 1093
       and  valor = 'CM';
    --- 2013-11-01 EARDILAC Incio Cursor creado para que el pedido sea de tangibilizacion
    
    -- 2014-09-01   Yhernana    ITP66474_CambVeloEqui: INI declaración variables y cursores
    CURSOR c_equipo (p_identificador FNX_SOLICITUDES.IDENTIFICADOR_ID%TYPE) IS
        SELECT tipo_elemento_id, equipo_id, marca_id, referencia_id
        FROM   fnx_equipos
        WHERE  identificador_id = p_identificador
          AND  estado = 'OCU';

    v_equipo_actual     C_EQUIPO%ROWTYPE;
    v_docsis            FNX_MARCAS_REFERENCIAS.TIPO_DOCSIS%TYPE;
    v_vel_tipo_docsis   NUMBER;
    v_tipo_docsis_impr  VARCHAR2(500);
    -- 2014-09-01   Yhernana    ITP66474_CambVeloEqui: FIN declaración variables     
    
    --  dmartinez** -           
    v_observacion_receq     VARCHAR2(700);   
    --  dmartinez**
    
BEGIN
    --DBMS_OUTPUT.put_line ('pr_ins_ordenes_trabajos '||p_identificador_ortr);
    -- Identificar el tipo de infraestructura asociada a la actividad del la orden para definir
    -- que tipo de infraestructura buscar (activa o en tramite)
    dbms_output.put_line('entro a: FENIX.pr_ins_ordenes_trabajos ');
    v_tipo_paquete := fn_valor_caract_subpedido(p_pedido,
                                                              1,
                                                              '2878');

    -- CVILLARE 06/02/2013
    -- 22605 - Sinergia Hogares - Pymes
    -- Modificación de la función para devolver HG en caso que venga valor en la
    -- característica 4975 como EMP, el uso del servicio es RES y el cálculo de
    -- la UEN es C3
    v_valor_4975 := coalesce(
                              fn_valor_caract_subpedido (p_pedido, p_subpedido, 4975),
                              fn_valor_caract_identif (p_identificador_ortr, 4975),
                              fn_valor_caract_identif (p_identificador_ortr||'-IC001', 4975)
                            );
    v_valor_2 := coalesce(
                           fn_valor_caract_subpedido (p_pedido, p_subpedido, 2),
                           fn_valor_caract_identif (p_identificador_ortr, 2),
                           fn_valor_caract_identif (p_identificador_ortr||'-IC001', 2)
                         );
    -- FIN 22605 - Sinergia Hogares - Pymes

    IF v_tipo_paquete IS NULL THEN
        BEGIN
            SELECT valor
              INTO v_tipo_paquete
              FROM fnx_caracteristica_solicitudes
             WHERE pedido_id = p_pedido
                AND producto_id = 'PQUETE'
                AND caracteristica_id = 2878
                AND rownum = 1;
        EXCEPTION
            WHEN OTHERS THEN
                v_tipo_paquete := NULL;
        END;
    END IF;

    pr_datos_cliente(fn_cliente_pedido(p_pedido),
                          v_tipo_cliente);
    v_tipo_infraestructura := fn_verificar_infraestructura(p_etapa,
                                                                             p_actividad);

    IF p_tipo_elemento_id = 'DECO' AND v_tipo_infraestructura = 'TRM' THEN
        v_tipo_infraestructura := 'ACT';
    END IF;

    --DBMS_OUTPUT.put_line ('****** <pr_ins_ordenes_trabajos*** v_tipo_paquete 1 ' || v_tipo_paquete || 'v_tipo_infraestructura ' || v_tipo_infraestructura);
    IF v_tipo_infraestructura = 'TRM' THEN
        IF p_producto NOT IN ('LP',
                                     'PCM',
                                     '2MB') THEN
            IF p_tecnologia_ortr IN ('CDMA',
                                             'GSM') THEN
                v_tipo_nodo := fn_tipo_central_tram_ord(p_pedido,
                                                                     p_subpedido,
                                                                     p_solicitud,
                                                                     'INALA');
            ELSE
                v_tipo_nodo := fn_tipo_central_tram_ord(p_pedido,
                                                                     p_subpedido,
                                                                     p_solicitud,
                                                                     p_tecnologia_ortr);
            END IF;

            IF v_tipo_nodo IS NULL THEN
                v_tipo_nodo := fn_tipo_central_inf_act(p_identificador_ortr);
            END IF;
        END IF;
    ELSIF v_tipo_infraestructura = 'ACT' THEN
        IF p_producto NOT IN ('LP',
                                     'PCM',
                                     '2MB') THEN
            v_tipo_nodo := fn_tipo_central_inf_act(p_identificador_ortr);
        END IF;
    END IF;

    IF p_producto IN ('INTER',
                            'INTEMP') --OR v_producto IN ('INTER', 'INTEMP')
     THEN
        v_tipo_nodo := fn_tipo_nodo_internet(p_pedido,
                                                         p_subpedido,
                                                         p_solicitud,
                                                         p_tipo_elemento_id);
    END IF;

    -- OJARAMIP 06172009 SE DEBE QUITAR LA CARACTERISTICA 3757 DEL PORTAFOLIO
    /*v_tipo_seg :=    coalesce (fn_valor_caract_subpedido (p_pedido, p_subpedido, 3757)
                             , fn_valor_caract_identif (p_identificador_ortr, 3757)
                             , fn_valor_caract_identif (fn_valor_caract_identif(p_identificador_ortr,130), 3757)

                         );
       if  v_tipo_seg is null then
        v_tipo_seg := nvl(fn_valor_caract_identif (fn_valor_caract_subpedido (p_pedido, p_subpedido, 90), 3757)
                          , fn_valor_caract_identif (fn_valor_caract_subpedido (p_pedido, p_subpedido,130), 3757));
       end if;
   */
    /*
   DOJEDAC 06/07/2010 Se quita el codigo de la función actual FN_CONSULTAR_UEN, para dar paso a la nueva
                      función FN_UEN_CALCULADA.*/

    /*
   v_tipo_seg := SUBSTR (fn_consultar_uen (p_pedido, p_subpedido, NULL), 1, 2);

   IF INSTR (v_tipo_seg, 'No') > 0 OR
      INSTR (v_tipo_seg, 'Er') > 0 OR v_tipo_seg IS NULL
   THEN
      v_tipo_seg := SUBSTR (fn_consultar_uen (NULL, NULL, p_identificador_ortr), 1, 2);
   END IF;

   IF INSTR (v_tipo_seg, 'No') > 0 OR
      INSTR (v_tipo_seg, 'Er') > 0 OR v_tipo_seg IS NULL
   THEN
      v_tipo_seg := SUBSTR
            (fn_consultar_uen
                         (NULL,
                          NULL,
                          NVL (fn_valor_caract_identif (p_identificador_ortr,
                                                        2126
                                                       ),
                               fn_identificador_subpedido (p_pedido,
                                                           p_subpedido
                                                          )
                              )
                         ),
             1,
             2
            );
   END IF;

   IF INSTR (v_tipo_seg, 'No') > 0 OR
      INSTR (v_tipo_seg, 'Er') > 0 OR v_tipo_seg IS NULL
   THEN
      v_tipo_seg := 'HG-';
   END IF;
   */

    --DOJEDAC 06/07/2010 Se implementa la nueva función FN_UEN_CALCULADA.

    v_tipo_seg := substr(fn_uen_calculada(p_pedido,
                                                      p_subpedido,
                                                      p_solicitud,
                                                      NULL),
                                1,
                                2);

    -- CVILLARE 06/02/2013
    -- 22605 - Sinergia Hogares - Pymes
    -- Modificación de la función para devolver HG en caso que venga valor en la
    -- característica 4975 como EMP, el uso del servicio es RES y el cálculo de
    -- la UEN es C3
    if v_tipo_seg = 'C3' and nvl(v_valor_4975, 'HOG') = 'EMP' and v_valor_2 = 'RES' then
      v_tipo_seg := 'HG';
    end if;
    -- FIN 22605 - Sinergia Hogares - Pymes
    /* 2014-07-14 JGLENA REQ36900_GPONHoga se agrega condicion para que busque segmento en danos que devuelven error E4 */
    IF instr(v_tipo_seg,
                'NA') > 0 OR instr(v_tipo_seg,
                                         'E1') > 0 OR instr(v_tipo_seg,
                                                                  'E2') > 0 OR instr(v_tipo_seg,
                                                                                            'E3') > 0 OR
        instr(v_tipo_seg,
                'E5') > 0 OR v_tipo_seg IS NULL OR (instr(v_tipo_seg,
                                         'E4') > 0 AND p_etapa = 'MANTE' AND p_tipo_solicitud <> 'REGEN' ) THEN
        v_tipo_seg := substr(fn_uen_calculada(NULL,
                                                          NULL,
                                                          NULL,
                                                          p_identificador_ortr),
                                    1,
                                    2);

        -- CVILLARE 06/02/2013
        -- 22605 - Sinergia Hogares - Pymes
        -- Modificación de la función para devolver HG en caso que venga valor en la
        -- característica 4975 como EMP, el uso del servicio es RES y el cálculo de
        -- la UEN es C3
        if v_tipo_seg = 'C3' and nvl(v_valor_4975, 'HOG') = 'EMP' and v_valor_2 = 'RES' then
          v_tipo_seg := 'HG';
        end if;
        -- FIN 22605 - Sinergia Hogares - Pymes



    END IF;

    /* 2014-07-14 JGLENA REQ36900_GPONHoga se agrega condicion para que busque segmento en danos que devuelven error E4 */
    IF instr(v_tipo_seg,
                'NA') > 0 OR instr(v_tipo_seg,
                                         'E1') > 0 OR instr(v_tipo_seg,
                                                                  'E2') > 0 OR instr(v_tipo_seg,
                                                                                            'E3') > 0 OR
        instr(v_tipo_seg,
                'E5') > 0 OR v_tipo_seg IS NULL OR (instr(v_tipo_seg,
                                         'E4') > 0 AND p_etapa = 'MANTE' AND p_tipo_solicitud <> 'REGEN' ) THEN
        v_tipo_seg := substr(fn_uen_calculada(NULL,
                                                          NULL,
                                                          NULL,
                                                          nvl(fn_valor_caract_identif(p_identificador_ortr,
                                                                                                2126),
                                                                fn_identificador_subpedido(p_pedido,
                                                                                                    p_subpedido))),
                                    1,
                                    2);

        -- CVILLARE 06/02/2013
        -- 22605 - Sinergia Hogares - Pymes
        -- Modificación de la función para devolver HG en caso que venga valor en la
        -- característica 4975 como EMP, el uso del servicio es RES y el cálculo de
        -- la UEN es C3
        if v_tipo_seg = 'C3' and nvl(v_valor_4975, 'HOG') = 'EMP' and v_valor_2 = 'RES' then
          v_tipo_seg := 'HG';
        end if;
        -- FIN 22605 - Sinergia Hogares - Pymes

    END IF;

 /*24/04/2012 SBUITRAB   Modificación para la UEN Calculada, para que retorne la UEN sin el guion*/
    IF instr(v_tipo_seg,'NA') > 0
    OR instr(v_tipo_seg,'E1') > 0
    OR instr(v_tipo_seg,'E2') > 0
    OR instr(v_tipo_seg,'E3') > 0
    OR instr(v_tipo_seg,'E5') > 0
    OR v_tipo_seg IS NULL THEN
        v_tipo_seg := 'HG';
    END IF;

    -----------------------------------------------------------------------------

    --JGOMEZVE Julio 06-2009 se pasa logia a Funcion  FN_CRITERIO_COLA
    v_valorcriterio  := fn_criterio_cola(p_pedido,
                                                     p_subpedido,
                                                     p_solicitud,
                                                     p_identificador_ortr,
                                                     p_tecnologia_ortr,
                                                     p_producto,
                                                     p_actividad,
                                                     p_tipo_solicitud,
                                                     v_tipo_seg,
                                                     v_criterio);
    v_tipo_solicitud := p_tipo_solicitud;
    /*  IF p_tipo_solicitud = 'NUEVO'
     AND p_etapa = 'INSTA'
     AND p_actividad = 'INSTA' THEN
       v_tipo_solicitud := NVL(fn_tipo_solicitud(p_pedido,p_subpedido,p_solicitud),p_tipo_solicitud);
     END IF;
   */
    v_cola            := fn_cola(p_servicio,
                                          p_producto,
                                          p_tecnologia_ortr,
                                          v_tipo_solicitud,
                                          p_etapa,
                                          p_actividad,
                                          p_empresa,
                                          p_municipio,
                                          v_criterio,
                                          v_valorcriterio);
    v_procesar_cola   := fn_procesar_cola(v_cola);
    v_plataforma_cola := fn_plataforma_cola(v_cola);

    dbms_output.put_line('Antes de ingresar a = MANTE y <> REGEN. v_cola: '||v_cola);

    --  dbms_output.put_line('despues acceso p_tecnologia_ortr ' || p_tecnologia_ortr);
    IF p_etapa = 'MANTE' AND p_tipo_solicitud <> 'REGEN' THEN
        ----07-01-2011 GSANTOS JGALLG SE TIENE EN CUENTA EL PRODUCTO 3PLAY
        -- GSANTOS 30-12-2010
        -- SE MODIFICA PORQUE PARA LOS CLIENTES C2 Y C3 YA NO APLICA QUE
        -- SEGUN UNA FALLA_ID SE TENGA AMARRADO A UNA COLA INCIAL SEGUN LA TABLA
        -- DE FNX_LOCALIZACIONES_FALLAS. SE DESARROLLO SEPARACION DE COLAS DE
        -- MANTENIMIENTO PARA C2 Y C3, LUEGO NO APLICA LA FUNCION FN_COLA_INICIAL_MTO

        OPEN c_criterio_valor;
        FETCH c_criterio_valor
            INTO v_criterio_l_fallas,
                  v_valor_l_fallas;
        CLOSE c_criterio_valor;


    --24-04-2012 SBUITRAB MODIFICACIOENS DEPENDENCIAS UEN_CALCULADA
    v_tipo_seg :=  fn_uen_calculada(NULL, NULL, NULL, p_identificador_ortr);

      /* 2014-07-14 JGLENA REQ36900_GPONHoga se agrega condicion para que reemplace segmento en danos */
      IF v_tipo_seg = 'C3' and nvl(v_valor_4975, 'HOG') = 'EMP' AND v_valor_2 = 'RES' THEN
        v_tipo_seg := 'HG';
      END IF;
        dbms_output.put_line('v_criterio_l_fallas: '||v_criterio_l_fallas||' v_valor_l_fallas: '||v_valor_l_fallas||' v_tipo_seg :'||v_tipo_seg);


    --24-04-2012 SBUITRAB MODIFICACIOENS DEPENDENCIAS UEN_CALCULADA
    --SE REEMPLAZA EL LLAMADO A LA FUNCION UEN_CALCULADA Y SE TOMA EL VALOR DE V_TIPO_SEG
     IF v_tipo_seg NOT IN ('C2', 'C3')
      THEN

            --MARISTIM  JULIO 06-2009 esto aplica solo para MANTE
            v_cola_inicial := fn_cola_inicial_mto(p_servicio,
                                                              p_producto,
                                                              p_tecnologia_ortr,
                                                              p_naturaleza,
                                                              p_empresa,
                                                              p_municipio);
                dbms_output.put_line('NO es C2 C3. v_cola_inicial :'||v_cola_inicial);

        --24-04-2012 SBUITRAB MODIFICACIOENS DEPENDENCIAS UEN_CALCULADA
    --SE REEMPLAZA EL LLAMADO A LA FUNCION UEN_CALCULADA Y SE TOMA EL VALOR DE V_TIPO_SEG
      ELSIF v_tipo_seg IN ('C2', 'C3')
            AND v_criterio_l_fallas = 'C'
            AND
     -- 'C' conserva el valor de la cola de la tabla fnx_localizaciones_fallas
                INSTR (v_valor_l_fallas,
    --SE REEMPLAZA EL LLAMADO A LA FUNCION UEN_CALCULADA Y SE TOMA EL VALOR DE V_TIPO_SEG
                       v_tipo_seg,
                       1,
                       1
                      ) > 0
      THEN
            --MARISTIM  JULIO 06-2009 esto aplica solo para MANTE
            v_cola_inicial := fn_cola_inicial_mto(p_servicio,
                                                              p_producto,
                                                              p_tecnologia_ortr,
                                                              p_naturaleza,
                                                              p_empresa,
                                                              p_municipio);
                dbms_output.put_line('Es C2 C3 y criterio C. v_cola_inicial :'||v_cola_inicial);
        END IF;
        /*
      ELSE
          IF p_producto = '3PLAY' THEN
               -- John Carlos Gallego (john.gallego@mvm.com.co) JGallg - 2011-01-14
               -- Se invoca la función con los dos nuevos argumentos.
               v_cola_inicial := fn_cola_inicial_mto(p_servicio,
                                                     p_producto,
                                                     p_tecnologia_ortr,
                                                     p_naturaleza,
                                                     p_empresa,
                                                     p_municipio,
                                                     'SEG',
                                                     fn_uen_calculada(NULL, NULL, NULL, p_identificador_ortr));
          END IF;
      END IF;
      */
        -- FIN GSANTOS 30-12-2010
        --07-01-2011 GSANTOS JGALLG

        -- 26/07/2011 DTREJOS - Se agrego la tecnologia REDCO a la validación, para que la incluya a las CCC

        --Verificar si tiene una CCC asociada
        IF p_producto IN ('TO',
                                'PBX',
                                'DID') OR p_tecnologia_ortr IN ('HFC',
                                                                          'COA',
                                                                          'REDCO') THEN
            pr_verificar_ccc(p_identificador_ortr,
                                  v_causa_comun,
                                  v_causa_activa,
                                  v_requerimiento,
                                  v_fecha_esperada);
        END IF;

       dbms_output.put_line(' --  ' || p_pedido || '  * *' || p_producto);
        --16/O7/2014 MMESAGO -  REQ49946_MejoVozNube Compensación  *VOZNUB
        IF INSTR(FN_VALOR_PARAMETROS_ORD('COMPPRODLOGIC','COMPLOGIC')||',', p_producto  || ',') > 0 THEN

          pr_verificar_ccc_xprod( p_identificador_ortr,
                                    v_causa_comun,
                                    v_causa_activa,
                                    v_requerimiento
                                    );

        END IF;
        --16/O7/2014 MMESAGO -  EQ49946_MejoVozNube Compensación *7

        --06/10/2011 DTREJOS - Se agrego la validación si existe una CCC logica
        --26/12/2011 DTREJOS - Se cambio la validación por la naturaleza de fallas masivas
        IF p_naturaleza = 'F001' THEN
            pr_verificar_ccc_logica(p_pedido,
                                    p_subpedido,
                                    p_solicitud,
                                    v_causa_activa,
                                    v_requerimiento,
                                    v_causa_comun_logic);

        END IF;

        --Para los grandes clientes siempre se debe enrutar a ANALI
        --OJARAMIP 04/10/2007 Para TO con ADSL  siempre se debe enrutar a ANALI
        -- OJARAMIP 21/08/2009 Ya no aplica el enrutamiento de TO + ADSL a la cola ANALI
        -- se comentarea para que elija la cola de la parametrizacion de la separacion de colas.
        --jcarval 12/04/2011  separacion de colas C2
        --    IF (v_tipo_cliente LIKE '%GVI' OR v_tipo_cliente LIKE '%VIP' OR
        --       v_tipo_cliente LIKE '%GCL' OR v_tipo_cliente LIKE '%GPY'
        --       /*Or (    INSTR
        --                                                                 (fn_valor_caract_identif (p_identificador_ortr,
        --                                                                                           2093
        --                                                                                          ),
        --                                                                  'ACP'
        --                                                                 ) > 0
        --                                                        And v_cola_inicial Not In ('REPCE', 'PREPTEL', 'TSTDA')
        --                                                       )*/
        --       -- problemas *25# tarjetas prepago restriccion de llamadas
        --       )
        --       AND p_producto IN ('TO', 'PBX', 'DID')       OR
            IF v_causa_comun = 'SI' THEN
                     IF p_tecnologia_ortr IN ('HFC', 'COA','PAR') THEN
                        v_cola := 'TVM-INT';
                     ELSE
                      --2012-10-25 MAPERADO - Se busca la cola asignada a la CCC de fnx_causas_comunes para asocial los nuevos daños a la misma cola.
                      --2012-12-03 JPULGARB - Se implementa nvl en el select into ya que el campo cola_id puede llegar null
                        BEGIN
                            SELECT NVL(COLA_ID,'ANALI')
                            INTO v_cola
                            FROM FNX_CAUSAS_COMUNES
                            WHERE  CAUSA_ID= v_causa_activa
                            AND ESTADO='ABIER';
                        EXCEPTION
                            WHEN OTHERS THEN
                            v_cola := 'ANALI';
                        END;

                        IF instr(fn_valor_caract_identif(p_identificador_ortr, 2093), 'ACP') > 0 THEN
                            v_observacion := 'Enruta ANALI Tiene ADSL asociado. ' || substr(v_observacion, 1, 650);
                        END IF;
            END IF;

            /* 2013-08-13 JGLENA REQ22579_GPON Se modifica apertura del cursor. Si no se devuelven datos, la funcionalidad será la misma anterior */
            v_condicionar := 'S';
            OPEN c_copr(v_cola ,p_servicio, p_producto, p_tecnologia_ortr, p_empresa, p_municipio, v_condicionar );
            FETCH c_copr
                INTO v_actividad_inicial;
            CLOSE c_copr;
            IF v_actividad_inicial IS NULL THEN
               v_condicionar := 'N';
               OPEN c_copr(v_cola ,p_servicio, p_producto, p_tecnologia_ortr, p_empresa, p_municipio, v_condicionar );
            FETCH c_copr
                INTO v_actividad_inicial;
                 CLOSE c_copr;
            END IF;

        ELSE
            --DBMS_OUTPUT.put_line ('***** DEBE v_cola_inicial' || v_cola_inicial || v_tipo_cliente );
            IF v_cola_inicial IS NOT NULL AND
                (v_tipo_cliente NOT LIKE '%GVI' OR v_tipo_cliente NOT LIKE '%VIP' OR v_tipo_cliente NOT LIKE '%GCL' OR v_tipo_cliente NOT LIKE '%GPY') THEN
                v_cola := v_cola_inicial;

                /* 2013-08-13 JGLENA REQ22579_GPON Se modifica apertura del cursor. Si no se devuelven datos, la funcionalidad será la misma anterior */
                v_condicionar := 'S';
                OPEN c_copr(v_cola ,p_servicio, p_producto, p_tecnologia_ortr, p_empresa, p_municipio, v_condicionar );
                FETCH c_copr
                    INTO v_actividad_inicial;
                CLOSE c_copr;
                IF v_actividad_inicial IS NULL THEN
                   v_condicionar := 'N';
                   OPEN c_copr(v_cola ,p_servicio, p_producto, p_tecnologia_ortr, p_empresa, p_municipio, v_condicionar );

                FETCH c_copr
                    INTO v_actividad_inicial;
                     CLOSE c_copr;
                END IF;

            END IF;
            --DBMS_OUTPUT.put_line ('v_actividad_inicial' || v_actividad_inicial );
        END IF;
        --06/10/2011 DTREJOS SI EXISTE UNA CCC LOGICA SE ASIGNA LA COLA
        --03-12-2012 JPULGARB SE SACA ESTA CONDICION DEL ELSIF Y SE DEJA INDEPENDIENTE, YA QUE NO TODA CAUSA COMUN ES UNA FALLA COMUN (SEGUN DTREJOS)
        IF v_causa_comun_logic = 'SI' THEN
                v_cola := 'PFALLASM';

                /* 2013-08-13 JGLENA REQ22579_GPON Se modifica apertura del cursor. Si no se devuelven datos, la funcionalidad será la misma anterior */
                v_condicionar := 'S';
                OPEN c_copr(v_cola ,p_servicio, p_producto, p_tecnologia_ortr, p_empresa, p_municipio, v_condicionar );
                FETCH c_copr
                    INTO v_actividad_inicial;
                CLOSE c_copr;
                IF v_actividad_inicial IS NULL THEN
                   v_condicionar := 'N';
                   OPEN c_copr(v_cola ,p_servicio, p_producto, p_tecnologia_ortr, p_empresa, p_municipio, v_condicionar );
                FETCH c_copr
                    INTO v_actividad_inicial;
                CLOSE c_copr;
                END IF;
        END IF;
    END IF; --IF p_etapa = 'MANTE' AND p_tipo_solicitud <> 'REGEN'

    -- Asignacion de la actividad
    IF v_actividad_inicial IS NOT NULL THEN
        v_actividad := v_actividad_inicial;
    ELSE
        v_actividad := p_actividad;
    END IF;

    --Identificar el la ruta para los cables de Interconexion y para las otras actividades
    --IF p_actividad IN ('ICIN1', 'ICIN2', 'RCIN1', 'RCIN2', 'ICINT', 'RCINT')
    -- JGLENA 2010-08-25 Se modifica texto de la traza
    dbms_output.put_line('antes de entrar por paquete: ' || p_pedido || '  * *' || p_producto);
    -- JGLENA 2010-08-25 La orden a enrutar corresponde a un paquete
    IF p_producto = 'PQUETE' THEN
        -- OJARAMIP 2010 04 08 Se reemplaza Select into por cursos para agregar tipo elemento_id ordenado
        -- JGLENA   2010-08-25 4. Se modifica apertura del cursor ya que se cambio la definicion y se agregan variables
        --                     para recibir subpedido_id y solicitud_id y poder buscar tecnologia de identificador a usar en la ruta
        --                     Se busca en fnx_caracteristicas_solicitudes el identificador, subpedido y solicitud
        --                     del elemento TO o TOIP en el mismo pedido; Esto solo es valido para MEDANTCOL
        OPEN c_valor(p_pedido);

        FETCH c_valor
            INTO v_identif_ruta,
                  v_subpedido_ruta,
                  v_solicitud_ruta;

        CLOSE c_valor;

        /*      BEGIN
               SELECT valor
                 INTO
                 FROM fnx_caracteristica_solicitudes
                WHERE producto_id = 'TO'
                  AND pedido_id = p_pedido
                  AND caracteristica_id = 1
                  AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_identif_ruta := '';
                  v_tecno_ruta := 'REDCO';
            END;
      */
        -- 2010-01-04 Se agrega busqueda de la carcaterística 2880 para paquetes, con el fin de
        --            de corregir zonas de influencia
        IF v_identif_ruta IS NULL OR v_identif_ruta = '' THEN
            -- JGLENA 2010-08-25 Se agrega comentario
            -- Se busca caracteristica 4372 que corresponde al tipo de traslado
            BEGIN
                SELECT valor
                  INTO v_traslado_paq
                  FROM fnx_caracteristica_solicitudes
                 WHERE pedido_id = p_pedido
                    AND caracteristica_id = 4372
                    AND rownum = 1;
            EXCEPTION
                WHEN OTHERS THEN
                    v_traslado_paq := NULL;
            END;
            -- JGLENA 2010-08-25 Se agrega comentario
            -- Traslados en MEDANTCOL tipo 1 a TO de destino existente, tipo 2 a TO de destino nueva , tipo 3 de la TO existente a nuevo destino
            -- Se busca caracteristica 2880 correspondiente a identificador de acceso a TO
            IF v_traslado_paq IN ('1',
                                         '2',
                                         '3') THEN
                BEGIN
                    -- JGLENA   2010-08-25 5. Se modifica select into para obtener subpedido_id y solicitud_id
                    --                     y poder buscar tecnologia de identificador a usar en la ruta
                    SELECT valor,
                             subpedido_id,
                             solicitud_id
                      INTO v_identif_ruta,
                             v_subpedido_ruta,
                             v_solicitud_ruta
                      FROM fnx_caracteristica_solicitudes
                     WHERE pedido_id = p_pedido
                        AND caracteristica_id = '2880'
                        AND rownum = 1;
                EXCEPTION
                    WHEN OTHERS THEN
                        v_identif_ruta := '';
                        v_tecno_ruta   := 'REDCO';
                        -- JGLENA   2010-08-25 Manejo de excepcion: variables v_subpedido_ruta y v_solicitud_ruta quedan nulas
                        v_subpedido_ruta := '';
                        v_solicitud_ruta := '';

                END;
            END IF;
        END IF;

        -- JGLENA 2010-08-25 7. Se agrega la siguiente logica de IF:
        --                   Si no existe un componente TO o TOIP y tampoco se encuentra caracteristica 2880
        --                   se busca el identificador, subpedido y solicitud del ACCESP en el mismo pedido
        IF v_identif_ruta IS NULL THEN
            OPEN c_componente_ruta(p_pedido);

            FETCH c_componente_ruta
                INTO v_identif_ruta,
                      v_subpedido_ruta,
                      v_solicitud_ruta;

            CLOSE c_componente_ruta;

        END IF;
        -- JGLENA 2010-08-25 3. Se agrega lógica para que busque la tecnología del identificador que se va a usar en
        --                   enrutamiento; Si no encuentra la tecnologia en fnx_configuraciones_identif debe
        --                   buscarla en las caracteristicas de la solicitud del componente TO o TOIP o ACCESP
        IF v_identif_ruta IS NOT NULL THEN
            v_tecno_ruta := coalesce(fn_tecnologia_identif(v_identif_ruta),
                                             fn_valor_caracteristica_sol(p_pedido,
                                                                                  v_subpedido_ruta,
                                                                                  v_solicitud_ruta,
                                                                                  33));
            -- JGLENA 2010-08-27 2. En el caso de los paquetes si la tecnología del elemento TO, TOIP, ACCESP, INSHFC o INSIP
            --                      que se use para enrutar es HFC, se debe usar el procedimiento de enrutar que corresponde a
            --                      esa tecnología. Se coloca valor fijo puesto que no se autorizó creación de dominio
            --                      para almacenar el nombre del procedimiento.
            IF v_tecno_ruta IS NOT NULL AND v_tecno_ruta = 'HFC' AND instr(upper(v_funcion_enruta),
                                                                                                'HFC') = 0 THEN
                v_funcion_enruta := 'pkg_rutas_trabajos.pr_ruta_hfc';

                -- JGLENA 2010-08-30 1. En el caso de los paquetes si la tecnologia del elemento TO, TOIP, ACCESP, INSHFC o INSIP
                --                      que se use para enrutar es REDCO, se debe usar el procedimiento de enrutar que corresponde a
                --                      esa tecnología. Se coloca valor fijo puesto que no se autorizó creación de dominio
                --                      para almacenar el nombre del procedimiento.
            ELSIF v_tecno_ruta IS NOT NULL AND v_tecno_ruta = 'REDCO' AND instr(upper(v_funcion_enruta),
                                                                                                      'REDCO') = 0 THEN
                v_funcion_enruta := 'pkg_rutas_trabajos.pr_ruta_redco';
            /* 2014-05-05 JGLENA REQ36900_GponHoga Se adiciona tecnologia GPON y funcion de enrutamiento correspondiente */
            ELSIF v_tecno_ruta IS NOT NULL AND v_tecno_ruta = 'GPON' AND instr(upper(v_funcion_enruta),
                                                                                                      'GPON') = 0 THEN
                v_funcion_enruta := 'pkg_rutas_trabajos.pr_ruta_gpon';

            END IF;

        ELSE
            -- JGLENA 2010-08-30 2. Se agrega traza para identificar que no se encontró elemento de acceso para paquete
            dbms_output.put_line('Identificador ruta Nulo en PQUETE.v_identif_ruta := p_identificador_ortr');
            dbms_output.put_line('Identificador ruta Nulo en PQUETE.v_tecno_ruta := p_tecnologia_ortr');
            v_identif_ruta := p_identificador_ortr;
            v_tecno_ruta   := p_tecnologia_ortr;
        END IF;

    ELSE
        -- JGLENA 2010-08-25 Se agrega comentario. La orden a insertar NO corresponde a un paquete
        IF p_producto = 'INTER' AND p_tipo_elemento_id NOT IN ('ACCESP',
                                                                                 'CPE',
                                                                                 'CABLEM',
                                                                                 'CPEWIM') THEN
            IF p_tipo_elemento_id = 'CUENTA' THEN
                v_identif_ruta := fn_valor_caract_identif(p_identificador_ortr,
                                                                        2093);
            ELSE
                v_identif_ruta := fn_valor_caract_identif(fn_identificador_subpedido(p_pedido,
                                                                                                            p_subpedido),
                                                                        2093);
            END IF;
        ELSIF p_producto = 'TV' AND p_tipo_elemento = 'SRV' --OR p_tipo_elemento_id = 'INTCON')
         THEN
            OPEN c_iden_agru;

            FETCH c_iden_agru
                INTO v_identif_ruta;

            CLOSE c_iden_agru;
        ELSIF p_producto = 'TO' AND p_tipo_elemento_id <> 'TOIP'
        --OR p_tipo_elemento_id = 'INTCON')
         THEN
            v_identif_ruta := coalesce(fn_agrupador_identificador(p_identificador_ortr),
                                                fn_identificador_subpedido(p_pedido,
                                                                                    p_subpedido),
                                                p_identificador_ortr);
        END IF;

        IF v_identif_ruta IS NOT NULL THEN
            v_tecno_ruta := fn_tecnologia_identif(v_identif_ruta);
        ELSE
            v_identif_ruta := p_identificador_ortr;
            v_tecno_ruta   := p_tecnologia_ortr;
        END IF;
    END IF;

    -- JGLENA 2010-08-27  3. Se modifica el llamado a pkg_rutas_trabajos.pr_ruta_principal para pasar
    --                       la variable v_funcion_enruta en lugar de p_funcion_enruta y se agrega traza

    dbms_output.put_line('p_pedido-> ' || p_pedido || ' p_subpedido-> ' || p_subpedido || ' p_solicitud-> ' || p_solicitud || ' p_tipo_solicitud-> ' ||
                                p_tipo_solicitud || ' v_identif_ruta-> ' || v_identif_ruta || ' v_tecno_ruta-> ' || v_tecno_ruta || ' p_empresa-> ' ||
                                p_empresa || ' p_municipio-> ' || p_municipio || ' v_tipo_infraestructura-> ' || v_tipo_infraestructura || ' v_cola-> ' ||
                                v_cola || ' p_funcion_enruta-> ' || p_funcion_enruta || ' v_funcion_enruta-> ' || v_funcion_enruta);
    dbms_output.put_line('entra por ruta: ' || v_identif_ruta);
    pkg_rutas_trabajos.pr_ruta_principal(p_pedido,
                                                     p_subpedido,
                                                     p_solicitud,
                                                     p_tipo_solicitud,
                                                     v_identif_ruta,
                                                     v_tecno_ruta,
                                                     p_empresa,
                                                     p_municipio,
                                                     v_tipo_infraestructura,
                                                     v_cola,
                                                     v_funcion_enruta,
                                                     v_area_operativa,
                                                     v_subzona,
                                                     v_area_trabajo,
                                                     v_ruta,
                                                     v_secuencia_influencia,
                                                     v_influencia,
                                                     v_nodo);

    --  Para el caso de que no se encuentre infraestructura en tramite (ej. cuando solo se
    --  pide un servicio especial la etapa es config y tipo inf en tramite, pero no la tiene,
    --  por lo tanto no identifica la ruta de trabajo.
    IF v_area_operativa IS NULL OR v_area_operativa = 'NULA' AND p_tipo_solicitud <> 'NUEVO' THEN
        v_tipo_infraestructura := 'ACT';
        pkg_rutas_trabajos.pr_ruta_principal(p_pedido,
                                                         p_subpedido,
                                                         p_solicitud,
                                                         p_tipo_solicitud,
                                                         v_identif_ruta,
                                                         v_tecno_ruta,
                                                         p_empresa,
                                                         p_municipio,
                                                         v_tipo_infraestructura,
                                                         v_cola,
                                                         v_funcion_enruta,
                                                         v_area_operativa,
                                                         v_subzona,
                                                         v_area_trabajo,
                                                         v_ruta,
                                                         v_secuencia_influencia,
                                                         v_influencia,
                                                         v_nodo);
    END IF;

    -- Hallar minima para el requerimiento
    v_secuencia_minima := fn_secuencia_minima(p_requerimiento);

    ---------------------------------------------------------
    --Identificar el concepto
    IF p_secuencia = v_secuencia_minima OR v_secuencia_minima = 0 THEN
        IF p_tipo_solicitud IN ('REFAL',
                                        'REGEN',
                                        'ATENC') THEN
            IF p_tipo_solicitud = 'REFAL' AND v_causa_comun = 'SI' OR v_causa_comun_logic = 'SI' --06/10/2011 DTREJOS Se valida si existe una CCC logica masiva
             THEN
                v_concepto := 'PPCCC';
            ELSE
                v_concepto := fn_concepto_orden_mto(v_cola);
            END IF;
        ELSE
            v_concepto := 'PPRG';
        END IF;

        -- Actualiza la etapa y actividad del requerimiento que inicialmente se habia creado
        -- en INSTAL,INSTAL por defecto.
        UPDATE fnx_requerimientos_trabajos
            SET etapa_id     = p_etapa,
                 actividad_id = p_actividad
         WHERE requerimiento_id = p_requerimiento;
    ELSE
        v_concepto := 'PENDI';
    END IF;

    --Verifica el concepto para la habilitacion de caneles y cable modemns en cabecera,
    --Si tiene orden de isntalacion servicio o instalacr cable modem, concepto = 'PROG'
    dbms_output.put_line('Ingresa.');
    IF p_actividad IN ('HAPRE',
                             'DEPRE',
                             'HACAM',
                             'DECAM') AND p_tipo_solicitud IN ('NUEVO',
                                                                          'RETIR') THEN
        IF p_tipo_elemento = 'CMP' AND v_actividad IN ('HAPRE',
                                                                      'DEPRE') AND v_concepto = 'PPRG' THEN
            v_concepto := 'PROG';
        END IF;

        IF v_actividad IN ('HACAM',
                                 'DECAM')
          -- 2122 Plan para 3PLAY y 124 Velocidad para Internet
            AND (pkg_solicitudes.fn_valor_caracteristica(p_pedido,
                                                                        p_subpedido,
                                                                        p_solicitud,
                                                                        2122) IS NOT NULL OR
            pkg_solicitudes.fn_valor_caracteristica(p_pedido,
                                                                        p_subpedido,
                                                                        p_solicitud,
                                                                        124) IS NOT NULL) AND p_tipo_solicitud = 'CAMBI' AND v_concepto = 'PPRG' THEN
            v_concepto := 'PROG';
        END IF;
    ELSIF p_actividad = 'HAIDM' AND p_tipo_solicitud = 'NUEVO' AND v_concepto = 'PPRG' THEN
        v_concepto := 'PROG';
        --MARISTIM Abril 2008 Televisión IP
    ELSIF p_actividad IN ('HSTBX',
                                 'HATOI',
                                 'HAMUN',
                                 'HAMTO',
                                 'HAPOR') AND p_tipo_solicitud = 'NUEVO' AND p_tipo_elemento_id = 'EQURED' AND v_concepto = 'PPRG' THEN
        v_concepto := 'PENDI';
        --Ysolarte 04/08/2011 se agrega condición para disparar la primera orden automatica para IPTV empresarial
        --Cmurillg 12/10/2011 se agrega condición para verificar que sea IPTV y asi controlar que la orden no se dispare para TV Digital
    ELSIF p_producto = 'TV' AND (p_secuencia = v_secuencia_minima OR v_secuencia_minima = 0) AND
            v_procesar_cola IN ('AUTO',
                                      'PLATA') AND p_tipo_elemento_id = 'EQURED' AND
            fn_valor_caracteristica_sol(p_pedido,
                                                 p_subpedido,
                                                 p_solicitud,
                                                 207) = 'IPTV' THEN
        v_concepto := 'PPRG';
        --Cmurillg 12/10/2011  Se agrega condición para TV Digital empresarial, para disparar la orden de plataforma en la transacción de retiro de DECO
    ELSIF p_producto = 'TV' AND (p_secuencia = v_secuencia_minima OR v_secuencia_minima = 0) AND
            v_procesar_cola IN ('AUTO',
                                      'PLATA') AND p_tipo_elemento_id = 'DECO' AND
            nvl(fn_valor_caract_identif(fn_valor_caract_identif(p_identificador_ortr,
                                                                                 90),
                                                 207),
                 'X') = 'HFCDG' AND p_tipo_solicitud = 'RETIR' THEN
        dbms_output.put_line('entre tv digital');
        v_concepto := 'PPRG';
    ELSIF p_producto IN ('TELEV','TV') AND (p_secuencia = v_secuencia_minima OR v_secuencia_minima = 0) AND
            v_procesar_cola IN ('AUTO',
                                      'PLATA') AND p_tipo_elemento_id = 'EQURED' AND p_actividad = 'RDECO' AND p_tipo_solicitud = 'RETIR' THEN
        dbms_output.put_line('entre telev digital');
        v_concepto := 'PPRG';
    /*Ysolarte 2012-02-10 LTE 4G Se agrega condición para insertar las ordenes de entrega de equipos en oficina en concepto PROG*/
    /*2013-03-11 Jcardal/Jtabarc LTE 4G servicios fijos - Se agrega condición para insertar las ordenes de entrega de equipos en oficina en concepto PROG */

    ELSIF     p_producto IN ('MOVLTE', 'MOVLTR','FIJLTE')
          AND (p_secuencia = v_secuencia_minima OR v_secuencia_minima = 0)
          AND p_actividad IN ('EOLTE')
    THEN
       v_concepto := 'PROG';
    /*Cmurillg 2013-02-11 Voz en la Nube Se agrega condición para que las ordenes de los componentes deben quedar en PENDI*/
    ELSIF     p_producto  = 'VOZNUB'
          AND (p_secuencia = v_secuencia_minima OR v_secuencia_minima = 0)
          AND p_tipo_elemento = 'SRV' AND p_tipo_solicitud = 'NUEVO'
    THEN
       v_concepto := 'PENDI';
        --2009-10-08 Jgomezve  Se pone en PENDI para los traslados MTA y THOMPSON
        -- 2010 08 25 ojaramip se exluye internet movil por que no aplica...
    ELSIF p_producto <> 'INTMOV' AND v_procesar_cola IN ('AUTO',
                                                                          'PLATA') AND p_tipo_elemento_id IN ('EQURED','DECO') AND v_concepto = 'PPRG' THEN
        IF p_actividad NOT IN ('RSTBX',
                                      'DEPQT') THEN
            -- JGallg - 2011-10-05
            v_concepto := 'PENDI';
        END IF;
    ELSIF p_actividad = 'HSTBX' AND p_tipo_elemento_id = 'STBOX' AND
            pkg_solicitudes.fn_tipo_trabajo(p_pedido,
                                                      p_subpedido,
                                                      p_solicitud,
                                                      1067) = 'CAMBI' AND v_concepto = 'PPRG' THEN
        v_concepto := 'PENDI';
        --SE COLOCA EN PENDI PORQUE ESTOS DEBE SER CUMPLIDOS CON LA ORDEN DE INSTALACIÓN
        --MARISTIM Enero 20-2009 Se coloca en
    ELSIF p_actividad IN ('HAPPV',
                                 'DEPPV') AND p_tipo_solicitud = 'NUEVO' AND p_tipo_elemento_id = 'DECO' AND v_concepto = 'PPRG' THEN
        v_concepto := 'PENDI';
        --SE COLOCA EN PENDI PORQUE ESTOS DEBE SER CUMPLIDOS CON LA ORDEN DE INSTALACIÓN
        --MARISTIM Enero 20-2009 Se coloca en
    ELSIF p_actividad = 'INTMO' AND v_concepto = 'PPRG' AND fn_valor_caract_subpedido(p_pedido,
                                                                                                                 p_subpedido,
                                                                                                                 1446) = 'UNE' THEN
        v_concepto := 'AGEN';
        -- OJARAMIP 24/09/2009 DEMANDA CON EQUIPO EN COMODATO
    ELSIF fn_valor_caract_subelem(p_pedido,
                                            p_subpedido,
                                            'INTCON',
                                            1339) = 'DEM' AND fn_valor_caract_subelem(p_pedido,
                                                                                                    p_subpedido,
                                                                                                    'EQACCP',
                                                                                                    1093) = 'CD' AND
            fn_valor_caract_subelem(p_pedido,
                                            p_subpedido,
                                            'INTCON',
                                            2005) = '81120744' AND p_etapa = 'INSTA' THEN
        -- 2010-02-03 CVILLARE  Validación necesaria para Internet BA x Demanda
        v_dem := 'SI';

        OPEN c_cambio;

        FETCH c_cambio
            INTO v_cambio;

        IF c_cambio%NOTFOUND THEN
            v_cambio := 'NO';
        END IF;

        CLOSE c_cambio;

        IF v_cambio = 'SI' THEN
            v_concepto := 'PROG';
        END IF;

    END IF;

    -- 2009-10-16 Truizd    Se agrega el tipo solicitud TRASL para traslado de pquete
    --  2010 - 03 04 No aplica para Banda ancha por Demanda
    IF p_etapa = 'INSTA' AND p_tipo_solicitud IN ('NUEVO',
                                                                 'TRASL') AND v_dem <> 'SI' THEN
        --identificar que la orden a insertar pertence a un paquete que agrupa las ordenes
        --para colocar el estado concepto correcto.
        BEGIN
            SELECT COUNT(1)
              INTO v_cantidad
              FROM fnx_componentes_paquetes
             WHERE paquete_id = v_tipo_paquete
                AND agrupador_instalacion IS NOT NULL
                AND servicio_id = p_servicio
                AND producto_id = p_producto
                AND tipo_elemento = p_tipo_elemento
                AND tipo_elemento_id = p_tipo_elemento_id;
        EXCEPTION
            WHEN OTHERS THEN
                v_cantidad := 0;
        END;
    END IF;

    IF v_cantidad > 0 -- v_tipo_paquete = 'OFERTA'     AND
     THEN
        v_concepto := 'PPQUE';

        --Septiembre 26-2006 Si la secuencia de la orden es la minima se coloca en PQUT
        IF p_secuencia = v_secuencia_minima OR v_secuencia_minima = 0 THEN
            v_concepto := 'PQUET';
        END IF;

        v_comppaq := 'S';

        -- 2009-10-20 Truizd    Se valida el tipo de infraestuctura para que las ordenes de
        -- desinstalacion de traslado de pquete no queden PQUET
        IF v_tipo_infraestructura = 'ACT' AND p_tipo_solicitud = 'TRASL' THEN
            v_concepto := 'PENDI';
            v_comppaq  := NULL;
        END IF;
    END IF;

    IF p_producto = 'ATENC' THEN
        v_concepto := 'PROG';
    END IF;

    -- GSANTOS 15/06/2010
    -- PBX POTS
    -- SE ADICIONA QUE MODIFIQUE EL CONCEPTO DE LA ORDEN TRONCAL COLECTIVA
    -- EN PBX POTS. SE NECESITA QUE SE INSERTE PENDI CUANDO VAYA CON UNA SEÑAL
    -- LLAMADA EN EL MISMO PEDIDO O SE INSERTE PPRG CUANDO VAYA SOLO LA TRONCAL
    -- COLECTIVA.
    IF (p_servicio = 'TELEMP' AND -- ACTIVIDAD
        p_producto = 'PBX' AND p_tipo_elemento = 'CMP' AND p_tipo_elemento_id = 'TC' AND p_actividad = 'CRLTC' AND p_tecnologia_ortr = 'POTS' AND
        p_tipo_solicitud IN ('NUEVO',
                                     'TRASL',
                                     'CAMBI')) OR
        (p_servicio = 'TELEMP' AND -- PUENTES
        p_producto = 'PBX' AND p_tipo_elemento = 'CMP' AND p_tipo_elemento_id = 'TC' AND p_etapa = 'PUENT' AND p_tecnologia_ortr = 'POTS' AND
        p_tipo_solicitud IN ('NUEVO',
                                     'TRASL',
                                     'CAMBI')) THEN
        OPEN c_tc_pots;

        FETCH c_tc_pots
            INTO v_min_sec;

        CLOSE c_tc_pots;

        IF v_min_sec < p_secuencia THEN
            OPEN c_sll_pots;

            FETCH c_sll_pots
                INTO v_sll_cumpl;

            CLOSE c_sll_pots;

            IF v_sll_cumpl = 1 THEN
                v_concepto := 'PPRG';
            ELSE
                v_concepto := 'PENDI';
            END IF;
        END IF;
    END IF;

    -- FIN GSANTOS 15/06/2010

    --FIN Concepto_id

    -------------------------------------------------------------------------------------
    --DATOS ADICIONALES PARA LA ORDEN DE TRABAJO
    -------------------------------------------------------------------------------------
    -- Verificar Plan Empleados
    v_plan_empleados := fn_plan_empleados(p_pedido,
                                                      p_subpedido,
                                                      p_solicitud);

    IF v_plan_empleados = 'S' THEN
        v_tipo_cliente := '1.EMP';
    END IF;

    -- Usuario que ejecuto la operacion
    -- v_usuario := fn_usuario;
    -- Uso del servicio
    v_uso_servicio := nvl(fn_uso_servicio(p_pedido,
                                                      p_subpedido,
                                                      p_solicitud),
                                 fn_uso_servicio_identif(p_identificador_ortr));
    --Identificar si es cambio de PRD para colocar OBservacion
    v_cambio_prd := fn_verificar_cambio_prd(p_pedido,
                                                         p_subpedido);

    IF v_cambio_prd = 'SI' AND p_etapa = 'INSTA' THEN
        BEGIN
            SELECT producto_id
              INTO v_producto_cambio
              FROM fnx_subpedidos
             WHERE pedido_id = p_pedido
                AND tipo_subpedido = 'RETIR';
        EXCEPTION
            WHEN OTHERS THEN
                NULL;
        END;

        v_observacion := '**CAMBIO DE PRODUCTO : ' || v_producto_cambio || ' --> ' || p_producto || v_observacion;
    END IF;

    IF p_etapa = 'PUENT' AND p_actividad IN ('ICNOR',
                                                          'ICIN1',
                                                          'ICIN2',
                                                          'ICINT',
                                                          'IGAPA',
                                                          'IINAL',
                                                          'RCNOR',
                                                          'RCIN1',
                                                          'RCIN2') THEN
        v_observacion := fn_puentes_simultaneos(p_pedido,
                                                             p_subpedido,
                                                             p_solicitud) || '' || v_observacion;
    END IF;

    --Permanencia servicio  ojaramip 2004/11/22
    v_permanencia := fn_valor_caracteristica_sol(p_pedido,
                                                                p_subpedido,
                                                                p_solicitud,
                                                                52);

    IF v_permanencia IS NULL THEN
        v_permanencia := fn_valor_caract_identif(p_identificador_ortr,
                                                              52);
    END IF;

    --------------------------------------------------------------------------
    -- Para el Cambio de Producto de PBX se creo actividad alterna para cambiar secuencia...
    -- aqui se cambia a la actividad real  --
    IF v_actividad = 'HACON' THEN
        v_actividad := 'HACOM';
    END IF;

    IF v_actividad = 'DECON' THEN
        v_actividad := 'DECOM';
    END IF;

    -------------------------------------------------------------------------------------
    -------------------------------------------------------------------------------------
    --DBMS_OUTPUT.put_line ('v_cola ' || v_cola);
    v_servicio           := p_servicio;
    v_producto           := p_producto;
    v_tipo_elemento_id   := p_tipo_elemento_id;
    v_tipo_elemento      := p_tipo_elemento;
    v_tecnologia_ortr    := p_tecnologia_ortr;
    v_identificador_ortr := p_identificador_ortr;

    IF p_producto = 'ATENC' THEN
        -- LMEDINA se busca el producto y tipo elemento id asociado a la ATENC ion.
        v_servicio           := fn_valor_caracteristica_sol(p_pedido,
                                                                             p_subpedido,
                                                                             p_solicitud,
                                                                             2186);
        v_producto           := fn_valor_caracteristica_sol(p_pedido,
                                                                             p_subpedido,
                                                                             p_solicitud,
                                                                             2187);
        v_tipo_elemento_id   := fn_valor_caracteristica_sol(p_pedido,
                                                                             p_subpedido,
                                                                             p_solicitud,
                                                                             2189);
        v_identificador_ortr := fn_valor_caracteristica_sol(p_pedido,
                                                                             p_subpedido,
                                                                             p_solicitud,
                                                                             2746);
        v_equipo             := fn_buscar_equipos_atenc(v_cola,
                                                                        v_area_operativa,
                                                                        v_subzona);

        IF v_identificador_ortr IS NULL THEN
            v_identificador_ortr := 'ATENC';
            v_tecnologia_ortr    := 'LOGIC';
        ELSE
            v_tecnologia_ortr := pkg_identificadores.fn_tecnologia(v_identificador_ortr);
        END IF;
    END IF;

    IF v_producto = 'ASP' THEN
        v_equipo := fn_valor_caract_subpedido(p_pedido,
                                                          p_subpedido,
                                                          2119);

        IF v_equipo IS NULL THEN
            ---AGOSTO09-2005 MLA El P_IDENTIFICADOR_REQT esta lleno, el otro tenemos dudas
            v_equipo := fn_valor_caract_identif(v_identificador_ortr,
                                                            2119);

            IF v_equipo IS NOT NULL THEN
                v_observacion := '1.' || v_identificador_ortr || ' - ' || v_equipo || '-' || v_observacion;
            END IF;
        END IF;

        IF v_equipo IS NULL THEN
            v_identif_ruta := fn_identificador_subpedido(p_pedido,
                                                                        p_subpedido);
            v_equipo       := fn_valor_caract_identif(v_identif_ruta,
                                                                    2119);

            IF v_equipo IS NOT NULL THEN
                v_observacion := '2.' || v_identif_ruta || ' - ' || v_equipo || '-' || v_observacion;
            END IF;
        END IF;
    END IF;

    --GSANTOS Feb 05-2009 SUIN 1157059 - Agrupación cola de retiro
    IF p_tipo_solicitud = 'RETIR' AND p_etapa = 'INSTA' THEN
        -- Idnetificar si se esta haciendo el retiro del servcic
        --Si la orden es de
        v_depto       := nvl(fn_valor_caract_subpedido(p_pedido,
                                                                      p_subpedido,
                                                                      204),
                                    fn_valor_caract_identif(p_identificador_ortr,
                                                                    204));
        v_cambio_prod := fn_verificar_cambio_prd(p_pedido,
                                                              p_subpedido);
                                                              
        --  dmartinez** - 03/09/2014 
        
        BEGIN -- Bloque para extraer y registrar la observación de por qué no se generó RECEQ
            SELECT SUBSTR( observacion, INSTR(UPPER(observacion), 'DESAP-') )
              INTO v_observacion_receq
              FROM fnx_pedidos
             WHERE pedido_id = p_pedido
               AND UPPER(observacion) like '%DESAP-%';
                
            IF INSTR(UPPER(v_observacion_receq), ';') > 0 THEN
                v_observacion_receq := SUBSTR(v_observacion_receq, 1, INSTR(UPPER(v_observacion_receq), ';') - 1);
            ELSE
                v_observacion_receq := SUBSTR(v_observacion_receq, 1, INSTR(UPPER(v_observacion_receq), '-', 7) - 1);
            END IF;
                        
            v_observacion := SUBSTR(v_observacion_receq || ';' || v_observacion, 1, 700);
                                                
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                DBMS_OUTPUT.put_line( '<% PR_INS_ORDENES_TRABAJOS-> Error extrayendo información del pedido'
                                 || SUBSTR (SQLERRM, 1, 80)
                                 );
        END; 
                        
        -- dmartinez**

        IF v_cambio_prod <> 'SI' AND p_tipo_elemento_id IN ('TOIP',
                                                                             'INSHFC',
                                                                             'INSIP',
                                                                             'TO',
                                                                             'ACCESP') THEN
            IF v_depto = 'ANT' THEN
                --pagina
                v_comppaq := fn_valor_caract_identif(p_identificador_ortr,
                                                                 38);
            ELSE
                --direccion
                v_comppaq := fn_valor_caract_identif(p_identificador_ortr,
                                                                 35);
                -- lA DIRECCION
                --1.QUITA ESPACIOS (Replace)
                v_comppaq := REPLACE(v_comppaq,
                                            ' ',
                                            '');
                --2.Quita caracterest(funcion pendiente )
                --3.longitud max 50
                v_comppaq := substr(v_comppaq,
                                          1,
                                          50);
            END IF;
        END IF;

        dbms_output.put_line(' MARCA PARA RETIRO v_compPaq ' || v_comppaq);
    END IF;

    dbms_output.put_line(' Genera Orden para Actividad HDECM');
    dbms_output.put_line(' p_producto =' || p_producto);
    dbms_output.put_line(' v_procesar_cola =' || v_procesar_cola);
    dbms_output.put_line(' p_actividad =' || p_actividad);
    dbms_output.put_line(' p_tipo_solicitud =' || p_tipo_solicitud);

    /**2013-11-01 EARDILAC  Verifica que el pedido sea tangibilizacion/*/

       OPEN c_tangibilizacion;
       FETCH c_tangibilizacion INTO v_can_tangi;
       CLOSE c_tangibilizacion;

       dbms_output.put_line('v_can_tangi:'|| v_can_tangi);
       IF v_can_tangi > 0 THEN
            /*Eardilac 2013-10-09 Inicio REQ 28628 Tangibilizacion Genera las ordenes de trabajo de la nueva actividad HDECM*/
            IF p_producto = 'TELEV' AND  v_procesar_cola IN ('AUTO') AND p_actividad in ('HDECM','HDECO') AND p_tipo_solicitud = 'NUEVO' THEN
                dbms_output.put_line('entre telev digital');
                v_concepto := 'PPRG';
            END IF;
            /*Eardilac 2013-10-09 Fin REQ 28628 Tangibilizacion Genera las ordenes de trabajo de la nueva actividad HDECM*/

       END IF; /**2013-11-01 EARDILAC  Verifica que el pedido sea tangibilizacion*/
    dbms_output.put_line('v_concepto3 ' || v_concepto);
    dbms_output.put_line('p_ident_paquete 5 ' || p_ident_paquete);
    -- 2010-01-25 ysolarte    Se agrega validación para la generación de la orden de la cola encuesta
    dbms_output.put_line('v_cola prueba ' || v_cola);
    v_estado_orden := 'PENDI';

    IF v_cola = 'ENCUESTA' THEN
        v_val_tel_encue := 'SI';
        dbms_output.put_line('Entro Prueba  ' || v_cola);
        v_val_tel_encue := fn_verificar_tel_encuesta(p_pedido);

        IF v_val_tel_encue = 'NO' THEN
            v_estado_orden := 'ANULA';
            v_concepto     := 'ANULA';
            v_observacion  := substr('ANULADA POR ERROR EN TELEFONO ' || v_observacion,
                                             1,
                                             700);
        END IF;
    END IF;

    dbms_output.put_line('v_val_tel_encue  ' || v_val_tel_encue);

    -- JGallg - 2011-06-11 - Desaprov-Cambio: Dejar automáticamente la orden en PENDI - SINTE si en el municipio
    --                       no hay la infraestructura adecuada para recoger equipos.
    pr_chequeo_sinte(v_identificador_ortr,
                          v_cola,
                          p_pedido,
                          p_subpedido,
                          v_estado_orden,
                          v_concepto);

    -- JGallg - 2011-07-09 - Desaprov-Estabiliza: Llevar a REFERENCIA_ID (v_comppaq) la marca de equipo obsoleto
    --                       que se usará para no permitir agendar ordenes RECEQ asociadas a equipos obsoletos.
    -- JGallg - 2011-09-28 - Para equipos obsoletos, programar de una vez la orden de trabajo.  La fecha de entrega
    --                       será 25 días después de la fecha de recibo.
    IF v_cola = 'RECEQ' THEN
        OPEN c_equipo_obsoleto;
        FETCH c_equipo_obsoleto
            INTO rg_equipo_obsoleto;
        CLOSE c_equipo_obsoleto;
        v_comppaq := rg_equipo_obsoleto.marca_obsoleto;

        -- 2014-02-24 Jrendbr Se adiciona logica para que tenga en cuenta los estado de los equipos NIR.
        v_equipo_nir := fn_consultar_equipo_nir (rg_equipo_obsoleto.marca_id,         rg_equipo_obsoleto.referencia_id,
                                                 rg_equipo_obsoleto.tipo_elemento_id, rg_equipo_obsoleto.tipo_elemento);

       -- 2014-02-24 Jrendbr Se adiciona logica para que tenga en cuenta los estado de los equipos NIR.
        IF v_comppaq = 'RECEQ_OBSOLETO=SI' OR v_equipo_nir= 'S' THEN
            v_fecha_entrega := SYSDATE + 25;
            v_hora_entrega  := to_char(SYSDATE,
                                                'HH24:MI');
            v_estado_orden  := 'PENDI';
            v_concepto      := 'PROG';
        ELSE
            v_fecha_entrega := NULL;
            v_hora_entrega  := NULL;
        END IF;

        --2013-03-11    Yhernana    Se captura la dirección de recogida de equipo en los retiros,
        --                          para adicionarlo en la observación de la orden.
        v_direccion_recogida:= fn_valor_caracteristica_sol(p_pedido, p_subpedido, p_solicitud, 4301);

        IF v_direccion_recogida IS NOT NULL THEN
            v_observacion  := SUBSTR('**DARE** '||v_direccion_recogida||''|| v_observacion,1,700);
        END IF;

    ELSE
        v_fecha_entrega := NULL;
        v_hora_entrega  := NULL;
    END IF;

     --20131004     JVASCO  -   Requerimiento servicios adicionales internet empresarial
     --                         concatena la observación de la tabla fnx_observaciones_generales
     --                         a la observación de la tabla fnx_ordenes_trabajos



    IF p_producto = 'INTER' AND p_tipo_elemento_id = 'ADMSER' THEN
        OPEN c_obsegene;
        FETCH c_obsegene INTO v_obse_gene;
        CLOSE c_obsegene;
        v_observacion := v_observacion || v_obse_gene;
    END IF;


    -- 2014-07-09 GSANTOS   ITPAM50300_NUEVO_RETIR. Se debe modificar el concepto O-310 por PDNR
    -- 2014-01-03 DTANGAHU REQ_Nuevo_Retir_Mejora Se incluye la condición para cambiar el concepto en la tabla
    IF  p_tipo_solicitud ='RETIR' AND  FN_VALOR_CARACT_SUBPEDIDO(p_pedido,p_subpedido,85) = 'DNR' AND
     fn_valor_caract_subpedido(p_pedido,p_subpedido,4941) IS NOT NULL AND  v_procesar_cola IN ('AUTO', 'PLATA')  THEN
        v_concepto := 'PDNR';
    END IF;
    
    -- 2014-09-01   Yhernana    ITP66474_CambVeloEqui: INI
    --                          Si hay cambio de velocidad igual o superior a 8 megas (característica 124)
    --                          y el cliente tenga instalado un equipo diferente a DOCSIS3.0, 
    --                          entonces se actualice la observación de la orden en curso 
    --                          adicionando  la siguiente marca: ** DOCSIS 3.0 **
    IF v_cola = 'TVCABI' AND v_actividad= 'INCAM' THEN
        BEGIN
            v_vel_tipo_docsis:= NVL(TO_NUMBER(fn_valor_parametros('DOCSIS',1)),8000);
            
            DBMS_OUTPUT.PUT_LINE ( 'v_vel_tipo_docsis = ' || v_vel_tipo_docsis );
        
            IF TO_NUMBER(FN_valor_caracteristica_sol( p_pedido,p_subpedido,p_solicitud,124)) >= v_vel_tipo_docsis AND 
               TO_NUMBER(FN_valor_caracteristica_sol( p_pedido, p_subpedido,p_solicitud,124)) >
               TO_NUMBER(FN_valor_caracteristica_trab( p_pedido,p_subpedido,p_solicitud,124)) THEN
                       
                OPEN c_equipo(v_identificador_ortr);
                FETCH c_equipo INTO v_equipo_actual;
                IF c_equipo%FOUND THEN
                    DBMS_OUTPUT.PUT_LINE ( 'v_equipo_actual = ' || v_equipo_actual.MARCA_ID );
                    DBMS_OUTPUT.PUT_LINE ( 'v_equipo_actual = ' || v_equipo_actual.REFERENCIA_ID );
                    DBMS_OUTPUT.PUT_LINE ( 'v_equipo_actual = ' || v_equipo_actual.TIPO_ELEMENTO_ID );
                     v_docsis:= fn_consultar_tipo_docsis (v_equipo_actual.marca_id,
                                                          v_equipo_actual.referencia_id,
                                                          v_equipo_actual.tipo_elemento_id);
                END IF;
                CLOSE c_equipo;
                
                DBMS_OUTPUT.PUT_LINE ( 'v_docsis = ' || v_docsis );
                       
                v_tipo_docsis_impr:= NVL(fn_valor_parametros('DOCSIS',2),'DOCSIS3.0');
            
                IF NVL(v_docsis, 'N') <> v_tipo_docsis_impr THEN
                       
                    v_observacion  := SUBSTR(' ** '||v_tipo_docsis_impr||' ** '|| v_observacion,1,700);
                    DBMS_OUTPUT.PUT_LINE ( 'v_observacion = ' || v_observacion );
                
                END IF;
               
            END IF;
        EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.put_line
                                (   '<% PR_INS_ORDENES_TRABAJOS-> Error comparando velocidad inter '
                                 || SUBSTR (SQLERRM, 1, 80)
                                 );
          NULL;
        END;
    END IF;
    -- 2014-09-01   Yhernana    ITP66474_CambVeloEqui: FIN

    BEGIN
        INSERT INTO fnx_ordenes_trabajos
            (requerimiento_id,
             etapa_id,
             actividad_id,
             secuencia,
             cola_id,
             fecha_recibo,
             hora_recibo,
             estado_id,
             concepto_id,
             fecha_estado,
             usuario,
             area_operativa_id,
             subzona_id,
             area_trabajo_id,
             ruta_trabajo_id,
             influencia_id,
             secuencia_influencia,
             pedido_id,
             subpedido_id,
             solicitud_id,
             identificador_id,
             tecnologia_id,
             tipo_cliente,
             tipo_trabajo,
             uso_servicio,
             tipo_nodo,
             observacion,
             fecha_entrega,
             hora_entrega,
             tipo_elemento_id,
             tipo_elemento,
             equipo_id,
             servicio_id,
             producto_id,
             paquete_id,
             empresa_id,
             municipio_id,
             marca_id,
             referencia_id)
        VALUES
            (p_requerimiento,
             p_etapa,
             v_actividad,
             p_secuencia,
             v_cola,
             SYSDATE,
             to_char(SYSDATE,
                        'HH24:MI'),
             v_estado_orden,
             v_concepto,
             SYSDATE,
             p_usuario,
             v_area_operativa,
             v_subzona,
             v_area_trabajo,
             v_ruta,
             v_influencia,
             v_secuencia_influencia,
             p_pedido,
             p_subpedido,
             p_solicitud,
             v_identificador_ortr,
             v_tecnologia_ortr,
             v_tipo_cliente,
             p_tipo_solicitud,
             v_uso_servicio,
             v_tipo_nodo,
             v_observacion,
             v_fecha_entrega, -- JGallg - 2011-09-28
             v_hora_entrega, -- JGallg - 2011-09-28
             v_tipo_elemento_id,
             v_tipo_elemento,
             v_equipo,
             v_servicio,
             v_producto,
             p_ident_paquete,
             nvl(p_empresa,
                  'UNE'),
             p_municipio,
             v_tipo_seg,
             v_comppaq);

        ------------------------------------------------------------------
        IF p_actividad IN ('HACUC',
                                 'DECUC') AND v_concepto = 'PPRG' THEN
            pr_crear_cuenta_controlada(p_pedido,
                                                p_subpedido,
                                                p_solicitud,
                                                p_identificador_ortr,
                                                p_identificador_ortr,
                                                p_actividad,
                                                p_tipo_solicitud);
        END IF;

        ------------------------------------------------------------------
        --Activacion y desactivacion automatica de comandos.
        IF p_actividad IN ('HAESP',
                                 'DEESP') AND v_concepto = 'PPRG'
                                          AND v_tipo_nodo IN ('AXE', 'FTM', 'FTX', 'NEC') THEN
            pr_crear_comando(p_pedido,
                                  p_subpedido,
                                  p_solicitud,
                                  p_identificador_ortr,
                                  p_identificador_ortr,
                                  p_actividad,
                                  p_tipo_solicitud,
                                  p_producto,
                                  v_concepto);
        END IF;

        ------------------------------------------------------------------
        --p_actividad IN ('HACIR', 'HASUS', 'HATOT')
        IF p_actividad IN ('HACIR',
                                 'HASUS',
                                 'HATOT') AND v_concepto = 'PPRG'
                                          AND v_tipo_nodo IN ('AXE', 'FTM', 'FTX', 'NEC')  THEN
            pr_crear_comando_descon(p_pedido,
                                            p_subpedido,
                                            p_solicitud,
                                            p_identificador_ortr,
                                            p_identificador_ortr,
                                            p_actividad,
                                            p_tipo_solicitud,
                                            p_producto,
                                            v_concepto);
        END IF;

        ------------------------------------------------------------------

        ------------------------------------------------------------------
        --'AUTO'
        IF v_procesar_cola IN ('AUTO',
                                      'PLATA') AND v_concepto = 'PPRG' THEN
            /*2011-12-12  YSOLARTE     LTE 4G Se modifica para reemplazar el proceso que inserta en el FNX_TAREAS_PLATAFORMAS por el proceso que inserta eventos*/
             OPEN c_tipotarea;
            FETCH c_tipotarea
             INTO v_tipo_tarea;
            CLOSE c_tipotarea;

            DBMS_OUTPUT.put_line (   'v_tipo_tarea  '
                                  || v_tipo_tarea
                                  || '-'
                                  || p_actividad
                                 );

            IF NVL (v_tipo_tarea, 'T') = 'B'
            THEN
               --v_codigo_evento := FN_VALOR_CARACT_SUBPEDIDO(p_pedido,p_subpedido,4805);
               v_codigo_evento :=
                  fn_valor_caracteristica_sol (p_pedido,
                                               p_subpedido,
                                               p_solicitud,
                                               4805
                                              );

                DBMS_OUTPUT.put_line (   'v_codigo_evento  '
                                  || v_codigo_evento
                                 );
               /*2012-05-24 YSOLARTE Se reemplaza la función fn_dominio_descripcion por la función fn_valor_adi_parametros_ord de órdenes*/
               v_aplicacion := fn_valor_adi_parametros_ord (v_codigo_evento,p_actividad);

               DBMS_OUTPUT.put_line (   'v_aplicacion  '
                                  || v_aplicacion
                                 );
            END IF;

            DBMS_OUTPUT.put_line (   'v_codigo_evento  '
                                     || v_codigo_evento
                                     || ' '
                                     || v_aplicacion
                                    );

            /*Cmurillg 2013-02-11 Voz en la Nube - Se modifica condicion para consultar la plataforma en un en el parametro plataformas*/
            --IF v_aplicacion IS NOT NULL
            IF instr(FN_VALOR_PARAMETROS_ORD('LTE','PLATAFORMAS')||',',NVL(v_aplicacion,'X') || ',') > 0
            THEN
               DBMS_OUTPUT.put_line ('PKG_GESTION_EVENTOS_LTE.PR_CREAR_EVENTO ');
               pkg_apis_lte_fenix.pr_crear_evento_lte (v_aplicacion,
                                                       v_codigo_evento,
                                                       p_pedido,
                                                       p_subpedido,
                                                       p_solicitud,
                                                       p_identificador_ortr,
                                                       p_tipo_solicitud,
                                                       p_actividad
                                                      );
            /*Cmurillf 2013-02-11 Voz en la Nube - Se agrega condición para invocar el proceso de inserción de eventos*/
            ELSIF instr(FN_VALOR_PARAMETROS_ORD('INTEGRABTS','PLATAFORMAS')||',',NVL(v_aplicacion,'X') || ',') > 0  THEN
              DBMS_OUTPUT.put_line ('PKG_INTEGRA_FENIX_BTS.PR_CREAR_EVENTO ');

              /*2013-04-25 YSOLARTE - Proyecto GPON Adición de parametro aplicación para insertar en la tabla bts_eventos en el campo aplicacion_id en el llamado al paquete PKG_INTEGRA_FENIX_BTS*/
              PKG_INTEGRA_FENIX_BTS.PR_CREAR_EVENTO(  p_pedido,
                                                       p_subpedido,
                                                       p_solicitud,
                                                       v_codigo_evento,
                                                       v_aplicacion,
                                                       p_producto,
                                                       p_actividad,
                                                       v_error_bts,
                                                       v_mensaje_bts);

            ELSE
               pkg_tareas_plataformas.pr_crear_tarea (p_pedido,
                                                      p_subpedido,
                                                      p_solicitud,
                                                      p_requerimiento,
                                                      p_identificador_ortr,
                                                      v_plataforma_cola,
                                                      v_actividad,
                                                      p_tipo_solicitud
                                                     );
            END IF;
        END IF;

        ------------------------------------------------------------------
        --'MAIL'
        IF v_plataforma_cola = 'MAIL' THEN
            --2009-06-05  Jgomezve  Se cambia el llamado del procedimiento porque se unió
            --                      pr_verificar_mail_mante y pr_verificar_mail_mante_otro
            --                      en uno solo
            pkg_ordenes_util.pr_verificar_mail_mante(p_pedido,
                                                                  p_subpedido,
                                                                  p_solicitud,
                                                                  v_cola,
                                                                  v_fecha_mante,
                                                                  v_hora_mante,
                                                                  p_naturaleza);
        END IF;
    END;
    /* OJARAMIP 2006/02/27
   No se puede controlar excepciones solo con mensajes de consola, estamos
   encontrando transacciones malas, inconsistentes.*/
    /*MARY LUZ MARZO 05-2009 NO ESTA DEVOLVIENDO EL ERROR
   EXCEPTION WHEN OTHERS THEN
         dbms_output.put_line(' Error '||SQLERRM);*/
END; 