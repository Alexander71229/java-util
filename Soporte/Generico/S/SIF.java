import java.util.*;
public class SIF extends Estructura{
	public String codigo;
	public String condicion;
	public ArrayList<Sentencia>verdaderas;
	public ArrayList<Sentencia>falsas;
	public boolean marcado;
	public SIF(Tokenizer fuente){
		condicion="";
		codigo="";
		String token="";
		while(fuente.hasNext()){
			token=fuente.next();
			if(token.toUpperCase().equals("w_inconsistent".toUpperCase())){
				marcado=true;
			}
			if(token.toUpperCase().equals("THEN")){
				break;
			}
			condicion+=" "+token;
		}
		this.codigo="IF(|"+this.condicion.trim()+" |)THEN\n";
		verdaderas=new ArrayList<Sentencia>();
		falsas=new ArrayList<Sentencia>();
		ArrayList<Sentencia>instrucciones=verdaderas;
		/*if(marcado){
			System.out.println(this.codigo);
			System.exit(0);
		}*/
		while(fuente.hasNext()){
			token=fuente.next();
			if(token.toUpperCase().equals("ELSE")){
				this.codigo+="°-°";
				instrucciones=falsas;
				continue;
			}
			if(token.toUpperCase().equals("ELSIF")){
				/*System.out.println("ELSIF no soportado");
				System.exit(0);*/
				instrucciones=falsas;
			}
			if(token.toUpperCase().equals("END")){
				token=fuente.next();
				if(!token.toUpperCase().equals("IF")){
					System.out.println(this.condicion);
					//System.out.println("Error detectando fin de IF (END IF):"+token+"----->"+instrucciones.get(0).codigo);
					System.out.println("Error detectando fin de IF (END IF):"+token+"----->"+instrucciones.get(instrucciones.size()-1).codigo);
					//System.out.println("Error detectando fin de IF (END IF):"+token);
					for(int i=0;i<30&&fuente.hasNext();i++){
						//System.out.println(fuente.next());
					}
					System.exit(0);
				}
				if(!fuente.next().equals(";")){
					System.out.println("Error detectando fin de IF (END IF;)");
					System.exit(0);
				}
				codigo+="$$END IF;\n";
				/*if(this.condicion.trim().equals("v_estado_referencia = 'INA'")){
					System.out.println(this.getCodigo(0));
				}*/
				break;
			}
			Sentencia sentencia=new Sentencia(token,fuente);
			instrucciones.add(sentencia);
			if(sentencia.codigo==null){
				System.out.println("Sentencia en IF nula:"+token);
				System.exit(0);
			}
			codigo+=sentencia.codigo+"\n";
		}
	}
	public String getCodigo(int nivel){
		String resultado="";
		resultado+=Util.tab(nivel)+"IF "+Util.format(this.condicion).trim()+" THEN\n";
		for(int i=0;i<verdaderas.size();i++){
			resultado+=verdaderas.get(i).getCodigo(nivel+1);
		}
		if(falsas.size()>0){
			boolean ielse=false;
			for(int i=0;i<falsas.size();i++){
				if(!ielse&&(!falsas.get(i).getTipo2().equals("ELSIF"))){
					resultado+=Util.tab(nivel)+"ELSE\n";
					ielse=true;
				}
				resultado+=falsas.get(i).getCodigo(nivel+1);
			}
		}
		resultado+=Util.tab(nivel)+"END IF;\n";
		return resultado;
	}
	public String getTipo(){
		return "IF";
	}
	public String desc(int nivel){
		String resultado="";
		resultado=Util.tab(nivel)+this.getTipo()+" "+this.condicion+"\n";
		for(int i=0;i<verdaderas.size();i++){
			resultado+=verdaderas.get(i).desc(nivel+1);
		}
		if(falsas.size()>0){
			resultado+=Util.tab(nivel)+"ELSE\n";
			for(int i=0;i<falsas.size();i++){
				resultado+=falsas.get(i).desc(nivel+1);
			}
		}
		resultado+=Util.tab(nivel)+"END IF\n\n";
		return resultado;
	}
	public String getHTML()throws Exception{
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		String contenidoSentencias=Util.leer("HTML//Sentencias.html");
		contenido=contenido.replace("Condicion","SELSIF "+this.condicion+" THEN");
		String verdaderasCodigo="";
		String falsasCodigo="";
		for(int i=0;i<verdaderas.size();i++){
			verdaderasCodigo+=contenidoSentencias.replace("Sentencia",verdaderas.get(i).getHTML());
		}
		contenido=contenido.replace("Verdaderas",verdaderasCodigo);
		for(int i=0;i<falsas.size();i++){
			falsasCodigo+=contenidoSentencias.replace("Sentencia",falsas.get(i).getHTML());
		}
		contenido=contenido.replace("Falsas",falsasCodigo);
		return contenido;
	}
}