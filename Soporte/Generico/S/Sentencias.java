import java.util.*;
public class Sentencias extends Estructura{
	public String codigo;
	public String tokenComprobacion;
	public ArrayList<Sentencia>lista;
	public Sentencias(){
		lista=new ArrayList<Sentencia>();
	}
	public static String comprobar(String token,Tokenizer fuente,Sentencias sentencias){
		if(token.toUpperCase().equals("BEGIN")){
			sentencias.tokenComprobacion=token;
			sentencias.codigo="BEGIN\n";
			while(fuente.hasNext()){
				token=fuente.next();
				if(token.toUpperCase().equals("EXCEPTION")){
					break;
				}
				if(token.toUpperCase().equals("END")){
					/*token=fuente.next();
					if(!token.equals(";")){
						System.out.println("Error al determinar fin de bloque");
						System.exit(0);
					}
					sentencias.codigo+="END;\n";*/
					break;
				}
				Sentencia sentencia=new Sentencia(token,fuente);
				sentencias.lista.add(sentencia);
				sentencias.codigo+=sentencia.codigo+"\n";
			}
		}
		return token;
	}
	public String getCodigo(int nivel){
		String resultado=Util.tab(nivel)+"BEGIN\n";
		for(int i=0;i<lista.size();i++){
			resultado+=lista.get(i).getCodigo(nivel+1);
		}
		return resultado;
	}
	public String getTipo(){
		return "SENTENCIAS";
	}
	public String desc(int nivel){
		String resultado="";
		if(!this.getCodigo(nivel).equals("")){
			resultado=Util.tab(nivel)+this.getTipo()+"\n";
			for(int i=0;i<lista.size();i++){
				resultado+=lista.get(i).desc(nivel+1);
			}
		}
		return resultado;
	}
	public String getHTML()throws Exception{
		String resultado="";
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		for(int i=0;i<lista.size();i++){
			resultado+=contenido.replace("Sentencia",lista.get(i).getHTML());
		}
		return resultado;
	}
}