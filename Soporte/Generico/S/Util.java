import java.io.*;
import java.util.*;
public class Util{
	public static String tab(int nivel){
		String resultado="";
		for(int i=0;i<nivel;i++){
			resultado+="  ";
		}
		return resultado;
	}
	public static String format(String cadena){
		String resultado=cadena.trim();
		resultado=resultado.replace("( ","(");
		resultado=resultado.replace(" (","(");
		resultado=resultado.replace(" )",")");
		resultado=resultado.replace(") ",")");
		resultado=resultado.replace(" , ",",");
		resultado=resultado.replace(" :=",":=");
		resultado=resultado.replace(":= ",":=");
		resultado=resultado.replace(" =","=");
		resultado=resultado.replace("= ","=");
		resultado=resultado.replace(" -","-");
		resultado=resultado.replace("- ","-");
		resultado=resultado.replace(" +","+");
		resultado=resultado.replace("+ ","+");
		resultado=resultado.replace(" %","%");
		resultado=resultado.replace("% ","%");
		resultado=resultado.replace(" <>","<>");
		resultado=resultado.replace("<> ","<>");
		resultado=resultado.replace(" >",">");
		resultado=resultado.replace("> ",">");
		resultado=resultado.replace(" <","<");
		resultado=resultado.replace("< ","<");
		resultado=resultado.replace(" ||","||");
		resultado=resultado.replace("|| ","||");
		return resultado.toUpperCase();
	}
	public static String leer(File file)throws Exception{
		String resultado="";
		Scanner s=new Scanner(file);
		while(s.hasNext()){
			resultado+=s.nextLine()+"\n";
		}
		return resultado;
	}
	public static String leer(String nombre)throws Exception{
		return leer(new File(nombre));
	}
}