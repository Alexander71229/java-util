DECLARE
-- HISTORIA DE MODIFICACIONES
-- Fecha        Autor       Observaciones
-- ----------   ----------  --------------------------------------------------------------------------
-- 2014/03/19   Yhernana    REQ_LimpEquiSistInfo (ITPAM #45732):cambio masivo de estado de los equipos
--													que se encuentran en el laboratorio.
-- 2014/04/08   Yhernana    REQ_LimpEquiSistInfo (ITPAM #45732):Limpieza variable para el nuevo equipo
-- 2014/09/23   Jherremo	ITP70458_CambiLimpEqui: Adicionar nuevos valores al log de salida dependiendo del caso.
-- 2015/01/13   Amolinag    ITP80608_DanoLimpEqui: Alexander Molina: Garantizar que se verifiquen tareas que tengan asociados requerimientos sin cumplir.
--                          Validar si existe una solicitud de daño posterior al pedido de nuevo para el equipo.

   -- Declaración de Variables Escalares
   v_archivo_entrada      VARCHAR2 (5000);
   v_linea_archivo        VARCHAR2 (5000);
   v_linea_temp           VARCHAR2 (5000);
   v_descriptor_archivo   text_io.file_type;
   v_mensaje              VARCHAR2 (5000);
   v_mensaje_log_tabla    VARCHAR2 (5000);
   v_texto_equipo         VARCHAR2 (5000);
   v_texto_parte_log      VARCHAR2 (255);
   v_num_linea            NUMBER;
   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732):Declaración de variables
   rg_linea               pkg_masivos_archivo.rec_linea;
   v_pedido               fnx_solicitudes.pedido_id%TYPE;
   v_subpedido            fnx_solicitudes.subpedido_id%TYPE;
   b_plataf_no_cumpl      BOOLEAN;
   v_fecha_ingreso        DATE;
   b_tiene_receq          BOOLEAN;
   b_tiene_retir          BOOLEAN;
   v_msg_proceso          VARCHAR2 (5000);

   b_pedido_nuevo_eq      BOOLEAN;--
   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732):Se requiere el valor del campo tipo_elemento,
   --                                                    el cual será utilizado al momento de invocar el procedimiento PR_CUMPLIR_RECEQ.
   CURSOR c_equipos_lab (
      pc_serial_real   fnx_equipos.serial_real%TYPE,
      pc_mac_real      fnx_equipos.mac_real%TYPE
   )
   IS
      SELECT a.equipo_id, a.marca_id, a.referencia_id, a.tipo_elemento_id,
             a.tipo_elemento, a.estado, b.obsoleto
        FROM fnx_equipos a, fnx_marcas_referencias b
       WHERE a.marca_id = b.marca_id
         AND a.referencia_id = b.referencia_id
         AND a.tipo_elemento_id = b.tipo_elemento_id
         AND a.tipo_elemento = b.tipo_elemento
         AND (a.serial_real = pc_serial_real OR a.mac_real = pc_mac_real);

   rg_equipos_lab         c_equipos_lab%ROWTYPE;

   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732):Se requiere el valor del campo tipo_elemento,
   --                                                    el cual será utilizado al momento de invocar el procedimiento PR_CUMPLIR_RECEQ.
   CURSOR c_equipos_lab_sin_serial (pc_mac_real fnx_equipos.mac_real%TYPE)
   IS
      SELECT a.equipo_id, a.marca_id, a.referencia_id, a.tipo_elemento_id,
             a.tipo_elemento, a.estado, b.obsoleto
        FROM fnx_equipos a, fnx_marcas_referencias b
       WHERE a.marca_id = b.marca_id
         AND a.referencia_id = b.referencia_id
         AND a.tipo_elemento_id = b.tipo_elemento_id
         AND a.tipo_elemento = b.tipo_elemento
         AND a.mac_real = pc_mac_real;

   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732):Se requiere el valor del campo tipo_elemento,
   --                                                    el cual será utilizado al momento de invocar el procedimiento PR_CUMPLIR_RECEQ.
   CURSOR c_equipos_lab_sin_mac (pc_serial_real fnx_equipos.serial_real%TYPE)
   IS
      SELECT a.equipo_id, a.marca_id, a.referencia_id, a.tipo_elemento_id,
             a.tipo_elemento, a.estado, b.obsoleto
        FROM fnx_equipos a, fnx_marcas_referencias b
       WHERE a.marca_id = b.marca_id
         AND a.referencia_id = b.referencia_id
         AND a.tipo_elemento_id = b.tipo_elemento_id
         AND a.tipo_elemento = b.tipo_elemento
         AND a.serial_real = pc_serial_real;

   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732):Se requiere obtener la fecha_ingreso de la orden de RECEQ,
   --                                                    para después verificar si posterior a esa fecha han habido pedidos de NUEVO.
   CURSOR c_soli_recequ (pc_equipo_id fnx_equipos.equipo_id%TYPE)
   IS
      SELECT t.valor, t.pedido_id, t.subpedido_id, t.solicitud_id,
             o.estado_id, o.concepto_id,
                t.pedido_id
             || '-'
             || t.subpedido_id
             || '-'
             || t.solicitud_id pedido_receq,
             o.estado_id || '-' || o.concepto_id concepto_receq,
             fn_valor_caracteristica_sol (t.pedido_id,
                                          t.subpedido_id,
                                          t.solicitud_id,
                                          4427
                                         ) recuperado,
             (o.fecha_recibo - 0.5) fecha_ingreso
        FROM fnx_trabajos_solicitudes t, fnx_ordenes_trabajos o
       WHERE t.pedido_id = o.pedido_id
         AND t.subpedido_id = o.subpedido_id
         AND t.solicitud_id = o.solicitud_id
         AND t.caracteristica_id = 200
         AND t.valor = pc_equipo_id
         AND o.cola_id = 'RECEQ'
         AND o.estado_id IN ('PENDI', 'CUMPL')
         AND t.tipo_trabajo = 'RETIR'
         ORDER BY o.pedido_id DESC;

   rg_soli_recequ         c_soli_recequ%ROWTYPE;

   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732): Se requiere obtener la fecha_ingreso del pedido de retiro,
   --                                                    para después verificar si posterior a esa fecha han habido pedidos de NUEVO.
   --                                                    También se crea variable rg_soli_ret_equ que almacenará el registro retornado por el cursor.
   CURSOR c_soli_ret_equ (pc_equipo_id fnx_equipos.equipo_id%TYPE)
   IS
      SELECT t.valor, t.pedido_id, t.subpedido_id, t.solicitud_id,
             o.estado_id, o.concepto_id,
                t.pedido_id
             || '-'
             || t.subpedido_id
             || '-'
             || t.solicitud_id pedido_receq,
             o.estado_id || '-' || o.concepto_id concepto_receq,
             fn_valor_caracteristica_sol (t.pedido_id,
                                          t.subpedido_id,
                                          t.solicitud_id,
                                          4427
                                         ) recuperado,
             (o.fecha_ingreso - 0.5) fecha_ingreso
        FROM fnx_trabajos_solicitudes t, fnx_solicitudes o
       WHERE o.pedido_id = t.pedido_id
         AND o.subpedido_id = t.subpedido_id
         AND o.solicitud_id = t.solicitud_id
         AND t.caracteristica_id = 200
         AND o.estado_soli = 'DEFIN'
         AND t.tipo_trabajo = 'RETIR'
         AND t.valor = pc_equipo_id
         ORDER BY o.pedido_id DESC;

   rg_soli_ret_equ        c_soli_ret_equ%ROWTYPE;

   CURSOR c_param_equipo (
      pc_estado     fnx_equipos.estado%TYPE,
      pc_obsoleto   fnx_marcas_referencias.obsoleto%TYPE
   )
   IS
      SELECT   SUBSTR (parametro_id, 1, 1) consecutivo,
               SUBSTR (valor, 1, 3) estado_ant, SUBSTR (valor, 5, 1) obsoleto,
               SUBSTR (valor, 7, 3) estado_nuevo,
               SUBSTR (valor, 11, 4) tipo_alerta
          FROM fnx_parametros
         WHERE modulo_id = 'EQUIP_LAB'
           AND SUBSTR (valor, 1, 3) = pc_estado
           AND SUBSTR (valor, 5, 1) = pc_obsoleto
      ORDER BY parametro_id;

   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732): Creación  de cursor para validar si un pedido de retiro de equipos,
   --                                                    tiene asociado solicitud de servicio profesional
   CURSOR c_soli_srvpro (p_pedido IN fnx_solicitudes.pedido_id%TYPE)
   IS
      SELECT   estado_id, concepto_id
          FROM fnx_solicitudes
         WHERE pedido_id = p_pedido
           AND producto_id = 'SRVPRO'
           AND tipo_elemento_id = 'REDIN'
      ORDER BY pedido_id DESC;

   v_soli_srvpro          c_soli_srvpro%ROWTYPE;

   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732): Creación de cursor para obtener las órdenes de plataforma
   --                                                    asociadas al mismo pedido de recuperación de equipo
   CURSOR c_orden_plataf (
      p_pedido      IN   fnx_solicitudes.pedido_id%TYPE,
      p_subpedido   IN   fnx_solicitudes.subpedido_id%TYPE
   )
   IS
      SELECT requerimiento_id, plataforma, actividad_id, tipo_intercambio,
             concepto_orden
        FROM fnx_tareas_plataformas
       WHERE requerimiento_id IN (
                     SELECT DISTINCT (requerimiento_id)
                                FROM fnx_ordenes_trabajos
                               WHERE pedido_id = p_pedido
                                 AND subpedido_id = p_subpedido
                                 AND estado_id <> 'CUMPL'); --20150113:amolinag:ITP80608_DanoLimpEqui:Alexander Molina: Se agrega la condición:{AND estado_id <> 'CUMPL')}

   v_orden_plataf         c_orden_plataf%ROWTYPE;

   --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732): Creación de cursor para obtener el pedido de
   --                                                    NUEVO asociado a un equipo
   CURSOR c_pedido_nuevo_eq (p_equipo_id IN fnx_equipos.equipo_id%TYPE)
   IS
      SELECT   caso.pedido_id, caso.subpedido_id, caso.solicitud_id,
               soli.fecha_ingreso
          FROM fnx_caracteristica_solicitudes caso,
               fnx_trabajos_solicitudes trso,
               fnx_solicitudes soli
         WHERE caso.pedido_id = trso.pedido_id
           AND caso.subpedido_id = trso.subpedido_id
           AND caso.solicitud_id = trso.solicitud_id
           AND caso.pedido_id = soli.pedido_id
           AND caso.subpedido_id = soli.subpedido_id
           AND caso.solicitud_id = soli.solicitud_id
           AND caso.caracteristica_id = trso.caracteristica_id
           AND caso.valor = p_equipo_id
           AND caso.caracteristica_id = 200
           AND trso.tipo_trabajo = 'NUEVO'
      ORDER BY caso.pedido_id DESC;

   v_pedido_nuevo_eq      c_pedido_nuevo_eq%ROWTYPE;

--2015/01/13 Amolinag ITP80608_DanoLimpEqui: Alexander Molina: Cursores para validar si existe un daño asociado al equipo en una solicitud posterior a la solicitud de nuevo.
--INICIO
	CURSOR c_existe_trabajo_posterior_eq(p_pedido_id IN fnx_pedidos.pedido_id%TYPE,p_fecha IN fnx_pedidos.fecha_ingreso%TYPE,p_equipo_id IN fnx_equipos.equipo_id%TYPE)IS
		SELECT 1 FROM fnx_requerimientos_trabajos WHERE identificador_id IN(
		SELECT identificador_id_nuevo FROM fnx_solicitudes WHERE pedido_id=p_pedido_id)
		AND paquete_id=p_equipo_id
		AND etapa_id='MANTE'
		AND fecha_recibo>=p_fecha;
	v_existe_trabajo_posterior_eq c_existe_trabajo_posterior_eq%ROWTYPE;
	CURSOR c_existe_orden_posterior_eq(p_pedido_id IN fnx_pedidos.pedido_id%TYPE,p_fecha IN fnx_pedidos.fecha_ingreso%TYPE,p_equipo_id IN fnx_equipos.equipo_id%TYPE)IS
		SELECT 1 FROM fnx_ordenes_trabajos WHERE requerimiento_id IN(
		SELECT requerimiento_id FROM fnx_requerimientos_trabajos WHERE identificador_id IN(
		SELECT identificador_id_nuevo FROM fnx_solicitudes WHERE pedido_id=p_pedido_id))
		AND etapa_id='MANTE'
		AND observacion LIKE '%'||p_equipo_id||'%'
		AND fecha_recibo>=p_fecha;
	v_existe_orden_posterior_eq c_existe_orden_posterior_eq%ROWTYPE;
--FIN

   FUNCTION fni_predecir_estado (
      pc_estado           fnx_equipos.estado%TYPE,
      pc_obsoleto         fnx_marcas_referencias.obsoleto%TYPE,
      pc_alerta     OUT   VARCHAR2
   )
      RETURN VARCHAR2
   IS
      v_cad   VARCHAR2 (50) := NULL;
   BEGIN
      FOR rg IN c_param_equipo (pc_estado, pc_obsoleto)
      LOOP
         v_cad := v_cad || rg.estado_nuevo || ' -> ';

         IF rg.tipo_alerta = 'ALER'
         THEN
            pc_alerta := '(!)';
         ELSIF rg.tipo_alerta = 'ANCO'
         THEN
            pc_alerta := '($)';
         ELSE
            pc_alerta := NULL;
         END IF;
      END LOOP;

      v_cad := SUBSTR (v_cad, 1, LENGTH (v_cad) - 4);
      RETURN (v_cad);
   END;

-- ----------------------------------
-- Obtener datos por cada linea leida
-- ----------------------------------
   PROCEDURE pr_obtener_datos (
      p_linea       IN       VARCHAR2,
      p_reg_linea   OUT      pkg_masivos_archivo.rec_linea
   )
   IS
      v_linea_temp   VARCHAR2 (5000);
   BEGIN
      v_linea_temp := p_linea;
      pkg_masivos_archivo.pr_extrae_dato (FALSE,
                                          v_linea_temp,
                                          p_reg_linea.v_serial_real
                                         );
      pkg_masivos_archivo.pr_extrae_dato (TRUE,
                                          v_linea_temp,
                                          p_reg_linea.v_mac_real
                                         );
   END;
BEGIN
   -- Crear carpetas para archivos.  No se presentarán excepciones si ya existen.
   pkg_masivos_archivo.pr_crea_carpeta ('D:\Fenix');
   pkg_masivos_archivo.pr_crea_carpeta ('D:\Fenix\CargDato');
   -- Leer archivo de entrada
   v_archivo_entrada :=
      GET_FILE_NAME
         (directory_name      => NULL,
          file_name           => 'EQUIPOS-LAB-001.txt',
          file_filter         => 'Delimitados por Comas (*.CSV)|*.CSV|TODOS (*.*)|*.*|',
          MESSAGE             => 'Archivo de Entrada Gestión Equipos Laboratorio',
          dialog_type         => 1,
          select_file         => TRUE
         );

   IF v_archivo_entrada IS NOT NULL
   THEN
      IF LENGTH (v_archivo_entrada) > 500
      THEN
         :blq_control.dsp_nombre_archivo :=
                  SUBSTR (v_archivo_entrada, LENGTH (v_archivo_entrada) - 500);
      ELSE
         :blq_control.dsp_nombre_archivo := v_archivo_entrada;
      END IF;

      v_descriptor_archivo := text_io.fopen (v_archivo_entrada, 'r');

      IF text_io.is_open (v_descriptor_archivo)
      THEN
         GO_BLOCK ('BLQ_EQUIPOS_LAB');
         v_num_linea := 0;

         LOOP
            BEGIN
               text_io.get_line (v_descriptor_archivo, v_linea_archivo);
               v_num_linea := v_num_linea + 1;
               pr_obtener_datos (v_linea_archivo, rg_linea);
               
               -- 2014/04/08   Yhernana    REQ_LimpEquiSistInfo (ITPAM #45732):Limpieza variable para el nuevo equipo
               rg_equipos_lab:= NULL;

               IF     rg_linea.v_serial_real IS NOT NULL
                  AND rg_linea.v_mac_real IS NOT NULL
               THEN
                  OPEN c_equipos_lab (rg_linea.v_serial_real,
                                      rg_linea.v_mac_real
                                     );

                  FETCH c_equipos_lab
                   INTO rg_equipos_lab;

                  CLOSE c_equipos_lab;
               ELSIF     rg_linea.v_serial_real IS NULL
                     AND rg_linea.v_mac_real IS NOT NULL
               THEN
                  OPEN c_equipos_lab_sin_serial (rg_linea.v_mac_real);

                  FETCH c_equipos_lab_sin_serial
                   INTO rg_equipos_lab;

                  CLOSE c_equipos_lab_sin_serial;
               ELSIF     rg_linea.v_serial_real IS NOT NULL
                     AND rg_linea.v_mac_real IS NULL
               THEN
                  OPEN c_equipos_lab_sin_mac (rg_linea.v_serial_real);

                  FETCH c_equipos_lab_sin_mac
                   INTO rg_equipos_lab;

                  CLOSE c_equipos_lab_sin_mac;
               END IF;

               :blq_equipos_lab.num_linea := v_num_linea;
               :blq_equipos_lab.serial_real := rg_linea.v_serial_real;
               :blq_equipos_lab.mac_real := rg_linea.v_mac_real;
               :blq_equipos_lab.procesado := 'En espera';

							 -- 2014/09/23 Jherremo ITP70458_CambiLimpEqui INICIO CAMBIO2
							 v_pedido := NULL;
               v_subpedido := NULL;
               v_fecha_ingreso := NULL;     
               IF rg_equipos_lab.equipo_id IS NULL
               THEN
                  :blq_equipos_lab.procesado := 'No encontrado';
               ELSE
                  --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732): INICIO
                  b_tiene_receq := FALSE;
                  -- 2014/09/23 Jherremo ITP70458_CambiLimpEqui INICIO CAMBIO3
                  b_tiene_retir := FALSE;
                  rg_soli_recequ := NULL;
                  rg_soli_ret_equ := NULL;

                  --Cursor para verificar pedido de retiro con RECEQ
                  OPEN c_soli_recequ (rg_equipos_lab.equipo_id);

                  FETCH c_soli_recequ
                   INTO rg_soli_recequ;

IF c_soli_recequ%FOUND THEN 
                  :blq_equipos_lab.orden_rec_equipo :=
                                                  rg_soli_recequ.pedido_receq;
                  :blq_equipos_lab.estado_concepto_orden :=
                                                rg_soli_recequ.concepto_receq;
                  :blq_equipos_lab.estado_id := rg_soli_recequ.estado_id;
                  :blq_equipos_lab.concepto_id := rg_soli_recequ.concepto_id;
                  :blq_equipos_lab.recuperado := rg_soli_recequ.recuperado;
                  v_pedido := rg_soli_recequ.pedido_id;
                  v_subpedido := rg_soli_recequ.subpedido_id;
                  v_fecha_ingreso := rg_soli_recequ.fecha_ingreso;
                  b_tiene_receq := TRUE;

				  END IF;
                  CLOSE c_soli_recequ;

                  b_tiene_retir := FALSE;

                 IF NOT b_tiene_receq THEN 
                  --Cursor para verificar pedido de retiro sin RECEQ
                  IF rg_soli_recequ.pedido_receq IS NULL
                  THEN
                     OPEN c_soli_ret_equ (rg_equipos_lab.equipo_id);

                     FETCH c_soli_ret_equ
                      INTO rg_soli_ret_equ;

  	                    IF c_soli_ret_equ%FOUND THEN
                     :blq_equipos_lab.orden_rec_equipo :=
                                                 rg_soli_ret_equ.pedido_receq;
                     :blq_equipos_lab.estado_concepto_orden :=
                                               rg_soli_ret_equ.concepto_receq;
                     :blq_equipos_lab.estado_id := rg_soli_ret_equ.estado_id;
                     :blq_equipos_lab.concepto_id :=
                                                  rg_soli_ret_equ.concepto_id;
                     :blq_equipos_lab.recuperado :=
                                                   rg_soli_ret_equ.recuperado;
                     v_pedido := rg_soli_ret_equ.pedido_id;
                     v_subpedido := rg_soli_ret_equ.subpedido_id;
                     v_fecha_ingreso := rg_soli_ret_equ.fecha_ingreso;
                     b_tiene_retir := TRUE;

END IF;
                     CLOSE c_soli_ret_equ;
                  
				  END IF;
                  END IF;

                  :blq_equipos_lab.equipo_id := rg_equipos_lab.equipo_id;
                  :blq_equipos_lab.marca_id := rg_equipos_lab.marca_id;
                  :blq_equipos_lab.referencia_id :=
                                                  rg_equipos_lab.referencia_id;
                  :blq_equipos_lab.tipo_elemento_id :=
                                               rg_equipos_lab.tipo_elemento_id;
                  :blq_equipos_lab.estado := rg_equipos_lab.estado;
                  :blq_equipos_lab.obsoleto := rg_equipos_lab.obsoleto;
                  :blq_equipos_lab.nuevo_estado :=
                     fni_predecir_estado (rg_equipos_lab.estado,
                                          rg_equipos_lab.obsoleto,
                                          :blq_equipos_lab.indicador_alerta
                                         );

                  -- Equipo sin pedido de retiro. El equipo pudo haber llegado al laboratorio por un Daño o por un cambio
                  IF :blq_equipos_lab.orden_rec_equipo IS NULL
                  THEN
                     :blq_equipos_lab.indicador_alerta := '(sin_retir)';
                  --Se debe permitir actualizar el equipo sin ningún inconveniente.
                  ELSE
                     -- Equipo con retiro asociado que no se encuentra Cumplido
                     IF :blq_equipos_lab.estado_id NOT IN ('CUMPL', 'FACTU')
                     THEN
                        --Retiro con RECEQ sin cumplir
                        IF b_tiene_receq
                        THEN
                           v_msg_proceso := NULL;
                           --Se invoca cumplido de orden de RECEQ, sin alterar estado de equipos
                           pr_cumplir_receq (v_pedido,
                                             v_subpedido,
                                             rg_soli_recequ.solicitud_id,
                                             rg_equipos_lab.equipo_id,
                                             rg_equipos_lab.marca_id,
                                             rg_equipos_lab.referencia_id,
                                             rg_equipos_lab.tipo_elemento_id,
                                             rg_equipos_lab.tipo_elemento,
                                             'BUENO',
                                             'S',
                                             v_msg_proceso
                                            );

                           IF v_msg_proceso IS NULL
                           THEN
                              --Alerta que permite actualizar estado del equipo
                              :blq_equipos_lab.indicador_alerta :=
                                                               '(rec_pen_ok)';
                           ELSE
                              --Alerta que NO permite actualizar estado del equipo
                              :blq_equipos_lab.indicador_alerta :=
                                                             '(rec_pen_fail)';
                           END IF;
                        --Solicitud de Retiro sin cumplir y que no tiene RECEQ asociado
                        ELSIF b_tiene_retir
                        THEN
                           :blq_equipos_lab.indicador_alerta := '(no_receq)';
                        --Alerta que no permite actualizar estado del equipo.
                        END IF;
                     END IF;
                  END IF;

                  --Si el equipo se encuentra en estado OCU, NO se permitirá la actualización del equipo.
                  IF :blq_equipos_lab.estado = ('OCU')
                  THEN
                     :blq_equipos_lab.indicador_alerta := '(equ_ocu)';
                  END IF;

                  --Si el equipo se encuentra en estado ECC, NO se permitirá la actualización del equipo.
                  IF :blq_equipos_lab.estado = ('ECC')
                  THEN
                     :blq_equipos_lab.indicador_alerta := '(equ_ecc)';
                  END IF;

                  --Si el pedido de retiro del equipo tiene asociado un SRVPRO(servicio profesional) diferente
                  --de FACTU o CUMPL, NO se permitirá la actualización del equipo.
                  OPEN c_soli_srvpro (v_pedido);

                  FETCH c_soli_srvpro
                   INTO v_soli_srvpro;

                  IF c_soli_srvpro%FOUND
                  THEN
                     IF v_soli_srvpro.concepto_id NOT IN ('FACTU', 'CUMPL')
                     THEN
                        :blq_equipos_lab.indicador_alerta := '(srvpro)';
                     END IF;
                  END IF;

                  CLOSE c_soli_srvpro;

                  --Si dentro del pedido de retiro donde se encuentra la RECEQ hay órdenes de plataforma sin cumplir,
                  --NO se permitirá la actualización del equipo.
                  b_plataf_no_cumpl := FALSE;

                  
				  v_orden_plataf := NULL;
                  v_pedido_nuevo_eq:= NULL;
                  v_soli_srvpro:= NULL;


                  IF v_pedido IS NOT NULL THEN
					OPEN c_orden_plataf (v_pedido, v_subpedido);
					 LOOP
                     FETCH c_orden_plataf
                      INTO v_orden_plataf;

                     EXIT WHEN c_orden_plataf%NOTFOUND OR b_plataf_no_cumpl;

                     IF v_orden_plataf.concepto_orden <> 'CUMPL'
                     THEN
                        :blq_equipos_lab.indicador_alerta := '(ret_plat)';
                        b_plataf_no_cumpl := TRUE;
						-- 2014/09/23 Jherremo ITP70458_CambiLimpEqui INICIO CAMBIO4
                        :blq_equipos_lab.valor_adic := 'Requerimiento: '||v_orden_plataf.requerimiento_id;
                     END IF;
                  
				  END LOOP;
                  CLOSE c_orden_plataf;

END IF;
				IF NOT b_plataf_no_cumpl THEN
                  --Si posterior al pedido de retiro, existe un pedido de Nuevo con el mismo equipo,
                  --NO se permitirá la actualización del equipo.
				  b_pedido_nuevo_eq := FALSE;
                  OPEN c_pedido_nuevo_eq (:blq_equipos_lab.equipo_id);

                  FETCH c_pedido_nuevo_eq
                   INTO v_pedido_nuevo_eq;

                  IF     c_pedido_nuevo_eq%FOUND
                     AND (v_pedido_nuevo_eq.fecha_ingreso > v_fecha_ingreso)
                  THEN

										--2015/01/13 Amolinag ITP80608_DanoLimpEqui: Alexander Molina: Cursores para validar si existe un daño asociado al equipo en una solicitud posterior a la solicitud de nuevo.
										--INICIO
										OPEN c_existe_trabajo_posterior_eq(v_pedido_nuevo_eq.pedido_id,v_pedido_nuevo_eq.fecha_ingreso,:blq_equipos_lab.equipo_id);
										FETCH c_existe_trabajo_posterior_eq INTO v_existe_trabajo_posterior_eq;
										CLOSE c_existe_trabajo_posterior_eq;
										IF c_existe_trabajo_posterior_eq%NOTFOUND THEN
											OPEN c_existe_orden_posterior_eq(v_pedido_nuevo_eq.pedido_id,v_pedido_nuevo_eq.fecha_ingreso,:blq_equipos_lab.equipo_id);
											FETCH c_existe_orden_posterior_eq INTO v_existe_orden_posterior_eq;
											CLOSE c_existe_orden_posterior_eq;
											IF c_existe_orden_posterior_eq%NOTFOUND THEN
		
		                   :blq_equipos_lab.indicador_alerta := '(nue_plat)';
		                
					   
					   -- 2014/09/23 Jherremo ITP70458_CambiLimpEqui INICIO CAMBIO5
		                   :blq_equipos_lab.valor_adic := 'Pedido nuevo: '||v_pedido_nuevo_eq.pedido_id ||' Pedido Retiro: '|| v_pedido;
		                   b_pedido_nuevo_eq := TRUE;                    

											END IF;
										END IF;
										--FIN
                  END IF;
				  CLOSE c_pedido_nuevo_eq;
            
			IF NOT b_pedido_nuevo_eq AND v_pedido IS NOT NULL THEN
                                --Si el pedido de retiro del equipo tiene asociado un SRVPRO(servicio profesional) diferente
                                --de FACTU o CUMPL, NO se permitirá la actualización del equipo.
                                OPEN c_soli_srvpro (v_pedido);
                                FETCH c_soli_srvpro
                                INTO v_soli_srvpro;
                                IF c_soli_srvpro%FOUND THEN
                                    IF v_soli_srvpro.concepto_id NOT IN ('FACTU', 'CUMPL') THEN
                                       :blq_equipos_lab.indicador_alerta := '(srvpro)';
                                       -- 2014/09/23 Jherremo ITP70458_CambiLimpEqui INICIO CAMBIO6
                                       :blq_equipos_lab.valor_adic := 'Pedido: '||v_pedido;
                                    END IF;
                                END IF;
                                CLOSE c_soli_srvpro;
                     END IF;                  
                  END IF; 
               --2014/03/19        Yhernana        REQ_LimpEquiSistInfo (ITPAM #45732): FIN
               END IF;

               :blq_control.cantidad_lineas := v_num_linea;
               NEXT_RECORD;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  EXIT;
               WHEN OTHERS
               THEN
                  msg_alert ('ERROR RECUPERA ARCHIVO EQUIPOS: ' || SQLERRM,
                             'I',
                             FALSE
                            );
                  EXIT;
            END;
         END LOOP;

         DELETE_RECORD;
         text_io.fclose (v_descriptor_archivo);
         FIRST_RECORD;
      ELSE
         msg_alert ('No se abrió el archivo de equipos a procesar',
                    'I',
                    FALSE
                   );
      END IF;
   END IF;
END;