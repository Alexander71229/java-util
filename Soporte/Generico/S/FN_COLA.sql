FUNCTION FN_COLA (
  p_servicio       IN   fnx_actividades_productos.servicio_id%TYPE
, p_producto       IN   fnx_actividades_productos.producto_id%TYPE
, p_tecnologia     IN   fnx_actividades_productos.tecnologia_id%TYPE
, p_tipo_trabajo   IN   fnx_actividades_productos.tipo_trabajo%TYPE
, p_etapa          IN   fnx_actividades_productos.etapa_id%TYPE
, p_actividad      IN   fnx_actividades_productos.actividad_id%TYPE
, p_empresa        IN   fnx_colas_productos.empresa_id%TYPE
, p_municipio      IN   fnx_colas_productos.municipio_id%TYPE
, p_criterio       IN   fnx_colas_productos.criterio%TYPE
, p_valor          IN   fnx_colas_productos.valor%TYPE
)
  RETURN VARCHAR2
IS
  v_cola  fnx_colas_trabajos.cola_id%TYPE;
BEGIN
--Modificado por: Mary Luz Aristizábal M. Julio 2007  (MULTI-EMPRESA)
  DBMS_OUTPUT.put_line ('****************FN_COLA****************');
  DBMS_OUTPUT.put_line (   '  etapa_id      = '     || p_etapa
                        || ' AND    actividad_id  = '|| p_actividad
                        || 'AND    servicio_id   = ' || p_servicio
                        || 'AND    producto_id   = ' || p_producto
                        || 'AND    tecnologia_id = ' || p_tecnologia
                        || 'AND    tipo_trabajo  = ' || p_tipo_trabajo
                        || ' p_empresa ' ||  p_empresa
                        || ' p_municipio ' ||  p_municipio
                        || ' p_criterio ' ||  p_criterio
                        || '      p_valor ' ||  p_valor
                         );

  IF p_criterio is not null
  THEN
    --Agiralp 2010-02-05 Validacion para Obtener las Colas ADSLC2 o ADSLC3 de INTER Comercial
    IF (p_criterio = 'SEG2' OR p_criterio = 'SEG3') THEN
        DBMS_OUTPUT.put_line ('1 Criterio = p_valor' || p_valor);
        SELECT cola_id
        INTO v_cola
        FROM fnx_colas_productos
       WHERE etapa_id = p_etapa
         AND actividad_id = p_actividad
         AND servicio_id = p_servicio
         AND producto_id = p_producto
         AND tecnologia_id = p_tecnologia
         AND tipo_trabajo = p_tipo_trabajo
         AND empresa_id = p_empresa
         AND municipio_id =p_municipio
         AND criterio = p_criterio
         --AND valor = p_valor
         AND INSTR(valor, 'C') >0
         AND cola_id <> 'CPROG'
         AND ROWNUM = 1;
    ELSE
        BEGIN
          DBMS_OUTPUT.put_line ('2 Criterio = p_valor' || p_valor);

          SELECT cola_id
            INTO v_cola
            FROM fnx_colas_productos
           WHERE etapa_id = p_etapa
             AND actividad_id = p_actividad
             AND servicio_id = p_servicio
             AND producto_id = p_producto
             AND tecnologia_id = p_tecnologia
             AND tipo_trabajo = p_tipo_trabajo
             AND empresa_id = p_empresa
             AND municipio_id =p_municipio
             AND criterio = p_criterio
             --AND valor = p_valor
             AND INSTR(valor, p_valor) >0
             AND cola_id <> 'CPROG'
             AND ROWNUM = 1;
        EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
            SELECT cola_id
              INTO v_cola
              FROM fnx_colas_productos
             WHERE etapa_id = p_etapa
               AND actividad_id = p_actividad
               AND servicio_id = p_servicio
               AND producto_id = p_producto
               AND tecnologia_id = p_tecnologia
               AND tipo_trabajo = p_tipo_trabajo
               AND empresa_id = NVL (p_empresa, 'UNE')
               AND municipio_id =p_municipio
               AND criterio IS NULL
               AND cola_id <> 'CPROG'
               AND ROWNUM = 1;
         END;
    END IF;
  ELSE--  p_criterio IS NULL THEN

        BEGIN
              SELECT cola_id
              INTO v_cola
              FROM fnx_colas_productos
             WHERE etapa_id = p_etapa
               AND actividad_id = p_actividad
               AND servicio_id = p_servicio
               AND producto_id = p_producto
               AND tecnologia_id = p_tecnologia
               AND tipo_trabajo = p_tipo_trabajo
               AND empresa_id = NVL (p_empresa, 'UNE')
               AND municipio_id =p_municipio
               AND criterio IS NULL
               AND cola_id <> 'CPROG'
               AND ROWNUM = 1;


        END;
  END IF;

  DBMS_OUTPUT.put_line ('FN_cola v_cola ' || v_cola);
  RETURN (v_cola);
EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
    RETURN (NULL);
END;