import java.util.*;
public class Excepcion extends Estructura{
	public String condicion;
	public ArrayList<Sentencia>sentencias;
	public Excepcion(Tokenizer fuente){
		condicion="";
		String token="";
		sentencias=new ArrayList<Sentencia>();
		while(fuente.hasNext()){
			token=fuente.next();
			if(token.toUpperCase().equals("THEN")){
				break;
			}
			condicion+=" "+token;
		}
		while(fuente.hasNext()&&(!fuente.ver().toUpperCase().equals("END"))&&(!fuente.ver().toUpperCase().equals("WHEN"))){
			sentencias.add(new Sentencia(fuente.next(),fuente));
		}
	}
	public String getCodigo(int nivel){
		String resultado="";
		resultado+=Util.tab(nivel)+"WHEN "+Util.format(this.condicion).trim()+" THEN\n";
		for(int i=0;i<sentencias.size();i++){
			resultado+=sentencias.get(i).getCodigo(nivel+1);
		}
		return resultado;
	}
	public String getTipo(){
		return "EXCEPCI�N";
	}
	public String desc(int nivel){
		String resultado="";
		if(!this.getCodigo(nivel).equals("")){
			resultado=Util.tab(nivel)+this.getTipo()+"\n";
			/*for(int i=0;i<sentencias.size();i++){
				resultado+=sentencias.get(i).desc(nivel+1);
			}*/
		}
		return resultado;
	}
	public String getHTML()throws Exception{
		return this.getCodigo(0);
	}
}