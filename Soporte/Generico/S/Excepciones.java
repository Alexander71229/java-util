import java.util.*;
public class Excepciones extends Estructura{
	public String codigo;
	public ArrayList<Excepcion>excepciones;
	public Excepciones(){
		this.codigo="";
		this.excepciones=new ArrayList<Excepcion>();
	}
	public static String comprobar(String token,Tokenizer fuente,Excepciones excepciones){
		if(token.toUpperCase().equals("EXCEPTION")){
			excepciones.codigo="EXCEPTION";
			while(fuente.hasNext()){
				token=fuente.next();
				if(token.toUpperCase().equals("END")){
					/*if(!fuente.next().equals(";")){
						System.out.println("Error al determinar el fin de las excepciones");
						System.exit(0);
					}*/
					break;
				}
				if(token.toUpperCase().equals("WHEN")){
					excepciones.excepciones.add(new Excepcion(fuente));
				}
				excepciones.codigo+=" "+token;
			}
		}
		return token;
	}
	public String getCodigo(int nivel){
		if(excepciones.size()==0){
			return "";
		}
		String resultado=Util.tab(nivel)+"EXCEPTION\n";
		for(int i=0;i<excepciones.size();i++){
			resultado+=excepciones.get(i).getCodigo(nivel+1);
		}
		return resultado;
	}
	public String getTipo(){
		return "EXCEPCIONES";
	}
	public String desc(int nivel){
		String resultado="";
		if(!this.getCodigo(nivel).equals("")){
			resultado=Util.tab(nivel)+this.getTipo()+"\n";
		}
		return resultado;
	}
	public String getHTML()throws Exception{
		String resultado="";
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		for(int i=0;i<excepciones.size();i++){
			resultado+=contenido.replace("Excepcion",excepciones.get(i).getHTML());
		}
		return resultado;
	}
}