PROCEDURE       pr_ins_trabajos_instalacion (
   p_pedido                IN   fnx_solicitudes.pedido_id%TYPE
 , p_subpedido             IN   fnx_solicitudes.subpedido_id%TYPE
 , p_solicitud             IN   fnx_solicitudes.solicitud_id%TYPE
 , p_tipo_solicitud        IN   fnx_reportes_instalacion.tipo_solicitud%TYPE
 , p_fecha_ingreso         IN   fnx_solicitudes.fecha_ingreso%TYPE
 , p_servicio              IN   fnx_solicitudes.servicio_id%TYPE
 , p_producto              IN   fnx_solicitudes.producto_id%TYPE
 , p_tipo_elemento         IN   fnx_solicitudes.tipo_elemento%TYPE
 , p_tipo_elemento_id      IN   fnx_solicitudes.tipo_elemento_id%TYPE
 , p_identificador_id      IN   fnx_solicitudes.identificador_id%TYPE
 , p_identificador_nuevo   IN   fnx_solicitudes.identificador_id%TYPE
 , p_tecnologia            IN   fnx_solicitudes.tecnologia_id%TYPE
 , p_fecha_cita            IN   fnx_solicitudes.fecha_cita%TYPE
 , p_hora_cita             IN   fnx_solicitudes.hora_cita%TYPE
 , p_naturaleza            IN   fnx_fallas.falla_id%TYPE
 , p_empresa               IN   fnx_servicios_productos.empresa_id%TYPE
 , p_municipio             IN   fnx_servicios_productos.municipio_id%TYPE
 )
 IS

 --Variable para crear requerimientos_trabajos
   v_usuario              fnx_requerimientos_trabajos.usuario%TYPE;
   v_requerimiento        fnx_requerimientos_trabajos.requerimiento_id%TYPE;
   v_concepto             fnx_requerimientos_trabajos.concepto_id%TYPE;

 --Variables que se modifican
   v_fecha_cita           fnx_solicitudes.fecha_cita%TYPE;
   v_hora_cita            fnx_solicitudes.hora_cita%TYPE;
   v_identificador_reqt   fnx_solicitudes.identificador_id%TYPE;
   v_tecnologia_reqt      fnx_solicitudes.tecnologia_id%TYPE;
   v_identificador_ortr  fnx_solicitudes.identificador_id%TYPE;
   v_tecnologia_ortr      fnx_solicitudes.tecnologia_id%TYPE;

   v_tipo_solicitud       fnx_reportes_instalacion.tipo_solicitud%TYPE;
   v_cita                 fnx_requerimientos_trabajos.cita%TYPE;
   v_agrupador_comercial  fnx_requerimientos_trabajos.agrupador_comercial%TYPE;
   v_agrupador            fnx_subpedidos.identificador_id%TYPE;
   v_cliente              fnx_identificadores.cliente_id%TYPE;
   v_tipo_categoria       fnx_clientes.cliente_id%TYPE;
   v_valor                fnx_caracteristica_solicitudes.valor%TYPE;
   v_subtipo              fnx_solicitudes.tecnologia_id%TYPE;
   v_ident_paquete        fnx_solicitudes.identificador_id%TYPE;
   v_tipo_equipo          fnx_caracteristica_solicitudes.valor%TYPE;
   v_tipo_paquete         fnx_trabajos_solicitudes.valor%TYPE;
   v_tipo_requerimiento   fnx_requerimientos_trabajos.tipo_requerimiento%TYPE;
   v_etapa                fnx_requerimientos_trabajos.etapa_id%TYPE;
   v_actividad            fnx_requerimientos_trabajos.actividad_id%TYPE;

   v_traslado             VARCHAR2 (2)                                 := 'NO';
   v_suspe_recon          VARCHAR2 (5)                                 := 'NO';
  -- 20131029    --    JVASCO        --Variable requerida para voz en la nube 
   v_suspe_recon_tra      VARCHAR2 (5)                                 := 'NO';

   v_valor_pquete fnx_caracteristica_solicitudes.valor%TYPE:= NULL;
   v_tecno_iptv_emp        fnx_solicitudes.tecnologia_id%TYPE;
   
   -- [Inicio REQ_SUSPECORELANTOLAN] - dmartinez* - 12/08/2014 - Variables para control de actividad RECEQ
    v_observacion_receq     VARCHAR2(700);
    v_cant_receq            NUMBER(2);
    -- [Fin REQ_SUSPECORELANTOLAN] - dmartinez*
   
   -- 20140521    JVASCO    -- Se agrega cursor para buscar la tecnología del instip asociado
   CURSOR c_tecno_iptv_emp
    IS
        SELECT TECNOLOGIA_ID 
          FROM FNX_IDENTIFICADORES
         WHERE AGRUPADOR_ID = p_identificador_id
           AND TIPO_ELEMENTO_ID = 'INSTIP'
           AND ESTADO = 'OCU'
           AND ROWNUM = 1;


 BEGIN
    v_tecno_iptv_emp := NULL;
    
    OPEN c_tecno_iptv_emp;
    FETCH c_tecno_iptv_emp INTO v_tecno_iptv_emp;
    CLOSE c_tecno_iptv_emp;

 --MARISTIM Julio 2007  modificado para FENIX-MULTIEMPRESA

   DBMS_OUTPUT.put_line ('****** <pr_ins_trabajos_instalacion>'
   || ', p_identificador_id ' || p_identificador_id
   || ', p_tecnologia ' || p_tecnologia
   || ','  || p_servicio
   || ','  || p_producto
   || ','  || p_tipo_elemento
   || ','  || p_tipo_elemento_id);


   v_usuario := fn_usuario;
   v_requerimiento := fn_nro_requerimiento;
   v_cliente := fn_cliente_pedido (p_pedido);
   v_agrupador_comercial := fn_agrupador_subpedido (p_pedido, p_subpedido);
   v_agrupador := fn_identificador_subpedido (p_pedido, p_subpedido);
   v_concepto := 'PPRG';
   pr_datos_cliente (v_cliente, v_tipo_categoria);

    --v_ident_paquete := fn_valor_caract_subelem (p_pedido, p_subpedido, 'PQUETE', 1);
    BEGIN
      SELECT valor
      INTO   v_ident_paquete
      FROM   fnx_caracteristica_solicitudes
      WHERE  pedido_id             = p_pedido
      AND    caracteristica_id     = 1
      AND    TIPO_ELEMENTO_ID      = 'PQUETE'
      AND    ROWNUM = 1;
    EXCEPTION WHEN OTHERS THEN
       v_ident_paquete:=NULL;
    END;

    -- 2009-10-21 Truizd - SAC TRASLADO PQUETE
    -- Si el identificador es null se verifica si existe un traslado de pquete para
    -- obtener este identificador

       IF v_ident_paquete IS NULL THEN

            BEGIN
            SELECT valor
              INTO v_valor_pquete
              FROM fnx_caracteristica_solicitudes
             WHERE pedido_id = p_pedido AND caracteristica_id = 4371 AND ROWNUM = 1;

            EXCEPTION WHEN OTHERS THEN
               v_valor_pquete :=NULL;
            END;

            IF v_valor_pquete IS NOT NULL
            THEN
              v_ident_paquete:= v_valor_pquete;
            END IF;

      END IF;

      dbms_output.put_line('v_ident_paquete 1: '||v_ident_paquete);

    --v_tipo_paquete  := fn_valor_caract_subpedido (p_pedido, p_subpedido, '2878');
    BEGIN
     SELECT valor
      INTO   v_tipo_paquete
      FROM   fnx_caracteristica_solicitudes
      WHERE  pedido_id             = p_pedido
      AND    caracteristica_id     = 2878
      AND    TIPO_ELEMENTO_ID      = 'PQUETE'
      AND    ROWNUM = 1;
    EXCEPTION WHEN OTHERS THEN
       v_tipo_paquete  :=NULL;
    END;


    -- Verificar si tiene cita
    IF p_fecha_cita IS NOT NULL
    THEN
       v_fecha_cita := p_fecha_cita;
       v_hora_cita := p_hora_cita;
       v_cita := 'S';
    ELSE
          IF p_tipo_solicitud IN( 'REFAL' ) THEN

                 IF p_producto IN ('INTCON','INTDED')
                 THEN
                    v_fecha_cita := TRUNC(SYSDATE);
                    v_hora_cita  := '08:00';
                    v_cita             := 'S';
                 END IF;

         ELSE
               IF p_producto IN ('INTER', '3PLAY')
               THEN   --LMZG 26/06/03 Asignacion de la fecha de cita para 3PLAY
                 v_fecha_cita := TRUNC (SYSDATE);
                 v_hora_cita := '08:00';
                 v_cita := 'S';
               END IF;
         END IF;

    END IF;




 ---------------------------------------------------------------------------
 --Verificar el tipo de solicitud y tipo de Cambio (si es un traslado u otro  tipo de cambio)
 -- ya que es necesario identificar el traslado para asignarle la cola respectiva.
   v_tipo_solicitud := p_tipo_solicitud;
   dbms_output.put_line('v_tipo_solicitud: '||v_tipo_solicitud);
   IF p_tipo_solicitud IN ('CAMBI', 'NUEVO')
   THEN
         IF p_producto = 'TELEV'
         THEN
           v_traslado :=
                     fn_verificar_traslado_tv (p_pedido, p_subpedido, p_solicitud);
         ELSE
           v_traslado :=
                        fn_verificar_traslado (p_pedido, p_subpedido, p_solicitud);
         END IF;

         IF v_traslado = 'SI'
         THEN
           v_tipo_solicitud := 'TRASL';
         ELSE
           v_tipo_solicitud := p_tipo_solicitud;
         END IF;

         --Verificar Suspensión o Reconexion del Servicio, para determinar el
         --tipo de trabajo independietne de la actividad
         IF v_tipo_solicitud = 'CAMBI'
         THEN
         
           v_suspe_recon :=
                     fn_verificar_suspe_recon (p_pedido, p_subpedido, p_solicitud);
           v_tipo_solicitud := NVL( v_suspe_recon,p_tipo_solicitud);
           
           /*Cmurillg 2013-08-20 Verificación de suspensión o reconexión para voz en la nube*/           
           IF p_producto = 'VOZNUB' THEN
             v_suspe_recon := FN_VALOR_CARACT_SUBPEDIDO(p_pedido, p_subpedido,132);
             v_suspe_recon_tra := fn_valor_trabaj_subelem(p_pedido, p_subpedido,'VOZNUB',132);
             
             IF NVL(v_suspe_recon,'X') IN ('SXOS','SXFP','SFPS', 'SXLT','SPRP', 'SXAC','SXFR') 
                AND v_suspe_recon_tra = 'ACTI' THEN
                v_tipo_solicitud  := 'SUSPE';
             ELSIF NVL(v_suspe_recon,'X') IN ('ACTI','AXFP') AND v_suspe_recon_tra <> 'ACTI' THEN
               v_tipo_solicitud := 'RECON';
           
             END IF;
           END IF;
           
           
         END IF;  

   ELSIF v_tipo_solicitud IN( 'REFAL','REGEN') THEN

        --  Asignar el identificador
        IF p_identificador_id  IS NOT NULL THEN
            v_identificador_reqt := p_identificador_id ;
        ELSE                                                                      -- Corresponde a un Reclamo General
             v_identificador_reqt:= 'RG-'||p_pedido;
            IF p_producto = 'TELEV' THEN
                v_tecnologia_reqt    := 'HFC';
            ELSE
                v_tecnologia_reqt    := 'REDCO';
            END IF;
        END IF;

   END IF;

 --FIN p_tipo_solicitud
 ---------------------------------------------------------------------------



   --identificador y tecnologia para el fnx_requerimientos_trabajos
   IF p_producto = 'ATENC'
   THEN
         v_identificador_reqt := NVL (p_identificador_id, 'ATENC');
         v_tecnologia_reqt :=  p_tecnologia  ;--FN_TECNOLOGIA_IDENTIF (v_identificador_reqt);
         BEGIN
           UPDATE fnx_reportes_atenciones
              SET requerimiento_id = v_requerimiento
            WHERE pedido_id = p_pedido
              AND subpedido_id = p_subpedido
              AND solicitud_id = p_solicitud;
         END;
   ELSIF v_tipo_solicitud IN( 'REFAL','REGEN') THEN

        dbms_output.put_line('DAÑO v_tipo_solicitud:: '||v_tipo_solicitud
            || ' p_identificador_id' || p_identificador_id || ' p_tecnologia' || p_tecnologia
            || ','  || p_servicio
            || ','  ||p_producto
            || ','  ||p_tipo_elemento
            || ','  ||p_tipo_elemento_id);

       v_tecnologia_reqt := fn_ident_tecno_ortr (p_pedido
                                , p_subpedido
                                , p_solicitud
                                , p_identificador_id
                                , p_identificador_nuevo
                                , p_tecnologia
                                , v_tipo_solicitud
                                , p_servicio
                                , p_producto
                                , p_tipo_elemento
                                , p_tipo_elemento_id
                                , 'ACT'
                                , v_identificador_reqt
                                , v_tecnologia_reqt
                                , v_identificador_reqt
                                , v_tecnologia_reqt
                                , v_identificador_ortr
                                , v_tecnologia_ortr
                                 );

        v_identificador_reqt :=  p_identificador_id;
        v_tecnologia_reqt := v_tecnologia_ortr;

   ELSE
         IF p_identificador_nuevo IS NOT NULL
         THEN
                v_identificador_reqt :=  p_identificador_nuevo;
                v_tecnologia_reqt := p_tecnologia;
         ELSIF p_producto = 'TV' AND v_tipo_solicitud IN ( 'RETIR', 'SUSPE', 'RECON') AND p_tipo_elemento_id = 'TV' AND FN_VALOR_CARACT_IDENTIF (p_identificador_id,207) = 'IPTV' THEN
                v_tecnologia_reqt := v_tecno_iptv_emp;
                v_identificador_reqt := p_identificador_id ;
         ELSE
             v_identificador_reqt := p_identificador_id ;
             v_tecnologia_reqt := NVL(p_tecnologia,FN_TECNOLOGIA_IDENTIF (v_identificador_reqt));
         END IF;
   END IF;

   IF v_tipo_solicitud IN( 'REFAL','REGEN') THEN

       v_tipo_requerimiento:='MANTE';
       -- Identificar la etapa y la actividad inicial de requerimiento
       PR_DATOS_ETAPAS (p_servicio
                   , p_producto
                   , v_tecnologia_ortr
                   , v_tipo_solicitud
                   , 'UNE'
                   , 'MEDANTCOL'
                   , v_etapa
                   , v_actividad
                   );
       IF v_tipo_solicitud  = 'REGEN' THEN
            v_tecnologia_reqt:='REDCO';
       END IF;
   dbms_output.put_line('v_tipo_requerimiento: '||v_tipo_requerimiento);

   ELSIF p_producto ='ATENC' THEN
       v_tipo_requerimiento:=  'ATENC' ;
       v_etapa:='INSTA';
       v_actividad:='INSTA';

   ELSE
       v_tipo_requerimiento:=  'INSTA';
       v_etapa:='INSTA';
       v_actividad:='INSTA';
   END IF;



 -- Crear el requerimiento de trabajo
   BEGIN
    --IMPORTANTE
    -- El requerimiento se crea con el identificador de la solicitud,
    -- al momento de generar las ordenes se hace el cambio del identificador
    -- de acuerdo a las diferente reglas
     dbms_output.put_line('la hora de ingreso es !!!!!!:' ||TO_CHAR (p_fecha_ingreso, 'HH24:MI'));
     dbms_output.put_line('v_ident_paquete 2: '||v_ident_paquete);

     INSERT INTO fnx_requerimientos_trabajos
                 (requerimiento_id
                , tipo_requerimiento
                , identificador_id
                , servicio_id
                , producto_id
                , tecnologia_id
                , tipo_trabajo
                , tipo_cliente
                , tipo_fuente
                , etapa_id
                , actividad_id
                , fecha_etapa
                , estado_id
                , concepto_id
                , cliente_id
                , fecha_ingreso
                , fecha_recibo
                , agrupador_comercial
                , agrupador_servicio
                , fecha_entrega
                , fecha_compromiso
                , hora_compromiso
                , pedido_id
                , subpedido_id
                , solicitud_id
                , hora_ingreso
                , hora_entrega
                , hora_recibo
                , usuario, cita
                , paquete_id
                , empresa_id
                , municipio_id
                 )
     VALUES      (v_requerimiento
                , v_tipo_requerimiento
                , v_identificador_reqt
                , p_servicio
                , p_producto
                , v_tecnologia_reqt
                , v_tipo_solicitud
                , v_tipo_categoria
                , 'CLIEN'
                , v_etapa
                , v_actividad
                , SYSDATE
                , 'PENDI'
                , v_concepto
                , v_cliente
                , p_fecha_ingreso
                , SYSDATE
                , v_agrupador_comercial
                , v_agrupador
                , NULL
                , v_fecha_cita
                , v_hora_cita
                , p_pedido
                , p_subpedido
                , p_solicitud
                , TO_CHAR (p_fecha_ingreso, 'HH24:MI')
                , NULL
                , TO_CHAR (SYSDATE, 'HH24:MI')
                , v_usuario
                , v_cita
                , v_ident_paquete
                , p_empresa
                , p_municipio
                 );
   /* EXCEPTION WHEN OTHERS THEN dbms_output.put_line(' error  pr_ins_trabajos_instalacion' ||  SUBSTR(SQLERRM, 1, 2000)           ); */
   END;

  dbms_output.put_line('v_ident_paquete 3: '||v_ident_paquete);
 -- Crear Ordenes de trabajo
   pr_ins_ordenes_instalacion (v_requerimiento
                      , p_pedido
                      , p_subpedido
                      , p_solicitud
                      , v_tipo_solicitud
                      , p_identificador_id
                      , p_identificador_nuevo
                      , v_tecnologia_reqt--p_tecnologia NOV2007
                      , p_servicio
                      , p_producto
                      , p_tipo_elemento
                      , p_tipo_elemento_id
                      , v_ident_paquete
                      , p_naturaleza
                      , v_usuario
                      , nvl(p_empresa,'UNE')
                      , p_municipio
                       );
            /*
            v_cant_receq := 0;
                       
            SELECT count(*)
              INTO v_cant_receq
              FROM fnx_ordenes_trabajos
             WHERE requerimiento_id = v_requerimiento
                AND pedido_id = p_pedido
                AND subpedido_id = p_subpedido
                AND solicitud_id = p_solicitud
                AND actividad_id = 'RECEQ';
                                
            DBMS_OUTPUT.put_line ('RECEQ generadas:  --> '||v_cant_receq); --*DMARTINEZ
            -- [Inicio REQ_SUSPECORELANTOLAN] - dmartinez* - 12/08/2014 - Escribe en la orden el motivo por el cual no se generó RECEQ
            IF v_cant_receq = 0 AND v_tipo_solicitud = 'RETIR' THEN
                                    
                -- Obtiene observacion del pedido
                SELECT SUBSTR(observacion, 1, INSTR(observacion, ';'))
                  INTO v_observacion_receq
                  FROM fnx_pedidos
                 WHERE pedido_id = p_pedido;
                                        
                /*DBMS_OUTPUT.put_line ('v_observacion_receq PEDIDO:  --> '||v_observacion_receq ||
                                        'requerimiento_id :  --> '||v_requerimiento ); --*DMARTINEZ*
                BEGIN 
                    UPDATE fnx_ordenes_trabajos
                      SET  observacion = SUBSTR(v_observacion_receq || observacion, 1, 700)
                     WHERE requerimiento_id = v_requerimiento
                       AND etapa_id = v_etapa
                       AND actividad_id = 'DESIN';
                EXCEPTION
                    WHEN OTHERS THEN                
                        DBMS_OUTPUT.put_line (SQLERRM || '<GEN-ORD> No se actualizó la orden'); --*DMARTINEZ
                END;
            END IF;
            -- [Fin REQ_SUSPECORELANTOLAN] - dmartinez*
            */
      
 END; 