import java.util.*;
import java.io.*;
public class Tokenizer{
	private String codigo;
	private char[]caracteres;
	private int puntero;
	private HashMap<Character,Integer>hash;
	private HashMap<Character,Integer>ign;
	public PrintStream ps;
	public Tokenizer(String codigo){
		this.codigo=codigo;
		this.caracteres=codigo.toCharArray();
		ign=new HashMap<Character,Integer>();
		ign.put('&',1);
		ign.put(' ',1);
		hash=new HashMap<Character,Integer>();
		hash.put('(',1);
		hash.put(')',1);
		hash.put(';',1);
		hash.put(',',1);
		hash.put('\'',1);
		hash.put('&',1);
		hash.put(' ',1);
		try{
			ps=new PrintStream(new FileOutputStream(new File("TokenLog.txt"),false));
		}catch(Exception e){
		}
	}
	public boolean hasNext(){
		//long ti=(new Date()).getTime();
		while(puntero<caracteres.length){
			/*if((new Date()).getTime()-ti>1000){
				System.out.println("Time 1");
				System.exit(0);
			}*/
			if(ign.get(caracteres[puntero])==null){
				break;
			}
			puntero++;
		}
		if(puntero>=caracteres.length){
			ps.println("Fin del archivo");
			return false;
		}
		return true;
	}
	public String ver(){
		int p=puntero;
		String resultado=xnext();
		puntero=p;
		return resultado;
	}
	private String xnext(){
		String resultado=this.pnext();
		return resultado;
	}
	public String next(){
		String resultado=this.pnext();
		if(resultado.toUpperCase().equals("ELSIF")){
			//resultado="ELSE IF";
		}
		ps.println(resultado);
		return resultado;
	}
	public boolean valido(){
		if(ign.get(caracteres[puntero])!=null){
			return false;
		}
		return true;
	}
	public String pnext(){
		//long ti=(new Date()).getTime();
		StringBuffer resultado=new StringBuffer("");
		while(puntero<caracteres.length){
			/*if((new Date()).getTime()-ti>1000){
				System.out.println("Time 2");
				System.exit(0);
			}*/
			if(ign.get(caracteres[puntero])==null){
				break;
			}
			puntero++;
		}
		while(caracteres[puntero]=='-'&&caracteres[puntero+1]=='-'){
			while(puntero<caracteres.length&&caracteres[puntero]!='&'){
				puntero++;
			}
			while(puntero<caracteres.length){
				if(ign.get(caracteres[puntero])==null){
					break;
				}
				puntero++;
			}
		}
		if(caracteres[puntero]=='\''){
			resultado.append(caracteres[puntero]);
			puntero++;
			while(caracteres[puntero]!='\''){
				/*if((new Date()).getTime()-ti>1000){
					System.out.println("Time 3");
					System.exit(0);
				}*/
				resultado.append(caracteres[puntero]);
				puntero++;
			}
			resultado.append(caracteres[puntero]);
			puntero++;
			//System.out.println(resultado.toString());
			return resultado.toString();
		}
		if(hash.get(caracteres[puntero])!=null){
			resultado.append(caracteres[puntero]);
			puntero++;
			//System.out.println(resultado.toString());
			return resultado.toString();
		}
		while(puntero<caracteres.length&&hash.get(caracteres[puntero])==null){
			/*if((new Date()).getTime()-ti>1000){
				System.out.println("Time 4");
				System.exit(0);
			}*/
			if(caracteres[puntero]=='-'&&puntero+1<caracteres.length&&caracteres[puntero+1]=='-'){
				break;
			}
			resultado.append(caracteres[puntero]);
			puntero++;
		}
		//System.out.println(resultado.toString());
		return resultado.toString();
	}
}