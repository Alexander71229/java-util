PACKAGE BODY "PKG_TAREAS_PLATAFORMAS"
IS
   -- Orden de Habilitar Pay Per View (NUEVO)
   -- Recargar Creditos (CAMBI)

   -- Organizacion     UNE EPM Telecomunicaciones S.A.
-- Descripcion:     Paquete que contiene las funciones de verificacion de generacion de ordenes
--                  automáticas

   -- HISTORIA DE MODIFICACIONES ---------------------------------------------------------------------
-- Fecha        Autor       Observaciones
-- -----------  ----------- -----------------------------------------------------------------------

-- 2009-08-12   WQUICENO    Se eliminan las variables globales tanto del spec como del body, con el
--                          fin de comprobar que el error ORA-04068 no se vuelva a presentar y las
--                          solicitudes no queden APROB por este error.
-- 2009-11-18   WQUICENO    Oferta POTS. Se adiciona llamado a la funcion de la plataforma ZTE.
-- 2010-03-09   WQUICENO    Oferta POTS. Se estaban generando intercambios cuya lógica de generación
--                          no pasaba por la funcion fn_plataforma_zte debido a la evaluacion de una
--                          lista de actividades especifica.
-- 2010-03-23   ysolarte    Traslado de paquetes Regionales. Se modifica el cursor de características solicitudes
--                          para excluir la característica 4371 que es exclisiva de traslado de paquetes.
-- 2010-06-28   etachej     Activacion Automatica - Se incluye llamado de la función de enrutamiento de la
--                          plataforma CABLEM (fn_plataforma_cablem).
-- 2010-07-14   etachej     TR069 - Se incluye llamado de la función de enrutamiento de la
--                          plataforma ACS (fn_plataforma_acs).
-- 2012-04-17   ETachej        Afinamiento PDN - Se hace la reestructuración del FENIX.PKG_TAREAS_PLATAFORMAS
-- 2011-10-27   GSolarte    Portabilidad MVNO. Se adiciona llamado a la funcion de la plataforma PORTAB.
-- 2012-05-29   Yhernana    Eliminación Tarea Plataforma cuando esta no ha sido tomada por la
--                          plataforma INTIGO en el momento de ingreso de una Anulación de un
--                          pedido de SXFP para producto INTMOV.
--2013-08-06    Jpulgarb    Implementacion para plataforma Servicios y Equipos SEREQU
-- 2013-09-09   Jtabarc     REQ21213_ServFijoLTECC - Se modifica procedimiento pr_anula_tarea Para generar correctamente intercambios inversos respectivos
--                          para la anulacion de la peticion Cambio de SIMCARD y Cambio de Router del producto FIJLTE.
-- GSANTOS     2014-01-21   Se modifica para que las tareas a plataforma se generen en concepto_orden PUAT
--                          para que no se las lleven a las plataformas. ESTE CAMBIO NO PUEDE PASAR A PRODUCCION,
--                          DEBE PERMANECER EN UAT.
-- JGLENA      2014-06-10   REQ36900_GPONHoga Modificaciones para cambio de equipo por mantenimiento
-- GSANTOS     2014-08-14   INC2413275_InciPOTS. Se modifica la ejecucion de la plataforma ZTE. Se cre nueva funcion de plataforma ZTE para los
--                          cambios de producto. Se hace llamado a la funcion fn_verificar_cambio_prd para detectar si es cambio
--                          de productro y llamar la nueva funcion de plataforma ZTE.

   v_identificador   fnx_identificadores.identificador_id%TYPE;

   /*Procedimiento para JOB - Tarea Programada PPV
   Busca las solicitudes de PPV que se encuentran en estado - concepto ORDEN - EVPRO

   y de acuerdo al Evento y la fecha de emision determina si la solicitud debe pasar
   a FACTU - PFACT */
   PROCEDURE pr_cumplir_job (
      p_tipo_elemento_id   IN   fnx_solicitudes.tipo_elemento_id%TYPE,
      p_estado_id          IN   fnx_estados_conceptos.estado_id%TYPE,
      p_concepto_id        IN   fnx_estados_conceptos.concepto_id%TYPE
   )
   IS
      -- Cursor con las solicitudes de tipo PPV en estado-concepto ORDEN-EVPRO
      CURSOR c_sol_evpro (
         v_tipo_elem_id   fnx_solicitudes.tipo_elemento_id%TYPE,
         v_estado         fnx_estados_conceptos.estado_id%TYPE,
         v_concepto       fnx_estados_conceptos.concepto_id%TYPE
      )
      IS
         SELECT pedido_id, subpedido_id, solicitud_id
           FROM fnx_solicitudes
          WHERE estado_id = v_estado
            AND concepto_id = v_concepto
            AND tipo_elemento_id = v_tipo_elem_id;

      v_evento      fnx_caracteristica_solicitudes.valor%TYPE;
      v_paso_fact   BOOLEAN;
      v_cont        NUMBER                                      := 0;
   BEGIN
      FOR reg IN c_sol_evpro (p_tipo_elemento_id, p_estado_id, p_concepto_id)
      LOOP
         v_evento :=
            pkg_solicitudes.fn_valor_caracteristica (reg.pedido_id,
                                                     reg.subpedido_id,
                                                     reg.solicitud_id,
                                                     2973
                                                    );

         -- Se verifica que el evento exista
         IF v_evento IS NOT NULL
         THEN
            -- se invoca funcion que determina si se debe pasar la solicitud a FACTU-PFACT
            v_paso_fact := pkg_zeus.fn_cartelera_eventos (v_evento, SYSDATE);

            IF v_paso_fact
            THEN
               BEGIN
                  UPDATE fnx_solicitudes
                     SET estado_id = 'CUMPL',
                         concepto_id = 'CUMPL'
                   WHERE pedido_id = reg.pedido_id
                     AND subpedido_id = reg.subpedido_id
                     AND solicitud_id = reg.solicitud_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.put_line
                        ('Problemas al Actualizar SOLICITUDES desde el PKG_PAYPERVIEW.PR_CUMPLIR_PPV_JOB. Consulte el Administrador'
                        );
               END;

               v_cont := v_cont + 1;
            END IF;
         END IF;

         IF v_cont >= 100
         THEN
            COMMIT;
            v_cont := 0;
         END IF;
      END LOOP;

      COMMIT;
   END;

   -- Para actualizar intercambios que esten pendientes una vez sea cerrada la orden
   -- manual
   PROCEDURE pr_actualizar_tarea (
      p_requerimiento   IN   fnx_ordenes_trabajos.requerimiento_id%TYPE,
      p_concepto        IN   fnx_ordenes_trabajos.concepto_id%TYPE
   )
   IS
      v_concepto   fnx_tareas_plataformas.concepto_orden%TYPE;
   BEGIN
      UPDATE fnx_tareas_plataformas
         SET concepto_orden = p_concepto
       WHERE requerimiento_id = p_requerimiento;
   END;

--Crea la tarea
   PROCEDURE pr_crear_tarea (
      p_pedido          IN   fnx_solicitudes.pedido_id%TYPE,
      p_subpedido       IN   fnx_solicitudes.subpedido_id%TYPE,
      p_solicitud       IN   fnx_solicitudes.solicitud_id%TYPE,
      p_requerimiento   IN   fnx_tareas_plataformas.requerimiento_id%TYPE,
      p_identificador   IN   fnx_tareas_plataformas.identificador_id%TYPE,
      p_plataforma      IN   fnx_tareas_plataformas.plataforma%TYPE,
      p_actividad       IN   fnx_tareas_plataformas.actividad_id%TYPE,
      p_tipo_trabajo    IN   fnx_tareas_plataformas.tipo_trabajo%TYPE
   )
   IS
      CURSOR c_inter
      IS
         SELECT   tipo_intercambio
             FROM fnx_tipos_intercambios
            WHERE plataforma = p_plataforma
              AND actividad_id = p_actividad
              AND tipo_trabajo = p_tipo_trabajo
         ORDER BY secuencia;

-- 2010-03-23   ysolarte    Traslado de paquetes Regionales. Se modifica el cursor de características solicitudes
--                          para excluir la característica 4371 que es exclisiva de traslado de paquetes.
      CURSOR c_eleme
      IS
         SELECT tipo_elemento_id, producto_id
           FROM fnx_caracteristica_solicitudes
          WHERE pedido_id = p_pedido
            AND subpedido_id = p_subpedido
            AND solicitud_id = p_solicitud
            AND caracteristica_id <> 4371
            AND ROWNUM <= 1;

      CURSOR c_ident (p_elem IN fnx_identificadores.tipo_elemento_id%TYPE)
      IS
         SELECT identificador_id, tipo_elemento_id, producto_id
           FROM fnx_identificadores
          WHERE agrupador_id = p_identificador
            AND tipo_elemento = 'CMP'
            AND tipo_elemento_id = p_elem;

      CURSOR c_ident1
      IS
         SELECT tipo_elemento_id, producto_id
           FROM fnx_identificadores
          WHERE identificador_id = p_identificador;

      v_concepto               fnx_ordenes_trabajos.concepto_id%TYPE;
      v_elemento               fnx_caracteristica_solicitudes.tipo_elemento_id%TYPE;
      v_producto               fnx_caracteristica_solicitudes.tipo_elemento_id%TYPE;
      v_tipo_intercambio_inv   fnx_tareas_plataformas.tipo_intercambio%TYPE;
      v_parametro              fnx_tareas_plataformas.parametro%TYPE;
      v_parametro_xml          VARCHAR2 (8000);
      err_resp                 VARCHAR2 (1000);
      v_necesita               VARCHAR2 (2)                            := 'NO';
/* 2014-06-10 JGLENA REQ36900_GPONHoga variables para generar intercambios de GPON */
      v_producto_iden          fnx_identificadores.producto_id%TYPE;
      v_prod_iden              pkg_multi_servicio.tab_rec_prod_iden;
      v_tecno_iden             fnx_identificadores.tecnologia_id%TYPE;
      v_prod_prio              fnx_identificadores.producto_id%TYPE;
      v_iden_prio              fnx_identificadores.identificador_id%TYPE;
      ct_tipo_elemento_cpe     fnx_tecnologias_elementos.tipo_elemento_id%TYPE := 'CPE';

      CURSOR c_rqtr01 IS
      SELECT producto_id, servicio_id, identificador_id
      FROM fnx_requerimientos_trabajos
      WHERE requerimiento_id = p_requerimiento;

      r_rqtr c_rqtr01%rowtype;
   BEGIN
     dbms_output.put_line('  p_plataforma - p_actividad - p_tipo_trabajo' ||   p_plataforma || '-' || p_actividad || '-' || p_tipo_trabajo);
      v_concepto := 'PPRG';

      OPEN c_eleme;

      FETCH c_eleme
       INTO v_elemento, v_producto;

      CLOSE c_eleme;

      IF v_elemento IS NULL
      THEN
         OPEN c_ident1;

         FETCH c_ident1
          INTO v_elemento, v_producto;

         CLOSE c_ident1;
      END IF;

      DBMS_OUTPUT.put_line (   '<<PR_CREAR_TAREA p_plataforma>>'
                            || p_plataforma
                            || '- p_actividad '
                            || p_actividad
                            || ' p_tipo_trabajo '
                            || p_tipo_trabajo
                           );

      IF     p_plataforma = 'OLA'
         AND p_tipo_trabajo IN ('RETIR', 'SUSPE', 'RECON')
         AND v_elemento = 'SRV'
      THEN
         DBMS_OUTPUT.put_line ('Ingresa al Loop de Identificadores de OLA ');


         FOR v_ident IN c_ident ('LMOVIL')
         LOOP
            FOR v_inter IN c_inter
            LOOP
               pkg_movil.pr_consultar_hamov (p_pedido,
                                             p_subpedido,
                                             p_solicitud,
                                             p_requerimiento,
                                             v_ident.identificador_id,
                                             p_tipo_trabajo,
                                             v_parametro
                                            );

--                  v_concepto := 'PEND';
               IF v_parametro <> 'N'
               THEN
                  pr_insertar_tarea (p_requerimiento,
                                     p_identificador,
                                     p_plataforma,
                                     p_actividad,
                                     p_tipo_trabajo,
                                     v_inter.tipo_intercambio,
                                     v_parametro,
                                     v_concepto,
                                     NULL
                                    );
               END IF;
            END LOOP;
         END LOOP;
      ELSE                                        --IF    p_plataforma = 'OLA'
         FOR v_inter IN c_inter
         LOOP
            DBMS_OUTPUT.put_line ('p_plataforma ' || p_plataforma);

            --Para
            IF p_plataforma IN ('DECO', 'CONPPV', /*'OLA', */ 'ZEUS', 'TOIP')
            THEN
--             DBMS_OUTPUT.put_line ('ENTRE POR EL INTERCAMBIO FN_PARAMETRO');
               v_parametro :=
                  fn_parametro (p_pedido,
                                p_subpedido,
                                p_solicitud,
                                p_requerimiento,
                                p_identificador,
                                p_plataforma,
                                p_actividad,
                                p_tipo_trabajo,
                                v_inter.tipo_intercambio
                               );

               IF v_parametro <> 'N'
               THEN
                  pr_insertar_tarea (p_requerimiento,
                                     p_identificador,
                                     p_plataforma,
                                     p_actividad,
                                     p_tipo_trabajo,
                                     v_inter.tipo_intercambio,
                                     v_parametro,
                                     v_concepto,
                                     NULL
                                    );
               END IF;
             ELSIF p_tipo_trabajo = 'REFAL' AND fn_tecnologia_identif(p_identificador) = 'GPON' THEN
               OPEN c_rqtr01;
               FETCH c_rqtr01 into r_rqtr;
               CLOSE c_rqtr01;

               DBMS_OUTPUT.put_line (   ' ANTES FN_PARAMETRO_XML'
                                       || ' - '
                                       || p_pedido
                                       || ' - '
                                       || p_subpedido
                                       || ' - '
                                       || p_solicitud
                                       || ' - '
                                       || p_requerimiento
                                       || ' - '
                                       || p_identificador
                                       || ' - '
                                       || v_producto
                                       || ' - '
                                       || v_elemento
                                       || ' - '
                                       || p_plataforma
                                       || ' - '
                                       || p_actividad
                                       || ' - '
                                       || p_tipo_trabajo
                                       || ' - '
                                       || v_inter.tipo_intercambio
                                      );
               v_necesita :=
                    fn_verificar_tareas (p_pedido,
                                         p_subpedido,
                                         p_solicitud,
                                         p_requerimiento,
                                         p_identificador,
                                         v_producto,
                                         v_elemento,
                                         p_plataforma,
                                         p_actividad,
                                         p_tipo_trabajo,
                                         v_inter.tipo_intercambio
                                        );
               DBMS_OUTPUT.put_line
                      (   ' FN_VERIFICAR_TAREAS v_necesita FN_PARAMETRO_XML XY '
                       || v_necesita
                      );

               IF v_necesita = 'SI'
               THEN
                  IF (r_rqtr.producto_id <> v_orden_trabajo_rec.producto_id
                  OR r_rqtr.servicio_id <> v_orden_trabajo_rec.servicio_id
                  OR r_rqtr.identificador_id <> v_orden_trabajo_rec.identificador_id) THEN
                     UPDATE fnx_requerimientos_trabajos
                        SET producto_id      = v_orden_trabajo_rec.producto_id,
                            servicio_id     =v_orden_trabajo_rec.servicio_id,
                            identificador_id = v_orden_trabajo_rec.identificador_id
                      WHERE requerimiento_id = p_requerimiento;
                  END IF;
                  v_parametro :=
                       fn_parametro_xml (p_pedido,
                                         p_subpedido,
                                         p_solicitud,
                                         p_requerimiento,
                                         p_identificador,
                                         'LOGIC',
                                         v_producto,
                                         p_plataforma,
                                         p_actividad,
                                         p_tipo_trabajo,
                                         v_inter.tipo_intercambio,
                                         v_parametro_xml
                                        );

                    IF p_plataforma = 'SER'
                    THEN
                       v_parametro :=
                          pkg_voip.fn_crear_voip (p_pedido,
                                                  p_subpedido,
                                                  p_solicitud,
                                                  v_inter.tipo_intercambio,
                                                  p_identificador
                                                 );
                    END IF;

                    pr_insertar_tarea (p_requerimiento,
                                       p_identificador,
                                       p_plataforma,
                                       p_actividad,
                                       p_tipo_trabajo,
                                       v_inter.tipo_intercambio,
                                       v_parametro,
                                       v_concepto,
                                       v_parametro_xml
                                      );


                    UPDATE fnx_requerimientos_trabajos
                          SET producto_id      = r_rqtr.producto_id,
                              servicio_id     = r_rqtr.servicio_id,
                              identificador_id = r_rqtr.identificador_id
                        WHERE requerimiento_id = p_requerimiento;
               END IF;                                   ---IF v_necesita ='SI' THEN

            ELSE
               DBMS_OUTPUT.put_line (   ' ANTES FN_PARAMETRO_XML'
                                     || ' - '
                                     || p_pedido
                                     || ' - '
                                     || p_subpedido
                                     || ' - '
                                     || p_solicitud
                                     || ' - '
                                     || p_requerimiento
                                     || ' - '
                                     || p_identificador
                                     || ' - '
                                     || v_producto
                                     || ' - '
                                     || v_elemento
                                     || ' - '
                                     || p_plataforma
                                     || ' - '
                                     || p_actividad
                                     || ' - '
                                     || p_tipo_trabajo
                                     || ' - '
                                     || v_inter.tipo_intercambio
                                    );
               v_necesita :=
                  fn_verificar_tareas (p_pedido,
                                       p_subpedido,
                                       p_solicitud,
                                       p_requerimiento,
                                       p_identificador,
                                       v_producto,
                                       v_elemento,
                                       p_plataforma,
                                       p_actividad,
                                       p_tipo_trabajo,
                                       v_inter.tipo_intercambio
                                      );
               DBMS_OUTPUT.put_line
                    (   ' FN_VERIFICAR_TAREAS v_necesita FN_PARAMETRO_XML XY '
                     || v_necesita
                    );

               IF v_necesita = 'SI'
               THEN
                  v_parametro :=
                     fn_parametro_xml (p_pedido,
                                       p_subpedido,
                                       p_solicitud,
                                       p_requerimiento,
                                       p_identificador,
                                       'LOGIC',
                                       v_producto,
                                       p_plataforma,
                                       p_actividad,
                                       p_tipo_trabajo,
                                       v_inter.tipo_intercambio,
                                       v_parametro_xml
                                      );

--OJARAMIP 20/03/2007 SE MANTIENE LAS 2 FORMAS DEL INTERCAMBIO PARA LA PLATAFORMA
-- SER MIENTRAS SE TERMINA EL DESARROLLO POR EL LADO DEL XML.
                  IF p_plataforma = 'SER'
                  THEN
                     v_parametro :=
                        pkg_voip.fn_crear_voip (p_pedido,
                                                p_subpedido,
                                                p_solicitud,
                                                v_inter.tipo_intercambio,
                                                p_identificador
                                               );
                  END IF;

                  pr_insertar_tarea (p_requerimiento,
                                     p_identificador,
                                     p_plataforma,
                                     p_actividad,
                                     p_tipo_trabajo,
                                     v_inter.tipo_intercambio,
                                     v_parametro,
                                     v_concepto,
                                     v_parametro_xml
                                    );
               END IF;                             ---IF v_necesita ='SI' THEN
            END IF;       --IF p_plataforma NOT IN('IDC', 'IAF','CNTXIP') THEN
         END LOOP;
      END IF;
/*   EXCEPTION
     WHEN OTHERS THEN
       dbms_output.put_line('ERRWO: '|| SUBSTR(SQLERRM,1,500));*/
   END;                                                           -- Procedure

   PROCEDURE pr_crear_tarea_inversa (
      p_requerimiento      IN       fnx_ordenes_trabajos.requerimiento_id%TYPE,
      p_identificador      IN       fnx_tareas_plataformas.identificador_id%TYPE,
      p_plataforma         IN       fnx_tareas_plataformas.plataforma%TYPE,
      p_actividad          IN       fnx_tareas_plataformas.actividad_id%TYPE,
      p_tipo_intercambio   IN       fnx_tareas_plataformas.tipo_intercambio%TYPE,
      p_concepto_orden     IN       fnx_tareas_plataformas.concepto_orden%TYPE,
      p_tipo_trabajo       IN       fnx_tareas_plataformas.tipo_trabajo%TYPE,
      p_xml_fenix          IN       VARCHAR2,
      p_xml_rta_fenix      IN       VARCHAR2,
      p_usuario            IN       fnx_tareas_plataformas.usuario%TYPE,
      p_mensaje            OUT      VARCHAR2
   )
   IS
-- ---------   ------  -------------------------------------------
--   maristim  05/05/2008 Creación de Pl, para anulación de tarea con XML
      v_intercambio_inv   fnx_tareas_plataformas.tipo_intercambio%TYPE;
      v_concepto          fnx_tareas_plataformas.concepto_orden%TYPE;
      v_act_inv           fnx_tareas_plataformas.actividad_id%TYPE;
      v_xml_fenix         VARCHAR2 (9000);
   BEGIN
      --message('1' ||  p_requerimiento || '*'||  p_plataforma || '*'|| p_actividad || '*'||p_tipo_intercambio );pause;
      IF NVL (p_concepto_orden, 'X') NOT IN ('CUMPL', 'PPRG')
      THEN
         DELETE      fnx_tareas_plataformas
               WHERE requerimiento_id = p_requerimiento
                 AND concepto_orden NOT IN ('CUMPL', 'PPRG')
                 AND actividad_id = p_actividad
                 AND tipo_intercambio = p_tipo_intercambio
                 AND tipo_trabajo = p_tipo_trabajo
                 AND plataforma = p_plataforma;
      ELSE
         SELECT tipo_intercambio_inv
           INTO v_intercambio_inv
           FROM fnx_tipos_intercambios
          WHERE plataforma = p_plataforma
            AND actividad_id = p_actividad
            AND tipo_intercambio = p_tipo_intercambio
            AND tipo_trabajo = p_tipo_trabajo;

         v_act_inv := fn_actividad_inv ('CONFI', p_actividad);
         v_xml_fenix := REPLACE (p_xml_fenix, p_actividad, v_act_inv);
         v_xml_fenix :=
                  REPLACE (v_xml_fenix, p_tipo_intercambio, v_intercambio_inv);

         INSERT INTO fnx_tareas_plataformas
                     (requerimiento_id, identificador_id, plataforma,
                      actividad_id, tipo_trabajo, tipo_intercambio,
                      parametro, concepto_orden, fecha, fecha_ejecucion,
                      codigo_error, mensaje_error, valor_retorno, usuario,
                      reintentos, transaccion_id, xml_fenix, xml_rta_fenix,
                      xml_transf_fenix, mensaje_id
                     )
              VALUES (p_requerimiento, p_identificador, p_plataforma,
                      v_act_inv, p_tipo_trabajo, v_intercambio_inv,
                      NULL, 'PPRG', SYSDATE, NULL,
                      NULL, NULL, NULL, p_usuario,
                      1, NULL, v_xml_fenix, NULL,
                      NULL, NULL
                     );

         UPDATE fnx_tareas_plataformas
            SET concepto_orden = 'ANULA'
          WHERE requerimiento_id = p_requerimiento
            AND concepto_orden = 'CUMPL'
            AND actividad_id = p_actividad
            AND tipo_intercambio = p_tipo_intercambio
            AND tipo_trabajo = p_tipo_trabajo
            AND plataforma = p_plataforma;
      --      message('inserto');pause;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         IF SQLERRM IS NOT NULL
         THEN
            p_mensaje := SQLERRM;
         END IF;
   END;

   PROCEDURE pr_insertar_tarea (
      p_requerimiento      IN   fnx_tareas_plataformas.requerimiento_id%TYPE,
      p_identificador      IN   fnx_tareas_plataformas.identificador_id%TYPE,
      p_plataforma         IN   fnx_tareas_plataformas.plataforma%TYPE,
      p_actividad          IN   fnx_tareas_plataformas.actividad_id%TYPE,
      p_tipo_trabajo       IN   fnx_tareas_plataformas.tipo_trabajo%TYPE,
      p_tipo_intercambio   IN   fnx_tareas_plataformas.tipo_intercambio%TYPE,
      p_parametro          IN   fnx_tareas_plataformas.parametro%TYPE,
      p_concepto           IN   fnx_tareas_plataformas.concepto_orden%TYPE,
      p_xml_fenix          IN   VARCHAR2
   )
   IS
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
--  ojaramip  05/05/2006 Creacion de la funcion para insertar el registros
--                       en la tabla fnx_tareas_plataformas.
--  Etachej   05/10/2010 TR069 - Cambio Equipo Daños - Se incluye en el where del DELETE
--                       el identificador_id
--  GSANTOS   2014-01-21 Se modifica para que las tareas a plataforma se generen en concepto_orden PUAT
--                       para que no se las lleven a las plataformas. ESTE CAMBIO NO PUEDE PASAR A PRODUCCION,
--                       DEBE PERMANECER EN UAT.

      -- Declare program variables as shown above
      v_usuario     fnx_usuarios.usuario_id%TYPE;
      v_secuencia   fnx_tareas_plataformas.transaccion_id%TYPE;

      CURSOR c_sec
      IS
         SELECT LPAD (TO_CHAR (ppv_sec.NEXTVAL), 4, '0')
           FROM DUAL;
   BEGIN
      BEGIN
--      DBMS_OUTPUT.put_line ('PR_INSERTAR_TAREA ');
         v_secuencia := NULL;
         --Usuario
         v_usuario := fn_usuario;

         IF p_plataforma = 'ZEUS'
         THEN
            OPEN c_sec;

            FETCH c_sec
             INTO v_secuencia;

            CLOSE c_sec;
         END IF;

         IF p_actividad = 'HAMOV'
         THEN
            DELETE      fnx_tareas_plataformas
                  WHERE requerimiento_id = p_requerimiento
                    AND plataforma = p_plataforma
                    AND actividad_id = p_actividad
                    AND tipo_intercambio = p_tipo_intercambio
                    AND concepto_orden <> 'CUMPL';
         ELSE
            DELETE      fnx_tareas_plataformas
                  WHERE requerimiento_id = p_requerimiento
                    AND plataforma = p_plataforma
                    AND actividad_id = p_actividad
                    AND tipo_intercambio = p_tipo_intercambio
                    AND identificador_id = p_identificador;
         END IF;
      END;

      BEGIN
         --DBMS_OUTPUT.put_line ('Antes de Insetar fnx_tareas_plataformas');
         INSERT INTO fnx_tareas_plataformas
                     (requerimiento_id, identificador_id, plataforma,
                      actividad_id, tipo_trabajo, tipo_intercambio,
                      parametro, concepto_orden, fecha, fecha_ejecucion,
                      codigo_error, mensaje_error, valor_retorno, usuario,
                      reintentos, transaccion_id, xml_fenix
                     )
              VALUES (p_requerimiento, p_identificador, p_plataforma,
                      p_actividad, p_tipo_trabajo, p_tipo_intercambio,
                      p_parametro, 'PUAT' --p_concepto GSANTOS 21-01-2014 TODA TAREA A PLATAFORMA SE INSERTA EN CONCEPTO_ORDEN PUAT, ESTO NO DEBE PASAR A PDN
                      , SYSDATE, NULL,
                      NULL, NULL, NULL, v_usuario,
                      0, v_secuencia, p_xml_fenix
                     );

         DBMS_OUTPUT.put_line ('Despues de Insetar .....');
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            DBMS_OUTPUT.put_line (   'ERROR PR_INSERTAR_TAREA'
                                  || SUBSTR (SQLERRM, 1, 800)
                                 );
      END;                                             --PR_INSERTAR_INTER_PPV
   END;

      PROCEDURE pr_anula_tarea (
      p_pedido       IN       fnx_solicitudes.pedido_id%TYPE,
      p_subpedido    IN       fnx_solicitudes.subpedido_id%TYPE,
      p_solicitud    IN       fnx_solicitudes.solicitud_id%TYPE,
      p_plataforma   IN       fnx_tareas_plataformas.plataforma%TYPE,
      p_etapa        IN       fnx_ordenes_trabajos.etapa_id%TYPE,
      p_actividad    IN       fnx_tareas_plataformas.actividad_id%TYPE,
      p_indicador    IN       NUMBER,
      p_mensaje      OUT      VARCHAR2
   )
   IS
-- ---------   ------  -------------------------------------------
--  ojaramip  11/11/2005 Creacion del procedimiento para cancelar los registros en la tabla de intercambios.
--  maristim  28/03/2007 Modificación Pl, para anulación de tarea con XML

      --Cursor tareas Plataformas con el requerimiento
      CURSOR c_tarea_pla (
         p_req   IN   fnx_ordenes_trabajos.requerimiento_id%TYPE,
         p_act   IN   fnx_ordenes_trabajos.actividad_id%TYPE
      )
      IS
         SELECT *
           FROM fnx_tareas_plataformas
          WHERE requerimiento_id = p_req
            AND plataforma = plataforma
            AND actividad_id = p_act   --FN_ACTIVIDAD_INV(p_etapa,p_actividad)
                                    ;

--Cursor de fnx_tipos_intercambios
      CURSOR c_tipo_inter (
         v_plataforma         fnx_tipos_intercambios.plataforma%TYPE,
         v_actividad          fnx_tipos_intercambios.actividad_id%TYPE,
         v_tipo_intercambio   fnx_tipos_intercambios.tipo_intercambio%TYPE,
         v_tipo_trabajo       fnx_tipos_intercambios.tipo_trabajo%TYPE
      )
      IS
         SELECT tipo_intercambio_inv, soporta_anulacion --Ldonadop 13/12/2010
           FROM fnx_tipos_intercambios
          WHERE plataforma = v_plataforma
            AND actividad_id = v_actividad
            AND tipo_intercambio = v_tipo_intercambio
            AND tipo_trabajo = v_tipo_trabajo;

--Cursor de requerimiento
      CURSOR c_rqtr
      IS
         SELECT requerimiento_id, identificador_id, producto_id, tecnologia_id --Ldonadop 13/12/2010
           FROM fnx_requerimientos_trabajos
          WHERE pedido_id = p_pedido
            AND subpedido_id = p_subpedido
            AND solicitud_id = p_solicitud
            AND ROWNUM = 1;

      err_resp            VARCHAR2 (100);
      v_intercambio       fnx_tareas_plataformas.tipo_intercambio%TYPE;
      v_intercambio_inv   fnx_tareas_plataformas.tipo_intercambio%TYPE;
      v_concepto          fnx_tareas_plataformas.concepto_orden%TYPE;
      v_parametro         fnx_tareas_plataformas.parametro%TYPE;
      v_id                fnx_ordenes_trabajos.identificador_id%TYPE;
      v_req               fnx_ordenes_trabajos.requerimiento_id%TYPE;
      v_producto          fnx_ordenes_trabajos.producto_id%TYPE;
      v_parametro_xml     VARCHAR2 (8000);
      v_cantidad          NUMBER (2);
      v_act               fnx_ordenes_trabajos.actividad_id%TYPE;
      v_tag_xml           VARCHAR2 (50);
      --Ldonadop 13/12/2010
      v_soporta_anulacion fnx_tipos_intercambios.soporta_anulacion%TYPE;
      v_tecnologia        fnx_ordenes_trabajos.tecnologia_id%TYPE;
      --Jtabarc  2013-09-09 REQ21213_ServFijoLTECC
      v_ModeloCPESale_New  fnx_ord_det_impresiones.FVALOR_ANTERIOR%TYPE;
      v_serialCPESale_New  fnx_ord_det_impresiones.FVALOR_ANTERIOR%TYPE;
      v_modeloCPEEntra_New fnx_ord_det_impresiones.FVALOR_ANTERIOR%TYPE;
      v_serialCPEEntra_New fnx_ord_det_impresiones.FVALOR_ANTERIOR%TYPE;
      v_tipoTerminal_old   fnx_ord_det_impresiones.FVALOR_ANTERIOR%TYPE;
      v_direccionMac_old   fnx_ord_det_impresiones.FVALOR_ANTERIOR%TYPE;
      v_tipoTerminal_New   fnx_ord_det_impresiones.FVALOR_ANTERIOR%TYPE;
      v_direccionMac_New   fnx_ord_det_impresiones.FVALOR_ANTERIOR%TYPE;

   BEGIN
      v_cantidad := 0;

--Identificar requerimiento y producto
      OPEN  c_rqtr;
      FETCH c_rqtr INTO v_req, v_id, v_producto, v_tecnologia;
      CLOSE c_rqtr;

      DBMS_OUTPUT.put_line (   'pr_anula_tarea  v_req, v_id, v_producto, p_actividad '
                            || v_req
                            || ','
                            || v_id
                            || ','
                            || v_producto
                            || ','
                            || p_actividad
                           );

      IF v_req IS NOT NULL
      THEN
         IF p_indicador = 1
         THEN
            v_act := p_actividad;
         ELSE
            v_act := fn_actividad_inv (p_etapa, p_actividad);
         END IF;

         DBMS_OUTPUT.put_line ('v_req, v_act ' || v_req || ' - ' || v_act);

         FOR v_tarea_pla IN c_tarea_pla (v_req, v_act)
         LOOP
            OPEN c_tipo_inter (v_tarea_pla.plataforma,
                               v_tarea_pla.actividad_id,
                               v_tarea_pla.tipo_intercambio,
                               v_tarea_pla.tipo_trabajo
                              );

            FETCH c_tipo_inter
             INTO v_intercambio_inv, v_soporta_anulacion; --Ldonadop 13/12/2010

            CLOSE c_tipo_inter;

            DBMS_OUTPUT.put_line (   'v_intercambio_inv  '
                                  || v_intercambio_inv
                                  || ' v_tarea_pla.concepto_orden '
                                  || v_tarea_pla.concepto_orden
                                 );
            --2010/03/18 Adecuación de anulaciones de para ordenes no cumplidas en POTS/NGN
            IF NVL (v_tarea_pla.concepto_orden, 'X') NOT IN ('CUMPL', 'PPRG')
            AND v_tarea_pla.plataforma <> 'ZTE'
            THEN
               --Borrar las tareas que no se han ejecutado
               DELETE      fnx_tareas_plataformas
                     WHERE requerimiento_id = v_req
                       AND concepto_orden NOT IN ('CUMPL', 'PPRG')
                       AND plataforma = plataforma;

            --2010/03/18 Adecuación de anulaciones de para ordenes no cumplidas en POTS/NGN

            ELSIF NVL (v_tarea_pla.concepto_orden, 'X') NOT IN ('CUMPL')
            AND v_tarea_pla.plataforma = 'ZTE'
            THEN
               --Borrar las tareas que no se han ejecutado
               DELETE      fnx_tareas_plataformas
                     WHERE requerimiento_id = v_req
                       AND concepto_orden NOT IN ('CUMPL')
                       AND plataforma = plataforma;

            --2012-05-29  Yhernana
            --Eliminación Tarea Plataforma cuando esta no ha sido tomada por la
            --plataforma INTIGO en el momento de ingreso de una Anulación de un
            --pedido de SXFP para producto INTMOV
            ELSIF NVL (v_tarea_pla.concepto_orden, 'X') NOT IN ('CUMPL')
            AND v_tarea_pla.plataforma      = 'INTIGO'
            AND v_tarea_pla.codigo_error    IS NULL
            AND v_tarea_pla.mensaje_error   IS NULL
            AND p_actividad                 = 'SUSPE'
            THEN
               --Borrar las tareas que no se han ejecutado
               DELETE      fnx_tareas_plataformas
                     WHERE requerimiento_id = v_req
                       AND concepto_orden NOT IN ('CUMPL')
                       AND plataforma = plataforma;

            ELSE
               IF v_intercambio_inv IS NOT NULL
               THEN
                  IF v_tarea_pla.concepto_orden = 'PPRG'
                  THEN
                     v_concepto := 'PENDI';
                  ELSE
                     v_concepto := 'PPRG';
                  END IF;

                  DBMS_OUTPUT.put_line (   ' Anular v_intercambio_inv '
                                        || v_intercambio_inv
                                       );

                  IF v_tarea_pla.plataforma IN ('DECO', 'CONPPV', 'OLA', 'ZEUS', 'TOIP')
                  THEN
                     v_parametro :=
                        fn_parametro (p_pedido,
                                      p_subpedido,
                                      p_solicitud,
                                      v_req,
                                      v_id,
                                      v_tarea_pla.plataforma,
                                      v_tarea_pla.actividad_id,
                                      v_tarea_pla.tipo_trabajo,
                                      v_intercambio_inv
                                     );
                     DBMS_OUTPUT.put_line ('Parametro: ' || v_parametro);

                     IF v_parametro <> 'N'
                     THEN
                        v_cantidad := v_cantidad + 1;
                        pr_insertar_tarea (v_req,
                                           v_id,
                                           p_plataforma,
                                           p_actividad,
                                           v_tarea_pla.tipo_trabajo,
                                           v_intercambio_inv,
                                           v_parametro,
                                           v_concepto,
                                           NULL
                                          );
                        DBMS_OUTPUT.put_line
                           (   'Se insertó un registro en tareas Plataformas: '
                            || v_req
                            || ' - '
                            || p_plataforma
                            || ' - '
                            || v_intercambio_inv
                           );
                     END IF;

                  --Ldonadop 13-12-2010 Proceso de Anulación - Intercambio Inverso
                  ELSIF v_soporta_anulacion = 'S' THEN

                     v_parametro :=
                         fn_parametro_xml (p_pedido,
                                           p_subpedido,
                                           p_solicitud,
                                           v_req,
                                           v_id,
                                           v_tecnologia,
                                           v_producto,
                                           v_tarea_pla.plataforma,
                                           v_tarea_pla.actividad_id,
                                           v_tarea_pla.tipo_trabajo,
                                           v_intercambio_inv,
                                           v_parametro_xml
                                          );

                     v_cantidad := v_cantidad + 1;
                     pr_insertar_tarea (v_req,
                                        v_id,
                                        p_plataforma,
                                        p_actividad,
                                        v_tarea_pla.tipo_trabajo,
                                        v_intercambio_inv,
                                        v_parametro,
                                        v_concepto,
                                        v_parametro_xml
                                       );
                     DBMS_OUTPUT.put_line
                          (   'Se insertó un registro en tareas Plataformas: '
                           || v_req
                           || ' - '
                           || p_plataforma
                           || ' - '
                           || v_intercambio_inv
                          );

                  ELSE
                     DBMS_OUTPUT.put_line (   'ANULAR fn_parametro_xml '
                                           || v_tarea_pla.actividad_id
                                           || v_tarea_pla.tipo_trabajo
                                          );
                     v_parametro_xml :=
                        REPLACE (v_tarea_pla.xml_fenix,
                                    '<tipo_intercambio>'
                                 || v_tarea_pla.tipo_intercambio,
                                 '<tipo_intercambio>' || v_intercambio_inv
                                );
                     v_parametro_xml :=
                        REPLACE (v_parametro_xml,
                                 '<actividad>' || v_tarea_pla.actividad_id,
                                 '<actividad>' || p_actividad
                                );

                     IF v_intercambio_inv = 'CEQUI'
                     THEN
                        v_parametro_xml :=
                           REPLACE (v_parametro_xml, 'macAnterior>', 'otra>');
                        v_parametro_xml :=
                            REPLACE (v_parametro_xml, 'mac>', 'macAnterior>');
                        v_parametro_xml :=
                                   REPLACE (v_parametro_xml, 'otra>', 'mac>');
                     END IF;
                     -- Jtabarc REQ21213_ServFijoLTECC - Se agregan validaciones para que en el intercambio generado en la anulacion modifique algunos Tags.
                     IF v_intercambio_inv = 'CADSL' AND v_producto = 'FIJLTE'
                     THEN

                        v_ModeloCPESale_New := FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'modeloCPEEntra');
                        v_serialCPESale_New := FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'serialCPEEntra');

                        v_modeloCPEEntra_New := FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'modeloCPESale');
                        v_serialCPEEntra_New := FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'serialCPESale');

                        v_parametro_xml :=
                        REPLACE (v_tarea_pla.xml_fenix,
                                    '<modeloCPESale>'
                                 || FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'modeloCPESale'),
                                 '<modeloCPESale>' || v_ModeloCPESale_New
                                );

                        v_parametro_xml :=
                        REPLACE (v_parametro_xml,
                                    '<serialCPESale>'
                                 || FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'serialCPESale'),
                                 '<serialCPESale>' || v_serialCPESale_New
                                );

                       v_parametro_xml :=
                        REPLACE (v_parametro_xml,
                                    '<modeloCPEEntra>'
                                 || FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'modeloCPEEntra'),
                                 '<modeloCPEEntra>' || v_modeloCPEEntra_New
                                );


                        v_parametro_xml :=
                        REPLACE (v_parametro_xml,
                                    '<serialCPEEntra>'
                                 || FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'serialCPEEntra'),
                                 '<serialCPEEntra>' || v_serialCPEEntra_New
                                );
                     END IF;

                     IF v_intercambio_inv = 'CETOI' AND v_producto = 'FIJLTE'
                     THEN

                        v_tipoTerminal_old := FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'tipoTerminal');
                        v_direccionMac_old := FN_EXTRAE_VALOR_TAG (v_tarea_pla.xml_fenix,'direccionMac');

                        v_tipoTerminal_New := FN_VALOR_CONFIG_EQUIPOS (fn_valor_trabaj_subelem (p_pedido,p_subpedido,'ROUTER',200),982);
                        v_direccionMac_New := FN_VALOR_CONFIG_EQUIPOS (fn_valor_trabaj_subelem (p_pedido,p_subpedido,'ROUTER',200),3765);

                         v_parametro_xml :=
                        REPLACE (v_tarea_pla.xml_fenix,
                                    '<tipoTerminal>'
                                 || v_tipoTerminal_old,
                                 '<tipoTerminal>' || v_tipoTerminal_New
                                );

                        v_parametro_xml :=
                        REPLACE (v_parametro_xml,
                                    '<direccionMac>'
                                 || v_direccionMac_old,
                                 '<direccionMac>' || v_direccionMac_New
                                );
                     END IF;

                     -- WQUICENO - 2010/03/18 - Ajustes para anulaciones POTS NGN
                     -- CVILLARE - 2010/09/03 - Se agrega la plataforma MIUNE para la anulación
                     -- correcta
                     -- Ldonadop - 13/12/2010 - Estas colas quedaron parametrizables como soporta_anulacion SI
                     /*IF v_tarea_pla.plataforma IN ('HTV', 'ATS', 'INTMOV', 'INTIGO', 'ZTE', 'MIUNE')
                     THEN
                        v_parametro :=
                           fn_parametro_xml (p_pedido,
                                             p_subpedido,
                                             p_solicitud,
                                             v_req,
                                             v_id,
                                             'LOGIC',
                                             v_producto,
                                             v_tarea_pla.plataforma,
                                             v_tarea_pla.actividad_id,
                                             v_tarea_pla.tipo_trabajo,
                                             v_intercambio_inv,
                                             v_parametro_xml
                                            );
                     END IF;*/

                     IF v_tarea_pla.plataforma = 'SER'
                     THEN
                        v_parametro :=
                           pkg_voip.fn_crear_voip (p_pedido,
                                                   p_subpedido,
                                                   p_solicitud,
                                                   v_intercambio_inv,
                                                   v_id
                                                  );
                     END IF;

                     v_cantidad := v_cantidad + 1;
                     pr_insertar_tarea (v_req,
                                        v_id,
                                        p_plataforma,
                                        p_actividad,
                                        v_tarea_pla.tipo_trabajo,
                                        v_intercambio_inv,
                                        v_parametro,
                                        v_concepto,
                                        v_parametro_xml
                                       );
                     DBMS_OUTPUT.put_line
                          (   'Se insertó un registro en tareas Plataformas: '
                           || v_req
                           || ' - '
                           || p_plataforma
                           || ' - '
                           || v_intercambio_inv
                          );

                  END IF; -- IF v_tarea_pla.plataforma IN ('DECO', 'CONPPV', 'OLA', 'ZEUS', 'TOIP')

               END IF;                --IF v_tipo_intercambio IS NOT NULL THEN
            END IF;       --IF nvl(v_tipo_inter.concepto_id,'X') = 'PPRG' then
         END LOOP;
      END IF;                                      --IF v_req IS NOT NULL THEN

      IF v_cantidad = 0
      THEN
         p_mensaje := 'El pedido: ' || p_pedido || ' No se puede ANULAR';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_mensaje := 'WOERR';
   END;                                         --   pr_anula_inter_ppv

   FUNCTION fn_parametro (
      p_pedido             IN   fnx_solicitudes.pedido_id%TYPE,
      p_subpedido          IN   fnx_solicitudes.subpedido_id%TYPE,
      p_solicitud          IN   fnx_solicitudes.solicitud_id%TYPE,
      p_requerimiento      IN   fnx_tareas_plataformas.requerimiento_id%TYPE,
      p_identificador      IN   fnx_tareas_plataformas.identificador_id%TYPE,
      p_plataforma         IN   fnx_tareas_plataformas.plataforma%TYPE,
      p_actividad          IN   fnx_tareas_plataformas.actividad_id%TYPE,
      p_tipo_trabajo       IN   fnx_tareas_plataformas.tipo_trabajo%TYPE,
      p_tipo_intercambio   IN   fnx_tareas_plataformas.tipo_intercambio%TYPE
   )
      RETURN VARCHAR2
   IS
      v_parametro   fnx_tareas_plataformas.parametro%TYPE   := 'S';
      v_par         fnx_tareas_plataformas.parametro%TYPE;
      v_xml_fenix   fnx_tareas_plataformas.xml_fenix%TYPE;
   BEGIN
      IF p_plataforma = 'ZEUS'
      THEN
         v_parametro :=
            pkg_zeus.fn_valor_inter (p_tipo_intercambio,
                                     p_pedido,
                                     p_subpedido,
                                     p_solicitud,
                                     p_identificador,
                                     p_tipo_trabajo
                                    );
      ELSIF p_plataforma = 'SER'
      THEN
         v_parametro :=
            pkg_voip.fn_crear_voip (p_pedido,
                                    p_subpedido,
                                    p_solicitud,
                                    p_tipo_intercambio,
                                    p_identificador
                                   );
--         DBMS_OUTPUT.put_line (' ');
      ELSIF p_plataforma = 'ABNET'
      THEN
         v_parametro :=
            pkg_pho_line.fn_crear_telint (p_pedido,
                                          p_subpedido,
                                          p_solicitud,
                                          p_tipo_intercambio,
                                          p_identificador
                                         );
      ELSIF p_plataforma = 'OLA'
      THEN
         -- v_parametro :=  p_identificador || '|'|| p_actividad;
--         DBMS_OUTPUT.put_line ('el TIPO DE PLATAFORMA ES MOVIL');
         pkg_movil.pr_consultar_hamov (p_pedido,
                                       p_subpedido,
                                       p_solicitud,
                                       p_requerimiento,
                                       p_identificador,
                                       p_tipo_trabajo,
                                       v_parametro
                                      );
--              v_concepto := 'PEND';
      END IF;

/*      DBMS_OUTPUT.put_line (   'Retorna '
                            || v_parametro
                            || ' - '

                            || p_tipo_intercambio
                           );*/
      RETURN v_parametro;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_parametro := 'N';
         DBMS_OUTPUT.put_line ('ERRWO: ' || SUBSTR (SQLERRM, 1, 250));
         RETURN v_parametro;
   END;

   FUNCTION fn_parametro_xml (
      p_pedido             IN       fnx_solicitudes.pedido_id%TYPE,
      p_subpedido          IN       fnx_solicitudes.subpedido_id%TYPE,
      p_solicitud          IN       fnx_solicitudes.solicitud_id%TYPE,
      p_requerimiento      IN       fnx_tareas_plataformas.requerimiento_id%TYPE,
      p_identificador      IN       fnx_tareas_plataformas.identificador_id%TYPE,
      p_tecnologia         IN       fnx_solicitudes.tecnologia_id%TYPE,
      p_producto           IN       fnx_solicitudes.producto_id%TYPE,
      p_plataforma         IN       fnx_tareas_plataformas.plataforma%TYPE,
      p_actividad          IN       fnx_tareas_plataformas.actividad_id%TYPE,
      p_tipo_trabajo       IN       fnx_tareas_plataformas.tipo_trabajo%TYPE,
      p_tipo_intercambio   IN       fnx_tareas_plataformas.tipo_intercambio%TYPE,
      p_xml_fenix          OUT      VARCHAR2
   )
      RETURN VARCHAR2
   IS
      v_parametro   fnx_tareas_plataformas.parametro%TYPE;
      v_xml_fenix   VARCHAR2 (8000);
   BEGIN
      /*
      CONSULTA PARA DEFINIR SI SE REQUIERE TAREA PLATAFORMA
      PR_VERIFICAR_TAREAS_PLATAFORMA
      */--dbms_output.put_line('ENTRO A PR_PARAMETRO_TRPL' );
      pr_parametro_trpl (p_pedido,
                         p_subpedido,
                         p_solicitud,
                         p_requerimiento,
                         p_producto,
                         p_identificador,
                         p_tecnologia,
                         p_plataforma,
                         p_tipo_intercambio,
                         'ELEM',
                         p_actividad,
                         v_xml_fenix
                        );
      DBMS_OUTPUT.put_line
         ('****************-------------------*salio A PR_PARAMETRO_TRPL ESTA EN FN_PARA_XML'
         );
      DBMS_OUTPUT.put_line (SUBSTR (v_xml_fenix, 1, 200));
      p_xml_fenix := v_xml_fenix;
      -- dbms_output.put_line('paso de p_xml_fenix' );
      v_parametro := 'S';
      RETURN v_parametro;
/*   EXCEPTION
     WHEN OTHERS THEN

         v_parametro := 'N';
         dbms_output.put_line('ERRWO: '|| SUBSTR(SQLERRM,1,800));

         RETURN  v_parametro;    */
   END;

   FUNCTION fn_verificar_tareas (
      p_pedido             IN   fnx_solicitudes.pedido_id%TYPE,
      p_subpedido          IN   fnx_solicitudes.subpedido_id%TYPE,
      p_solicitud          IN   fnx_solicitudes.solicitud_id%TYPE,
      p_requerimiento      IN   fnx_requerimientos_trabajos.requerimiento_id%TYPE,
      p_identificador      IN   fnx_solicitudes.identificador_id%TYPE,
      p_producto           IN   fnx_solicitudes.producto_id%TYPE,
      p_elemento           IN   fnx_solicitudes.tipo_elemento_id%TYPE,
      p_plataforma         IN   fnx_tareas_plataformas.plataforma%TYPE,
      p_actividad          IN   fnx_tareas_plataformas.actividad_id%TYPE,
      p_tipo_trabajo       IN   fnx_tareas_plataformas.tipo_trabajo%TYPE,
      p_tipo_intercambio   IN   fnx_tareas_plataformas.tipo_intercambio%TYPE
   )
      RETURN VARCHAR2
   IS
      v_necesita   VARCHAR2 (2) := 'NO';
   BEGIN
      v_necesita := 'NO';

      IF p_plataforma = 'IAF'
      THEN
         DBMS_OUTPUT.put_line ('SI es IAF');
          v_necesita :=
            fn_plataforma_iaf (p_pedido,
                               p_subpedido,
                               p_solicitud,
                               p_producto,
                               p_elemento,
                               p_plataforma,
                               p_actividad,
                               p_tipo_trabajo,
                               p_tipo_intercambio,
                               p_identificador
                              );
      ELSIF p_plataforma = 'IDC'
      THEN
         --2012-04-17 ETachej - Afinamiento PDN - Se invoca fn_plataforma_idc
          v_necesita :=
            fn_plataforma_idc (p_pedido,
                               p_subpedido,
                               p_solicitud,
                               p_producto,
                               p_elemento,
                               p_plataforma,
                               p_actividad,
                               p_tipo_trabajo,
                               p_tipo_intercambio
                              );
      ELSIF p_plataforma = 'SER'
      THEN
         v_necesita :=
            fn_plataforma_ser (p_pedido,
                               p_subpedido,
                               p_solicitud,
                               p_producto,
                               p_elemento,
                               p_identificador,
                               p_plataforma,
                               p_actividad,
                               p_tipo_trabajo,
                               p_tipo_intercambio
                              );
         DBMS_OUTPUT.put_line (' MARY LUZ fn_plataforma_ser ' || v_necesita);
      ELSIF p_plataforma = 'RIGHTV'
      THEN
         v_necesita :=
            fn_plataforma_rightv (p_pedido,
                                  p_subpedido,
                                  p_solicitud,
                                  p_producto,
                                  p_elemento,
                                  p_plataforma,
                                  p_actividad,
                                  p_tipo_trabajo,
                                  p_tipo_intercambio
                                 );
      ELSIF p_plataforma IN ('LDAP', 'LDAP3')
      THEN
         v_necesita :=
            fn_plataforma_ldap (p_pedido,
                                p_subpedido,
                                p_solicitud,
                                p_requerimiento,
                                p_identificador,
                                p_producto,
                                p_elemento,
                                p_plataforma,
                                p_actividad,
                                p_tipo_trabajo,
                                p_tipo_intercambio
                               );
     --2010-06-28 Etachej - Activacion Automatica
      ELSIF p_plataforma = 'CABLEM'
      THEN
         v_necesita :=
            fn_plataforma_cablem (p_pedido,
                                  p_subpedido,
                                  p_solicitud,
                                  p_requerimiento,
                                  p_identificador,
                                  p_producto,
                                  p_elemento,
                                  p_plataforma,
                                  p_actividad,
                                  p_tipo_trabajo,
                                  p_tipo_intercambio
                                 );
      --2010-07-14 Etachej - TR069
      ELSIF p_plataforma = 'ACS'
      THEN
         v_necesita :=
            fn_plataforma_acs (p_pedido,
                               p_subpedido,
                               p_solicitud,
                               p_producto,
                               p_elemento,
                               p_identificador,
                               p_plataforma,
                               p_actividad,
                               p_tipo_trabajo,
                               p_tipo_intercambio
                              );
      ELSIF p_plataforma IN ('ACC400')
      THEN
        --2012-04-17 ETachej - Afinamiento PDN - Se invoca fn_plataforma_acc4000
         v_necesita :=
            fn_plataforma_acc4000 (p_pedido,
                                   p_subpedido,
                                   p_solicitud,
                                   p_requerimiento,
                                   p_identificador,
                                   p_producto,
                                   p_elemento,
                                   p_plataforma,
                                   p_actividad,
                                   p_tipo_trabajo,
                                   p_tipo_intercambio
                                  );
      ELSIF p_plataforma = 'OLA'
      THEN
         --2012-04-17 ETachej - Afinamiento PDN - Se invoca fn_plataforma_ola
         v_necesita :=
            fn_plataforma_ola (p_pedido,
                               p_subpedido,
                               p_solicitud,
                               p_requerimiento,
                               p_identificador,
                               p_producto,
                               p_elemento,
                               p_plataforma,
                               p_actividad,
                               p_tipo_trabajo,
                               p_tipo_intercambio
                              );
      ELSIF p_plataforma = 'COMAN'
      THEN
        --2012-04-17 ETachej - Afinamiento PDN - Se invoca fn_plataforma_coman
         DBMS_OUTPUT.put_line ('SI es COMAN');
          v_necesita :=
            fn_plataforma_coman (p_pedido,
                                 p_subpedido,
                                 p_solicitud,
                                 p_producto,
                                 p_elemento,
                                 p_plataforma,
                                 p_actividad,
                                 p_tipo_trabajo,
                                 p_tipo_intercambio
                                );
      ELSIF p_plataforma = 'REDINT'
      THEN
         --2012-04-17 ETachej - Afinamiento PDN - Se invoca fn_plataforma_redin
         DBMS_OUTPUT.put_line ('fn_plataforma_redin ');
         v_necesita :=
            fn_plataforma_redin (p_pedido,
                                 p_subpedido,
                                 p_solicitud,
                                 p_producto,
                                 p_elemento,
                                 p_plataforma,
                                 p_actividad,
                                 p_tipo_trabajo,
                                 p_tipo_intercambio
                                );
      -- WQUICENO - Ajustes Para oferta POTS. Se estaban generando intercambios
      -- cuya lógica de generación no pasaba por la funcion fn_plataforma_zte
      -- debido a la evaluacion de la lista de actividades abajo escritas.
      ELSIF     p_plataforma <> 'ZTE'
            AND p_producto IN ('TO')
            AND p_actividad NOT IN
                   ('HAESP',
                    'DEESP',
                    'ACTIV',
                    'SUSPE',
                    'HATOI',
                    'DETOI',
                    'HACTA',
                    'HAUSR',
                    'DECTA'
                   )
      THEN
         DBMS_OUTPUT.put_line ('  p_elemento ' || p_elemento);
         v_necesita := 'SI';
      ELSIF p_plataforma = 'OPEN' AND p_producto IN ('WEBHOS')
      THEN
         v_necesita := 'SI';
      ELSIF p_plataforma = 'INTERA' AND p_actividad = 'ENCUE'
      THEN
         DBMS_OUTPUT.put_line ('  p_elemento ' || p_elemento);
         v_necesita := 'SI';
      ELSIF p_plataforma IN ('INTIGO', 'INTPLA')
      THEN
         v_necesita :=
            fn_plataforma_intigo (p_pedido,
                                  p_subpedido,
                                  p_solicitud,
                                  p_requerimiento,
                                  p_identificador,
                                  p_producto,
                                  p_elemento,
                                  p_plataforma,
                                  p_actividad,
                                  p_tipo_trabajo,
                                  p_tipo_intercambio
                                 );
      -- MVM Ingeniería de Software S.A.
      -- Autor: Carlos A. Villarreal Torres
      -- Fecha: Marzo 09 de 2010 10:40 AM
      -- Descripción: Se añade la opción para verificar la nueva
      --              plataforma MIUNE
      ELSIF p_plataforma = 'MIUNE'
      THEN
         v_necesita :=
            fn_plataforma_miune (p_pedido,
                                 p_subpedido,
                                 p_solicitud,
                                 p_requerimiento,
                                 p_identificador,
                                 p_producto,
                                 p_elemento,
                                 p_plataforma,
                                 p_actividad,
                                 p_tipo_trabajo,
                                 p_tipo_intercambio
                                );
      ELSIF p_plataforma = 'USRPOR'
      THEN
         v_necesita :=
            fn_plataforma_usrpor (p_pedido,
                                  p_subpedido,
                                  p_solicitud,
                                  p_requerimiento,
                                  p_identificador,
                                  p_producto,
                                  p_elemento,
                                  p_plataforma,
                                  p_actividad,
                                  p_tipo_trabajo,
                                  p_tipo_intercambio
                                 );
      ELSIF p_plataforma = 'ATS'
      THEN
         v_necesita :=
            fn_plataforma_ats (p_pedido,
                               p_subpedido,
                               p_solicitud,
                               p_requerimiento,
                               p_identificador,
                               p_producto,
                               p_elemento,
                               p_plataforma,
                               p_actividad,
                               p_tipo_trabajo,
                               p_tipo_intercambio
                              );
----------------------------------------------------------------------------------------
-- Wquiceno 18-11-2009 - Oferta POTS: Nueva plataforma ZTE
      ELSIF p_plataforma = 'ZTE'
      THEN
        -- GSANTOS     2014-08-14   INC2413275_InciPOTS.
        IF NVL(fn_verificar_cambio_prd (p_pedido, p_subpedido),'NO') = 'NO' THEN
            v_necesita :=
            fn_plataforma_zte (p_pedido,
                               p_subpedido,
                               p_solicitud,
                               p_producto,
                               p_elemento,
                               p_identificador,
                               p_plataforma,
                               p_actividad,
                               p_tipo_trabajo,
                               p_tipo_intercambio
                              );
        ELSIF NVL(fn_verificar_cambio_prd (p_pedido, p_subpedido),'NO') = 'SI' THEN
            v_necesita :=
            fn_plataforma_zte_n (p_pedido,
                               p_subpedido,
                               p_solicitud,
                               p_producto,
                               p_elemento,
                               p_identificador,
                               p_plataforma,
                               p_actividad,
                               p_tipo_trabajo,
                               p_tipo_intercambio
                              );
        END IF;
        -- FIN GSANTOS     2014-08-14   INC2413275_InciPOTS.
      ----------------------------------------------------------------------------------------
      -- GSANTOS 20-09-2011 - Television HFC Digital
      ELSIF p_plataforma = 'CISCO'
      THEN
         v_necesita :=
            fn_plataforma_cisco (p_pedido,
                                 p_subpedido,
                                 p_solicitud,
                                 p_producto,
                                 p_elemento,
                                 p_identificador,
                                 p_plataforma,
                                 p_actividad,
                                 p_tipo_trabajo,
                                 p_tipo_intercambio
                                );
    ----------------------------------------------------------------------------------------
      -- GSOLARTE 2011-10-27 - Portabilidad MVNO Automático
      ELSIF p_plataforma = 'PORTAB'
      THEN
         v_necesita :=
           fn_plataforma_portab (p_pedido,
                                 p_subpedido,
                                 p_solicitud,
                                 p_producto,
                                 p_elemento,
                                 p_identificador,
                                 p_plataforma,
                                 p_actividad,
                                 p_tipo_trabajo,
                                 p_tipo_intercambio
                                );
      ----------------------------------------------------------------------------------------
      -- CTAMAYOL 2011-11-04 - IDIOMAS
      ELSIF p_plataforma = 'TELLME'
      THEN
         v_necesita :=
           fn_plataforma_tellme (p_pedido,
                                 p_subpedido,
                                 p_solicitud,
                                 p_producto,
                                 p_elemento,
                                 p_identificador,
                                 p_plataforma,
                                 p_actividad,
                                 p_tipo_trabajo,
                                 p_tipo_intercambio
                                );
      -- JVASCO 2012-11-08 - Requerimiento IAAS
      ELSIF p_plataforma = 'VMWARE'
      THEN
         v_necesita :=
           fn_plataforma_vmware (p_pedido,
                                 p_subpedido,
                                 p_solicitud,
                                 p_producto,
                                 p_elemento,
                                 p_identificador,
                                 p_plataforma,
                                 p_actividad,
                                 p_tipo_trabajo,
                                 p_tipo_intercambio
                                );
      --2013-08-06 Jpulgarb - Servicios y Equipos
      ELSIF p_plataforma = 'SEREQU'
      THEN
         v_necesita :=
            fn_plataforma_serequ (p_pedido,
                                  p_subpedido,
                                  p_solicitud,
                                  p_requerimiento,
                                  p_identificador,
                                  p_producto,
                                  p_elemento,
                                  p_plataforma,
                                  p_actividad,
                                  p_tipo_trabajo,
                                  p_tipo_intercambio
                                 );
      END IF;

      RETURN v_necesita;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (   'FN_VERIFICAR_TAREAS - ERRWO: '
                               || SUBSTR (SQLERRM, 1, 800)
                              );
         RETURN v_necesita;
   END;
                                                       --Fin verificar tareas

 /* 2014-06-10 JGLENA REQ36900_GPONHoga procedimientos para setear y traer valores de orden de trabajo que genera intercambios */
   PROCEDURE pr_get_orden_trabajo_rec(p_orden_trabajo_rec IN OUT ty_orden_trabajo_rec) IS
   BEGIN
     p_orden_trabajo_rec := v_orden_trabajo_rec;
   END;
   PROCEDURE pr_set_orden_trabajo_rec(p_orden_trabajo_rec IN ty_orden_trabajo_rec) IS
   BEGIN
     v_orden_trabajo_rec := p_orden_trabajo_rec;
   END;

END; 