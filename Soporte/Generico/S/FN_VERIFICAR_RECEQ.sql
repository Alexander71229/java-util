FUNCTION       FN_VERIFICAR_RECEQ (
    p_pedido                IN   fnx_solicitudes.pedido_id%TYPE,
    p_subpedido             IN   fnx_solicitudes.subpedido_id%TYPE,
    p_solicitud             IN   fnx_solicitudes.solicitud_id%TYPE,
    p_identificador_reqt    IN   fnx_requerimientos_trabajos.identificador_id%TYPE,
    p_tecnologia_reqt       IN   fnx_requerimientos_trabajos.tecnologia_id%TYPE,
    p_identificador_nuevo   IN   fnx_requerimientos_trabajos.identificador_id%TYPE,
    p_tecnologia_nuevo      IN   fnx_requerimientos_trabajos.tecnologia_id%TYPE,
    p_tipo_trabajo          IN   fnx_requerimientos_trabajos.tipo_trabajo%TYPE,
    p_producto              IN   fnx_solicitudes.producto_id%TYPE,
    p_tipo_elemento         IN   fnx_solicitudes.tipo_elemento%TYPE,
    p_tipo_elemento_id      IN   fnx_solicitudes.tipo_elemento_id%TYPE
) RETURN VARCHAR2 IS
    -- Fecha        Autor       Observacion
    -- --------     ----------- ------------------------------------------------------------
    -- 2011-03-10   JGallg      Construcción inicial de la función (Desaprovisionamiento).
    -- 2011-03-23   JGallg      Solo para solicitudes de equipo se necesita generar orden de
    --                          recuperar equipo (Desaprovisionamiento).
    -- 2011-03-24   JGallg      Chequear solamente los tipo_elemento_id EQURED y EQACCP.
    -- 2011-03-30   JGallg      Cuando se presenta infraestructura compartida, se debe generar
    --                          orden de recuperación de equipo cuando son equipos de televisión.
    -- 2011-03-31   JGallg      Solo generar orden RECEQ cuando se especifiquen equipos en
    --                          FNX_TRABAJOS_SOLICITUDES.
    -- 2011-04-6    JGallg      No generar orden de recuperación de equipo para referencias
    --                          de equipos declaradas obsoletas.
    -- 2011-04-14   JGallg      Tratamiento de la tecnologia INALA.
    -- 2011-04-26   JGallg      Exclusión de televisión HFC de validación de infraestructura
    --                          compartida para efectos de recuperar equipos.
    -- 2011-05-24   JGallg      Comentar bloque que chequea tipo de retiro (de servicio o de
    --                          equipo), según solicitud de Edith Tache.
    -- 2011-06-07   JGallg      Comentar bloque que valida equipo obsoleto segun control de
    --                          cambios.
    -- 2011-07-01   JGallg      1. Considerar los mismos tipos de acceso de internet manejados
    --                             en el proceso de estudio técnico.
    --                          2. Cuando el TIPO_ELEMENTO_ID es EQURED, no considerar
    --                             infraestructura compartida.
    -- 2011-07-06   JGallg      No considerar el tema de infraestructura compartida, pues este
    --                          ya se tiene en cuenta en el estudio técnico.
    -- 2011-08-05   JGallg      No validar por tipo o plan de internet.
    -- 2012-01-27   JGallg      No generar orden RECEQ si ya existe otra orden RECEQ asociada al equipo.
    -- 2012-11-08   Cmurillg    Se agrega condición para generar RECEQ para el EQUSER
    -- 2014-07-29   OREYR       Se contempla el tipo_elemento_id EQUVAR en el cursor c_trabajos_solicitudes

    v_tecnologia_acceso   fnx_identificadores.tecnologia_id%TYPE;
    v_solic_servicio      fnx_solicitudes.solicitud_id%type;
    v_serial_equipo       fnx_configuraciones_identif.valor%TYPE;
    v_serial_equipo_val   fnx_configuraciones_identif.valor%TYPE;
    v_propiedad_equipo    fnx_configuraciones_identif.valor%TYPE;
    v_necesita            VARCHAR2(2) := 'NO';
    v_cantidad            INTEGER;
    v_obsoleto            VARCHAR2(2);
    v_motivo_retiro       fnx_caracteristica_solicitudes.valor%TYPE;

    -- Determinar el equipo que se va a recuperar
    CURSOR c_serial_equipo IS
        select valor
        from fnx_trabajos_solicitudes
        where pedido_id         = p_pedido
          and subpedido_id      = p_subpedido
          and solicitud_id      = p_solicitud
          and caracteristica_id = 200;

    -- Determinar propiedad del equipo
    CURSOR c_propiedad_equipo(pv_identif_equipo fnx_equipos.equipo_id%type) IS
        SELECT fn_valor_config_equipos(pv_identif_equipo, 1093)
        FROM dual;

    CURSOR c_solic_servicio IS
        SELECT DISTINCT solicitud_id
        FROM fnx_trabajos_solicitudes
        WHERE pedido_id = p_pedido
          AND subpedido_id = p_subpedido
          AND tipo_elemento_id in ('ACCESP', 'TOIP', 'INSIP', 'INSHFC');

    -- Determinar la cantidad de equipos a retirar estipulada en FNX_TRABAJOS_SOLICITUDES
    CURSOR c_trabajos_solicitudes IS
        SELECT COUNT (*)
        FROM fnx_trabajos_solicitudes
        WHERE pedido_id         = p_pedido
          AND subpedido_id      = p_subpedido
          AND solicitud_id      = p_solicitud
          AND tipo_elemento_id IN ('EQACCP', 'EQURED','EQUVAR')
          AND caracteristica_id = 200
          AND valor         IS NOT NULL
          AND tipo_trabajo      = 'RETIR';

    -- Determinar si ya hay una orden RECEQ con el mismo equipo
    CURSOR c_receq_equipo IS
        select b.valor
        from fnx_ordenes_trabajos a, fnx_trabajos_solicitudes b
        where a.pedido_id    = b.pedido_id
          and a.subpedido_id = b.subpedido_id
          and a.solicitud_id = b.solicitud_id
          and b.caracteristica_id = 200
          and a.cola_id      = 'RECEQ'
          and a.estado_id   <> 'ANULA'
          and a.pedido_id    = p_pedido
          and b.valor        = v_serial_equipo
          and rownum = 1;
BEGIN
    OPEN c_solic_servicio;
    FETCH c_solic_servicio INTO v_solic_servicio;
    CLOSE c_solic_servicio;

    -- -----------------------------------------------------------------------------------------
    -- Para internet: Si la tecnologia es LOGIC, obtener tecnología del identificador de acceso.
    -- -----------------------------------------------------------------------------------------
    IF p_producto = 'INTER' THEN
        IF p_tecnologia_reqt = 'LOGIC' THEN
            v_tecnologia_acceso := fn_tecnologia_identif(
                                      nvl(fn_valor_caract_identif(p_identificador_reqt, 2093),
                                         fn_valor_caract_identif_resp (p_subpedido, p_pedido, v_solic_servicio, 2093, p_identificador_reqt)));
        ELSE
            v_tecnologia_acceso := p_tecnologia_reqt;
        END IF;
    ELSE
        v_tecnologia_acceso := p_tecnologia_reqt;
    END IF;

    -- ------------------------------------------------
    -- Obtener los valores de las variables necesarios.
    -- ------------------------------------------------
    OPEN c_serial_equipo;
    FETCH c_serial_equipo INTO v_serial_equipo;
    CLOSE c_serial_equipo;

    OPEN c_receq_equipo;
    FETCH c_receq_equipo INTO v_serial_equipo_val;
    CLOSE c_receq_equipo;

    OPEN c_propiedad_equipo (v_serial_equipo);
    FETCH c_propiedad_equipo INTO v_propiedad_equipo;
    CLOSE c_propiedad_equipo;

    OPEN c_trabajos_solicitudes;
    FETCH c_trabajos_solicitudes INTO v_cantidad;
    CLOSE c_trabajos_solicitudes;

    -- ------------------------------------------------------------------------------
    -- Solo las solicitudes con tipo de elemento EQU requieren generar orden de RECEQ
    -- ------------------------------------------------------------------------------
    IF p_tipo_elemento_id IN ('EQURED', 'EQACCP') THEN
        v_necesita := 'SI';
    ELSE
        v_necesita := 'NO';
    END IF;

    IF v_necesita = 'SI' THEN
        IF p_tipo_elemento_id IN ('EQURED', 'EQACCP') AND p_producto = 'TELEV' THEN
            v_necesita := 'SI';
        ELSIF p_tipo_elemento_id IN ('EQURED', 'EQACCP') AND p_producto = 'TO' AND fn_tipo_elemento_id(p_identificador_reqt) = 'TOIP' THEN
            v_necesita := 'SI';
        ELSIF p_tipo_elemento_id IN ('EQURED', 'EQACCP') AND p_producto = 'INTER' THEN
            v_necesita := 'SI';
        -- 2012-11-08   Cmurillg    Se agrega condición para generar RECEQ para el EQUSER
        ELSIF p_tipo_elemento = 'EQU' AND p_producto = 'EQUSER' THEN
            v_necesita := 'SI';
        END IF;
    END IF;

    -- ------------------------------------------------------------------
    -- Los equipos alquilados y en comodato deben recuperarse.
    -- ------------------------------------------------------------------
    IF v_necesita = 'SI' THEN
        IF nvl(v_propiedad_equipo, 'CD') IN ('CD', 'AL') THEN
            v_necesita := 'SI';
        ELSE
            -- 2014-08-06 CVILLARE
            -- Req. 48512 - Repetidor Wifi para BA
            -- Se añade lógica para generar la orden de recuperación de equipo comprado
            -- cuando es un retiro por ley de retracto
            v_motivo_retiro := fn_valor_caracteristica_sol(p_pedido, p_subpedido, p_solicitud, 85);
            IF nvl(v_propiedad_equipo, 'CD') = 'CM' AND nvl(v_motivo_retiro, 'RXOS') = 'RLR' THEN
              v_necesita := 'SI';
            ELSE
              v_necesita := 'NO';
            END IF;
            -- Fin Req. 48512 - Repetidor Wifi para BA
        END IF;
    END IF;

    -- ---------------------------------------------------------------------------------
    -- El servicio de telefonía convencional no necesita orden de recuperación de equipo
    -- ---------------------------------------------------------------------------------
    IF p_producto = 'TO' AND fn_tipo_elemento_id(p_identificador_reqt) = 'TO' AND v_tecnologia_acceso IN ('REDCO', 'GANPA') THEN
        v_necesita := 'NO';
    END IF;

    -- --------------------------------------------------------------------------
    -- Si no hay equipos en FNX_TRABAJOS_SOLICITUDES, no se genera orden de RECEQ
    -- --------------------------------------------------------------------------
    IF v_cantidad = 0 THEN
        v_necesita := 'NO';
    END IF;

    -- ------------------------------------------------------------------------------
    -- Si ya existe una orden RECEQ con el equipo a procesar, no se necesita la orden
    -- ------------------------------------------------------------------------------
    IF nvl(v_serial_equipo_val, 'XX') = v_serial_equipo THEN
        v_necesita := 'NO';
    END IF;

    RETURN (v_necesita);
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_necesita := 'NO';
        RETURN (v_necesita);
END FN_VERIFICAR_RECEQ;