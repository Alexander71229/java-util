import java.util.*;
public class SELSIF extends Estructura{
	public String codigo;
	public String condicion;
	public ArrayList<Sentencia>verdaderas;
	public SELSIF(Tokenizer fuente){
		condicion="";
		codigo="";
		String token="";
		while(fuente.hasNext()){
			token=fuente.next();
			if(token.toUpperCase().equals("THEN")){
				break;
			}
			condicion+=" "+token;
		}
		this.codigo="ELSIF (|"+this.condicion.trim()+" |)THEN\n";
		verdaderas=new ArrayList<Sentencia>();
		ArrayList<Sentencia>instrucciones=verdaderas;
		while(fuente.hasNext()){
			token=fuente.ver();
			if(token.toUpperCase().equals("ELSE")){
				break;
			}
			if(token.toUpperCase().equals("ELSIF")){
				break;
			}
			if(token.toUpperCase().equals("END")){
				break;
			}
			token=fuente.next();
			Sentencia sentencia=new Sentencia(token,fuente);
			instrucciones.add(sentencia);
			codigo+=sentencia.codigo+"\n";
		}
	}
	public String getCodigo(int nivel){
		nivel--;
		String resultado="";
		resultado+=Util.tab(nivel)+"ELSIF "+Util.format(this.condicion).trim()+" THEN\n";
		for(int i=0;i<verdaderas.size();i++){
			resultado+=verdaderas.get(i).getCodigo(nivel+1);
		}
		return resultado;
	}
	public String getTipo(){
		return "ELSIF";
	}
	public String desc(int nivel){
		String resultado="";
		resultado=Util.tab(nivel)+this.getTipo()+" "+this.condicion+"\n";
		for(int i=0;i<verdaderas.size();i++){
			resultado+=verdaderas.get(i).desc(nivel+1);
		}
		return resultado;
	}
	public String getHTML()throws Exception{
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		String contenidoSentencias=Util.leer("HTML//Sentencias.html");
		contenido=contenido.replace("Condicion","SELSIF "+this.condicion+" THEN");
		String verdaderasCodigo="";
		for(int i=0;i<verdaderas.size();i++){
			verdaderasCodigo+=contenidoSentencias.replace("Sentencia",verdaderas.get(i).getHTML());
		}
		contenido=contenido.replace("Verdaderas",verdaderasCodigo);
		return contenido;
	}
}