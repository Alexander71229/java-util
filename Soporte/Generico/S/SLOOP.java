import java.util.*;
public class SLOOP extends Estructura{
	public String codigo;
	public ArrayList<Sentencia>instrucciones;
	public SLOOP(Tokenizer fuente){
		codigo="";
		String token="";
		instrucciones=new ArrayList<Sentencia>();
		while(fuente.hasNext()){
			token=fuente.next();
			if(token.toUpperCase().equals("END")){
				token=fuente.next();
				if(!token.toUpperCase().equals("LOOP")){
					//System.out.println(this.codigo);
					System.out.println("Error detectando fin de LOOP (END LOOP):"+token+"\n---->"+instrucciones.get(instrucciones.size()-1).codigo);
					//System.out.println("Error detectando fin de LOOP (END LOOP):"+token);
					/*for(int i=0;i<30&&fuente.hasNext();i++){
						System.out.println(fuente.next());
					}*/
					System.exit(0);
				}
				if(!fuente.next().equals(";")){
					//System.out.println(instrucciones.get(instrucciones.size()-1).codigo);
					System.out.println("Error detectando fin de LOOP (END LOOP;)");
					//System.exit(0);
				}
				codigo+="END LOOP;\n";
				break;
			}
			Sentencia sentencia=new Sentencia(token,fuente);
			instrucciones.add(sentencia);
			codigo+=sentencia.codigo+"\n";
		}
	}
	public String getCodigo(int nivel){
		String resultado="";
		resultado+=Util.tab(nivel)+"LOOP\n";
		for(int i=0;i<instrucciones.size();i++){
			resultado+=instrucciones.get(i).getCodigo(nivel+1);
		}
		resultado+=Util.tab(nivel)+"END LOOP;\n";
		return resultado;
	}
	public String getTipo(){
		return "LOOP";
	}
	public String desc(int nivel){
		String resultado="";
		resultado=Util.tab(nivel)+this.getTipo()+"\n";
		for(int i=0;i<instrucciones.size();i++){
			resultado+=instrucciones.get(i).desc(nivel+1);
		}
		resultado+=Util.tab(nivel)+"END LOOP\n";
		return resultado;
	}
	public String getHTML()throws Exception{
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		String contenidoSentencias=Util.leer("HTML//Sentencias.html");
		String sentenciasCodigo="";
		for(int i=0;i<instrucciones.size();i++){
			sentenciasCodigo+=contenidoSentencias.replace("Sentencia",instrucciones.get(i).getHTML());
		}
		contenido=contenido.replace("Sentencias",sentenciasCodigo);
		return contenido;
	}
}