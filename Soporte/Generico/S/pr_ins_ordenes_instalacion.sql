PROCEDURE       pr_ins_ordenes_instalacion (
   p_requerimiento         IN   fnx_requerimientos_trabajos.requerimiento_id%TYPE,
   p_pedido                IN   fnx_solicitudes.pedido_id%TYPE,
   p_subpedido             IN   fnx_solicitudes.subpedido_id%TYPE,
   p_solicitud             IN   fnx_solicitudes.solicitud_id%TYPE,
   p_tipo_solicitud        IN   fnx_reportes_instalacion.tipo_solicitud%TYPE,
   p_identificador_id      IN   fnx_requerimientos_trabajos.identificador_id%TYPE,
   p_identificador_nuevo   IN   fnx_requerimientos_trabajos.identificador_id%TYPE,
   p_tecnologia_id         IN   fnx_solicitudes.tecnologia_id%TYPE,
   p_servicio              IN   fnx_solicitudes.servicio_id%TYPE,
   p_producto              IN   fnx_solicitudes.producto_id%TYPE,
   p_tipo_elemento         IN   fnx_solicitudes.tipo_elemento%TYPE,
   p_tipo_elemento_id      IN   fnx_solicitudes.empresa_id%TYPE,
   p_ident_paquete         IN   fnx_solicitudes.identificador_id%TYPE,
   p_naturaleza            IN   fnx_reportes_mantenimiento.falla_id%TYPE,
   p_usuario               IN   fnx_ordenes_trabajos.usuario%TYPE,
   p_empresa               IN   fnx_servicios_productos.empresa_id%TYPE,
   p_municipio             IN   fnx_servicios_productos.municipio_id%TYPE
)
IS
-- HISTORIA DE MODIFICACIONES
-- Fecha        Autor       Observacion
-- --------     ----------- ------------------------------------------------------------
-- 2008-12-03   Fgallop      Que en el cambio de tecnologia del producto de Internet
--                           se identifique la tecnologia de retiro
-- 2011-02-24   Etachej      Se corrige la forma de obtener la tecnologia NUEVA y Anterior
--                           para los cambios de tecnologia
-- 2011-05-04   YHernana     Desaprovisionamiento - Suspensión - Con Cambio de Numero
--                           se incluye el parametro de v_actividad en fn_ident_tecno_ortr
----------------------------------------------------------------------------------------
--Variable del Cursor actividades_productos
   v_etapa                  fnx_actividades_productos.etapa_id%TYPE;
   v_secuencia              fnx_actividades_productos.secuencia%TYPE;
   v_actividad              fnx_actividades_productos.actividad_id%TYPE;
   v_funcion_verificacion   fnx_actividades_productos.funcion_verificacion%TYPE;
   v_tipo_verificacion      fnx_actividades_productos.tipo_verificacion%TYPE;
   v_funcion_enruta         fnx_actividades_productos.funcion_enruta%TYPE;
   v_tipo_infraestructura   fnx_actividades_ordenes.tipo_infraestructura%TYPE;
   v_subtipo                fnx_solicitudes.tecnologia_id%TYPE;
   v_motivo                 fnx_reportes_atenciones.motivo_id%TYPE;
   v_submotivo              fnx_reportes_atenciones.submotivo_id%TYPE := 'NO';
   v_cambio_prd             fnx_caracteristica_solicitudes.valor%TYPE := 'NO';
   v_cambio_tecno           fnx_caracteristica_solicitudes.valor%TYPE := 'NO';
   v_cambio_central         fnx_caracteristica_solicitudes.valor%TYPE := 'NO';
   v_existe_atenc           BOOLEAN;
   v_cantidad               NUMBER (3, 0);
   v_necesita               VARCHAR2 (2);
   v_tipo_elem              fnx_solicitudes.tipo_elemento%TYPE;
   v_tipo_elem_id           fnx_solicitudes.tipo_elemento_id%TYPE;

--------------------------------------------------------------
--Variables para hacer los cambios identificadores
   v_identif_nue            fnx_solicitudes.identificador_id%TYPE;
   v_tecno_nue              fnx_solicitudes.tecnologia_id%TYPE;
   v_identif_old            fnx_solicitudes.identificador_id%TYPE;
   v_tecno_old              fnx_solicitudes.tecnologia_id%TYPE;
--------------------------------------------------------------
   v_identif_ortr           fnx_solicitudes.identificador_id%TYPE;
   v_tecno_ortr             fnx_solicitudes.tecnologia_id%TYPE;
   v_valor                  fnx_caracteristica_solicitudes.valor%TYPE;
--------------------------------------------------------------
--variables con la cual se buscan las actividades
   v_serv_activ             fnx_solicitudes.servicio_id%TYPE;
   v_prod_activ             fnx_solicitudes.producto_id%TYPE;
   v_tipo_elem_activ        fnx_solicitudes.tipo_elemento%TYPE;
   v_tipo_elem_id_activ     fnx_solicitudes.tipo_elemento_id%TYPE;
   v_tecno_actividad        fnx_solicitudes.tecnologia_id%TYPE;
   v_sec_activ              fnx_actividades_productos.secuencia%TYPE;
   v_tipo_sol_activ         fnx_reportes_instalacion.tipo_solicitud%TYPE;
   v_subped                 fnx_solicitudes.subpedido_id%TYPE;
   v_soli                   fnx_solicitudes.solicitud_id%TYPE;
   v_numord                 NUMBER (3, 0)                                := 0;

--2014-05-21 AAVILAS
  v_elemento_id             fnx_solicitudes.tipo_elemento_id%TYPE;
  v_tipo_param              Varchar2 (10);
    
--2014-05-21 AAVILAS  
   --Seleccion de actividades a generar
   CURSOR c_acpr
   IS
      SELECT   etapa_id, actividad_id, secuencia, funcion_verificacion,
               tipo_verificacion, funcion_enruta
          FROM fnx_actividades_productos
         WHERE servicio_id = v_serv_activ
           AND producto_id = v_prod_activ
           AND tecnologia_id = v_tecno_actividad
           AND tipo_trabajo = v_tipo_sol_activ
           AND tipo_elemento = NVL (v_tipo_elem_activ, tipo_elemento)
           AND tipo_elemento_id = NVL (v_tipo_elem_id_activ, tipo_elemento_id)
           AND tipo_elemento_id = NVL (v_tipo_elem_id_activ, tipo_elemento_id)
           AND empresa_id = p_empresa
           AND municipio_id = p_municipio
           AND secuencia = NVL (v_sec_activ, secuencia)
           AND estado = 'ACT'
         --  AND actividad_id = 'HAIP'
         ---------------------------------
         -- GSANTOS  09-12-2009
         -- SE ADICIONA QUE EL CAMPO OPCIONALIDAD SEA NULO
         -- ESTO NO AFECTA NINGUNA TRANSACCION YA QUE EL TEMA DE
         -- ORDENES OPCIONALES APENAS SE ESTA DESARROLLANDO Y
         -- NINGUNO DE LOS PRODUCTOS SE VE AFECTADO POR ESO.
           AND opcionalidad IS NULL
         ---------------------------------
      ORDER BY secuencia, etapa_id;

   --Seleccion de actividades a generar por retiro (En Cambio de producto - ))
   CURSOR c_producto
   IS
      SELECT servicio_id, producto_id, tipo_elemento, tipo_elemento_id, valor,
             subpedido_id, solicitud_id
        FROM fnx_trabajos_solicitudes
       WHERE pedido_id = p_pedido
         AND subpedido_id IN (p_subpedido - 1)
         AND caracteristica_id = 1
         AND tipo_trabajo = 'RETIR';

   --Seleccion de actividades a generar por retiro (En Cambio de producto - ))
   CURSOR c_prod_inter
   IS
      SELECT servicio_id, producto_id, tipo_elemento, tipo_elemento_id, valor,
             subpedido_id, solicitud_id
        FROM fnx_trabajos_solicitudes
       WHERE pedido_id = p_pedido
         AND caracteristica_id = 1
         AND subpedido_id IN (p_subpedido - 1)
         AND tipo_trabajo = 'RETIR'
         AND tipo_elemento_id = 'CUENTA';
    
    
    CURSOR c_tecn_ele
    IS
      SELECT COUNT (tipo_parametrizacion)
        INTO v_cantidad
        FROM fnx_tecnologias_elementos
       WHERE servicio_id = v_serv_activ
         AND producto_id = v_prod_activ
         AND tipo_trabajo = v_tipo_sol_activ
         AND tipo_parametrizacion = 'ELEME';
    
    --2014-05-22 AAVILAS, Modificación del cursor para capturar el elemento_id
    Cursor c_tecnologias_elementos
    Is
        SELECT tipo_elemento_id
        INTO v_elemento_id
        FROM fnx_tecnologias_elementos
        WHERE servicio_id = v_serv_activ
        AND producto_id = v_prod_activ
        AND tipo_trabajo = v_tipo_sol_activ
        AND tipo_parametrizacion = 'ELEME';

         
    
BEGIN
   v_serv_activ := p_servicio;
   v_prod_activ := p_producto;
   v_tipo_sol_activ := p_tipo_solicitud;
   v_cantidad := 0;
   
   IF v_tipo_sol_activ IN ('REFAL', 'REGEN')
   THEN
      v_sec_activ := 1;
   END IF;

--Identificar si es cambio
-------------------------------------------------------------------------
   v_cambio_central :=
      fn_cambio_tipo_central (p_pedido,
                              p_subpedido,
                              p_solicitud,
                              p_identificador_id,
                              p_identificador_nuevo,
                              p_tecnologia_id
                             );
   v_cambio_tecno :=
      fn_verificar_cambiotecno (p_pedido,
                                p_subpedido,
                                p_solicitud,
                                p_identificador_id,
                                p_identificador_nuevo,
                                p_tecnologia_id
                               );
   v_cambio_prd := fn_verificar_cambio_prd (p_pedido, p_subpedido);
--------------------------------------------------------------------------------
 /*  DBMS_OUTPUT.put_line (   '****** v_cambio_prd'
                         || v_cambio_prd
                         || '******  v_cambio_tecno'
                         || v_cambio_tecno
                         || '******  v_cambio_central'
                         || v_cambio_central
                        );*/
--------------------------------------------------------------------------------
--Identificar tipo de infraestructura
   v_tipo_infraestructura :=
                           fn_verificar_infraestructura (v_etapa, v_actividad);

--------------------------------------------------------------------------
--Si en tecnologias elementos se marca el componente como configuarable
--Por elemento (ELEME), se consultan las actividades con el elemento
--------------------------------------------------------------------------
   OPEN c_tecn_ele;
   FETCH c_tecn_ele INTO v_cantidad;
   CLOSE c_tecn_ele;
   
   DBMS_OUTPUT.put_line (   '****** v_cantidad '||v_cantidad);
    --2014-05-22 AAVILAS Se comenta forma actual de validar en tecnologias elementos
  IF v_cantidad > 0
   THEN
      v_tipo_elem_activ := p_tipo_elemento;
      v_tipo_elem_id_activ := p_tipo_elemento_id;
      v_cantidad := 0;
   ELSE
      v_tipo_elem_activ := NULL;
      v_tipo_elem_id_activ := NULL;
   END IF;


--2014-05-22 AAVILAS
  /* If v_cantidad > 0
   Then
    If p_servicio = 'MOVLTE'
    Then
       Open c_tecnologias_elementos;
       Fetch c_tecnologias_elementos into v_elemento_id;
       Close c_tecnologias_elementos;
       
       If v_elemento_id <> p_tipo_elemento_id
       Then 
        v_tipo_elem_activ := NULL;
        v_tipo_elem_id_activ := NULL;
       Else
        v_tipo_elem_activ := p_tipo_elemento;
        v_tipo_elem_id_activ := p_tipo_elemento_id;
        v_cantidad := 0;
       End If;
    Else
        v_tipo_elem_activ := p_tipo_elemento;
        v_tipo_elem_id_activ := p_tipo_elemento_id;
        v_cantidad := 0;
     End If;

   Else
      v_tipo_elem_activ := NULL;
      v_tipo_elem_id_activ := NULL;
   End If;*/

--2014-05-22 AAVILAS
--------------------------------------------------------------------------
--Recorrido del cursor de actividades para cambio de producto o tecnologia
--------------------------------------------------------------------------
   IF     p_tipo_elemento <> 'EQU'
      AND p_tipo_solicitud IN ('NUEVO', 'CAMBI', 'TRASL')
      AND (   v_cambio_prd = 'SI'
           OR v_cambio_tecno = 'SI'
          )
   THEN
      --Cursor para el NUEVO en CAMBIO_PRD
      v_tipo_sol_activ := 'NUEVO';

      ---Siempre pasa a PORDE el NUEVO y este trae por defecto la tecnologia
      IF p_producto = 'INTER' AND p_tipo_elemento_id <> 'ACCESP'
      --AND  p_tipo_elemento_id NOT IN ('ACCESP','CPE','CABLEM','CPEWIM')
      THEN
        --DBMS_OUTPUT.put_line ('***** NUEVO 1');

                     v_valor :=
                        fn_ident_tecno_ortr (p_pedido,
                                             p_subpedido,
                                             p_solicitud,
                                             p_identificador_id,
                                             p_identificador_nuevo,
                                             p_tecnologia_id,
                                             v_tipo_sol_activ,
                                             p_servicio,
                                             p_producto,
                                             p_tipo_elemento,
                                             p_tipo_elemento_id,
                                             'TRM',
                                             v_identif_old,
                                             v_tecno_old,
                                             v_identif_nue,
                                             v_tecno_nue,
                                             v_identif_ortr,
                                             v_tecno_ortr
                                            );
                     v_tecno_actividad := v_tecno_ortr;
                     v_identif_ortr := NVL (p_identificador_nuevo, p_identificador_id);
       --2011-02-24 Etachej Se busca tecnologia NUEVA para el ACCESP
      ELSIF p_producto = 'INTER' AND p_tipo_elemento_id = 'ACCESP'
      THEN
            v_tecno_ortr := fn_valor_caract_subelem (p_pedido, p_subpedido,'ACCESP',33);
            v_tecno_actividad := v_tecno_ortr;
            --dbms_output.put_line('v_tecno_actividad '||v_tecno_actividad);
            v_identif_ortr := NVL (p_identificador_nuevo, p_identificador_id);
      --2011-02-24 JVASCO CAMBIO TECNO
      ELSIF p_producto = 'CNTXIP' AND p_tipo_elemento_id = 'SEDECX'
      THEN
            v_tecno_ortr := fn_valor_caract_subelem (p_pedido, p_subpedido,'SEDECX',33);
            v_tecno_actividad := v_tecno_ortr;
            --dbms_output.put_line('v_tecno_actividad  '||v_tecno_actividad);
            v_identif_ortr := NVL (p_identificador_nuevo, p_identificador_id);
            
      ELSIF p_producto = 'TRKSIP' AND p_tipo_elemento_id = 'SEDEIP'
      THEN
            v_tecno_ortr := fn_valor_caract_subelem (p_pedido, p_subpedido,'SEDEIP',33);
            v_tecno_actividad := v_tecno_ortr;
            --dbms_output.put_line('v_tecno_actividad  '||v_tecno_actividad);
            v_identif_ortr := NVL (p_identificador_nuevo, p_identificador_id);
            
      ELSE
         v_identif_ortr := NVL (p_identificador_nuevo, p_identificador_id);
         -- GSANTOS 17/06/2010
         -- SE MODIFICA PARA QUE ENCUENTRE LA TECNOLOGIA
         -- CORRECTA EN UN CAMBIO DE PRODUCTO DE PBX POTS
         -- SI SE CAMBIA A PBX POTS(SLL - TC) EN LA TABLA DE
         -- IDENTIFICADORES EL IDENTIFICADOR ESTA PRE Y TODAVIA NO
         -- TIENE TECNOLOGIA, LA VERDADERA TECNOLOGIA ESTA EN REDES TRAMITES
         /*
         v_tecno_actividad :=
            NVL (fn_tecnologia_identif (p_identificador_nuevo),
                 p_tecnologia_id
                );
          */
          v_tecno_actividad :=
            NVL(NVL(fn_tecnologia_identif (p_identificador_nuevo),
                    fn_subtipo_tecno_redco(p_pedido, p_subpedido, p_solicitud, p_identificador_nuevo)),
                p_tecnologia_id
               );
         -- FIN GSANTOS 17/06/2010

         IF v_tecno_actividad = 'INALA'
         THEN
            v_tecno_actividad :=
               fn_subtipo_tecno_inala (p_pedido,
                                       p_subpedido,
                                       p_solicitud,
                                       p_identificador_nuevo
                                      );
         END IF;

         v_tecno_ortr := v_tecno_actividad;
      END IF;

      v_tecno_nue := v_tecno_ortr;
      v_identif_nue := v_identif_ortr;
     DBMS_OUTPUT.put_line
         (   '**********2--- NUEVO ---DATOS PARA SELECCIONAR ACTIVIDADES NUEVO v_cambio_tecno'
          || v_cambio_central
          || '- v_cambio_prd '
          || v_cambio_prd
         );
      DBMS_OUTPUT.put_line (   'servicio_id = '
                            || v_serv_activ
                            || 'AND producto_id = '
                            || v_prod_activ
                            || 'AND tecnologia_id ='
                            || v_tecno_actividad
                            || 'AND tipo_trabajo = '
                            || v_tipo_sol_activ
                            || 'AND tipo_elemento = NVL ('
                            || v_tipo_elem_activ
                            || ', tipo_elemento)'
                            || 'AND tipo_elemento_id = NVL ('
                            || v_tipo_elem_id_activ
                            || ', tipo_elemento_id)'
                            || 'AND empresa_id = '
                            || p_empresa
                            || 'AND municipio_id = '
                            || p_municipio
                           );

      OPEN c_acpr;

      LOOP
         FETCH c_acpr
          INTO v_etapa, v_actividad, v_secuencia, v_funcion_verificacion,
               v_tipo_verificacion, v_funcion_enruta;

         EXIT WHEN c_acpr%NOTFOUND;

         --Funciones que reciben etapa y actividad
         IF v_funcion_verificacion IS NOT NULL
         THEN
            --DBMS_OUTPUT.put_line (   'fn_funcion_verificacion '|| v_funcion_verificacion);
            v_necesita :=
               fn_funcion_verificacion (v_funcion_verificacion,
                                        v_tipo_verificacion,
                                        p_pedido,
                                        p_subpedido,
                                        p_solicitud,
                                        p_identificador_id,
                                        p_tecnologia_id,
                                        v_identif_ortr,
                                        v_tecno_ortr,
                                        v_tipo_sol_activ,
                                        p_producto,
                                        p_tipo_elemento,
                                        p_tipo_elemento_id,
                                        v_etapa,
                                        v_actividad
                                       );
         END IF;

         IF v_necesita = 'SI'
         THEN
            
            pr_ins_ordenes_trabajos (p_requerimiento,
                                     v_etapa,
                                     v_actividad,
                                     v_secuencia,
                                     p_pedido,
                                     p_subpedido,
                                     p_solicitud,
                                     v_tipo_sol_activ,
                                     p_servicio,
                                     p_producto,
                                     p_tipo_elemento,
                                     p_tipo_elemento_id,
                                     v_identif_ortr,
                                     v_tecno_ortr,
                                     p_ident_paquete,
                                     p_naturaleza,
                                     v_funcion_enruta,
                                     p_usuario,
                                     p_empresa,
                                     p_municipio
                                    );
         END IF;
      END LOOP;

      CLOSE c_acpr;
      

      -----------------RETIRO EN CAMBIO DE PRODUCTO---------------------------------
      --Identificar el producto a RETIRAR
      IF p_tipo_elemento <> 'EQU'
      THEN
         v_subped := p_subpedido;
         v_soli := p_solicitud;
         v_identif_old := p_identificador_id;

         IF p_producto = 'INTER'
         THEN
            OPEN c_prod_inter;

            FETCH c_prod_inter
             INTO v_serv_activ, v_prod_activ, v_tipo_elem,
                  v_tipo_elem_id, v_identif_old, v_subped, v_soli;

            CLOSE c_prod_inter;
            -- 2008-12-03   Fgallop
            --2011-02-24 Etachej TR069 - Cambio de tecnologia -- se busca tecnologia anterior
            v_tecno_old :=

            --NVL(fn_tecnologia_identif(fn_identificador_subpedido(p_pedido, nvl(v_subped,p_subpedido))),
                fn_valor_caracteristica_trab (p_pedido, p_subpedido, p_solicitud,33);--);                                   

         ELSE      --Para otros cambios de productos
            v_identif_old := p_identificador_id;

            OPEN c_producto;

            FETCH c_producto
             INTO v_serv_activ, v_prod_activ, v_tipo_elem,
                  v_tipo_elem_id, v_identif_old, v_subped, v_soli;

            CLOSE c_producto;

            v_tecno_old :=
               NVL (fn_tecnologia_identif (v_identif_old),
                    fn_valor_caract_subpedido (p_pedido, p_subpedido, 33)
                   );
           -- GSANTOS 13/08/2010
           -- SE ADICIONA PARA QUE CONSULTE LA TECNOLOGIA CORRECTA EN PBX POTS
           IF p_servicio = 'TELEMP' AND
              p_producto = 'PBX'  AND
              p_tipo_elemento IN ('CMP','SRV') AND
              p_tipo_elemento_id IN ('SLL','TC') THEN
              v_tecno_old := fn_subtipo_tecno_redco(NULL, NULL, NULL, p_identificador_id);
           END IF;
           -- FIN GSANTOS 13/08/2010
         END IF;
      END IF;

      IF v_tecno_old = 'INALA'
      THEN
         v_tecno_old :=
            NVL (fn_subtipo_tecno_inala (p_pedido,
                                         p_subpedido,
                                         p_solicitud,
                                         v_identif_old
                                        ),
                 v_tecno_old
                );
      END IF;


      v_tipo_sol_activ := 'RETIR';
      v_tecno_actividad := v_tecno_old;
      v_tecno_ortr := v_tecno_old;
      v_identif_ortr := v_identif_old;

      IF v_tipo_elem IS NULL AND v_tipo_elem_id IS NULL
      THEN
         v_tipo_elem := p_tipo_elemento;
         v_tipo_elem_id := p_tipo_elemento_id;
      END IF;
      OPEN c_tecn_ele;
      FETCH c_tecn_ele INTO v_cantidad;
      CLOSE c_tecn_ele;

       IF v_cantidad > 0
       THEN
          v_tipo_elem_activ := v_tipo_elem;
          v_tipo_elem_id_activ := v_tipo_elem_id;
          v_cantidad := 0;
       ELSE
          v_tipo_elem_activ := NULL;
          v_tipo_elem_id_activ := NULL;
       END IF;

     DBMS_OUTPUT.put_line
         (   '**********2--- RETIRO ---DATOS PARA SELECCIONAR ACTIVIDADES RETIRO v_cambio_tecno'
          || v_cambio_central
          || '- v_cambio_prd '
          || v_cambio_prd
         );
      DBMS_OUTPUT.put_line (   'servicio_id = '
                            || v_serv_activ
                            || 'AND producto_id = '
                            || v_prod_activ
                            || 'AND tecnologia_id = '
                            || v_tecno_actividad
                            || 'AND tipo_trabajo = '
                            || v_tipo_sol_activ
                            || 'AND tipo_elemento = NVL ('
                            || v_tipo_elem
                            || ', tipo_elemento)'
                            || 'AND tipo_elemento_id = NVL ('
                            || v_tipo_elem_id
                            || ', tipo_elemento_id)'
                            || 'AND empresa_id = '
                            || p_empresa
                            || 'AND municipio_id = '
                            || p_municipio
                           );

--Cursor para el RETIR en CAMBIO_PRD
      OPEN c_acpr;

      LOOP
         FETCH c_acpr
          INTO v_etapa, v_actividad, v_secuencia, v_funcion_verificacion,
               v_tipo_verificacion, v_funcion_enruta;

         EXIT WHEN c_acpr%NOTFOUND;

         IF v_funcion_verificacion IS NOT NULL
         THEN
            v_necesita :=
               fn_funcion_verificacion (v_funcion_verificacion,
                                        v_tipo_verificacion,
                                        p_pedido,
                                        v_subped,
                                        v_soli,
                                        v_identif_ortr,
                                        v_tecno_ortr,
                                        v_identif_nue,
                                        v_tecno_nue,
                                        v_tipo_sol_activ,
                                        v_prod_activ,
                                        v_tipo_elem,
                                        v_tipo_elem_id,
                                        v_etapa,
                                        v_actividad
                                       );
         END IF;

         IF v_necesita = 'SI'
         THEN
                        
            pr_ins_ordenes_trabajos (p_requerimiento,
                                     v_etapa,
                                     v_actividad,
                                     v_secuencia,
                                     p_pedido,
                                     p_subpedido,
                                     p_solicitud,
                                     v_tipo_sol_activ,
                                     v_serv_activ,
                                     v_prod_activ,
                                     v_tipo_elem,
                                     v_tipo_elem_id,
                                     v_identif_ortr,
                                     v_tecno_ortr,
                                     p_ident_paquete,
                                     p_naturaleza,
                                     v_funcion_enruta,
                                     p_usuario,
                                     p_empresa,
                                     p_municipio
                                    );
         END IF;
      END LOOP;

      CLOSE c_acpr;
      
      


   ELSE         --TODO lo demas p_tipo_solicitud IN ( 'REFAL','REGEN' Y OTROS)
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--Recorrido del cursor de actividades
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
  --------------------------------------------------------------------------------
    --Identificar la tecnologia con la que se buscan las actividades y se genera la orden de trabajo
    --  DBMS_OUTPUT.put_line ('Antes de modificar Tecnologia' || p_tecnologia_id
                         --  );
        DBMS_OUTPUT.put_line ('ENTRO POR EL ELSE --> '||p_tipo_elemento); 
      if p_producto in('TRKSIP','CNTXIP' ) then
         v_valor :=fn_tecnologia_orden(p_pedido,
                              p_subpedido,
                              p_solicitud,
                              p_identificador_id,
                              p_identificador_nuevo,
                              p_tecnologia_id,
                              p_tipo_solicitud,
                              p_servicio,
                              p_producto,
                              p_tipo_elemento,
                              p_tipo_elemento_id,
                              v_tipo_infraestructura
                             );
                             v_tecno_ortr := v_valor;
                             IF p_identificador_nuevo IS NOT NULL THEN
                                v_identif_ortr := p_identificador_nuevo;
                             ELSE
                                v_identif_ortr := p_identificador_id;
                             END IF;

                             -- Maristim Junio 10-2009 para el caso de retiro de equipo de Centrex,
                             --la tecnologia llega correcta es decir que no se debe reemplazar
                             IF p_producto = 'CNTXIP'
                               AND  p_tecnologia_id <>'LOGIC' THEN
                               v_tecno_ortr := p_tecnologia_id;
                             END IF;

                             v_identif_old := v_identif_ortr;
                             v_tecno_old := v_tecno_ortr;



      else
        v_valor :=
             fn_ident_tecno_ortr(p_pedido,
                              p_subpedido,
                              p_solicitud,
                              p_identificador_id,
                              p_identificador_nuevo,
                              p_tecnologia_id,
                              p_tipo_solicitud,
                              p_servicio,
                              p_producto,
                              p_tipo_elemento,
                              p_tipo_elemento_id,
                              v_tipo_infraestructura,
                              v_identif_old,
                              v_tecno_old,
                              v_identif_nue,
                              v_tecno_nue,
                              v_identif_ortr,
                              v_tecno_ortr
                             );
      end if;

      v_tecno_actividad := v_tecno_ortr;
    /*  DBMS_OUTPUT.put_line
                     (   'TECNOLOGIA para buscar ACTIVIDADES -  v_tecno_ortr '
                      || v_tecno_actividad
                      || '='
                      || v_tecno_ortr
                     );*/
--------------------------------------------------------------------------------
      DBMS_OUTPUT.put_line
         ('3 ****************************DATOS PARA SELECCIONAR ACTIVIDADES****************************'
         );
      DBMS_OUTPUT.put_line (   'servicio_id = '
                            || v_serv_activ
                            || 'AND producto_id = '
                            || v_prod_activ
                            || 'AND tecnologia_id = '
                            || v_tecno_actividad
                            || 'AND tipo_trabajo = '
                            || v_tipo_sol_activ
                            || 'AND tipo_elemento = NVL ('
                            || v_tipo_elem_activ
                            || ', tipo_elemento)'
                            || 'AND tipo_elemento_id = NVL ('
                            || v_tipo_elem_id_activ
                            || ', tipo_elemento_id)'
                            || 'AND empresa_id = '
                            || p_empresa
                            || 'AND municipio_id = '
                            || p_municipio
                           );

/*IMPORTANTE:
   La tecnologia con la que se buscan en las actividades es la tecnologia del componente
   que pasa a PORDE  para los servicios como:
        1- TELRES, TELEMP,TELPUB (REDCO- CDMA - GANPA - INALA - GSM -PORTA)
        2- ENTRE (COA  - HFC  -    PAR   - REDCO)
        3- COMUNIDADES, IDC, REDINT, TELVIR, SRVPRO  (LOGIC)
        4- RADIO (RADIO)
        5- PROYE    (HFC - REDCO )
        6- TELMOV    (INALA)
  Para el caso de Servicio de DATOS con producto INTER o 3PLAY las actividades se van con la tecnologia
        de acceso y ademas las tecnologia LOGIC
        1- DATOS
        (CDMA  -    FIBRA -    HFC   -    LOGIC -    REDCO -    WIMAX )
        2- INTER (LOGIC -    WIMAX )
        con la que se genera la orden de trabajo es con la tecnologia del accesso
*/
        
      OPEN c_acpr;
      LOOP
         FETCH c_acpr
          INTO v_etapa, v_actividad, v_secuencia, v_funcion_verificacion,
               v_tipo_verificacion, v_funcion_enruta;

         EXIT WHEN c_acpr%NOTFOUND;
         --Identificar cual es el identificador y la tecnologia con la cual se debe
         --Por que puede ser el identificador sobre el que se debe generar la orden
         --sea otro ej: Inter debe generar orden de ACCESO
         v_tipo_infraestructura :=
                          fn_verificar_infraestructura (v_etapa, v_actividad);
        
        --2011-05-04 YHernana Se incluye el parametro de v_actividad en fn_ident_tecno_ortr
         IF    ( p_identificador_id IS NOT NULL
            AND p_identificador_nuevo IS NOT NULL
            AND p_identificador_id <> p_identificador_nuevo) OR (v_actividad IN ('SUSPC','ACTIC'))
         THEN
            IF p_producto = 'ATENC'
            THEN
               v_existe_atenc :=
                  fn_buscar_atenc_eams (v_etapa,
                                        v_actividad,
                                        v_motivo,
                                        v_submotivo
                                       );

               IF v_existe_atenc = FALSE
               THEN
                ---
                  v_funcion_verificacion := '';
                  v_etapa:='ATENC';
                  v_actividad :='SINRES';
                  v_motivo :='999';
                  v_submotivo   :='999';
                  v_existe_atenc := TRUE;
               --Limpiar variable para que no entre a la función
               END IF;
            ELSE


            if p_producto in('TRKSIP','CNTXIP') then
                   v_valor :=fn_tecnologia_orden(p_pedido,
                              p_subpedido,
                              p_solicitud,
                              p_identificador_id,
                              p_identificador_nuevo,
                              p_tecnologia_id,
                              p_tipo_solicitud,
                              p_servicio,
                              p_producto,
                              p_tipo_elemento,
                              p_tipo_elemento_id,
                              v_tipo_infraestructura
                             );
            else
                --2011-05-04 YHernana Se incluye el parametro de v_actividad en fn_ident_tecno_ortr
               v_valor :=
                  fn_ident_tecno_ortr (p_pedido,
                                       p_subpedido,
                                       p_solicitud,
                                       p_identificador_id,
                                       p_identificador_nuevo,
                                       p_tecnologia_id,
                                       p_tipo_solicitud,
                                       p_servicio,
                                       p_producto,
                                       p_tipo_elemento,
                                       p_tipo_elemento_id,
                                       v_tipo_infraestructura,
                                       v_identif_old,
                                       v_tecno_old,
                                       v_identif_nue,
                                       v_tecno_nue,
                                       v_identif_ortr,
                                       v_tecno_ortr,
                                       v_actividad
                                      );
             END IF;
            END IF;
         END IF;
        DBMS_OUTPUT.put_line ('CICLO v_funcion_verificacion --> '||v_funcion_verificacion); 
         IF v_funcion_verificacion IS NOT NULL AND v_numord = 0
         THEN
           v_necesita :=
               fn_funcion_verificacion (v_funcion_verificacion,
                                        v_tipo_verificacion,
                                        p_pedido,
                                        p_subpedido,
                                        p_solicitud,
                                        v_identif_old,
                                        v_tecno_old,
                                        v_identif_nue,
                                        v_tecno_nue,
                                        v_tipo_sol_activ,
                                        p_producto,
                                        p_tipo_elemento,
                                        p_tipo_elemento_id,
                                        v_etapa,
                                        v_actividad
                                       );
                                       DBMS_OUTPUT.put_line ('actividad -->'||v_actividad||' v_necesidad -->'||v_necesita);

            IF v_necesita = 'SI'
            THEN
                
               pr_ins_ordenes_trabajos (p_requerimiento,
                                        v_etapa,
                                        v_actividad,
                                        v_secuencia,
                                        p_pedido,
                                        p_subpedido,
                                        p_solicitud,
                                        v_tipo_sol_activ,
                                        p_servicio,
                                        p_producto,
                                        p_tipo_elemento,
                                        p_tipo_elemento_id,
                                        v_identif_ortr,
                                        v_tecno_ortr,
                                        p_ident_paquete,
                                        p_naturaleza,
                                        v_funcion_enruta,
                                        p_usuario,
                                        p_empresa,
                                        p_municipio
                                       );

               --Si es un daño o un reporte general, que solo inserte un registro
               IF v_tipo_sol_activ IN ('REGEN', 'REFAL')
               THEN
                  v_numord := 1;
                 -- DBMS_OUTPUT.put_line ('v_numOrd' || v_numord);
               END IF;
            END IF;                                    -- IF v_necesita = 'SI'
         END IF;
      END LOOP;
              
            
            
   END IF;
   
    

   IF p_ident_paquete IS NOT NULL
   THEN
      pr_upd_pendisoli (p_pedido, p_subpedido, p_solicitud, v_etapa,
                        'INICIO');
   END IF;
END; 