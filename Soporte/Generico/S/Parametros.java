import java.util.*;
public class Parametros extends Estructura{
	public String codigo;
	public Parametros(Tokenizer fuente){
		codigo="";
		if(!fuente.ver().equals("(")){
			return;
		}
		codigo=fuente.next();
		while(fuente.hasNext()){
			String token=fuente.next();
			codigo+=" "+token;
			if(token.equals(")")){
				break;
			}
		}
	}
	public String getCodigo(int nivel){
		return Util.format(this.codigo);
	}
	public String getTipo(){
		return "PARAMETROS";
	}
	public String desc(int nivel){
		String resultado="";
		resultado=this.getTipo();
		return resultado;
	}
	public String getHTML()throws Exception{
		return this.getCodigo(0);
	}
}