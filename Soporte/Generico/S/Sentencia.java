import java.util.*;
public class Sentencia extends Estructura{
	public String codigo;
	public Estructura elemento;
	public Sentencia(String token,Tokenizer fuente){
		codigo="";
		analisis(token,fuente);
	}
	public void analisis(String token,Tokenizer fuente){
		if(token.toUpperCase().equals("IF")){
			SIF sif=new SIF(fuente);
			this.codigo=sif.codigo;
			this.elemento=sif;
			return;
		}
		if(token.toUpperCase().equals("ELSIF")){
			SELSIF selsif=new SELSIF(fuente);
			this.codigo=selsif.codigo;
			this.elemento=selsif;
			return;
		}
		if(token.toUpperCase().equals("DECLARE")){
			System.out.println("Error--->"+token);
			System.exit(0);
		}
		if(token.toUpperCase().equals("BEGIN")){
			Bloque bloque=new Bloque(token,fuente);
			this.codigo=bloque.codigo;
			this.elemento=bloque;
			return;
		}
		if(token.toUpperCase().equals("LOOP")){
			SLOOP sloop=new SLOOP(fuente);
			this.codigo=sloop.codigo;
			this.elemento=sloop;
			return;
		}
		if(token.toUpperCase().equals("FOR")){
			//System.out.println("Error--->"+token);
			//System.exit(0);
			SFOR sfor=new SFOR(fuente);
			this.codigo=sfor.codigo;
			this.elemento=sfor;
			return;
		}
		if(token.toUpperCase().equals("WHILE")){
			System.out.println("Error--->"+token);
			System.exit(0);
		}
		this.codigo=token;
		while(fuente.hasNext()){
			token=fuente.next();
			if(token.equals(";")){
				this.codigo+="\n";
				return;
			}
			this.codigo+=" "+token;
		}
	}
	public String getCodigo(int nivel){
		String resultado="";
		if(elemento==null){
			resultado+=Util.tab(nivel)+Util.format(this.codigo)+";\n";
		}else{
			resultado+=elemento.getCodigo(nivel);
		}
		return resultado;
	}
	public String getTipo(){
		return "SENTENCIA";
	}
	public String getTipo2(){
		if(elemento==null){
			return "SIMPLE";
		}else{
			return elemento.getTipo();
		}
	}
	public String desc(int nivel){
		String resultado="";
		if(elemento!=null){
			resultado=this.elemento.desc(nivel);
		}
		return resultado;
	}
	public String getHTML()throws Exception{
		if(this.elemento==null){
			return this.getCodigo(0);
		}else{
			return this.elemento.getHTML();
		}
	}
}