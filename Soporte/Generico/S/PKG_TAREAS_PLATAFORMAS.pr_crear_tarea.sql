   PROCEDURE pr_crear_tarea (
      p_pedido          IN   fnx_solicitudes.pedido_id%TYPE,
      p_subpedido       IN   fnx_solicitudes.subpedido_id%TYPE,
      p_solicitud       IN   fnx_solicitudes.solicitud_id%TYPE,
      p_requerimiento   IN   fnx_tareas_plataformas.requerimiento_id%TYPE,
      p_identificador   IN   fnx_tareas_plataformas.identificador_id%TYPE,
      p_plataforma      IN   fnx_tareas_plataformas.plataforma%TYPE,
      p_actividad       IN   fnx_tareas_plataformas.actividad_id%TYPE,
      p_tipo_trabajo    IN   fnx_tareas_plataformas.tipo_trabajo%TYPE
   )
   IS
      CURSOR c_inter
      IS
         SELECT   tipo_intercambio
             FROM fnx_tipos_intercambios
            WHERE plataforma = p_plataforma
              AND actividad_id = p_actividad
              AND tipo_trabajo = p_tipo_trabajo
         ORDER BY secuencia;

-- 2010-03-23   ysolarte    Traslado de paquetes Regionales. Se modifica el cursor de características solicitudes
--                          para excluir la característica 4371 que es exclisiva de traslado de paquetes.
      CURSOR c_eleme
      IS
         SELECT tipo_elemento_id, producto_id
           FROM fnx_caracteristica_solicitudes
          WHERE pedido_id = p_pedido
            AND subpedido_id = p_subpedido
            AND solicitud_id = p_solicitud
            AND caracteristica_id <> 4371
            AND ROWNUM <= 1;

      CURSOR c_ident (p_elem IN fnx_identificadores.tipo_elemento_id%TYPE)
      IS
         SELECT identificador_id, tipo_elemento_id, producto_id
           FROM fnx_identificadores
          WHERE agrupador_id = p_identificador
            AND tipo_elemento = 'CMP'
            AND tipo_elemento_id = p_elem;

      CURSOR c_ident1
      IS
         SELECT tipo_elemento_id, producto_id
           FROM fnx_identificadores
          WHERE identificador_id = p_identificador;

      v_concepto               fnx_ordenes_trabajos.concepto_id%TYPE;
      v_elemento               fnx_caracteristica_solicitudes.tipo_elemento_id%TYPE;
      v_producto               fnx_caracteristica_solicitudes.tipo_elemento_id%TYPE;
      v_tipo_intercambio_inv   fnx_tareas_plataformas.tipo_intercambio%TYPE;
      v_parametro              fnx_tareas_plataformas.parametro%TYPE;
      v_parametro_xml          VARCHAR2 (8000);
      err_resp                 VARCHAR2 (1000);
      v_necesita               VARCHAR2 (2)                            := 'NO';
/* 2014-06-10 JGLENA REQ36900_GPONHoga variables para generar intercambios de GPON */
      v_producto_iden          fnx_identificadores.producto_id%TYPE;
      v_prod_iden              pkg_multi_servicio.tab_rec_prod_iden;
      v_tecno_iden             fnx_identificadores.tecnologia_id%TYPE;
      v_prod_prio              fnx_identificadores.producto_id%TYPE;
      v_iden_prio              fnx_identificadores.identificador_id%TYPE;
      ct_tipo_elemento_cpe     fnx_tecnologias_elementos.tipo_elemento_id%TYPE := 'CPE';

      CURSOR c_rqtr01 IS
      SELECT producto_id, servicio_id, identificador_id
      FROM fnx_requerimientos_trabajos
      WHERE requerimiento_id = p_requerimiento;

      r_rqtr c_rqtr01%rowtype;
   BEGIN
--     dbms_output.put_line('  p_plataforma - p_actividad - p_tipo_trabajo' ||   p_plataforma || '-' || p_actividad || '-' || p_tipo_trabajo);
      v_concepto := 'PPRG';

      OPEN c_eleme;

      FETCH c_eleme
       INTO v_elemento, v_producto;

      CLOSE c_eleme;

      IF v_elemento IS NULL
      THEN
         OPEN c_ident1;

         FETCH c_ident1
          INTO v_elemento, v_producto;

         CLOSE c_ident1;
      END IF;

      DBMS_OUTPUT.put_line (   '<<PR_CREAR_TAREA p_plataforma>>'
                            || p_plataforma
                            || '- p_actividad '
                            || p_actividad
                            || ' p_tipo_trabajo '
                            || p_tipo_trabajo
                           );

      IF     p_plataforma = 'OLA'
         AND p_tipo_trabajo IN ('RETIR', 'SUSPE', 'RECON')
         AND v_elemento = 'SRV'
      THEN
         DBMS_OUTPUT.put_line ('Ingresa al Loop de Identificadores de OLA ');


         FOR v_ident IN c_ident ('LMOVIL')
         LOOP
            FOR v_inter IN c_inter
            LOOP
               pkg_movil.pr_consultar_hamov (p_pedido,
                                             p_subpedido,
                                             p_solicitud,
                                             p_requerimiento,
                                             v_ident.identificador_id,
                                             p_tipo_trabajo,
                                             v_parametro
                                            );

--                  v_concepto := 'PEND';
               IF v_parametro <> 'N'
               THEN
                  pr_insertar_tarea (p_requerimiento,
                                     p_identificador,
                                     p_plataforma,
                                     p_actividad,
                                     p_tipo_trabajo,
                                     v_inter.tipo_intercambio,
                                     v_parametro,
                                     v_concepto,
                                     NULL
                                    );
               END IF;
            END LOOP;
         END LOOP;
      ELSE                                        --IF    p_plataforma = 'OLA'
         FOR v_inter IN c_inter
         LOOP
            DBMS_OUTPUT.put_line ('p_plataforma ' || p_plataforma);

            --Para
            IF p_plataforma IN ('DECO', 'CONPPV', /*'OLA', */ 'ZEUS', 'TOIP')
            THEN
--             DBMS_OUTPUT.put_line ('ENTRE POR EL INTERCAMBIO FN_PARAMETRO');
               v_parametro :=
                  fn_parametro (p_pedido,
                                p_subpedido,
                                p_solicitud,
                                p_requerimiento,
                                p_identificador,
                                p_plataforma,
                                p_actividad,
                                p_tipo_trabajo,
                                v_inter.tipo_intercambio
                               );

               IF v_parametro <> 'N'
               THEN
                  pr_insertar_tarea (p_requerimiento,
                                     p_identificador,
                                     p_plataforma,
                                     p_actividad,
                                     p_tipo_trabajo,
                                     v_inter.tipo_intercambio,
                                     v_parametro,
                                     v_concepto,
                                     NULL
                                    );
               END IF;
             ELSIF p_tipo_trabajo = 'REFAL' AND fn_tecnologia_identif(p_identificador) = 'GPON' THEN
               OPEN c_rqtr01;
               FETCH c_rqtr01 into r_rqtr;
               CLOSE c_rqtr01;

               DBMS_OUTPUT.put_line (   ' ANTES FN_PARAMETRO_XML'
                                       || ' - '
                                       || p_pedido
                                       || ' - '
                                       || p_subpedido
                                       || ' - '
                                       || p_solicitud
                                       || ' - '
                                       || p_requerimiento
                                       || ' - '
                                       || p_identificador
                                       || ' - '
                                       || v_producto
                                       || ' - '
                                       || v_elemento
                                       || ' - '
                                       || p_plataforma
                                       || ' - '
                                       || p_actividad
                                       || ' - '
                                       || p_tipo_trabajo
                                       || ' - '
                                       || v_inter.tipo_intercambio
                                      );
               v_necesita :=
                    fn_verificar_tareas (p_pedido,
                                         p_subpedido,
                                         p_solicitud,
                                         p_requerimiento,
                                         p_identificador,
                                         v_producto,
                                         v_elemento,
                                         p_plataforma,
                                         p_actividad,
                                         p_tipo_trabajo,
                                         v_inter.tipo_intercambio
                                        );
               DBMS_OUTPUT.put_line
                      (   ' FN_VERIFICAR_TAREAS v_necesita FN_PARAMETRO_XML XY '
                       || v_necesita
                      );

               IF v_necesita = 'SI'
               THEN
                  IF (r_rqtr.producto_id <> v_orden_trabajo_rec.producto_id
                  OR r_rqtr.servicio_id <> v_orden_trabajo_rec.servicio_id
                  OR r_rqtr.identificador_id <> v_orden_trabajo_rec.identificador_id) THEN
                     UPDATE fnx_requerimientos_trabajos
                        SET producto_id      = v_orden_trabajo_rec.producto_id,
                            servicio_id     =v_orden_trabajo_rec.servicio_id,
                            identificador_id = v_orden_trabajo_rec.identificador_id
                      WHERE requerimiento_id = p_requerimiento;
                  END IF;

                  v_parametro :=
                       fn_parametro_xml (p_pedido,
                                         p_subpedido,
                                         p_solicitud,
                                         p_requerimiento,
                                         p_identificador,
                                         'LOGIC',
                                         v_producto,
                                         p_plataforma,
                                         p_actividad,
                                         p_tipo_trabajo,
                                         v_inter.tipo_intercambio,
                                         v_parametro_xml
                                        );

                    IF p_plataforma = 'SER'
                    THEN
                       v_parametro :=
                          pkg_voip.fn_crear_voip (p_pedido,
                                                  p_subpedido,
                                                  p_solicitud,
                                                  v_inter.tipo_intercambio,
                                                  p_identificador
                                                 );
                    END IF;

                    pr_insertar_tarea (p_requerimiento,
                                       p_identificador,
                                       p_plataforma,
                                       p_actividad,
                                       p_tipo_trabajo,
                                       v_inter.tipo_intercambio,
                                       v_parametro,
                                       v_concepto,
                                       v_parametro_xml
                                      );


                    UPDATE fnx_requerimientos_trabajos
                          SET producto_id      = r_rqtr.producto_id,
                              servicio_id     = r_rqtr.servicio_id,
                              identificador_id = r_rqtr.identificador_id
                        WHERE requerimiento_id = p_requerimiento;
               END IF;                                   ---IF v_necesita ='SI' THEN

            ELSE
               DBMS_OUTPUT.put_line (   ' ANTES FN_PARAMETRO_XML'
                                     || ' - '
                                     || p_pedido
                                     || ' - '
                                     || p_subpedido
                                     || ' - '
                                     || p_solicitud
                                     || ' - '
                                     || p_requerimiento
                                     || ' - '
                                     || p_identificador
                                     || ' - '
                                     || v_producto
                                     || ' - '
                                     || v_elemento
                                     || ' - '
                                     || p_plataforma
                                     || ' - '
                                     || p_actividad
                                     || ' - '
                                     || p_tipo_trabajo
                                     || ' - '
                                     || v_inter.tipo_intercambio
                                    );
               v_necesita :=
                  fn_verificar_tareas (p_pedido,
                                       p_subpedido,
                                       p_solicitud,
                                       p_requerimiento,
                                       p_identificador,
                                       v_producto,
                                       v_elemento,
                                       p_plataforma,
                                       p_actividad,
                                       p_tipo_trabajo,
                                       v_inter.tipo_intercambio
                                      );
               DBMS_OUTPUT.put_line
                    (   ' FN_VERIFICAR_TAREAS v_necesita FN_PARAMETRO_XML XY '
                     || v_necesita
                    );

               IF v_necesita = 'SI'
               THEN
                  v_parametro :=
                     fn_parametro_xml (p_pedido,
                                       p_subpedido,
                                       p_solicitud,
                                       p_requerimiento,
                                       p_identificador,
                                       'LOGIC',
                                       v_producto,
                                       p_plataforma,
                                       p_actividad,
                                       p_tipo_trabajo,
                                       v_inter.tipo_intercambio,
                                       v_parametro_xml
                                      );

--OJARAMIP 20/03/2007 SE MANTIENE LAS 2 FORMAS DEL INTERCAMBIO PARA LA PLATAFORMA
-- SER MIENTRAS SE TERMINA EL DESARROLLO POR EL LADO DEL XML.
                  IF p_plataforma = 'SER'
                  THEN
                     v_parametro :=
                        pkg_voip.fn_crear_voip (p_pedido,
                                                p_subpedido,
                                                p_solicitud,
                                                v_inter.tipo_intercambio,
                                                p_identificador
                                               );
                  END IF;

                  pr_insertar_tarea (p_requerimiento,
                                     p_identificador,
                                     p_plataforma,
                                     p_actividad,
                                     p_tipo_trabajo,
                                     v_inter.tipo_intercambio,
                                     v_parametro,
                                     v_concepto,
                                     v_parametro_xml
                                    );
               END IF;                             ---IF v_necesita ='SI' THEN
            END IF;       --IF p_plataforma NOT IN('IDC', 'IAF','CNTXIP') THEN
         END LOOP;
      END IF;
/*   EXCEPTION
     WHEN OTHERS THEN
       dbms_output.put_line('ERRWO: '|| SUBSTR(SQLERRM,1,500));*/
   END;