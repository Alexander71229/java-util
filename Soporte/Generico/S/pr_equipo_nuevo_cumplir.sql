CREATE OR REPLACE PROCEDURE FENIX.pr_equipo_nuevo_cumplir(p_pedido           IN fnx_solicitudes.pedido_id%TYPE,
																	 p_subpedido        IN fnx_solicitudes.subpedido_id%TYPE,
																	 p_solicitud        IN fnx_solicitudes.solicitud_id%TYPE,
																	 p_identificador    IN fnx_identificadores.identificador_id%TYPE,
																	 p_agrupador        IN fnx_identificadores.agrupador_id%TYPE,
																	 p_tipo_elemento_id IN fnx_solicitudes.tipo_elemento_id%TYPE,
																	 p_mensaje          OUT VARCHAR2,
																	 p_municipio        IN fnx_solicitudes.municipio_id%TYPE DEFAULT 'MEDANTCOL',
																	 p_empresa          IN fnx_solicitudes.empresa_id%TYPE DEFAULT 'UNE',
																	 p_validar_pag      IN VARCHAR2 DEFAULT 'NO') IS
	-- Organizacion:        Empresas Publicas de Medellin.  Area Informatica Telco
	-- Construido por:      MVM Ingenieria de Software S.A. John Eduar Rincon G.
	-- Fecha:               Septiembre 15 de 2004
	-- Descripcion:         Procedimiento para cumplir una solicitud de equipo, con base
	--                      en las caracteristicas Serial, Marca y Referencia (200, 962 y 980)
	--                      respectivamente.
	--
	-- Fecha        Autor       Observacion
	-- ------------------------------------------------------------------------
	-- 2004-10-27   Jrincong    En la seccion de configuracion del equipo, se agrega
	--                          el tipo de elemento EQACCP
	-- 2007-02-01   Aacosta     Se adiciona el elemento CONECT el cual trae asociado en las caracteristicas
	--                          de la solicitud el equipo CPE asociado.
	-- 2007-03-08   Jrincong    Validacion Estado Equipo.  Se agrega estado PEQ como excepcion.
	-- 2007-07-12   Aacosta     Asiganar la caracteristica de multiservicio al equipo a cumplir.
	-- 2007-09-03   Aacosta     Obtención de la solicitud asociada al servicio que se está cumpliendo.
	-- 2007-09-11   Jrincong    Adición de parámetros municipio y empresa
	-- 2007-09-11   Jrincong    Instrucción UPDATE. Adición campos municipio y empresa
	-- 2008-02-05   Aacosta     Se realiza el manejo de exception para cuando el valor de la variable es nulo.
	--                          ejemplo del cambio. IF v_equipo_marca IS NULL cambia por IF NVL(v_equipo_marca,'N') = 'N'
	--                          esta misma validación se implemta para la marca y la referencia.
	-- 2008-02-13   Aacosta     Se adiciona a la lista de estado RES, con el fin de tener en cuenta los equipos de
	--                          movilidad que se reservan en la provisión del pedido.
	-- 2008-05-28   Jrincong    Cursor c_solicitud.  Inclusión de tipo elemento TOIP
	-- 2008-06-14   Jrincong    Sí el equipo se encuentra reservado y tiene un identificador, debe coincidir con
	--                          el identificador que se pasa como parámetro.
	--                          ( Requerimiento TOIP Cableras )
	-- 2008-06-18   Jrincong    Implementación de validación por dirección de servicio, cuando
	--                          la página es diferente.
	-- 2008-10-15   Aacosta     Se incluye la validación para cableras en el sentido de validar la característica 3840 para el caso
	--                          de pedido de Cableras, para los demás casos debe validar sobre la 38.
	-- 2008-10-22   Aacosta     Determinar si el equipo a cumplir es de Cableras. En caso de serlo no debe hacer las validaciones
	--                          asociadas a Multi servicio de equipos.
	-- 2008-10-28   Aacosta     Validar si el equipo que se intenta cumplir trae en las características
	--                          de la solicitud en número de la terminal la cual se va a ocupar para ese equipo.
	--                          se valida la característica XXXX (OJO FALTA POR DEFINIR POR PARTE DE ATC).
	-- 2008-12-19   Jgomezve    Se valida si el producto de la solicitud es INTER y la
	--                          tecnología es HFC se llama a pr_inter_validar_eqaccp
	--                          para validar la solicitud del equipo de acceso primario
	-- 2009-01-19   Jrincong    Proc. pr_equipo_act_estado.  Adición de valores para parámetros marca y referencia.
	--                          Requerimiento AAcosta
	-- 2009-02-16   Jrincong    Agregar condición para validar sí el identificador con el que se va a cumplir es el mismo
	--                          que tiene asociado el equipo actualmente.  Es el Caso de ATA MultiPuertos
	-- 2009-02-16   Jrincong    Determinar el tipo de elemento del identificador que sirve como agrupador. Antes
	--                          se utilizaba v_equipo_datos.identificador_id
	-- 2009-02-16   Jrincong    Definición de característica de Multiservicio con base en el tipo elemento del identificador
	--                          con el cual se está cumpliendo el equipo
	-- 2009-02-16   Jrincong    Parametro para verificar sí en caso de un error de validación, se debe sobre-escribir el mensaje.
	-- 2009-03-04   Aacosta     Se adiciona la condición a que sólo se valide esta norma asociada para los
	--                          equipos de acceso primario EQACCP.
	-- 2009-03-09   JGomezve    Proc. PKG_CUMPLIDO_UTIL.pr_inter_validar_eqaccp.  Adición de parámetro v_producto_sol
	-- 2009-04-06   Npalacir    Modificacion de la funcion para recuperar el valor de la caracteristica 2811 para los pedidos que
	--                          incluyen equipo homegate (caja magica).  Se presenta el problema que se asigna el tipo equipo CONV
	--                          ya que se busca el valor a nivel de subpedido y no a nivel de solicitud.  Se coloca NVL para el caso
	--                          en el cual no encuentre el valor a nivel de solicitud, lo asigne a nivel de subpedido
	-- 2009-04-23   Aacosta     Se incluye el tipo_elemento_id STBOX en la lista de equipos para configurar las caracteristicas.
	-- 2009-06-02   Jrincong    Unificacion Modelo TOIP.
	--                          Para los casos en los cuales se utiliza la terminal 1, es posible que no exista creado
	--                          el registro en la tabla FNX_TERMINALES_EQUIPOS.  En tal caso, no se debe
	--                          interpretar como mensaje de error y el proceso debe continuar.
	-- 2009-07-11   Wmendoza    Al momento de cumplir NUEVO BA se debe verificar si comparte Infra con
	--                          TOIP ocupado, si es así, el cumplido debe cambiar los datos del equipo
	--                          para que queden asociados al BA.
	-- 2009-07-14   Wmendoza    En caso de Multiservicio de Equipos, se verifica sì el identificador que se està cumpliendo
	--                          es del tipo ACCESP ( variable v_tipo_elemento ) y en tal caso se actualiza el identificador
	--                          del equipo con el valor del parametro p_identificador.
	-- 2009-07-18   Wmendoza    Se verifica la validación de multiequipos para el tema de MTA. Para cablera, existen multiequipos.
	-- 2009-07-21   Jrincong    Requerimiento MTA.  Cumplido Equipo CableModem o CPE ( EQACPP ) para TOIP
	--                          Buscar sì existe un identificador de acceso de Internet Banda Ancha el cual sirva de base
	--                          para el uso de infraestructura de red.  En tal caso, el equipo nuevo libre, debe
	--                          quedar asociado al identificador de Internet Banda Ancha.
	-- 2009-07-21   Jrincong    Requerimiento MTA.  Registro de transaccion Cambio de Equipo.
	--                          Se realizan las transacciones de cambio de Equipo y el equipo anterior se deja en estado LABoratorio
	-- 2009-07-22   Wmendoza    Actualizar la tabla de configuraciones equipos con el identificador correspondiente.
	-- 2009-07-27   Wmendoza    Actualizar la tabla de configuraciones equipos con el identificador correspondiente.
	-- 2009-07-28   Wmendoza    Modificación del cursor c_equipo_asociado. Se crea join con la tabla fnx_configuracioones_equipos.
	-- 2009-07-28   Wmendoza    Se verifican para los tipos de equipos diferentes a HOMEGATE.
	-- 2009-07-30   Wmendoza    Se agrega join al cursor de c_equipo_asociado.
	-- 2009-08-14   Jrincong    Cursor c_equipo_estado. Adición de parámetros al cursor.
	-- 2009-08-14   Jrincong    Multiservicio IdentificadorII
	--                          Configuraciòn de la caracterìstica del identificador asociado al equipo como Multiservicio
	--                          correspondiente al identificador que actualmente se encuentra configurado en el equipo.
	-- 2009-08-27   Etachej     SAC Equipos Thomson - Se realiza el procedimiento de transferencia de equipos para IPTV, es decir,
	--              Ysolarte    se valida si comparte infraestructura con un BA o TOIP
	-- 2009-11-27   Aacosta     Validación asociada a actualizar la característica 1093 del equipo de acceso
	--                          del banda ancha por demanda con el valor que venga en las características de la solicitud.
	-- 2009-12-29   Jsierrao    Se realiza la modificación en los mensajes del cumplido para nuevo y traslado con el fin
	--                          de dar mayor claridad a los usuarios
	-- 2010-02-26   Jaristz     Para el producto Conectividad se valida que la variable v_terminal_id quede con valor 1 cuando vaya a
	--                          actualizar el equipo terminal con el paquete PKG_EQUIPOS_UTIL.pr_equipo_act_terminal.
	-- 2010-03-24   Jrincong    Uso de dominio EACCP_CUMPL_VALIDAR, para determinar sì se debe validar la
	--                          caracterìstica 2811 con procedimiento PR_INTER_VALIDAR_EQACCP.
	-- 2011-08-08   ETachej     Problema Cumplido - Se realiza el llamado al PR_CONF_EQU_MULTISERV_CARACT
	--                          para actualizar las caracteristicas multiservicio
	-- 2011-10-13   aagudc      Se incluye el tipo_elemento_id DECO para configurar las caracteristicas de equipos para TV Digital. 
	-- 2012-05-17   Wmendoza    Se verifica si se trata de una migración
	--                          de televisión digital, en ese caso, se inserta el agrupador
	--                          y la caracteristica 33 en el equipo.
    -- 2013-04-22   Wmendoza    Se verifica si el equipo con el que se va a cumplir, corresponde
    --                          Si el valor de la caracteristica 4936 viene como DNR' y además tiene valor la 4938
    --                          entonces en un traslado con nuevo y retiro.
    --2014-07-07    dgirall     En cumplidos de Nuevo Servicio en los que el equipo no esta LIBre, se valida si el equipo esta 
    --                          ocupado por un pedido de Nuevo Retiro (DNR) en cuyo caso se debe liberar el equipo para 
    --                          continuar con la transaccion normalmente
    --2014-08-01    Dvalera     REQ55982_InteBA2Acce: No se permite configurar multiservicio con un equipo de segundo acceso banda
    --                          ancha, y tampoco cumplir con un equipo que esta ocupado con un segundo acceso banda ancha 
    --2014-11-04    Dorozm      REQ36900_GPONHoga: Adición validación tecnología y Producto para que sea adicionado registro en FNX_TERMINALES_EQUIPOS

	v_equipo_serial        fnx_equipos.equipo_id%TYPE;
	v_equipo_marca         fnx_equipos.marca_id%TYPE;
	v_equipo_referencia    fnx_equipos.referencia_id%TYPE;
	v_equipo_tipo_elem     fnx_equipos.tipo_elemento%TYPE;
	v_equipo_tipo_elemid   fnx_equipos.tipo_elemento_id%TYPE;
	v_estado_equipo        fnx_equipos.estado%TYPE;
	v_servicio             fnx_equipos.servicio_id%TYPE;
	v_producto             fnx_equipos.producto_id%TYPE;
	v_tipo_equipo          fnx_caracteristica_solicitudes.valor%TYPE;
	v_total_ext_ejecutadas fnx_caracteristica_solicitudes.valor%TYPE;
	v_pagina_asociada      fnx_caracteristica_solicitudes.valor%TYPE := NULL;
	v_pagina_nueva         fnx_caracteristica_solicitudes.valor%TYPE := NULL;

	v_direccion_asociada fnx_caracteristica_solicitudes.valor%TYPE := NULL;
	v_direccion_nueva    fnx_caracteristica_solicitudes.valor%TYPE := NULL;

	b_multi_equipos   BOOLEAN;
	b_equipo_cableras BOOLEAN := FALSE;
	b_ocupar_terminal BOOLEAN := FALSE;
	
	b_migracion_hfc     BOOLEAN:=FALSE;
    v_valor_1067   fnx_caracteristica_solicitudes.valor%TYPE := NULL;     

	v_multiservicio_identificador fnx_identificadores.identificador_id%TYPE;
	v_multiservicio_caracterist   fnx_tipos_caracteristicas.caracteristica_id%TYPE;
	v_tipo_elemento               fnx_solicitudes.tipo_elemento_id%TYPE;
	v_solicitud                   fnx_solicitudes.solicitud_id%TYPE;
	-- 2008-12-19 Jgomezve
	-- Declaración de variables para validar producto  y tecnología de la solicitud
	v_producto_sol   fnx_solicitudes.producto_id%TYPE;
	v_tecnologia_sol fnx_solicitudes.tecnologia_id%TYPE;

	-- Cursor para obtener el servicio y producto de la solicitud.
	CURSOR c_sol_serv_prod IS
		SELECT servicio_id,
				 producto_id
		  FROM fnx_solicitudes
		 WHERE pedido_id = p_pedido
			AND subpedido_id = p_subpedido
			AND solicitud_id = p_solicitud;

	-- Cursor para obtener el identificador asociado y el tipo de elemento
	-- del equipo con un estado determinado
	CURSOR c_equipo_estado(p_marca         IN fnx_marcas_referencias.marca_id%TYPE,
								  p_referencia    IN fnx_marcas_referencias.referencia_id%TYPE,
								  p_equipo        IN fnx_equipos.equipo_id%TYPE,
								  p_estado_equipo IN fnx_equipos.estado%TYPE) IS
		SELECT identificador_id,
				 tipo_elemento,
				 tipo_elemento_id
		  FROM fnx_equipos
		 WHERE equipo_id = p_equipo
			AND marca_id = p_marca
			AND referencia_id = p_referencia
			AND estado = p_estado_equipo;

	v_equipo_datos c_equipo_estado%ROWTYPE;

	--Cursor para obtener el tipo_elemento_id de un identificador.
	CURSOR c_tipo_elemento(pp_identificador IN fnx_identificadores.identificador_id%TYPE) IS
		SELECT tipo_elemento_id,
				 producto_id,
				 estado
		  FROM fnx_identificadores
		 WHERE identificador_id = pp_identificador;

	vr_datos_ident c_tipo_elemento%ROWTYPE;

	-- Cursor para obtener el número de la solicitud del pedido, de acuerdo al
	-- tipo elemento.
	CURSOR c_solicitud IS
		SELECT solicitud_id
		  FROM fnx_solicitudes
		 WHERE pedido_id = p_pedido
			AND subpedido_id = p_subpedido
			AND tipo_elemento_id IN ('ACCESP',
											 'TELEV',
											 'TOIP')
			AND estado_id = 'ORDEN';

	v_caract_pagina fnx_caracteristica_solicitudes.caracteristica_id%TYPE := NULL;
	v_terminal_id   fnx_terminales_equipos.terminal_id%TYPE := 0;
	v_fecha         fnx_caracteristica_solicitudes.valor%TYPE := NULL;

	-- 2008-12-19 Jgomezve  Cursor para obtener producto y tecnología de la solicitud
	CURSOR c_prod_tec_sol IS
		SELECT producto_id,
				 tecnologia_id,
				 tipo_elemento_id
		  FROM fnx_solicitudes
		 WHERE pedido_id = p_pedido
			AND subpedido_id = p_subpedido
			AND solicitud_id = p_solicitud;

	-- Cursor para buscar los datos de un equipo asociado a un identificador
	CURSOR c_equipo_asociado(pc_identificador IN fnx_identificadores.identificador_id%TYPE) IS
		SELECT a.equipo_id,
				 a.marca_id,
				 a.referencia_id,
				 a.tipo_elemento_id,
				 b.valor
		  FROM fnx_equipos                 a,
				 fnx_configuraciones_equipos b
		 WHERE a.identificador_id = pc_identificador
			AND a.tipo_elemento_id <> 'ANTENA'
			AND a.estado = 'OCU'
			AND b.caracteristica_id = 347
			AND a.marca_id = b.marca_id
			AND a.referencia_id = b.referencia_id
			AND a.tipo_elemento = b.tipo_elemento
			AND a.tipo_elemento_id = b.tipo_elemento_id
			AND a.equipo_id = b.equipo_id;
			
	-- Cursor para el tema de nuevo retiro
	CURSOR c_valor_4938 IS
	SELECT VALOR
	FROM FNX_CARACTERISTICA_SOLICITUDES
	WHERE PEDIDO_ID         = p_pedido
	  AND SUBPEDIDO_ID      = p_subpedido
	  AND solicitud_id      = p_solicitud
	  AND caracteristica_id = 4938;
	  
	v_valor_4938            c_valor_4938%ROWTYPE;	
    
    -- Cursor para el tema de nuevo retiro
	CURSOR c_valor_4936 IS
	SELECT VALOR
	  FROM FNX_CARACTERISTICA_SOLICITUDES
	 WHERE PEDIDO_ID         = p_pedido
	   AND caracteristica_id = 4936
       AND valor             = 'DNR'
       AND rownum            = 1;
	  
	v_valor_4936            c_valor_4936%ROWTYPE;

	vr_equipo_anterior       c_equipo_asociado%ROWTYPE;
	b_mismo_identificador    BOOLEAN := FALSE;
	v_parametro              fnx_parametros.parametro_id%TYPE;
	v_sobreescribir_mensaje  fnx_parametros.valor%TYPE;
	v_tipo_elemento_id       fnx_solicitudes.tipo_elemento_id%TYPE := NULL;
	v_identificador_accesp   fnx_identificadores.identificador_id%TYPE;
	v_identificador_equipo   fnx_identificadores.identificador_id%TYPE;
	v_agrupador_equipo       fnx_identificadores.agrupador_id%TYPE;
	v_tipo_elemento_acceso   fnx_tipos_elementos.tipo_elemento_id%TYPE;
	v_producto_acceso        fnx_productos.producto_id%TYPE;
	v_identificador_estado   fnx_identificadores.estado%TYPE;
	b_cambio_equ_asociado    BOOLEAN;
	v_mensaje_log            VARCHAR2(200);
	v_mensaje_equipo         VARCHAR2(200);
	v_estado_equipo_anterior fnx_equipos.estado%TYPE;
	v_identificador_toip     fnx_identificadores.identificador_id%TYPE;
	v_tipo_elemento_toip     fnx_tipos_elementos.tipo_elemento_id%TYPE;
	v_producto_toip          fnx_productos.producto_id%TYPE;
	b_comparte_ba            BOOLEAN := FALSE;
	v_tipo_venta             fnx_caracteristica_solicitudes.valor%TYPE := NULL;
	v_propiedad_equipo       fnx_caracteristica_solicitudes.valor%TYPE := NULL;
	v_validar_eqaccp         cg_ref_codes.rv_meaning%TYPE;
    --2014-07-07   dgirall  Nuevo cursor para identificar el pedido de retiro en pedidos de Nuevo Retiro 
    CURSOR c_pedido_retir(p_valor       fnx_caracteristica_solicitudes.valor%type,
                          p_producto    fnx_caracteristica_solicitudes.producto_id%type
                         ) IS
        SELECT carsol.pedido_id, carsol.subpedido_id
        FROM  fnx_caracteristica_solicitudes carsol
        WHERE carsol.caracteristica_id = 4941
        AND  carsol.valor       = p_valor
        AND  carsol.producto_id = p_producto
        AND  ROWNUM = 1
    ;

    --2014-07-07   dgirall  Nuevo cursor para identificar el equipo del pedido de retiro  
    CURSOR c_equipo_retir(p_pedido fnx_pedidos.pedido_id%type,
                          p_subpedido fnx_subpedidos.subpedido_id%type,
                          p_valor fnx_caracteristica_solicitudes.valor%type
                         ) IS
        SELECT COUNT(carsol.valor)
        FROM fnx_caracteristica_solicitudes carsol
        WHERE carsol.pedido_id       = p_pedido
        AND carsol.subpedido_id      = p_subpedido
        AND carsol.caracteristica_id = 200
        AND carsol.valor = p_valor
    ;
    
    --2014-07-07   dgirall  Nuevas variables 
    v_pedido_retir              fnx_pedidos.pedido_id%type       := NULL;
    v_subpedido_retir           fnx_subpedidos.subpedido_id%type := NULL;
    v_equ_ret_nue_iguales       NUMBER(2)                        := 0;
    b_nuevo_retiro              BOOLEAN                          := FALSE; 

    --2014-08-01    Dvalera REQ55982_InteBA2Acce   
   v_sol_segundo_acceso         FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
   v_eq_segundo_acceso          FNX_CONFIGURACIONES_EQUIPOS.valor%TYPE;

BEGIN

    --2014-07-07    dgirall     Inicio     
    --  Se traslada logica varias lineas adelante, cuando ya se tenga asignado el 
    --  valor de las variables v_producto_sol, v_equipo_serial, v_equipo_marca, y v_equipo_referencia 
    /*-- 2013-04-22 Se verifica si el equipo con el que se va a cumplir, corresponde
    -- Si el valor de la caracteristica 4936 viene como DNR' y además tiene valor la 4938
    -- entonces en un traslado con nuevo y retiro.
    
    OPEN c_valor_4936;
    FETCH c_valor_4936 INTO v_valor_4936;
    CLOSE c_valor_4936;
    
   IF NVL(v_valor_4936.valor,'N')='DNR' THEN
        OPEN c_valor_4938;
        FETCH c_valor_4938 INTO v_valor_4938;
        CLOSE c_valor_4938;
        
        IF v_valor_4938.valor='S' THEN
            UPDATE FNX_EQUIPOS
            SET ESTADO      = 'LIB',
            fecha_estado    = SYSDATE
            WHERE EQUIPO_ID = PKG_SOLICITUDES.fn_valor_caracteristica(p_pedido,p_subpedido,p_solicitud,200);
        END IF; 
        
   END IF;  */
   --2014-07-07    dgirall     Fin 
   
      
    -- 2008-10-22
    -- Determinar si el equipo a cumplir es de Cableras. En caso de serlo no debe hacer las validaciones
    -- asociadas a Multi servicio de equipos.

	-- 2009-07-18  Se verifica la validación de multiequipos para el tema de MTA. Para cablera, existen multiequipos.
	IF nvl(fn_dominio_descripcion('CABLERAS_EQUIPOS',
											p_municipio),
			 'NO') = 'SI' THEN
		b_equipo_cableras := TRUE;
	END IF;

	-- 2008-10-28   Aacosta    Validar si el equipo que se intenta cumplir trae en las características
	--                         de la solicitud en número de la terminal la cual se va a ocupar para ese equipo.
	--                         se valida la característica 3923.
	v_terminal_id      := pkg_solicitudes.fn_valor_caracteristica(p_pedido,
																					  p_subpedido,
																					  p_solicitud,
																					  '3923');
	v_producto_sol     := NULL;
	v_tecnologia_sol   := NULL;
	v_tipo_elemento_id := NULL;

	OPEN c_prod_tec_sol;

	FETCH c_prod_tec_sol
		INTO v_producto_sol,
			  v_tecnologia_sol,
			  v_tipo_elemento_id;

	CLOSE c_prod_tec_sol;

	IF nvl(v_terminal_id,
			 999) <> 999 OR (v_producto_sol = 'CONECT' AND v_tipo_elemento_id = 'EQURED') THEN
		b_ocupar_terminal := TRUE;
	END IF;

	IF v_producto_sol = 'CONECT' AND v_tipo_elemento_id = 'EQURED' THEN
		v_terminal_id := 1;
	END IF;

    --2014-11-04 REQ36900_GPONHoga
    --<< Inicio Dorozm
    IF v_tecnologia_sol = 'GPON' AND v_producto_sol = 'TO' THEN
        v_terminal_id := 1;
        b_ocupar_terminal := TRUE;
    END IF;
    --2014-11-04 REQ36900_GPONHoga
    -->> Fin Dorozm

	-- Determinar serial digitado en la solicitud de equipo
	v_equipo_serial := pkg_solicitudes.fn_valor_caracteristica(p_pedido,
																				  p_subpedido,
																				  p_solicitud,
																				  200);
	b_multi_equipos := FALSE;

	-- 2008-02-05  Aacosta  Se realiza el manejo de exception para cuando el valor de la variable es nulo.
	--                      ejemplo del cambio. IF v_equipo_marca IS NULL cambia por IF NVL(v_equipo_marca,'N') = 'N'
	--                      esta misma validación se implemta para la marca y la referencia.
	IF nvl(v_equipo_serial,
			 'N') = 'N' THEN
		-- 2009-12-29 Jsierrao      Se realiza la modificación en los mensajes del cumplido para nuevo y traslado con el fin
		--                            de dar mayor claridad a los usuarios
		p_mensaje := '<CUMPL> En la solicitud del equipo falta el serial correspondiente, Caracteristica 200';
	ELSE
		-- Determinar la marca digitada en la solicitud de equipo
		v_equipo_marca := pkg_solicitudes.fn_valor_caracteristica(p_pedido,
																					 p_subpedido,
																					 p_solicitud,
																					 960);

		IF nvl(v_equipo_marca,
				 'N') = 'N' THEN
			p_mensaje := '<CUMPL> En la solicitud del equipo falta la marca correspondiente, Caracteristica 960';
		ELSE
			-- Determinar la referencia digitada en la solicitud del equipo
			v_equipo_referencia := pkg_solicitudes.fn_valor_caracteristica(p_pedido,
																								p_subpedido,
																								p_solicitud,
																								982);

			IF nvl(v_equipo_referencia,
					 'N') = 'N' THEN
				p_mensaje := '<CUMPL> En la solicitud del equipo falta la referencia correspondiente, Caracteristica 982';
			END IF;
		END IF;
	END IF;
    
    
    --2014-07-07    dgirall     Inicio     
    IF p_mensaje IS NULL THEN 
    
        --  Logica trasladada con ajustes 
        OPEN c_valor_4936;
        FETCH c_valor_4936 INTO v_valor_4936;
        CLOSE c_valor_4936;
    
        IF NVL(v_valor_4936.valor,'N')='DNR' THEN
        
            --2014-07-07    dgirall     Inicio Ajuste 
            --  En vez de validar la variable 4938, se valida si el equipo asociado al retiro es el mismo 
            --  equipo del pedido de nuevo, en cuyo caso se debe liberar para poder continuar sin errores 
                                        
            /*OPEN c_valor_4938;
            FETCH c_valor_4938 INTO v_valor_4938;
            CLOSE c_valor_4938;
        
            IF v_valor_4938.valor='S' THEN
                UPDATE FNX_EQUIPOS
                SET ESTADO      = 'LIB',
                fecha_estado    = SYSDATE
                WHERE EQUIPO_ID = PKG_SOLICITUDES.fn_valor_caracteristica(p_pedido,p_subpedido,p_solicitud,200);
            END IF;*/ 
            
            OPEN c_pedido_retir(p_pedido, v_producto_sol);
            FETCH c_pedido_retir INTO v_pedido_retir, v_subpedido_retir;
            CLOSE c_pedido_retir;
                                            
            IF NVL(v_pedido_retir,'X') != 'X' THEN
                                            
                OPEN c_equipo_retir(v_pedido_retir, v_subpedido_retir, v_equipo_serial);
                FETCH c_equipo_retir INTO v_equ_ret_nue_iguales;
                CLOSE c_equipo_retir;
                                                  
                IF v_equ_ret_nue_iguales > 0 THEN
                                               
                    b_nuevo_retiro :=TRUE;
                
                    UPDATE fnx_equipos
                    SET estado       = 'LIB',
                        fecha_estado = SYSDATE
                    WHERE equipo_id     = v_equipo_serial
                    AND   marca_id      = v_equipo_marca
                    AND   referencia_id = v_equipo_referencia;
                                                  
                END IF;
                                            
            END IF;
            --2014-07-07    dgirall     Fin Ajuste  
        
        END IF;
                                                                          
    END IF; 
    --2014-07-07    dgirall     Inicio 
    
	IF p_mensaje IS NULL THEN
		-- 2008-12-19   Jgomezve
		-- Se valida si el producto de la solicitud es INTER y la tecnología es HFC se llama a pr_inter_validar_eqaccp
		-- para validar la solicitud del equipo de acceso primario

		-- 2010-03-24
		-- Uso de dominio EACCP_CUMPL_VALIDAR, para determinar sì se debe validar la caracterìstica 2811
		-- en el equipo EQACPP con procedimiento PR_INTER_VALIDAR_EQACCP.
		IF p_tipo_elemento_id = 'EQACCP' THEN
			v_validar_eqaccp := fn_dominio_values_meaning('EQACCP_CUMPLIR_VALIDAR',
																		 v_producto_sol,
																		 v_tecnologia_sol);
		ELSE
			v_validar_eqaccp := 'NO';
		END IF;

		-- 2009-03-04   Aacosta   Se adiciona la condición a que sólo se valide esta norma asociada para los
		--                        equipos de acceso primario EQACCP.
		IF nvl(v_validar_eqaccp,
				 'NO') = 'SI' THEN
			-- Validar el equipo de acceso primario asociado a la solicitud.
			pkg_cumplido_util.pr_inter_validar_eqaccp(p_pedido,
																	p_subpedido,
																	p_solicitud,
																	v_producto_sol,
																	p_mensaje);
			dbms_output.put_line('paso por cumplido_util ');
		END IF;

		IF p_mensaje IS NULL THEN
			-- Determinar el tipo de elemento y el tipo elementoID del equipo
			pkg_equipos_util.pr_equipo_tipoelem(v_equipo_serial,
															v_equipo_marca,
															v_equipo_referencia,
															v_equipo_tipo_elem,
															v_equipo_tipo_elemid,
															p_mensaje);
		END IF;
	END IF;

	-- SECCION Validacion del estado del equipo que se va a ocupar
	-- 2007-07-12  Aacosta  Asignar la caracteristica de multiservicio al equipo a cumplir.
	IF p_mensaje IS NULL THEN
		v_estado_equipo := pkg_equipos_util.fn_estado_equipo(v_equipo_serial,
																			  v_equipo_marca,
																			  v_equipo_referencia,
																			  v_equipo_tipo_elemid,
																			  v_equipo_tipo_elem);

		-- 2008-02-13   Aacosta   Se adiciona a la lista de estado RES, con el fin de tener en cuenta los equipos de
		--                        movilidad que se reservan en la provisión del pedido.
        IF v_estado_equipo NOT IN ('LIB','PEQ','RES') THEN
        
            -- 2014-08-01   Dvalera     No se puede cumplir si es un equipo de segundo acceso BA y esta ocupado
            v_eq_segundo_acceso:= pkg_equipos_util.fn_valor_configuracion_mr(v_equipo_marca,
                                            v_equipo_referencia,v_equipo_serial,5595);
            
            IF nvl(v_eq_segundo_acceso,'N') = 'S' THEN

                p_mensaje := '<CUMPL> El Equipo con el que se intenta cumplir es de segundo acceso banda ancha.';
            
            ELSE

                OPEN c_equipo_estado(v_equipo_marca,
                                     v_equipo_referencia,
                                     v_equipo_serial,
                                     'OCU');
                FETCH c_equipo_estado
                    INTO v_equipo_datos;
                CLOSE c_equipo_estado;
            
                IF nvl(v_equipo_datos.identificador_id,
                         'NO') = 'NO' THEN
                    p_mensaje := '<CUMPL> El Equipo está Ocupado, pero no tiene identificador asociado.';
                END IF;            
            
            END IF;
                
            -- 2014-08-01   Dvalera     FIN 
			
			IF p_mensaje IS NULL AND NOT b_equipo_cableras THEN
				IF p_validar_pag = 'SI' THEN
					-- 2007-09-03   Aacosta
					-- Obtención de la solicitud asociada al servicio que se está cumpliendo.
					OPEN c_solicitud;

					FETCH c_solicitud
						INTO v_solicitud;

					CLOSE c_solicitud;

					-- 2008-10-15
					-- Se incluye la validación para cableras en el sentido de validar la característica 3840 para el caso
					-- de pedido de Cableras, para los demás casos debe validar sobre la 38.
					IF nvl(fn_dominio_descripcion('CABLERAS',
															p_municipio),
							 'NING') <> 'NING' THEN
						v_caract_pagina := '3840';
					ELSE
						v_caract_pagina := '38';
					END IF;

					v_pagina_asociada := pkg_identificadores.fn_valor_configuracion(v_equipo_datos.identificador_id,
																										 v_caract_pagina);
					v_pagina_nueva    := nvl(pkg_solicitudes.fn_valor_caracteristica(p_pedido,
																										  p_subpedido,
																										  v_solicitud,
																										  v_caract_pagina),
													 fn_valor_caract_subpedido(p_pedido,
																						p_subpedido,
																						v_caract_pagina));

					-- 2009-02-16
					-- Agregar condición para validar sí el identificador con el que se va a cumplir es el mismo
					-- que tiene asociado el equipo actualmente.  Es el Caso de ATA MultiPuertos
					IF nvl(v_pagina_asociada,
							 'SIN') = nvl(v_pagina_nueva,
											  'NOT') OR v_equipo_datos.identificador_id = p_identificador THEN
						b_multi_equipos := TRUE;

						IF v_equipo_datos.identificador_id = p_identificador THEN
							b_mismo_identificador := TRUE;
						ELSE
							b_mismo_identificador := FALSE;
						END IF;
					ELSE
						-- 2008-06-18
						-- Implementación de validación por dirección de servicio, cuando
						-- la página es diferente.

						-- Sí la página no es igual, se debe verificar sí las direcciones son iguales.
						v_direccion_asociada := pkg_identificadores.fn_valor_configuracion(v_equipo_datos.identificador_id,
																												 35);
						v_direccion_nueva    := nvl(pkg_solicitudes.fn_valor_caracteristica(p_pedido,
																												  p_subpedido,
																												  v_solicitud,
																												  35),
															 fn_valor_caract_subpedido(p_pedido,
																								p_subpedido,
																								35));

						IF nvl(v_direccion_asociada,
								 'SIN') = nvl(v_direccion_nueva,
												  'NOT') THEN
							b_multi_equipos := TRUE;
						ELSE
							b_multi_equipos := FALSE;

							-- 2009-12-29 Jsierrao      Se realiza la modificación en los mensajes del cumplido para nuevo y traslado con el fin
							--                          de dar mayor claridad a los usuarios
							IF v_direccion_nueva IS NULL THEN
								p_mensaje := '<CUMPL> Por favor verifique el estado del serial del equipo ya que ' ||
												 ' este puede encontrarse asociado a otro contrato.';
							ELSIF v_direccion_asociada IS NULL THEN
								p_mensaje := '<CUMPL> El serial asignado al identificador ' || p_identificador || ' se encuentra ocupado, por favor verique.';
							ELSIF v_pagina_nueva IS NULL THEN
								p_mensaje := '<CUMPL>  Por favor verifique el estado del serial del equipo ya que ' ||
												 ' este puede encontrarse asociado a otro contrato.';
							ELSIF v_pagina_asociada IS NULL THEN
								p_mensaje := '<CUMPL> El serial asignado al identificador ' || p_identificador || ' se encuentra ocupado, por favor verique.';
							--2014-07-07    dgirall     Se condiciona mensaje de error a pedidos que no sean Nuevo Retiro 
                            --ELSIF v_direccion_nueva <> v_direccion_asociada THEN 
                            ELSIF NOT b_nuevo_retiro AND (v_direccion_nueva <> v_direccion_asociada) THEN
                                p_mensaje := '<CUMPL> El serial asignado al identificador ' || p_identificador || ' se encuentra ocupado, por favor verique.';
                            --2014-07-07    dgirall     Se condiciona mensaje de error a pedidos que no sean Nuevo Retiro 
                            --ELSIF v_pagina_nueva <> v_pagina_asociada THEN 
							ELSIF NOT b_nuevo_retiro AND (v_pagina_nueva <> v_pagina_asociada) THEN
								p_mensaje := '<CUMPL> El serial asignado al identificador ' || p_identificador || ' se encuentra ocupado, por favor verique.';
							END IF;
						END IF;
					END IF;
				ELSE
					b_multi_equipos := FALSE;
					p_mensaje       := '<CUMPL> Equipo no está LIBre /PEQ/RES <Serial> ' || v_equipo_serial || ' <Marca> ' || v_equipo_marca || ' <Ref> ' ||
											 v_equipo_referencia;
				END IF;

				-- SECCION.  Configurar caracteristicas Multiservicio
				IF p_mensaje IS NULL AND b_multi_equipos THEN
					-- 2009-02-16
					-- Determinar el tipo de elemento del identificador que sirve como agrupador. Antes
					-- se utilizaba v_equipo_datos.identificador_id
					OPEN c_tipo_elemento(p_identificador);

					FETCH c_tipo_elemento
						INTO v_tipo_elemento,
							  v_producto,
							  v_identificador_estado;

					CLOSE c_tipo_elemento;

					-- 2009-02-16
					-- Definición de característica de Multiservicio con base en el tipo elemento del identificador
					-- con el cual se está cumpliendo el equipo
					IF v_tipo_elemento = 'ACCESP' AND NOT b_mismo_identificador THEN
						v_multiservicio_identificador := p_identificador;
						v_multiservicio_caracterist   := 2093;
					ELSIF v_tipo_elemento IN ('TELEV',
													  'INSIP') AND NOT b_mismo_identificador THEN
						v_multiservicio_identificador := p_identificador;
						v_multiservicio_caracterist   := 2087;
					ELSIF v_tipo_elemento = 'TOIP' AND NOT b_mismo_identificador THEN
						v_multiservicio_identificador := p_identificador;
						v_multiservicio_caracterist   := 4093;
					ELSE
						v_multiservicio_identificador := NULL;
						-- Se está cumpliendo con otro tipo de elemento no definido
					END IF;

					-- Marcación del equipo como multiservicio. Caract 2095
					pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
																	  v_equipo_referencia,
																	  v_equipo_datos.tipo_elemento_id,
																	  v_equipo_datos.tipo_elemento,
																	  v_equipo_serial,
																	  2095,
																	  'S',
																	  p_mensaje);

					IF v_multiservicio_identificador IS NOT NULL AND p_mensaje IS NULL THEN
						-- Marcación característica de multiservicio.
						pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
																		  v_equipo_referencia,
																		  v_equipo_datos.tipo_elemento_id,
																		  v_equipo_datos.tipo_elemento,
																		  v_equipo_serial,
																		  v_multiservicio_caracterist,
																		  v_multiservicio_identificador,
																		  p_mensaje);
					END IF;

					-- 2009-08-14
					-- SECCION.  Multiservicio IdentificadorII
					-- Configuraciòn de la caracterìstica del identificador asociado al equipo como Multiservicio
					-- correspondiente al identificador que actualmente se encuentra configurado en el equipo.
					OPEN c_tipo_elemento(v_equipo_datos.identificador_id);

					FETCH c_tipo_elemento
						INTO vr_datos_ident;

					CLOSE c_tipo_elemento;

					IF vr_datos_ident.tipo_elemento_id = 'ACCESP' AND NOT b_mismo_identificador THEN
						v_multiservicio_caracterist := 2093;
					ELSIF vr_datos_ident.tipo_elemento_id IN ('TELEV',
																			'INSIP') AND NOT b_mismo_identificador THEN
						v_multiservicio_caracterist := 2087;
					ELSIF vr_datos_ident.tipo_elemento_id = 'TOIP' AND NOT b_mismo_identificador THEN
						v_multiservicio_caracterist := 4093;
					ELSE
						-- El identificador v_equipo_datos.identificador_id tiene un tipo de elemento no definido
						-- para el uso de Multiservicio.
						-- La variable v_multiservicio_caracterist se hace nula para controlar la inserciòn posterior
						-- de la caracterìstica
						v_multiservicio_caracterist := NULL;
					END IF;

					IF v_multiservicio_caracterist IS NOT NULL AND p_mensaje IS NULL THEN
						-- Marcación característica de multiservicio.para el identificador asociado al equipo
						pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
																		  v_equipo_referencia,
																		  v_equipo_datos.tipo_elemento_id,
																		  v_equipo_datos.tipo_elemento,
																		  v_equipo_serial,
																		  v_multiservicio_caracterist,
																		  v_equipo_datos.identificador_id,
																		  p_mensaje);
					END IF;

					-- FIN SECCION,  Multiservicio IdentificadorII

					-- Ocupar la terminal_id asociada al equipo.
					IF b_ocupar_terminal THEN
						pkg_equipos_util.pr_equipo_act_terminal(v_equipo_tipo_elemid,
																			 v_equipo_tipo_elem,
																			 v_equipo_marca,
																			 v_equipo_referencia,
																			 v_equipo_serial,
																			 v_terminal_id,
																			 'OCU',
																			 p_identificador,
																			 p_agrupador,
																			 p_mensaje);

						-- 2009-06-02
						-- Unificacion Modelo TOIP.
						-- Para los casos en los cuales se utiliza la terminal 1, es posible que no exista creado
						-- el registro en la tabla FNX_TERMINALES_EQUIPOS.  En tal caso, no se debe
						-- interpretar como mensaje de error y el proceso debe continuar.
						IF p_mensaje IS NOT NULL AND v_terminal_id = 1 THEN
							p_mensaje := NULL;
						END IF;
					END IF;
				END IF;
				-- FIN SECCION.  Configurar caracteristicas Multiservicio
			END IF;
		ELSE
			-- El equipo se encuentra LIBRE ( mayoria de los casos ), RESERvado o
			-- en concepto PEQ

			-- 2008-06-14
			-- Sí el equipo se encuentra reservado y tiene un identificador, debe coincidir con
			-- el identificador que se pasa como parámetro.
			-- ( Requerimiento TOIP Cableras )
			IF v_estado_equipo = 'RES' THEN
				OPEN c_equipo_estado(v_equipo_marca,
											v_equipo_referencia,
											v_equipo_serial,
											v_estado_equipo);

				FETCH c_equipo_estado
					INTO v_equipo_datos;

				CLOSE c_equipo_estado;

				IF v_equipo_datos.identificador_id IS NOT NULL AND v_equipo_datos.identificador_id <> p_identificador THEN
					p_mensaje := '<CUMPL> ID asociado al equipo no corresponde con ID ' || p_identificador;
					dbms_output.put_line('<CUMPL> ID asociado al equipo no corresponde con ID ' || p_identificador);
				END IF;
			END IF;
		END IF;
		---------------
	END IF;

	-- FIN SECCION Validacion del estado del equipo que se va a ocupar
	IF p_mensaje IS NULL THEN
		OPEN c_sol_serv_prod;

		FETCH c_sol_serv_prod
			INTO v_servicio,
				  v_producto;

		CLOSE c_sol_serv_prod;
	END IF;

	-- 2009-07-21
	-- Requerimiento MTA.  Cumplido Equipo CableModem o CPE ( EQACPP ) para TOIP
	-- Buscar sì existe un identificador de acceso de Internet Banda Ancha el cual sirva de base
	-- para el uso de infraestructura de red.  En tal caso, el equipo nuevo libre, debe quedar asociado al
	-- identificador de Internet Banda Ancha.
	IF p_mensaje IS NULL AND NOT b_multi_equipos THEN
		v_identificador_equipo := p_identificador;
		v_agrupador_equipo     := p_agrupador;

		--2009-08-27 Se realiza la validación para IPTV, si comparte infraestructura con un BA
		IF (v_servicio = 'TELRES' AND v_producto = 'TO' AND v_tipo_elemento_id = 'EQACCP') OR
			(v_servicio = 'ENTRE' AND v_producto = 'TELEV' AND v_tipo_elemento_id = 'EQACCP') THEN
			-- Verificar sì se utilizò un identificador de ACCESP para el reuso de Infraestructura
			-- Se hace la busqueda por subpedido, dado que la caracterìstica debe encontrarse en
			-- la solicitud de TOIP
			v_identificador_accesp := fn_valor_caract_subpedido(p_pedido,
																				 p_subpedido,
																				 2093);

			IF v_identificador_accesp IS NOT NULL THEN
				-- Validar que el identificador sea del tipo ACCESP para garantizar la transferencia correcta.
				OPEN c_tipo_elemento(v_identificador_accesp);

				FETCH c_tipo_elemento
					INTO v_tipo_elemento_acceso,
						  v_producto_acceso,
						  v_identificador_estado;

				CLOSE c_tipo_elemento;

				IF v_tipo_elemento_acceso = 'ACCESP' AND
					v_identificador_estado IN ('OCU',
														'XLI') THEN
					-- En este caso se garantiza que el identificador es del tipo elemento ACCESP y el
					-- equipo nuevo debe asignarse a este identificador.
					v_identificador_equipo := v_identificador_accesp;
					v_agrupador_equipo     := p_agrupador;
					-- Se cambian los valores de las variables v_servicio y v_producto a DATOS - INTER
					v_servicio            := 'DATOS';
					v_producto            := 'INTER';
					b_cambio_equ_asociado := FALSE;

					-- Buscar el equipo que actualmente tiene asociado el identificador ACCESP para luego
					-- ser liberado en la secciòn Liberaciòn Equipo Anterior.
					FOR reg IN c_equipo_asociado(v_identificador_accesp) LOOP
						-- 2009-07-28 Se verifican para los tipos de equipos diferentes a HOMEGATE.
						IF nvl(reg.valor,
								 'CABLEM') <> 'HOMEGATE' THEN
							-- Solo se asignan las variables para el equipo que no es HOMEGATE,es decir, el cablemodem
							vr_equipo_anterior.marca_id         := reg.marca_id;
							vr_equipo_anterior.referencia_id    := reg.referencia_id;
							vr_equipo_anterior.equipo_id        := reg.equipo_id;
							vr_equipo_anterior.tipo_elemento_id := reg.tipo_elemento_id;
							b_cambio_equ_asociado               := TRUE;
							b_comparte_ba                       := TRUE;
						END IF;
					END LOOP;
				END IF;
			END IF;
		END IF;

		--2009-08-27 Se realiza la validación para IPTV, si comparte infraestructura con un TOIP
		--2009-08-27 Se realiza la validación para IPTV, si comparte infraestructura con un TOIP
		IF (v_servicio = 'ENTRE' AND v_producto = 'TELEV' AND v_tipo_elemento_id = 'EQACCP') AND NOT b_comparte_ba THEN
			-- Verificar sì se utilizò un identificador de TOIP para el reuso de Infraestructura
			-- Se hace la busqueda por subpedido, dado que la caracterìstica debe encontrarse en
			-- la solicitud de IPTV
			v_identificador_toip := fn_valor_caract_subpedido(p_pedido,
																			  p_subpedido,
																			  4093);

			--dbms_output.put_line('v_identificador_toip: '||v_identificador_toip);
			IF v_identificador_toip IS NOT NULL THEN
				-- Validar que el identificador sea del tipo ACCESP para garantizar la transferencia correcta.
				OPEN c_tipo_elemento(v_identificador_toip);

				FETCH c_tipo_elemento
					INTO v_tipo_elemento_toip,
						  v_producto_toip,
						  v_identificador_estado;

				CLOSE c_tipo_elemento;

				IF v_tipo_elemento_toip = 'TOIP' AND v_identificador_estado IN ('OCU',
																									 'XLI') THEN
					-- En este caso se garantiza que el identificador es del tipo elemento EQACCP y el
					-- equipo nuevo debe asignarse a este identificador.
					v_identificador_equipo := v_identificador_toip;
					v_agrupador_equipo     := p_agrupador;
					-- Se cambian los valores de las variables v_servicio y v_producto a TELRES - TO
					v_servicio            := 'TELRES';
					v_producto            := 'TO';
					b_cambio_equ_asociado := FALSE;

					-- Buscar el equipo que actualmente tiene asociado el identificador ACCESP para luego
					-- ser liberado en la secciòn Liberaciòn Equipo Anterior.
					FOR reg IN c_equipo_asociado(v_identificador_toip) LOOP
						-- 2009-07-28 Se verifican para los tipos de equipos diferentes a HOMEGATE.
						IF nvl(reg.valor,
								 'CABLEM') <> 'HOMEGATE' THEN
							-- Solo se asignan las variables para el equipo que no es HOMEGATE,es decir, el cablemodem
							vr_equipo_anterior.marca_id         := reg.marca_id;
							vr_equipo_anterior.referencia_id    := reg.referencia_id;
							vr_equipo_anterior.equipo_id        := reg.equipo_id;
							vr_equipo_anterior.tipo_elemento_id := reg.tipo_elemento_id;
							b_cambio_equ_asociado               := TRUE;
						END IF;
					END LOOP;
				END IF;
			END IF;
		END IF;
	END IF;

	-- 2009-11-27  Aacosta  Validación asociada a actualizar la característica 1093 del equipo de acceso
	--                      del banda ancha por demanda con el valor que venga en las características de la solicitud.
	v_tipo_venta := fn_valor_caract_subpedido(p_pedido,
															p_subpedido,
															1339);

	IF p_mensaje IS NULL AND b_multi_equipos AND nvl(v_tipo_venta,
																	 'N') = 'DEM' THEN
		v_propiedad_equipo := nvl(fn_valor_caracteristica_sol(p_pedido,
																				p_subpedido,
																				p_solicitud,
																				1093),
										  'CD');
		pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
														  v_equipo_referencia,
														  v_equipo_tipo_elemid,
														  v_equipo_tipo_elem,
														  v_equipo_serial,
														  1093,
														  v_propiedad_equipo,
														  p_mensaje);
	END IF;

	IF p_mensaje IS NULL AND NOT b_multi_equipos THEN
		-- 2009-01-19   Jrincong
		-- Adición de valores para parámetros marca y referencia.  Requerimiento AAcosta
		-- Ocupar el equipo con el identificador
		pkg_equipos_util.pr_equipo_act_estado(v_equipo_tipo_elemid,
														  v_equipo_tipo_elem,
														  v_equipo_serial,
														  'OCU',
														  v_identificador_equipo,
														  v_agrupador_equipo,
														  p_mensaje,
														  p_municipio,
														  p_empresa,
														  v_equipo_marca,
														  v_equipo_referencia);

		-- Ocupar la terminal_id asociada al equipo.
		IF b_ocupar_terminal THEN
			pkg_equipos_util.pr_equipo_act_terminal(v_equipo_tipo_elemid,
																 v_equipo_tipo_elem,
																 v_equipo_marca,
																 v_equipo_referencia,
																 v_equipo_serial,
																 v_terminal_id,
																 'OCU',
																 p_identificador,
																 p_agrupador,
																 p_mensaje);

			-- 2009-06-02
			-- Unificacion Modelo TOIP.
			-- Para los casos en los cuales se utiliza la terminal 1, es posible que no exista creado
			-- el registro en la tabla FNX_TERMINALES_EQUIPOS.  En tal caso, no se debe
			-- interpretar como mensaje de error y el proceso debe continuar.
			IF p_mensaje IS NOT NULL AND v_terminal_id = 1 THEN
				p_mensaje := NULL;
			END IF;
		END IF;

		-- Actualizacion de campos servicio y producto del equipo
		BEGIN
			UPDATE fnx_equipos
				SET servicio_id  = v_servicio,
					 producto_id  = v_producto,
					 ubicacion_id = NULL,
					 municipio_id = p_municipio,
					 empresa_id   = p_empresa
			 WHERE marca_id = v_equipo_marca
				AND referencia_id = v_equipo_referencia
				AND tipo_elemento_id = v_equipo_tipo_elemid
				AND tipo_elemento = v_equipo_tipo_elem
				AND equipo_id = v_equipo_serial;
		END;
	END IF;

	IF p_mensaje IS NULL AND NOT b_multi_equipos THEN
		-- Dependiendo del tipo de elemento, la configuracion del equipo se hace de
		-- manera diferente.
		-- 2007-02-01 Aacosta Se adiciona el elemento CONECT el cual trae asociado en las caracteristicas
		--                    de la solicitud el equipo CPE asociado.

		-- 2009-04-23  Aacosta  Se incluye el tipo_elemento_id STBOX en la lista de equipos para configurar las caracteristicas.
		-- 2011-10-13 aagudc Se incluye el tipo_elemento_id DECO para configurar las caracteristicas de equipos para TV Digital.
		IF p_tipo_elemento_id IN ('EQURED',
										  'EQACCP',
										  'CONECT',
										  'STBOX',
										  'DECO') THEN
			-- Configurar el equipo con base en las caracteristicas de la solicitud
			pkg_equipos_util.pr_configurar_equipo_mr(p_pedido,
																  p_subpedido,
																  p_solicitud,
																  v_equipo_serial,
																  v_equipo_tipo_elemid,
																  v_equipo_tipo_elem,
																  v_equipo_marca,
																  v_equipo_referencia,
																  p_mensaje);

			IF b_cambio_equ_asociado THEN
				-- 2009-07-22  Actualizar la tabla de configuraciones equipos con el identificador correspondiente.
				pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
																  v_equipo_referencia,
																  v_equipo_tipo_elemid,
																  'EQU',
																  v_equipo_serial,
																  90,
																  v_identificador_accesp,
																  p_mensaje);
			END IF;
		ELSE
			-- Insertar las caracteristicas basicas para el equipo ( serial, marca,
			-- referencia, identificador asociado  ( 200, 960, 982 y 90 respectiv.)

			-- Insertar la caracteristica identificador asociado.
			pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
															  v_equipo_referencia,
															  v_equipo_tipo_elemid,
															  v_equipo_tipo_elem,
															  v_equipo_serial,
															  90,
															  nvl(p_identificador,
																	p_agrupador),
															  p_mensaje);
			-- Insertar la caracteristica de serial de equipo.
			pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
															  v_equipo_referencia,
															  v_equipo_tipo_elemid,
															  v_equipo_tipo_elem,
															  v_equipo_serial,
															  200,
															  v_equipo_serial,
															  p_mensaje);
			-- Insertar la caracteristica marca de equipo.
			pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
															  v_equipo_referencia,
															  v_equipo_tipo_elemid,
															  v_equipo_tipo_elem,
															  v_equipo_serial,
															  960,
															  v_equipo_marca,
															  p_mensaje);
			-- Insertar la caracteristica referencia de equipo.
			pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
															  v_equipo_referencia,
															  v_equipo_tipo_elemid,
															  v_equipo_tipo_elem,
															  v_equipo_serial,
															  982,
															  v_equipo_referencia,
															  p_mensaje);
		END IF;
	END IF;

	-- 2009-07-14
	-- En caso de Multiservicio de Equipos, se verificar sì el identificador que se està cumpliendo
	-- es del tipo ACCESP ( variable v_tipo_elemento ) y en tal caso se actualiza el identificador
	-- del equipo con el valor del parametro p_identificador
	IF p_mensaje IS NULL AND b_multi_equipos THEN
		-- Verificar sì el identificador_id para el cual se està cumpliendo, es un identificador
		-- de ACCESP ( DATOS - INTER ).  En tal caso, se hace la transferencia al identificador
		-- que viene como paràmetro
		IF v_tipo_elemento = 'ACCESP' THEN
			BEGIN
				UPDATE fnx_equipos
					SET identificador_id = p_identificador,
						 agrupador_id     = NULL,
						 servicio_id      = v_servicio,
						 producto_id      = v_producto
				 WHERE marca_id = v_equipo_marca
					AND referencia_id = v_equipo_referencia
					AND tipo_elemento_id = v_equipo_tipo_elemid
					AND tipo_elemento = v_equipo_tipo_elem
					AND equipo_id = v_equipo_serial;
			EXCEPTION
				WHEN OTHERS THEN
					dbms_output.put_line('<PR_EQUIPO_NUEVO_CUMPLIR> ' || substr(SQLERRM,
																									1,
																									100));
			END;

			-- 2009-07-27  Actualizar la tabla de configuraciones equipos con el identificador correspondiente.
			pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
															  v_equipo_referencia,
															  v_equipo_tipo_elemid,
															  'EQU',
															  v_equipo_serial,
															  90,
															  p_identificador,
															  p_mensaje);
		END IF;
	END IF;

	-- Insertar caracteristicas de puerto y equipo asociado en identificador de puerto.
	-- Se consideran los elementos utilizados en los productos que tienen puertos, asi:
	-- PLANT ( Puerto LantoLan), PTLAN ( Puerto MultiLan ), MTLAN (Puerto MultiLan),
	-- PMULT ( Puerto Multinet ).
	IF p_mensaje IS NULL AND p_tipo_elemento_id IN ('PLANT',
																	'PTLAN',
																	'MTLAN',
																	'PMULT') THEN
		-- Si el tipo de elemento_id de la solicitud corresponde a un puerto
		-- se debe configurar la caracteristica 1100

		-- Insertar la caracteristica de puerto asociado.
		pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
														  v_equipo_referencia,
														  v_equipo_tipo_elemid,
														  v_equipo_tipo_elem,
														  v_equipo_serial,
														  1100,
														  nvl(p_agrupador,
																p_identificador),
														  p_mensaje);

		IF p_tipo_elemento_id IN ('PLANT',
										  'PTLAN',
										  'MTLAN') THEN
			pkg_identificadores.pr_insertar_config(p_identificador,
																215,
																v_equipo_serial);
			pkg_identificadores.pr_insertar_config(p_identificador,
																2131,
																v_equipo_marca);
			pkg_identificadores.pr_insertar_config(p_identificador,
																2132,
																v_equipo_referencia);
		END IF;
	END IF;

	-- 2006-01-23
	-- Busqueda de caracteristica 2811 Tipo de Equipo en el subpedido, para configurar el equipo.
	IF p_mensaje IS NULL AND NOT b_multi_equipos THEN
		-- 2009-04-06 Npalacir
		-- Modificacion funcion v_tipo_equipo := FN_valor_caract_subpedido ( p_pedido, p_subpedido, 2811);
		SELECT nvl(pkg_solicitudes.fn_valor_caracteristica(p_pedido,
																			p_subpedido,
																			p_solicitud,
																			2811),
					  fn_valor_caract_subpedido(p_pedido,
														 p_subpedido,
														 2811))
		  INTO v_tipo_equipo
		  FROM dual;

		IF v_tipo_equipo IS NOT NULL THEN
			-- Insertar la caracteristica Tipo Equipo.
			pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
															  v_equipo_referencia,
															  v_equipo_tipo_elemid,
															  v_equipo_tipo_elem,
															  v_equipo_serial,
															  2811,
															  v_tipo_equipo,
															  p_mensaje);
		END IF;
	END IF;

	-- 2006-03-14
	-- Insertar la caracteristica de fecha de instalacion
	IF p_mensaje IS NULL AND NOT b_multi_equipos THEN
		pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
														  v_equipo_referencia,
														  v_equipo_tipo_elemid,
														  v_equipo_tipo_elem,
														  v_equipo_serial,
														  1098,
														  to_char(SYSDATE,
																	 'YYYY-MM-DD'),
														  p_mensaje);
	END IF;

	-- 2006-07-15
	-- Configuracion de caracteristica 2612 Total extensiones instaladas con base
	-- en las extensiones ejecutadas (2613).
	IF p_mensaje IS NULL AND NOT b_multi_equipos THEN
		v_total_ext_ejecutadas := pkg_solicitudes.fn_valor_caracteristica(p_pedido,
																								p_subpedido,
																								p_solicitud,
																								2613);

		IF v_total_ext_ejecutadas IS NOT NULL THEN
			pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
															  v_equipo_referencia,
															  v_equipo_tipo_elemid,
															  v_equipo_tipo_elem,
															  v_equipo_serial,
															  2612,
															  v_total_ext_ejecutadas,
															  p_mensaje);
		END IF;
	END IF;

	-- 2009-07-21
	-- Requerimiento MTA.  Registro de transaccion Cambio de Equipo.
	-- Se realizan las transacciones de cambio de Equipo y el equipo anterior se deja en estado LABoratorio
	IF p_mensaje IS NULL AND b_cambio_equ_asociado THEN
		-- Insertar registro de log de cambio de Equipo
		-- Invocar procedimiento para hacer un registro del cambio de equipo.
		pr_equipo_insert_log_cambio(vr_equipo_anterior.marca_id,
											 vr_equipo_anterior.referencia_id,
											 vr_equipo_anterior.equipo_id,
											 v_equipo_marca,
											 v_equipo_referencia,
											 v_equipo_serial,
											 v_mensaje_log);
		-- Registrar para el Equipo Anterior la fecha de retiro
		pkg_equipos_trans.pr_upd_fecha_retir(vr_equipo_anterior.marca_id,
														 vr_equipo_anterior.referencia_id,
														 vr_equipo_anterior.tipo_elemento_id,
														 'EQU',
														 vr_equipo_anterior.equipo_id,
														 v_identificador_accesp,
														 SYSDATE,
														 v_mensaje_log);
		-- Registrar el Equipo Nuevo asociado al Identificador
		pkg_equipos_trans.pr_insupd_registro(v_equipo_marca,
														 v_equipo_referencia,
														 v_equipo_tipo_elemid,
														 'EQU',
														 v_equipo_serial,
														 v_identificador_accesp,
														 SYSDATE,
														 NULL,
														 v_mensaje_log);

		IF v_mensaje_log IS NOT NULL THEN
			dbms_output.put_line('<PR_EQUIPO_NUEVO_CUMPLIR>. Transaccion Cambio Equipo');
			dbms_output.put_line('<Error> ' || v_mensaje_log);
		END IF;

		v_estado_equipo_anterior := 'XLI';
		-- Se marca el Equipo Anterior en estado de LABoratorio.
		pkg_equipos_util.pr_equipo_act_estado(vr_equipo_anterior.tipo_elemento_id,
														  'EQU',
														  vr_equipo_anterior.equipo_id,
														  v_estado_equipo_anterior,
														  NULL,
														  NULL,
														  v_mensaje_equipo,
														  p_municipio,
														  p_empresa,
														  vr_equipo_anterior.marca_id,
														  vr_equipo_anterior.referencia_id);

		IF v_mensaje_equipo IS NOT NULL THEN
			dbms_output.put_line('<PR_EQUIPO_NUEVO_CUMPLIR>. Cambio de Estado a Pendiente por liberar');
			dbms_output.put_line('<Error> ' || v_mensaje_equipo);
		END IF;
	END IF;

	-- 2011-08-08  ETachej      Problema Cumplido - Se realiza el llamado al PR_CONF_EQU_MULTISERV_CARACT
    -- 2014-08-01  Dvalera     Se configura un equipo multiservicio si no es un segundo equipo de acceso BA
    IF p_mensaje IS NULL AND v_tipo_elemento_id = 'EQACCP' AND
        ((v_servicio = 'TELRES' AND v_producto = 'TO') OR
        (v_servicio = 'DATOS' AND v_producto = 'INTER') OR
        (v_servicio = 'ENTRE' AND v_producto = 'TELEV')) THEN

        v_sol_segundo_acceso := pkg_solicitudes.fn_valor_caracteristica(p_pedido, 
                                                p_subpedido, p_solicitud, 5595);

        IF nvl(v_sol_segundo_acceso,'N') = 'N' THEN
             pr_conf_equ_multiserv_caract(p_identificador,
                                          p_pedido,
                                          p_subpedido,
                                          p_solicitud,
                                          v_tecnologia_sol,
                                          v_equipo_serial,
                                          v_equipo_tipo_elemid,
                                          vr_equipo_anterior.equipo_id,
                                          vr_equipo_anterior.tipo_elemento_id,
                                          p_mensaje);
                                          
        END IF;
    
    END IF;
    -- 2014-08-01  Dvalera FIN    
	
	-- 2012-05-17 Se verifica si se trata de una migración
	-- de televisión digital, en ese caso, se inserta el agrupador
	-- y la caracteristica 33 en el equipo.
    b_migracion_hfc:=FALSE;
    
    v_valor_1067   :=pkg_solicitudes.fn_valor_trabajo(p_pedido,p_subpedido,p_solicitud,1067);
    IF v_valor_1067='ANALOGO' THEN
       b_migracion_hfc:=TRUE;
    END IF;
    
    IF b_migracion_hfc THEN
    
            pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
    											  v_equipo_referencia,
    											  v_equipo_tipo_elemid,
    											  v_equipo_tipo_elem,
    											  v_equipo_serial,
    											  90,
    											  nvl(p_identificador,
      											  p_agrupador),
     											  p_mensaje);
     		
            IF  p_mensaje IS NULL THEN									  
                pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
        											  v_equipo_referencia,
    	      										  v_equipo_tipo_elemid,
    	       										  v_equipo_tipo_elem,
    		      									  v_equipo_serial,
    			     								  33,
    				    							  v_tecnologia_sol,
     					      						  p_mensaje);
     		END IF;			      						  
    END IF;

	-- 2009-02-16
	-- Parametro para verificar sí en caso de un error de validación, se debe sobre-escribir el mensaje.
	IF p_mensaje IS NOT NULL THEN
		v_parametro             := 88;
		v_sobreescribir_mensaje := nvl(fn_valor_parametros('CUMPL_EQU',
																			v_parametro),
												 'N');

		IF v_sobreescribir_mensaje = 'S' THEN
			dbms_output.put_line('<PR_EQUIPO_NUEVO_CUMPLIR>');
			dbms_output.put_line('<Error> ' || p_mensaje);
			p_mensaje := NULL;
		END IF;
	END IF;
END; -- FIN PR_EQUIPO_NUEVO_CUMPLIR
/

