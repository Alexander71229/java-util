PROCEDURE       PR_ESTUDIO_TECNICO_TELEF_IP
(
   w_pedido       IN      fnx_pedidos.pedido_id%TYPE,
   p_concepto     IN      FNX_SOLICITUDES.concepto_id%TYPE DEFAULT 'PETEC',
   w_resp         IN OUT  fnx_caracteristica_solicitudes.valor%TYPE
) IS

-- Descripcion:             Proc. para realizar el estudio técnico de solicitudes de
--                          Telefonia IP.
-- Organización:            EPM Telecomunicaciones UNE

-- HISTORIA DE MODIFICACIONES
-- Fecha        Persona      Observaciones
-- ----------   ----------- ------------------------------------------------------
-- 2008-01-30   Jrincong    Creación inicial del procedimiento
-- ..........
-- 2009-01-23   Jrincong    Uso de procedimiento PR_PROV_TO_TELEF_IP para provisionar el número de solicitudes de TO
--                          indicados por el tipo de tecnología
-- 2009-01-23   Jrincong    Se quita invocación a procedimiento PR_PROV_TOIP_ACTUALIZAR utilizada para actualizar
--                          las características de servicio TOIP.  El estudio se realiza en el procedimiento
--                          PR_PROV_TO_TELEF_IP
-- 2009-01-23   Jrincong    Determinar el número de solicitudes de TO que se deben provisionar, con base en la tecnología
--                          definida como base para el acceso.
-- 2009-01-23   Jrincong    Enrutamiento de las solicitudes de equipo EQACCP o EQURED.  .
-- 2009-01-23   Jrincong    Actualización de característica 33 Tecnologia en la solicitud
-- 2009-01-30   WMendozaL   Para la tecnologia INALAmbrica, se debe presupuestar una dirección IP para
--                          poder asociarla al Equipo CPE.
-- 2009-02-07   Jrincong    Poblar una tabla PL/SQL con los identificadores presupuestados para
--                          utilizarlos en la asociación con los EQURED.
-- 2009-02-07   Jrincong    Buscar el uso del servicio, para determinar sí las solicitudes de TO
--                          deben generar ORDEN.
-- 2009-02-07   Jrincong    Para el caso de telefonia IP Empresarial, las solicitudes de TO provisionadas, deben
--                          pasar al concepto PORDE.
-- 2009-02-07   Jrincong    Actualización de solicitud de EQACCP a ORDEN - POPTO
-- 2009-02-07   Jrincong    Inserción de característica 4093 para cada una de las solicitudes de TO provisionadas
-- 2009-02-07   Jrincong    Lógica para creación de solicitudes de retiro para cada TO asociada al identificador TOIP
-- 2009-02-07   Jrincong    Lógica para creación de solicitudes de Traslado para cada TO asociada al identificador TOIP.
-- 2009-02-08   Jrincong    Determinar agrupador de servicio utilizado. Dependiendo del uso del servicio
--                          y de la portabilidad de línea, el agrupador del servicio es diferente
-- 2009-02-09   Jrincong    Estado y concepto de las solicitudes de EQURED.  Deben generar ORDEN ( PORDE )
-- 2009-02-09   Jrincong    Condición para evitar que se pongan en POPTO otros equipos EQACCP que pueden pertenecer
--                          a otros accesos.  Cada acceso TOIP solo tiene un equipo EQACCP
-- 2009-02-11   Jrincong    Proc. PR_SOLICITUD_CAMBI_TOIP. Adición de variable v_solic.identificador_id para parametro
-- 2009-02-15   Jrincong    Asignación de variable vt_tabla_solic ( v_ind_tabla_solic) := reg.solicitud_id. Antes se
--                          hacía con v_solic.solicitud_id, causando error en la actualización de característica 4093
-- 2009-03-02   Jrincong    Traslado de servicio.  Para el caso de ciudades con enrutamiento a PORDE, el concepto de las
--                          solicitudes de TO es PSERV ( antes POPTO ).
-- 2009-03-02   Jrincong    Asignación de variable w_incons = TRUE para casos de mensajes de error, cuando no existe el
--                          municipio de servicio o el municipio de salida.
-- 2009-03-02   Jrincong    En la transacción de traslado, sí el municipio de la solicitud es nulo se debe buscar el valor
--                          de la característica, en la configuración del identificador.
-- 2009-03-02   Jrincong    Variable v_lineas_provisionadas. Asignación de valor 0. Generaba error de lógica
-- 2009-03-02   Jrincong    Generación de inconsistencia cuando la tecnología no es válida o cuando el valor
--                          de v_total_lineas_xacceso es nulo
-- 2009-03-02   Jrincong    Inicialización de variables v_ind_tabla_solic y v_tabla_indice antes de iniciar cada ciclo
--                          de provisión de las solicitudes de TO
-- 2009-03-02    Jrincong    Controlar que solo se provisionen el número de equipos de red, de acuerdo con
--                             el número de líneas permitidas para cada acceso de acuerdo con la tecnología.
-- 2009-03-05   Jrincong    Verificación de parámetro ESTTEC_TO / 50 para validar sí se debe buscar una dirección
--                          IP para la tecnología inalámbrica ( MarTen )
-- 2009-03-18   Jrincong    Cambio de condiciòn v_uso_servicio = 'COM' por v_uso_servicio <> 'RES'
-- 2009-04-20   Jrincong    Consideraciòn de uso de servicio RESidencial para cambio a concepto POPTO de solicitud
--                          de EQURED
-- 2009-05-07   Jrincong    Seccion Retiro. Creacion de solicitudes de TO.  Cambio en estado y concepto de insercion
--                          de la solicitud. Se inserta en TECNI - PETEC.  Antes era ORDEN - PINSC
-- 2009-05-07   Jrincong    Seccion Retiro. Creaciòn de solicitudes de TO.  Actualización del estado y concepto de
--                          solicitudes de TO a ORDEN-PORDE.  Se hace después de insertar la solicitud para que se
---                         genere la orden respectiva.
-- 2009-05-07   Jaristz     Procedimiento PR_SOLICITUD_CAMBI_TOIP.  Adiciòn de parametro de liquidacion
-- 2009-05-15   Jrincong    Seccion. Retiro. Modificacion parametro proc. PR_ESTADO_CONCEPTO. Se estaba enviando
--                          v_solic.solicitud_id, el nuevo paràmetro es v_nro_solicitud_crear
-- 2009-05-28   Jrincong    Unificacion Modelo. El agrupador de servicio es el identificador utilizado para el acceso
--                          ( TOIP-9999 ). No se utiliza el numero de TO que se utilizaba para el modelo TOIP Residencial
-- 2009-05-28   Jrincong    Unificacion Modelo TOIP.  Las solicitudes de EQURED del modelo Residencial pasan a ORDEN-PORDE
-- 2009-05-28   Jrincong    Unificacion Modelo TOIP. No se considera Uso para asignación de identificador asociado
-- 2009-05-30   Jrincong    Unificacion Modelo TOIP.  Invocacion procedimiento de linea portada PR_PROV_TOIP_PORTADA
--                          Cambio en parametro de solicitud. ( v_solic_linea_portada X v_solic.solicitud_id )
-- 2009-06-17   Jrincong    Provisiòn de login de TOIP para la solicitud, con base en el identificador de TOIP.
-- 2009-06-17   Jrincong    Seccion. Busqueda Acceso Wimax en Direccion - Uso RESidencial y lògica para
--                          bùsqueda de caracteristicas de AutoLogon en contrato Internet asociado.
-- 2009-06-19   Jrincong    Validaciòn en caso que se haya acabado la numeraciòn de Telefonìa IP.
--                          Se deja en concepto 78. Pendiente por Numeraciòn IP.
-- 2009-06-27   Jrincong    Nuevo cursor c_solic_equaccp para buscar las solicitudes de equipos asociados EQACCP.
-- 2009-06-27   Jrincong    Cursor c_solic_equ_asoc. Adicion de parametro p_tipo_equipo para determinar el tipo de equipos
--                          a enrutar ( ATA o ANTWIM )
-- 2009-06-27   Jrincong    Enrutamiento de solicitudes de ANTENA WIMAX en pedidos de tecnologia INALAmbrica. Uso
--                          de cursor c_solic_equ_asoc con ANTWIM
-- 2009-06-30   Jrincong    Traslado.  AutoLogon Wimax.  Tecnologia Inalambrica. Creaciòn de solicitud de ANTENA
-- 2009-06-30   Jrincong    Adiciòn de condiciòn p_nuevo.  El enrutamiento de solicitudes en PINSC solo se realiza para la transacciòn
--                          de Nuevo TO
-- 2009-06-30   Jrincong    Asignaciòn de variable v_tecnologia_solic y v_tecnologia_provision
-- 2009-07-28   Jrincong    No se utiliza proc. PR_RUTAS_PROV_ET.  Se cambia por el procedimiento PR_RUTAS_MUNIC_PRODUCTO
--                          el cual incluye el paràmetro de uso de servicio, para filtrar màs el enrutamiento.
-- 2009-07-28   Jrincong    No se utiliza el proc. PR_RUTAS_PROV_TECNOLOGIA, dado que el valor de la tecnologìa a utilizar
--                          se devuelve en el parametro v_tecnologia_provision del proc. PR_RUTAS_MUNIC_PRODUCTO
-- 2009-09-24   Etachej     SAC Equipos Thomson - Traslados.
--              Ysolarte    Se realiza inserción de la solicitud de EQACCP para traslado y se valida si es necesaria la solicitud del EQURED.
-- 2009-11-24   Jsierrao    Cursor c_solic_equ_asoc.  Se modifica el procedimiento para el enrutamiento de los pedidos en PETEC
--                          cuando pasan de PERLV a INGRE en los cursores que enrutan los equipos
-- 2009-12-09   Jrincong    Búsqueda de valor de característica 3870 para determinar el proveedor de Servicio y definir el
--                          enrutamiento de las solicitudes y el tipo de tecnologìa
-- 2009-12-09   Jrincong    Para el caso del traslado de servicio, búsqueda del valor de caracterìstica 3870 en la
--                          configuración del identificador.
-- 2009-12-09   Jrincong    Sì el valor de la variable v_proveedor_servicio es nulo, se asigna el valor del campo EMPRESA_ID
--                          de la solicitud como valor por defecto.
-- 2010-01-20   Aacosta     Se consulta el uso del servicio en el identificador de servicio asociado a la caracteristica
--                          130 del TO.
-- 2010-01-20   Aacosta     Provisión de equipos de red asociados a un atraslado de TOIP.
-- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
--                          se quedan en petec.
-- 2010-04-06   Jsierrao    Se modifica la Logica para que no se presupuesten direcciones ip para el producto toip
-- 2010-04-20   ysolarte    Si se trata de un pedido de traslado de paquetes la solicitud de TO debe ingresarse como liquidada.
-- 2010-05-06   Lgonzalg    Uso de procedimiento para generar puentes de configuraciòn / desconfiguracion cuando
--                          se retira un servicio TOIP. El procedimiento verifica condiciones de Multiservicio.
-- 2010-05-06   Lgonzalg    Agregar variable v_rutasprov
-- 2010-05-10   Lgonzalg    Agregar cursor c_rutasprov para consultar la variable CONTROL_PUENTES.
-- 2010-05-10   Lgonzalg    Verificar si se deben generar puentes. Se consulta la variable CONTROL_PUENTES de rutas provisión.
-- 2010-09-21   Wmendoza    En adelante se utilizará el procedimiento PR_VERIFICAR_CICLO_CORRERIA
--                          para realizar las verificaciones y saber si un pedido se coloca en concepto PUMED.
-- 2010-09-23   Wmendoza    Se actualiza la solicitud a estado PUMED y se sale del ciclo.
-- 2010-09-29   Wmendoza    Para insertar la solicitud de EQACCP, no debe ser un pedido en concepto INGRE-PUMED.
-- 2011-03-15   Dojedac     PRY DESAPROVISIONAMIENTO -  Se hacen los ajustes para poder crear la solicutud recuperación de equipos cuando se efectua una operación de retiro.
-- 2011-05-16   ETachej     Integridad Fenix - OPEN - Traslados TOIP - modificación para enviar datos del equipo de RED al procedimiento PR_SOLICITUD_CAMBI_EQURED
-- 2011-06-07   JGallg      Desaprov-Estabiliza: Generar solicitud de recuperación de EQURED para TOIP así comparta infraestructura.
-- 2012-01-27   JGallg      Desaprov-NPE: Quitar lógica que genera solicitud de carta, pues no se necesita y ocasiona problemas de
--                          llave primaria.
-- 2012-04-10   JGallg      MejorasDesaprov: Obtener dato de propiedad del equipo por medio de cursor.
-- 2012-04-10   JGallg      MejorasDesaprov: Función interna para validar si se genera o no la solicitud de recuperación de equipo y se centraliza
--                          validación en un solo punto.
-- 2012-04-10   JGallg      MejorasDesaprov: Procedimiento interno para paso de solicitud a ORDEN - PORDE y llamado después de generar la solicitud.
-- 2012-08-21   Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo tiene infraestructura compartida
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es compartido
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es propiedad del cliente
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que no se debe generar Solicitud Recuperación equipos
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el equipo es Obsoleto
-- 2012-09-20   Sbuitrab    Se realiza modificación para el tema de ciclo correria, puesto que antes de pasar a estado PORDE se debe verificar
--                          que el pedido si cumpla con los requisitos para la correcta facturación
-- 2013-03-07   ETachej     REQ_GeneNuevNumePedi - Ampliar campo pedido
-- 2013-04-22   Gsantos     Se agrea condicion al where del cursor c_solicitudes para que no tenga en cuenta el concepto R3066
-- 2013-10-03   Yhernana    REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
--                          Generar solicitud de recogida de equipos en peticiones de retiro cuando se
--                          comparte infraestructura con otro producto que también se está retirando.
-- 2014-01-14   lhernans    Automatización Cola DSLAM. Lógica para inserción de etiquetas para generar eventos.
-- 2014-01-15   Jrendbr     Se adiciona codigo para que procese la solicitud de adición de equipo DPS en TO
-- 2014-02-24   Jrendbr     Se adiciona logica para que tenga en cuenta los esato de los equipos NIR.
-- 2014-03-26   dgirall     Se ajusta la revision de retiro de equipo compartido para incluir el tipo_elemento ATA entre
--                          los equipos que consultan si comparte infraestructura en FNI_GENERAR_SOLIC_EQUIPO
-- 2014-03-26   dgirall     Para retiros de TOIP, validar si se comparte infraestructura con una BA que tiene un pedido de
--                          retiro pendiente (que no ha sido cumplido)  en cuyo caso no debe pasar a PORDE sino a PINSC
-- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
--                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
-- 2014-04-14   dgirall     Se modifica cursor c_solic_toip_pdte para modificar condicion de los estados validos del cursor
-- 2014-04-21   Jrendbr     Se adiciona logica para que sea enviado el identifcador con el que comparte infraestructura
-- 2014-06-25   Jrincong    Req 36900 GPON Hogares.  Función fni_generar_solic_equipo. Consideración de teconología GPON para verificación
--                          de Infraestructura compartida activa.
-- 2014-06-25   Jrincong    Req 36900 GPON Hogares.  Función fni_generar_solic_equipo. Consideración de tecnología GPON para verificar Equipo Compartido.
-- 2014-06-25   Jrincong    Req 36900 GPON Hogares.  Sección Desaprovisionamiento. Consideración de tecnologia GPON para verificación de
--                          Infraestructura compartida.
-- 2014-09-17   MGATTIS     Automatización Cola DSLAM ITPAM25626_AutoColaDSLA. Modificación de Lógica para inserción de etiquetas 4805 ADICIONAR_DSLAM
--                          en NUEVO y TRASLADO para generar eventos BTS.
-- 2014-10-14   MGATTIS     Automatización Cola DSLAM ITPAM25626_AutoColaDSLA. Actualización Lógica para inserción de etiquetas para generar eventos.
-- 2014-10-22   RVELILLR    Automatización Cola DSLAM ITPAM25626_AutoColaDSLA. Condicional de tecnologia para consulta de etiquetas 4805 para generar eventos.

   TYPE t_TableIdent  IS TABLE OF FNX_IDENTIFICADORES.identificador_id%TYPE INDEX BY BINARY_INTEGER;
   vt_tabla_ident            t_TableIdent;
   v_tabla_indice            BINARY_INTEGER := 0;

   TYPE t_TableSolic  IS TABLE OF FNX_SOLICITUDES.solicitud_id%TYPE INDEX BY BINARY_INTEGER;
   vt_tabla_solic            t_TableSolic;
   v_ind_tabla_solic         BINARY_INTEGER := 0;

   w_aa_toip_nored           VARCHAR2 (1);

   w_identificador_toip       FNX_IDENTIFICADORES.identificador_id%TYPE;
   v_identificador_to_pres    FNX_IDENTIFICADORES.identificador_id%TYPE;
   v_identificador_TOIP_nuevo FNX_IDENTIFICADORES.identificador_id%TYPE;
   v_identificador_TO_exist   FNX_IDENTIFICADORES.identificador_id%TYPE;
   v_agrupador_servicio       FNX_IDENTIFICADORES.identificador_id%TYPE;
   identificador_comparte     FNX_IDENTIFICADORES.identificador_id%TYPE;
   v_nro_solicitud_crear      FNX_SOLICITUDES.solicitud_id%TYPE;
   v_nro_solicitud_to         FNX_SOLICITUDES.solicitud_id%TYPE;
   v_mensaje_creacion         VARCHAR2(150);
   e_no_ejecutar_estudio      EXCEPTION;

   p_retiro                  BOOLEAN;
   p_nuevo                   BOOLEAN;
   p_traslado                BOOLEAN;


   w_mensaje_error           VARCHAR2 (200);
   w_obs_pumed               VARCHAR2 (70);
   p_obs_pumed               VARCHAR2 (70);

   v_concepto_real           FNX_SOLICITUDES.concepto_id%TYPE;

   w_incons                  BOOLEAN;
   w_estado_id               FNX_ESTADOS.estado_id%TYPE;
   p_estado_id               FNX_ESTADOS.estado_id%TYPE;
   w_estado_solicitud        FNX_SOLICITUDES.estado_soli%TYPE;
   w_concepto                fnx_conceptos.concepto_id%TYPE;
   p_concepto_pumed          fnx_conceptos.concepto_id%TYPE;
   p_mensaje                 VARCHAR2(50);
   v_estado_identificador    FNX_IDENTIFICADORES.estado%TYPE;

   b_ident_presupuestado     BOOLEAN;
   v_paquete_pendiente       NUMBER(2) := 0;
   v_red_provisionada        VARCHAR2(15);
   b_genera_solic_equipo     BOOLEAN;

   CURSOR c_solicitudes IS
      SELECT pedido_id, subpedido_id, solicitud_id, servicio_id,
             producto_id, tipo_elemento, tipo_elemento_id,
             identificador_id, identificador_id_nuevo,  estado_id,
             municipio_id, empresa_id, tecnologia_id
      FROM   fnx_solicitudes
      WHERE  pedido_id        = w_pedido
      AND    servicio_id      = 'TELRES'
      AND    producto_id      = 'TO'
      AND    tipo_elemento    = 'CMP'
      AND    tipo_elemento_id = 'TOIP'
      AND    estado_id        = 'TECNI'
      AND    estado_soli      = 'PENDI'
      AND    concepto_id      <> 'R3066' -- 2013-04-22   Gsantos
      ORDER BY   tipo_elemento DESC;

   v_solic                   c_solicitudes%ROWTYPE;

   CURSOR c_subped IS
      SELECT agrupador, identificador_id
      FROM   fnx_subpedidos
      WHERE  pedido_id = w_pedido;

   v_subped                  c_subped%ROWTYPE;

   CURSOR c_trabaj (p_pedido     IN   FNX_SOLICITUDES.pedido_id%TYPE,
                    p_subpedido  IN   FNX_SOLICITUDES.subpedido_id%TYPE,
                    p_solicitud  IN   FNX_SOLICITUDES.solicitud_id%TYPE
                    ) IS
      SELECT caracteristica_id, tipo_trabajo,valor
      FROM   fnx_trabajos_solicitudes
      WHERE  pedido_id    = p_pedido
      AND    subpedido_id = p_subpedido
      AND    solicitud_id = p_solicitud;

   v_trabaj                  c_trabaj%ROWTYPE;

   -- Manejo de parámetros
   v_param                   fnx_parametros%ROWTYPE;
   w_parametro               NUMBER (3);

   CURSOR c_parametros (p_parametro    IN  FNX_PARAMETROS.parametro_id%TYPE) IS
      SELECT *
      FROM   fnx_parametros
      WHERE  modulo_id    = 'PRV_SERV'
      AND    parametro_id = p_parametro;

  w_enrutamiento_exitoso    BOOLEAN;
  w_mensaje_enrutamiento    VARCHAR2 (100);
  v_mensaje_insupd          VARCHAR2(200);
  w_paquete                    FNX_CONFIGURACIONES_IDENTIF.VALOR%TYPE;

  b_generar_ordenes         BOOLEAN;
  v_concepto_paquete        FNX_SOLICITUDES.concepto_id%TYPE;
  v_tipo_paquete            FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
  v_paquete_identif_trasl   FNX_IDENTIFICADORES.identificador_id%TYPE;

  v_identificador_solic     FNX_IDENTIFICADORES.identificador_id%TYPE;
  v_identificador_to_asoc   FNX_IDENTIFICADORES.identificador_id%TYPE;
  v_tecnologia_solic        FNX_SOLICITUDES.tecnologia_id%TYPE;
  v_tecnologia_provision    FNX_SOLICITUDES.tecnologia_id%TYPE;

  v_mostrar_seguimiento      VARCHAR2(5) := 'S';
  b_provision_automatica     BOOLEAN := FALSE;

  b_ingre_pumed              BOOLEAN := FALSE;

  b_enrutamiento_porde       BOOLEAN;
  w_uno                         VARCHAR2(1);
  w_dos                         VARCHAR2(1);
  w_tres                     VARCHAR2(1);

  v_municipio_servicio       FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
  v_munic_salida_int         FNX_MUNICIPIOS.mun_salida_internet%TYPE;
  v_municipio_numeracion     FNX_MUNICIPIOS.municipio_numeracion%TYPE;

  vo_enrutar_et              FNX_RUTAS_PROVISION.control_est_tecnico%TYPE;
  v_concepto_toip            FNX_CONCEPTOS.concepto_id%TYPE;

  v_parametro_est_to         NUMBER(3);
  v_total_lineas_xacceso     NUMBER(3);
  v_lineas_provisionadas     NUMBER(3);
  v_equred_provisionados     NUMBER(3);
  v_prov_dirip_inala         FNX_PARAMETROS.valor%TYPE;
  v_prov_login_toip          FNX_PARAMETROS.valor%TYPE;

  v_uso_servicio             FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
  b_linea_portada            BOOLEAN := FALSE;
  v_contador_eqaccp          NUMBER(2);
  v_contador_antena          NUMBER(2);
  v_solicitud_liquidada      FNX_REPORTES_INSTALACION.liquido%TYPE;

  vo_toip_login               FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
  vo_toip_password            FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
  vo_mensaje_login            VARCHAR2(200);

  v_prov_ver_autologon        FNX_PARAMETROS.valor%TYPE;
  v_pagina_servicio           FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
  vo_identificador_accesp     FNX_IDENTIFICADORES.identificador_id%TYPE;
  vo_mensaje_accesp           VARCHAR2(200);
  vo_mascara_ip               FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
  vo_direccion_ip             FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
  vo_crear_solic_ip           FNX_RUTAS_MUNIC_PRODUCTO.crear_solicitudes%TYPE;
  --2014-01-14 lhernans Se crea variable para almacenar etiqeuta
  v_etiqueta                  fnx_caracteristica_solicitudes.valor%TYPE;
  --2014-10-14   MGATTIS
  v_aplicacion                FNX_PARAM_EVENTOS_BTS.aplicacion%TYPE;
  -- 2014-10-14   MGATTIS Fin declaración de variables

-- Cursor para buscar las solicitudes que se encuentran en estado y concepto TECNI-PETEC, dentro
-- del pedido y que correspondan al servicio, producto y tipo elemento enviados por parámetro.
CURSOR c_solic_pserv
 ( p_pedido           IN   FNX_PEDIDOS.pedido_id%TYPE,
   p_subpedido        IN   FNX_SUBPEDIDOS.subpedido_id%TYPE,
   p_servicio         IN   FNX_SERVICIOS.servicio_id%TYPE,
   p_producto         IN   FNX_PRODUCTOS.producto_id%TYPE,
   p_tipo_elemento_id IN   FNX_TIPOS_ELEMENTOS.tipo_elemento_id%TYPE,
   p_tipo_elemento    IN   FNX_TIPOS_ELEMENTOS.tipo_elemento%TYPE
  )
IS
SELECT a.solicitud_id, b.tipo_solicitud, a.identificador_id
FROM   FNX_SOLICITUDES A,
       FNX_REPORTES_INSTALACION B
WHERE  a.pedido_id    = b.pedido_id
AND    a.subpedido_id = b.subpedido_id
AND    a.solicitud_id = b.solicitud_id
AND    a.pedido_id    = p_pedido
AND    a.subpedido_id = p_subpedido
AND    a.servicio_id  = p_servicio
AND    a.producto_id  = p_producto
AND    a.tipo_elemento_id = p_tipo_elemento_id
AND    a.tipo_elemento = p_tipo_elemento
AND    a.estado_id     = 'ORDEN'
AND    a.concepto_id   = 'PSERV'
ORDER  BY solicitud_id;

-- 2009-11-24 Jsierrao
-- Se incluye el concepto TECNI para cuando el pedido lo modifiquen de PERLV a INGRE

-- Cursor para buscar las solicitudes de equipos asociados ( EQURED ) dentro del pedido
CURSOR c_solic_equ_asoc
 ( pc_pedido          IN   FNX_PEDIDOS.pedido_id%TYPE,
   pc_subpedido       IN   FNX_SUBPEDIDOS.subpedido_id%TYPE,
   p_servicio         IN   FNX_SERVICIOS.servicio_id%TYPE,
   p_producto         IN   FNX_PRODUCTOS.producto_id%TYPE,
   p_tipo_elemento_id IN   FNX_TIPOS_ELEMENTOS.tipo_elemento_id%TYPE,
   p_tipo_equipo      IN   FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE
  )
IS
SELECT a.solicitud_id
FROM   FNX_SOLICITUDES A,
       FNX_CARACTERISTICA_SOLICITUDES B
WHERE  a.pedido_id        = b.pedido_id
AND    a.subpedido_id     = b.subpedido_id
AND    a.solicitud_id     = b.solicitud_id
AND    a.pedido_id        = pc_pedido
AND    a.subpedido_id     = pc_subpedido
AND    a.servicio_id      = p_servicio
AND    a.producto_id      = p_producto
AND    a.tipo_elemento_id = p_tipo_elemento_id
AND    a.tipo_elemento    = 'EQU'
AND    a.estado_id         IN ('RUTAS','TECNI')
AND    b.caracteristica_id = 347
AND    b.valor             = p_tipo_equipo
ORDER  BY solicitud_id;

-- Cursor para buscar las solicitudes de equipos asociados EQACCP
CURSOR c_solic_equaccp
 ( p_pedido           IN   FNX_PEDIDOS.pedido_id%TYPE,
   p_subpedido        IN   FNX_SUBPEDIDOS.subpedido_id%TYPE,
   p_servicio         IN   FNX_SERVICIOS.servicio_id%TYPE,
   p_producto         IN   FNX_PRODUCTOS.producto_id%TYPE,
   p_tipo_elemento_id IN   FNX_TIPOS_ELEMENTOS.tipo_elemento_id%TYPE
  )
IS
SELECT solicitud_id
FROM   FNX_SOLICITUDES
WHERE  pedido_id        = p_pedido
AND    subpedido_id     = p_subpedido
AND    servicio_id      = p_servicio
AND    producto_id      = p_producto
AND    tipo_elemento_id = p_tipo_elemento_id
AND    tipo_elemento    = 'EQU'
AND    estado_id         IN ('RUTAS','TECNI')
ORDER  BY solicitud_id;

-- Cursor para obtener los identificadores de TO asociados al identificador
CURSOR c_ident_to_asociados
 ( p_identificador_toip  IN   FNX_IDENTIFICADORES.identificador_id%TYPE )
IS
SELECT a.identificador_id
FROM   FNX_CONFIGURACIONES_IDENTIF A,
       FNX_IDENTIFICADORES B
WHERE  a.caracteristica_id = 4093
AND    a.valor             = p_identificador_toip
AND    a.identificador_id  = B.identificador_id
AND    b.servicio_id       = 'TELRES'
AND    b.producto_id       = 'TO'
AND    b.tipo_elemento_id  = 'TO'
AND    b.tipo_elemento     = 'CMP';

-- 2009-09-24 SAC Equipos Thomson - Traslados.
CURSOR c_equipo_multiservicio ( p_identificador_toip  IN   FNX_IDENTIFICADORES.identificador_id%TYPE )
IS
SELECT  marca_id,referencia_id, tipo_elemento,tipo_elemento_id, equipo_id
FROM    FNX_CONFIGURACIONES_EQUIPOS
WHERE   caracteristica_id = 4093
  AND   valor             = p_identificador_toip;


CURSOR c_ident_equipo_mult ( pc_equipo  IN   FNX_EQUIPOS.equipo_id%TYPE )
IS
SELECT  identificador_id
FROM    FNX_EQUIPOS
WHERE   equipo_id = pc_equipo
  AND   identificador_id is not null;

--Cursor para determinar si el pedido es de traslado de paquetes
CURSOR c_solicitud_paquete_trasl (p_pedido_paquete IN VARCHAR2)
IS
   SELECT identificador_id
     FROM fnx_solicitudes
    WHERE pedido_id = p_pedido_paquete
      AND servicio_id = 'PQUETE'
      AND producto_id = 'PQUETE'
      AND tipo_elemento_id = 'PQUETE';

 CURSOR c_rutasprov ( p_proveedor           IN FNX_PROVEEDORES_PRODUCTOS.proveedor_id%TYPE
                     ,p_municipio_servicio  IN FNX_PROVEEDORES_PRODUCTOS.municipio_id%TYPE
                     ,p_producto            IN FNX_PROVEEDORES_PRODUCTOS.producto_id%TYPE
                     ,p_tecnologia          IN FNX_RUTAS_PROVISION.tecnologia_id%TYPE
                    ) IS
 SELECT control_puentes, control_ordenes
 FROM   FNX_RUTAS_PROVISION
 WHERE  proveedor_id   = p_proveedor
 AND    municipio_id   = p_municipio_servicio
 AND    producto_id    = p_producto
 AND    tecnologia_id  = p_tecnologia ;

v_rutasprov     c_rutasprov%ROWTYPE;

-- 2011-05-16 - ETachej - Integridad Fenix - OPEN - Traslados TOIP
   -- Cursor para obtener los datos principales del equipo de RED actual asociado
   -- al identificador.
   CURSOR c_equipo_actual
     ( p_tipo_equipo                IN FNX_EQUIPOS.tipo_elemento_id%TYPE,
       p_identificador_asociado     IN FNX_IDENTIFICADORES.identificador_id%TYPE)
   IS
   SELECT equipo_id, marca_id, referencia_id
   FROM   FNX_EQUIPOS
   WHERE  identificador_id = p_identificador_asociado
   AND    tipo_elemento_id = p_tipo_equipo
   AND    estado           = 'OCU';

v_solicitud_eqaccp        FNX_SOLICITUDES.SOLICITUD_ID%TYPE;
v_solicitud_equred        FNX_SOLICITUDES.SOLICITUD_ID%TYPE;
v_cliente                 FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_serial_equipo           FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_marca_equipo            FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_referencia_equipo       FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_terminal_asociado       FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_multiserv_ind           FNX_MARCAS_REFERENCIAS.MULTISERVICIO_ID%TYPE;
v_ident_asociado          FNX_SOLICITUDES.IDENTIFICADOR_ID%TYPE;
v_ident_equipo            FNX_EQUIPOS.EQUIPO_ID%TYPE;

v_proveedor_servicio      FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_iden_to                 FNX_IDENTIFICADORES.identificador_id%TYPE;
v_estado                  FNX_SOLICITUDES.estado_id%TYPE;
v_concepto                FNX_SOLICITUDES.concepto_id%TYPE;
v_estado_soli             FNX_SOLICITUDES.estado_soli%TYPE;
v_tecnologia              FNX_SOLICITUDES.tecnologia_id%TYPE;



-- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
--                          se quedan en petec.
v_tamano_observacion       NUMBER(4);
v_nueva_observacion        VARCHAR2(1000);
v_tamano_nueva_obs         NUMBER(4);

--dojedac. 16/03/2011, se crea la variable para verificar en un retiro si se debe crear o no la solicitud de equipo.
v_tipo_elemento_eq          FNX_IDENTIFICADORES.TIPO_ELEMENTO_ID%TYPE;
v_identificador_comparte    FNX_IDENTIFICADORES.identificador_id%TYPE := NULL;

--Dojedac: Variables de referencia para el proyecto de desaprovisionamiento, RECUPERACIÓN DE EQUIPOS
v_cant_equipos NUMBER;
v_tipo_error VARCHAR2(500);
v_tipo_elemento_solic fnx_solicitudes.tipo_elemento_id%type;
v_equipo_obsoleto           VARCHAR2(1);
v_si_obsoleto               NUMBER(2);
v_no_obsoleto               NUMBER(2);
v_equip_obsol               FNX_SOLICITUDES.OBSERVACION%TYPE;
v_cad_equip_acceso          VARCHAR2(200);  -- JGallg - 2011-07-06 - Desaprov-Estabiliza
v_identifica_acceso         FNX_IDENTIFICADORES.identificador_id%TYPE;

--Sbuitrab  20/09/2012  Variables para manejo de verificación ciclo correria
v_estado_pumed          fnx_estados.estado_id%TYPE;
v_concepto_pumed        fnx_conceptos.concepto_id%TYPE;
v_observacion_pumed     VARCHAR2(1000);
w_mensaje_pumed         BOOLEAN;
paquete_out             BOOLEAN;

TYPE regMemory_t IS RECORD (
     EQUIPO_ID            FNX_EQUIPOS.EQUIPO_ID%TYPE,
     MARCA_ID             FNX_EQUIPOS.MARCA_ID%TYPE,
     REFERENCIA_ID        FNX_EQUIPOS.REFERENCIA_ID%TYPE,
     TIPO_ELEMENTO        FNX_EQUIPOS.TIPO_ELEMENTO%TYPE,
     TIPO_ELEMENTO_ID     FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
     TIPO_ELEMENTO_ID_EQU FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
     GENERAR              NUMBER(1)
                            );

TYPE CUR_TYP IS REF CURSOR;
c_Equipos_solic CUR_TYP;
v_cade_Query VARCHAR2(1000);
reg_tipo_equipo regMemory_t;


-- 2013-10-03   Yhernana    Inicio
--                          REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
v_accesp_comparte       fnx_solicitudes.identificador_id%TYPE;
v_cant_retir_proc       NUMBER(2) := 0;
v_marca_id              fnx_equipos.marca_id%TYPE;
v_refer_id              fnx_equipos.referencia_id%TYPE;
v_tipele_id             fnx_equipos.tipo_elemento_id%TYPE;
v_tipo_ele              fnx_equipos.tipo_elemento%TYPE;
v_insip_comparte        fnx_solicitudes.identificador_id%TYPE;
b_comparte_dos          BOOLEAN:= FALSE;
b_comparte_accesp       BOOLEAN:= FALSE;
v_equipo_retiro         FNX_EQUIPOS.EQUIPO_ID%TYPE;
b_aplica_retir          BOOLEAN:= FALSE;
b_no_comparte_infra     BOOLEAN:= FALSE;

CURSOR c_datos_identif2 (pc_identificador_id fnx_identificadores.identificador_id%TYPE) IS
    SELECT cliente_id, fn_valor_caract_identif(identificador_id, 38) pagina_servicio
    FROM fnx_identificadores
    WHERE identificador_id = pc_identificador_id;
rg_datos_identif2 c_datos_identif2%ROWTYPE;

CURSOR c_equipo_comparte2 (
                    pc_identificador_id fnx_identificadores.identificador_id%TYPE,
                    pc_cliente_id       fnx_identificadores.cliente_id%TYPE,
                    pc_pagina_servicio  fnx_configuraciones_identif.valor%TYPE
                ) IS
SELECT a.identificador_id, b.producto_id
FROM fnx_configuraciones_identif a, fnx_identificadores b
WHERE a.identificador_id  = b.identificador_id
  AND a.identificador_id <> pc_identificador_id
  AND a.caracteristica_id = 38
  AND a.valor             = pc_pagina_servicio
  AND b.producto_id      IN ('TELEV', 'TO', 'INTER')
  AND b.tipo_elemento_id IN ('INSIP', 'TOIP', 'ACCESP')
  AND b.cliente_id        = pc_cliente_id
  AND fn_valor_caract_identif(a.identificador_id, DECODE((SELECT x.producto_id
                                                          FROM fnx_identificadores x
                                                          WHERE x.identificador_id = pc_identificador_id),
                              'INTER', 2093,
                              'TELEV', 2087,
                              'TO', 4093, 2093)) = pc_identificador_id;

-- 2013-10-03   Yhernana    Fin

  -- Cursor para buscar las solicitudes de equipso dps y actualizar los estados
CURSOR c_solic_dps
 ( p_pedido           IN   FNX_PEDIDOS.pedido_id%TYPE,
   p_subpedido        IN   FNX_SUBPEDIDOS.subpedido_id%TYPE
  )
IS
SELECT pedido_id, subpedido_id, solicitud_id, tipo_elemento_id
FROM   FNX_SOLICITUDES
WHERE  pedido_id    = p_pedido
AND    subpedido_id = p_subpedido ;

v_solic_dps     c_solic_dps%ROWTYPE;

  -- 2014-01-15 Jrendbr Variables para la creacion del pediodo de adicin equipo DPS.
   p_equipo_dps                  BOOLEAN;
   p_mensaje_proc        VARCHAR2(100);
   v_contador_caract         NUMBER(3) := 0;
   indice_lcar               NUMBER(2) := 0;
   v_caracteristicas         FENIX.PKG_SOLICITUD_CREACION.tab_rec_caract;
   v_trabajos                FENIX.PKG_SOLICITUD_CREACION.tab_rec_trabaj;
   v_subpedido               FNX_SUBPEDIDOS.subpedido_id%TYPE;
   v_solicit               FNX_SOLICITUDES.solicitud_id%TYPE;
   v_esta_solicitud        VARCHAR2(5);
   v_concept_solicitud      VARCHAR2(5);
   v_tecnolog              VARCHAR2(5);
   v_tip_solicitud          VARCHAR2(5);
   v_servicio                VARCHAR2(6);
   v_product                VARCHAR2(6);
   v_tipo_elemento_id        VARCHAR2(6);
   v_tipo_elemento           VARCHAR2(3);
   v_ejecutivo               VARCHAR2(15);
   v_portafolio              FNX_CONFIGURACIONES_IDENTIF.VALOR%TYPE:=NULL;
--2014-02-24    Jrendbr Creacion de variables equipos NIR
v_equipo_nir           VARCHAR2(1);
v_si_nir               NUMBER(2);
v_tipele_id_sol        fnx_solicitudes.tipo_elemento_id%TYPE;
---------------------------------------------------------------------------------------------------------


    v_ident_comparte    fnx_identificadores.identificador_id%type;  -- 2014-03-26 dgirall nueva variable
    v_pedido_acp_pdte   fnx_pedidos.pedido_id%type;                 -- 2014-03-26 dgirall nueva variable

    -- 2014-03-26 dgirall nuevo cursor para buscar solicitudes que relacionen el identificador con que se comparte
    --infraestructura que no estén cumplidas, facturadas o anuladas, y cuyo reporte de instalación sea RETIR.
    -- 2014-04-14 dgirall se modifica condicion de estados validos del cursor
    CURSOR c_solic_acp_pdte (p_ident_acp  fnx_identificadores.identificador_id%type) IS
        SELECT a.pedido_id
        FROM  fnx_solicitudes a, fnx_reportes_instalacion b
        WHERE a.pedido_id        = b.pedido_id
        AND   a.subpedido_id     = b.subpedido_id
        AND   a.solicitud_id     = b.solicitud_id
        AND   a.identificador_id = p_ident_acp
        AND   b.tipo_solicitud   = 'RETIR'
        --AND   a.estado_id      NOT IN ('ANULA','CUMPL','FACTU')
        AND   a.estado_id        = 'ORDEN'
    ;


    -- JGallg - 2012-04-10 - MejorasDesaprov: Función interna para validar si se genera o no la solicitud de recuperación de equipo.
    FUNCTION fni_generar_solic_equipo (
        p_identificador_id   fnx_solicitudes.identificador_id%TYPE,
        p_tecnologia         fnx_solicitudes.tecnologia_id%TYPE,
        prg_equipo           regMemory_t,
        p_pedido_id          fnx_pedidos.pedido_id%TYPE
    ) RETURN BOOLEAN IS
        b_genera           BOOLEAN;
        v_identif_comparte fnx_solicitudes.identificador_id%TYPE;
        v_equipo_comodato  fnx_configuraciones_equipos.valor%TYPE;
        v_cantidad         NUMBER;

        CURSOR c_caract_equipo (
            prg_datos_equipo     regMemory_t,
            pc_caracteristica_id fnx_tipos_caracteristicas.caracteristica_id%type
        ) IS
            SELECT valor
            FROM fnx_configuraciones_equipos
            WHERE marca_id          = prg_datos_equipo.marca_id
              AND referencia_id     = prg_datos_equipo.referencia_id
              AND tipo_elemento_id  = prg_datos_equipo.tipo_elemento_id
              AND tipo_elemento     = prg_datos_equipo.tipo_elemento
              AND equipo_id         = prg_datos_equipo.equipo_id
              AND caracteristica_id = pc_caracteristica_id;

        -- 2013-10-03   Yhernana    Inicio
        --                          REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
        cursor c_datos_equipo(p_marca           IN  fnx_equipos.MARCA_ID%TYPE,
                              p_referencia      IN  FNX_EQUIPOS.REFERENCIA_ID%TYPE,
                              p_tipo_elemento   IN  FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
                              p_equipo          IN  FNX_EQUIPOS.EQUIPO_ID%TYPE
        ) is
            select equipo_id, identificador_id, tipo_elemento_id
            from fnx_equipos
            where marca_id         = p_marca
              and referencia_id    = p_referencia
              and tipo_elemento_id = p_tipo_elemento
              and tipo_elemento    = 'EQU'
              and equipo_id        = p_equipo;
        rg_datos_equipo c_datos_equipo%rowtype;

        cursor c_datos_identif (pc_identificador_id fnx_identificadores.identificador_id%type
        ) is
            select cliente_id, fn_valor_caract_identif(identificador_id, 38) pagina_servicio
            from fnx_identificadores
            where identificador_id = pc_identificador_id;
        rg_datos_identif c_datos_identif%rowtype;

         cursor c_equipo_comparte (
            pc_identificador_id fnx_identificadores.identificador_id%type,
            pc_cliente_id       fnx_identificadores.cliente_id%type,
            pc_pagina_servicio  fnx_configuraciones_identif.valor%type
        ) is
            select a.identificador_id
            from fnx_configuraciones_identif a, fnx_identificadores b
            where a.identificador_id  = b.identificador_id
              and a.identificador_id <> pc_identificador_id
              and a.caracteristica_id = 38
              and a.valor             = pc_pagina_servicio
              and b.producto_id      in ('TELEV', 'TO', 'INTER')
              and b.tipo_elemento_id in ('INSIP', 'TOIP', 'ACCESP')
              and b.cliente_id        = pc_cliente_id
              and fn_valor_caract_identif(a.identificador_id, decode((select x.producto_id
                                                                      from fnx_identificadores x
                                                                      where x.identificador_id = pc_identificador_id),
                                          'INTER', 2093,
                                          'TELEV', 2087,
                                          'TO', 4093, 2093)) = pc_identificador_id;


        v_cant_retir_proce       NUMBER(2) := 0;
        -- 2013-10-03   Yhernana    Fin



    BEGIN
        -- Validar infraestructura compartida
        -- 2014-03-26 dgirall
        --  Se incluye el ATA entre los tipo_elemento_id a validar, de forma que para este tambien se consulte si
        --  comparte o no infraestructura y dependiendo de eso se determine si el equipo debe ser retirado o no.
        IF prg_equipo.tipo_elemento_id IN ('CABLEM', 'CPE', 'ATA') THEN
            IF p_tecnologia = 'INALA' THEN
                b_genera := TRUE;
            ELSIF p_tecnologia = 'FIBRA' THEN
                b_genera := TRUE;
            ELSIF p_tecnologia = 'HFC' THEN
                v_identif_comparte := fn_comparte_infra_activa (p_identificador_id, p_tecnologia, 'INTER', 'ACCESP', NULL);
                IF v_identif_comparte IS NOT NULL THEN
                    b_genera := FALSE;

                    -- 2012-08-21   Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo tiene infraestructura compartida
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-Infra Comp;' || observacion,1,200)
                        WHERE pedido_id     = p_pedido_id;
                    EXCEPTION
                        WHEN OTHERS THEN
                            NULL;
                    END;
                ELSE
                  b_genera := TRUE;
                END IF;

            -- 2014-06-25. Req 36900 GPON Hogares.  Función fni_generar_solic_equipo.
            -- Consideración de teconología GPON para verificación  de Infraestructura compartida activa.

            ELSIF p_tecnologia IN ('REDCO', 'GPON') THEN
                v_identif_comparte := fn_comparte_infra_activa (p_identificador_id, p_tecnologia, 'TELEV', 'INSIP', NULL);
                IF v_identif_comparte IS NULL THEN
                    v_identif_comparte := fn_comparte_infra_activa (p_identificador_id, p_tecnologia, 'INTER', 'ACCESP', NULL);
                END IF;

                -- 2013-10-03   Yhernana    Inicio
                --                          REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
                -- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
                --                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
                IF v_identif_comparte IS NOT NULL THEN
                    IF FN_IDENTIFICADOR_PRODUCTO(v_identif_comparte) = 'TELEV' THEN

                        SELECT COUNT (trso.pedido_id)
                          INTO v_cant_retir_proce
                          FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                         WHERE trso.pedido_id = soli.pedido_id
                           AND trso.subpedido_id = soli.subpedido_id
                           AND trso.solicitud_id = soli.solicitud_id
                           AND trso.caracteristica_id = 1
                           AND trso.valor = v_identif_comparte
                           AND trso.tipo_trabajo = 'RETIR'
                           AND soli.estado_id = 'ORDEN';

                        IF v_cant_retir_proce = 1 THEN
                            b_genera := TRUE;
                        ELSIF v_cant_retir_proce = 0 THEN
                            -- 2013-10-03   Yhernana    Fin

                    b_genera := FALSE;

                    -- 2012-08-21   Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo tiene infraestructura compartida
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-Infra Comp;' || observacion,1,200)
                        WHERE pedido_id     = p_pedido_id;
                    EXCEPTION
                        WHEN OTHERS THEN
                            NULL;
                    END;
                        END IF;
                    ELSE
                        b_genera := FALSE;
                    END IF;
                ELSE
                  b_genera := TRUE;
                END IF;
            END IF;

            -- SECCION.  Validación de Equipo Compartido
            IF b_genera THEN

                -- 2014-06-25. Req 36900 GPON Hogares.  Función fni_generar_solic_equipo.
                -- Consideración de tecnología GPON para verificar Equipo Compartido.

                -- Validar equipo compartido
                IF p_tecnologia IN ('REDCO', 'HFC', 'GPON') THEN
                    v_cantidad := fn_comparte_equipo (prg_equipo.marca_id, prg_equipo.referencia_id, prg_equipo.tipo_elemento_id, prg_equipo.equipo_id);

                    IF v_cantidad = 0 THEN
                        b_genera := TRUE;
                    ELSE

                        -- 2013-10-03   Yhernana    Inicio
                        --                          REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
                        open c_datos_equipo(prg_equipo.marca_id, prg_equipo.referencia_id, prg_equipo.tipo_elemento_id, prg_equipo.equipo_id);
                        fetch c_datos_equipo into rg_datos_equipo;
                        close c_datos_equipo;

                        open c_datos_identif (rg_datos_equipo.identificador_id);
                        fetch c_datos_identif into rg_datos_identif;
                        close c_datos_identif;
                        -- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
                        --                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
                        FOR rg_equipo_comparte IN c_equipo_comparte (rg_datos_equipo.identificador_id, rg_datos_identif.cliente_id, rg_datos_identif.pagina_servicio) LOOP

                            SELECT COUNT (trso.pedido_id)
                              INTO v_cant_retir_proce
                              FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                             WHERE trso.pedido_id = soli.pedido_id
                               AND trso.subpedido_id = soli.subpedido_id
                               AND trso.solicitud_id = soli.solicitud_id
                               AND trso.caracteristica_id = 1
                               AND trso.valor = rg_equipo_comparte.identificador_id
                               AND trso.tipo_trabajo = 'RETIR'
                               AND soli.estado_id = 'ORDEN';

                            IF v_cant_retir_proce = 0 THEN
                        b_genera := FALSE;
                            END IF;

                            IF NOT b_genera THEN
                                EXIT;
                            END IF;

                        END LOOP;

                        IF NOT b_genera THEN
                            -- 2013-10-03   Yhernana    Fin

                        ---- 2012-08-21 Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es compartido
                        BEGIN
                            UPDATE FNX_PEDIDOS
                            SET observacion =  SUBSTR ('Desap-Equi Comp:' || prg_equipo.equipo_id ||';'|| observacion,1,200)
                            WHERE pedido_id     = p_pedido_id;
                        EXCEPTION
                            WHEN OTHERS THEN
                                NULL;
                        END;
                    END IF;
                    END IF;
                ELSE
                    b_genera := TRUE;
                END IF;
            END IF;
        ELSE
            b_genera := TRUE;
        END IF;

        IF b_genera THEN
            -- Validar propiedad del equipo
            OPEN c_caract_equipo (prg_equipo, 1093);
            FETCH c_caract_equipo INTO v_equipo_comodato;
            CLOSE c_caract_equipo;
            v_equipo_comodato := NVL(v_equipo_comodato, 'CD');

            IF v_equipo_comodato IN ('CD', 'AL') THEN
                b_genera := TRUE;
            ELSE
                b_genera := FALSE;

                ---- 2012-08-21 Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es propiedad del cliente
                BEGIN
                    UPDATE FNX_PEDIDOS
                    SET observacion =  SUBSTR ('Desap-Equi Client;' || observacion,1,200)
                    WHERE pedido_id     = p_pedido_id;
                EXCEPTION
                    WHEN OTHERS THEN
                        NULL;
                END;
            END IF;
        END IF;

        RETURN(b_genera);
    END;

    -- JGallg - 2012-04-10 - MejorasDesaprov: Procedimiento interno para paso de solicitud a ORDEN - PORDE.
    PROCEDURE pri_paso_orden_porde (
        p_pedido_id        fnx_solicitudes.pedido_id%type,
        p_subpedido_id     fnx_solicitudes.subpedido_id%type,
        p_solicitud_id     fnx_solicitudes.solicitud_id%type,
        p_concepto         fnx_estados_conceptos.concepto_id%type,  -- 2014-03-26 dgirall nuevo parametro
        p_observ_obsol     fnx_solicitudes.observacion%type,
        p_tecnologia       fnx_solicitudes.tecnologia_id%type,
        p_mensaje      out varchar2
    ) IS

    BEGIN
        -- 2014-03-26 dgirall  Se modifica actualizacion del concepto_id directamente a PORDE, para
        --  actualizar con el valor del parametro p_concepto
        UPDATE FNX_SOLICITUDES
        SET estado_id     = 'ORDEN',
            --concepto_id   = 'PORDE',
            concepto_id   = p_concepto,
            estado_soli   = 'DEFIN',
            tecnologia_id = p_tecnologia,
            observacion = SUBSTR ( p_observ_obsol || observacion,1,2000)
        WHERE pedido_id     = p_pedido_id
          AND subpedido_id  = p_subpedido_id
          AND solicitud_id  = p_solicitud_id;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := 'PR_ESTUDIO_TECNICO_TELEF_IP:  No puso solicitud en ORDEN-PORDE.  mensaje: '||sqlerrm||'  ('||to_char(sqlcode)||').';
    END;
BEGIN

-- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
--                          se quedan en petec.
w_parametro             := 1;
v_tamano_observacion    := NVL(FN_valor_parametros ( 'ESTUD_TEC', w_parametro ), 1000);

w_incons                  := FALSE;
w_mensaje_error           := 'N';

-- Parámetro para ejecutar el estudio automático a traves de red HFC
w_parametro := 23;
OPEN  c_parametros (w_parametro );
FETCH c_parametros INTO v_param;
CLOSE c_parametros;

BEGIN
  w_aa_toip_nored := NVL (v_param.valor, 'N');
EXCEPTION
  WHEN OTHERS THEN
     w_aa_toip_nored := 'N';
END;

-- Parámetro para verificar sí se deben mostrar mensajes de seguimiento
w_parametro := 99;
v_mostrar_seguimiento  := NVL(FN_valor_parametros ( 'PRV_SERV', w_parametro ), 'N');

-- Parámetro para establecer el concepto en que debe quedar la solicitud de TOIP
w_parametro        := 60;
v_concepto_toip    := NVL(FN_valor_parametros ( 'PRV_SERV', w_parametro ), 'PORDE');

DBMS_OUTPUT.PUT_LINE ( 'Concepto TOIP = ' || v_concepto_toip );

IF NVL(w_aa_toip_nored, 'N') = 'N' THEN
   RAISE e_no_ejecutar_estudio;
END IF;

OPEN  c_solicitudes;
FETCH c_solicitudes INTO v_solic;

  LOOP
     EXIT WHEN c_solicitudes%NOTFOUND OR w_incons;

       p_retiro                 := FALSE;
       p_nuevo                  := FALSE;
       p_traslado               := FALSE;
       b_ingre_pumed            := FALSE;


       -- Identificacion de los trabajos solicitados
       OPEN c_trabaj ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id);

       LOOP
          FETCH c_trabaj INTO v_trabaj;
          EXIT WHEN c_trabaj%NOTFOUND;

          IF  v_trabaj.caracteristica_id = 1 AND
              v_trabaj.tipo_trabajo = 'RETIR' THEN
              p_retiro := TRUE;
          ELSIF v_trabaj.caracteristica_id = 1 AND
              v_trabaj.tipo_trabajo = 'NUEVO' THEN
              p_nuevo := TRUE;
          ELSIF v_trabaj.caracteristica_id IN (34, 35, 38) AND
              v_trabaj.tipo_trabajo = 'CAMBI'
          THEN
              p_traslado := TRUE;
          ELSIF v_trabaj.caracteristica_id = 5083 AND v_trabaj.tipo_trabajo = 'CAMBI'
          THEN
              p_equipo_dps := TRUE;
          END IF;
       END LOOP;
       CLOSE c_trabaj;
        -- Seccion comercial INGRE-PUMED
       IF p_nuevo OR p_traslado THEN

           -- 2010-09-21 En adelante se utilizará el procedimiento PR_VERIFICAR_CICLO_CORRERIA
           -- para realizar las verificaciones y saber si un pedido se coloca en concepto PUMED.
           IF p_nuevo THEN
                v_identificador_solic   := v_solic.identificador_id_nuevo;
           ELSIF p_traslado THEN
                v_identificador_solic := pkg_identificadores.fn_valor_configuracion(v_solic.identificador_id,130);
           END IF;


               PR_VERIFICAR_CICLO_CORRERIA(v_solic.pedido_id,
                                           v_solic.subpedido_id,
                                           v_solic.solicitud_id,
                                           v_identificador_solic,
                                           p_estado_id,
                                           p_concepto_pumed,
                                           p_obs_pumed);

           IF NVL(p_obs_pumed,'N')<>'N' THEN
                  w_estado_id     := p_estado_id;
                  w_concepto      := p_concepto_pumed;
            --2010-09-23 Se actualiza la solicitud a estado PUMED y se sale del ciclo.
                   BEGIN
                      UPDATE fnx_solicitudes
                      SET   estado_soli   = 'PENDI',
                            estado_id     = w_estado_id,
                            concepto_id   = w_concepto,
                            observacion   =  '<ET-TOIP>-> [' || w_concepto || '] #>'
                      WHERE pedido_id     = v_solic.pedido_id
                        AND subpedido_id  = v_solic.subpedido_id;
                 EXCEPTION
                        WHEN OTHERS THEN
                           DBMS_OUTPUT.put_line
                                          (   '<%ET-TO> Error actualizando solicitud_id PUMED '
                                          || SUBSTR (SQLERRM, 1, 80)
                                             );
                         NULL;
                   END;

                     b_ingre_pumed:=TRUE;
                   EXIT;

               END IF;
            END IF;

      -- 2008-02-13
      -- SECCION.  Verificar enrutamiento para la ciudad
      -- Determinar enrutamiento de Estudio Técnico del producto con base en el Municipio
      -- de servicio de la solicitud.

       IF ( p_nuevo OR p_traslado )  THEN

           v_municipio_servicio := PKG_SOLICITUDES.fn_valor_caracteristica
                                  (w_pedido, v_solic.subpedido_id, v_solic.solicitud_id, 34 );


           -- 2009-03-02
           -- En la transacción de traslado, sí el municipio de la solicitud es nulo se debe buscar el valor
           -- de la característica, en la configuración del identificador.
           IF v_municipio_servicio IS NULL AND p_traslado  THEN
              v_municipio_servicio := PKG_IDENTIFICADORES.fn_valor_configuracion ( v_solic.identificador_id, 34);
           END IF;

           -- 2009-12-09
           -- Búsqueda de valor de característica 3870 para determinar el proveedor de Servicio y definir el
           -- enrutamiento de las solicitudes y el tipo de tecnologìa
           v_proveedor_servicio  := FN_VALOR_CARACT_SUBPEDIDO (v_solic.pedido_id, v_solic.subpedido_id, 3870);

           -- 2009-12-09
           -- Para el caso del traslado de servicio, búsqueda del valor de caracterìstica 3870 en la
           -- configuración del identificador.
           IF v_proveedor_servicio IS NULL AND p_traslado  THEN
              v_proveedor_servicio := PKG_IDENTIFICADORES.fn_valor_configuracion ( v_solic.identificador_id, 3870);
           END IF;

           -- 2009-12-09
           -- Sì la variable v_proveedor_servicio es nula, se asigna el valor del campo EMPRESA_ID de la solicitud,
           -- como valor por defecto
           IF v_proveedor_servicio IS NULL THEN
              v_proveedor_servicio := v_solic.empresa_id;
           END IF;

           DBMS_OUTPUT.PUT_LINE('<TRACK BRQ> Proveedor Servicio: '||v_proveedor_servicio);

           IF v_municipio_servicio IS NOT NULL THEN

              -- Buscar el municipio de salida internet asociado al Municipio de servicio.
              v_munic_salida_int := PKG_PROV_INTER_UTIL.fn_munic_salida (v_municipio_servicio);
              IF v_munic_salida_int IS NOT NULL THEN

                -- Busqueda uso del servicio
                v_uso_servicio  := NVL(fn_valor_caract_subpedido (v_solic.pedido_id, v_solic.subpedido_id, 2 ), 'MET');

                -- 2010-01-20   Aacosta  Se consulta el uso del servicio en el identificador de servicio asociado a la caracteristica
                --                       130 del TO.
                IF v_uso_servicio = 'MET' THEN
                   v_iden_to := PKG_IDENTIFICADORES.fn_valor_configuracion(v_solic.identificador_id,130);

                   v_uso_servicio  := NVL(PKG_IDENTIFICADORES.fn_valor_configuracion(v_iden_to,2), 'MET');
                END IF;

                 -- 2009-07-28
                 -- Uso de procedimiento PR_RUTAS_MUNIC_PRODUCTO para buscar la ruta de Estudio Técnico
                 -- asociada a la combinacion: Proveedor Municipio, Producto y Uso de servicio
                PR_RUTAS_MUNIC_PRODUCTO (  v_proveedor_servicio, v_munic_salida_int, 'TO', v_uso_servicio
                                          ,v_tecnologia_provision, vo_enrutar_et, vo_crear_solic_ip
                                          ,w_mensaje_error );

                DBMS_OUTPUT.PUT_LINE('<TRACK BRQ> Uso Servicio:        '||v_uso_servicio);
                DBMS_OUTPUT.PUT_LINE('<TRACK BRQ> Municipio Servicio:  '||v_municipio_servicio);
                DBMS_OUTPUT.PUT_LINE('<TRACK BRQ> Municipio Salida IN: '||v_munic_salida_int);
                DBMS_OUTPUT.PUT_LINE('<TRACK BRQ> Tecnologia:          '||v_tecnologia_provision);
                DBMS_OUTPUT.PUT_LINE('<TRACK BRQ> Enrutar ET:          '||vo_enrutar_et);
                DBMS_OUTPUT.PUT_LINE('<TRACK BRQ> Mensaje PR:          '||w_mensaje_error);


                 IF w_mensaje_error IS NOT NULL THEN
                     w_incons := TRUE;
                 ELSE

                   -- Si para el enrutamiento del municipio y producto, no se hace estudio técnico de red,
                   -- se define como un enrutamiento directo al estado de Ordenes
                   IF vo_enrutar_et = 'N' THEN

                      b_enrutamiento_porde  := TRUE;

                      IF v_tecnologia_provision IS NULL THEN
                         w_mensaje_error   := '<TOIP> Tecnología no definida para: ['||v_munic_salida_int||']';
                         w_incons := TRUE;
                      END IF;

                   ELSE
                      b_enrutamiento_porde  := FALSE;
                      v_tecnologia_provision := 'HFC';
                      DBMS_OUTPUT.PUT_LINE('<TRACK BRQ> Sin enrutamiento a PORDE ');
                   END IF;

                 END IF;

              ELSE
                 w_mensaje_error   := '<TOIP> No se encontró municipio de salida para ['||v_municipio_servicio||']';
                 w_incons          := TRUE;
              END IF;
           ELSE
              w_mensaje_error   := '<TOIP> El municipio de servicio no debe ser nulo. ';
              w_incons          := TRUE;
           END IF;

       END IF; -- FIN SECCION.  Verificar enrutamiento para la ciudad

       -- SECCION.  Nuevo o traslado de servicio
       IF ( p_nuevo OR p_traslado ) AND b_enrutamiento_porde THEN

             -- Actualizacion de la fecha de Estudio técnico
             BEGIN
                UPDATE FNX_REPORTES_INSTALACION
                SET    fecha_est_tecnico = SYSDATE
                WHERE  pedido_id    = v_solic.pedido_id
                AND    subpedido_id = v_solic.subpedido_id
                AND    solicitud_id = v_solic.solicitud_id;
             EXCEPTION
                WHEN OTHERS THEN
                   NULL;
             END;




             IF p_nuevo THEN

                -- 2009-02-07
                -- Buscar el uso del servicio, para determinar diferentes condiciones.
                -- Estado y concepto de solicitud de TO en proc. PR_PROV_TO_TELEF_IP
                -- Generación de ordenes de solicitudes de TO para uso COMercial
                -- Definición del agrupador de servicio apropiado.

                v_uso_servicio  := NVL(fn_valor_caract_subpedido (v_solic.pedido_id, v_solic.subpedido_id, 2 ), 'MET');

                -- 2009-01-23
                -- Determinar el número de solicitudes de TO que se deben provisionar, con base en la tecnología
                -- definida como base para el acceso.
                IF   NVL(v_tecnologia_provision, 'NOT')  = 'REDCO' THEN
                     v_parametro_est_to      := 1;
                     v_total_lineas_xacceso  := FN_valor_parametros ( 'ESTTEC_TO', v_parametro_est_to );
                ELSIF NVL(v_tecnologia_provision, 'NOT') = 'HFC'   THEN
                     v_parametro_est_to      := 5;
                     v_total_lineas_xacceso  := FN_valor_parametros ( 'ESTTEC_TO', v_parametro_est_to );
                ELSIF NVL(v_tecnologia_provision, 'NOT') = 'INALA' THEN
                     v_parametro_est_to      := 10;
                     v_total_lineas_xacceso  := FN_valor_parametros ( 'ESTTEC_TO', v_parametro_est_to );
                ELSE
                    w_mensaje_error   := '<TOIP> Tecnologia no definida para TOIP';
                    w_incons   := TRUE;
                END IF;

                v_tecnologia_solic  := v_tecnologia_provision;

                -- 2009-03-02
                -- Generación de inconsistencia cuando la tecnología no es válida o cuando el valor
                -- de v_total_lineas_xacceso es nulo
                IF v_total_lineas_xacceso IS NULL AND NOT w_incons THEN
                   w_mensaje_error   := '<TOIP> Revisar configuración ESTTEC_TO. '||v_tecnologia_provision;
                   w_incons   := TRUE;
                END IF;

                IF w_incons THEN
                   EXIT;
                END IF;

                -- Búsqueda de identificador de TOIP libre para presupuestarlo
                IF NOT w_incons THEN
                     --2013-03-07 ETachej - REQ_GeneNuevNumePedi - Ampliar campo pedido
                     w_uno  := SUBSTR(w_pedido, length(w_pedido), 1);
                     w_dos  := SUBSTR(w_pedido, length(w_pedido)-1, 1);
                     w_tres := SUBSTR(w_pedido, length(w_pedido)-2, 1);

                    -- Buscar un identificador de Acceso del tipo TOIP, al cual
                    -- asociar la red infraestructura.
                    PR_TRAER_ACCESO_RND (w_uno, w_dos, w_tres,
                                         'TELRES', 'TO', 'CMP', 'TOIP',
                                         w_identificador_toip, w_mensaje_error );

                    IF w_identificador_toip IS NULL THEN
                       w_estado_id    := 'TECNI';
                       w_concepto     := '21';
                       w_estado_solicitud := 'PENDI';
                       w_incons       := TRUE;
                       EXIT;
                    END IF;

                ELSE
                   DBMS_OUTPUT.put_line('<TOIP> Error buscando identificador TO para: '||v_municipio_servicio);
                   DBMS_OUTPUT.put_line(w_mensaje_error);
                END IF;

                -- 2009-01-30
                -- Para la tecnologia INALAmbrica, se debe presupuestar una dirección IP para
                -- poder asociarla al Equipo CPE.  Se toman direcciones IP clasificadas bajo
                -- el producto INTER
                -- 2009-03-05
                -- Verificación de parámetro ESTTEC_TO / 50 para validar sí se debe buscar una dirección
                -- IP para la tecnología inalámbrica
                w_parametro := 50;
                v_prov_dirip_inala  := NVL(FN_valor_parametros ( 'ESTTEC_TO', w_parametro ), 'N');

                -- 2009-06-17
                -- SECCION. Busqueda Acceso Wimax en Direccion - Uso RESidencial
                -- Lògica para verificar sì en la direcciòn de servicio del nuevo TOIP, existe un
                -- acceso de Internet Banda Ancha Wimax, el cual ya tenga asignada una direcciòn IP
                -- de AutoLogon Wimax
                IF w_identificador_toip IS NOT NULL AND v_tecnologia_provision   = 'INALA' AND
                   v_prov_dirip_inala = 'S' AND v_uso_servicio = 'RES'
                THEN

                   w_parametro := 55;
                   v_prov_ver_autologon :=  NVL(FN_valor_parametros ( 'ESTTEC_TO', w_parametro ), 'N');
                   v_pagina_servicio    :=  PKG_SOLICITUDES.fn_valor_caracteristica
                                            ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id, 38);

                   IF v_prov_ver_autologon = 'S' AND v_pagina_servicio IS NOT NULL THEN

                      -- Uso de procedimiento para buscar un identificador de ACCESP con tecnologia
                      -- INALA, que se encuentre en la direcciòn, cuya pàgina sea igual al valor
                      -- de la pàgina de la solicitud actual
                      PR_IDENTIF_CARACT_VALOR
                        ( 'DATOS', 'INTER', 'ACCESP', 'CMP'
                         ,'OCU', 'INALA'
                         ,38, v_pagina_servicio
                         ,vo_identificador_accesp
                         ,vo_mensaje_accesp );
                   END IF;

                   -- Si el identificador de ACCESP no es nulo, se debe buscar sì para este acceso
                   -- el contrato de Internet asociado, tiene la caracterìstica de AutoLogon Wimax y
                   -- sì tiene las caracteristicas de IP Lan y Màscara, se actualizan para la solicitud.
                   IF vo_identificador_accesp IS NOT NULL THEN
                      PR_PROV_DATOS_AUTOLOGON
                         ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                           'TELRES', 'TO', 'TOIP', 'CMP',
                           v_solic.municipio_id, v_solic.empresa_id,
                           vo_identificador_accesp,
                           vo_mascara_ip, vo_direccion_ip
                          );

                      IF vo_direccion_ip IS NOT NULL THEN
                          DBMS_OUTPUT.put_line('<PR_ESTUDIO_TECNICO_TELEF_IP> ');
                          DBMS_OUTPUT.put_line('<TRACK> Acceso BA Wimax con AutoLogon: '||vo_identificador_accesp);
                      ELSE
                          DBMS_OUTPUT.put_line('<PR_ESTUDIO_TECNICO_TELEF_IP> ');
                          DBMS_OUTPUT.put_line('<TRACK> Acceso BA Wimax sin AutoLogon: '||vo_identificador_accesp);
                      END IF;
                   ELSE
                      DBMS_OUTPUT.put_line('<PR_ESTUDIO_TECNICO_TELEF_IP> ');
                      DBMS_OUTPUT.put_line('<TRACK> No se encontrò Acceso BA Wimax');
                   END IF;

                END IF;
                -- FIN SECCION. Busqueda Acceso Wimax en Direccion

                --2010-04-06    Jsierrao    Se modifica la Logica para que no se presupuesten direcciones ip para el producto toip

                -- SECCION. Provision de Direccion IP para el caso de tecnologia INALAmbrica ( Wimax )
                -- y para el cual no se haya encontrado un acceso Banda Ancha con direccion IP
                /*IF w_identificador_toip IS NOT NULL AND v_tecnologia_provision   = 'INALA' AND
                   v_prov_dirip_inala = 'S'  AND vo_direccion_ip IS NULL
                THEN

                    PKG_DIRECCIONES_IP.PR_PROV_DIRIP_INTER
                       ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                         w_identificador_toip, 'INALA',
                         w_mensaje_error,
                         'INTER' );

                    IF w_mensaje_error IS NOT NULL THEN

                       dbms_output.put_line('<ET-TELEFIP> '||w_mensaje_error);
                       -- No se pudo encontrar o presupuestar una dirección IP.  No se debe
                       -- continuar con el proceso de Provisión.
                       w_estado_id        := 'TECNI';
                       w_concepto         := '79';
                       w_estado_solicitud := 'PENDI';
                       w_incons := TRUE;
                       EXIT;
                    END IF;

                END IF;*/

                w_parametro := 60;
                v_prov_login_toip  := NVL(FN_valor_parametros ( 'ESTTEC_TO', w_parametro ), 'N');

                -- 2009-06-17
                -- Provisiòn de login de TOIP para la solicitud, con base en el identificador de TOIP.
                IF w_identificador_toip     IS NOT NULL AND v_tecnologia_provision   = 'INALA' AND
                   v_prov_login_toip = 'S' AND NOT w_incons
                THEN
                    PR_PROV_LOGIN_ELEMENTO
                      ( p_pedido    => v_solic.pedido_id
                       ,p_subpedido => v_solic.subpedido_id
                       ,p_solicitud => v_solic.solicitud_id
                       ,p_cadena_random => w_uno||w_dos||w_tres
                       ,p_login_base => LOWER(REPLACE(w_identificador_toip, '-')) ,p_prefijo => 'usr'
                       ,p_prov_cuenta => TRUE, p_reservar_login => TRUE
                       ,p_caract_login   => 110, p_caract_pwd => 133
                       ,p_login_obtenido => vo_toip_login
                       ,p_clave_obtenida => vo_toip_password
                       ,p_mensaje        => vo_mensaje_login
                      );

                END IF;

                -- 2009-01-23
                -- Uso de procedimiento PR_PROV_TO_TELEF_IP para provisionar las solicitudes de
                -- TO, con numeración configurada para cada ciudad
                -- PR_PROV_TO_TELEF_IP

                vt_tabla_ident.DELETE;
                vt_tabla_solic.DELETE;
                v_ind_tabla_solic := 0;
                v_tabla_indice    := 0;
                v_lineas_provisionadas  := 0;

                FOR reg IN c_solic_pserv
                   ( v_solic.pedido_id, v_solic.subpedido_id, 'TELRES', 'TO',
                     'TO', 'CMP' )
                LOOP

                    -- Para el uso de servicio ( v_uso_servicio ) - RESidencial, no se actualizan
                    -- los campos de estado y concepto.
                    PR_PROV_TO_TELEF_IP
                        ( v_solic.pedido_id,  v_solic.subpedido_id, reg.solicitud_id
                         ,v_tecnologia_provision, v_municipio_servicio
                         ,v_solic.municipio_id, v_solic.empresa_id
                         ,reg.tipo_solicitud, 'ORDEN', 'PINSC',
                         v_uso_servicio,  v_identificador_to_pres
                         ,w_mensaje_error );

                    -- Si el tipo de solicitud es CAMBI, el pedido se trata de una línea portada.
                    -- En tal caso, se deben actualizar las características asociadas a este tipo
                    -- solicitud

                    IF v_identificador_to_pres IS NOT NULL THEN

                        IF reg.tipo_solicitud = 'CAMBI' THEN
                           b_linea_portada             := TRUE;
                           v_identificador_TO_exist    := reg.identificador_id;
                        END IF;

                        v_identificador_TOIP_nuevo  := v_identificador_to_pres;

                       -- 2009-02-07
                       -- Poblar una tabla PL/SQL con los identificadores presupuestados para
                       -- utilizarlos en la asociación con los EQURED.
                       v_tabla_indice  := NVL(v_tabla_indice, 0) + 1;
                       vt_tabla_ident ( v_tabla_indice )  := v_identificador_to_pres;
                       DBMS_OUTPUT.put_line('<Identificador en tabla> '||vt_tabla_ident ( v_tabla_indice ));
                    END IF;

                    v_ind_tabla_solic := NVL(v_ind_tabla_solic, 0) + 1;
                    vt_tabla_solic ( v_ind_tabla_solic) := reg.solicitud_id;

                    DBMS_OUTPUT.put_line('<Solicitud Ciclo> '||vt_tabla_solic ( v_ind_tabla_solic ));

                    IF w_mensaje_error IS NOT NULL THEN
                       w_incons         := TRUE;
                       EXIT;
                    ELSE
                        -- Controlar que solo se provisionen el número de líneas permitidas
                        -- para un acceso de acuerdo con la tecnología.
                        v_lineas_provisionadas := v_lineas_provisionadas + 1;
                        IF v_lineas_provisionadas = v_total_lineas_xacceso THEN
                           EXIT;
                        END IF;
                    END IF;

                END LOOP;

                -- 2009-06-19
                -- Validaciòn en caso que se haya acabado la numeraciòn de Telefonìa IP.
                -- Se deja en concepto 78. Pendiente por Numeraciòn IP.
                IF w_mensaje_error IS NOT NULL AND w_incons THEN
                   w_estado_id        := 'TECNI';
                   w_concepto         := '78';
                   w_estado_solicitud := 'PENDI';
                   w_incons           := TRUE;
                   EXIT;
                END IF;

                IF w_identificador_toip IS NOT NULL  THEN

                    b_ident_presupuestado     := TRUE;
                    v_identificador_solic   := w_identificador_toip;

                    UPDATE FNX_CARACTERISTICA_SOLICITUDES
                    SET    valor        = w_identificador_toip
                    WHERE  pedido_id    = v_solic.pedido_id
                    AND    subpedido_id = v_solic.subpedido_id
                    AND    solicitud_id = v_solic.solicitud_id
                    AND    caracteristica_id = 1;

                    -- 2009-01-23
                    -- Actualización de característica 33 Tecnologia en la solicitud de TOIP
                    UPDATE FNX_CARACTERISTICA_SOLICITUDES
                    SET    valor        = v_tecnologia_provision
                    WHERE  pedido_id    = v_solic.pedido_id
                    AND    subpedido_id = v_solic.subpedido_id
                    AND    solicitud_id = v_solic.solicitud_id
                    AND    caracteristica_id = 33;

                ELSE
                    b_ident_presupuestado     := FALSE;
                END IF;
             ELSIF p_traslado THEN
                 b_ident_presupuestado  := TRUE;
                 v_identificador_solic  := v_solic.identificador_id;
                 v_tecnologia_solic     := v_tecnologia_provision;
             END IF;

            IF NOT b_ident_presupuestado THEN
                w_estado_id    := 'TECNI';
                w_concepto     := '21';
                w_estado_solicitud := 'PENDI';
                v_identificador_solic := NULL;
            ELSE
                w_estado_id    := 'ORDEN';
                w_concepto     := v_concepto_toip;
                w_estado_solicitud := 'DEFIN';

                DBMS_OUTPUT.PUT_LINE ( 'Tecnología solicitud = ' || v_tecnologia_solic );

               -- Inserción de registro en tabla de infraestructura en trámite
               IF v_tecnologia_solic = 'INALA' THEN
                     BEGIN
                        INSERT INTO fnx_inf_inalambricas_tramites
                           ( pedido_id, subpedido_id, solicitud_id
                            ,identificador_id, tecnologia_id, subtipo_tecnologia_id)
                        VALUES
                           ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                             v_identificador_solic,
                             'INALA', 'WIMAX' );
                     EXCEPTION
                        WHEN OTHERS THEN
                           NULL;
                     END;
               ELSIF v_tecnologia_solic = 'HFC' THEN
                     BEGIN
                        INSERT INTO fnx_inf_tv_tramites
                           ( pedido_id, subpedido_id, solicitud_id
                            ,identificador_id )
                        VALUES
                           ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                             v_identificador_solic );
                    EXCEPTION
                        WHEN OTHERS THEN
                             NULL;
                             DBMS_OUTPUT.put_line('<%TV-ET> Error insertando en TV Tramites');
                    END;
               END IF;

               IF w_concepto = 'PORDE' THEN
                   PKG_ESTUDIOT_GENERAL.pr_pqute_concepto_porde
                     (  v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
                       ,w_concepto, v_paquete_pendiente );
               END IF;

               IF v_mostrar_seguimiento = 'S' THEN
                 DBMS_OUTPUT.put_line('<TRACK 50> Revisión Concepto Paquete '||w_concepto);
               END IF;

            END IF;

            w_obs_pumed  := '<% TOIP AA '||w_obs_pumed;
            w_enrutamiento_exitoso := TRUE;
            w_mensaje_enrutamiento := NULL;

            IF p_nuevo THEN

                  -- 2009-01-23
                  -- Se quita invocación a procedimiento PR_PROV_TOIP_ACTUALIZAR utilizada para actualizar
                  -- las características de servicio TOIP.  El estudio se realiza en el procedimiento
                  -- PR_PROV_TO_TELEF_IP

                  -- 2009-02-07
                  -- Para el caso de una linea portada, se deben actualizar las características de la linea portada.
                  IF b_linea_portada THEN
                       PR_PROV_TOIP_PORTADA ( v_solic.pedido_id,  v_solic.subpedido_id, v_solic.solicitud_id,
                                              v_identificador_TOIP_nuevo, w_mensaje_error );
                  END IF;

                  --2014-09-17 MGATTIS
                  --Se determina si se inserta o no etiqueta para NUEVO validando que
                  --el Estado-Concepto sea ORDEN-PORDE
                  IF w_estado_id = 'ORDEN' AND w_concepto = 'PORDE' THEN
                      --2014-10-22   RVELILLR Condicional de tecnologia para consulta de etiquetas para generar eventos.
                      IF NVL(v_solic.tecnologia_id, 'NOT') = 'REDCO' THEN
                        --2014-10-14   MGATTIS
                        v_aplicacion := 'DSLAM';

                        v_etiqueta := PKG_ETIQUETAS_EVENTOS_BTS.FN_VALOR_PARAM_EVENTOS_BTS (
                                                    v_aplicacion,
                                                    v_solic.producto_id,
                                                    v_solic.tipo_elemento_id,
                                                    'NUEVO',
                                                    1
                                                    );
                        --2014-10-14   MGATTIS FIN Lógica.
                      ELSE
                        --Se recupera valor de la etiqueta
                        --formado por ETBTS_[MUNICIPIO]_[PRODUCTO]_[TECNOLOGIA] (Sin corchetes)
                        v_etiqueta := fn_dominio_values_meaning (
                                                          'ETBTS'
                                                          || '_'
                                                          || v_munic_salida_int
                                                          || '_'
                                                          || v_solic.producto_id
                                                          || '_'
                                                          || v_solic.tecnologia_id,
                                                          'NUEVO',
                                                          1
                                                                );
                      END IF;
                     --2014-10-22   RVELILLR FIN Lógica.

                      IF NVL (v_etiqueta, 'NA') <> 'NA' THEN

                         --Rutina de inserción de etiquetas
                         pr_insertar_etiquetas_bts (
                                               v_solic.pedido_id,
                                               v_solic.subpedido_id,
                                               v_solic.solicitud_id,
                                               v_solic.servicio_id,
                                               v_solic.producto_id,
                                               v_solic.tipo_elemento,
                                               v_solic.tipo_elemento_id,
                                               4805,
                                               v_solic.municipio_id,
                                               v_solic.empresa_id,
                                               v_etiqueta,
                                               w_mensaje_error
                                               );

                      END IF;
                   END IF;
                  --2014-09-17 MGATTIS FIN Lógica.

                  PKG_ESTUDIOT_GENERAL.pr_act_nueva_solic
                        ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
                         ,v_identificador_solic, v_tecnologia_solic
                         ,w_estado_solicitud, w_estado_id, w_concepto
                         ,w_obs_pumed, v_concepto_real, w_mensaje_error );

                  dbms_output.put_line('Mensaje '||w_mensaje_error);

            ELSIF p_traslado THEN
                   --2014-09-17 MGATTIS
                   --Se determina si se inserta o no etiqueta para TRASLADO validando que
                   --el Estado-Concepto sea ORDEN-PORDE
                   IF w_estado_id = 'ORDEN' AND w_concepto = 'PORDE' THEN
                      --2014-10-22   RVELILLR Condicional de tecnologia para consulta de etiquetas para generar eventos.
                      IF NVL(v_solic.tecnologia_id, 'NOT') = 'REDCO' THEN
                        --2014-10-14   MGATTIS
                        v_aplicacion := 'DSLAM';

                        v_etiqueta := PKG_ETIQUETAS_EVENTOS_BTS.FN_VALOR_PARAM_EVENTOS_BTS (
                                                v_aplicacion,
                                                v_solic.producto_id,
                                                v_solic.tipo_elemento_id,
                                                'CAMBI',
                                                38
                                                );
                        --2014-10-14   MGATTIS FIN Lógica.
                      ELSE
                        --Se recupera valor de la etiqueta
                        --formado por ETBTS_[MUNICIPIO]_[PRODUCTO]_[TECNOLOGIA] (Sin corchetes)
                        v_etiqueta := fn_dominio_values_meaning (
                                                          'ETBTS'
                                                          || '_'
                                                          || v_munic_salida_int
                                                          || '_'
                                                          || v_solic.producto_id
                                                          || '_'
                                                          || v_solic.tecnologia_id,
                                                          'CAMBI',
                                                          38
                                                                );
                      END IF;
                      --2014-10-22   RVELILLR FIN Lógica.

                      IF NVL (v_etiqueta, 'NA') <> 'NA' THEN

                         --Rutina de inserción de etiquetas
                         pr_insertar_etiquetas_bts (
                                               v_solic.pedido_id,
                                               v_solic.subpedido_id,
                                               v_solic.solicitud_id,
                                               v_solic.servicio_id,
                                               v_solic.producto_id,
                                               v_solic.tipo_elemento,
                                               v_solic.tipo_elemento_id,
                                               4805,
                                               v_solic.municipio_id,
                                               v_solic.empresa_id,
                                               v_etiqueta,
                                               w_mensaje_error
                                               );

                      END IF;
                   END IF;
                   --2014-09-17 MGATTIS FIN Lógica.

                   -- En los traslados no se actualiza el identificador Nuevo
                   PKG_ESTUDIOT_GENERAL.pr_actualizar_solic
                     ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
                      ,v_identificador_solic, v_tecnologia_solic
                      ,w_estado_solicitud, w_estado_id, w_concepto
                      ,w_obs_pumed, v_concepto_real, w_mensaje_error );
            END IF;

            -- Si el concepto de la solicitud es PORDE, se habilita la variable
            -- para generacion de ordenes de paquete
            IF v_concepto_real = 'PORDE' AND v_paquete_pendiente > 0
            THEN
               b_generar_ordenes  := TRUE;
               v_concepto_paquete := 'PORDE';
            END IF;

            -- Si el concepto de la solicitud es PORDE, se habilita la variable
            -- para generacion de ordenes de paquete y el concepto se deja en APROB.
            IF v_concepto_real = 'APROB' AND v_paquete_pendiente > 0
            THEN
               b_generar_ordenes  := TRUE;
               v_concepto_paquete := 'APROB';
            END IF;

            -- 2008-02-20
            -- Sí no se pudo asignar ID de acceso, se sale del ciclo para no
            -- enrutar los equipos.
            IF w_concepto = '21' THEN
               EXIT;
            END IF;

            -- 2009-05-28
            -- Unificacion Modelo. El agrupador de servicio es el identificador utilizado para el acceso
            -- ( TOIP-9999 ).  No se utiliza el numero de TO que se utilizaba para el modelo TOIP Residencial
            v_agrupador_servicio  := w_identificador_toip;

             -- Actualización de la característica 90 de la solicitud de TOIP, para mantener
             -- la asociación del agrupador de servicio correspondiente.
            IF p_nuevo  THEN
                BEGIN
                   UPDATE FNX_CARACTERISTICA_SOLICITUDES
                   SET    valor              = v_agrupador_servicio
                   WHERE  pedido_id          = v_solic.pedido_id
                   AND    subpedido_id       = v_solic.subpedido_id
                   AND    solicitud_id       = v_solic.solicitud_id
                   AND    caracteristica_id  = 90;
                EXCEPTION
                    WHEN OTHERS THEN
                         NULL;
                END;
            END IF;

             -- Para TOIP Residencial, actualización de la característica 130 de la solicitud de TOIP,
             -- correspondiente al nuevo identificador de TOIP
            IF p_nuevo  AND v_uso_servicio = 'RES' THEN
                BEGIN
                   UPDATE FNX_CARACTERISTICA_SOLICITUDES
                   SET    valor              = v_identificador_TOIP_nuevo
                   WHERE  pedido_id          = v_solic.pedido_id
                   AND    subpedido_id       = v_solic.subpedido_id
                   AND    solicitud_id       = v_solic.solicitud_id
                   AND    caracteristica_id  = 130;
                EXCEPTION
                    WHEN OTHERS THEN
                         NULL;
                END;
            END IF;

            -- 2009-02-09 / 2009-05-28.  Unificacion de Modelo
            -- Estado y concepto de las solicitudes de EQURED.  Deben generar ORDEN ( PORDE )
            w_estado_id  := 'ORDEN';
            w_concepto   := 'PORDE';

            -- 2009-01-23
            -- Enrutamiento de las solicitudes de equipo EQACCP o EQURED.

            v_tabla_indice          := 1;
            v_equred_provisionados  := 0;

            FOR regequ  IN c_solic_equ_asoc
                ( v_solic.pedido_id, v_solic.subpedido_id, 'TELRES', 'TO', 'EQURED', 'ATA')
            LOOP

                -- 2009-05-28
                -- Unificacion Modelo. No se considera Uso para asignación de identificador asociado
                -- Buscar un identificador en la tabla PL/SQL de identificadores
                IF vt_tabla_ident.EXISTS(v_tabla_indice) THEN
                   v_identificador_to_asoc := vt_tabla_ident ( v_tabla_indice );
                   v_tabla_indice  := v_tabla_indice + 1;
                ELSE
                   v_identificador_to_asoc := w_identificador_toip;
                END IF;

                -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                -- se quedan en petec.
                v_nueva_observacion := '<% TOIP AA ['||w_concepto||'] %> ';
                v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                -- Actualización de estado y concepto de la solicitud de Equipo.
                BEGIN
                   UPDATE FNX_SOLICITUDES
                   SET    estado_soli   = 'DEFIN',
                          estado_id     = w_estado_id,
                          concepto_id   = w_concepto,
                          identificador_id_nuevo = v_identificador_to_asoc,
                          tecnologia_id = v_tecnologia_solic,
                          -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                          -- se quedan en petec.
                          --observacion   = '<% TOIP AA ['||w_concepto||'] %> '
                          --                ||observacion
                          observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                   WHERE  pedido_id        = v_solic.pedido_id
                   AND    subpedido_id     = v_solic.subpedido_id
                   AND    solicitud_id     = regequ.solicitud_id;
                EXCEPTION
                   WHEN OTHERS THEN
                       NULL;
                END;

                -- Actualización de la característica 90 de la solicitud de Equipo, para mantener
                -- la asociación del agrupador de servicio correspondiente.
                BEGIN
                   UPDATE FNX_CARACTERISTICA_SOLICITUDES
                   SET    valor              = v_agrupador_servicio
                   WHERE  pedido_id          = v_solic.pedido_id
                   AND    subpedido_id       = v_solic.subpedido_id
                   AND    solicitud_id       = regequ.solicitud_id
                   AND    caracteristica_id  = 90;
                EXCEPTION
                    WHEN OTHERS THEN
                         NULL;
                END;

                -- 2009-03-02
                -- Controlar que solo se provisionen el número de equipos de red, de acuerdo con
                -- el número de líneas permitidas para cada acceso de acuerdo con la tecnología.
                v_equred_provisionados := v_equred_provisionados + 1;
                IF v_equred_provisionados = v_total_lineas_xacceso THEN
                   EXIT;
                END IF;

            END LOOP;

            -- 2009-02-07
            -- Actualización de solicitud de EQACCP a ORDEN - POPTO y actualización de
            -- caracteristica de agrupador de servicio.

            -- Estado y concepto de las solicitudes de EQACCP.  No generan ORDEN
            w_estado_id  := 'ORDEN';
            w_concepto   := 'POPTO';
            v_contador_eqaccp := 1;

            FOR regequ  IN c_solic_equaccp
                ( v_solic.pedido_id, v_solic.subpedido_id, 'TELRES', 'TO', 'EQACCP')
            LOOP

                -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                -- se quedan en petec.
                v_nueva_observacion := '<% TOIP AA ['||w_concepto||'] %> ';
                v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                -- Actualización de estado y concepto de la solicitud de Equipo.
                BEGIN
                   UPDATE FNX_SOLICITUDES
                   SET    estado_soli   = 'DEFIN',
                          estado_id     = w_estado_id,
                          concepto_id   = w_concepto,
                          identificador_id_nuevo = v_agrupador_servicio,
                          tecnologia_id = v_tecnologia_solic,
                          -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                          -- se quedan en petec.
                          --observacion   = '<% TOIP AA ['||w_concepto||'] %> '
                          --                ||observacion
                          observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                   WHERE  pedido_id        = v_solic.pedido_id
                   AND    subpedido_id     = v_solic.subpedido_id
                   AND    solicitud_id     = regequ.solicitud_id;
                EXCEPTION
                   WHEN OTHERS THEN
                       NULL;
                END;

                -- Actualización de la característica 90 de la solicitud de Equipo, para mantener
                -- la asociación del TOIP correspondiente.
                BEGIN
                   UPDATE FNX_CARACTERISTICA_SOLICITUDES
                   SET    valor              = v_agrupador_servicio
                   WHERE  pedido_id          = v_solic.pedido_id
                   AND    subpedido_id       = v_solic.subpedido_id
                   AND    solicitud_id       = regequ.solicitud_id
                   AND    caracteristica_id  = 90;
                EXCEPTION
                    WHEN OTHERS THEN
                         NULL;
                END;

                -- 2009-02-09
                -- Condición para evitar que se pongan en POPTO otros equipos EQACCP que pueden pertenecer
                -- a otros accesos.  Cada acceso TOIP solo tiene un equipo EQACCP
                v_contador_eqaccp  := v_contador_eqaccp + 1;
                IF  v_contador_eqaccp > 1 THEN
                   EXIT;
                END IF;

            END LOOP;

            -- 2009-06-27
            -- SECCION.  Enrutamiento de solicitudes de ANTENA WIMAX en pedidos con tecnologia de
            --           Provisiòn INALAmbrica.
            IF v_tecnologia_provision = 'INALA' THEN

                w_estado_id  := 'ORDEN';
                w_concepto   := 'POPTO';
                v_contador_antena := 1;

                FOR equant  IN c_solic_equ_asoc
                    ( v_solic.pedido_id, v_solic.subpedido_id, 'TELRES', 'TO', 'EQURED', 'ANTWIM')
                LOOP

                    -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                    -- se quedan en petec.
                    v_nueva_observacion := '<% TOIP AA ['||w_concepto||'] %> ';
                    v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                    -- Actualización de estado y concepto de la solicitud de Equipo.
                    BEGIN
                       UPDATE FNX_SOLICITUDES
                       SET    estado_soli   = 'DEFIN',
                              estado_id     = w_estado_id,
                              concepto_id   = w_concepto,
                              identificador_id_nuevo = v_agrupador_servicio,
                              tecnologia_id = v_tecnologia_solic,
                              -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                              -- se quedan en petec.
                              --observacion   = '<% TOIP AA ['||w_concepto||'] %> '
                              --                ||observacion
                              observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                       WHERE  pedido_id        = v_solic.pedido_id
                       AND    subpedido_id     = v_solic.subpedido_id
                       AND    solicitud_id     = equant.solicitud_id;
                    EXCEPTION
                       WHEN OTHERS THEN
                           NULL;
                    END;

                    -- Actualización de la característica 90 de la solicitud de Equipo, para mantener
                    -- la asociación del TOIP correspondiente.
                    BEGIN
                       UPDATE FNX_CARACTERISTICA_SOLICITUDES
                       SET    valor              = v_agrupador_servicio
                       WHERE  pedido_id          = v_solic.pedido_id
                       AND    subpedido_id       = v_solic.subpedido_id
                       AND    solicitud_id       = equant.solicitud_id
                       AND    caracteristica_id  = 90;
                    EXCEPTION
                        WHEN OTHERS THEN
                             NULL;
                    END;

                    -- Condición para evitar que se pongan en POPTO otros equipos EQURED que pueden pertenecer
                    -- a otros accesos.  Cada acceso TOIP solo tiene un equipo del tipo ANTENA ( ANTWIM )
                    v_contador_antena  := v_contador_antena + 1;
                    IF  v_contador_antena > 1 THEN
                       EXIT;
                    END IF;
                END LOOP;

            END IF;
            -- FIN SECCION.  Enrutamiento de solicitudes de ANTENA WIMAX en pedidos de tecnologia INALAmbrica.

            -- 2009-02-07
            -- Inserción de característica 4093 para cada una de las solicitudes de TO provisionadas
            DBMS_OUTPUT.put_line('<Identificador TOIP> '||w_identificador_toip);

            FOR t_solic IN 1..vt_tabla_solic.COUNT LOOP

              -- Insertar la caracteristica 4093 para cada una de las solicitudes de TO
              -- asociadas
              PKG_PEDIDOS_UTIL.pr_insupd_caracteristica
                   ( v_solic.pedido_id, v_solic.subpedido_id, vt_tabla_solic(t_solic),
                     'TELRES', 'TO', 'CMP', 'TO', 4093
                    ,w_identificador_toip, v_solic.municipio_id, v_solic.empresa_id
                    , v_mensaje_insupd );

              DBMS_OUTPUT.put_line('<Solicitud No>: '||vt_tabla_solic(t_solic));

            END LOOP;

            b_provision_automatica  := TRUE;

       ELSIF p_retiro   THEN

           -- 2009-02-07
           -- Lógica para creación de solicitudes de retiro para cada TO asociada al identificador TOIP
           DBMS_OUTPUT.put_line('<LOGICA DE RETIRO> ');

           v_nro_solicitud_crear   := NULL;
           v_tecnologia_solic  := PKG_IDENTIFICADORES.fn_tecnologia ( v_solic.identificador_id );


           -- 2009-05-07
           -- Seccion Retiro. Creacion de solicitudes de TO.  Cambio en estado y concepto de insercion
           -- de la solicitud. Se inserta en TECNI - PETEC.  Antes era ORDEN - PINSC

           FOR regident IN c_ident_to_asociados ( v_solic.identificador_id ) LOOP

                -- Crear las solicitudes de retiro de los componentes TO asociados al TOIP
                PR_SOLICITUD_RETIR_TOIP ( w_pedido, v_solic.subpedido_id, v_solic.solicitud_id,
                              'TELRES', 'TO', 'TO', 'CMP',
                              regident.identificador_id, v_tecnologia_solic,
                              'TECNI', 'PETEC',
                              v_solic.municipio_id, v_solic.empresa_id,
                              v_nro_solicitud_crear, v_mensaje_creacion );

                -- 2009-05-07
                -- Actualización del estado y concepto de solicitudes de TO a ORDEN-PORDE.  Se hace después de
                -- insertar la solicitud para que se genere la orden respectiva.

                -- 2014-03-26 dgirall  Consulta si el identificador de TOIP comparte infraestructura con un identificador
                --  de ACCESP y si este identificador tiene pedidos de retiro pendientes de cumplir
                v_ident_comparte := FN_COMPARTE_INFRA(V_SOLIC.IDENTIFICADOR_ID, 'HFC', 'DATOS', 'INTER', null, null, null);

                IF NVL(v_ident_comparte,'-') <> '-' THEN
                    OPEN c_solic_acp_pdte(v_ident_comparte);
                    FETCH c_solic_acp_pdte INTO v_pedido_acp_pdte;
                    CLOSE c_solic_acp_pdte;
                END IF;

                -- 2014-03-26 dgirall  Antes de pasar a PORDE las solicitudes, validar si existe un pedido de retiro
                --    pendiente de cumplir, del ACCESP con que comparte infraestructura el TOIP que se esta retirando
                IF NVL(v_pedido_acp_pdte,'-') <> '-' THEN
                    v_concepto_toip   := 'PINSC';
                ELSE
                    v_concepto_toip   := 'PORDE';
                END IF;
                -- 2014-03-26 - Fin

                -- 2014-03-26 dgirall  Se modifica el llamado a PKG_PEDIDOS_UTIL.PR_Estado_Concepto para enviar la
                --  variable v_concepto_toip en lugar de enviar directamente el concepto PORDE
                PKG_PEDIDOS_UTIL.PR_Estado_Concepto
                              ( w_pedido, v_solic.subpedido_id, v_nro_solicitud_crear,
                                'ORDEN', v_concepto_toip, 'DEFIN' );


               IF v_mensaje_creacion IS NOT NULL THEN
                  EXIT;
               ELSE
                   v_nro_solicitud_crear   := v_nro_solicitud_crear + 1;
               END IF;

           END LOOP;

           -- 2010-05-10
           -- Verificar si se deben generar puentes. Se consulta la variable CONTROL_PUENTES de rutas provisión.
           v_municipio_servicio := PKG_IDENTIFICADORES.fn_valor_configuracion ( v_solic.identificador_id, 34);
           IF v_municipio_servicio IS NOT NULL THEN
              -- Buscar el municipio de salida internet asociado al Municipio de servicio.
              v_munic_salida_int := PKG_PROV_INTER_UTIL.fn_munic_salida (v_municipio_servicio);
              IF v_munic_salida_int IS NOT NULL THEN
                    DBMS_OUTPUT.put_line('<TRACK 03> Municipio Salida ->'||v_munic_salida_int);

                    v_rutasprov.control_puentes := NULL;
                    v_rutasprov.control_ordenes := NULL;
                    -- Buscar la ruta de provisión de servicio asociada al proveedor, municipio y servicio
                    OPEN  c_rutasprov ( v_solic.empresa_id, v_munic_salida_int, 'TO',  'REDCO');
                    FETCH c_rutasprov INTO v_rutasprov;

                    IF c_rutasprov%NOTFOUND THEN
                       DBMS_OUTPUT.put_line('<TRACK 05> Error: Ruta de provisión no configurada. No se generaron puentes.'||v_solic.empresa_id||'-'||v_munic_salida_int);
                    END IF;
                    CLOSE c_rutasprov;

                    IF NVL(v_rutasprov.control_puentes,'N') = 'S'
                       AND NVL(v_rutasprov.control_ordenes,'N') = 'S'
                    THEN
                        -- 2010-05-06
                        -- Uso de procedimiento para generar puentes de configuraciòn / desconfiguracion cuando
                        -- se retira un servicio TOIP. El procedimiento verifica condiciones de Multiservicio.
                       PR_PROV_PUENTES_ORIGEN
                          ( p_pedido              => w_pedido
                          ,p_subpedido            => v_solic.subpedido_id
                          ,p_solicitud            => v_solic.solicitud_id
                          ,p_tipo_elemento_id     => 'DATBA'
                          ,p_tipo_transaccion     => 'RETIR'
                          ,p_identificador_actual => v_solic.identificador_id
                         );
                    END IF;

              ELSE
                  DBMS_OUTPUT.put_line('<TRACK 04> Error: Municipio de salida NO encontrado. No se generaron puentes.');
              END IF;
           ELSE
              DBMS_OUTPUT.put_line('<TRACK 03> Error: Municipio Servicio NO encontrado. No se generaron puentes.');
           END IF;

            --2014-01-14 lhernans
            --Se determina si se inserta o no etiqueta

            --2014-10-22   RVELILLR Condicional de tecnologia para consulta de etiquetas para generar eventos.
           IF NVL(v_solic.tecnologia_id, 'NOT') = 'REDCO' THEN
                --2014-10-14   MGATTIS
                v_aplicacion := 'DSLAM';

                v_etiqueta := PKG_ETIQUETAS_EVENTOS_BTS.FN_VALOR_PARAM_EVENTOS_BTS (
                                                v_aplicacion,
                                                v_solic.producto_id,
                                                v_solic.tipo_elemento_id,
                                                'RETIR',
                                                1
                                                );
                --2014-10-14   MGATTIS FIN Lógica.
           ELSE
             --Se recupera valor de la etiqueta
             --formado por ETBTS_[MUNICIPIO]_[PRODUCTO]_[TECNOLOGIA] (Sin corchetes)
             v_etiqueta := fn_dominio_values_meaning (
                                                'ETBTS'
                                                   || '_'
                                                   || v_munic_salida_int
                                                   || '_'
                                                   || v_solic.producto_id
                                                   || '_'
                                                   || v_solic.tecnologia_id,
                                                'RETIR',
                                                1
                                                );
           END IF;
           --2014-10-22   RVELILLR FIN Lógica.

           IF NVL (v_etiqueta, 'NA') <> 'NA' THEN

              --Rutina de inserción de etiquetas
              pr_insertar_etiquetas_bts (
                                        v_solic.pedido_id,
                                        v_solic.subpedido_id,
                                        v_solic.solicitud_id,
                                        v_solic.servicio_id,
                                        v_solic.producto_id,
                                        v_solic.tipo_elemento,
                                        v_solic.tipo_elemento_id,
                                        4805,
                                        v_solic.municipio_id,
                                        v_solic.empresa_id,
                                        v_etiqueta,
                                        w_mensaje_error
                                        );

           END IF;
           --2014-01-14 lhernans FIN Lógica.

           w_estado_solicitud  := 'DEFIN';
           w_estado_id         := 'ORDEN';

           -- 2014-03-26 dgirall  Se modifica asignacion para asignar la variable v_concepto_toip en lugar de asignar
           --   directamente el concepto PORDE
           --w_concepto          := 'PORDE';
           w_concepto          := v_concepto_toip;

           -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
           -- se quedan en petec.
           v_nueva_observacion := ' <# ET-TOIP [PORDE]> ';
           v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

           -- Paso a concepto PORDE de solicitud de TOIP
           BEGIN
                 UPDATE FNX_SOLICITUDES
                 SET         estado_soli    = w_estado_solicitud
                         ,estado_id        = w_estado_id
                         ,concepto_id    = w_concepto
                         ,tecnologia_id     = v_tecnologia_solic
                         -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                         -- se quedan en petec.
                         --,observacion     =  ' <# ET-TOIP [PORDE]> '||observacion
                         ,observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                 WHERE    pedido_id       = v_solic.pedido_id
                AND     subpedido_id    = v_solic.subpedido_id
                AND     solicitud_id    = v_solic.solicitud_id;

           EXCEPTION

                 WHEN OTHERS THEN
                    w_concepto  := 'APROB';

                    -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                    -- se quedan en petec.
                    v_nueva_observacion := ' <# ET-TOIP [APROB]> ';
                    v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                    BEGIN
                         UPDATE FNX_SOLICITUDES
                         SET         estado_soli    = w_estado_solicitud
                                 ,estado_id        = w_estado_id
                                 ,concepto_id    = w_concepto
                                 ,tecnologia_id     = v_tecnologia_solic
                                 -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                                 -- se quedan en petec.
                                 --,observacion     =  ' <# ET-TOIP [APROB]> '||observacion
                                 ,observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                         WHERE    pedido_id       = v_solic.pedido_id
                        AND     subpedido_id    = v_solic.subpedido_id
                        AND     solicitud_id    = v_solic.solicitud_id;
                    EXCEPTION
                            WHEN OTHERS THEN
                                w_mensaje_error        := '<ET-TOIP> Error solicitud a APROB.'||SUBSTR(SQLERRM, 1, 15);
                    END;
           END;

       -- 2014-01-14 Jrendbr   Se adiciona codigo para que procese la solicitud de adición de equipo DPS en internet
       ELSIF p_equipo_dps THEN
                --- //////////////////////////////////////////////////////////////////////
                -- Inicializacion de los paràmetros fijos para crear una solicitud
                --- //////////////////////////////////////////////////////////////////////
                v_subpedido         := '1';
                v_solicit         := '2';
                v_esta_solicitud  := 'TECNI';
                v_concept_solicitud:= 'PETEC';
                v_tecnolog        := 'REDCO';
                v_tip_solicitud    := 'NUEVO';
                v_servicio          := 'TELRES';
                v_product          := 'TO';
                v_tipo_elemento_id  := 'EQURED';
                v_tipo_elemento     := 'EQU';
                v_ejecutivo         := '460137';

                -- Inicialización de listas para contener caracterìsticas y trabajos
                v_caracteristicas := FENIX.PKG_SOLICITUD_CREACION.TAB_REC_CARACT(NULL);
                v_trabajos        := FENIX.PKG_SOLICITUD_CREACION.TAB_REC_TRABAJ(NULL);


                --- //////////////////////////////////////////////////////////////////////
                -- Lista de caracteristicas vacias
                --- //////////////////////////////////////////////////////////////////////

                indice_lcar := indice_lcar + 1;
                v_caracteristicas(indice_lcar).car_id   := 90;
                v_caracteristicas(indice_lcar).valor    := NULL;
                v_caracteristicas.EXTEND;

                indice_lcar := indice_lcar + 1;
                v_caracteristicas(indice_lcar).car_id   := 200;
                v_caracteristicas(indice_lcar).valor    := NULL;
                v_caracteristicas.EXTEND;

                indice_lcar := indice_lcar + 1;
                v_caracteristicas(indice_lcar).car_id   := 347;
                v_caracteristicas(indice_lcar).valor    := 'DPS';
                v_caracteristicas.EXTEND;

                indice_lcar := indice_lcar + 1;
                v_caracteristicas(indice_lcar).car_id   := 960;
                v_caracteristicas(indice_lcar).valor    := NULL;
                v_caracteristicas.EXTEND;

                indice_lcar := indice_lcar + 1;
                v_caracteristicas(indice_lcar).car_id   := 982;
                v_caracteristicas(indice_lcar).valor    := NULL;
         --       v_caracteristicas.EXTEND;

                --- //////////////////////////////////////////////////////////////////////
                -- Lista de trabajos a insertar en la solicitud
                --- //////////////////////////////////////////////////////////////////////

                v_contador_caract := v_contador_caract + 1;
                v_trabajos(v_contador_caract).car_id          := 200;
                v_trabajos(v_contador_caract).valor           := NULL;
                v_trabajos(v_contador_caract).tipo_trabajo    := 'NUEVO';
        --        v_trabajos.EXTEND;

                --v_contador_caract := v_contador_caract + 1;
                --v_trabajos(v_contador_caract).car_id          := 960;
                --v_trabajos(v_contador_caract).valor           := NULL;
                --v_trabajos(v_contador_caract).tipo_trabajo    := 'CAMBI';
                --v_trabajos.EXTEND;

                --v_contador_caract := v_contador_caract + 1;
                --v_trabajos(v_contador_caract).car_id          := 982;
                --v_trabajos(v_contador_caract).valor           := NULL;
                --v_trabajos(v_contador_caract).tipo_trabajo    := 'CAMBI';

                --- //////////////////////////////////////////////////////////////////////
                -- Invocar procedimiento para crear la solicitud
                --- //////////////////////////////////////////////////////////////////////
               PKG_SOLICITUD_CREACION.PR_SOLICITUD_CREAR (v_solic.pedido_id , v_subpedido, v_solicit,
                            NULL, v_solic.identificador_id, v_caracteristicas,  v_trabajos,
                            v_esta_solicitud, v_concept_solicitud,  v_tecnolog,
                            v_tip_solicitud,   v_servicio, v_product,
                            v_tipo_elemento_id, v_tipo_elemento, v_ejecutivo,
                            'S', 'MEDANTCOL',  'UNE',  p_mensaje_proc );

             OPEN  c_solic_dps(v_solic.pedido_id , v_subpedido);

               LOOP
                  FETCH c_solic_dps INTO v_solic_dps;
                  EXIT WHEN c_solic_dps%NOTFOUND;
                   w_estado_id         := 'ORDEN';
                   IF v_solic_dps.tipo_elemento_id = 'EQURED'THEN
                    w_concepto          := 'PORDE';
                    v_nueva_observacion := ' <# ET-TOIP [PORDE]> ';
                    v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);
                   ELSE
                    w_concepto          := 'POPTO';
                    v_nueva_observacion := ' <# ET-TOIP [POPTO]> ';
                    v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);
                   END IF;
                   w_estado_solicitud  := 'DEFIN';

                   BEGIN
                         UPDATE FNX_SOLICITUDES
                         SET         estado_soli    = w_estado_solicitud
                                 ,estado_id        = w_estado_id
                                 ,concepto_id    = w_concepto
                          --      ,tecnologia_id     = v_tecnolog
                                ,observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                         WHERE    pedido_id       = v_solic_dps.pedido_id
                        AND     subpedido_id    = v_solic_dps.subpedido_id
                        AND     solicitud_id    = v_solic_dps.solicitud_id;

                   EXCEPTION
                         WHEN OTHERS THEN
                            w_concepto  := 'APROB';
                            v_nueva_observacion := ' <# ET-TOIP [APROB]> ';
                            v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                            BEGIN
                                 UPDATE FNX_SOLICITUDES
                                 SET         estado_soli    = w_estado_solicitud
                                         ,estado_id        = w_estado_id
                                         ,concepto_id    = w_concepto
                                  --       ,tecnologia_id     = v_tecnolog
                                         ,observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                                 WHERE    pedido_id       = v_solic_dps.pedido_id
                                AND     subpedido_id    = v_solic_dps.subpedido_id
                                AND     solicitud_id    = v_solic_dps.solicitud_id;
                            EXCEPTION
                                     WHEN OTHERS THEN
                                         w_mensaje_error        := '<ET-TOIP> Error solicitud a APROB.'||SUBSTR(SQLERRM, 1, 15);
                            END;
                   END;

              END LOOP;
            CLOSE c_solic_dps;
       END IF; -- FIN -- FIN SECCION.  Nuevo o traslado de servicio

       -- SECCION.  Creacion de solicitudes de Traslado para cada TO asociada al identificador TOIP
       IF  p_traslado  AND w_mensaje_error IS NULL THEN

           DBMS_OUTPUT.put_line('<TELEF-IP> Antes de crear solicitudes de Traslado');

           DBMS_OUTPUT.put_line('<ET-TOIP> Traslado de servicio COMERCIAL');
           IF b_enrutamiento_porde THEN
              w_estado_id  := 'ORDEN';
              w_concepto   := 'PSERV';
              v_tecnologia_solic  := PKG_IDENTIFICADORES.fn_tecnologia ( v_solic.identificador_id );
           ELSE
              w_estado_id  := 'TECNI';
              w_concepto   := 'PRACC';
              v_tecnologia_solic := NULL;
           END IF;

           v_nro_solicitud_crear   := NULL;
           v_solicitud_liquidada   := 'N';
           -- 2010-04-20 Buscar si existe una solicitud de empaquetamiento para el pedido en tal caso la solicitud no necesita liquidación
           v_paquete_identif_trasl := NULL;
           OPEN c_solicitud_paquete_trasl (w_pedido);
          FETCH c_solicitud_paquete_trasl
           INTO v_paquete_identif_trasl;
          CLOSE c_solicitud_paquete_trasl;

           IF v_paquete_identif_trasl IS NOT NULL THEN
                v_solicitud_liquidada   := 'S';
           END IF;

           FOR regident IN c_ident_to_asociados ( v_solic.identificador_id ) LOOP

                -- Crear las solicitudes de cambio de los componentes TO asociados al TOIP
                PR_SOLICITUD_CAMBI_TOIP ( w_pedido, v_solic.subpedido_id, v_solic.solicitud_id,
                              v_solic.identificador_id,
                              'TELRES', 'TO', 'TO', 'CMP',
                              regident.identificador_id, v_tecnologia_solic,
                              w_estado_id, w_concepto,
                              v_solic.municipio_id, v_solic.empresa_id,
                              v_solicitud_liquidada,
                              v_nro_solicitud_crear, v_mensaje_creacion );

               IF v_mensaje_creacion IS NOT NULL THEN
                  EXIT;
               ELSE
                   v_nro_solicitud_to      := v_nro_solicitud_crear;
                   v_nro_solicitud_crear   := v_nro_solicitud_crear + 1;
               END IF;

           END LOOP;

           -- 2009-06-30
           -- Traslado.  AutoLogon Wimax.  Tecnologia Inalambrica. Creaciòn de solicitud de ANTENA
           IF v_tecnologia_solic = 'INALA' THEN

              IF b_enrutamiento_porde THEN
                 w_estado_id  := 'ORDEN';
                 w_concepto   := 'POPTO';
              ELSE
                 w_estado_id  := 'RUTAS';
                 w_concepto   := 'PRUTA';
              END IF;

              PR_SOLICITUD_NUEVA_TOIP ( w_pedido, v_solic.subpedido_id, v_nro_solicitud_to,
                                        'TELRES', 'TO', 'ANTENA', 'EQU',
                                        v_solic.identificador_id, v_tecnologia_solic,
                                        w_estado_id, w_concepto,
                                        v_solic.municipio_id, v_solic.empresa_id,
                                        v_nro_solicitud_crear, v_mensaje_creacion );

              -- Actualización de la caracterìstica 90 para la nueva solicitud de ANTENA
              UPDATE FNX_CARACTERISTICA_SOLICITUDES
              SET    valor              = v_solic.identificador_id
              WHERE  pedido_id          = v_solic.pedido_id
              AND    subpedido_id       = v_solic.subpedido_id
              AND    solicitud_id       = v_nro_solicitud_crear
              AND    caracteristica_id  = 90;

           END IF;

           -- SECCION.  Generación Orden para solicitud de TOIP
           -- Paso a concepto PORDE de solicitud de TOIP, cuando el enrutamiento es directo
           -- al estado de Ordenes.  Caso de pedidos para otras ciudades.
           IF b_provision_automatica  THEN

               w_estado_id         := 'ORDEN';
               w_concepto          := 'PORDE';
               w_estado_solicitud  := 'DEFIN';

               -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
               -- se quedan en petec.
               v_nueva_observacion := ' <# ET-TOIP [PORDE]> ';
               v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

               BEGIN
                     UPDATE FNX_SOLICITUDES
                     SET         estado_soli    = w_estado_solicitud
                             ,estado_id        = w_estado_id
                             ,concepto_id    = w_concepto
                            ,tecnologia_id     = v_tecnologia_solic
                            -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                            -- se quedan en petec.
                            --,observacion     =  ' <# ET-TOIP [PORDE]> '||observacion
                            ,observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                     WHERE    pedido_id       = v_solic.pedido_id
                    AND     subpedido_id    = v_solic.subpedido_id
                    AND     solicitud_id    = v_solic.solicitud_id;

               EXCEPTION
                     WHEN OTHERS THEN
                        w_concepto  := 'APROB';

                           -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                           -- se quedan en petec.
                           v_nueva_observacion := ' <# ET-TOIP [APROB]> ';
                           v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

                        BEGIN
                             UPDATE FNX_SOLICITUDES
                             SET         estado_soli    = w_estado_solicitud
                                     ,estado_id        = w_estado_id
                                     ,concepto_id    = w_concepto
                                     ,tecnologia_id     = v_tecnologia_solic
                                     -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                                     -- se quedan en petec.
                                     --,observacion     =  ' <# ET-TOIP [APROB]> '||observacion
                                     ,observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                             WHERE    pedido_id       = v_solic.pedido_id
                            AND     subpedido_id    = v_solic.subpedido_id
                            AND     solicitud_id    = v_solic.solicitud_id;
                        EXCEPTION
                                 WHEN OTHERS THEN
                                     w_mensaje_error        := '<ET-TOIP> Error solicitud a APROB.'||SUBSTR(SQLERRM, 1, 15);
                        END;
               END;

           END IF; -- FIN SECCION.  Generación Orden para solicitud de TOIP

       END IF;
      --- FIN SECCION.  Creacion de solicitudes de Traslado para cada TO

     FETCH c_solicitudes INTO v_solic;
END LOOP;
CLOSE c_solicitudes;

-- Si hubo un fallo en la provisiòn, la solicitud de TOIP se actualiza al estado y concepto que
-- se determinó en el fallo.
IF w_incons AND  w_concepto IN ('21', '78', '79' ) THEN

    -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
    -- se quedan en petec.
    v_nueva_observacion := '<# ET-TOIP ['||w_concepto||'] ';
    v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

    BEGIN
         UPDATE FNX_SOLICITUDES
         SET         estado_soli    = w_estado_solicitud
                 ,estado_id        = w_estado_id
                 ,concepto_id    = w_concepto
                ,tecnologia_id     = v_tecnologia_solic
                -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                -- se quedan en petec.
                --,observacion     =  '<# ET-TOIP ['||w_concepto||'] '||observacion
                ,observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
         WHERE    pedido_id       = v_solic.pedido_id
        AND     subpedido_id    = v_solic.subpedido_id
        AND     solicitud_id    = v_solic.solicitud_id;
    EXCEPTION
            WHEN OTHERS THEN
                w_mensaje_error        := '<ET-TOIP> Error solicitud actualizacion'||SUBSTR(SQLERRM, 1, 15);
    END;
END IF;

-- 2009-02-07
-- Para el caso de telefonia IP Empresarial, las solicitudes de TO provisionadas, deben
-- pasar al concepto PORDE.

-- 2009-06-30
-- Adiciòn de condiciòn p_nuevo.  El enrutamiento de solicitudes en PINSC solo se realiza para la transacciòn
-- de Nuevo TO
IF p_nuevo  AND b_provision_automatica  THEN

     w_concepto   := 'PORDE';

     -- 2012-09-20  Sbuitrab      Se realiza modificación para el tema de ciclo correria, puesto que antes de pasar a estado PORDE se debe verificar
     --                           que el pedido si cumpla con los requisitos para la correcta facturación
        IF   w_concepto = 'PORDE' THEN
            pr_estudio_tecnico_pumed(w_pedido,v_estado_pumed,v_concepto_pumed,v_observacion_pumed,w_mensaje_pumed,paquete_out);
                IF w_mensaje_pumed THEN
                    w_concepto:= v_concepto_pumed;
                END IF;
        END IF;

     BEGIN
         UPDATE FNX_SOLICITUDES
         SET    concepto_id      = w_concepto,
                tecnologia_id    = v_tecnologia_solic,
                observacion      = '<% TOIP AA ['||w_concepto||'] %> '
                                   ||SUBSTR(observacion, 1, 950)
         WHERE  pedido_id        = w_pedido
         AND    servicio_id      = 'TELRES'
         AND    producto_id      = 'TO'
         AND    tipo_elemento_id = 'TO'
         AND    estado_id        = 'ORDEN'
         AND    concepto_id      = 'PINSC'
         ;
    EXCEPTION
        WHEN OTHERS THEN

             w_concepto  := 'APROB';
             UPDATE FNX_SOLICITUDES
             SET    concepto_id      = w_concepto,
                    tecnologia_id    = v_tecnologia_solic,
                    observacion      = '<% TOIP AA ['||w_concepto||'] %> '
                                       ||SUBSTR(observacion, 1, 950)
             WHERE  pedido_id        = w_pedido
             AND    servicio_id      = 'TELRES'
             AND    producto_id      = 'TO'
             AND    tipo_elemento_id = 'TO'
             AND    estado_id        = 'ORDEN'
             AND    concepto_id      = 'PINSC'
             ;
    END;

END IF;

-- 2007-04-09
-- Logica para pasar la solicitud del paquete a ORDEN-PORDE, sí la solicitud de
-- de acceso primario, fue puesta en ORDEN-PORDE.
IF b_generar_ordenes AND NOT w_incons     AND
 v_concepto_paquete = 'PORDE'
THEN
  PKG_PROVISION_PAQUETE.pr_genorden_pexpq ( v_solic.pedido_id,v_solic.tipo_elemento_id, p_mensaje);
ELSIF b_generar_ordenes AND NOT w_incons  AND
    v_concepto_paquete = 'APROB'
THEN

   -- Logica para paso a ORDEN-APROB de solicitud de paquete, si la television
   -- se quedo en APROB.
   -- Verificacion del tipo de paquete
   v_tipo_paquete := PKG_PROVISION_PAQUETE.fn_valor_caract_paquete (w_pedido, 2878);

   -- 2008-06-24   Aacosta   Inclusión de los nuevos planes de paquete para la UEN hogares.
   --                        'UNETE1','UNETE2','UNETE3'.
   -- 2008-07-17   Aacosta   Modificación de la forma como se valida si el tipo de paquete
   --                        está configurado para que el componente de paquete genere orden.
   --                        VALIDACIÓN ANTERIOR: IF NVL(v_tipo_paquete, 'NULO') IN ('OFERTA','UNETE1','UNETE2','UNETE3') THEN

 IF NVL(PKG_PROVISION_PAQUETE.fn_generar_orden(v_tipo_paquete),'SI') = 'SI' THEN

        -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
        -- se quedan en petec.
        v_nueva_observacion := '<#TOIP ['||v_concepto_paquete||'] #>';
        v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

       BEGIN
          UPDATE FNX_SOLICITUDES
          SET    estado_id    = 'ORDEN',
                 concepto_id  = v_concepto_paquete,
                 -- 2010-03-02   Aobregon    Se modifica la actualizacion de las observaciones para que no superen los 1000 caractereres porque los pedidos
                 -- se quedan en petec.
                 --observacion  = '<#TOIP ['||v_concepto_paquete||'] #>'||observacion
                 observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
          WHERE  pedido_id    = w_pedido
          AND    servicio_id  = 'PQUETE'
          AND    producto_id  = 'PQUETE'
          AND    tipo_elemento_id = 'PQUETE'
          AND    estado_id    = 'ORDEN'
          AND    concepto_id  = 'PXSLN';
       EXCEPTION
          WHEN OTHERS THEN
             NULL;
       END;
  END IF;
END IF;

--2009-09-24 SAC Equipos Thomson - Se inserta solicitud de EQACCP para traslado de internet BARES
--2010-09-29 Para insertar la solicitud de EQACCP, no debe ser un pedido en concepto INGRE-PUMED.
IF NOT w_incons AND
        p_traslado  AND NOT
        b_ingre_pumed
THEN

    v_solicitud_eqaccp := NULL;
    v_cliente := NULL;

    PR_SOLICITUD_EQUIPO_CREAR ( v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_eqaccp
                               ,v_solic.servicio_id,v_solic.producto_id
                               ,'EQACCP', 'EQU', v_tecnologia_solic, v_cliente
                               ,v_solic.identificador_id, v_solic.municipio_id, v_solic.empresa_id
                               ,p_mensaje);

    IF p_mensaje IS NULL THEN

        IF v_solicitud_eqaccp IS NOT NULL THEN

           -- 2010-01-20  Aacosta  Provisión de equipos de red asociados a un atraslado de TOIP.
           IF b_provision_automatica THEN
              v_estado   := 'ORDEN';
              v_concepto := 'POPTO';
              v_estado_soli := 'DEFIN';
              v_tecnologia := v_tecnologia_solic;
           ELSE
              v_estado   := 'RUTAS';
              v_concepto := 'PRUTA';
              v_estado_soli := 'PENDI';
              v_tecnologia := v_tecnologia_solic;
           END IF;

           UPDATE FNX_SOLICITUDES
           SET    estado_id     = v_estado,
                  concepto_id   = v_concepto,
                  estado_soli   = v_estado_soli,
                  tecnologia_id = v_tecnologia
           WHERE  pedido_id     = v_solic.pedido_id
           AND    subpedido_id  = v_solic.subpedido_id
           AND    solicitud_id  = v_solicitud_eqaccp;

        END IF;

        v_solicitud_equred := v_solicitud_eqaccp+1;

        PR_SOLICITUD_CAMBI_EQURED( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                                   v_solic.servicio_id, v_solic.producto_id,
                                   'EQURED','EQU',
                                   v_solic.identificador_id, v_solic.identificador_id,
                                   v_serial_equipo, v_marca_equipo, v_referencia_equipo, v_terminal_asociado,
                                   v_tecnologia_solic,'RUTAS','PRUTA',v_solic.municipio_id,v_solic.empresa_id,
                                   v_solicitud_equred,p_mensaje);

        IF p_mensaje IS NULL THEN

            IF v_solicitud_equred IS NOT NULL THEN

               --2011-05-16 ETachej - Integridad Fenix - OPEN - Traslados TOIP
                -- Obtener los datos del equipo de RED actual asociado al identificador.
                OPEN  c_equipo_actual ( 'ATA' , v_solic.identificador_id);
                FETCH c_equipo_actual INTO v_serial_equipo,v_marca_equipo,v_referencia_equipo;
                CLOSE c_equipo_actual;

                 IF (v_serial_equipo IS NULL) THEN
                    OPEN  c_equipo_actual ( 'CPE' , v_solic.identificador_id);
                    FETCH c_equipo_actual INTO v_serial_equipo,v_marca_equipo,v_referencia_equipo;
                    CLOSE c_equipo_actual;
                 END IF;

                IF (v_serial_equipo IS NOT NULL) THEN

                    -- Actualización de la caracterìstica 200 para EQURED
                      UPDATE FNX_TRABAJOS_SOLICITUDES
                      SET    valor              = v_serial_equipo
                      WHERE  pedido_id          = v_solic.pedido_id
                      AND    subpedido_id       = v_solic.subpedido_id
                      AND    solicitud_id       = v_solicitud_equred
                      AND    caracteristica_id  = 200;

                     PKG_PEDIDOS_UTIL.PR_Crear_Trabajo
                     ( v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_equred,
                       v_solic.servicio_id, v_solic.producto_id, 'EQU', 'EQURED',
                       960, v_marca_equipo,
                       'CAMBI', v_solic.municipio_id, v_solic.empresa_id, p_mensaje );

                       PKG_PEDIDOS_UTIL.PR_Crear_Trabajo
                     ( v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_equred,
                       v_solic.servicio_id, v_solic.producto_id, 'EQU', 'EQURED',
                       982, v_referencia_equipo,
                       'CAMBI', v_solic.municipio_id, v_solic.empresa_id, p_mensaje );

                END IF;


               IF b_provision_automatica THEN
                  v_estado   := 'ORDEN';
                  v_concepto := 'PORDE';
                  v_estado_soli := 'DEFIN';
                  v_tecnologia := v_tecnologia_solic;
               ELSE
                  v_estado   := 'RUTAS';
                  v_concepto := 'PRUTA';
                  v_estado_soli := 'PENDI';
                  v_tecnologia := v_tecnologia_solic;
               END IF;

               UPDATE FNX_SOLICITUDES
                  SET    estado_id     = v_estado,
                         concepto_id   = v_concepto,
                         estado_soli   = v_estado_soli,
                         tecnologia_id = v_tecnologia
               WHERE  pedido_id    = v_solic.pedido_id
               AND    subpedido_id = v_solic.subpedido_id
               AND    solicitud_id = v_solicitud_equred;
            END IF;
        ELSE
             w_incons := FALSE;
        END IF;

        IF (p_mensaje IS NOT NULL) THEN
          w_incons := FALSE;
        END IF;

    ELSE
       w_incons := FALSE;
    END IF;
END IF;

/***************************************************************************************************/
    -- 2011-03-14. DOJEDAC. Pry-Desaprovisionamiento Se agrega la validación de que si la transacción es un retiro
    --                     para poder crear la solicitud de equipo.
    IF NOT w_incons AND p_retiro THEN

        v_identifica_acceso := FN_VALOR_CARACT_IDENTIF(v_solic.identificador_id, 90);

        --Dojedac: Validar la cantidad de equipos asociados al servicio/productos que se va a retirar para generar
        --         las solicitudes de recuperación de equipos
        v_cade_Query := 'SELECT A.EQUIPO_ID, A.MARCA_ID, A.REFERENCIA_ID, A.TIPO_ELEMENTO, A.TIPO_ELEMENTO_ID, ' ||
                        '        DECODE(A.TIPO_ELEMENTO_ID, ''CPE'', ''EQACCP'', ''CPEWIM'', ''EQACCP'', ''CABLEM'', ''EQACCP'', ''EQURED'') TIPO_ELEMENTO_ID_EQU, ' ||
                        '       CASE WHEN B.NUM_MTA = 2 AND A.TIPO_ELEMENTO_ID IN (''CABLEM'') THEN ' ||
                        '              1 ' ||
                        '            WHEN B.NUM_MTA = 2 AND A.TIPO_ELEMENTO_ID IN (''ATA'') THEN ' ||
                        '              0 ' ||
                        '            ELSE ' ||
                        '              1 ' ||
                        '       END GENERAR ' ||
                        'FROM FNX_EQUIPOS A, (SELECT MARCA_ID, REFERENCIA_ID, COUNT(TIPO_ELEMENTO_ID) NUM_MTA ' ||
                        '                     FROM FNX_MARCAS_REFERENCIAS ' ||
                        '                     GROUP BY MARCA_ID, REFERENCIA_ID) B ' ||
                        'WHERE A.MARCA_ID = B.MARCA_ID ' ||
                        '  AND A.REFERENCIA_ID = B.REFERENCIA_ID ' ||
                        '  AND A.IDENTIFICADOR_ID IN ('''|| v_solic.identificador_id || ''', ''' || v_identifica_acceso || ''')' ||
                        '  AND A.ESTADO = ''OCU'' ';

        v_equipo_obsoleto := 'N';
        v_si_obsoleto := 0;
        v_no_obsoleto := 0;
        v_si_nir := 0;
        v_equip_obsol := 'EQUIPOS OBSOLETOS ';

        -- Dojedac: Se ejecuta el cursor de forma dinamica y se generan las solicitudes para
        --         recuperar los equipos asociados al identificador que se va a retirar.
        OPEN c_Equipos_solic FOR v_cade_Query;
        LOOP
            FETCH c_Equipos_solic INTO reg_tipo_equipo;
            EXIT WHEN c_Equipos_solic%NOTFOUND;

            --Dojedac. 05/05/2011. Validar si el equipos es o no obsoleto para generar o no orden de carta o de recuperación del equipo.
            v_equipo_obsoleto := fn_consultar_equipo_obsoleto (reg_tipo_equipo.marca_id,         reg_tipo_equipo.referencia_id,
                                                               reg_tipo_equipo.tipo_elemento_id, reg_tipo_equipo.tipo_elemento);

            IF v_equipo_obsoleto = 'N' THEN

                --2014-02-24    Jrendbr Se adiciona logica para que tenga en cuenta los esatod de los equipos NIR.
                v_equipo_nir := fn_consultar_equipo_nir (reg_tipo_equipo.marca_id,         reg_tipo_equipo.referencia_id,
                                                         reg_tipo_equipo.tipo_elemento_id, reg_tipo_equipo.tipo_elemento);

                IF  v_equipo_nir = 'S' THEN
                    v_si_nir      := v_si_nir + 1;
                    v_equip_obsol := '; Equipo NIR: Marca: ' || reg_tipo_equipo.marca_id || ', Referencia: ' || reg_tipo_equipo.referencia_id || ', Serial: '||reg_tipo_equipo.equipo_id|| '';
                ELSE
                    v_no_obsoleto := v_no_obsoleto + 1;
                    v_equip_obsol := '; Equipo a recuperar: Marca: ' || reg_tipo_equipo.marca_id || ', Referencia: ' || reg_tipo_equipo.referencia_id || ', Serial: '||reg_tipo_equipo.equipo_id|| '';
                END IF;
            ELSE
                v_si_obsoleto := v_si_obsoleto + 1;
                v_equip_obsol := '; EQUIPO OBSOLETO: Marca: ' || reg_tipo_equipo.marca_id || ', Referencia: ' || reg_tipo_equipo.referencia_id || ', Serial: '||reg_tipo_equipo.equipo_id|| '';
            END IF;



            IF reg_tipo_equipo.generar = 1 THEN
                -- JGallg - 2012-04-10 - MejorasDesaprov: Centralizar validación para generar solicitud en una sola función interna.  En ella
                --                       se valida: infraestructura compartida para CPE y CABLEM, equipo compartido y propiedad de equipo.
                b_genera_solic_equipo := fni_generar_solic_equipo (v_solic.identificador_id, v_tecnologia_solic, reg_tipo_equipo, w_pedido);
                IF b_genera_solic_equipo THEN
                    v_solicitud_eqaccp := NULL;

                    PR_SOLICITUD_RETIR_EQUIPO (v_solic.pedido_id,         v_solic.subpedido_id,                 v_solicitud_eqaccp,   v_solic.servicio_id,
                                               v_solic.producto_id,       reg_tipo_equipo.tipo_elemento_id_equ, 'EQU',                v_tecnologia_solic,
                                               v_cliente,                 v_solic.identificador_id,             v_solic.municipio_id, v_solic.empresa_id,
                                               reg_tipo_equipo.equipo_id, 'S',                                  p_mensaje);

                    IF p_mensaje IS NULL THEN
                        IF v_solicitud_eqaccp IS NOT NULL THEN
                            -- JGallg - 2012-04-10 - MejorasDesaprov: Llamado a procedimiento interno que hace paso a ORDEN - PORDE

                            -- 2014-03-26 dgirall  Se envia por parametro el concepto de la nueva solicitud (PINSC o PORDE)
                            IF NVL(v_pedido_acp_pdte,'-') <> '-' THEN
                                pri_paso_orden_porde (v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_eqaccp, 'PINSC', v_equip_obsol, v_tecnologia_solic, p_mensaje);
                            ELSE
                                pri_paso_orden_porde (v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_eqaccp, 'PORDE', v_equip_obsol, v_tecnologia_solic, p_mensaje);
                            END IF;

                        END IF;

                        v_solicitud_equred := v_solicitud_eqaccp + 1;

                        IF p_mensaje IS NOT NULL THEN
                            w_incons := FALSE;
                        END IF;
                    ELSE
                        w_incons := FALSE;
                    END IF;
                END IF;
            ELSE
                ---- 2012-08-21 Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que no se debe generar Solicitud Recuperación equipos
                BEGIN
                    UPDATE FNX_PEDIDOS
                    SET observacion =  SUBSTR ('Desap-NoGeneraSoli RecupEquip' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                    WHERE pedido_id     = w_pedido;
                EXCEPTION
                    WHEN OTHERS THEN
                        NULL;
                END;
            END IF;

            ---- 2012-08-21 Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el equipo es Obsoleto
            IF v_si_obsoleto >= 1 THEN
                BEGIN
                    UPDATE FNX_PEDIDOS
                    SET observacion =  SUBSTR ('Desap-Equi Obsolet' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                    WHERE pedido_id     = w_pedido;
                EXCEPTION
                    WHEN OTHERS THEN
                        NULL;
                END;
            END IF;

            --2014-02-24    Jrendbr Se adiciona logica para que tenga en cuenta los esatod de los equipos NIR.
            IF v_si_nir >= 1 THEN
                BEGIN
                    UPDATE FNX_PEDIDOS
                    SET observacion =  SUBSTR ('Desap-Equi NIR' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                    WHERE pedido_id     = w_pedido;
                EXCEPTION
                    WHEN OTHERS THEN
                        NULL;
                END;
            END IF;

        END LOOP;
        CLOSE c_Equipos_solic;

        -- 2013-10-03   Yhernana    Inicio
        --                          REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
        IF v_tecnologia_solic = 'HFC' THEN

            v_accesp_comparte := fn_comparte_infra_activa (v_solic.identificador_id, v_tecnologia_solic, 'INTER', 'ACCESP', NULL);
            -- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
            --                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
            IF v_accesp_comparte IS NOT NULL THEN

                SELECT COUNT (trso.pedido_id)
                  INTO v_cant_retir_proc
                  FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                 WHERE trso.pedido_id = soli.pedido_id
                   AND trso.subpedido_id = soli.subpedido_id
                   AND trso.solicitud_id = soli.solicitud_id
                   AND trso.caracteristica_id = 1
                   AND (trso.valor = v_accesp_comparte OR soli.identificador_id = v_accesp_comparte)
                   AND trso.tipo_trabajo = 'RETIR'
                   AND soli.estado_id = 'ORDEN';

                IF v_cant_retir_proc = 1 THEN

                    v_equipo_retiro:= FN_VERIFICAR_RETIR_EQUIPO('INTER',v_accesp_comparte, v_tecnologia_solic);--> Busco equipo en el ACCESP

                    IF v_equipo_retiro IS NOT NULL THEN
                        b_aplica_retir:= TRUE;
                    END IF;
                END IF;
            ELSE

                OPEN c_datos_identif2 (v_solic.identificador_id);
                FETCH c_datos_identif2 INTO rg_datos_identif2;
                CLOSE c_datos_identif2;

                FOR rg_equipo_comparte IN c_equipo_comparte2 (v_solic.identificador_id, rg_datos_identif2.cliente_id, rg_datos_identif2.pagina_servicio) LOOP

                    BEGIN
                        SELECT EQUIPO_ID, MARCA_ID, REFERENCIA_ID, TIPO_ELEMENTO_ID, TIPO_ELEMENTO
                          INTO v_equipo_retiro, v_marca_id, v_refer_id, v_tipele_id, v_tipo_ele
                          FROM FNX_EQUIPOS
                         WHERE identificador_id = rg_equipo_comparte.identificador_id;
                    EXCEPTION
                        WHEN OTHERS THEN
                            v_equipo_retiro     := NULL;
                            v_marca_id          := NULL;
                            v_refer_id          := NULL;
                            v_tipo_ele          := NULL;
                            v_tipele_id         := NULL;
                    END;

                    IF v_equipo_retiro IS NULL THEN
                        b_aplica_retir:= FALSE;
                        EXIT;
                    ELSE
                        -- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
                        --                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
                        SELECT COUNT (trso.pedido_id)
                          INTO v_cant_retir_proc
                          FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                         WHERE trso.pedido_id = soli.pedido_id
                           AND trso.subpedido_id = soli.subpedido_id
                           AND trso.solicitud_id = soli.solicitud_id
                           AND trso.caracteristica_id = 1
                           AND (trso.valor = rg_equipo_comparte.identificador_id OR
                                soli.identificador_id = rg_equipo_comparte.identificador_id)
                           AND trso.tipo_trabajo = 'RETIR'
                           AND soli.estado_id = 'ORDEN';

                        IF v_cant_retir_proc = 1 THEN
                            b_aplica_retir:= TRUE;
                        END IF;
                    END IF;
                END LOOP;
            END IF;
        END IF;

        -- 2014-06-25.  Req 36900. GPON Hogares.  Sección Desaprovisionamiento.
        -- Consideración de tecnologia GPON para verificación de Infraestructura activa compartida.

        IF v_tecnologia_solic IN ('REDCO', 'GPON') THEN

            v_accesp_comparte := fn_comparte_infra_activa (v_solic.identificador_id, v_tecnologia_solic, 'INTER', 'ACCESP', NULL);

            v_insip_comparte := fn_comparte_infra_activa (v_solic.identificador_id, v_tecnologia_solic, 'TELEV', 'INSIP', NULL);

            IF v_accesp_comparte IS NOT NULL AND v_insip_comparte IS NOT NULL THEN
                b_comparte_dos:= TRUE;
            ELSIF v_accesp_comparte IS NOT NULL AND v_insip_comparte IS NULL THEN
                b_comparte_accesp:= TRUE;
            ELSIF  v_accesp_comparte IS NULL AND v_insip_comparte IS NULL THEN
                b_no_comparte_infra:= TRUE;
            END IF;

            IF b_comparte_dos THEN
                -- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
                --                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
                SELECT COUNT (trso.pedido_id)
                  INTO v_cant_retir_proc
                  FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                 WHERE trso.pedido_id = soli.pedido_id
                   AND trso.subpedido_id = soli.subpedido_id
                   AND trso.solicitud_id = soli.solicitud_id
                   AND trso.caracteristica_id = 1
                   AND (trso.valor IN (v_accesp_comparte,v_insip_comparte) OR
                        soli.identificador_id IN (v_accesp_comparte,v_insip_comparte)
                        )
                   AND trso.tipo_trabajo = 'RETIR'
                   AND soli.estado_id = 'ORDEN';

                IF v_cant_retir_proc = 2 THEN
                    v_equipo_retiro:= FN_VERIFICAR_RETIR_EQUIPO('INTER',v_accesp_comparte, v_tecnologia_solic);--> Busco equipo en el ACCESP

                    IF v_equipo_retiro IS NOT NULL THEN
                        b_aplica_retir:= TRUE;
                    END IF;
                END IF;
            ELSIF b_comparte_accesp THEN
                -- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
                --                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
                SELECT COUNT (trso.pedido_id)
                  INTO v_cant_retir_proc
                  FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                 WHERE trso.pedido_id = soli.pedido_id
                   AND trso.subpedido_id = soli.subpedido_id
                   AND trso.solicitud_id = soli.solicitud_id
                   AND trso.caracteristica_id = 1
                   AND (trso.valor = v_accesp_comparte OR
                        soli.identificador_id = v_accesp_comparte
                        )
                   AND trso.tipo_trabajo = 'RETIR'
                   AND soli.estado_id = 'ORDEN';

                IF v_cant_retir_proc = 1 THEN
                    v_equipo_retiro:= FN_VERIFICAR_RETIR_EQUIPO('INTER',v_accesp_comparte, v_tecnologia_solic);--> Busco equipo en el ACCESP

                    IF v_equipo_retiro IS NOT NULL THEN
                        b_aplica_retir:= TRUE;
                    END IF;
                END IF;
            ELSIF b_no_comparte_infra THEN

                OPEN c_datos_identif2 (v_solic.identificador_id);
                FETCH c_datos_identif2 INTO rg_datos_identif2;
                CLOSE c_datos_identif2;

                FOR rg_equipo_comparte IN c_equipo_comparte2 (v_solic.identificador_id, rg_datos_identif2.cliente_id, rg_datos_identif2.pagina_servicio) LOOP

                    IF rg_equipo_comparte.producto_id= 'INTER' THEN
                        BEGIN
                            SELECT EQUIPO_ID, MARCA_ID, REFERENCIA_ID, TIPO_ELEMENTO_ID, TIPO_ELEMENTO
                              INTO v_equipo_retiro, v_marca_id, v_refer_id, v_tipele_id, v_tipo_ele
                              FROM FNX_EQUIPOS
                             WHERE identificador_id = rg_equipo_comparte.identificador_id;
                        EXCEPTION
                            WHEN OTHERS THEN
                                v_equipo_retiro     := NULL;
                                v_marca_id          := NULL;
                                v_refer_id          := NULL;
                                v_tipo_ele          := NULL;
                                v_tipele_id         := NULL;
                        END;

                        IF v_equipo_retiro IS NULL THEN
                            b_aplica_retir:= FALSE;
                            EXIT;
                        ELSE
                            -- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
                            --                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
                            SELECT COUNT (trso.pedido_id)
                              INTO v_cant_retir_proc
                              FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                             WHERE trso.pedido_id = soli.pedido_id
                               AND trso.subpedido_id = soli.subpedido_id
                               AND trso.solicitud_id = soli.solicitud_id
                               AND trso.caracteristica_id = 1
                               AND (trso.valor = rg_equipo_comparte.identificador_id OR
                                    soli.identificador_id = rg_equipo_comparte.identificador_id
                                    )
                               AND trso.tipo_trabajo = 'RETIR'
                               AND soli.estado_id = 'ORDEN';

                            IF v_cant_retir_proc = 1 THEN
                                b_aplica_retir:= TRUE;
                            ELSE
                                b_aplica_retir:= FALSE;
                                EXIT;
                            END IF;
                        END IF;
                    ELSIF rg_equipo_comparte.producto_id= 'TELEV' THEN
                        -- 2014-04-11   Jrendbr     Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los
                        --                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
                        SELECT COUNT (trso.pedido_id)
                          INTO v_cant_retir_proc
                          FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                         WHERE trso.pedido_id = soli.pedido_id
                           AND trso.subpedido_id = soli.subpedido_id
                           AND trso.solicitud_id = soli.solicitud_id
                           AND trso.caracteristica_id = 1
                           AND (trso.valor = rg_equipo_comparte.identificador_id OR
                                soli.identificador_id = rg_equipo_comparte.identificador_id
                                )
                           AND trso.tipo_trabajo = 'RETIR'
                           AND soli.estado_id = 'ORDEN';

                        IF v_cant_retir_proc = 1 THEN
                            b_aplica_retir:= TRUE;
                        ELSE
                            b_aplica_retir:= FALSE;
                            EXIT;
                        END IF;

                    END IF;
                END LOOP;
            END IF;
        END IF;


        IF b_aplica_retir THEN
            --2014-02-24    Jrendbr Se adiciona logica para que tenga en cuenta los esatod de los equipos NIR.
            BEGIN
                SELECT MARCA_ID, REFERENCIA_ID, TIPO_ELEMENTO_ID, TIPO_ELEMENTO, DECODE(TIPO_ELEMENTO_ID, 'CPE', 'EQACCP', 'CPEWIM', 'EQACCP', 'CABLEM', 'EQACCP', 'EQURED')
                  INTO v_marca_id, v_refer_id, v_tipele_id, v_tipo_ele, v_tipele_id_sol
                  FROM FNX_EQUIPOS
                 WHERE EQUIPO_ID = v_equipo_retiro;
            EXCEPTION
                WHEN OTHERS THEN
                    v_marca_id  := NULL;
                    v_refer_id  := NULL;
                    v_tipo_ele  := NULL;
                    v_tipele_id := NULL;
                    v_tipele_id_sol := NULL;
            END;

            v_equipo_obsoleto := fn_consultar_equipo_obsoleto (v_marca_id, v_refer_id, v_tipele_id, v_tipo_ele);

            IF v_equipo_obsoleto = 'N' THEN

                --2014-02-24    Jrendbr Se adiciona logica para que tenga en cuenta los esatod de los equipos NIR.
                v_equipo_nir := fn_consultar_equipo_nir (v_marca_id, v_refer_id, v_tipele_id, v_tipo_ele);

                IF  v_equipo_nir = 'S' THEN
                    v_equip_obsol := '; Equipo NIR: Marca: ' || v_marca_id || ', Referencia: ' || v_refer_id || ', Serial: '||v_equipo_retiro|| '';
                ELSE
                    v_equip_obsol := '; Equipo a recuperar: Marca: ' || v_marca_id || ', Referencia: ' || v_refer_id || ', Serial: '||v_equipo_retiro|| '';
                END IF;
            ELSE
                v_equip_obsol := '; EQUIPO OBSOLETO: Marca: ' || v_marca_id || ', Referencia: ' || v_refer_id || ', Serial: '||v_equipo_retiro|| '';
            END IF;

            v_solicitud_eqaccp := NULL;
            --2014-04-21    Jrendbr Se adiciona logica para que sea enviado el identifcador con el que comparte infraestructura
            IF v_accesp_comparte IS NOT NULL THEN
                identificador_comparte := v_accesp_comparte;
            ELSE
                identificador_comparte := v_insip_comparte ;
            END IF;

           --2014-02-24    Jrendbr Se adiciona logica para que tenga en cuenta los esatod de los equipos NIR.
            IF v_marca_id IS NOT NULL THEN
            PR_SOLICITUD_RETIR_EQUIPO (v_solic.pedido_id,   v_solic.subpedido_id,       v_solicitud_eqaccp,   v_solic.servicio_id,
                                     v_solic.producto_id,   v_tipele_id_sol,            'EQU',                v_tecnologia_solic,
                                     v_cliente,             identificador_comparte ,   v_solic.municipio_id, v_solic.empresa_id,
                                     v_equipo_retiro,       'S',                        p_mensaje);
            END IF;

            IF p_mensaje IS NULL THEN
                IF v_solicitud_eqaccp IS NOT NULL THEN

                    -- 2014-03-26 dgirall  Se envia por parametro el concepto de la nueva solicitud (PINSC o PORDE)
                    IF NVL(v_pedido_acp_pdte,'-') <> '-' THEN
                        pri_paso_orden_porde (v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_eqaccp, 'PINSC', v_equip_obsol, v_tecnologia_solic, p_mensaje);
                    ELSE
                        pri_paso_orden_porde (v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_eqaccp, 'PORDE', v_equip_obsol, v_tecnologia_solic, p_mensaje);
                    END IF;

                END IF;

                v_solicitud_equred := v_solicitud_eqaccp + 1;

                IF p_mensaje IS NOT NULL THEN
                    w_incons := FALSE;
                END IF;
            ELSE
                w_incons := FALSE;
            END IF;
        END IF;
        -- 2013-10-03   Yhernana    Fin

    END IF;
    /**** dojedac 16/03/2011 DESAPROVISIONAMIENTO *********************************/

    IF p_mensaje IS NOT NULL THEN
      w_incons        := TRUE;
      w_mensaje_error := p_mensaje;
    END IF;

    IF NOT w_incons THEN
      IF NOT b_provision_automatica THEN
         w_resp := 'S';
      ELSE
         w_resp := 'AA';
      END IF;
    ELSE
      w_resp := SUBSTR (w_mensaje_error, 1, 50);
    END IF;

EXCEPTION
     WHEN e_no_ejecutar_estudio THEN
           w_resp := 'S';
           DBMS_OUTPUT.put_line('<PR_ESTUDIO_TECNICO_TELEF_IP>' );
           DBMS_OUTPUT.put_line('<Estudio Tecnico Telefonia IP Deshabilitado>.  Parametro ');
     WHEN OTHERS THEN
           w_resp := 'S';
           DBMS_OUTPUT.put_line('<PR_ESTUDIO_TECNICO_TELEF_IP> Error' );
           DBMS_OUTPUT.put_line(SUBSTR(SQLERRM, 1, 150));
END PR_ESTUDIO_TECNICO_TELEF_IP;