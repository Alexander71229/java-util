FUNCTION       fn_comparte_infra_activa (
   p_identificador       IN   fnx_identificadores.identificador_id%TYPE,
   p_tecnologia          IN   fnx_solicitudes.tecnologia_id%TYPE,
   p_producto            IN   fnx_solicitudes.producto_id%TYPE,
   p_tipo_elemento_id    IN   fnx_solicitudes.tipo_elemento_id%TYPE,
   p_identif_tentativo   IN   fnx_identificadores.identificador_id%TYPE
)
   RETURN VARCHAR2
IS
-- Organizacion     UNE EPM Telecomunicaciones S.A.
-- Descripcion:     Función para retornar el identificador con que se comparte infraestructura activa,
--                  dependiendo del producto y tipo_elemento_id.
-- Construido por:  Luis Alberto Gonzalez G.. - MVM Ingenieria de Software

   -- HISTORIA
-- Fecha        Autor       Observaciones
-- -----------  ----------- ------------------------------------------------------
-- 2011-01-24   Lgonzalg    Construcción inicial
-- 2014-06-25   Jrincong    Req 36900 GPON Hogares.  Cursor c_inf_gpon_activas para buscar la red GPON activa de un identificador
-- 2014-06-25   Jrincong    Req 36900 GPON Hogares.  Cursor c_inf_gpon_activa_otrosid para buscar los identificadores asociados 
--                          a los elementos de red GPON.
-- 2014-06-25   Jrincong    Req 36900 GPON Hogares. Adición de tecnologia GPON para verificar infraestructura compartida activa.


   CURSOR c_inf_tv_activas (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT identificador_id, centro_distribucion_intermedia,
             nodo_optico_electrico_id, tipo_amplificador_id, amplificador_id,
             tipo_derivador_id, derivador_id, terminal_derivador_id
        FROM fnx_inf_tv_activas
       WHERE identificador_id = pc_identificador;

   CURSOR c_inf_tv_tramites (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT identificador_id, centro_distribucion_intermedia,
             nodo_optico_electrico_id, tipo_amplificador_id, amplificador_id,
             tipo_derivador_id, derivador_id, terminal_derivador_id
        FROM fnx_inf_tv_tramites
       WHERE identificador_id = pc_identificador;

   v_inf_tv_tramite              c_inf_tv_tramites%ROWTYPE;

   CURSOR c_inf_tv_activas_red (
      pc_identificador               IN   fnx_identificadores.identificador_id%TYPE,
      pc_centro_distribucion_inter   IN   fnx_inf_tv_activas.centro_distribucion_intermedia%TYPE,
      pc_nodo_optico_electrico_id    IN   fnx_inf_tv_activas.nodo_optico_electrico_id%TYPE,
      pc_amplificador_id             IN   fnx_inf_tv_activas.amplificador_id%TYPE,
      pc_tipo_amplificador_id        IN   fnx_inf_tv_activas.tipo_amplificador_id%TYPE,
      pc_tipo_derivador_id           IN   fnx_inf_tv_activas.tipo_derivador_id%TYPE,
      pc_derivador_id                IN   fnx_inf_tv_activas.derivador_id%TYPE,
      pc_terminal_derivador_id       IN   fnx_inf_tv_activas.terminal_derivador_id%TYPE
   )
   IS
      SELECT tv.identificador_id, i.estado, i.producto_id,
             i.tipo_elemento_id
        FROM fnx_inf_tv_activas tv, fnx_identificadores i
       WHERE tv.identificador_id = i.identificador_id
         AND tv.centro_distribucion_intermedia = pc_centro_distribucion_inter
         AND tv.nodo_optico_electrico_id = pc_nodo_optico_electrico_id
         AND tv.tipo_amplificador_id = pc_tipo_amplificador_id
         AND tv.amplificador_id = pc_amplificador_id
         AND tv.tipo_derivador_id = pc_tipo_derivador_id
         AND tv.derivador_id = pc_derivador_id
         AND tv.terminal_derivador_id = pc_terminal_derivador_id
         AND tv.identificador_id <> pc_identificador;

   v_inf_tv_activas              c_inf_tv_activas%ROWTYPE;

   CURSOR c_inf_tv_tramites_red (
      pc_identificador               IN   fnx_identificadores.identificador_id%TYPE,
      pc_centro_distribucion_inter   IN   fnx_inf_tv_activas.centro_distribucion_intermedia%TYPE,
      pc_nodo_optico_electrico_id    IN   fnx_inf_tv_activas.nodo_optico_electrico_id%TYPE,
      pc_amplificador_id             IN   fnx_inf_tv_activas.amplificador_id%TYPE,
      pc_tipo_amplificador_id        IN   fnx_inf_tv_activas.tipo_amplificador_id%TYPE,
      pc_tipo_derivador_id           IN   fnx_inf_tv_activas.tipo_derivador_id%TYPE,
      pc_derivador_id                IN   fnx_inf_tv_activas.derivador_id%TYPE,
      pc_terminal_derivador_id       IN   fnx_inf_tv_activas.terminal_derivador_id%TYPE
   )
   IS
      SELECT tv.identificador_id, i.estado, i.producto_id, i.tipo_elemento_id
        FROM fnx_inf_tv_tramites tv, fnx_identificadores i
       WHERE tv.identificador_id = i.identificador_id
         AND tv.centro_distribucion_intermedia = pc_centro_distribucion_inter
         AND tv.nodo_optico_electrico_id = pc_nodo_optico_electrico_id
         AND tv.tipo_amplificador_id = pc_tipo_amplificador_id
         AND tv.amplificador_id = pc_amplificador_id
         AND tv.tipo_derivador_id = pc_tipo_derivador_id
         AND tv.derivador_id = pc_derivador_id
         AND tv.terminal_derivador_id = pc_terminal_derivador_id
         AND tv.identificador_id <> pc_identificador;

   CURSOR c_inf_redes_activas (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT nodo_conmutador_id, armario_id, strip_id, par_primario_id,
             caja_id, par_secundario_id
        FROM fnx_inf_redes_activas
       WHERE identificador_id = pc_identificador;

   v_inf_redes_activas           c_inf_redes_activas%ROWTYPE;

   CURSOR c_inf_redes_directas (
      pc_identificador        IN   fnx_identificadores.identificador_id%TYPE,
      pc_nodo_conmutador_id   IN   fnx_inf_redes_activas.nodo_conmutador_id%TYPE,
      pc_armario_id           IN   fnx_inf_redes_activas.armario_id%TYPE,
      pc_strip_id             IN   fnx_inf_redes_activas.strip_id%TYPE,
      pc_par_primario_id      IN   fnx_inf_redes_activas.par_primario_id%TYPE
   )
   IS
      SELECT ac.identificador_id, i.estado, i.producto_id, i.tipo_elemento_id
        FROM fnx_inf_redes_activas ac, fnx_identificadores i
       WHERE ac.identificador_id = i.identificador_id
         AND ac.nodo_conmutador_id = pc_nodo_conmutador_id
         AND ac.armario_id = pc_armario_id
         AND NVL (ac.strip_id, 'N') = pc_strip_id
         AND NVL (ac.par_primario_id, -1) = pc_par_primario_id
         AND ac.identificador_id <> pc_identificador;

   CURSOR c_inf_redes_secundarias (
      pc_identificador       IN   fnx_identificadores.identificador_id%TYPE,
      pc_armario_id          IN   fnx_inf_redes_activas.armario_id%TYPE,
      pc_caja_id             IN   fnx_inf_redes_activas.caja_id%TYPE,
      pc_par_secundario_id   IN   fnx_inf_redes_activas.par_secundario_id%TYPE
   )
   IS
      SELECT ac.identificador_id, i.estado, i.producto_id, i.tipo_elemento_id
        FROM fnx_inf_redes_activas ac, fnx_identificadores i
       WHERE ac.identificador_id = i.identificador_id
         AND ac.armario_id = pc_armario_id
         AND NVL (ac.caja_id, -1) = pc_caja_id
         AND NVL (ac.par_secundario_id, -1) = pc_par_secundario_id
         AND ac.identificador_id <> pc_identificador;

   CURSOR c_inf_redes_completa (
      pc_identificador        IN   fnx_identificadores.identificador_id%TYPE,
      pc_nodo_conmutador_id   IN   fnx_inf_redes_activas.nodo_conmutador_id%TYPE,
      pc_armario_id           IN   fnx_inf_redes_activas.armario_id%TYPE,
      pc_strip_id             IN   fnx_inf_redes_activas.strip_id%TYPE,
      pc_par_primario_id      IN   fnx_inf_redes_activas.par_primario_id%TYPE,
      pc_caja_id              IN   fnx_inf_redes_activas.caja_id%TYPE,
      pc_par_secundario_id    IN   fnx_inf_redes_activas.par_secundario_id%TYPE
   )
   IS
      SELECT ac.identificador_id, i.estado, i.producto_id, i.tipo_elemento_id
        FROM fnx_inf_redes_activas ac, fnx_identificadores i
       WHERE ac.identificador_id = i.identificador_id
         AND ac.nodo_conmutador_id = pc_nodo_conmutador_id
         AND ac.armario_id = pc_armario_id
         AND NVL (ac.strip_id, 'N') = pc_strip_id
         AND NVL (ac.par_primario_id, -1) = pc_par_primario_id
         AND NVL (ac.caja_id, -1) = pc_caja_id
         AND NVL (ac.par_secundario_id, -1) = pc_par_secundario_id
         AND ac.identificador_id <> pc_identificador;

   CURSOR c_inf_eqmul_activas (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT tipo_tarjeta_dato_gdc, tarjeta_dato_gdc,
             circuito_tarjeta_dato_gdc
        FROM fnx_inf_eqmul_activas
       WHERE identificador_id = pc_identificador;

   v_inf_eqmul_activas           c_inf_eqmul_activas%ROWTYPE;

   CURSOR c_inf_eqmul_redes (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE,
      pc_tipo_tarjeta    IN   fnx_inf_eqmul_activas.tipo_tarjeta_dato_gdc%TYPE,
      pc_tarjeta         IN   fnx_inf_eqmul_activas.tarjeta_dato_gdc%TYPE,
      pc_circuito        IN   fnx_inf_eqmul_activas.circuito_tarjeta_dato_gdc%TYPE
   )
   IS
      SELECT ac.identificador_id, i.estado, i.producto_id, i.tipo_elemento_id
        FROM fnx_inf_eqmul_activas ac, fnx_identificadores i
       WHERE ac.identificador_id = i.identificador_id
         AND ac.tipo_tarjeta_dato_gdc = pc_tipo_tarjeta
         AND ac.tarjeta_dato_gdc = pc_tarjeta
         AND ac.circuito_tarjeta_dato_gdc = pc_circuito
         AND ac.identificador_id <> pc_identificador;

   v_identificador_comparte      fnx_identificadores.identificador_id%TYPE
                                                                       := NULL;
   b_encontre_tentativo          BOOLEAN := FALSE;
   b_tiene_secundaria            BOOLEAN := FALSE;
   b_tiene_primaria              BOOLEAN := FALSE;
   b_tiene_circuito              BOOLEAN := FALSE;

   -- Cursor c_inf_gpon_activas para buscar la red GPON activa de un identificador
   CURSOR c_inf_gpon_activa
   (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE
   )
   IS
      SELECT identificador_id, olt_id, armario_id, municipio_id, splitter_id,
             nap_id, hilo_id, tarjeta_id, puerto_id, pto_logico
        FROM fnx_inf_gpon_activas
       WHERE identificador_id = pc_identificador;

   -- Cursor c_inf_gpon_activa_otrosid para buscar los identificadores asociados a los elementos de red GPON.
   -- Los identificadores deben ser diferentes al identificador que se pasa como parámetro.   
   CURSOR c_inf_gpon_activa_otrosid  
     (
      pc_identificador   IN   fnx_identificadores.identificador_id%TYPE,
      pc_olt_id          IN   fnx_inf_gpon_activas.olt_id%TYPE,
      pc_armario_id      IN   fnx_inf_gpon_activas.armario_id%TYPE,
      pc_municipio_id    IN   fnx_inf_gpon_activas.municipio_id%TYPE,
      pc_splitter_id     IN   fnx_inf_gpon_activas.splitter_id%TYPE,
      pc_nap_id          IN   fnx_inf_gpon_activas.nap_id%TYPE,
      pc_hilo_id         IN   fnx_inf_gpon_activas.hilo_id%TYPE,
      pc_tarjeta_id      IN   fnx_inf_gpon_activas.tarjeta_id%TYPE,
      pc_puerto_id       IN   fnx_inf_gpon_activas.puerto_id%TYPE,
      pc_pto_logico      IN   fnx_inf_gpon_activas.pto_logico%TYPE
   )
   IS
      SELECT ac.identificador_id, ide.estado, ide.producto_id, ide.tipo_elemento_id
        FROM fnx_inf_gpon_activas ac,
             fnx_identificadores ide
       WHERE ac.identificador_id = ide.identificador_id
         AND ac.olt_id = pc_olt_id
         AND ac.armario_id = pc_armario_id
         AND ac.municipio_id = pc_municipio_id
         AND ac.splitter_id = pc_splitter_id
         AND ac.nap_id = pc_nap_id
         AND ac.hilo_id = pc_hilo_id
         AND ac.tarjeta_id = pc_tarjeta_id
         AND ac.puerto_id = pc_puerto_id
         AND ac.pto_logico = pc_pto_logico
         AND ac.identificador_id <> pc_identificador;

    v_inf_gpon_activa            c_inf_gpon_activa%ROWTYPE;


BEGIN

   IF p_tecnologia = 'REDCO' THEN
      -- Bùsqueda de la red asociada al identificador p_identificador
      OPEN c_inf_redes_activas (p_identificador);

      FETCH c_inf_redes_activas
       INTO v_inf_redes_activas;

      CLOSE c_inf_redes_activas;

      -- Bùsqueda del circuito al identificador p_identificador
      OPEN c_inf_eqmul_activas (p_identificador);

      FETCH c_inf_eqmul_activas
       INTO v_inf_eqmul_activas;

      CLOSE c_inf_eqmul_activas;

      IF v_inf_redes_activas.par_primario_id IS NOT NULL THEN
         b_tiene_primaria := TRUE;
      END IF;

      IF v_inf_redes_activas.par_secundario_id IS NOT NULL THEN
         b_tiene_secundaria := TRUE;
      END IF;

      IF v_inf_eqmul_activas.circuito_tarjeta_dato_gdc IS NOT NULL THEN
         b_tiene_circuito := TRUE;
      END IF;

      IF b_tiene_primaria AND b_tiene_secundaria THEN
         -- Buscar los otros identificadores que puedan estar instalados en la misma red
         -- que el identificador p_identificador
         FOR reg_activas IN
            c_inf_redes_completa (p_identificador,
                                  v_inf_redes_activas.nodo_conmutador_id,
                                  v_inf_redes_activas.armario_id,
                                  v_inf_redes_activas.strip_id,
                                  v_inf_redes_activas.par_primario_id,
                                  v_inf_redes_activas.caja_id,
                                  v_inf_redes_activas.par_secundario_id
                                 )
         LOOP
            IF     reg_activas.producto_id = p_producto
               AND reg_activas.tipo_elemento_id = p_tipo_elemento_id
               AND reg_activas.estado IN ('OCU') THEN
               v_identificador_comparte := reg_activas.identificador_id;

               -- Si el identificador encontrado es el mismo que el identificador tentativo, se sale del ciclo para retornar
               -- este identificador.
               IF reg_activas.identificador_id =
                                              NVL (p_identif_tentativo, 'NO') THEN
                  b_encontre_tentativo := TRUE;
                  EXIT;
               END IF;
            END IF;
         END LOOP;
      END IF;

      IF NOT b_encontre_tentativo AND b_tiene_primaria THEN
         -- Buscar los otros identificadores que puedan estar instalados en la misma red primaria
         -- que el identificador p_identificador
         FOR reg_activas IN
            c_inf_redes_directas (p_identificador,
                                  v_inf_redes_activas.nodo_conmutador_id,
                                  v_inf_redes_activas.armario_id,
                                  v_inf_redes_activas.strip_id,
                                  v_inf_redes_activas.par_primario_id
                                 )
         LOOP
            IF     reg_activas.producto_id = p_producto
               AND reg_activas.tipo_elemento_id = p_tipo_elemento_id
               AND reg_activas.estado IN ('OCU') THEN
               v_identificador_comparte := reg_activas.identificador_id;

               -- Si el identificador encontrado es el mismo que el identificador tentativo, se sale del ciclo para retornar
               -- este identificador.
               IF reg_activas.identificador_id =
                                              NVL (p_identif_tentativo, 'NO') THEN
                  b_encontre_tentativo := TRUE;
                  EXIT;
               END IF;
            END IF;
         END LOOP;
      END IF;

      IF NOT b_encontre_tentativo AND b_tiene_secundaria THEN
         -- Buscar los otros identificadores que puedan estar instalados en la misma red secundaria
         -- que el identificador p_identificador
         FOR reg_activas IN
            c_inf_redes_secundarias (p_identificador,
                                     v_inf_redes_activas.armario_id,
                                     v_inf_redes_activas.caja_id,
                                     v_inf_redes_activas.par_secundario_id
                                    )
         LOOP
            IF     reg_activas.producto_id = p_producto
               AND reg_activas.tipo_elemento_id = p_tipo_elemento_id
               AND reg_activas.estado IN ('OCU') THEN
               v_identificador_comparte := reg_activas.identificador_id;

               -- Si el identificador encontrado es el mismo que el identificador tentativo, se sale del ciclo para retornar
               -- este identificador.
               IF reg_activas.identificador_id =
                                              NVL (p_identif_tentativo, 'NO') THEN
                  b_encontre_tentativo := TRUE;
                  EXIT;
               END IF;
            END IF;
         END LOOP;
      END IF;

      IF NOT b_encontre_tentativo AND b_tiene_circuito THEN
         -- Buscar los otros identificadores que puedan estar instalados en el mismo circuito
         -- que el identificador p_identificador
         FOR reg_activas IN
            c_inf_eqmul_redes (p_identificador,
                               v_inf_eqmul_activas.tipo_tarjeta_dato_gdc,
                               v_inf_eqmul_activas.tarjeta_dato_gdc,
                               v_inf_eqmul_activas.circuito_tarjeta_dato_gdc
                              )
         LOOP
            IF     reg_activas.producto_id = p_producto
               AND reg_activas.tipo_elemento_id = p_tipo_elemento_id
               AND reg_activas.estado IN ('OCU') THEN
               v_identificador_comparte := reg_activas.identificador_id;

               -- Si el identificador encontrado es el mismo que el identificador tentativo, se sale del ciclo para retornar
               -- este identificador.
               IF reg_activas.identificador_id =
                                              NVL (p_identif_tentativo, 'NO') THEN
                  b_encontre_tentativo := TRUE;
                  EXIT;
               END IF;
            END IF;
         END LOOP;
      END IF;
   END IF;

   -- Verificaciones para tecnologia HFC
   IF p_tecnologia = 'HFC' THEN
      -- Bùsqueda de la red HFC activa asociada al identificador p_identificador
      OPEN c_inf_tv_activas (p_identificador);

      FETCH c_inf_tv_activas
       INTO v_inf_tv_activas;

      CLOSE c_inf_tv_activas;

      -- Buscar los otros identificadores que puedan estar instalados en la misma red HFC
      -- que el identificador p_identificador1
      FOR reg_activas IN
         c_inf_tv_activas_red
                            (p_identificador,
                             v_inf_tv_activas.centro_distribucion_intermedia,
                             v_inf_tv_activas.nodo_optico_electrico_id,
                             v_inf_tv_activas.amplificador_id,
                             v_inf_tv_activas.tipo_amplificador_id,
                             v_inf_tv_activas.tipo_derivador_id,
                             v_inf_tv_activas.derivador_id,
                             v_inf_tv_activas.terminal_derivador_id
                            )
      LOOP
         IF     reg_activas.producto_id = p_producto
            AND reg_activas.tipo_elemento_id = p_tipo_elemento_id
            AND reg_activas.estado IN ('OCU') THEN
            v_identificador_comparte := reg_activas.identificador_id;

            -- Si el identificador encontrado es el mismo por el q se pregunta
            -- en los parametros de entrada, se sale del ciclo para retornar
            -- este identificador.
            IF reg_activas.identificador_id = NVL (p_identif_tentativo, 'NO') THEN
               EXIT;
            END IF;
         END IF;
      END LOOP;

      IF v_identificador_comparte IS NULL THEN
         -- Bùsqueda de la red HFC activa asociada al identificador p_identificador
         OPEN c_inf_tv_tramites (p_identificador);

         FETCH c_inf_tv_tramites
          INTO v_inf_tv_tramite;

         CLOSE c_inf_tv_tramites;

         -- Buscar los otros identificadores que puedan estar instalados en la misma red HFC
         -- que el identificador p_identificador1
         FOR reg_activas IN
            c_inf_tv_tramites_red
                            (p_identificador,
                             v_inf_tv_activas.centro_distribucion_intermedia,
                             v_inf_tv_activas.nodo_optico_electrico_id,
                             v_inf_tv_activas.amplificador_id,
                             v_inf_tv_activas.tipo_amplificador_id,
                             v_inf_tv_activas.tipo_derivador_id,
                             v_inf_tv_activas.derivador_id,
                             v_inf_tv_activas.terminal_derivador_id
                            )
         LOOP
            IF     reg_activas.producto_id = p_producto
               AND reg_activas.tipo_elemento_id = p_tipo_elemento_id     /*AND
                       reg_activas.estado IN ('OCU') */
                                                                    THEN
               v_identificador_comparte := reg_activas.identificador_id;

               -- Si el identificador encontrado es el mismo que el identificador tentativo, se sale del ciclo para retornar
               -- este identificador.
               IF reg_activas.identificador_id =
                                              NVL (p_identif_tentativo, 'NO') THEN
                  EXIT;
               END IF;
            END IF;
         END LOOP;
      END IF;
   END IF;

   -- 2014-06-25.  Req 36900 GPON Hogares.
   -- Adición de tecnologia GPON para verificar infraestructura compartida activa.

   IF p_tecnologia = 'GPON' THEN 

      OPEN c_inf_gpon_activa (p_identificador);

      FETCH c_inf_gpon_activa
       INTO v_inf_gpon_activa;

      CLOSE c_inf_gpon_activa;

      FOR reg_activas IN
         c_inf_gpon_activa_otrosid (p_identificador,
                                   v_inf_gpon_activa.olt_id,
                                   v_inf_gpon_activa.armario_id,
                                   v_inf_gpon_activa.municipio_id,
                                   v_inf_gpon_activa.splitter_id,
                                   v_inf_gpon_activa.nap_id,
                                   v_inf_gpon_activa.hilo_id,
                                   v_inf_gpon_activa.tarjeta_id,
                                   v_inf_gpon_activa.puerto_id,
                                   v_inf_gpon_activa.pto_logico
                                  )
     LOOP

            IF     reg_activas.producto_id = p_producto
               AND reg_activas.tipo_elemento_id = p_tipo_elemento_id 
               AND reg_activas.estado = 'OCU' 
            THEN

               v_identificador_comparte := reg_activas.identificador_id;

               -- Si el identificador encontrado es el mismo que el identificador tentativo, se sale del ciclo para retornar
               -- este identificador.
               IF reg_activas.identificador_id = NVL (p_identif_tentativo, 'NO') THEN
                  EXIT;
               END IF;
            END IF;

     END LOOP;

   END IF;

   RETURN v_identificador_comparte;

EXCEPTION
   WHEN OTHERS THEN
      RETURN v_identificador_comparte;
END; 