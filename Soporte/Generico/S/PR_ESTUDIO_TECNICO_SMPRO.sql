PROCEDURE       PR_ESTUDIO_TECNICO_SMPRO
(
  p_pedido                      IN  FNX_PEDIDOS.pedido_id%TYPE,
  p_mensaje                     OUT VARCHAR2
)
IS
-- Descripcion:             Proc. para realizar el estudio tecnico de solicitudes en SMPRO
-- Organizacion:            EPM Telecomunicaciones UNE

-- HISTORIA DE MODIFICACIONES
-- Fecha        Persona      Observaciones
-- ----------   ----------- ------------------------------------------------------
-- 2013-10-24   Jrendbr    Creacion inicial del procedimiento con base en


--buscando en trabajos el cambio en la caracteristica 124
CURSOR c_cambio_vel (p_pedido     FNX_PEDIDOS.pedido_id%TYPE)
IS
SELECT PEDIDO_ID, SUBPEDIDO_ID, SOLICITUD_ID
  FROM fnx_trabajos_solicitudes
 WHERE PEDIDO_ID         = p_pedido
   AND SERVICIO_ID       = 'DATOS'
   AND PRODUCTO_ID       = 'INTER'
   AND TIPO_ELEMENTO_ID  = 'ACCESP'
   AND TIPO_ELEMENTO     = 'CMP'
   AND CARACTERISTICA_ID = 124
   AND TIPO_TRABAJO      = 'CAMBI';

-- buscando la tecnologia del identificador del componente INTCON en el portafolio
CURSOR c_ident_intcon(p_pedido     FNX_PEDIDOS.pedido_id%TYPE)
IS
SELECT SOL.IDENTIFICADOR_ID
  FROM FNX_SOLICITUDES SOL
 WHERE SOL.PEDIDO_ID        = p_pedido
   AND SOL.SERVICIO_ID      = 'DATOS'
   AND SOL.PRODUCTO_ID      = 'INTER'
   AND SOL.TIPO_ELEMENTO_ID = 'INTCON'
   AND SOL.TIPO_ELEMENTO    = 'CMP';

-- Se busca la tecnologia
CURSOR c_tecno_ident_intcon(p_ident  FNX_IDENTIFICADORES.identificador_id%TYPE)
IS
SELECT IDENTIFICADOR_ID
  FROM FNX_CONFIGURACIONES_IDENTIF 
 WHERE IDENTIFICADOR_ID  = p_ident
   AND CARACTERISTICA_ID = 33
   AND VALOR             = 'REDCO';


--buscar en FNX_TRABAJOS_SOLICITUDES la adicion de la caracteristica 200
CURSOR c_adicion_deco (p_pedido  FNX_PEDIDOS.pedido_id%TYPE)
IS
SELECT PEDIDO_ID, SUBPEDIDO_ID, SOLICITUD_ID
  FROM FNX_TRABAJOS_SOLICITUDES TRSOL
 WHERE TRSOL.PEDIDO_ID         = p_pedido
   AND TRSOL.SERVICIO_ID       = 'ENTRE'
   AND TRSOL.PRODUCTO_ID       = 'TELEV'
   AND TRSOL.TIPO_ELEMENTO_ID  = 'EQURED'
   AND TRSOL.TIPO_ELEMENTO     = 'EQU'
   AND TRSOL.CARACTERISTICA_ID = 200
   AND TRSOL.TIPO_TRABAJO      = 'NUEVO';

--Evaluar la tecnologia, si es adicion de decodificador
CURSOR c_tecnologia (p_pedido     FNX_PEDIDOS.pedido_id%TYPE,
                     p_subpedido  FNX_SUBPEDIDOS.subpedido_id%TYPE,
                     p_solicitud  FNX_SOLICITUDES.solicitud_id%TYPE)
IS
SELECT COUNT(1) contador
  FROM FNX_CARACTERISTICA_SOLICITUDES CARSOL
 WHERE CARSOL.PEDIDO_ID         = p_pedido
   AND CARSOL.SUBPEDIDO_ID      = p_subpedido
   AND CARSOL.SOLICITUD_ID      = p_solicitud
   AND CARSOL.CARACTERISTICA_ID = 33
   AND CARSOL.VALOR             = 'REDCO';


--Trae la velocidad
CURSOR c_velocidad (p_pedido     FNX_PEDIDOS.pedido_id%TYPE,
                    p_subpedido  FNX_SUBPEDIDOS.subpedido_id%TYPE,
                    p_solicitud  FNX_SOLICITUDES.solicitud_id%TYPE)
IS
SELECT valor
  FROM FNX_CARACTERISTICA_SOLICITUDES 
 WHERE PEDIDO_ID         = p_pedido
   AND SUBPEDIDO_ID      = p_subpedido
   AND SOLICITUD_ID      = p_solicitud
   AND CARACTERISTICA_ID = 124;

v_velocidad     fnx_caracteristica_solicitudes.valor%TYPE;

--Cuenta los decos
CURSOR c_decos (p_pedido     FNX_PEDIDOS.pedido_id%TYPE,
                p_subpedido  FNX_SUBPEDIDOS.subpedido_id%TYPE,
                p_solicitud  FNX_SOLICITUDES.solicitud_id%TYPE)
IS
SELECT valor
  FROM FNX_CARACTERISTICA_SOLICITUDES 
 WHERE PEDIDO_ID         = p_pedido
   AND SUBPEDIDO_ID      = p_subpedido
   AND SOLICITUD_ID      = p_solicitud
   AND CARACTERISTICA_ID = 1067;

v_valor_deco    VARCHAR2(10);
v_cantidad_sd   VARCHAR2(10);
v_cantidad_hd   VARCHAR2(10);

--Cuenta si tiene paquetes HD, ya que se pueden entregar equipos HD pero para usar de manera estandar, sin canalaes HD
-- y se deben contar como HD si tiene canales HD, se debe contar como estandar si no tiene canales HD
CURSOR c_canales_hd(p_pedido     FNX_PEDIDOS.pedido_id%TYPE)
IS
SELECT COUNT(1)
  FROM fnx_trabajos_solicitudes TRSOL,
       fnx_tipos_caracteristicas TCAR
 WHERE TRSOL.PEDIDO_ID = p_pedido
   AND TRSOL.TIPO_TRABAJO = 'NUEVO'
   AND TCAR.CARACTERISTICA_ID = TRSOL.CARACTERISTICA_ID
   AND TCAR.INDICADOR_TIPO = 'CANAL'
   AND TCAR.TIPO_SESP = 'HD';

v_canales_hd   NUMBER(3);



-- Declaracion de variables

v_cambio_vel                   c_cambio_vel%ROWTYPE;
v_ident_intcon                 c_ident_intcon%ROWTYPE;
v_tecno_ident_intcon           c_tecno_ident_intcon%ROWTYPE;
v_adicion_deco                 c_adicion_deco%ROWTYPE;
v_tecnologia                   c_tecnologia%ROWTYPE;
v_ident_telev                  fnx_identificadores.identificador_id%TYPE;
v_ident_inter                  fnx_identificadores.identificador_id%TYPE;
v_pedido                       fnx_transac_smpro.pedido_id%TYPE;
v_respuesta                    fnx_transac_smpro.respuesta%TYPE;

BEGIN

    OPEN  c_cambio_vel (p_pedido );
    FETCH c_cambio_vel INTO v_cambio_vel;
    CLOSE c_cambio_vel;
DBMS_OUTPUT.put_line ('* v_cambio_vel.pedido_id '||NVL(v_cambio_vel.pedido_id,'X'));
    IF NVL(v_cambio_vel.pedido_id,'X') != 'X' THEN
        
        OPEN  c_ident_intcon (v_cambio_vel.pedido_id );
        FETCH c_ident_intcon INTO v_ident_intcon;
        CLOSE c_ident_intcon;
DBMS_OUTPUT.put_line ('** v_ident_intcon.identificador_id '||v_ident_intcon.identificador_id);
        IF v_ident_intcon.identificador_id is not null THEN
            OPEN  c_tecno_ident_intcon (v_ident_intcon.identificador_id );
            FETCH c_tecno_ident_intcon INTO v_tecno_ident_intcon;
            CLOSE c_tecno_ident_intcon;
DBMS_OUTPUT.put_line ('*** v_tecno_ident_intcon.identificador_id' ||v_tecno_ident_intcon.identificador_id);            
            IF v_tecno_ident_intcon.identificador_id is not null THEN

                v_velocidad := NULL;
                OPEN  c_velocidad (v_cambio_vel.pedido_id, v_cambio_vel.subpedido_id , v_cambio_vel.solicitud_id  );
                FETCH c_velocidad INTO v_velocidad;
                CLOSE c_velocidad;
                
                v_ident_inter := REPLACE(v_tecno_ident_intcon.identificador_id,'-IC001','');
                
                
                BEGIN
                    SELECT PEDIDO_ID,RESPUESTA
                    INTO v_pedido, v_respuesta
                    FROM FNX_TRANSAC_SMPRO
                    WHERE PEDIDO_ID = v_cambio_vel.pedido_id and
                    SUBPEDIDO_ID = v_cambio_vel.subpedido_id and
                    SOLICITUD_ID = v_cambio_vel.solicitud_id;
                EXCEPTION
                      WHEN OTHERS
                      THEN
                         NULL;
                END;
DBMS_OUTPUT.put_line ('**** v_pedido' ||v_pedido||' - '||v_respuesta);                      
                IF v_pedido is null THEN

                    BEGIN
                        INSERT INTO FENIX.FNX_TRANSAC_SMPRO (
                           PEDIDO_ID, SUBPEDIDO_ID, SOLICITUD_ID,
                           IDENTIFICADOR_ID, FECHA, PROCESADO,
                            TIPO_TRANSACCION, VELOCIDAD, CANTIDAD_SD, CANTIDAD_HD )
                        VALUES ( v_cambio_vel.pedido_id , v_cambio_vel.subpedido_id , v_cambio_vel.solicitud_id ,
                            v_ident_inter, sysdate, 'N',
                            'CAMVE', v_velocidad, '0', '0');

                        UPDATE fnx_solicitudes
                        SET concepto_id   = 'PSMPR',
                            observacion   =  '<% ET-SMPRO-> [PSMPR-->'||observacion
                        WHERE PEDIDO_ID        = v_cambio_vel.pedido_id
                            AND SUBPEDIDO_ID      = v_cambio_vel.subpedido_id;
                    EXCEPTION
                          WHEN OTHERS
                          THEN
                             NULL;
                    END;
                ELSIF v_pedido IS NOT NULL AND v_respuesta != 'APROBADO' THEN
                    UPDATE FNX_TRANSAC_SMPRO
                       SET PROCESADO = 'N',
                           FECHA_PROCESADO = NULL
                     WHERE PEDIDO_ID = v_cambio_vel.pedido_id
                       AND SUBPEDIDO_ID = v_cambio_vel.subpedido_id
                       AND SOLICITUD_ID = v_cambio_vel.solicitud_id;
                END IF;
            END IF;
        END IF;
    ELSE
        --Adicion decodificador
        OPEN  c_adicion_deco (p_pedido );
        FETCH c_adicion_deco INTO v_adicion_deco;
        CLOSE c_adicion_deco;
        IF NVL(v_adicion_deco.pedido_id,'X') != 'X' THEN

            OPEN  c_tecnologia (v_adicion_deco.pedido_id,v_adicion_deco.subpedido_id,v_adicion_deco.solicitud_id );
            FETCH c_tecnologia INTO v_tecnologia;
            CLOSE c_tecnologia;
            IF v_tecnologia.contador >= 1 THEN
            
                v_valor_deco := NULL;
                v_cantidad_sd := 0;
                v_cantidad_hd := 0;
                OPEN  c_decos (v_adicion_deco.pedido_id, v_adicion_deco.subpedido_id , v_adicion_deco.solicitud_id  );
                FETCH c_decos INTO v_valor_deco;
                CLOSE c_decos;
                IF v_valor_deco = 'STBOX' THEN
                   v_cantidad_sd := v_cantidad_sd + 1;
                ELSIF v_valor_deco = 'STBAS' THEN
                   -- se verifica si tiene canales HD
                   -- si NO tiene canales HD se cuenta como estandar
                   -- si SI tiene canales HD se cuenta como HD
                   OPEN c_canales_hd(v_adicion_deco.pedido_id);
                   FETCH c_canales_hd INTO v_canales_hd;
                   CLOSE c_canales_hd;
                   IF v_canales_hd > 0 THEN
                      v_cantidad_hd := v_cantidad_hd + 1;
                   ELSE
                      v_cantidad_sd := v_cantidad_sd + 1;
                   END IF;
                END IF;

                v_ident_telev := fn_identificador_solicitud(v_adicion_deco.pedido_id,
                                                            v_adicion_deco.subpedido_id,
                                                            v_adicion_deco.solicitud_id);
                IF INSTR(v_ident_telev,'-IP') = 0 THEN
                   v_ident_telev := v_ident_telev || '-IP';
                END IF;

                BEGIN
                    SELECT PEDIDO_ID,RESPUESTA
                    INTO v_pedido, v_respuesta
                    FROM FNX_TRANSAC_SMPRO
                    WHERE PEDIDO_ID = v_cambio_vel.pedido_id and
                    SUBPEDIDO_ID = v_cambio_vel.subpedido_id and
                    SOLICITUD_ID = v_cambio_vel.solicitud_id;
                EXCEPTION
                      WHEN OTHERS
                      THEN
                         NULL;
                END;
                
                IF v_pedido is null THEN

                    BEGIN
                        INSERT INTO FENIX.FNX_TRANSAC_SMPRO (
                            PEDIDO_ID, SUBPEDIDO_ID, SOLICITUD_ID,
                            IDENTIFICADOR_ID, FECHA, PROCESADO,
                            TIPO_TRANSACCION, VELOCIDAD, CANTIDAD_SD, CANTIDAD_HD  )
                        VALUES ( v_adicion_deco.pedido_id , v_adicion_deco.subpedido_id , v_adicion_deco.solicitud_id ,
                            v_ident_telev, sysdate, 'N',
                            'ADIDE', '0', v_cantidad_sd, v_cantidad_hd);
                            
                        UPDATE fnx_solicitudes
                        SET concepto_id   = 'PSMPR',
                            observacion   =  '<% ET-SMPRO-> [PSMPR-->'||observacion
                        WHERE PEDIDO_ID        = v_adicion_deco.pedido_id
                            AND SUBPEDIDO_ID      = v_adicion_deco.subpedido_id
                            AND SOLICITUD_ID      = v_adicion_deco.solicitud_id;
                    EXCEPTION
                          WHEN OTHERS
                          THEN
                             NULL;
                    END;
                ELSIF v_pedido IS NOT NULL AND v_respuesta != 'APROBADO' THEN
                    UPDATE FNX_TRANSAC_SMPRO
                       SET PROCESADO = 'N',
                           FECHA_PROCESADO = NULL
                     WHERE PEDIDO_ID = v_cambio_vel.pedido_id
                       AND SUBPEDIDO_ID = v_cambio_vel.subpedido_id
                       AND SOLICITUD_ID = v_cambio_vel.solicitud_id;
                END IF;
            END IF;

        END IF;

    END IF;


END; 