FUNCTION       fn_obtener_tag_cablem_traslado
(  p_pedido        IN       fnx_solicitudes.pedido_id%type default null,
   p_subpedido     IN       fnx_solicitudes.subpedido_id%type default null,
   p_solicitud     IN       fnx_solicitudes.solicitud_id%type default null,
   p_identificador IN       fnx_solicitudes.identificador_id%type default null,
   p_elemento_id   IN       fnx_solicitudes.tipo_elemento_id%type default null,
   p_producto      IN       fnx_solicitudes.producto_id%type default null,
   p_tag           IN       varchar2,
   p_transaccion   IN       varchar2,
   p_requerimiento IN       fnx_ordenes_trabajos.requerimiento_id%type default null,
   p_actividad     IN       fnx_ordenes_trabajos.actividad_id%type default null
)
   RETURN VARCHAR2
IS

/*
© Copyright:  Empresas Públicas de Medellín E.S.P.
No está permitida su reproducción por ningún medio impreso, fotostático,
electrónico o similar, sin la previa autorización escrita del titular de
los derechos reservados.

Descripcion: Función para retornar el valor del tag correspondiente dependiendo del
             producto para la plataforma CABLEM, para el tipo de trabajo Traslado.
             Proyecto Activacion Automatica

HISTORIA DE MODIFICACIONES ------------------------------------------------------------------------
Fecha        Autor                      Observaciones
-----------  -------------------------  -----------------------------------------------------------
2010-10-13   Etachej                    Creación de la función - Implementación BA-TOIP
2011-03-15   ETachej                    Se realiza cambio para la transacción de Traslado, para que busque el equipo en
                                        FNX_SOLICITUDES.EQUIPO_ID_NUEVO,se hace llamado a la función fn_valor_equipo_anterior.
-----------------------------------------------------------------------------------------------------------------------
*/
-- 22-11-2013 MALVARAS      Traslado con cambio de velocidad y con cambio de equipo de Convencional a Wifi

-- Cursor que obtiene Identificador y estado del equipo
  CURSOR c_equipo_nuevo(pc_equipo IN fnx_equipos.equipo_id%type,
                        pc_tipo_elem_id  IN fnx_equipos.tipo_elemento_id%type) IS
   SELECT identificador_id, estado
   FROM FNX_EQUIPOS
   WHERE equipo_id        = pc_equipo
   and   tipo_elemento_id = pc_tipo_elem_id;

 -- Cursor que Identificador y estado del equipo
  CURSOR c_equipo_identif(pc_identificador IN fnx_identificadores.identificador_id%type,
                                                  pc_tipo_elem_id  IN fnx_equipos.tipo_elemento_id%type) IS
   SELECT equipo_id
   FROM FNX_EQUIPOS
   WHERE identificador_id = pc_identificador
   and   tipo_elemento_id = pc_tipo_elem_id
   and   estado           = 'OCU';

   -- Cursor tipo equipo
   CURSOR c_tipo_equipo(pc_equipo IN fnx_equipos.equipo_id%type) IS
    select decode((select count(1)
                            from fnx_marcas_referencias
                            where multiservicio_id=2
                            and (marca_id, referencia_id) in (select marca_id, referencia_id
                                                           from fnx_equipos e
                                                           where e.equipo_id  =pc_equipo
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                          from dual;

    -- Cursor Municipio
    CURSOR c_municipio(pc_municipio fnx_municipios.municipio_id%type)  IS
      select upper(nombre_municipio)
      from fnx_municipios
      where  municipio_id= pc_municipio;

   -- Cursor Trazabilidad de Equipos vs Identificadores
   CURSOR c_equipo_identif_bk(pc_pedido IN fnx_solicitudes.pedido_id%type,
                              pc_subpedido IN fnx_solicitudes.subpedido_id%type,
                              pc_solicitud IN fnx_solicitudes.solicitud_id%type,
                              pc_identificador IN fnx_identificadores.identificador_id%type
                              ) IS
    select equipo_id
    from fnx_log_cambios_equipos
    where pedido_id  = pc_pedido
    and subpedido_id = pc_subpedido
    and solicitud_id = pc_solicitud
    and identificador_id = pc_identificador;

-- 22-11-2012 MALVARAS
      CURSOR c_trabaj (p_pedido     IN   FNX_SOLICITUDES.pedido_id%TYPE,
                    p_subpedido  IN   FNX_SOLICITUDES.subpedido_id%TYPE,
                    p_solicitud  IN   FNX_SOLICITUDES.solicitud_id%TYPE
                    ) IS
      SELECT caracteristica_id, tipo_trabajo
      FROM   fnx_trabajos_solicitudes
      WHERE  pedido_id    = p_pedido
      AND    subpedido_id = p_subpedido
      AND    solicitud_id = p_solicitud;

       v_trabaj                             c_trabaj%ROWTYPE;
       v_cambio_vel                         BOOLEAN;
 --/ 22-11-2012 MALVARAS

    -- Declaración de variables locales a la función
    v_valor_tag          varchar2(100);
    v_identificador      fnx_solicitudes.identificador_id%type;
    v_ident_comp_ba      FNX_IDENTIFICADORES.IDENTIFICADOR_ID%TYPE;
    v_ident_comp_toip    FNX_IDENTIFICADORES.IDENTIFICADOR_ID%TYPE;
    v_ident_comp_ba_o    FNX_IDENTIFICADORES.IDENTIFICADOR_ID%TYPE;
    v_ident_comp_toip_o  FNX_IDENTIFICADORES.IDENTIFICADOR_ID%TYPE;
    v_equipo_nuevo       FNX_EQUIPOS.EQUIPO_ID%TYPE;
    v_equipo_ant         FNX_EQUIPOS.EQUIPO_ID%TYPE;
    v_municipio          fnx_municipios.municipio_id%type;
    v_producto_id        fnx_solicitudes.producto_id%type;
    v_tipo_trabajo       fnx_ordenes_trabajos.tipo_trabajo%type;
    v_tipo_elemento      fnx_equipos.TIPO_ELEMENTO_ID%type;
    v_equipo_ident       FNX_EQUIPOS.EQUIPO_ID%TYPE;
    v_equipo_toip        FNX_EQUIPOS.EQUIPO_ID%TYPE;
    v_equipo_ident_bk    FNX_EQUIPOS.EQUIPO_ID%TYPE;
    v_equipo_estado      FNX_EQUIPOS.ESTADO%TYPE;
    v_estado_soli        FNX_SOLICITUDES.ESTADO_ID%TYPE:=NULL;



BEGIN
    v_identificador := p_identificador;
    v_producto_id   := p_producto;
    v_tipo_elemento :='CABLEM';


  --2010-10-13 Etachej - Se valida si es un Traslado
   IF pkg_solicitudes.fn_tipo_trabajo(p_pedido,p_subpedido,p_solicitud,38) = 'CAMBI' THEN
      v_tipo_trabajo :='TRASL';
   END IF;


    -- -------------------------------------------------------------------------------------
    -- 1. Obtención de identificadores por producto
    -- -------------------------------------------------------------------------------------

    -- 1.1 Obtener identificadores de acceso primario y cuenta respectivamente para Internet
    IF (v_producto_id = 'INTER') THEN

        IF (v_tipo_trabajo ='TRASL') THEN
            v_identificador := p_identificador;
        END IF;

    -- 1.2 Obtener identificadores de acceso primario y cuenta respectivamente para ToIP
    ELSIF (v_producto_id = 'TO') THEN
        IF (v_tipo_trabajo ='TRASL') THEN
            IF (p_elemento_id = 'TO') THEN
                v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 4093);
            ELSE
                v_identificador := p_identificador;
            END IF;
        END IF;
    END IF;

     -- 2.  Verifica si comparte Infraestructura
    IF (p_producto ='INTER') THEN
        v_ident_comp_toip    := FN_COMPARTE_INFRA (v_identificador, 'HFC', 'TELRES','TO',p_pedido, p_subpedido, p_solicitud);
        v_ident_comp_toip_o  := FN_COMPARTE_INFRA (v_identificador, 'HFC', 'TELRES','TO',null, null, null);
        IF NVL(fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33),'N')='HFC' THEN
            v_ident_comp_toip    := FN_COMPARTE_INFRA (v_identificador, 'REDCO', 'TELRES','TO',p_pedido, p_subpedido, p_solicitud);
        END IF;

        IF NVL(fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33),'N')='REDCO' THEN
            v_ident_comp_toip    := FN_COMPARTE_INFRA (v_identificador, 'HFC', 'TELRES','TO',p_pedido, p_subpedido, p_solicitud);
        END IF;
    ELSIF (p_producto ='TO' ) THEN
        v_ident_comp_ba      := FN_COMPARTE_INFRA (v_identificador, 'HFC', 'DATOS','INTER',p_pedido, p_subpedido, p_solicitud);
        v_ident_comp_ba_o    := FN_COMPARTE_INFRA (v_identificador, 'HFC', 'DATOS','INTER',null, null, null);
    END IF;

     IF(p_actividad='DECAM') THEN
        v_estado_soli := pkg_solicitudes.FN_ESTADO(p_pedido, p_subpedido,p_solicitud);
         IF(v_estado_soli IN ('CUMPL','FACTU')) THEN
            IF (p_producto ='INTER' AND p_actividad ='DECAM' ) THEN
                v_ident_comp_toip_o := FN_COMPARTE_INFRA_RESP (v_identificador, 'HFC', 'TELRES','TO',p_pedido, p_subpedido, p_solicitud);
                v_ident_comp_toip   := FN_COMPARTE_INFRA (v_identificador, 'HFC', 'TELRES','TO',null, null, null);
                 IF NVL(fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33),'N')='HFC' THEN
                    v_ident_comp_toip := FN_COMPARTE_INFRA (v_identificador, 'REDCO', 'TELRES','TO',null, null, null);
                 END IF;
            ELSIF (p_producto ='TO' AND p_actividad ='DECAM' ) THEN
                v_ident_comp_ba_o  := FN_COMPARTE_INFRA_RESP (v_identificador, 'HFC', 'DATOS','INTER',p_pedido, p_subpedido, p_solicitud);
                v_ident_comp_ba    := FN_COMPARTE_INFRA (v_identificador, 'HFC', 'DATOS','INTER',null, null, null);
            END IF;
         END IF;
     END IF;
    v_equipo_nuevo := fn_valor_caract_subelem (p_pedido, p_subpedido, 'EQACCP', 200);

    OPEN  c_equipo_nuevo(v_equipo_nuevo,'CABLEM');
    FETCH c_equipo_nuevo INTO v_equipo_ident,v_equipo_estado;
    CLOSE c_equipo_nuevo;

    OPEN  c_equipo_identif(v_identificador,v_tipo_elemento);
    FETCH c_equipo_identif INTO v_equipo_ant;
    CLOSE c_equipo_identif;

    -- 3.  Obtención del tag correspondiente por producto
    -- 3.1 Tratamiento del producto INTERNET para obtener el tag pasado como parámetro (Edith Paola Tache - Analista Desarrollador MVM)
    IF (v_producto_id = 'INTER') THEN
       IF (p_actividad ='HACAM' AND p_transaccion='CREAR') THEN
            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_nuevo;
            END IF;

            --<identificador>43007564</identificador>
            IF (lower(p_tag)='identificador') THEN
                IF (p_elemento_id = 'ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_valor_tag:= v_identificador;
            END IF;

            --<uso>RES</uso>
            IF (lower(p_tag)='uso') THEN
                IF (p_elemento_id = 'ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            END IF;

            --<plan>BA-1000</plan>
             IF (lower(p_tag)='plan') THEN

                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                IF (NVL(FN_VALOR_TRABAJ_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N' AND NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N') THEN
                    v_valor_tag :='BA-'||FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124);
                ELSE
                    v_valor_tag     := 'BA-'||FN_VALOR_CARACT_IDENTIF(v_identificador,124);
                END IF;
             END IF;

            --<tipoEquipo>EMTA</tipoEquipo>
             IF (lower(p_tag)='tipoequipo') THEN
                 OPEN  c_tipo_equipo(v_equipo_nuevo);
                 FETCH c_tipo_equipo INTO v_valor_tag;
                 CLOSE c_tipo_equipo;
             END IF;

            --<ciudad>MEDELLIN</ciudad>
             IF (lower(p_tag)='ciudad') THEN
                v_municipio := null;
                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_municipio := FN_VALOR_CARACT_IDENTIF(v_identificador,34);
                 OPEN  c_municipio(v_municipio);
                 FETCH c_municipio INTO v_valor_tag;
                 CLOSE c_municipio;
             END IF;

             --<departamento>ANT</departamento>
             IF (lower(p_tag)='departamento') THEN
                 IF (p_elemento_id ='ACCESP') THEN
                     v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                 END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
             END IF;

       END IF;

       IF (p_actividad ='HACAM' AND (p_transaccion='CAMBIO' AND v_ident_comp_toip IS NOT NULL)
           OR p_transaccion='CEQUI') THEN
            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_nuevo;
            END IF;

            --<macActual>00A0FC354B93</macActual>
            IF (lower(p_tag)='macactual') THEN
                v_valor_tag := v_equipo_ant;
            END IF;

            --<identificador>43007564</identificador>
            IF (lower(p_tag)='identificador') THEN
                IF (p_elemento_id = 'ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_valor_tag:= v_identificador;
            END IF;

            --<uso>RES</uso>
            IF (lower(p_tag)='uso') THEN
                 IF (p_elemento_id = 'ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            END IF;

            --<plan>BA-1000</plan>
             IF (lower(p_tag)='plan') THEN

                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                IF (NVL(FN_VALOR_TRABAJ_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N' AND NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N') THEN
                    v_valor_tag :='BA-'||FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124);
                ELSE
                    v_valor_tag     := 'BA-'||FN_VALOR_CARACT_IDENTIF(v_identificador,124);
                END IF;
             END IF;

            --<tipoEquipo>EMTA</tipoEquipo>
             IF (lower(p_tag)='tipoequipo') THEN
                 OPEN  c_tipo_equipo(v_equipo_nuevo);
                 FETCH c_tipo_equipo INTO v_valor_tag;
                 CLOSE c_tipo_equipo;
             END IF;

            --<ciudad>MEDELLIN</ciudad>
             IF (lower(p_tag)='ciudad') THEN
                v_municipio := null;
                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_municipio := FN_VALOR_CARACT_IDENTIF(v_identificador,34);
                 OPEN  c_municipio(v_municipio);
                 FETCH c_municipio INTO v_valor_tag;
                 CLOSE c_municipio;
             END IF;

             --<departamento>ANT</departamento>
             IF (lower(p_tag)='departamento') THEN
                 IF (p_elemento_id ='ACCESP') THEN
                     v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                 END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
             END IF;

       END IF;

       -- 22-11-2013 MALVARAS Traslado con cambio de velocidad y con cambio de equipo de Convencional a Wifi
                --Se verifica si hay cambio de plan
                  OPEN c_trabaj ( p_pedido, p_subpedido, p_solicitud);
                     LOOP
                       FETCH c_trabaj INTO v_trabaj;
                       EXIT WHEN c_trabaj%NOTFOUND;
                       IF v_trabaj.caracteristica_id IN (124) AND  v_trabaj.tipo_trabajo = 'CAMBI'
                       THEN
                          v_cambio_vel  := TRUE;
                       END IF;
                     END LOOP;
                  CLOSE c_trabaj;

         IF (v_tipo_trabajo ='TRASL' AND v_cambio_vel= TRUE AND p_actividad ='HACAM' AND p_transaccion='CAMBIO'  ) THEN
            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_nuevo;
            END IF;

            --<macActual>00A0FC354B93</macActual>
            IF (lower(p_tag)='macactual') THEN
                v_valor_tag := v_equipo_ant;
            END IF;

            --<identificador>43007564</identificador>
            IF (lower(p_tag)='identificador') THEN
                IF (p_elemento_id = 'ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_valor_tag:= v_identificador;
            END IF;

            --<uso>RES</uso>
            IF (lower(p_tag)='uso') THEN
                 IF (p_elemento_id = 'ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            END IF;

            --<plan>BA-1000</plan>
             IF (lower(p_tag)='plan') THEN

                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                IF (NVL(FN_VALOR_TRABAJ_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N' AND NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N') THEN
                    v_valor_tag :='BA-'||FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124);
                ELSE
                    v_valor_tag     := 'BA-'||FN_VALOR_CARACT_IDENTIF(v_identificador,124);
                END IF;
             END IF;

            --<tipoEquipo>EMTA</tipoEquipo>
             IF (lower(p_tag)='tipoequipo') THEN
                 OPEN  c_tipo_equipo(v_equipo_nuevo);
                 FETCH c_tipo_equipo INTO v_valor_tag;
                 CLOSE c_tipo_equipo;
             END IF;

            --<ciudad>MEDELLIN</ciudad>
             IF (lower(p_tag)='ciudad') THEN
                v_municipio := null;
                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_municipio := FN_VALOR_CARACT_IDENTIF(v_identificador,34);
                 OPEN  c_municipio(v_municipio);
                 FETCH c_municipio INTO v_valor_tag;
                 CLOSE c_municipio;
             END IF;

             --<departamento>ANT</departamento>
             IF (lower(p_tag)='departamento') THEN
                 IF (p_elemento_id ='ACCESP') THEN
                     v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                 END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
             END IF;

       END IF;

       --/ 22-11-2013 MALVARAS



        IF (p_actividad ='HACAM' AND p_transaccion='CAMEQ') THEN
            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_nuevo;
            END IF;

            --<macActual>00A0FC354B93</macActual>
            IF (lower(p_tag)='macactual') THEN
                v_valor_tag := fn_valor_equipo_anterior(p_pedido, p_subpedido, 'EQACCP');
            END IF;

                       --<uso>RES</uso>
            IF (lower(p_tag)='uso') THEN
                 IF (p_elemento_id = 'ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            END IF;

            --<plan>BA-1000</plan>
             IF (lower(p_tag)='plan') THEN

                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                IF (NVL(FN_VALOR_TRABAJ_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N' AND NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N') THEN
                    v_valor_tag :='BA-'||FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124);
                ELSE
                    v_valor_tag     := 'BA-'||FN_VALOR_CARACT_IDENTIF(v_identificador,124);
                END IF;
             END IF;

            --<tipoEquipo>EMTA</tipoEquipo>
             IF (lower(p_tag)='tipoequipo') THEN
                 OPEN  c_tipo_equipo(v_equipo_nuevo);
                 FETCH c_tipo_equipo INTO v_valor_tag;
                 CLOSE c_tipo_equipo;
             END IF;

            --<ciudad>MEDELLIN</ciudad>
             IF (lower(p_tag)='ciudad') THEN
                v_municipio := null;
                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_municipio := FN_VALOR_CARACT_IDENTIF(v_identificador,34);
                 OPEN  c_municipio(v_municipio);
                 FETCH c_municipio INTO v_valor_tag;
                 CLOSE c_municipio;
             END IF;

             --<departamento>ANT</departamento>
             IF (lower(p_tag)='departamento') THEN
                 IF (p_elemento_id ='ACCESP') THEN
                     v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                 END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
             END IF;

       END IF;

       IF (p_actividad ='DECAM' AND p_transaccion='CAMBIO' AND v_ident_comp_toip_o IS NOT NULL) THEN
            OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_identificador);
            FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
            CLOSE c_equipo_identif_bk;

            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_ident_bk;
            END IF;

            --<identificador>43007564</identificador>
            IF (lower(p_tag)='identificador') THEN
                v_valor_tag:= v_ident_comp_toip_o;
            END IF;

            --<uso>RES</uso>
            IF (lower(p_tag)='uso') THEN
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_ident_comp_toip_o,130),2);
            END IF;

            --<plan>BA-1000</plan>
            IF (lower(p_tag)='plan') THEN
                v_valor_tag     := 'TO';
            END IF;

            --<tipoEquipo>EMTA</tipoEquipo>
             IF (lower(p_tag)='tipoequipo') THEN
                 OPEN  c_tipo_equipo(v_equipo_ant);
                 FETCH c_tipo_equipo INTO v_valor_tag;
                 CLOSE c_tipo_equipo;
             END IF;

            --<ciudad>MEDELLIN</ciudad>
             IF (lower(p_tag)='ciudad') THEN
                v_municipio := null;
                v_municipio :=FN_VALOR_CARACT_IDENTIF(v_ident_comp_toip_o,34);
                OPEN  c_municipio(v_municipio);
                FETCH c_municipio INTO v_valor_tag;
                CLOSE c_municipio;
             END IF;

             --<departamento>ANT</departamento>
             IF (lower(p_tag)='departamento') THEN
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_ident_comp_toip_o,204);
             END IF;

       END IF;

       IF (p_actividad ='DECAM' AND p_transaccion='RETIRO' AND v_ident_comp_toip IS NOT NULL
           AND v_ident_comp_toip_o IS NULL
           AND fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33) IS NULL ) THEN
            OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_ident_comp_toip);
            FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
            CLOSE c_equipo_identif_bk;
            --<mac>00A0FC354B93</mac>
           IF (lower(p_tag)='mac') THEN
            IF (v_equipo_nuevo = v_equipo_ident_bk) THEN
                OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_identificador);
                FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
                CLOSE c_equipo_identif_bk;
            END IF;
            v_valor_tag := v_equipo_ident_bk;
           END IF;
       END IF;

       IF (p_actividad ='DECAM' AND p_transaccion='RETIRO' AND v_ident_comp_toip IS NULL  AND v_ident_comp_toip_o IS NULL
           AND fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33) IS NULL) THEN
            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_ant;
            END IF;
       END IF;

        IF (p_actividad ='DECAM' AND p_transaccion='RETIRO' AND v_ident_comp_toip IS NOT NULL
            AND v_ident_comp_toip_o IS NOT NULL AND fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33) IS NULL) THEN
            OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_ident_comp_toip);
            FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
            CLOSE c_equipo_identif_bk;

            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_ident_bk;
            END IF;
       END IF;

       IF (p_actividad ='DECAM' AND p_transaccion='RETIRO' --AND v_ident_comp_toip IS NOT NULL
            AND NVL(fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33),'N')='HFC') THEN
            OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_identificador);
            FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
            CLOSE c_equipo_identif_bk;

            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := NVL(v_equipo_ident_bk,v_equipo_ant);
            END IF;
       END IF;

       IF (p_actividad ='DECAM' AND p_transaccion='RETIRO' AND v_ident_comp_toip IS NOT NULL
            AND NVL(fn_valor_trabaj_subelem (p_pedido, p_subpedido, 'ACCESP', 33),'N')='REDCO') THEN
            OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_ident_comp_toip);
            FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
            CLOSE c_equipo_identif_bk;

            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_ident_bk;
            END IF;
       END IF;

        IF (p_actividad ='DECAM' AND p_transaccion='CREAR') THEN
            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_identificador);
                FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
                CLOSE c_equipo_identif_bk;
                v_valor_tag := v_equipo_ident_bk;
            END IF;

            --<identificador>43007564</identificador>
            IF (lower(p_tag)='identificador') THEN
                v_valor_tag:= v_ident_comp_toip_o;
            END IF;

            --<uso>RES</uso>
            IF (lower(p_tag)='uso') THEN
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_ident_comp_toip_o,130),2);
            END IF;

            --<plan>BA-1000</plan>
            IF (lower(p_tag)='plan') THEN
                v_valor_tag     := 'TO';
            END IF;

            --<tipoEquipo>EMTA</tipoEquipo>
             IF (lower(p_tag)='tipoequipo') THEN
                 OPEN  c_tipo_equipo(v_equipo_ant);
                 FETCH c_tipo_equipo INTO v_valor_tag;
                 CLOSE c_tipo_equipo;
             END IF;

            --<ciudad>MEDELLIN</ciudad>
             IF (lower(p_tag)='ciudad') THEN
                v_municipio := null;
                v_municipio :=FN_VALOR_CARACT_IDENTIF(v_ident_comp_toip_o,34);
                OPEN  c_municipio(v_municipio);
                FETCH c_municipio INTO v_valor_tag;
                CLOSE c_municipio;
             END IF;

             --<departamento>ANT</departamento>
             IF (lower(p_tag)='departamento') THEN
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_ident_comp_toip_o,204);
             END IF;

       END IF;
    END IF;


    -- 2.3 Tratamiento del producto TO para obtener el tag pasado como parámetro

     IF(v_producto_id ='TO') THEN
      IF (p_actividad ='HACAM' AND p_transaccion='CREAR') THEN
        --<mac>00A0FC354B93</mac>
        IF (lower(p_tag)='mac') THEN
          v_valor_tag := v_equipo_nuevo;
        END IF;

        --<identificador>43007564</identificador>
         IF (lower(p_tag)='identificador') THEN
             v_valor_tag:= v_identificador;
         END IF;

        --<uso>RES</uso>
        IF (lower(p_tag)='uso') THEN
            v_valor_tag := FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_identificador,130),2);
        END IF;

        --<plan>BA-1000</plan>
         IF (lower(p_tag)='plan') THEN
             v_valor_tag  :='TO';
         END IF;

        --<tipoEquipo>EMTA</tipoEquipo>
         IF (lower(p_tag)='tipoequipo') THEN
           OPEN  c_tipo_equipo(v_equipo_nuevo);
           FETCH c_tipo_equipo INTO v_valor_tag;
           CLOSE c_tipo_equipo;
         END IF;

        --<ciudad>MEDELLIN</ciudad>
         IF (lower(p_tag)='ciudad') THEN
            v_municipio := NULL;
            IF (v_tipo_trabajo IN ('NUEVO','TRASL')) THEN
               v_municipio := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',34);
            ELSE
               v_municipio :=FN_VALOR_CARACT_IDENTIF(v_identificador,34);
            END IF;
            OPEN  c_municipio(v_municipio);
            FETCH c_municipio INTO v_valor_tag;
            CLOSE c_municipio;
         END IF;

         --<departamento>ANT</departamento>
         IF (lower(p_tag)='departamento') THEN
             v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
         END IF;
      END IF;

      IF (p_actividad ='HACAM' AND p_transaccion='CEQUI' AND (v_ident_comp_ba IS NOT NULL OR v_ident_comp_ba_o IS NOT NULL)) THEN
        --<mac>00A0FC354B93</mac>
        IF (lower(p_tag)='mac') THEN
          v_valor_tag := v_equipo_nuevo;
        END IF;

         --<macactual>00A0FC354B93</macactual>
        IF (lower(p_tag)='macactual') THEN
           IF (NVL(v_equipo_estado,'N')='LIB') THEN
            OPEN  c_equipo_identif(v_ident_comp_ba,v_tipo_elemento);
            FETCH c_equipo_identif INTO v_equipo_ant;
            CLOSE c_equipo_identif;
           ELSE
            IF (v_ident_comp_ba IS NULL) THEN
                OPEN  c_equipo_identif(v_identificador,v_tipo_elemento);
                FETCH c_equipo_identif INTO v_equipo_ant;
                CLOSE c_equipo_identif;
            ELSE
               OPEN  c_equipo_identif(v_ident_comp_ba,v_tipo_elemento);
                FETCH c_equipo_identif INTO v_equipo_ant;
                CLOSE c_equipo_identif;
            END IF;
           END IF;
           v_valor_tag := v_equipo_ant;
        END IF;

        --<uso>RES</uso>
        IF (lower(p_tag)='uso') THEN
            v_valor_tag := FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_identificador,130),2);
        END IF;

        --<plan>BA-1000</plan>
        IF (lower(p_tag)='plan') THEN
            IF (v_ident_comp_ba IS NULL) THEN
                v_valor_tag  :='TO';
              ELSE
                v_valor_tag := 'BA-'||FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_ident_comp_ba,2126)||'-IC001',124);
              END IF;
        END IF;

        --<tipoEquipo>EMTA</tipoEquipo>
        IF (lower(p_tag)='tipoequipo') THEN
             OPEN  c_tipo_equipo(v_equipo_nuevo);
             FETCH c_tipo_equipo INTO v_valor_tag;
             CLOSE c_tipo_equipo;
        END IF;

         --<ciudad>MEDELLIN</ciudad>
         IF (lower(p_tag)='ciudad') THEN
            v_municipio := NULL;
            IF (v_tipo_trabajo  IN ('NUEVO','TRASL')) THEN
               v_municipio := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',34);
            ELSE
               v_municipio :=FN_VALOR_CARACT_IDENTIF(v_identificador,34);
            END IF;
            OPEN  c_municipio(v_municipio);
            FETCH c_municipio INTO v_valor_tag;
            CLOSE c_municipio;
         END IF;

         --<departamento>ANT</departamento>
         IF (lower(p_tag)='departamento') THEN
             v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
         END IF;
     END IF;

     IF (p_actividad ='HACAM' AND p_transaccion='CAMEQ') THEN
        --<mac>00A0FC354B93</mac>
        IF (lower(p_tag)='mac') THEN
          v_valor_tag := v_equipo_nuevo;
        END IF;

         --<macactual>00A0FC354B93</macactual>
        IF (lower(p_tag)='macactual') THEN
           v_valor_tag := fn_valor_equipo_anterior(p_pedido, p_subpedido, 'EQACCP');
        END IF;

        --<uso>RES</uso>
        IF (lower(p_tag)='uso') THEN
           v_valor_tag := FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_identificador,130),2);
        END IF;

        --<plan>BA-1000</plan>
        IF (lower(p_tag)='plan') THEN
             IF (v_ident_comp_ba IS NULL) THEN
                v_valor_tag  :='TO';
              ELSE
                v_valor_tag := 'BA-'||FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_ident_comp_ba,2126)||'-IC001',124);
              END IF;
        END IF;

        --<tipoEquipo>EMTA</tipoEquipo>
        IF (lower(p_tag)='tipoequipo') THEN
             OPEN  c_tipo_equipo(v_equipo_nuevo);
             FETCH c_tipo_equipo INTO v_valor_tag;
             CLOSE c_tipo_equipo;
        END IF;

        --<ciudad>MEDELLIN</ciudad>
         IF (lower(p_tag)='ciudad') THEN
            v_municipio := NULL;
            IF (v_tipo_trabajo IN ('NUEVO','TRASL')) THEN
               v_municipio := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',34);
            ELSE
               v_municipio :=FN_VALOR_CARACT_IDENTIF(v_identificador,34);
            END IF;
            OPEN  c_municipio(v_municipio);
            FETCH c_municipio INTO v_valor_tag;
            CLOSE c_municipio;
         END IF;

         --<departamento>ANT</departamento>
         IF (lower(p_tag)='departamento') THEN
             v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
         END IF;
     END IF;

      IF (p_actividad ='HACAM' AND p_transaccion='CAMBIO' AND v_ident_comp_ba IS NOT NULL) THEN
            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_nuevo;
            END IF;

            --<identificador>43007564</identificador>
            IF (lower(p_tag)='identificador') THEN
                v_identificador:= FN_VALOR_CARACT_IDENTIF(v_ident_comp_ba,2126);
                v_valor_tag:= v_identificador;
            END IF;

            --<uso>RES</uso>
            IF (lower(p_tag)='uso') THEN
                v_identificador:= FN_VALOR_CARACT_IDENTIF(v_ident_comp_ba,2126);
                v_identificador := v_identificador||'-IC001';
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            END IF;

            --<plan>BA-1000</plan>
             IF (lower(p_tag)='plan') THEN
                v_identificador := FN_VALOR_CARACT_IDENTIF(v_ident_comp_ba,2126);
                v_identificador := v_identificador||'-IC001';
                v_valor_tag     := 'BA-'||FN_VALOR_CARACT_IDENTIF(v_identificador,124);
             END IF;

            --<tipoEquipo>EMTA</tipoEquipo>
             IF (lower(p_tag)='tipoequipo') THEN
                 OPEN  c_tipo_equipo(v_equipo_nuevo);
                 FETCH c_tipo_equipo INTO v_valor_tag;
                 CLOSE c_tipo_equipo;
             END IF;

            --<ciudad>MEDELLIN</ciudad>
             IF (lower(p_tag)='ciudad') THEN
                v_municipio := null;
                v_identificador := FN_VALOR_CARACT_IDENTIF(v_ident_comp_ba,2126);
                v_identificador := v_identificador||'-IC001';
                v_municipio := FN_VALOR_CARACT_IDENTIF(v_identificador,34);
                 OPEN  c_municipio(v_municipio);
                 FETCH c_municipio INTO v_valor_tag;
                 CLOSE c_municipio;
             END IF;

             --<departamento>ANT</departamento>
             IF (lower(p_tag)='departamento') THEN
                v_identificador := FN_VALOR_CARACT_IDENTIF(v_ident_comp_ba,2126);
                v_identificador := v_identificador||'-IC001';
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
             END IF;

       END IF;

       IF (p_actividad ='HACAM' AND p_transaccion='CAMBIO' AND v_ident_comp_ba_o IS NOT NULL AND  v_ident_comp_ba IS NULL) THEN
            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_nuevo;
            END IF;

            --<identificador>43007564</identificador>
            IF (lower(p_tag)='identificador') THEN
                v_valor_tag:= v_identificador;
            END IF;

             --<uso>RES</uso>
        IF (lower(p_tag)='uso') THEN
           v_valor_tag := FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_identificador,130),2);
        END IF;

        --<plan>BA-1000</plan>
        IF (lower(p_tag)='plan') THEN
             IF (v_ident_comp_ba IS NULL) THEN
                v_valor_tag  :='TO';
              ELSE
                v_valor_tag := 'BA-'||FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_ident_comp_ba,2126)||'-IC001',124);
              END IF;
        END IF;

        --<tipoEquipo>EMTA</tipoEquipo>
        IF (lower(p_tag)='tipoequipo') THEN
             OPEN  c_tipo_equipo(v_equipo_nuevo);
             FETCH c_tipo_equipo INTO v_valor_tag;
             CLOSE c_tipo_equipo;
        END IF;

        --<ciudad>MEDELLIN</ciudad>
         IF (lower(p_tag)='ciudad') THEN
            v_municipio := NULL;
            IF (v_tipo_trabajo IN ('NUEVO','TRASL')) THEN
               v_municipio := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',34);
            ELSE
               v_municipio :=FN_VALOR_CARACT_IDENTIF(v_identificador,34);
            END IF;
            OPEN  c_municipio(v_municipio);
            FETCH c_municipio INTO v_valor_tag;
            CLOSE c_municipio;
         END IF;

         --<departamento>ANT</departamento>
         IF (lower(p_tag)='departamento') THEN
             v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
         END IF;

       END IF;

      IF (p_actividad ='DECAM' AND p_transaccion='RETIRO' AND v_ident_comp_ba_o IS NULL AND v_ident_comp_ba IS NULL ) THEN
            OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_identificador);
            FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
            CLOSE c_equipo_identif_bk;

            --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                v_valor_tag := v_equipo_ident_bk;
            END IF;
      END IF;

      IF (p_actividad ='DECAM' AND p_transaccion='RETIRO' AND v_ident_comp_ba IS NOT NULL AND v_ident_comp_ba_o IS NULL) THEN
        --<mac>00A0FC354B93</mac>
        IF (lower(p_tag)='mac') THEN
            OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_identificador);
            FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
            CLOSE c_equipo_identif_bk;

             IF (v_equipo_nuevo = v_equipo_ident_bk) THEN
                OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_ident_comp_ba);
                FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
                CLOSE c_equipo_identif_bk;
                v_valor_tag := v_equipo_ident_bk;
             ELSE
                v_valor_tag := v_equipo_ident_bk;
             END IF;

        END IF;


      END IF;

       IF (p_actividad ='DECAM' AND p_transaccion='RETIRO' AND v_ident_comp_ba IS NOT NULL AND v_ident_comp_ba_o IS NOT NULL) THEN
        dbms_output.put_line('v_ident_comp_ba:'||v_ident_comp_ba);
        --<mac>00A0FC354B93</mac>
            IF (lower(p_tag)='mac') THEN
                OPEN  c_equipo_identif_bk(p_pedido,p_subpedido,p_solicitud,v_ident_comp_ba);
                FETCH c_equipo_identif_bk INTO v_equipo_ident_bk;
                CLOSE c_equipo_identif_bk;
                v_valor_tag := v_equipo_ident_bk;

            END IF;
        END IF;

    END IF;
    RETURN(trim(v_valor_tag));

EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error en FN_OBTENER_TAG_CABLEM_PRODUCTO: '||SQLERRM||' ['||TO_CHAR(SQLCODE)||'].');
        RETURN (NULL);
END;