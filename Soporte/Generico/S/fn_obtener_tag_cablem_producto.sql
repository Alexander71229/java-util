FUNCTION       fn_obtener_tag_cablem_producto
(  p_pedido        IN       fnx_solicitudes.pedido_id%TYPE DEFAULT NULL,
   p_subpedido     IN       fnx_solicitudes.subpedido_id%TYPE DEFAULT NULL,
   p_solicitud     IN       fnx_solicitudes.solicitud_id%TYPE DEFAULT NULL,
   p_identificador IN       fnx_solicitudes.identificador_id%TYPE DEFAULT NULL,
   p_elemento_id   IN       fnx_solicitudes.tipo_elemento_id%TYPE DEFAULT NULL,
   p_producto      IN       fnx_solicitudes.producto_id%TYPE DEFAULT NULL,
   p_tag           IN       VARCHAR2,
   p_requerimiento IN       fnx_ordenes_trabajos.requerimiento_id%TYPE DEFAULT NULL
)
   RETURN VARCHAR2
IS

/*
© Copyright:  Empresas Públicas de Medellín E.S.P.
No está permitida su reproducción por ningún medio impreso, fotostático,
electrónico o similar, sin la previa autorización escrita del titular de
los derechos reservados.

Descripcion: Función para retornar el valor del tag correspondiente dependiendo del
             producto para la plataforma CABLEM.
             Proyecto Activacion Automatica

HISTORIA DE MODIFICACIONES ------------------------------------------------------------------------
Fecha        Autor                      Observaciones
-----------  -------------------------  -----------------------------------------------------------
2010-06-25   Etachej                    Creación de la función - Implementación BA-TOIP-3PLAY
2010-07-27   JGallg                     Adición de la lógica para el manejo del producto CENTREX.
                                        Incluidos comentarios adicionales.
2010-07-28   JGallg                     Obtención de identificadores de acceso primario y de cuenta
                                        por producto antes de buscar los valores de los tags.
2010-07-28   JGallg                     Llamado a las nuevas funciones "fun_ejecuta_select",
                                        "fn_valor_para_tag" y "fn_tipo_intercambio" para simplificar
                                        la obtención de datos.
2010-07-30   JGallg                     Obtención del tag correspondiente de acuerdo a la transacción.
                                        Ya no se requiere invocar la función FN_TIPO_INTERCAMBIO.
2010-08-05   JGallg                     Para las transacciones de nuevo de CENTREX se requiere preguntar
                                        por el tag correspondiente a la MAC.
2010-08-09   JGallg                     Adición de la lógica para el manejo del producto TRONCALES.
2010-08-11   JGallg                     Adición de rastreo de error en la sección Exceptions con
                                        DBMS_OUTPUT.PUT_LINE.
2010-08-18   JGallg                     Para los tags de "identificador" y "uso" se corrige el
                                        tipo_elemento_id con el que se devuelve el identificador de sede
2010-08-19   JGallg                     Corrección de uso para trabajar con el servicio Centrex.
2010-08-19   Etachej                    Cambio TOIP para NUEVO cuando comparte con BA
2010-08-24   JGallg                     Para transacciones diferentes a nuevos y cambios, obtener la MAC
                                        con base en el agrupador_id y no en el identificador_id.
2010-08-24   JGallg                     Para la transacción NUEVO tratamiento para diferenciar cuando
                                        es un nuevo servicio o una adición de sede a un centrex existente
2010-08-25   JGallg                     Para la transacción NUEVO tratamiento para diferenciar cuando
                                        es un nuevo servicio o una adición de sede a un troncales existente
2010-09-14   Etachej                    Implementación Cambio de Equipo por Mantenimiento - BA
2010-09-14   Etachej                    Implementación Cambio de Equipo por Mantenimiento - TOIP
2010-09-16   JGallg                     Para el valor obtenido para el Tag, quitar espacios al principio y
                                        al final debido a que en las MAC se encontró ese problema.  Eso
                                        ocasiona error de "Mac no valida".
2010-09-17   JGallg                     Para los productos Centrex y troncales obtener la MAC de la solicitud
                                        y no de la sede para nuevos servicios.
2010-09-28   Jgomezve                   Se busca la sede para Centrex y Troncales, desde la mac anterior
2010-10-06   JGallg                     Tratar la macNueva en las transacciones de nuevo servicio (Centrex y Troncales)
2010-10-07   JGallg                     Para los tag de ciudad, departamento, tipoEquipo e identificador corregir
                                        la manera de como se obtienen ya que se deben obtener de cada sede y no del
                                        subpedido (productos Centrex y Troncales).
2010-10-19   JGallg                     Para los productos Centrex y Troncales, para el tema de daños, traer el uso
                                        del identificador agrupador.
2010-10-20   JGallg                     Para el tema de daños, obtener el identificador agrupador para Centrex y Troncales
2010-10-25   JGallg                     Para los productos Centrex y Troncales, el tema de daños se da en la respectiva sede
                                        seleccionada.  Se debe revisar que en los equipos se esté dando una inversión de
                                        identificador (que queda con el agrupador) e identificador agrupador (que queda con el de sede).
2011-09-08   Jpulgarb                   Se modifica condicion de busqueda de datos para TO-RETIR para definir el tipo de busqueda cuando
                                        comparte infraestructura con INTER
2011-12-19   Jpulgarb                   se modifica la validacion de comparte infraestructura para consultar el identificador

2011-12-27   Jpulgarb                   se modifica la condicion de busqueda del identificador dependiendo del intercambio
                                        si INTER envió el CREAR, el identificador debe ser la cuenta del INTER para el intercambio CDACL,
                                        Si TOIP envió el CREAR, el identificador debe ser el TOIP-XXXXX
2012-01-02   JPulgarb                   Se implementa logica para busqueda de equivalencias de planes de velocidad
2012-06-07   JGLENA                     Se modifican condiciones de búsqueda de campo serie_id en
                                        fnx_ordenes_trabajos para cambio de equipo por mantenimiento.
2014-01-03   Jrendbr                    Se modifica las condiciones de búsqueda de la MAC para tipo_trabajo: NUEVO,
                                        se utilice la función FN_VALOR_CARACT_SUBELEM.

2014-07-14   Jgilve                     Se adiciona la lógica para un segundo acceso. (Un bloque IF-ELSE) para los tags del producto 'INTER ' .
                                        Tags identificador, plan, ciudad, departamento y uso.

2014-08-20   Jgilve                     Se adiciona una condicion AND para que se busque que tenga la caracteristica 5595 en 'N'.

014-08-25  jgilve REQ62353_InteBA2Acce  Realizar el cambio de un equipo de segundo acceso validando que la velocidad sea de 5 Megas.

2014-09-09  Jrendbr ITP66474_CambVeloEqui:Si hay cambio de velocidad igual o superior a 8 megas (característica 124)
--                          y el cliente tenga instalado un equipo diferente a DOCSIS3.0,
--                          entonces se actualice la observación de la solicitud en curso
--                          adicionando  la siguiente marca: ** DOCSIS 3.0 **
-----------------------------------------------------------------------------------------------------------------------
*/
    -- Declaración de variables locales a la función
    v_valor_tag          VARCHAR2(100);
    v_identificador                 fnx_solicitudes.identificador_id%TYPE;
    v_identificador_acceso_prim     fnx_solicitudes.identificador_id%TYPE;
    v_identificador_cuenta          fnx_solicitudes.identificador_id%TYPE;
    v_identificador_comparte_infra  fnx_equipos.identificador_id%TYPE;
    v_sentencia_select              VARCHAR2(500);
    v_caracteristica                INTEGER;
    v_tipo_trabajo                  fnx_trabajos_solicitudes.tipo_trabajo%TYPE;
    v_requerimeinto                 fnx_ordenes_trabajos.requerimiento_id%TYPE;
    v_municipio                     fnx_solicitudes.municipio_id%TYPE;
    v_servicio                      fnx_identificadores.servicio_id%TYPE;
    v_producto                      fnx_identificadores.producto_id%TYPE;
    v_producto_id                   fnx_identificadores.producto_id%TYPE;
    v_equipo                        fnx_equipos.equipo_id%TYPE;
    v_equipo_ant                    fnx_equipos.equipo_id%TYPE;
    v_producto_daño                 fnx_identificadores.producto_id%TYPE;

    v_identif_agrupa                fnx_solicitudes.identificador_id%TYPE;
    v_identif_daño                  fnx_solicitudes.identificador_id%TYPE;
    v_cantidad_sedes                INTEGER;

    --2010-09-28   Jgomezve                   Se busca la sede para Centrex y Troncales, desde la mac anterior
    v_agrupador_empresas            fnx_equipos.agrupador_id%TYPE;
      -- 2011-12-27   Jpulgarb
    v_cantidad                          NUMBER (1) := 0;
    v_plan_plataf                       fnx_caracteristica_solicitudes.valor%type;       --2012-01-02   JPulgarb
    v_uso                               fnx_caracteristica_solicitudes.valor%type;       --2012-01-02   JPulgarb

      -- 2011-12-27   Jpulgarb
      CURSOR c_tarPlat_InterCam_Prod(p_inter      IN   fnx_tareas_plataformas.tipo_intercambio%TYPE,
                                     p_producto   IN   fnx_requerimientos_trabajos.producto_id%type)
         IS
            SELECT COUNT(1)
              FROM fnx_tareas_plataformas
             WHERE requerimiento_id in (select requerimiento_id
                                        from fnx_requerimientos_trabajos
                                        where pedido_id = p_pedido
                                        and   producto_id = p_producto
                                       )
              and  tipo_intercambio =  p_inter
              and  plataforma = 'CABLEM';

      -- 2011-12-27   Jpulgarb
      CURSOR c_cuenta_inter
         IS
            SELECT   valor
              FROM   fnx_caracteristica_solicitudes
             WHERE       pedido_id = p_pedido
                     AND caracteristica_id = 2126
                     AND producto_id = 'INTER'
                     AND tipo_elemento_id = 'ACCESP'
                     AND ROWNUM < 2;

      --2012-01-02   JPulgarb
      CURSOR c_plan_plataforma (i_uso               IN fnx_caracteristica_solicitudes.valor%type,
                                i_perfil_plan_fenix IN fnx_caracteristica_solicitudes.valor%type)
      IS
         SELECT   'BA-' || RV_HIGH_VALUE
           FROM   CG_REF_CODES
          WHERE   RV_DOMAIN    = 'PLANES_PATAFORMA_CABLEM'
                  AND RV_LOW_VALUE = i_perfil_plan_fenix
                  AND RV_ABBREVIATION = i_uso
                  AND RV_MEANING = 'ACT'
                  AND ROWNUM < 2;

-- 2011-12-19   Jpulgarb
FUNCTION FN_COMPARTE_INFRA2 (
   p_identificador IN       fnx_solicitudes.identificador_id%type,
   p_servicio      IN       fnx_solicitudes.servicio_id%type,
   p_producto      IN       fnx_solicitudes.producto_id%type,
   p_pedido        IN       fnx_solicitudes.pedido_id%type,
   p_subpedido     IN       fnx_solicitudes.subpedido_id%type,
   p_solicitud     IN       fnx_solicitudes.solicitud_id%type)
      RETURN VARCHAR2
IS


-- 04-12-2011 JPULGARB
CURSOR c_inf_tv_activas(pc_identificador1          IN        FNX_IDENTIFICADORES.identificador_id%TYPE)
IS
SELECT  identificador_id,centro_distribucion_intermedia,
        nodo_optico_electrico_id,
        tipo_amplificador_id,amplificador_id,tipo_derivador_id,
        derivador_id,terminal_derivador_id
FROM    fnx_inf_tv_activas
WHERE   identificador_id = pc_identificador1;

-- 04-12-2011 JPULGARB
v_inf_tv_activas            c_inf_tv_activas%ROWTYPE;
v_identif_servicio          FNX_IDENTIFICADORES.servicio_id%TYPE := NULL;
v_identif_producto          FNX_IDENTIFICADORES.producto_id%TYPE := NULL;
v_identificador_comparte    FNX_IDENTIFICADORES.identificador_id%TYPE := NULL;

-- 04-12-2011 JPULGARB
CURSOR c_inf_tv_tramites(pc_identificador1          IN FNX_IDENTIFICADORES.identificador_id%TYPE,
                         pc_pedido                  IN FNX_SOLICITUDES.pedido_id%TYPE,
                         pc_subpedido               IN FNX_SOLICITUDES.subpedido_id%TYPE,
                         pc_solicitud               IN FNX_SOLICITUDES.solicitud_id%TYPE)
IS
SELECT  identificador_id,centro_distribucion_intermedia,
        nodo_optico_electrico_id,
        tipo_amplificador_id,amplificador_id,tipo_derivador_id,
        derivador_id,terminal_derivador_id
FROM    fnx_inf_tv_tramites
WHERE   identificador_id = pc_identificador1
AND     pedido_id        = pc_pedido
AND     subpedido_id     = pc_subpedido
AND     solicitud_id     = pc_solicitud
AND     NVL(INSTALADO,'N') <>'S'
;

CURSOR c_inf_tv_tramites_2 ( pc_identificador1                          IN    FNX_IDENTIFICADORES.identificador_id%TYPE,
                            pc_centro_distribucion_inter             IN     FNX_INF_TV_ACTIVAS.centro_distribucion_intermedia%TYPE,
                            pc_nodo_optico_electrico_id              IN     FNX_INF_TV_ACTIVAS.nodo_optico_electrico_id%TYPE,
                            pc_amplificador_id                         IN    FNX_INF_TV_ACTIVAS.amplificador_id%TYPE,
                            pc_tipo_amplificador_id                    IN    FNX_INF_TV_ACTIVAS.tipo_amplificador_id%TYPE,
                            pc_tipo_derivador_id                       IN    FNX_INF_TV_ACTIVAS.tipo_derivador_id%TYPE,
                            pc_derivador_id                            IN    FNX_INF_TV_ACTIVAS.derivador_id%TYPE,
                            pc_terminal_derivador_id                   IN    FNX_INF_TV_ACTIVAS.terminal_derivador_id%TYPE)
IS

SELECT   identificador_id
FROM     fnx_inf_tv_tramites
WHERE     centro_distribucion_intermedia=    pc_centro_distribucion_inter
AND nodo_optico_electrico_id            =    pc_nodo_optico_electrico_id
AND tipo_amplificador_id                =    pc_tipo_amplificador_id
AND amplificador_id                     =    pc_amplificador_id
AND tipo_derivador_id                   =    pc_tipo_derivador_id
AND derivador_id                        =    pc_derivador_id
AND terminal_derivador_id               =    pc_terminal_derivador_id
AND identificador_id                    <>     pc_identificador1
AND     NVL(INSTALADO,'N') <>'S';

CURSOR c_identif_producto ( pc_identificador1                          IN    FNX_IDENTIFICADORES.identificador_id%TYPE)
IS
SELECT  servicio_id, producto_id
FROM    FNX_IDENTIFICADORES
WHERE   identificador_id= pc_identificador1
AND     TIPO_ELEMENTO_ID <>'TO';

BEGIN

v_inf_tv_activas := NULL;
            OPEN   c_inf_tv_tramites(p_identificador,p_pedido,p_subpedido,p_solicitud);
            FETCH  c_inf_tv_tramites INTO v_inf_tv_activas;
            CLOSE  c_inf_tv_tramites;

              FOR r_inf_tv_tramites_2 IN c_inf_tv_tramites_2(p_identificador,v_inf_tv_activas.centro_distribucion_intermedia,v_inf_tv_activas.nodo_optico_electrico_id,
                           v_inf_tv_activas.amplificador_id,v_inf_tv_activas.tipo_amplificador_id,v_inf_tv_activas.tipo_derivador_id,
                           v_inf_tv_activas.derivador_id,v_inf_tv_activas.terminal_derivador_id) LOOP


               v_identif_servicio:=NULL;
               v_identif_producto:=NULL;
               OPEN c_identif_producto(r_inf_tv_tramites_2.identificador_id);
               FETCH c_identif_producto INTO v_identif_servicio,v_identif_producto;
               CLOSE c_identif_producto;

               IF (NVL(v_identif_servicio,'N') = p_servicio) AND (NVL(v_identif_producto,'N') = p_producto) THEN
                 v_identificador_comparte := r_inf_tv_tramites_2.identificador_id;
               END IF;

              END LOOP;

RETURN v_identificador_comparte;

END;

BEGIN
    v_identificador := p_identificador;
    v_producto_id   := p_producto;


 --2010-09-14   Etachej  Cambio de Equipo por Mantenimiento
  IF p_requerimiento IS NULL THEN
    BEGIN
       SELECT tipo_trabajo, requerimiento_id
       INTO   v_tipo_trabajo, v_requerimeinto
       FROM   fnx_requerimientos_trabajos
       WHERE  pedido_id    = p_pedido
       AND    subpedido_id = p_subpedido
       AND    solicitud_id = p_solicitud
       AND ROWNUM =1;
    EXCEPTION
     WHEN OTHERS THEN
       v_tipo_trabajo := NULL;
       v_requerimeinto := NULL;
    END;
  ELSE
     BEGIN
       SELECT tipo_trabajo, requerimiento_id
       INTO   v_tipo_trabajo, v_requerimeinto
       FROM   fnx_requerimientos_trabajos
       WHERE  requerimiento_id = p_requerimiento
       AND ROWNUM =1;
    EXCEPTION
     WHEN OTHERS THEN
       v_tipo_trabajo := NULL;
       v_requerimeinto := NULL;
    END;
  END IF;

    IF v_tipo_trabajo IS NULL OR v_requerimeinto IS NULL THEN
        RETURN(NULL);
    END IF;

   --20100927 Etachej Tipo Trabajo Refal, se busca identificador de equipo a Cambiar
    IF v_tipo_trabajo ='REFAL' THEN
       BEGIN
        SELECT trim(paquete_id), producto_id
        INTO v_equipo_ant, v_producto_daño
        FROM fnx_requerimientos_trabajos
        WHERE requerimiento_id = p_requerimiento
             AND paquete_id IS NOT NULL
             AND ROWNUM=1;
       EXCEPTION
       WHEN OTHERS THEN
          v_equipo_ant :=NULL;
       END;

       IF (v_equipo_ant IS NOT NULL) THEN
         BEGIN
            SELECT producto_id, identificador_id, agrupador_id
            INTO v_producto_id, v_identif_daño, v_agrupador_empresas
            FROM fnx_equipos
            WHERE equipo_id = v_equipo_ant;
         EXCEPTION
         WHEN OTHERS THEN
              v_producto :=NULL;
              v_identif_daño := NULL;
         END;
       END IF;
    END IF;
    -- -------------------------------------------------------------------------------------
    -- 1. Obtención de identificadores por producto (por John Carlos Gallego - Analista MVM)
    -- -------------------------------------------------------------------------------------

    -- 1.1 Obtener identificadores de acceso primario y cuenta respectivamente para Internet
    IF (v_producto_id = 'INTER') THEN

        IF (v_tipo_trabajo ='NUEVO') THEN
            v_identificador := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'CUENTA', 1);
        ELSIF (v_tipo_trabajo ='REFAL') THEN
           IF (v_equipo_ant IS NOT NULL) THEN
              IF (v_producto_id <> NVL(v_producto_daño,'N')) THEN
                v_identificador := v_identif_daño;
              ELSE
                v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 2093);
              END IF;
           ELSE
               v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 2093);
           END IF;
        ELSE
            v_identificador := p_identificador;
        END IF;

        IF (v_identificador IS NULL) THEN
          v_identificador := p_identificador;
        END IF;

    -- 1.2 Obtener identificadores de acceso primario y cuenta respectivamente para 3Play
    ELSIF (v_producto_id = '3PLAY') THEN
        IF (v_tipo_trabajo ='NUEVO') THEN
            v_identificador := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido, '3PLAY', 1);
        ELSE
            v_identificador := p_identificador;
        END IF;

    -- 1.3 Obtener identificadores de acceso primario y cuenta respectivamente para ToIP
    ELSIF (v_producto_id = 'TO') THEN
        IF (p_elemento_id = 'TO') THEN
            v_caracteristica := 4093;
        END IF;

        IF (v_tipo_trabajo ='NUEVO') THEN
            v_identificador := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP', 1);
        ELSIF (v_tipo_trabajo ='REFAL') THEN
           IF (v_equipo_ant IS NOT NULL) THEN
              IF (v_producto_id <> NVL(v_producto_daño,'N')) THEN
                v_identificador := v_identif_daño;
              ELSE
                v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 4093);
              END IF;
           ELSE
               v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 4093);
           END IF;
        ELSE
          IF (v_caracteristica IS NOT NULL) THEN
            v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, v_caracteristica);
          END IF;
        END IF;

        IF (v_identificador IS NULL) THEN
          v_identificador := p_identificador;
        END IF;

    -- 1.4 Obtener identificadores de acceso primario y cuenta respectivamente para Centrex
    ELSIF (v_producto_id = 'CNTXIP') THEN
        IF (v_tipo_trabajo ='NUEVO') THEN
            v_identificador := FN_VALOR_CARACTERISTICA_SOL (p_pedido, p_subpedido, p_solicitud, 1);
        ELSIF (v_tipo_trabajo ='REFAL') THEN
           IF (v_equipo_ant IS NOT NULL) THEN
                IF (v_producto_id <> NVL(v_producto_daño,'N')) THEN
                    v_identificador := v_identif_daño;
                ELSE
                    v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 1);
                END IF;
           ELSE
              v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 1);
           END IF;
        ELSE
            v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 1);
        END IF;

    -- 1.5 Obtener identificadores de acceso primario y cuenta respectivamente para Troncales
    ELSIF (v_producto_id = 'TRKSIP') THEN
        IF (v_tipo_trabajo ='NUEVO') THEN
            v_identificador := FN_VALOR_CARACTERISTICA_SOL (p_pedido, p_subpedido, p_solicitud, 1);
        ELSIF (v_tipo_trabajo ='REFAL') THEN
           IF (v_equipo_ant IS NOT NULL) THEN
                IF (v_producto_id <> NVL(v_producto_daño,'N')) THEN
                    v_identificador := v_identif_daño;
                ELSE
                    v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 1);
                END IF;
           ELSE
              v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 1);
           END IF;
        ELSE
            v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador, 1);
        END IF;
    END IF;

    -- 2.  Obtención del tag correspondiente por producto
    -- 2.1 Tratamiento del producto INTERNET para obtener el tag pasado como parámetro (Edith Paola Tache - Analista Desarrollador MVM)
    IF (v_producto_id = 'INTER') THEN
        --<mac>00A0FC354B93</mac>
        IF (LOWER(p_tag)='mac') THEN
          IF (v_tipo_trabajo ='NUEVO') THEN
            v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
          ELSE
             IF (p_elemento_id = 'CUENTA') THEN
               v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2093);
             END IF;

             BEGIN
              SELECT equipo_id
              INTO v_valor_tag
              FROM fnx_equipos
              WHERE identificador_id=v_identificador
              AND tipo_elemento_id='CABLEM'
              -- 2014-08-20 jgilve REQ62353_InteBA2Acce
              -- Validación para la suspención, que no sea el Segundo Cable Modem.
              AND NVL( pkg_equipos_util.fn_valor_configuracion_mr( marca_id,
                                                                   referencia_id,
                                                                   equipo_id,
                                                                   5595 ),'N') = 'N'
              -- FIN Validación para la suspención, que no sea el Segundo Cable Modem.
              AND ROWNUM=1;
             EXCEPTION
                WHEN OTHERS THEN
                --2014-01-03   Jrendbr                    Se modifica las condiciones de búsqueda de la MAC para tipo_trabajo: NUEVO,
                --                                         se utilice la función FN_VALOR_CARACT_SUBELEM.
                    IF v_tipo_trabajo = 'CAMBI' THEN
                        v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
                    ELSE
                        v_valor_tag :=NULL;
                    END IF;
             END;
          END IF;
        END IF;

        --<macnueva>00A0FC354B93</macnueva>
        IF (LOWER(p_tag)='macnueva') THEN
         --2010-09-14 Etachej - Cambio de Equipo MTTO
          IF (v_tipo_trabajo ='REFAL') THEN
           BEGIN
              SELECT trim(serie_id)
              INTO v_valor_tag
              FROM fnx_ordenes_trabajos o
              WHERE requerimiento_id = v_requerimeinto
              AND serie_id IS NOT NULL
              AND estado_id = 'PENDI'
              AND (o.cola_id, o.actividad_id) IN
                   (SELECT cp.cola_id, cp.actividad_id
                      FROM fnx_colas_productos cp
                     WHERE cp.tipo_trabajo = 'REFAL'
                       AND cp.tipo_elemento = 'EQU')
              AND ROWNUM=1;
           EXCEPTION
              WHEN OTHERS THEN
                   v_valor_tag :=NULL;
           END;
          ELSE
            --  2014-09-09  Jrendbr ITP66474_CambVeloEqui:Si hay cambio de velocidad igual o superior a 8 megas (característica 124)
            --                          y el cliente tenga instalado un equipo diferente a DOCSIS3.0,
            --                          entonces se actualice la observación de la solicitud en curso
            --                          adicionando  la siguiente marca: ** DOCSIS 3.0 **
            v_valor_tag := NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'CABLEM',200),FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200));          END IF;
        END IF;

        --<identificador>43007564</identificador>
         IF (LOWER(p_tag)='identificador') THEN
              IF (v_tipo_trabajo ='NUEVO') THEN
                -- 2014-07-04 jgilve REQ62353_InteBA2Acce
                --  Adición segundo acceso a Internet.
                IF (FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',5595) = 'S') THEN
                    v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',90);
                ELSE
                    v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'CUENTA',1);
                END IF;
              ELSIF (v_tipo_trabajo ='RETIR') THEN
                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
               --Verifica si comparte infraestructura con un TOIP
                v_identificador_comparte_infra := NULL;
                v_identificador_comparte_infra := FN_COMPARTE_INFRA (FN_VALOR_CARACT_IDENTIF(v_identificador,2093), 'HFC', 'TELRES','TO',NULL,NULL,NULL);
                IF (v_identificador_comparte_infra IS NOT NULL) THEN
                    v_valor_tag     := v_identificador_comparte_infra;
                ELSE
                   v_valor_tag     := v_identificador;
                END IF;

              ELSIF (v_tipo_trabajo ='REFAL') THEN
                v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                v_valor_tag:= v_identificador;
              ELSE
                 IF (p_elemento_id IN ('ACCESP','CABLEM')) THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                 END IF;
                v_valor_tag:= v_identificador;
              END IF;
        END IF;

        --<uso>RES</uso>
        IF (LOWER(p_tag)='uso') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                -- 2014-07-04 jgilve REQ62353_InteBA2Acce
                --  Adición segundo acceso a Internet.
                IF (FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',5595) = 'S') THEN
                    v_valor_tag := FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',90)||'-IC001',2);
                ELSE
                    v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',2);
                END IF;
            ELSIF (v_tipo_trabajo ='REFAL') THEN
                v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                v_identificador := v_identificador||'-IC001';
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            ELSE
                IF (p_elemento_id IN ('ACCESP','CABLEM')) THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador, 2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            END IF;
        END IF;

        --<plan>BA-1000</plan>
         IF (LOWER(p_tag)='plan') THEN
             --Si es Suspesión se verifica si comparte infraestructura con un TOIP
             IF (v_tipo_trabajo IN ('SUSPE','RETIR')) THEN
                IF (p_elemento_id ='ACCESP') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                --Verifica si comparte infraestructura con un TOIP
                v_identificador_comparte_infra := NULL;
                v_identificador_comparte_infra := FN_COMPARTE_INFRA (FN_VALOR_CARACT_IDENTIF(v_identificador,2093), 'HFC', 'TELRES','TO',NULL,NULL,NULL);
                IF (v_identificador_comparte_infra IS NOT NULL) THEN
                    v_valor_tag     := 'TO';
                ELSE
                   v_identificador := v_identificador||'-IC001';
                   v_valor_tag     := 'BA-'||FN_VALOR_CARACT_IDENTIF(v_identificador,124);
                END IF;
             ELSIF (v_tipo_trabajo ='NUEVO') THEN
                -- 2014-07-04 jgilve REQ62353_InteBA2Acce
                --  Adición segundo acceso a Internet.
                IF (FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',5595) = 'S') THEN
                    v_valor_tag :='BA-'||FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',5594);
                ELSE
                    v_valor_tag :='BA-'||FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124);
                END IF;
             ELSIF (v_tipo_trabajo ='REFAL') THEN
        -- 2014-08-25  jgilve REQ62353_InteBA2Acce
                -- Controlar la lógica para genererar el valor del tag según sea el primer acceso o el segundo acceso.
                IF(NVL(FN_VALOR_CONFIG_EQUIPOS(v_equipo_ant,'5595'),'N') = 'N') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                    v_identificador := v_identificador||'-IC001';
                    v_valor_tag := 'BA-'||FN_VALOR_CARACT_IDENTIF(v_identificador,124);
                ELSE
                    v_valor_tag := 'BA-'||FN_VALOR_CONFIG_EQUIPOS(v_equipo_ant,'5594');
                END IF;
                -- FIN Cambio de un equipo de segundo acceso validando que la velocidad sea de 5 Megas.
             ELSE
                IF (p_elemento_id IN ('ACCESP','CABLEM')) THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                IF (NVL(FN_VALOR_TRABAJ_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N' AND NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124),'N') <>'N') THEN
                    v_valor_tag := 'BA-'||FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',124);
                ELSE
                    v_valor_tag := 'BA-'||FN_VALOR_CARACT_IDENTIF(v_identificador,124);
                END IF;
             END IF;

             --<2012-01-02   JPulgarb
             v_uso := FN_OBTENER_TAG_CABLEM_PRODUCTO(p_pedido,p_subpedido,p_solicitud,p_identificador,p_elemento_id,p_producto,'uso',p_requerimiento);
             OPEN  c_plan_plataforma  (v_uso,replace(v_valor_tag,'BA-',NULL));
              FETCH c_plan_plataforma  INTO v_plan_plataf;
             CLOSE c_plan_plataforma;
             v_valor_tag := NVL(v_plan_plataf,v_valor_tag);
             -->

         END IF;

        --<tipoEquipo>EMTA</tipoEquipo>
         IF (LOWER(p_tag)='tipoequipo') THEN
             IF (v_tipo_trabajo ='NUEVO') THEN
                 BEGIN
                     SELECT DECODE((SELECT COUNT(1)
                        FROM fnx_marcas_referencias
                        WHERE multiservicio_id=2
                        AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                       FROM fnx_equipos e
                                                       WHERE e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200)
                                                       ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                       INTO v_valor_tag
                      FROM DUAL;
                 EXCEPTION
                  WHEN OTHERS THEN
                    v_valor_tag := NULL;
                 END;
             ELSIF (v_tipo_trabajo ='REFAL') THEN
                 v_equipo  := NULL;
                   BEGIN
                      SELECT trim(serie_id)
                      INTO v_equipo
                      FROM fnx_ordenes_trabajos o
                      WHERE requerimiento_id = v_requerimeinto
                      AND serie_id IS NOT NULL
                      AND estado_id = 'PENDI'
                      AND (o.cola_id, o.actividad_id) IN
                           (SELECT cp.cola_id, cp.actividad_id
                              FROM fnx_colas_productos cp
                             WHERE cp.tipo_trabajo = 'REFAL'
                               AND cp.tipo_elemento = 'EQU')
                      AND ROWNUM=1;
                   EXCEPTION
                      WHEN OTHERS THEN
                           v_equipo :=NULL;
                   END;

                   BEGIN
                         SELECT DECODE((SELECT COUNT(1)
                            FROM fnx_marcas_referencias
                            WHERE multiservicio_id=2
                            AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                           FROM fnx_equipos e
                                                           WHERE e.equipo_id  =v_equipo
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           INTO v_valor_tag
                          FROM DUAL;
                   EXCEPTION
                      WHEN OTHERS THEN
                        v_valor_tag := NULL;
                   END;
             ELSE
                 IF (p_elemento_id = 'CUENTA') THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2093);
                 END IF;
                  IF (p_elemento_id = 'CABLEM') THEN
                     BEGIN
                         SELECT DECODE((SELECT COUNT(1)
                            FROM fnx_marcas_referencias
                            WHERE multiservicio_id=2
                            AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                           FROM fnx_equipos e
                                                           WHERE e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'CABLEM',200)
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           INTO v_valor_tag
                          FROM DUAL;
                     EXCEPTION
                      WHEN OTHERS THEN
                        v_valor_tag := NULL;
                     END;
                  ELSE
                    BEGIN
                         SELECT DECODE((SELECT COUNT(1)
                            FROM fnx_marcas_referencias
                            WHERE multiservicio_id=2
                            AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                           FROM fnx_equipos e
                                                           WHERE e.identificador_id  =v_identificador) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           INTO v_valor_tag
                          FROM DUAL;
                     EXCEPTION
                      WHEN OTHERS THEN
                        v_valor_tag := NULL;
                     END;
                  END IF;
             END IF;
         END IF;

        --<ciudad>MEDELLIN</ciudad>
         IF (LOWER(p_tag)='ciudad') THEN
            v_municipio := NULL;
             IF (v_tipo_trabajo ='NUEVO') THEN
                -- 2014-07-04 jgilve REQ62353_InteBA2Acce
                --  Adición segundo acceso a Internet.
                -- 2014-08-06 obteniendo el valor del tag desde el identificador.
        IF (FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',5595) = 'S') THEN
                    v_municipio := NVL(fn_valor_caract_identif (v_identificador, 34),fn_valor_caract_subelem(p_pedido, p_subpedido,'INTCON',34));
                ELSE
                    v_municipio := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',34);
                END IF;
             ELSIF (v_tipo_trabajo = 'REFAL') THEN
                v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                v_identificador := v_identificador||'-IC001';
                v_municipio := FN_VALOR_CARACT_IDENTIF(v_identificador,34);
             ELSE
                IF (p_elemento_id IN ('ACCESP','CABLEM')) THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_municipio := FN_VALOR_CARACT_IDENTIF(v_identificador,34);
             END IF;

             BEGIN
              SELECT UPPER(nombre_municipio)
                INTO v_valor_tag
                FROM fnx_municipios
                WHERE  municipio_id= v_municipio;
              EXCEPTION
              WHEN OTHERS THEN
                v_valor_tag := NULL;
             END;

         END IF;

         --<departamento>ANT</departamento>
         IF (LOWER(p_tag)='departamento') THEN
             IF (v_tipo_trabajo ='NUEVO') THEN
                -- 2014-07-04 jgilve REQ62353_InteBA2Acce
                --  Adición segundo acceso a Internet.
                -- 2014-08-06 obteniendo el valor del tag desde el identificador.
                IF (FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',5595) = 'S') THEN
                    v_valor_tag := NVL(fn_valor_caract_identif (v_identificador, 204),fn_valor_caract_subelem(p_pedido, p_subpedido,'INTCON',204));
                ELSE
                    v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'INTCON',204);
                END IF;
             ELSIF (v_tipo_trabajo ='REFAL') THEN
                v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                v_identificador := v_identificador||'-IC001';
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
             ELSE
                IF (p_elemento_id IN ('ACCESP','CABLEM')) THEN
                    v_identificador:= FN_VALOR_CARACT_IDENTIF(v_identificador,2126);
                END IF;
                v_identificador := v_identificador||'-IC001';
                v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
             END IF;
         END IF;
    END IF;

    -- 2.2 Tratamiento del producto 3PLAY para obtener el tag pasado como parámetro
     IF(v_producto_id ='3PLAY') THEN

        --<mac>00A0FC354B93</mac>
        IF (LOWER(p_tag)='mac') THEN
          IF (v_tipo_trabajo ='NUEVO') THEN
            v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
          ELSE
             BEGIN
              SELECT equipo_id
              INTO v_valor_tag
              FROM fnx_equipos
              WHERE identificador_id=v_identificador
              AND tipo_elemento_id='CABLEM'
              AND ROWNUM=1;
             EXCEPTION
                WHEN OTHERS THEN
                    v_valor_tag :=NULL;
             END;
          END IF;
        END IF;

        --<macnueva>00A0FC354B93</macnueva>
        IF (LOWER(p_tag)='macnueva') THEN
           v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'CABLEM',200);
        END IF;

        --<identificador>43007564</identificador>
         IF (LOWER(p_tag)='identificador') THEN
          IF (v_tipo_trabajo ='NUEVO') THEN
            v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'3PLAY',1);
          ELSIF (NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'3PLAY',2626),'N') <>'N') THEN
             v_identificador := fn_valor_caract_subelem (p_pedido, p_subpedido,'3PLAY',2125);
            v_valor_tag := v_identificador;
          ELSE
            v_valor_tag := v_identificador;
          END IF;
        END IF;

        --<uso>RES</uso>
        IF (LOWER(p_tag)='uso') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'3PLAY',2);
            ELSE
             v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador,1);
             v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            END IF;
        END IF;

        --<plan>BA-1000</plan>
         IF (LOWER(p_tag)='plan') THEN
           IF (v_tipo_trabajo ='NUEVO') THEN
                v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'3PLAY',2122);
           ELSIF (v_tipo_trabajo <>'CAMBI') THEN
            v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador,1);
            v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2122);
           ELSE
            IF (NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'3PLAY',2626),'N') <>'N') THEN
                v_valor_tag := 'BA-'||fn_valor_caract_subelem (p_pedido, p_subpedido,'3PLAY',124);
            ELSE
                 v_valor_tag := fn_valor_caract_subelem (p_pedido, p_subpedido,'3PLAY',2122);
            END IF;

             --<2012-01-02   JPulgarb
             v_uso := FN_OBTENER_TAG_CABLEM_PRODUCTO(p_pedido,p_subpedido,p_solicitud,p_identificador,p_elemento_id,p_producto,'uso',p_requerimiento);
             OPEN  c_plan_plataforma  (v_uso,replace(v_valor_tag,'BA-',NULL));
              FETCH c_plan_plataforma  INTO v_plan_plataf;
             CLOSE c_plan_plataforma;
             v_valor_tag := NVL(v_plan_plataf,v_valor_tag);
             -->

           END IF;

           IF(v_valor_tag IS NULL) THEN
            v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador,1);
            v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2122);
           END IF;

             --<2012-01-02   JPulgarb
             v_uso := FN_OBTENER_TAG_CABLEM_PRODUCTO(p_pedido,p_subpedido,p_solicitud,p_identificador,p_elemento_id,p_producto,'uso',p_requerimiento);
             OPEN  c_plan_plataforma  (v_uso,replace(v_valor_tag,'BA-',NULL));
              FETCH c_plan_plataforma  INTO v_plan_plataf;
             CLOSE c_plan_plataforma;
             v_valor_tag := NVL(v_plan_plataf,v_valor_tag);
             -->

         END IF;

        --<tipoEquipo>EMTA</tipoEquipo>
         IF (LOWER(p_tag)='tipoequipo') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                BEGIN
                     SELECT DECODE((SELECT COUNT(1)
                        FROM fnx_marcas_referencias
                        WHERE multiservicio_id=2
                        AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                       FROM fnx_equipos e
                                                       WHERE e.equipo_id  =FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'CABLEM',200)
                                                       ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                       INTO v_valor_tag
                      FROM DUAL;
                 EXCEPTION
                  WHEN OTHERS THEN
                    v_valor_tag := NULL;
                END;
              ELSIF (v_tipo_trabajo ='REFAL') THEN
                 v_equipo  := NULL;
                   BEGIN
                      SELECT trim(serie_id)
                      INTO v_equipo
                      FROM fnx_ordenes_trabajos o
                      WHERE requerimiento_id = v_requerimeinto
                      AND serie_id IS NOT NULL
                      AND estado_id = 'PENDI'
                      AND (o.cola_id, o.actividad_id) IN
                           (SELECT cp.cola_id, cp.actividad_id
                              FROM fnx_colas_productos cp
                             WHERE cp.tipo_trabajo = 'REFAL'
                               AND cp.tipo_elemento = 'EQU')
                      AND ROWNUM=1;
                   EXCEPTION
                      WHEN OTHERS THEN
                           v_equipo :=NULL;
                   END;

                   BEGIN
                         SELECT DECODE((SELECT COUNT(1)
                            FROM fnx_marcas_referencias
                            WHERE multiservicio_id=2
                            AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                           FROM fnx_equipos e
                                                           WHERE e.equipo_id  =v_equipo
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           INTO v_valor_tag
                          FROM DUAL;
                   EXCEPTION
                      WHEN OTHERS THEN
                        v_valor_tag := NULL;
                   END;
             ELSE
                BEGIN
                     SELECT DECODE((SELECT COUNT(1)
                        FROM fnx_marcas_referencias
                        WHERE multiservicio_id=2
                        AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                       FROM fnx_equipos e
                                                       WHERE e.identificador_id  =v_identificador) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                       INTO v_valor_tag
                      FROM DUAL;
                 EXCEPTION
                  WHEN OTHERS THEN
                    v_valor_tag := NULL;
                END;
            END IF;
         END IF;

        --<ciudad>MEDELLIN</ciudad>
         IF (LOWER(p_tag)='ciudad') THEN
             v_municipio := NULL;
             IF (v_tipo_trabajo ='NUEVO') THEN
                v_municipio := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'3PLAY',34);
             ELSE
               v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador,1);
               v_municipio := FN_VALOR_CARACT_IDENTIF(v_identificador,34);
             END IF;

             BEGIN
                SELECT UPPER(nombre_municipio)
                INTO v_valor_tag
                FROM fnx_municipios
                WHERE  municipio_id= v_municipio;
              EXCEPTION
              WHEN OTHERS THEN
                v_valor_tag := NULL;
             END;

         END IF;

         --<departamento>ANT</departamento>
         IF (LOWER(p_tag)='departamento') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                v_valor_tag  := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'3PLAY',204);
            ELSE
             v_identificador := FN_VALOR_CARACT_IDENTIF(v_identificador,1);
             v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
            END IF;
         END IF;
     END IF;

    -- 2.3 Tratamiento del producto TO para obtener el tag pasado como parámetro

     IF(v_producto_id ='TO') THEN

          --<mac>00A0FC354B93</mac>
        IF (LOWER(p_tag)='mac') THEN
          IF (v_tipo_trabajo ='NUEVO') THEN
                --Verifica si comparte infraestructura con un BA
                v_servicio :='DATOS';
                v_producto :='INTER';
                v_identificador_comparte_infra := NULL;
                v_identificador_comparte_infra := FN_COMPARTE_INFRA (v_identificador, 'HFC', v_servicio,v_producto,p_pedido,p_subpedido,p_solicitud);
              IF (v_identificador_comparte_infra IS NULL) THEN
                v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
              ELSE
                 BEGIN
                  SELECT equipo_id
                  INTO v_valor_tag
                  FROM fnx_equipos
                  WHERE identificador_id=v_identificador_comparte_infra
                  AND tipo_elemento_id='CABLEM'
                  AND ROWNUM=1;
                 EXCEPTION
                    WHEN OTHERS THEN
                        v_valor_tag :=NULL;
                 END;
              END IF;
          ELSE
             BEGIN
              SELECT equipo_id
              INTO v_valor_tag
              FROM fnx_equipos
              WHERE identificador_id=v_identificador
              AND tipo_elemento_id='CABLEM'
              AND ROWNUM=1;
             EXCEPTION
                WHEN OTHERS THEN
                    v_valor_tag :=NULL;
             END;
--<2011-09-08   Jpulgarb
           IF v_valor_tag IS NULL THEN
--Verifica si comparte infraestructura con un BA
                v_servicio :='DATOS';
                v_producto :='INTER';
                v_identificador_comparte_infra := NULL;
                v_identificador_comparte_infra := FN_COMPARTE_INFRA (v_identificador, 'HFC', v_servicio,v_producto,NULL,NULL,NULL);


                IF v_identificador_comparte_infra IS NOT NULL THEN
                BEGIN
                  SELECT equipo_id
                  INTO v_valor_tag
                  FROM fnx_equipos
                  WHERE identificador_id=v_identificador_comparte_infra
                  AND tipo_elemento_id='CABLEM'
                  AND ROWNUM=1;
                 EXCEPTION
                    WHEN OTHERS THEN
                        v_valor_tag :=NULL;
                 END;
                END IF;


             END IF;
-->


          END IF;
        END IF;

         --<macnueva>00A0FC354B93</macnueva>
        IF (LOWER(p_tag)='macnueva') THEN
          IF (v_tipo_trabajo ='NUEVO') THEN
            v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200);
          ELSE
           v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'CABLEM',200);
          END IF;
        END IF;

        --<identificador>43007564</identificador>
         IF (LOWER(p_tag)='identificador') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
               --Verifica si comparte infraestructura con un BA
                v_servicio :='DATOS';
                v_producto :='INTER';
                v_identificador_comparte_infra := NULL;
                v_identificador_comparte_infra := NVL(FN_COMPARTE_INFRA (v_identificador, 'HFC', v_servicio,v_producto,p_pedido,p_subpedido,p_solicitud),FN_COMPARTE_INFRA2(v_identificador, v_servicio,v_producto,p_pedido,p_subpedido,p_solicitud));-- 2011-12-19   Jpulgarb
              IF (v_identificador_comparte_infra IS NULL) THEN
                v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',1);
              ELSE

              -- SI COMPARTE
                    -- si existe CREAR para INTER  el identificador debe ser la cuenta del INTER

             -- 2011-12-27   Jpulgarb
             OPEN c_tarPlat_InterCam_Prod('CREAR','INTER');
             FETCH c_tarPlat_InterCam_Prod INTO v_cantidad;
             CLOSE c_tarPlat_InterCam_Prod;

              IF v_cantidad > 0 THEN
                 OPEN c_cuenta_inter;
                 FETCH c_cuenta_inter INTO v_valor_tag;
                 CLOSE c_cuenta_inter;
              ELSE
              v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',1);
              END IF;

             /*
             v_cantidad :=0;
             OPEN c_tarPlat_InterCam_Prod('CREAR','TO');
             FETCH c_tarPlat_InterCam_Prod INTO v_cantidad;
             CLOSE c_tarPlat_InterCam_Prod;

             -- si existe CREAR para TOIP el identificador es el TOIP
             IF v_cantidad > 0 THEN
             v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',1);
             END IF;
*/
             IF v_valor_tag IS NULL THEN
             v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador_comparte_infra,2126);
             END IF;

              END IF;
            ELSE
--<2011-09-08   Jpulgarb
             IF v_tipo_trabajo ='RETIR' THEN
        --Verifica si comparte infraestructura con un BA
                        v_servicio :='DATOS';
                        v_producto :='INTER';
                        v_identificador_comparte_infra := NULL;
                        v_identificador_comparte_infra := FN_COMPARTE_INFRA (v_identificador, 'HFC', v_servicio,v_producto,NULL,NULL,NULL);


                        IF v_identificador_comparte_infra IS NOT NULL THEN
                        BEGIN
                          SELECT equipo_id
                          INTO v_equipo
                          FROM fnx_equipos
                          WHERE identificador_id=v_identificador_comparte_infra
                          AND tipo_elemento_id='CABLEM'
                          AND ROWNUM=1;
                         EXCEPTION
                            WHEN OTHERS THEN
                                v_equipo :=NULL;
                         END;
                        END IF;


                        IF v_equipo IS NOT  NULL THEN
                           v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador_comparte_infra,2126);
                            ELSE
                         v_valor_tag:= v_identificador;
                         END IF;
             ELSE
              v_valor_tag:= v_identificador;
             END IF;

-->
             END IF;


        END IF;

        --<uso>RES</uso>
        IF (LOWER(p_tag)='uso') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
              v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TO',2);
            ELSIF (v_tipo_trabajo ='REFAL') THEN
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_identificador,130),2);
            ELSE
                v_valor_tag := FN_VALOR_CARACT_IDENTIF(v_identificador,2);
            END IF;

        END IF;

        --<plan>BA-1000</plan>
         IF (LOWER(p_tag)='plan') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
               --Verifica si comparte infraestructura con un BA
                v_servicio :='DATOS';
                v_producto :='INTER';
                v_identificador_comparte_infra := NULL;
                v_identificador_comparte_infra := FN_COMPARTE_INFRA (v_identificador, 'HFC', v_servicio,v_producto,p_pedido,p_subpedido,p_solicitud);
              IF (v_identificador_comparte_infra IS NULL) THEN
                v_valor_tag  :='TO';
              ELSE
                v_valor_tag := 'BA-'||FN_VALOR_CARACT_IDENTIF(FN_VALOR_CARACT_IDENTIF(v_identificador_comparte_infra,2126)||'-IC001',124);

             --<2012-01-02   JPulgarb
             v_uso := FN_OBTENER_TAG_CABLEM_PRODUCTO(p_pedido,p_subpedido,p_solicitud,p_identificador,p_elemento_id,p_producto,'uso',p_requerimiento);
             OPEN  c_plan_plataforma  (v_uso,replace(v_valor_tag,'BA-',NULL));
              FETCH c_plan_plataforma  INTO v_plan_plataf;
             CLOSE c_plan_plataforma;
             v_valor_tag := NVL(v_plan_plataf,v_valor_tag);
             -->

              END IF;
            ELSE
                v_valor_tag  :='TO';
            END IF;
         END IF;

        --<tipoEquipo>EMTA</tipoEquipo>
         IF (LOWER(p_tag)='tipoequipo') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
               BEGIN
                 v_equipo  := NULL;
                 v_equipo := NVL(FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'EQACCP',200),FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'CABLEM',200));
                 SELECT DECODE((SELECT COUNT(1)
                    FROM fnx_marcas_referencias
                    WHERE multiservicio_id=2
                    AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                   FROM fnx_equipos e
                                                   WHERE e.equipo_id  = v_equipo
                                                   )AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                   INTO v_valor_tag
                  FROM DUAL;
             EXCEPTION
              WHEN OTHERS THEN
                v_valor_tag := NULL;
             END;
            ELSIF (v_tipo_trabajo ='REFAL') THEN
                 v_equipo  := NULL;
                   BEGIN
                      SELECT trim(serie_id)
                      INTO v_equipo
                      FROM fnx_ordenes_trabajos o
                      WHERE requerimiento_id = v_requerimeinto
                      AND serie_id IS NOT NULL
                      AND estado_id = 'PENDI'
                      AND (o.cola_id, o.actividad_id) IN
                           (SELECT cp.cola_id, cp.actividad_id
                              FROM fnx_colas_productos cp
                             WHERE cp.tipo_trabajo = 'REFAL'
                               AND cp.tipo_elemento = 'EQU')
                      AND ROWNUM=1;
                   EXCEPTION
                      WHEN OTHERS THEN
                           v_equipo :=NULL;
                   END;

                   BEGIN
                         SELECT DECODE((SELECT COUNT(1)
                            FROM fnx_marcas_referencias
                            WHERE multiservicio_id=2
                            AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                           FROM fnx_equipos e
                                                           WHERE e.equipo_id  =v_equipo
                                                           ) AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                           INTO v_valor_tag
                          FROM DUAL;
                   EXCEPTION
                      WHEN OTHERS THEN
                        v_valor_tag := NULL;
                   END;
            ELSE
             BEGIN
                 SELECT DECODE((SELECT COUNT(1)
                    FROM fnx_marcas_referencias
                    WHERE multiservicio_id=2
                    AND (marca_id, referencia_id) IN (SELECT marca_id, referencia_id
                                                   FROM fnx_equipos e
                                                   WHERE e.identificador_id  =v_identificador)AND TIPO_ELEMENTO_ID='CABLEM'),1,'EMTA',0,'CABLEM','CABLEM') tipo_equipo
                   INTO v_valor_tag
                  FROM DUAL;
             EXCEPTION
              WHEN OTHERS THEN
                v_valor_tag := NULL;
             END;
            END IF;
         END IF;

        --<ciudad>MEDELLIN</ciudad>
         IF (LOWER(p_tag)='ciudad') THEN
            v_municipio := NULL;
            IF (v_tipo_trabajo ='NUEVO') THEN
               v_municipio := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',34);
            ELSE
               v_municipio :=FN_VALOR_CARACT_IDENTIF(v_identificador,34);
            END IF;

             BEGIN
                SELECT UPPER(nombre_municipio)
                INTO v_valor_tag
                FROM fnx_municipios
                WHERE  municipio_id= v_municipio ;
              EXCEPTION
              WHEN OTHERS THEN
                v_valor_tag := NULL;
             END;

         END IF;

         --<departamento>ANT</departamento>
         IF (LOWER(p_tag)='departamento') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
              v_valor_tag     :=  FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido,'TOIP',204);
            ELSE
             v_valor_tag     := FN_VALOR_CARACT_IDENTIF(v_identificador,204);
            END IF;
         END IF;
     END IF;

    -- 2.4 Tratamiento del producto CENTREX para obtener el tag pasado como parámetro (John Carlos Gallego - Analista Desarrollador MVM)
    -- Nota: El identificador obtenido para este proceso ya se obtuvo del numeral 1 de esta función
    IF (v_producto_id = 'CNTXIP') THEN
        -- Obtener los datos de mac del equipo CableModem.  Ej: <mac>00A0FC354B93</mac>
        IF (v_tipo_trabajo ='NUEVO') THEN
            IF (LOWER(p_tag) = 'mac') THEN
                v_sentencia_select := 'SELECT FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 200) '||
                                       ' INTO :v_valor '||
                                      'FROM DUAL';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            END IF;

            -- Tratamiento del mac nueva en nuevos servicios de Centrex (JGallg)
            IF (LOWER(p_tag) = 'macnueva') THEN
                -- Característica 200: identificación del equipo
                v_sentencia_select := 'SELECT FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 200) '||
                                       ' INTO :v_valor '||
                                      'FROM DUAL';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            END IF;
        ELSIF (v_tipo_trabajo ='CAMBI') THEN
            -- Para transacciones de cambio de equipo.
            IF (LOWER(p_tag) = 'macactual') THEN
                v_sentencia_select := 'SELECT EQUIPO_ID INTO :v_valor '||
                                        'FROM FNX_EQUIPOS '||
                                       'WHERE IDENTIFICADOR_ID = '''||v_identificador||''' '||
                                        ' AND TIPO_ELEMENTO_ID = ''CABLEM'' '||
                                        ' AND ROWNUM = 1';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            END IF;

            IF (LOWER(p_tag) = 'macnueva') THEN
                IF (v_tipo_trabajo ='REFAL') THEN
                    v_sentencia_select := 'SELECT TRIM(SERIE_ID) INTO :v_valor '||
                                            'FROM fnx_ordenes_trabajos o '||
                                           'WHERE REQUERIMIENTO_ID = '||v_requerimeinto||' '||
                                            ' AND SERIE_ID_ID IS NOT NULL '||
                                            ' AND estado_id = '||chr(39)||'PENDI'||chr(39)||
                                            ' AND (o.cola_id, o.actividad_id) IN '||
                                            ' (SELECT cp.cola_id, cp.actividad_id  '||
                                            ' FROM fnx_colas_productos cp '||
                                            ' WHERE cp.tipo_trabajo = '||chr(39)||'REFAL'||chr(39)||
                                            ' AND cp.tipo_elemento = '||chr(39)||'EQU'||chr(39)||')'||
                                            ' AND ROWNUM = 1';
                    v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
                ELSE
                    -- Característica 200: identificación del equipo
                    v_sentencia_select := 'SELECT FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 200) '||
                                           ' INTO :v_valor '||
                                          'FROM DUAL';
                    v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
                END IF;
            END IF;
        ELSE
            IF (LOWER(p_tag) = 'mac') THEN
                v_sentencia_select := 'SELECT EQUIPO_ID INTO :v_valor '||
                                        'FROM FNX_EQUIPOS '||
                                       'WHERE AGRUPADOR_ID = '''||v_identificador||''' '||
                                        ' AND TIPO_ELEMENTO_ID = ''CABLEM'' '||
                                        ' AND ROWNUM = 1';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            END IF;
        END IF;

        -- Obtener el identificador asociado al CableModem.  Ej: <identificador>43007564</identificador>
         IF (LOWER(p_tag) = 'identificador') THEN
            IF (v_tipo_trabajo = 'NUEVO') THEN
                v_valor_tag := FN_VALOR_CARACTERISTICA_SOL (p_pedido, p_subpedido, p_solicitud, 1);
            ELSIF (v_tipo_trabajo ='REFAL') THEN
                v_valor_tag:= v_identificador;
            ELSE
                v_valor_tag := fn_valor_para_tag (v_identificador, 1, NULL, NULL, 'identi', NULL);
            END IF;
        END IF;

        -- Obtener el uso asociado al CableModem. Ej: <uso>COM</uso>
        IF (LOWER(p_tag) = 'uso') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                -- Obtener el identificador agrupador de Centrex (JGallg)
                SELECT identificador_id INTO v_identif_agrupa
                FROM fnx_subpedidos
                WHERE  pedido_id = p_pedido AND subpedido_id = p_subpedido;

                -- Obtener la cantidad de sedes que tenga el agrupador (JGallg)
                BEGIN
                    SELECT COUNT(identificador_id) INTO v_cantidad_sedes
                    FROM fnx_identificadores
                    WHERE agrupador_id      = v_identif_agrupa
                      AND identificador_id <> v_identificador
                      AND tipo_elemento_id  = 'SEDECX';
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_cantidad_sedes := 0;
                    WHEN OTHERS THEN
                        v_cantidad_sedes := 0;
                END;

                -- De acuerdo a la cantidad de sedes, hacer la diferenciación de
                -- nuevo servicio centrex o adición de sede a centrex existente (JGallg)
                IF NVL(v_cantidad_sedes, 0) = 0 THEN
                    v_valor_tag := fn_valor_caract_subelem(p_pedido, p_subpedido, 'CNTXIP', 2);
                ELSE
                    v_valor_tag := fn_valor_para_tag (v_identif_agrupa, 2, NULL, NULL, 'caract', NULL);
                END IF;
            ELSIF (v_tipo_trabajo = 'REFAL') THEN
                SELECT FN_VALOR_CARACT_IDENTIF(v_identificador,90) INTO v_identif_agrupa FROM DUAL;
                v_valor_tag := fn_valor_para_tag (v_identif_agrupa, 2, NULL, NULL, 'caract', NULL);
            ELSE
                -- Obtención del uso estandar (JGallg)
                SELECT FN_VALOR_CARACT_IDENTIF(v_identificador,90) INTO v_identif_agrupa FROM DUAL;
                v_valor_tag := fn_valor_para_tag (v_identif_agrupa, 2, NULL, NULL, 'caract', NULL);
            END IF;
        END IF;

        -- Obtener el plan asociado al producto ej: <plan>BA-1000</plan>
        IF (LOWER(p_tag) = 'plan') THEN
            v_valor_tag := 'CNTXIP';
        END IF;

        -- Obtener el tipo de equipo.  Ej:  <tipoEquipo>EMTA</tipoEquipo>
        IF (LOWER(p_tag) = 'tipoequipo') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                v_sentencia_select := 'SELECT DECODE((SELECT COUNT(1) '||
                                                     'FROM FNX_MARCAS_REFERENCIAS '||
                                                     'WHERE MULTISERVICIO_ID = 2 '||
                                                      ' AND (MARCA_ID, REFERENCIA_ID) IN (SELECT MARCA_ID, REFERENCIA_ID '||
                                                                                         'FROM FNX_EQUIPOS E '||
                                                                                         'WHERE E.IDENTIFICADOR_ID = FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 200)) AND TIPO_ELEMENTO_ID = ''CABLEM''), '||
                                                    '1, ''EMTA'', 0, ''CABLEM'', ''CABLEM'') TIPO_EQUIPO '||
                                      'INTO :v_valor '||
                                      'FROM DUAL';
            ELSIF (v_tipo_trabajo ='REFAL') THEN
                -- Obtener los datos del equipo
                v_sentencia_select := 'SELECT TRIM(SERIE_ID) INTO :v_valor '||
                                      'FROM fnx_ordenes_trabajos o '||
                                      'WHERE REQUERIMIENTO_ID = '||v_requerimeinto||' '||
                                       ' AND SERIE_ID_ID IS NOT NULL '||
                                       ' AND estado_id = '||chr(39)||'PENDI'||chr(39)||
                                       ' AND (o.cola_id, o.actividad_id) IN '||
                                       ' (SELECT cp.cola_id, cp.actividad_id  '||
                                       ' FROM fnx_colas_productos cp '||
                                       ' WHERE cp.tipo_trabajo = '||chr(39)||'REFAL'||chr(39)||
                                       ' AND cp.tipo_elemento = '||chr(39)||'EQU'||chr(39)||')'||
                                       ' AND ROWNUM = 1';
                v_equipo := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);

                -- Obtener los datos del tipo de equipo
                v_sentencia_select := 'SELECT DECODE((SELECT COUNT(1) '||
                                                     'FROM FNX_MARCAS_REFERENCIAS '||
                                                     'WHERE MULTISERVICIO_ID = 2 '||
                                                      ' AND (MARCA_ID, REFERENCIA_ID) IN (SELECT MARCA_ID, REFERENCIA_ID '||
                                                                                         'FROM FNX_EQUIPOS E '||
                                                                                         'WHERE E.EQUIPO_ID = '''||v_equipo||''' '||
                                                                                         ') '||
                                                      ' AND TIPO_ELEMENTO_ID=''CABLEM''),1,''EMTA'',0,''CABLEM'',''CABLEM'') TIPO_EQUIPO '||
                                      'INTO :v_valor '||
                                      'FROM DUAL';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            ELSE
                v_sentencia_select := 'SELECT DECODE((SELECT COUNT(1) '||
                                                     'FROM FNX_MARCAS_REFERENCIAS '||
                                                     'WHERE MULTISERVICIO_ID = 2 '||
                                                      ' AND (MARCA_ID, REFERENCIA_ID) IN (SELECT MARCA_ID, REFERENCIA_ID '||
                                                                                         'FROM FNX_EQUIPOS E '||
                                                                                         'WHERE E.IDENTIFICADOR_ID = '''||v_identificador||''') AND TIPO_ELEMENTO_ID = ''CABLEM''), '||
                                                    '1, ''EMTA'', 0, ''CABLEM'', ''CABLEM'') TIPO_EQUIPO '||
                                      'INTO :v_valor '||
                                      'FROM DUAL';
            END IF;
            v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
        END IF;

        -- Obtener el nombre completo de la ciudad.  Ej: <ciudad>MEDELLIN</ciudad>
        IF (LOWER(p_tag) = 'ciudad') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                v_sentencia_select := 'SELECT UPPER(NOMBRE_MUNICIPIO) '||
                                      'INTO :v_valor '||
                                      'FROM FNX_MUNICIPIOS '||
                                      'WHERE MUNICIPIO_ID = FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 34)';
            ELSE
                -- John Carlos Gallego (JGallg) - 25-10-2010 - Los daños y las otras transacciones se dan en el identificador de la respectiva sede.
                v_sentencia_select := 'SELECT UPPER(NOMBRE_MUNICIPIO) '||
                                      'INTO :v_valor '||
                                      'FROM FNX_MUNICIPIOS '||
                                      'WHERE MUNICIPIO_ID = FN_VALOR_CARACT_IDENTIF('''||v_identificador||''', 34)';
            END IF;
            v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
        END IF;

         -- Obtener el código del departamento. Ej: <departamento>ANT</departamento>
        IF (LOWER(p_tag) = 'departamento') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                v_sentencia_select := 'SELECT FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 204) '||
                                       ' INTO :v_valor '||
                                      'FROM DUAL';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            ELSE
                -- John Carlos Gallego (JGallg) - 25-10-2010 - Los daños y las otras transacciones se dan en el identificador de la respectiva sede.
                v_valor_tag := fn_valor_para_tag (v_identificador, 204, NULL, NULL, 'caract', NULL);
            END IF;
        END IF;
    END IF;

    -- 2.4 Tratamiento del producto TRONCALES para obtener el tag pasado como parámetro (John Carlos Gallego - Analista Desarrollador MVM)
    IF (v_producto_id = 'TRKSIP') THEN
        -- Obtener los datos de mac del equipo CableModem.  Ej: <mac>00A0FC354B93</mac>
        IF (v_tipo_trabajo ='NUEVO') THEN
            IF (LOWER(p_tag) = 'mac') THEN
                v_sentencia_select := 'SELECT FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 200) '||
                                       ' INTO :v_valor '||
                                      'FROM DUAL';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            END IF;

            -- Tratamiento del mac nueva en nuevos servicios de Troncales (JGallg)
            IF (LOWER(p_tag) = 'macnueva') THEN
                -- Característica 200: identificación del equipo
                v_sentencia_select := 'SELECT FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 200) '||
                                       ' INTO :v_valor '||
                                      'FROM DUAL';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            END IF;
        ELSIF (v_tipo_trabajo ='CAMBI') THEN
            -- Para transacciones de cambio de equipo.
            IF (LOWER(p_tag) = 'macactual') THEN
                v_sentencia_select := 'SELECT EQUIPO_ID INTO :v_valor '||
                                        'FROM FNX_EQUIPOS '||
                                       'WHERE IDENTIFICADOR_ID = '''||v_identificador||''' '||
                                        ' AND TIPO_ELEMENTO_ID = ''CABLEM'' '||
                                        ' AND ROWNUM = 1';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            END IF;

            IF (LOWER(p_tag) = 'macnueva') THEN
                IF (v_tipo_trabajo ='REFAL') THEN
                    v_sentencia_select := 'SELECT TRIM(SERIE_ID) INTO :v_valor '||
                                            'FROM fnx_ordenes_trabajos o '||
                                           'WHERE REQUERIMIENTO_ID = '||v_requerimeinto||' '||
                                            ' AND SERIE_ID_ID IS NOT NULL '||
                                            ' AND estado_id = '||chr(39)||'PENDI'||chr(39)||
                                            ' AND (o.cola_id, o.actividad_id) IN '||
                                            ' (SELECT cp.cola_id, cp.actividad_id  '||
                                            ' FROM fnx_colas_productos cp '||
                                            ' WHERE cp.tipo_trabajo = '||chr(39)||'REFAL'||chr(39)||
                                            ' AND cp.tipo_elemento = '||chr(39)||'EQU'||chr(39)||')'||
                                            ' AND ROWNUM = 1';
                    v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
                ELSE
                    -- Característica 200: identificación del equipo
                    v_sentencia_select := 'SELECT FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 200) '||
                                           ' INTO :v_valor '||
                                          'FROM DUAL';
                    v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
                END IF;
            END IF;
        ELSE
            IF (LOWER(p_tag) = 'mac') THEN
                v_sentencia_select := 'SELECT EQUIPO_ID INTO :v_valor '||
                                        'FROM FNX_EQUIPOS '||
                                       'WHERE IDENTIFICADOR_ID = '''||v_identificador||''' '||
                                        ' AND TIPO_ELEMENTO_ID = ''CABLEM'' '||
                                        ' AND ROWNUM = 1';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            END IF;
        END IF;

        -- Obtener el identificador asociado al CableModem.  Ej: <identificador>43007564</identificador>
         IF (LOWER(p_tag) = 'identificador') THEN
            IF (v_tipo_trabajo = 'NUEVO') THEN
                v_valor_tag := FN_VALOR_CARACTERISTICA_SOL (p_pedido, p_subpedido, p_solicitud, 1);
            ELSIF (v_tipo_trabajo ='REFAL') THEN
                v_valor_tag:= v_identificador;
            ELSE
                v_valor_tag := fn_valor_para_tag (v_identificador, 1, NULL, NULL, 'identi', NULL);
            END IF;
        END IF;

        -- Obtener el uso asociado al CableModem. Ej: <uso>COM</uso>
        IF (LOWER(p_tag) = 'uso') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                -- Obtener el identificador agrupador de Troncales (JGallg)
                SELECT identificador_id INTO v_identif_agrupa
                FROM fnx_subpedidos
                WHERE  pedido_id = p_pedido AND subpedido_id = p_subpedido;

                -- Obtener la cantidad de sedes que tenga el agrupador (JGallg)
                BEGIN
                    SELECT COUNT(identificador_id) INTO v_cantidad_sedes
                    FROM fnx_identificadores
                    WHERE agrupador_id      = v_identif_agrupa
                      AND identificador_id <> v_identificador
                      AND tipo_elemento_id  = 'SEDEIP';
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_cantidad_sedes := 0;
                    WHEN OTHERS THEN
                        v_cantidad_sedes := 0;
                END;

                -- De acuerdo a la cantidad de sedes, hacer la diferenciación de nuevo
                -- servicio troncales o adición de sede a troncales sip existente (JGallg)
                IF NVL(v_cantidad_sedes, 0) = 0 THEN
                    v_valor_tag := FN_VALOR_CARACT_SUBELEM(p_pedido, p_subpedido, 'TRKSIP', 2);
                ELSE
                    v_valor_tag := fn_valor_para_tag (v_identif_agrupa, 2, NULL, NULL, 'caract', NULL);
                END IF;
            ELSIF (v_tipo_trabajo ='REFAL') THEN
                SELECT FN_VALOR_CARACT_IDENTIF(v_identificador,90) INTO v_identif_agrupa FROM DUAL;
                v_valor_tag := fn_valor_para_tag (v_identif_agrupa, 2, NULL, NULL, 'caract', NULL);
            ELSE
                SELECT FN_VALOR_CARACT_IDENTIF(v_identificador,90) INTO v_identif_agrupa FROM DUAL;
                v_valor_tag := fn_valor_para_tag (v_identif_agrupa, 2, NULL, NULL, 'caract', NULL);
            END IF;
        END IF;

        -- Obtener el plan asociado al producto ej: <plan>BA-1000</plan>
        IF (LOWER(p_tag) = 'plan') THEN
            v_valor_tag := 'TRKSIP';
        END IF;

        -- Obtener el tipo de equipo.  Ej:  <tipoEquipo>EMTA</tipoEquipo>
        IF (LOWER(p_tag) = 'tipoequipo') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                v_sentencia_select := 'SELECT DECODE((SELECT COUNT(1) '||
                                                     'FROM FNX_MARCAS_REFERENCIAS '||
                                                     'WHERE MULTISERVICIO_ID = 2 '||
                                                      ' AND (MARCA_ID, REFERENCIA_ID) IN (SELECT MARCA_ID, REFERENCIA_ID '||
                                                                                         'FROM FNX_EQUIPOS E '||
                                                                                         'WHERE E.IDENTIFICADOR_ID = FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 200)) AND TIPO_ELEMENTO_ID = ''CABLEM''), '||
                                                    '1, ''EMTA'', 0, ''CABLEM'', ''CABLEM'') TIPO_EQUIPO '||
                                      'INTO :v_valor '||
                                      'FROM DUAL';
            ELSIF v_tipo_trabajo = 'REFAL' THEN
                -- Obtener los datos del equipo
                v_sentencia_select := 'SELECT TRIM(SERIE_ID) INTO :v_valor '||
                                      'FROM fnx_ordenes_trabajos o '||
                                      'WHERE REQUERIMIENTO_ID = '||v_requerimeinto||' '||
                                       ' AND SERIE_ID_ID IS NOT NULL '||
                                       ' AND estado_id = '||chr(39)||'PENDI'||chr(39)||
                                       ' AND (o.cola_id, o.actividad_id) IN '||
                                       ' (SELECT cp.cola_id, cp.actividad_id  '||
                                       ' FROM fnx_colas_productos cp '||
                                       ' WHERE cp.tipo_trabajo = '||chr(39)||'REFAL'||chr(39)||
                                       ' AND cp.tipo_elemento = '||chr(39)||'EQU'||chr(39)||')'||
                                       ' AND ROWNUM = 1';
                v_equipo := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);

                -- Obtener los datos del tipo de equipo
                v_sentencia_select := 'SELECT DECODE((SELECT COUNT(1) '||
                                                     'FROM FNX_MARCAS_REFERENCIAS '||
                                                     'WHERE MULTISERVICIO_ID = 2 '||
                                                      ' AND (MARCA_ID, REFERENCIA_ID) IN (SELECT MARCA_ID, REFERENCIA_ID '||
                                                                                         'FROM FNX_EQUIPOS E '||
                                                                                         'WHERE E.EQUIPO_ID = '''||v_equipo||''' '||
                                                                                         ') '||
                                                      ' AND TIPO_ELEMENTO_ID=''CABLEM''),1,''EMTA'',0,''CABLEM'',''CABLEM'') TIPO_EQUIPO '||
                                      'INTO :v_valor '||
                                      'FROM DUAL';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            ELSE
                v_sentencia_select := 'SELECT DECODE((SELECT COUNT(1) '||
                                                     'FROM FNX_MARCAS_REFERENCIAS '||
                                                     'WHERE MULTISERVICIO_ID = 2 '||
                                                      ' AND (MARCA_ID, REFERENCIA_ID) IN (SELECT MARCA_ID, REFERENCIA_ID '||
                                                                                         'FROM FNX_EQUIPOS E '||
                                                                                         'WHERE E.IDENTIFICADOR_ID = '''||v_identificador||''') AND TIPO_ELEMENTO_ID = ''CABLEM''), '||
                                                    '1, ''EMTA'', 0, ''CABLEM'', ''CABLEM'') TIPO_EQUIPO '||
                                      'INTO :v_valor '||
                                      'FROM DUAL';
            END IF;
            v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
        END IF;

        -- Obtener el nombre completo de la ciudad.  Ej: <ciudad>MEDELLIN</ciudad>
        IF (LOWER(p_tag) = 'ciudad') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                v_sentencia_select := 'SELECT UPPER(NOMBRE_MUNICIPIO) '||
                                      'INTO :v_valor '||
                                      'FROM FNX_MUNICIPIOS '||
                                      'WHERE MUNICIPIO_ID = FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 34)';
            ELSE
                -- John Carlos Gallego (JGallg) - 25-10-2010 - Los daños y las otras transacciones se dan en el identificador de la respectiva sede.
                v_sentencia_select := 'SELECT UPPER(NOMBRE_MUNICIPIO) '||
                                      'INTO :v_valor '||
                                      'FROM FNX_MUNICIPIOS '||
                                      'WHERE MUNICIPIO_ID = FN_VALOR_CARACT_IDENTIF('''||v_identificador||''', 34)';
            END IF;
            v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
        END IF;

         -- Obtener el código del departamento. Ej: <departamento>ANT</departamento>
        IF (LOWER(p_tag) = 'departamento') THEN
            IF (v_tipo_trabajo ='NUEVO') THEN
                v_sentencia_select := 'SELECT FN_VALOR_CARACTERISTICA_SOL ('''||p_pedido||''', '|| TO_CHAR(p_subpedido) || ', ' || TO_CHAR(p_solicitud) || ', 204) '||
                                       ' INTO :v_valor '||
                                      'FROM DUAL';
                v_valor_tag := fn_valor_para_tag (v_identificador, NULL, NULL, NULL, 'select', v_sentencia_select);
            ELSE
                -- John Carlos Gallego (JGallg) - 25-10-2010 - Los daños y las otras transacciones se dan en el identificador de la respectiva sede.
                v_valor_tag := fn_valor_para_tag (v_identificador, 204, NULL, NULL, 'caract', NULL);
            END IF;
        END IF;
    END IF;

    RETURN(trim(v_valor_tag));

EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error en FN_OBTENER_TAG_CABLEM_PRODUCTO: '||SQLERRM||' ['||TO_CHAR(SQLCODE)||'].');
        RETURN (NULL);
END;