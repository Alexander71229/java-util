PROCEDURE       PR_ESTUDIO_TECNICO_TELEV (
   w_pedido   IN       fnx_pedidos.pedido_id%TYPE,
   w_resp     IN OUT   fnx_caracteristica_solicitudes.valor%TYPE
) IS

-- Organizacion:              Empresas Publicas de Medellin E.S.P
--                            Area Informatica Telecomunicaciones.
-- Creacion:                  Paulo Antonio Mosquera G.
-- HISTORIA DE MODIFICACIONES
-- Fecha       Persona      Observaciones
-- ----------  -----------  ------------------------------------------------------
-- 2002-2007               .....
-- 2007-09-13  Jrincong     Verificación de uso de variable w_concepto_id - cambio por
--                          w_concepto
-- 2008-07-17  Aacosta      Modificación de la forma como se valida si el tipo de paquete
--                          está configurado para que el componente de paquete genere orden.
--                          VALIDACIÓN ANTERIRO: IF NVL(v_tipo_paquete, 'NULO') IN ('OFERTA','UNETE1','UNETE2','UNETE3') THEN
-- 2008-07-18  Aacosta      Modificación del cursor c_solicitudes, con el fin de adicionar el nuevo componente SERV que aplica
--                          para Cableras.
-- 2008-07-18  Aacosta      Provisión del componente servicio de configuración SERV
--                          para Televisión. Se implementó para Cableras pero queda genérico.
-- 2008-07-22  Aacosta      Se valida que la página asociada no sea nula o que tenga una longitud menor de 18
--                          caractéres.
-- 2008-10-04  Jrincong     SUIN 1096181
--                          Buscar sí existe un componente de Nueva TELEVision en el mismo pedido y diferente al
--                          subpedido.  En tal caso, se evalua como un cambio de producto
-- 2008-10-04  Jrincong     Uso de procedimiento para asignación con uso de zonas de influencia
--                          Asignación de red HFC con base en ZONAS DE INFLUENCIA */
-- 2008-10-04  Jrincong     Uso de procedimientos para buscar acceso HFC en pedido o en trámite y así permitir
--                          automatización para instalaciones con Banda ancha instalada o ya provisionada
-- 2008-10-06  Jrincong     Variable msg_observac.  Adición de variables v_tipo de acceso y tramo de red asignada
-- 2008-10-06  Jrincong     Invocación de procs. PKG_PROVISION_TELEV.  Uso de variable w_agrupador, para actualizar
--                          en lugar de w_identificador
-- 2008-10-06  Jrincong     Implementación parametro w_ea_buscar_zona_inf para determinar sí para un municipio se hace
--                          busqueda de infraestructura con base en zonas de influencia
-- 2008-10-08  Jrincong     Lógica para considerar enrutamiento de solicitudes de Televisión con base en tabla
--                          FNX_RUTAS_PROVISION.
-- 2008-12-30  Aacosta      Se incluye el nuevo parámetro Booleano al invocar el procedimiento, con el fin que
--                          no se validen los equipos de tipo SET UP BOX.
-- 2008-12-31  Aacosta      Se valida la salida el procedimiento de enrutamiento de componentes Telev,
--                          con el fin que cuando venga un equipos Set Up Box de alta definición, no se
--                          generen las ordenes correspondientes, sino que se deje el pedido en PETEC
--                          para validar si cumple con la distancia mínima requerida.
-- 2009-04-07  Jrincong     Se modifica la manera en la que se considera la condiciòn de no actualizaciòn.
--                          Ya no se utiliza la instrucciòn EXIT para salir del ciclo, pues con el cambio de base de datos,
--                          la manera en que se realiza el ORDER BY principal es diferente.
-- 2009-06-20  Lquintez     Se hace modificación para que los mixtos y los mixtos basicos queden eb PORDE cuando no hay INSIP
-- 20090623    Lquintez     Se modifica lógica para que en los cambio de tecnologia MIXTO a MIXTO BASICO y viceversa o
--                          BASICO a REDCO y viceversa pase la TELEV  a PORDE, no debe llegar otro componente
-- 2009-06-23  Lquintez     Se actualiza logica por el valor de retorno ltcar de la caracteristica 3849
-- 2009-09-24  Etachej      SAC Equipos Thomson - Traslados.
--             Ysolarte     Se realiza inserción de la solicitud de EQACCP para traslado.
-- 2009-10-10  Etachej      SAC Equipos Thomson - Traslados.
--             Ysolarte     Se realiza validación para que solo inserte la solicitud de EQACCP para planes comerciales <> ('HFC','PAR')
-- 2009-10-10  Truizd       SAC Traslado de Paquetes - Se agrega NVL porque para traslados de paquetes el identificador de paquete
--                          viene en la caracteritica 4371(Identif Paquete Traslado).
-- 2009-10-13  Jaristz      Se realiza la validacion del concepto PLICO para el tema de Constructores.
-- 2010-02-04  Jsierrao     Se declaran las variables para que cuando las observaciones superen los 1000 caractereres, los pedidos
--                          no se queden en petec y se invoca al actualizar las solicitudes de la tv y se incluye en fnx_parametros
--                          modulo ESTUD_TEC (nuevo).
-- 2010-05-05  Lgonzalg     Uso de procedimiento para generar puentes de configuraciòn / desconfiguracion cuando
--                          se retira un servicio de TvDig INSIP . El procedimiento verifica condiciones de Multiservicio.
-- 2010-05-10  Lgonzalg     Agregar cursor c_rutasprov para consultar la variable CONTROL_PUENTES.
-- 2010-05-10  Lgonzalg     Verificar si se deben generar puentes. Se consulta la variable CONTROL_PUENTES de rutas provisión.
-- 2010-06-03  Truizd       Se valida por la caracteristica 2005 para determinar si es un global Individual para no asiganar
--                          red de acceso ya que estos tienen el servicio instalado.
-- 2010-06-16  Aacosta      Se crea el nuevo cursor c_soli_insta, con el fin de verificar si en el pedido llega una solicitud de
--                          INSIP o INSHFC en estado TECNI. en caso de venir no se debe aprovisionar ninguno de los demas
--                          componentes del pedido.
-- 2010-07-21  Jaristz      Actualizacion de las solicitudes que se ecnuentran en concepto
--                          PEXPQ y PXSLN al concepto PLICO.
-- 2010-07-22  Wmendoza     Se comenta el siguiente código a fin de poder realizar las validaciones
--                          para otro tipos de SET UP BOX.
--                          IF NVL(v_tipo_equipo, 'NING') = 'STBAD' THEN
--                              b_stbox_hdd := TRUE;
--                          END IF;
-- 2010-07-22  Wmendoza     Se verifica si el equipo que viene, corresponde a un set up box de alta definición.
-- 2010-07-29  Wmendoza     Cursor para verificar que viene un EQURED en trabajos de la solicitud
-- 2010-07-29  Wmendoza     Cursor para verificar que si viene en el INSIP un cambio en la 2909
-- 2010-07-29  Wmendoza     Validación para poder colocar en PORDE una solicitud de INSIP.
-- 2010-07-29  Wmendoza     Solo en el caso que venga un cambio en el INSIP y que venga nuevo EQURED el INSIP se coloca en PORDE.
-- 2010-08-02  Jrincong     Cursor c_rutasprov.  Adicion de campos aa_switche_asignacion, aa_zonainf_hfc, aa_buscar_reuso_red
--                          para verificaciòn de configuración de Asignación automàtica.
-- 2010-08-02  Jrincong     Búsqueda de configuracion para variables de Asignación Automàtica asociadas al municipio,
--                          producto y tecnología..
-- 2010-08-02  Jrincong     Si la variable v_munic_salida_int se asigna el valor TRUE a w_incons y la variable vo_enrutar_et
--                          se hace nula para evitar enrutamiento erróneo.
-- 2010-08-02  Jrincong     Se cambia la variable w_ea_buscar_red_acceso por la variablle v_rutasprov.aa_buscar_reuso_red
-- 2010-08-02  Jrincong     Se cambia la variable w_ea_buscar_zona_inf por la variable v_rutasprov.aa_zonainf_hfc
-- 2010-08-02  Jrincong     Establecer variable de Estudio Tècnico Automàtico Habilitado con base en campo AA_SWITCHE_ASIGNACION
-- 2010-08-02  Jrincong     Se estable variable v_tipo_acceso con el valor NO-DEFINIDO para indicar que no se realizó
--                          el proceso de búsqueda de red de acceso.
-- 2010-08-02  Jrincong     Se agrega condicion b_estudio_automatico en secciòn para busqueda de red de acceso
-- 2010-08-02  Jrincong     Se agregan condiciones vo_enrutar_et = 'S' AND b_estudio_automatico para sección donde
--                          se realiza la bùsqueda de a través de zonas de influencia
-- 2010-08-02  Jrincong     Se utiliza condicion v_sin_automatica = 0 para verificar sí se puede realizar estudio de
--                          componente INSHFC.
-- 2010-08-02  Jrincong     Se quita el uso del parametro PRV-SERV * 9 dada la nueva implementación con base en la tabla
--                          FNX_RUTAS_PROVISION.
-- 2010-08-02  Jrincong     Se utiliza el dominio CABLERA_PAGINA_VALOR_3840 en lugar del dominio anterior CABLERA.
--                          Antes la condicion comparaba el municipio de la solicitud (no el de servicio), Para el caso
--                          de Cali no funcionaba, debido a que las solicitudes de Cali, ingresan por el portafolio de
--                          Medellin.
-- 2010-08-02  Jrincong     Se utiliza el dominio CABLERA_SIN_TERMINAL_DERIV para verificar sí para el municipio de
--                          servicio se hace la verificación de la red, sólo hasta el elemento DERIVADOR en
--                          los casos que no exista el terminal o boca.
-- 2010-08-02  Jrincong     Se agrega valor b_sin_terminal_deriv para uso de parametro en procedimientos PR_RED_EXISTENTE
--                          y PR_RED_TRAMITE
-- 2010-08-04  Wmendoza     Se agrega el siguiente código para poder cumplir las solicitudes de equipos
-- 2010-08-06  Jrincong     Actualización de identificador ID en tabla FNX_INF_TV_TRAMITES para la transaccion
--                          de traslado
-- 2010-08-26  Jrincong     Se agrega la asignaciòn msg_observac = 'AA-R' para indicar que se hizo asignaciòn automàtica
--                          con Red o sin Red sobre el elemento INSHFC.
-- 2010-08-26  Jrincong     Se quita el literal (AA) en las instrucciones de actualizaciòn de la solicitud, dado que no
--                          para todas las transacciones tiene sentido el término Asignación Automàtica.
-- 2010-08-26  Jrincong     Se verifica sí en la variable v_mensaje_asignacion viene el literal @PAR, para
--                          determinar el tipo de tecnologìa (PAR)ABOLICA o HFC. Inicialmente, la asignaciòn a través
--                          de tecnología Parabólica puede venir del proc. de asignación de zonas de influencia.
-- 2010-09-02  Wmendoza     Cursor para equipos en el nuevo tema de equipos set top box.
-- 2010-09-02  Wmendoza     <Sección Nuevo enrutamiento>
--                          Se crea lógica a Fin de poder colocar genérica la manera en la que se deben enrutar.
--                          los STBOX HD, teniendo cuenta las peticiones que se puedan presentar, por tal motivo se comenta
--                          la forma en la que se enrutaban.
-- 2010-09-08  Jaristz      Para constrcutores se quita la validacion de que las solicitudes no deben estar liquidadas para ponerlas en
--                          concepto PLICO.
-- 2010-09-21  Wmendoza     En adelante se utilizará el procedimiento PR_VERIFICAR_CICLO_CORRERIA
--                          para realizar las verificaciones y saber si un pedido se coloca en concepto PUMED.
-- 2010-09-23  Wmendoza     Se actualiza la solicitud a estado PUMED y se sale del ciclo para evitar que se creen
--                          las demás solicitudes.
-- 2010-09-28  Wmendoza     Se actualiza solo cuando se trata de una solicitud que no está en INGRE-PUMED.
-- 2010-09-29  Wmendoza     Se coloca la validación para saber si es un pedido que debe quedar en INGRE-PUMED
-- 2010-09-28  Wmendoza     Se agrega el campo identificador_id_nuevo.
-- 2010-10-19  Wmendoza     Sección de validación de demostración. Se encuentra el valor de la caracteristica
--                          3851, en caso de venir, se activa la variable b_demostracion para determinar que se trata de una demostración.
-- 2010-10-20  Wmendoza     Para el tema de cambio de paquete, se debe realizar la validación con relación al elemento
--                          SERVIP.
-- 2010-11-03  Wmendoza     Se coloca en estado PCPRO solo cuando no se trata de una demostración.
-- 2010-12-13  Aacosta      Se adiciona el nuevo parámetro estrato, con el fin de poder actualizar este dato en el pedido de Television.
--                          Proyecto Bogota.
-- 2010-12-13  Aacosta      Actaulizacion del estrato (Caract 37) en el pedido de televisión para zonas de influencia de Bogota.
-- 2010-12-13  Aacosta      Para el caso que haya un servicio instalado, se debe tomar el estrato (caract 37) del
--                          servicio (SRV) y actualizarlo en el pedido de BA. Proyecto Bogota.
-- 2011-03-15  Dojedac      Proyecto desaprovisionamiento. Se hacen los ajustes para poder crear la solicutud recuperación de equipos cuando se efectua una operación de retiro.
-- 2011-03-27  Dojedac      Determinar cantidad de equipos asociados al servicio de televisión
-- 2011-08-02  Wmendoza     Lógica para el nuevo elemento ELSIS. Se debe colocar en PORDE
--                          el componente ELSIS que viene como adición.
-- 2011-08-02  Wmendoza     Cursor que se utilizará para determinar si en un pedido viene un tipo elemento
--                          igual a SRV y si viene un tipo elemento id ELSIS.
-- 2011-09-14  Wmendoza     Se agrega la condición para el tipo de tecnología IP.
-- 2011-11-21  Wmendoza     Se coloca el valor de REDCO en vez de IP.
-- 2011-12-13  Aacosta      Validar para cambios de plan de mixto a basico o mixto a platino que cuando venga esta peticion solo suba los equipos
--                          del componente que se esté retirando, sea INSIP o INSHFC.
-- 2011-12-14  Aacosta      Se comentarea la lógica siguiente debido que se valida en el bloque anterior.
-- 2012-01-27  JGallg       Desaprov-NPE: Quitar lógica que genera solicitud de carta, pues no se necesita y ocasiona problemas de
--                          llave primaria.  Se descomenta clausula ORDER BY del cursor C_SOLICITUDES.
-- 2012-04-10  JGallg       MejorasDesaprov: Obtener dato de propiedad del equipo por medio de cursor.
-- 2012-04-10  JGallg       MejorasDesaprov: Función interna para validar si se genera o no la solicitud de recuperación de equipo y se centraliza
--                          validación en un solo punto.
-- 2012-04-10  JGallg       MejorasDesaprov: Procedimiento interno para paso de solicitud a ORDEN - PORDE y llamado después de generar la solicitud.
-- 2012-07-03  Yhernana     MejorasDesaprov: Se cumplen las solicitudes de RETIR de TELEV e INSHFC que se encuentran Suspendidos Por Falta de Pago y
--                          que no tienen órdenes generadas.
-- 2012-07-05  Wmendoza     Se modifica el tipo de elemento que se manejaba en demostraciones, de 
--                          ahora en adelante, será con el SERVIP.
-- 2012-07-26  DGiralL      Se incluye funcionalidad para actualizar la caract. 4094 (Red), dependiendo de la direccionalidad del nodo optico
-- 2012-08-08  Wmendoza     Se modifica el cursor para que de ahora en adelante, acepte también el tipo elemento
--                          SERHFC.
-- 2012-08-21  Yhernana     MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo tiene infraestructura compartida
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es compartido
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es propiedad del cliente
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que no se debe generar Solicitud Recuperación equipos
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el equipo es Obsoleto
--                          MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que no se encontraron equipos.
-- 2012-09-13  DGiralL      En el cursor C_EQU cuando una solicitud no tiene la caract.33=REDCO, se valida la tecnologia sobre el identificador
-- 2013-01-24  Mmedinac     Req20216: Mejora Superplay Adicion función fn_Verificar_PendET y procedimiento PR_GENORDEN_PEXPQ para identificar
--                          si existen subpedidos pendientes de estudio técnico en el paquete y evitar que los otros subpedidos se vayan a PORDE 
-- 2013-02-19  Yhernana     Se obtiene el ID asociado y se pasa como parametro en el PR_SOLICITUD_RETIR_EQUIPO.
-- 2013-06-13  Mmedinac     Se adiciona modificación para que los pedidos de constructores pasen correctamente a estado PLICO  
-- 2013-06-12  jvillabu     Se adiciona llamado al PR_CREAR_SOLIC_EQUI_CANAL para inclusion de equipo regalo y canal siembra
--                          en nuevos planes de television HFC, estratos 5 y 6
-- 2013-09-03  Aarbob       si el equipo es Comprado (1093-propeidad del equipo valor "CM"), se debe garantizar que no se genere la solictud de recogida del equipo y el decodificador debe quedar en estado RES, para una posible reutilización del EQUIPO, REQ28278_Tangibilizacion
-- 2013-10-03  Yhernana     REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
--                          Generar solicitud de recogida de equipos en peticiones de retiro cuando se 
--                          comparte infraestructura con otro producto que también se está retirando.
-- 2014-01-15  lhernans     Automatización cola DSLAM. Lógica para inserción de característica que genera evento.
-- 2013-12-05  Jrendbr      Se agrega el procedimiento encargado de insertar las etiquetas DSLAM PR_INSERTAR_ETIQUETAS_BTS
-- 2014-02-24  Jrendbr      Se adiciona logica para que tenga en cuenta los esatod de los equipos NIR.
-- 2014-04-07  Wmendoza     Si el equipo es de fidelización, se debe generar la solicitud de equipo.
-- 2014-04-11  Jrendbr      Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los 
--                          que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
-- 2014-04-21  Jrendbr      Se adiciona logica para que sea enviado el identifcador con el que comparte infraestructura
-- 2014-06-25  Jrincong     Req 36900 GPON Hogares.  Función fni_generar_solic_equipo. Consideración de teconología GPON para verificación 
--                          de Infraestructura compartida activa.
-- 2014-06-25  Jrincong     Req 36900 GPON Hogares.  Función fni_generar_solic_equipo. Consideración de tecnología GPON para verificar Equipo Compartido.
-- 2014-06-25  Jrincong     Req 36900. GPON Hogares. Sección Desaprovisionamiento. 
--                          Consideración de tecnologia GPON para verificación de Infraestructura compartida.
-- 2014-06-27  Jrincong     Req 36900 GPON Hogares. Retiros de Accesos INSIP 
-- 2014-06-27  Jrincong     Req 36900 GPON Hogares. La variable tecnología se asigna con el valor de la tecnología del identificador. 
-- 2014-06-27  Jrincong     Req 36900 GPON Hogares. La variable tecnología se asigna previamente con el valor de la tecnología del identificador.  
--                          Antes estaba REDCO y generaba problemas para accesos con tecnología GPON.                 
-- 2014-06-27  Jrincong     Req 36900 GPON Hogares. Req 36900 GPON Hogares. Se agrega condición de tecnología REDCO para la generación de puentes.  
--                          No es necesario cuando la tecnología es GPON. 
-- 2014-06-27  Jrincong     Req 36900 GPON Hogares. En la instrucción de UPDATE de solicitudes del cursor regcmp_equ, se cambia el valor REDCO 
--                          por la variable w_tecnología para que corresponda con la tecnología del identificador de acceso.  
-- 2014-06-27  Jrincong     Req 36900 GPON Hogares. Retiro de Componentes
--                          Uso de función fn_tecnologia para buscar la tecnología que se debe asignar a la solicitud.  Antes era REDCO y podría
--                          generar ordenes erróneas para los casos de accesos con tecnología GPON. 
-- 2014-10-15   MGATTIS     Automatización Cola DSLAM ITPAM25626_AutoColaDSLA. Actualización Lógica para inserción de etiquetas para generar eventos.
-- 2014-10-22  RVELILLR     Automatización Cola DSLAM ITPAM25626_AutoColaDSLA. Condicional de tecnologia para consulta de etiquetas 4805 para generar eventos.

w_respuesta_comercial     VARCHAR2 (3);
w_obs_pumed               VARCHAR2 (40);
w_estado_id               FNX_SOLICITUDES.estado_id%TYPE;
w_concepto                FNX_CONCEPTOS.concepto_id%TYPE;
v_estado_solic            FNX_SOLICITUDES.estado_soli%TYPE;

w_incons                  BOOLEAN;
p_retiro_acceso           BOOLEAN;
p_retiro_cmp              BOOLEAN;
p_nuevo                   BOOLEAN;
p_nuevo_serv              BOOLEAN;
p_traslado                BOOLEAN;
p_soli_ext                BOOLEAN;
p_cambio_tecno            BOOLEAN;
w_mensaje_error           VARCHAR2 (200);
w_paquete                 FNX_CONFIGURACIONES_IDENTIF.VALOR%TYPE;
w_pagina                  FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_cambio_plan             VARCHAR2(15);
b_cambio_plan_telev       BOOLEAN;
v_valor_3830              VARCHAR2(15);
b_genera_solic_equipo     BOOLEAN;
--2010-09-28 Se agrega el campo identificador_id_nuevo.

--2013-09-02    aarbob  REQ28278_Tangibilizacion
v_propiedadEqu  FNX_CONFIGURACIONES_EQUIPOS.VALOR%TYPE;

--2012-07-26
cursor c_solicitud_telev(p_pedido fnx_solicitudes.pedido_id%type,
                      p_subpedido fnx_solicitudes.subpedido_id%type) is 
    select pedido_id    ,subpedido_id ,solicitud_id
            ,servicio_id    ,producto_id  ,tipo_elemento    ,tipo_elemento_id
            ,municipio_id    ,empresa_id
    from   fnx_solicitudes
    where  pedido_id = p_pedido
    and    subpedido_id = p_subpedido
    and    tipo_elemento_id = 'TELEV';
v_solicitud_telev          c_solicitud_telev%ROWTYPE; 
v_direccion_nodo          VARCHAR2(10);


CURSOR c_solicitudes IS
SELECT  pedido_id, subpedido_id, solicitud_id
       ,servicio_id, producto_id, tipo_elemento_id
       ,tipo_elemento, identificador_id,identificador_id_nuevo, empresa_id, municipio_id
       ,observacion
FROM   FNX_SOLICITUDES
WHERE  pedido_id = w_pedido
AND    servicio_id      = 'ENTRE'
AND    producto_id      = 'TELEV'
AND    tipo_elemento    IN ('SRV',  'CMP')
AND    tipo_elemento_id IN ('TELEV', 'INSHFC', 'INSIP','SERHFC','SERVIP','SERV','ELSIS')
AND    estado_id        = 'TECNI'
AND    concepto_id      = 'PETEC'
ORDER BY   tipo_elemento DESC
FOR UPDATE;

v_solic                   c_solicitudes%ROWTYPE;

CURSOR c_subped ( p_subpedido  IN   FNX_SUBPEDIDOS.subpedido_id%TYPE)
IS
SELECT identificador_id
FROM   FNX_SUBPEDIDOS
WHERE  pedido_id    = w_pedido
AND    subpedido_id = p_subpedido;

v_subped                  c_subped%ROWTYPE;

CURSOR c_trabaj (p_pedido     IN   FNX_SOLICITUDES.pedido_id%TYPE,
                p_subpedido  IN   FNX_SOLICITUDES.subpedido_id%TYPE,
                p_solicitud  IN   FNX_SOLICITUDES.solicitud_id%TYPE
                ) IS
SELECT caracteristica_id, tipo_trabajo
FROM   fnx_trabajos_solicitudes
WHERE  pedido_id = p_pedido
AND subpedido_id = p_subpedido
AND solicitud_id = p_solicitud;

v_trabaj                  c_trabaj%ROWTYPE;

w_uno                     VARCHAR2 (1);
w_dos                     VARCHAR2 (1);
w_tres                    VARCHAR2 (1);
w_identificador           fnx_identificadores.identificador_id%TYPE;
w_encontro_numero         BOOLEAN := FALSE;
w_automatica              BOOLEAN;
b_estudio_automatico      BOOLEAN;
b_sin_terminal_deriv      BOOLEAN;

b_generar_ordenes         BOOLEAN;
v_concepto_paquete        FNX_SOLICITUDES.concepto_id%TYPE;

w_tecnologia              fnx_caracteristica_solicitudes.valor%TYPE;
w_carac                   fnx_caracteristica_solicitudes.caracteristica_id%TYPE;
w_valor                   fnx_caracteristica_solicitudes.valor%TYPE;
w_agrupador               FNX_IDENTIFICADORES.agrupador_id%TYPE:=NULL;
w_estado                  FNX_IDENTIFICADORES.estado%TYPE:=NULL;
w_reserva_identificador      NUMBER (3);
p_mensaje                 VARCHAR2(500);
b_prov_stb                BOOLEAN := FALSE;

CURSOR    c_pagina (p_caract_pagina IN FNX_CARACTERISTICA_SOLICITUDES.caracteristica_id%TYPE )
IS
  SELECT valor
  FROM   fnx_caracteristica_solicitudes
  WHERE  pedido_id          = v_solic.pedido_id
  AND    subpedido_id       = v_solic.subpedido_id
  AND    solicitud_id       = v_solic.solicitud_id
  AND    caracteristica_id  = p_caract_pagina;


CURSOR c_tvactiva (p_identificador  IN FNX_IDENTIFICADORES.identificador_id%TYPE ) IS
  SELECT centro_distribucion_intermedia, nodo_optico_electrico_id,
         tipo_amplificador_id, amplificador_id,
         tipo_derivador_id, derivador_id, terminal_derivador_id
  FROM   fnx_inf_tv_activas
  WHERE  identificador_id = p_identificador;

v_tvactiva                c_tvactiva%ROWTYPE;

CURSOR c_carac (p_pedido    IN   FNX_SOLICITUDES.pedido_id%TYPE,
               p_subpedido IN   FNX_SOLICITUDES.subpedido_id%TYPE,
               p_solicitud IN   FNX_SOLICITUDES.solicitud_id%TYPE,
               p_caracteristica IN FNX_CARACTERISTICA_SOLICITUDES.caracteristica_id%TYPE ) IS
  SELECT  valor
  FROM    fnx_caracteristica_solicitudes
  WHERE   pedido_id = p_pedido
  AND subpedido_id  = p_subpedido
  AND solicitud_id  = p_solicitud
  AND caracteristica_id = p_caracteristica
  FOR UPDATE;

-- Reconexion
CURSOR c_instal (p_pagina   IN   FNX_INSTALACIONES_TELEVISION.instalacion%TYPE ) IS
  SELECT servicio_activo
  FROM   fnx_instalaciones_television
  WHERE  instalacion = p_pagina;

v_instal                 c_instal%ROWTYPE;
msg_observac             VARCHAR2 (300);

w_parametro              NUMBER (3);

-- Cursor para determinar si existe una solicitud de PQUETE en el pedido y que se encuentre PXSLN.
CURSOR c_pquete_pendiente
IS
SELECT COUNT(*)
FROM   FNX_SOLICITUDES
WHERE  pedido_id   = w_pedido
AND    servicio_id = 'PQUETE'
AND    producto_id = 'PQUETE'
AND    tipo_elemento_id = 'PQUETE'
AND    tipo_elemento = 'SRV'
AND    estado_id   = 'ORDEN'
AND    concepto_id = 'PXSLN';

v_paquete_pendiente              NUMBER(1);

-- Cursor para determinar si en el pedido existen solicitudes que se encuentren
-- pendientes en algun concepto Tecnico o Ingre.
CURSOR c_solic_pendientes  ( p_subpedido         IN  FNX_SUBPEDIDOS.subpedido_id%TYPE )
IS
SELECT COUNT(*)
FROM   FNX_SOLICITUDES
WHERE  pedido_id   = w_pedido
AND    ( ( estado_id  = 'TECNI' AND concepto_id <> 'PRACC' )
         OR
         ( estado_id  = 'ORDEN' AND concepto_id IN ('PECBA', 'PEGDC','PEDNA', 'OKRED' ))
         OR
         ( estado_id  = 'INGRE' )
        )
AND    subpedido_id  <> p_subpedido;

v_solic_pendientes           NUMBER(2);

-- Cursor para buscar el identificador de paquete en el pedido.
CURSOR c_paquete_identificador  ( p_pedido        IN  FNX_PEDIDOS.pedido_id%TYPE )
IS
SELECT identificador_id_nuevo
FROM   FNX_SOLICITUDES
WHERE  pedido_id        = p_pedido
AND    servicio_id      = 'PQUETE'
AND    producto_id      = 'PQUETE'
AND    tipo_elemento_id = 'PQUETE'
AND    tipo_elemento    = 'SRV'
AND    estado_id        IN ('ORDEN', 'CUMPL');


CURSOR c_ident_esp (p_identificador   IN  FNX_IDENTIFICADORES.identificador_id%TYPE )
IS
SELECT   identificador_id, serie_id, servicio_id, producto_id
FROM     FNX_IDENTIFICADORES
WHERE    identificador_id = p_identificador
FOR UPDATE;

v_ident                   c_ident_esp%ROWTYPE;

CURSOR c_solic_insip
  ( p_subpedido          IN  FNX_SUBPEDIDOS.subpedido_id%TYPE )
IS
SELECT solicitud_id
FROM   FNX_SOLICITUDES
WHERE  pedido_id         = w_pedido
AND    subpedido_id      = p_subpedido
AND    tipo_elemento_id  = 'INSIP'
AND    tipo_elemento     = 'CMP'
AND    estado_soli       <> 'ANULA';

CURSOR c_solic_cmp_equ ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                         p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                        )
IS
SELECT a.subpedido_id,a.solicitud_id
FROM   FNX_SOLICITUDES A,
       FNX_REPORTES_INSTALACION B
WHERE  a.pedido_id    = p_pedido
AND    a.subpedido_id = p_subpedido
AND    a.pedido_id    = b.pedido_id
AND    a.subpedido_id = b.subpedido_id
AND    a.solicitud_id = b.solicitud_id
AND    a.producto_id  = 'TELEV'
AND    a.tipo_elemento_id NOT IN ('INSHFC','INSIP')
AND    b.tipo_solicitud = 'RETIR'
AND    a.tipo_elemento IN ('CMP', 'EQU')
AND    a.estado_id IN ('TECNI', 'RUTAS');

v_solic_cmp_equ             c_solic_cmp_equ%ROWTYPE;

CURSOR c_solic_srv
IS
SELECT b.tipo_solicitud
FROM   FNX_SOLICITUDES A,
       FNX_REPORTES_INSTALACION B
WHERE  a.pedido_id   =  w_pedido
AND    a.producto_id =  'TELEV'
AND    a.tipo_elemento_id = 'TELEV'
AND    a.pedido_id        = b.pedido_id
AND    a.subpedido_id     = b.subpedido_id
AND    a.solicitud_id     = b.solicitud_id
AND    b.tipo_solicitud   IN ('CAMBI','NUEVO');


CURSOR c_solic_srv_nuevo
IS
SELECT b.tipo_solicitud,b.pedido_id,b.subpedido_id,b.solicitud_id
FROM   FNX_SOLICITUDES A,
       FNX_REPORTES_INSTALACION B
WHERE  a.pedido_id   =  w_pedido
AND    a.producto_id =  'TELEV'
AND    a.tipo_elemento_id = 'TELEV'
AND    a.pedido_id        = b.pedido_id
AND    a.subpedido_id     = b.subpedido_id
AND    a.solicitud_id     = b.solicitud_id
AND    b.tipo_solicitud = 'NUEVO';

v_solic_srv_nuevo c_solic_srv_nuevo%ROWTYPE;

CURSOR c_soli_extensiones
IS
SELECT a.pedido_id,a.subpedido_id,a.solicitud_id
FROM  FNX_SOLICITUDES A,
      FNX_REPORTES_INSTALACION B
WHERE a.pedido_id        = v_solic.pedido_id
AND   a.subpedido_id     = v_solic.subpedido_id
AND   a.pedido_id        = b.pedido_id
AND   a.subpedido_id     = b.subpedido_id
AND   a.solicitud_id     = b.solicitud_id
AND   a.servicio_id      = 'ENTRE'
AND   a.producto_id      = 'TELEV'
AND   a.tipo_elemento_id IN ('EQURED','STBOX')
AND   b.tipo_solicitud   = 'NUEVO'
AND   a.estado_soli <> 'ANULA';

CURSOR c_retir_cmp
IS
SELECT a.subpedido_id,a.solicitud_id,a.tipo_elemento,a.tipo_elemento_id,a.identificador_id
FROM   FNX_SOLICITUDES A,
       FNX_REPORTES_INSTALACION B
WHERE  a.pedido_id         =  v_solic.pedido_id
AND    a.pedido_id         = b.pedido_id
AND    a.subpedido_id      = b.subpedido_id
AND    a.solicitud_id      = b.solicitud_id
AND    a.tipo_elemento_id IN ('DECO','PPV','EQURED','STBOX')
AND    b.tipo_solicitud    = 'RETIR'
AND    a.estado_soli       <> 'ANULA';

v_retir_cmp                 c_retir_cmp%ROWTYPE;

CURSOR c_cmp_servip (p_subpedido IN FNX_SOLICITUDES.subpedido_id%TYPE)
IS
SELECT COUNT(*)
FROM   FNX_SOLICITUDES A,
       FNX_REPORTES_INSTALACION B
WHERE  a.pedido_id        =   w_pedido
AND    a.subpedido_id     =   1
AND    a.pedido_id        =   b.pedido_id
AND    a.subpedido_id     =   b.subpedido_id
AND    a.solicitud_id     =   b.solicitud_id
AND    b.tipo_solicitud   =   'CAMBI'
AND    a.tipo_elemento_id =   'SERVIP'
AND    a.estado_soli      <> 'ANULA';

CURSOR c_est_soli (p_subpedido IN FNX_SOLICITUDES.subpedido_id%TYPE,
                   p_solicitud IN FNX_SOLICITUDES.solicitud_id%TYPE)
IS
SELECT estado_id
FROM   fnx_solicitudes
WHERE  pedido_id    =  w_pedido
AND    subpedido_id =  p_subpedido
AND    solicitud_id =  p_solicitud
AND    estado_soli <> 'ANULA';

CURSOR c_soli_acceso (pr_subpedido IN FNX_SOLICITUDES.subpedido_id%TYPE)
IS
SELECT COUNT(*)
FROM   FNX_SOLICITUDES A,
       FNX_REPORTES_INSTALACION B
WHERE  a.pedido_id        =  w_pedido
AND    a.subpedido_id     =  pr_subpedido
AND    a.pedido_id        =  b.pedido_id
AND    a.subpedido_id     =  b.subpedido_id
AND    a.solicitud_id     =  b.solicitud_id
AND    b.tipo_solicitud   =  'RETIR'
AND    a.tipo_elemento_id IN ('INSIP','INSHFC')
AND    a.estado_soli      <> 'ANULA';

CURSOR c_equipos_subpedido
  ( p_subpedido  IN  FNX_SUBPEDIDOS.subpedido_id%TYPE )
IS
SELECT subpedido_id, solicitud_id, tipo_elemento_id, tecnologia_id
FROM   FNX_SOLICITUDES
WHERE  pedido_id     = w_pedido
AND    subpedido_id  = p_subpedido
AND    servicio_id   = 'ENTRE'
AND    producto_id   = 'TELEV'
AND    tipo_elemento = 'EQU';

v_soli_insta                NUMBER(2):=0;
v_soli_acceso               NUMBER(2):=0;
v_est_soli                  FNX_SOLICITUDES.estado_id%TYPE:=NULL;
v_count_servip              NUMBER(2):=0;
v_soli_ext                  FNX_SOLICITUDES.solicitud_id%TYPE:=NULL;
v_solic_srv                 FNX_REPORTES_INSTALACION.tipo_solicitud%TYPE;
v_solicitud_insip           FNX_SOLICITUDES.solicitud_id%TYPE;
v_plan_comercial            FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE:=NULL;
v_paquete_identificador     FNX_SOLICITUDES.identificador_id_nuevo%TYPE;
v_tipo_paquete              FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_valor_trab_3830           FNX_TRABAJOS_SOLICITUDES.valor%TYPE:=NULL;
v_valor_caract_3830         FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE:=NULL;
v_identificador_pumed       FNX_IDENTIFICADORES.identificador_id%TYPE:=NULL;

b_error_actualizando        BOOLEAN;
b_act_solicitud             BOOLEAN;
b_retiro_cambio_producto    BOOLEAN;
v_sin_automatica            NUMBER(4);
v_carac_pagina              FNX_CARACTERISTICA_SOLICITUDES.caracteristica_id%TYPE ;
v_instalado                 VARCHAR2(1):=NULL ;
b_ingre_pumed               BOOLEAN:=FALSE;

b_pedido_tvdigital          BOOLEAN:=FALSE;
b_tiene_tvdigital           BOOLEAN:=FALSE;

-- Cursor para buscar si existe una solicitud de Nuevo, en un subpedido diferente al que se está evaluando
CURSOR c_nueva_solic_tv
( p_subpedido_actual   IN   FNX_SUBPEDIDOS.subpedido_id%TYPE,
  p_tipo_elemento      IN   FNX_ELEMENTOS.tipo_elemento_id%TYPE
)
IS
SELECT COUNT(*)
FROM   FNX_SOLICITUDES A,
       FNX_REPORTES_INSTALACION B
WHERE  a.pedido_id        =  w_pedido
AND    a.subpedido_id     <> p_subpedido_actual
AND    a.pedido_id        =  b.pedido_id
AND    a.subpedido_id     =  b.subpedido_id
AND    a.solicitud_id     =  b.solicitud_id
AND    b.tipo_solicitud   =  'NUEVO'
AND    a.tipo_elemento_id =  p_tipo_elemento
AND    a.estado_soli      <> 'ANULA';

 CURSOR c_pedido
 IS
 SELECT constructor
 FROM    FNX_PEDIDOS
 WHERE    pedido_id     = w_pedido;

-- 2012-08-08 Se modifica el cursor para que de ahora en adelante, acepte también el tipo elemento
--            SERHFC.
CURSOR C_DEMOSTRACION (p_pedido_id         fnx_solicitudes.pedido_id%type,
                       --p_tipo_elemento_id  fnx_solicitudes.tipo_elemento_id%type,
                       p_caracteristica_id fnx_caracteristica_solicitudes.caracteristica_id%type)
IS
SELECT A.PEDIDO_ID,
       A.SUBPEDIDO_ID,
       A.SOLICITUD_ID,
       A.VALOR,
       A.CARACTERISTICA_ID
  FROM FNX_CARACTERISTICA_SOLICITUDES A
 WHERE A.pedido_id         = p_pedido_id
   AND A.CARACTERISTICA_ID = p_caracteristica_id
   AND A.tipo_elemento_id IN ('SERVIP','SERHFC')
   AND ROWNUM   =   1;
   --AND A.TIPO_ELEMENTO_ID  = p_tipo_elemento_id;

   v_demostracion                 C_DEMOSTRACION%ROWTYPE;
   v_demostracion_cambio_paquete  C_DEMOSTRACION%ROWTYPE;



 -- Cursor para obtener configuracion de Provision establecida para el proveedor, muunicipio, producto
 -- y tecnologia
 CURSOR c_rutasprov ( p_proveedor           IN FNX_PROVEEDORES_PRODUCTOS.proveedor_id%TYPE
                     ,p_municipio_servicio  IN FNX_PROVEEDORES_PRODUCTOS.municipio_id%TYPE
                     ,p_producto            IN FNX_PROVEEDORES_PRODUCTOS.producto_id%TYPE
                     ,p_tecnologia          IN FNX_RUTAS_PROVISION.tecnologia_id%TYPE
                    ) IS
 SELECT control_puentes, control_ordenes,
        aa_switche_asignacion, aa_zonainf_hfc, aa_buscar_reuso_red
 FROM   FNX_RUTAS_PROVISION
 WHERE  proveedor_id   = p_proveedor
 AND    municipio_id   = p_municipio_servicio
 AND    producto_id    = p_producto
 AND    tecnologia_id  = p_tecnologia ;

v_rutasprov     c_rutasprov%ROWTYPE;

CURSOR c_soli_insta
IS
SELECT COUNT(*)
FROM  FNX_SOLICITUDES
WHERE PEDIDO_ID = w_pedido
AND   SERVICIO_ID = 'ENTRE'
AND   PRODUCTO_ID = 'TELEV'
AND   TIPO_ELEMENTO = 'CMP'
AND   TIPO_ELEMENTO_ID IN ('INSIP','INSHFC')
AND   ESTADO_ID = 'TECNI';

-- 2010-07-29 Cursor para verificar que viene un EQURED en trabajos de la solicitud
   CURSOR c_trabajos (
      p_pedido_id      fnx_solicitudes.pedido_id%TYPE,
      p_subpedido_id   fnx_solicitudes.subpedido_id%TYPE
   )
   IS
      SELECT tipo_trabajo, pedido_id, subpedido_id, solicitud_id
        FROM fnx_trabajos_solicitudes
       WHERE pedido_id = p_pedido_id
         AND subpedido_id = p_subpedido_id
         AND tipo_elemento_id IN ('EQURED', 'STBOX')
         AND caracteristica_id = 200;

-- 2010-07-29 Cursor para verificar que si viene en el INSIP un cambio en la 2909
   CURSOR c_insip (
      p_pedido_id      fnx_solicitudes.pedido_id%TYPE,
      p_subpedido_id   fnx_solicitudes.subpedido_id%TYPE
   )
   IS
      SELECT tipo_trabajo
        FROM fnx_trabajos_solicitudes
       WHERE pedido_id = p_pedido_id
         AND subpedido_id = p_subpedido_id
         AND tipo_elemento_id = 'INSIP'
         AND caracteristica_id = 2909;

--2010-09-02 Cursor para equipos en el nuevo tema de equipos set top box.
--2011-09-14 Se agrega la condición para el tipo de tecnología IP
--2012-09-13 Cuando una solicitud no tiene la caract.33=REDCO, se valida la tecnologia sobre el identificador 
   CURSOR c_equ ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                  p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                )
   IS
    SELECT a.subpedido_id,a.solicitud_id,b.tipo_solicitud,
           FN_VALOR_CARACTERISTICA_SOL(A.pedido_id,A.subpedido_id,A.solicitud_id,1067) tipo_equipo_carct
    FROM fnx_solicitudes a, fnx_reportes_instalacion b
    WHERE a.pedido_id       = b.pedido_id
    AND a.subpedido_id      = b.subpedido_id
    AND a.solicitud_id      = b.solicitud_id
    AND a.tipo_elemento     = 'EQU'
    AND A.estado_soli       <> 'ANULA'
    AND a.pedido_id         = p_pedido
    AND a.subpedido_id      = p_subpedido
    --AND FN_VALOR_CARACTERISTICA_SOL(A.pedido_id,A.subpedido_id,A.solicitud_id,33)='IP';
    -- 2011-11-21 Se coloca el valor de REDCO en vez de IP.
    AND ( FN_VALOR_CARACTERISTICA_SOL(A.pedido_id,A.subpedido_id,A.solicitud_id,33)='REDCO'
    OR    FN_TECNOLOGIA_IDENTIF(A.identificador_id)='REDCO'  --2012-09-13 
        ) ;

-- 2011-08-02 Cursor que se utilizará para determinar si en un pedido viene un tipo elemento
-- igual a SRV y si viene un tipo elemento id ELSIS.
  CURSOR C_EQUIPOS_ELSIS_SRV( p_pedido_id FNX_SOLICITUDES.pedido_id%TYPE, p_subpedido_id FNX_SOLICITUDES.subpedido_id%TYPE)
      IS
  SELECT A.pedido_id,
         A.subpedido_id,
         A.solicitud_id,
         A.servicio_id,
         A.tipo_elemento_id,
         A.tipo_elemento
    FROM FNX_SOLICITUDES A
   WHERE A.pedido_id    = p_pedido_id
     AND A.subpedido_id = p_subpedido_id;

  CURSOR C_EQUIPOS_ELSIS( p_pedido_id FNX_SOLICITUDES.pedido_id%TYPE, p_subpedido_id FNX_SOLICITUDES.subpedido_id%TYPE)
      IS
  SELECT A.pedido_id,
         A.subpedido_id,
         A.solicitud_id,
         A.servicio_id,
         A.tipo_elemento_id,
         A.tipo_elemento
    FROM FNX_SOLICITUDES A
   WHERE A.pedido_id    = p_pedido_id
     AND A.subpedido_id = p_subpedido_id;

v_constructor               FNX_PEDIDOS.constructor%TYPE;



v_total_nuevos              NUMBER(2) := 0;

b_red_provisionada          BOOLEAN  := FALSE;
v_tramo_red_asignada        VARCHAR2 (20);
b_inconsistencia            BOOLEAN  := FALSE;
b_stbox_hdd                 BOOLEAN := FALSE;
v_mensaje_asignacion        VARCHAR2(300);

b_existe_elsis              BOOLEAN:=FALSE;
b_existe_servicio           BOOLEAN:=FALSE;

v_pedido_infra              FNX_PEDIDOS.pedido_id%TYPE;
v_subpedido_infra           FNX_SUBPEDIDOS.subpedido_id%TYPE;
v_solicitud_infra           FNX_SOLICITUDES.solicitud_id%TYPE;
v_tipo_acceso               VARCHAR2(25);
w_ident_acceso_HFC          FNX_IDENTIFICADORES.identificador_id%TYPE;

v_municipio_servicio       FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_munic_salida_int         FNX_MUNICIPIOS.mun_salida_internet%TYPE;
vo_enrutar_et              FNX_RUTAS_PROVISION.control_est_tecnico%TYPE := NULL;
v_tipo_equipo              FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE := NULL;
v_tipo_equipo_trabajos     FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE := NULL;
v_solicitud_eqaccp         FNX_SOLICITUDES.SOLICITUD_ID%TYPE;
v_plan_comercial_agr       FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE:=NULL;

v_plan_fact_global_indiv   FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE:=NULL;
b_global_indiv             BOOLEAN:= FALSE;

-- 2010-02-04   Jsierrao   Se declaran las variables para que cuando las observaciones superen los 1000 caractereres, los pedidos
--                         no se queden en petec y se invoca al actualizar las solicitudes de la tv y se incluye en fnx_parametros
--                         modulo ESTUD_TEC (nuevo).

v_tamano_observacion       NUMBER(4);
v_nueva_observacion        VARCHAR2(1000);
v_tamano_nueva_obs         NUMBER(4);
v_tiene_stboxhd            VARCHAR2(10);


v_tipo_trabajo_equred     fnx_trabajos_solicitudes.tipo_trabajo%type;
v_tipo_trabajo_insip      fnx_trabajos_solicitudes.tipo_trabajo%type;

b_tipo_trabajo_insip      BOOLEAN:=FALSE;
b_tipo_trabajo_equred      BOOLEAN:=FALSE;
b_demostracion             BOOLEAN:= FALSE;
b_demostracion_cambio_paquete  BOOLEAN:= FALSE;

v_pedido_id               fnx_solicitudes.pedido_id%type;
v_subpedido_id            fnx_solicitudes.subpedido_id%type;
v_solicitud_id            fnx_solicitudes.solicitud_id%type;
v_estrato                 FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE:=NULL;
v_depto_nal               FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE:=NULL;
v_tipo_elemento_id_iden   FNX_IDENTIFICADORES.tipo_elemento_id%TYPE:=NULL;

-- dojedac. 24/03/2011, PRY DESAPROVISIONAMIENTO - Cursor para obtener la cantidad de equipos (DECO o STBOX) asociados al servicio
CURSOR c_cant_equipos (pv_identif_telev fnx_identificadores.identificador_id%type)
IS
  SELECT COUNT(*) CUENTA
  FROM FNX_EQUIPOS
  WHERE IDENTIFICADOR_ID = NVL(FN_VALOR_CARACT_IDENTIF(pv_identif_telev, 90), pv_identif_telev)
    AND ESTADO = 'OCU';

CURSOR c_soli_srv (p_pedido IN FNX_SOLICITUDES.pedido_id%TYPE,
                   p_subpedido IN FNX_SOLICITUDES.subpedido_id%TYPE
                   )
IS
SELECT A.SOLICITUD_ID
FROM FNX_SOLICITUDES A, FNX_REPORTES_INSTALACION B
WHERE a.pedido_id = b.pedido_id
AND   A.subpedido_id =  B.subpedido_id
AND   A.solicitud_id = B.solicitud_id
AND   A.tipo_elemento_id = 'TELEV'
AND   A.pedido_id = p_pedido
AND   a.subpedido_id = p_subpedido;

v_soli_srv    NUMBER(5);

--Dojedac: Variables de referencia para el proyecto de desaprovisionamiento, RECUPERACIÓN DE EQUIPOS
v_cad_equip_acceso          VARCHAR2(200);
v_cant_equipos NUMBER;
v_tipo_error VARCHAR2(500);
v_tipo_elemento_solic fnx_solicitudes.tipo_elemento_id%type;
v_identificador_ip fnx_identificadores.identificador_id%type;

v_equipo_obsoleto           VARCHAR2(1);
v_si_obsoleto               NUMBER(2);
v_no_obsoleto               NUMBER(2);
v_equip_obsol               FNX_SOLICITUDES.OBSERVACION%TYPE;
v_identifica_acceso         FNX_IDENTIFICADORES.identificador_id%TYPE;
identificador_comparte      FNX_IDENTIFICADORES.identificador_id%TYPE;

--yhernana 2012-07-03 
b_retir_hfc                 BOOLEAN:= FALSE;
v_identificador_servicio    FNX_CONFIGURACIONES_IDENTIF.valor%TYPE;
v_estado_servicio           FNX_CONFIGURACIONES_IDENTIF.valor%TYPE;
v_mensaje                   VARCHAR2(4000);
v_motivo_retiro             FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
v_estado_identif            FNX_IDENTIFICADORES.estado%TYPE;
v_mensaje_bts_cyr           VARCHAR2(250);
v_si_equipo                    BOOLEAN;    
--2013-01-28: MMedinac: Req20216: Línea adicionada, variable para determinar si hay subpedidos del paquete trio superplay pendientes 
p_SubPendi                   BOOLEAN := TRUE;
--2013-01-28: MMedinac: Req20216: Línea adicionada, variable que guardará si el paquete que se esta trabajando es Trio Superplay 
bEsOfertaEspecifica          BOOLEAN := FALSE;
--2014-01-15 lhernans Automatización cola DSLAM. Variable para almacenar etiqueta
v_etiqueta                   fnx_caracteristica_solicitudes.valor%TYPE;
--2014-10-15   MGATTIS       
v_aplicacion                FNX_PARAM_EVENTOS_BTS.aplicacion%TYPE;   
-- 2014-10-15   MGATTIS Fin declaración de variables

v_identific_asoci           fnx_identificadores.identificador_id%type;

CURSOR c_soli_inshfc (p_pedido IN FNX_SOLICITUDES.pedido_id%TYPE,
                      p_subpedido IN FNX_SOLICITUDES.subpedido_id%TYPE
                      )
IS
SELECT A.SOLICITUD_ID
FROM FNX_SOLICITUDES A, FNX_REPORTES_INSTALACION B
WHERE a.pedido_id = b.pedido_id
AND   A.subpedido_id =  B.subpedido_id
AND   A.solicitud_id = B.solicitud_id
AND   A.tipo_elemento_id = 'INSHFC'
AND   A.pedido_id = p_pedido
AND   a.subpedido_id = p_subpedido;

v_soli_inshfc    fnx_solicitudes.solicitud_id%TYPE;

TYPE regMemory_t IS RECORD (
     EQUIPO_ID            FNX_EQUIPOS.EQUIPO_ID%TYPE,
     MARCA_ID             FNX_EQUIPOS.MARCA_ID%TYPE,
     REFERENCIA_ID        FNX_EQUIPOS.REFERENCIA_ID%TYPE,
     TIPO_ELEMENTO        FNX_EQUIPOS.TIPO_ELEMENTO%TYPE,
     TIPO_ELEMENTO_ID     FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
     TIPO_ELEMENTO_ID_EQU FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
     GENERAR              NUMBER(1)
                            );

TYPE CUR_TYP IS REF CURSOR;
c_Equipos_solic CUR_TYP;
v_cade_Query VARCHAR2(1000);
reg_tipo_equipo regMemory_t;

-- 2013-10-03   Yhernana    Inicio
--                          REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
v_accesp_comparte       fnx_solicitudes.identificador_id%TYPE;
v_toip_comparte         fnx_solicitudes.identificador_id%TYPE;
v_marca_id              fnx_equipos.marca_id%TYPE;
v_refer_id              fnx_equipos.referencia_id%TYPE;
v_tipele_id             fnx_equipos.tipo_elemento_id%TYPE;
v_tipo_ele              fnx_equipos.tipo_elemento%TYPE;
b_comparte_dos          BOOLEAN:= FALSE;
b_comparte_accesp       BOOLEAN:= FALSE;
b_comparte_toip         BOOLEAN:= FALSE;
b_aplica_retir          BOOLEAN:= FALSE;
v_cant_retir_proc       NUMBER(2) := 0;
v_equipo_retiro         FNX_EQUIPOS.EQUIPO_ID%TYPE;
b_no_comparte_infra     BOOLEAN:= FALSE;

CURSOR c_datos_identif2 (pc_identificador_id fnx_identificadores.identificador_id%TYPE) IS
    SELECT cliente_id, fn_valor_caract_identif(identificador_id, 38) pagina_servicio
    FROM fnx_identificadores
    WHERE identificador_id = pc_identificador_id;
rg_datos_identif2 c_datos_identif2%ROWTYPE;

CURSOR c_equipo_comparte2 (
                    pc_identificador_id fnx_identificadores.identificador_id%TYPE,
                    pc_cliente_id       fnx_identificadores.cliente_id%TYPE,
                    pc_pagina_servicio  fnx_configuraciones_identif.valor%TYPE
                ) IS
SELECT a.identificador_id, b.producto_id
FROM fnx_configuraciones_identif a, fnx_identificadores b
WHERE a.identificador_id  = b.identificador_id
  AND a.identificador_id <> pc_identificador_id
  AND a.caracteristica_id = 38
  AND a.valor             = pc_pagina_servicio
  AND b.producto_id      IN ('TELEV', 'TO', 'INTER')
  AND b.tipo_elemento_id IN ('INSIP', 'TOIP', 'ACCESP')
  AND b.cliente_id        = pc_cliente_id
  AND fn_valor_caract_identif(a.identificador_id, DECODE((SELECT x.producto_id
                                                          FROM fnx_identificadores x
                                                          WHERE x.identificador_id = pc_identificador_id),
                              'INTER', 2093,
                              'TELEV', 2087,
                              'TO', 4093, 2093)) = pc_identificador_id;

-- 2013-10-03   Yhernana    FIN
--2014-02-24    Jrendbr Creacion de variables equipos NIR
v_equipo_nir           VARCHAR2(1);
v_si_nir               NUMBER(2);
v_tipele_id_sol        fnx_solicitudes.tipo_elemento_id%TYPE;

---------------------------------------------------------------------------------------------------------

    -- JGallg - 2012-04-10 - MejorasDesaprov: Función interna para validar si se genera o no la solicitud de recuperación de equipo.
    FUNCTION fni_generar_solic_equipo (
        p_identificador_id   fnx_solicitudes.identificador_id%TYPE,
        p_tecnologia         fnx_solicitudes.tecnologia_id%TYPE,
        prg_equipo           regMemory_t,
        p_pedido_id             fnx_pedidos.pedido_id%TYPE
    ) RETURN BOOLEAN IS
        b_genera           BOOLEAN;
        v_identif_comparte fnx_solicitudes.identificador_id%TYPE;
        v_equipo_comodato  fnx_configuraciones_equipos.valor%TYPE;
        v_cantidad         NUMBER;

        CURSOR c_caract_equipo (
            prg_datos_equipo     regMemory_t,
            pc_caracteristica_id fnx_tipos_caracteristicas.caracteristica_id%type
        ) IS
            SELECT valor
            FROM fnx_configuraciones_equipos
            WHERE marca_id          = prg_datos_equipo.marca_id
              AND referencia_id     = prg_datos_equipo.referencia_id
              AND tipo_elemento_id  = prg_datos_equipo.tipo_elemento_id
              AND tipo_elemento     = prg_datos_equipo.tipo_elemento
              AND equipo_id         = prg_datos_equipo.equipo_id
              AND caracteristica_id = pc_caracteristica_id;
    BEGIN
        -- Validar infraestructura compartida
        IF prg_equipo.tipo_elemento_id IN ('CABLEM', 'CPE') THEN
            IF p_tecnologia = 'INALA' THEN
                b_genera := TRUE;
            ELSIF p_tecnologia = 'FIBRA' THEN
                b_genera := TRUE;
            ELSIF p_tecnologia = 'HFC' THEN
                b_genera := TRUE;

            ELSIF p_tecnologia IN ('REDCO', 'GPON') THEN

               -- 2014-06-25. Req 36900 GPON Hogares.  Función fni_generar_solic_equipo. 
               -- Consideración de teconología GPON para verificación  de Infraestructura compartida activa.
                        
                v_identif_comparte := fn_comparte_infra_activa (p_identificador_id, p_tecnologia, 'INTER', 'ACCESP', NULL);
                IF v_identif_comparte IS NULL THEN
                    v_identif_comparte := fn_comparte_infra_activa (p_identificador_id, p_tecnologia, 'TO', 'TOIP', NULL);
                END IF;

                IF v_identif_comparte IS NOT NULL THEN
                    b_genera := FALSE;
                    
                    -- 2012-08-21    Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo tiene infraestructura compartida
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-Infra Comp;' || observacion,1,200)
                        WHERE pedido_id     = p_pedido_id;
                    EXCEPTION 
                        WHEN OTHERS THEN
                            NULL;
                    END;
                    
                ELSE
                  b_genera := TRUE;
                END IF;
            END IF;

            -- SECCION.  Validación de Equipo Compartido 
            IF b_genera THEN

                -- 2014-06-25. Req 36900 GPON Hogares.  Función fni_generar_solic_equipo.
                -- Consideración de tecnología GPON para verificar Equipo Compartido.

                IF p_tecnologia IN ('REDCO', 'HFC', 'GPON') THEN
                    v_cantidad := fn_comparte_equipo (prg_equipo.marca_id, prg_equipo.referencia_id, prg_equipo.tipo_elemento_id, prg_equipo.equipo_id);
                    
                    IF v_cantidad = 0 THEN
                        b_genera := TRUE;
                    ELSE
                        b_genera := FALSE;
                        
                        -- 2012-08-21    Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es compartido
                        BEGIN
                            UPDATE FNX_PEDIDOS
                            SET observacion =  SUBSTR ('Desap-Equi Comp:' || prg_equipo.equipo_id ||';'|| observacion,1,200)
                            WHERE pedido_id     = p_pedido_id;
                        EXCEPTION 
                            WHEN OTHERS THEN
                                NULL;
                        END;
                    END IF;
                ELSE
                    b_genera := TRUE;
                END IF;
            END IF;
        ELSE
            b_genera := TRUE;
        END IF;

        IF b_genera THEN
            -- Validar propiedad del equipo
            OPEN c_caract_equipo (prg_equipo, 1093);
            FETCH c_caract_equipo INTO v_equipo_comodato;
            CLOSE c_caract_equipo;
            v_equipo_comodato := NVL(v_equipo_comodato, 'CD');
            -- 2014-04-07 Si el equipo es de fidelización, se debe generar la solicitud de equipo.
            IF v_equipo_comodato IN ('CD', 'AL','FI') THEN
                b_genera := TRUE;
            ELSE
                b_genera := FALSE;
                
                -- 2012-08-21    Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el Equipo es propiedad del cliente
                BEGIN
                    UPDATE FNX_PEDIDOS
                    SET observacion =  SUBSTR ('Desap-Equi Client;' || observacion,1,200)
                    WHERE pedido_id     = p_pedido_id;
                EXCEPTION 
                    WHEN OTHERS THEN
                        NULL;
                END;
            END IF;
        END IF;

        RETURN(b_genera);
    END;

    -- JGallg - 2012-04-10 - MejorasDesaprov: Procedimiento interno para paso de solicitud a ORDEN - PORDE.
    PROCEDURE pri_paso_orden_porde (
        p_pedido_id        fnx_solicitudes.pedido_id%type,
        p_subpedido_id     fnx_solicitudes.subpedido_id%type,
        p_solicitud_id     fnx_solicitudes.solicitud_id%type,
        p_observ_obsol     fnx_solicitudes.observacion%type,
        p_mensaje      out varchar2
    ) IS
    BEGIN
        UPDATE FNX_SOLICITUDES
        SET estado_id   = 'ORDEN',
            concepto_id = 'PORDE',
            estado_soli = 'DEFIN',
            observacion = SUBSTR ( p_observ_obsol || observacion,1,2000)
        WHERE pedido_id     = p_pedido_id
          AND subpedido_id  = p_subpedido_id
          AND solicitud_id  = p_solicitud_id;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := 'PR_ESTUDIO_TECNICO_TELEV:  No puso solicitud en ORDEN-PORDE.  mensaje: '||sqlerrm||'  ('||to_char(sqlcode)||').';
    END;

BEGIN
-- 2013-06-12   jvillabu    Se adiciona llamado al PR_CREAR_SOLIC_EQUI_CANAL para inclusion de equipo regalo y canal siembra
--                          en nuevos planes de television HFC, estratos 5 y 6
OPEN  c_solic_srv_nuevo;
FETCH c_solic_srv_nuevo INTO v_solic_srv_nuevo;
CLOSE c_solic_srv_nuevo;

  IF NVL(v_solic_srv_nuevo.tipo_solicitud,'NOP') = 'NUEVO' THEN    
     v_estrato:=fn_valor_caracteristica_sol(v_solic_srv_nuevo.pedido_id,v_solic_srv_nuevo.subpedido_id,v_solic_srv_nuevo.solicitud_id,37); 
     v_plan_comercial := fn_valor_caracteristica_sol(v_solic_srv_nuevo.pedido_id,v_solic_srv_nuevo.subpedido_id,v_solic_srv_nuevo.solicitud_id,3830);
     v_plan_fact_global_indiv :=  Fn_valor_caract_subpedido(v_solic_srv_nuevo.pedido_id, v_solic_srv_nuevo.subpedido_id, 2005 );
     IF INSTR(fn_dominio_descripcion('PROV_PLANES_PROMO_TELEV','1'),NVL(v_estrato,0))!=0 AND NVL(v_plan_comercial,'NOP') = fn_dominio_descripcion('PROV_PLANES_PROMO_TELEV','2') AND INSTR(fn_dominio_descripcion('PROV_PLANES_PROMO_TELEV','3'),NVL(v_plan_fact_global_indiv,'NOP'))!=0 THEN
       DBMS_OUTPUT.PUT_LINE('Creacion solicitud equipo regalo television HFC');
       pr_crear_solic_equi_canal(v_solic_srv_nuevo.pedido_id,v_solic_srv_nuevo.subpedido_id,p_mensaje);
       IF p_mensaje IS NOT NULL THEN
          DBMS_OUTPUT.PUT_LINE('Fallo creacion solicitud equipo regalo: '||p_mensaje);
               UPDATE fnx_solicitudes
                  SET observacion = observacion||'<#[ET-TV] Falló creación solicitud equipo regalo#> '
                WHERE pedido_id = v_solic_srv_nuevo.pedido_id
                  AND subpedido_id = v_solic_srv_nuevo.subpedido_id
                  AND solicitud_id = v_solic_srv_nuevo.solicitud_id;
       END IF;    
     END IF; 
  END IF;   
-- 2010-02-04   Jsierrao   Se declaran las variables para que cuando las observaciones superen los 1000 caractereres, los pedidos
--                         no se queden en petec y se invoca al actualizar las solicitudes de la tv y se incluye en fnx_parametros
--                         modulo ESTUD_TEC (nuevo).

w_parametro             := 1;
v_tamano_observacion    := NVL(FN_valor_parametros ( 'ESTUD_TEC', w_parametro ), 1000);

w_incons        := FALSE;
w_mensaje_error := 'N';
w_tecnologia    := NULL;
p_mensaje       := NULL;
v_valor_3830:= NULL;

-- CICLO PRINCIPAL DE SOLICITUDES
OPEN c_solicitudes;
FETCH c_solicitudes INTO v_solic;

LOOP

 EXIT WHEN c_solicitudes%NOTFOUND  OR w_incons;

 p_retiro_acceso    := FALSE;
 p_nuevo            := FALSE;
 p_nuevo_serv       := FALSE;
 p_traslado         := FALSE;
 p_soli_ext         := FALSE;
 p_cambio_tecno     := FALSE;
 b_generar_ordenes  := FALSE;
 b_act_solicitud    := FALSE;
 b_prov_stb         := FALSE;
 b_ingre_pumed      := FALSE;
 b_demostracion     := FALSE;
 b_cambio_plan_telev:= FALSE;

-- 2009-02-05  Aacosta   Validar si el equipo de tipo set up box es de alta definición. Para
--                       este caso solo se debe provisionar el componente de servicios, los
--                       demás deben quedar PETEC para ser estudiados desde la forma.

--  2010-09-02 <Sección Nuevo enrutamiento>
--  Se crea lógica a Fin de poder colocar genérica la manera en la que se deben enrutar.
--  los STBOX HD, teniendo cuenta las peticiones que se puedan presentar, por tal motivo se comenta
--  la forma en la que se enrutaban.

        /*v_tipo_equipo  := FN_VALOR_CARACT_SUBPEDIDO ( v_solic.pedido_id, v_solic.subpedido_id, 1067 );

        -- 2010-07-22 Se verifica si el equipo que viene, corresponde a un set up box de alta definición.
        v_tiene_stboxhd := NVL(FN_DOMINIO_DESCRIPCION('EQU_TELEV',v_tipo_equipo),'NO');
        IF v_tiene_stboxhd = 'SI' THEN
            b_stbox_hdd := TRUE;
        END IF;*/
        
            b_existe_elsis        :=FALSE;
            b_existe_servicio    :=FALSE;
        -- 2011-08-02 Se abre el cursor del tema de equipos con el fin de determinar
        -- si en la solicitud viene un equipo y un servicio.
            FOR reg_elsis IN C_EQUIPOS_ELSIS_SRV (v_solic.pedido_id,v_solic.subpedido_id) LOOP
                IF reg_elsis.tipo_Elemento ='SRV' THEN
                   b_existe_servicio:=TRUE;
                END IF;
            END LOOP;

        -- 2011-08-02 Se comienza a buscar ELSIS por pedido y subpedido
            FOR reg_elsis IN C_EQUIPOS_ELSIS (v_solic.pedido_id,v_solic.subpedido_id) LOOP
                IF reg_elsis.tipo_Elemento_id ='ELSIS' THEN
                   b_existe_elsis:=TRUE;
                END IF;
            END LOOP;

            FOR reg IN c_equ (v_solic.pedido_id, v_solic.subpedido_id) LOOP
            --2010-08-31 Se cambia la condición IF reg.tipo_solicitud IN ('CAMBI','NUEVO') THEN
            -- por la condición IF reg.tipo_solicitud ='NUEVO' THEN de acuerdo al requerimiento 1371892
             IF reg.tipo_solicitud ='NUEVO' THEN
                v_tipo_equipo := NVL(FN_DOMINIO_DESCRIPCION('EQU_TELEV',reg.tipo_equipo_carct),'NO');

                IF v_tipo_equipo = 'SI' THEN
                  b_stbox_hdd := TRUE;
                END IF;
              END IF;
              --2010-09-01 Se debe agregar una validación considerando el tipo de cambio de equipo que se va a realizar.

              IF reg.tipo_solicitud IN('CAMBI') THEN

                    v_tipo_equipo:= NVL(FN_DOMINIO_DESCRIPCION('EQU_TELEV',reg.tipo_equipo_carct),'NO');
                    v_tipo_equipo_trabajos:= NVL(FN_DOMINIO_DESCRIPCION('EQU_TELEV',PKG_SOLICITUDES.fn_valor_trabajo(v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,1067)),'NO');

                   IF v_tipo_equipo = 'SI' AND v_tipo_equipo_trabajos='NO' THEN
                       b_stbox_hdd := TRUE;
                   END IF;

               END IF;

            END LOOP;
    -- <Fin Sección Nuevo enrutamiento>


 -- Identificacion de los trabajos solicitados
 OPEN c_trabaj ( v_solic.pedido_id, v_solic.subpedido_id
                 ,v_solic.solicitud_id );

 LOOP
     FETCH c_trabaj INTO v_trabaj;
     EXIT WHEN c_trabaj%NOTFOUND;

     IF      v_trabaj.caracteristica_id = 1
        AND v_trabaj.tipo_trabajo = 'RETIR'
        AND v_solic.tipo_elemento_id NOT IN ('SERHFC','SERVIP') THEN
            p_retiro_acceso := TRUE;
     ELSIF v_trabaj.caracteristica_id = 1
        AND v_trabaj.tipo_trabajo = 'RETIR'
        AND v_solic.tipo_elemento_id IN ('SERHFC','SERVIP') AND
        NOT p_retiro_acceso THEN
            p_retiro_cmp := TRUE;
     ELSIF      v_trabaj.caracteristica_id = 1
         AND v_trabaj.tipo_trabajo = 'NUEVO'
         AND v_solic.tipo_elemento_id <> 'SERV' THEN
         p_nuevo := TRUE;
     ELSIF      v_trabaj.caracteristica_id IN (34, 35, 38)
         AND v_trabaj.tipo_trabajo = 'CAMBI' THEN
         p_traslado := TRUE;
     -- 2008-03-22  Aacosta   Consideración del cambio de tecnología para el nuevo producto IPTV.
     ELSIF      v_trabaj.caracteristica_id = '3830'
        AND  v_trabaj.tipo_trabajo = 'CAMBI' THEN
        p_cambio_tecno := TRUE;
     ELSIF      v_trabaj.caracteristica_id = '2909'
        AND    v_trabaj.tipo_trabajo = 'CAMBI'
        AND    NOT p_traslado                THEN
        p_soli_ext := TRUE;
     ELSIF  v_trabaj.caracteristica_id = 1
         AND v_trabaj.tipo_trabajo = 'NUEVO'
         AND v_solic.tipo_elemento_id = 'SERV' THEN
         p_nuevo_serv := TRUE;
     END IF;

 END LOOP Trabajos_Solicitud;


 /* Obtener Tecnologia */
 CLOSE c_trabaj;
 w_carac := 207;
 OPEN  c_carac ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id, w_carac);
 FETCH c_carac INTO w_tecnologia;
 IF c_carac%NOTFOUND THEN
    w_tecnologia := NULL;
 END IF;
 CLOSE c_carac;

 -- Verificar sí la tecnología es nula.  Normalmente en caso de traslados
 IF p_traslado AND w_tecnologia IS NULL THEN
    w_tecnologia   := PKG_IDENTIFICADORES.fn_tecnologia ( v_solic.identificador_id);
 END IF;

 -- 2008-05-27
 -- Verificacion de mensaje en la observacion para no realizar provision automática
 v_sin_automatica   := INSTR(UPPER(v_solic.observacion), 'SIN AUTOMATICA');
 IF v_sin_automatica > 0 THEN

    NULL;
 END IF;


  -- 2010-09-21 En adelante se utilizará el procedimiento PR_VERIFICAR_CICLO_CORRERIA
 -- para realizar las verificaciones y saber si un pedido se coloca en concepto PUMED.
  IF p_nuevo OR p_traslado THEN
    IF  p_nuevo THEN
        v_identificador_pumed := v_solic.identificador_id_nuevo;
    ELSIF p_traslado THEN
        v_identificador_pumed := v_solic.identificador_id;
    END IF;

           PR_VERIFICAR_CICLO_CORRERIA(v_solic.pedido_id,
                                       v_solic.subpedido_id,
                                       v_solic.solicitud_id,
                                       v_identificador_pumed,
                                       w_estado_id,
                                       w_concepto,
                                       w_obs_pumed);


          IF NVL(w_obs_pumed,'N')<>'N' THEN
                  BEGIN
                        UPDATE fnx_solicitudes
                           SET estado_soli   = 'PENDI',
                               estado_id     = w_estado_id,
                               concepto_id   = w_concepto,
                               observacion   =  '<#TV-RES ==> '||w_concepto||'-->'||observacion
                         WHERE pedido_id     = v_solic.pedido_id
                           AND subpedido_id  = v_solic.subpedido_id;

                  EXCEPTION
                  WHEN OTHERS THEN

                           NULL;
                  END;
                  b_ingre_pumed:=TRUE;
                  EXIT;
           END IF;
     END IF;
    -- FIN VERIFICACION CONCEPTOS INGRE-PUMED.
/* NUEVO Y TRASLADO */

 -- 2008-02-01   Aacosta   Validación del componente SRV para la asignación del identificador de televisión.
 IF p_nuevo AND v_solic.tipo_elemento = 'SRV'
 THEN

     --2013-03-07 ETachej - REQ_GeneNuevNumePedi - Ampliar campo pedido
     w_uno  := SUBSTR(w_pedido, length(w_pedido), 1);
     w_dos  := SUBSTR(w_pedido, length(w_pedido)-1, 1);
     w_tres := SUBSTR(w_pedido, length(w_pedido)-2, 1);

     -- Uso de procedimiento PR_TRAER_IDENTIF_ELEMENTO para para buscar y presupuestar
     -- un identificador de TELEV libre
     FENIX.PR_TRAER_IDENTIF_ELEMENTO (  w_uno, w_dos, w_tres
                    ,'ENTRE', 'TELEV', 'SRV', 'TELEV'
                    ,w_identificador
                    ,p_mensaje );

     IF w_identificador IS NOT NULL THEN

         w_encontro_numero     := TRUE;

         BEGIN
              UPDATE FNX_SUBPEDIDOS
               SET    identificador_id = w_identificador
              WHERE  pedido_id        = v_solic.pedido_id
               AND       subpedido_id        = v_solic.subpedido_id;
         EXCEPTION
               WHEN OTHERS THEN
                       NULL;
         END;


         w_estado_id    := 'ORDEN';
         w_concepto        := 'PSERV';
         v_estado_solic := 'DEFIN';
         w_tecnologia   := 'LOGIC';

         -- 2006-10-02
         -- Verificacion de paquete en la solucion, para actualizar identificador
         -- de paquete en la caracteristica 2879.  Adicion de subpedidos
         OPEN  c_paquete_identificador (v_solic.pedido_id);
         FETCH c_paquete_identificador INTO v_paquete_identificador;
         IF c_paquete_identificador%NOTFOUND THEN
            v_paquete_identificador := NULL;
         END IF;
         CLOSE c_paquete_identificador;

         IF v_paquete_identificador IS NOT NULL THEN
            UPDATE FNX_CARACTERISTICA_SOLICITUDES
            SET    valor        = v_paquete_identificador
            WHERE  pedido_id    = v_solic.pedido_id
            AND    subpedido_id = v_solic.subpedido_id
            AND    solicitud_id = v_solic.solicitud_id
            AND    caracteristica_id = 2879;
         END IF;

         -- 2008-04-08   Aacosta   Actualización de la característica 90 para cada uno de los componentes
         --                        asociados al pedido - subpedido de Televisión.
         IF NOT w_incons THEN
              BEGIN
                 UPDATE fnx_caracteristica_solicitudes
                    SET valor             = w_identificador
                  WHERE pedido_id         = v_solic.pedido_id
                    AND subpedido_id      = v_solic.subpedido_id
                    AND caracteristica_id = 90;
              EXCEPTION
                 WHEN OTHERS THEN
                    w_encontro_numero := FALSE;
                    w_mensaje_error :=
                          '<ET-TV> Actualizando car 90'|| SUBSTR(SQLERRM, 1, 50);
              END;

         END IF;

         IF NOT w_incons THEN
            b_act_solicitud := TRUE;
         END IF;

     ELSE
         w_incons := TRUE;
         w_encontro_numero := FALSE;
        w_mensaje_error   := '<ET-TELEV> No existen ID libres de TELEV.';
     END IF;
 -- 2008-03-23   Aacosta  Provisión del SRV para los casos de tralado del servicio para IPTV.
 ELSIF p_traslado AND  v_solic.tipo_elemento = 'SRV'
 THEN
     w_estado_id    := 'ORDEN';
     w_concepto        := 'PSERV';
     v_estado_solic := 'DEFIN';
     w_tecnologia   := 'LOGIC';

     IF NOT w_incons THEN
        b_act_solicitud := TRUE;
     END IF;

 -- 2008-01-30
 -- Aacosta  Cambio de la condición v_solic.tipo_elemento_id = 'TELEV' por
 -- v_solic.tipo_elemento = 'CMP' para el nuevo modelo de IPTV.
 ELSIF  ( p_nuevo  OR p_traslado )  AND v_solic.tipo_elemento = 'CMP' AND
          w_tecnologia IN ('HFC','PAR','COA') AND
          v_sin_automatica = 0                AND
          v_solic.tipo_elemento_id = 'INSHFC' AND
          NOT b_stbox_hdd
 THEN

      -- Busqueda del dato del agrupador de servicio
      OPEN  c_subped ( v_solic.subpedido_id );
      FETCH c_subped INTO w_agrupador;
      CLOSE c_subped;



      IF w_agrupador IS NOT NULL THEN
         w_identificador := w_agrupador||'-HFC';
      ELSE
         w_incons := TRUE;
         w_mensaje_error   := '<ET-TELEV> Agrupador es nulo';
      END IF;

      -- 2008-10-08
      -- Determinar enrutamiento de Estudio Técnico del producto con base en el Municipio
      -- de servicio de la solicitud.
       v_municipio_servicio := PKG_SOLICITUDES.fn_valor_caracteristica
                              (v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id, 34 );

       IF v_municipio_servicio IS NOT NULL THEN

          -- Buscar el municipio de salida internet asociado al Municipio de servicio.
          v_munic_salida_int := PKG_PROV_INTER_UTIL.fn_munic_salida (v_municipio_servicio);
          IF v_munic_salida_int IS NOT NULL THEN
             -- Buscar la ruta de Estudio Técnico asociada al proveedor
             -- municipio y producto
             PR_RUTAS_PROV_ET ( v_solic.empresa_id,
                                v_munic_salida_int, 'TELEV',
                                vo_enrutar_et,  w_mensaje_error );

             IF vo_enrutar_et IS NULL THEN
                vo_enrutar_et := 'S';
             END IF;

             -- 2010-08-02
             -- Búsqueda de configuracion para variables de Asignación Automàtica asociadas al municipio,
             -- producto y tecnología..
             OPEN  c_rutasprov ( v_solic.empresa_id, v_munic_salida_int, 'TELEV',  'HFC');
             FETCH c_rutasprov INTO v_rutasprov;
             CLOSE c_rutasprov;

             -- 2010-08-02
             -- Establecer variable de Estudio Tècnico Automàtico Habilitado con base en campo AA_SWITCHE_ASIGNACION
             IF  p_nuevo      AND INSTR(v_rutasprov.aa_switche_asignacion, '1') > 0 THEN
                 b_estudio_automatico := TRUE;
             ELSIF p_traslado AND INSTR(v_rutasprov.aa_switche_asignacion, '2') > 0 THEN
                 b_estudio_automatico := TRUE;
             ELSE
                 b_estudio_automatico := FALSE;
             END IF;


             -- 2008-07-13   Aacosta
             -- Verificación si el pedido de Televisión ingresado es Local o de Cableras. Caso de ser
             -- Cableras se verifica la característica 3840, que es la página. Con ella se trata de hacer
             -- la asignación automática si éste está en las zonas de influencias.

             IF b_estudio_automatico THEN

                 -- 2010-08-02
                 -- Se utiliza el dominio CABLERA_PAGINA_VALOR_3840 en lugar del dominio anterior CABLERA.
                 -- Antes la condicion comparaba el municipio de la solicitud (no el de servicio), Para el caso
                 -- de Cali no funcionaba, debido a que las solicitudes de Cali, ingresan por el portafolio de
                 -- Medellin.

                 IF NVL(FN_DOMINIO_DESCRIPCION('CABLERA_PAGINA_VALOR_3840', v_munic_salida_int), 'NO') = 'NO'
                 THEN
                    v_carac_pagina := 38;
                    v_instalado := NULL;
                 ELSE
                    v_carac_pagina := 3840;
                    v_instalado := 'A';
                 END IF;

                 -- 2010-08-02
                 -- Se utiliza el dominio CABLERA_SIN_TERMINAL_DERIV para verificar sí para el municipio de
                 -- servicio se hace la verificación de la red, sólo hasta el elemento DERIVADOR en
                 -- los casos que no exista el terminal o boca.
                 IF NVL(FN_DOMINIO_DESCRIPCION('CABLERA_SIN_TERMINAL_DERIV', v_municipio_servicio), 'NO') = 'SI'
                 THEN
                    b_sin_terminal_deriv := TRUE;
                 ELSE
                    b_sin_terminal_deriv := FALSE;
                 END IF;

             END IF;

          ELSE
             -- 2010-08-02
             -- Si la variable v_munic_salida_int se asigna el valor TRUE a w_incons y la variable vo_enrutar_et
             -- se hace nula para evitar enrutamiento erróneo.
             w_mensaje_error   := '<%ET-TV> No se encontró municipio para ['||v_municipio_servicio||']';
             w_incons          := TRUE;
             vo_enrutar_et     := NULL;
             b_estudio_automatico := FALSE;
          END IF;
       ELSE

           vo_enrutar_et := 'S';
       END IF;

       IF vo_enrutar_et = 'N' THEN
          v_tipo_acceso        := 'SIN-INFRA';
          v_tramo_red_asignada := 'SIN-ASIG';
          b_estudio_automatico := TRUE;
       END IF;


      /* Jrincong.  Marca de codigo para indicar codigo de zona de influencia. START -OLDZINF */
      /* SECCION Asignacion Automatica de Television */


     -- 2010-06-03   Truizd  Se valida para determinar si es un global Individual,
     --                      de ser asi no requiere asignar red de acceso.
      v_plan_fact_global_indiv := Fn_valor_caract_subpedido
                              (v_solic.pedido_id, v_solic.subpedido_id, 2005 );

       IF v_plan_fact_global_indiv = '72390808' THEN
           vo_enrutar_et              := 'N';
           v_tramo_red_asignada       := 'COMPLETA';
           v_rutasprov.aa_zonainf_hfc := 'N';
           b_global_indiv := TRUE;
       END IF;

       IF v_rutasprov.aa_buscar_reuso_red = 'S' AND vo_enrutar_et = 'S' AND b_estudio_automatico
       THEN



           -- Buscar la ruta que se debe utilizar para provisionar la red de acceso.

           PKG_PROVISION_TELEV.pr_buscar_red_acceso
                    ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
                     ,v_solic.producto_id, v_solic.tipo_elemento_id
                     ,v_carac_pagina, v_tipo_acceso
                     ,v_pedido_infra, v_subpedido_infra, v_solicitud_infra
                     ,w_ident_acceso_HFC);


       ELSE
          -- 2010-08-02
          -- Se estable variable v_tipo_acceso con el valor NO-DEFINIDO para indicar que no se realizó
          -- el proceso de búsqueda de red de acceso.
          v_tipo_acceso := 'NO-DEFINIDO';
       END IF;


       -- Asignación de red HFC con base en la infraestructura de un servicio existente
       IF v_tipo_acceso = 'EXIST_INSTA'  AND w_ident_acceso_HFC IS NOT NULL
       THEN
          PKG_PROVISION_TELEV.pr_red_existente
              ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
               ,w_ident_acceso_HFC
               ,w_agrupador, b_sin_terminal_deriv
               ,b_red_provisionada,  v_tramo_red_asignada
               ,w_incons, v_mensaje_asignacion );


               -- 2010-12-13  Aacosta  Para el caso que haya un servicio instalado, se debe tomar el estrato (caract 37) del
               --                      servicio (SRV) y actualizarlo en el pedido de BA. Proyecto Bogota.
               IF b_red_provisionada THEN
                   v_depto_nal := NVL(FN_VALOR_CARACT_SUBPEDIDO(v_solic.pedido_id, v_solic.subpedido_id,204),'N');

                   IF NVL(FN_DOMINIO_DESCRIPCION('VAL_ESTRATO_NACIONAL',v_depto_nal),'N') = 'SI' THEN
                      v_tipo_elemento_id_iden := PKG_IDENTIFICADORES.fn_elemento_id(w_ident_acceso_HFC);

                      IF NVL(v_tipo_elemento_id_iden,'N') = 'ACCESP' THEN
                         v_estrato := NVL(PKG_IDENTIFICADORES.fn_valor_configuracion
                                        (PKG_IDENTIFICADORES.fn_valor_configuracion(w_ident_acceso_HFC,2126)||'-IC001',37),'N');
                      ELSIF NVL(v_tipo_elemento_id_iden,'N') = 'TOIP' THEN
                         v_estrato := NVL(PKG_IDENTIFICADORES.fn_valor_configuracion
                                        (PKG_IDENTIFICADORES.fn_valor_configuracion(w_ident_acceso_HFC,130),37),'N');
                      END IF;
                   END IF;
               END IF;


       END IF;

       -- Asignación de red HFC con base en la infraestructura de un pedido en trámite
       IF v_tipo_acceso IN ( 'NUEVO_PAQUETE', 'TVX_PEDIDO') AND v_pedido_infra IS NOT NULL
       THEN
           PKG_PROVISION_TELEV.pr_red_tramite
             ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
               ,v_pedido_infra, v_subpedido_infra, v_solicitud_infra
               ,w_agrupador, b_sin_terminal_deriv
               ,b_red_provisionada,  v_tramo_red_asignada
               ,w_incons, v_mensaje_asignacion );

               -- 2010-12-13  Aacosta  Para el caso que haya un servicio en tramite, se debe tomar el estrato (caract 37) del
               --                      servicio (SRV) y actualizarlo en el pedido de BA. Proyecto Bogota.
               IF b_red_provisionada THEN
                   v_depto_nal := NVL(FN_VALOR_CARACT_SUBPEDIDO(v_solic.pedido_id, v_solic.subpedido_id,204),'N');

                   IF NVL(FN_DOMINIO_DESCRIPCION('VAL_ESTRATO_NACIONAL',v_depto_nal),'N') = 'SI' THEN
                      v_estrato := NVL(FN_VALOR_CARACT_SUBPEDIDO(v_pedido_infra,v_subpedido_infra,37),'N');
                   END IF;
               END IF;


       END IF;


      /* 2008-10-04.  Uso de procedimiento para asignación con uso de zonas de influencia
         Asignación de red HFC con base en ZONAS DE INFLUENCIA */
      IF  NVL(v_tipo_acceso, 'NO-DEFINIDO') = 'NO-DEFINIDO' AND v_rutasprov.aa_zonainf_hfc = 'S' AND
          vo_enrutar_et = 'S'  AND b_estudio_automatico
      THEN

         -- 2010-12-13  Aacosta  Se adiciona el nuevo parámetro estrato, con el fin de poder actualizar este dato en el pedido de Television.
         --                      Proyecto Bogota.
         PR_PROV_TELEV_ZONA_INF
             ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
               w_agrupador,   v_carac_pagina, v_instalado,
               b_red_provisionada,  v_tramo_red_asignada,
               w_incons,  v_mensaje_asignacion, v_estrato );

         v_tipo_acceso  := 'ZONAINF-';


      END IF;
      /* Jrincong.  Marca de codigo para indicar codigo de zona de influencia. END -OLDZINF */
      /* FIN ZONAS DE INFLUENCIA */

      --2010-12-13  Aacosta  Actaulizacion del estrato (Caract 37) en el pedido de televisión para zonas de influencia de Bogota.
      IF v_tramo_red_asignada IN ('COMPLETA', 'SIN-ASIG') AND v_estrato <> 'N' THEN
         BEGIN
           UPDATE FNX_CARACTERISTICA_SOLICITUDES
           SET    valor             = v_estrato
           WHERE  pedido_id         = v_solic.pedido_id
           AND    subpedido_id      = v_solic.subpedido_id
           AND    caracteristica_id = 37;
          EXCEPTION
             WHEN others THEN
                w_incons := TRUE;

         END;
      END IF;

        -- SECCION.  Definición e Inserción de identificador de TV-HFC
        -- 2008-06-09   Aacosta   Se cambia la codición para la creación del identificador de televisión.
        --                        IF w_encontro_deriv por IF w_terminal_libre.
        IF v_tramo_red_asignada IN ('COMPLETA', 'SIN-ASIG') AND p_nuevo  AND NOT w_incons
        THEN
           -- Insertar el identificador de TV - HFC
            BEGIN
                 INSERT INTO FNX_IDENTIFICADORES
                        ( identificador_id, estado,
                          fecha_estado, serie_id,
                          servicio_id,
                          producto_id, tecnologia_id)
                 VALUES ( w_identificador, 'LIB',
                          SYSDATE, v_ident.serie_id,
                          v_ident.servicio_id,
                          v_ident.producto_id, 'HFC');

                 w_encontro_numero := TRUE;

            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                     w_encontro_numero := TRUE;
                WHEN OTHERS THEN
                     w_mensaje_error := '<TELEV#> Error insertando ID <'
                                             || w_identificador||'> '
                                             ||SUBSTR (SQLERRM, 1, 50);
                     w_incons          := TRUE;
                     w_encontro_numero := FALSE;
            END;

            IF w_encontro_numero THEN
               -- Actualizar el identificador presupuestado en la tabla de TV_TRAMITES
               PKG_PROVISION_TELEV.pr_act_identif_tramhfc
                    ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
                     ,w_identificador, w_mensaje_error );

              msg_observac  := ' <AA-R>';

            END IF;

        ELSIF v_tramo_red_asignada IN ('COMPLETA', 'SIN-ASIG') AND p_traslado AND NOT w_incons
        THEN
               w_encontro_numero := TRUE;
               w_identificador   := v_solic.identificador_id;

               msg_observac  := ' <AA-R>';
        ELSE
               w_identificador   := NULL;
        END IF;

        -- Si el tramo de red asignada es SIN-ASIG, se inserta un registro en FNX_INF_TV_TRAMITES
        -- para que Ordenes pueda generar las ordenes correctamente.
        IF  v_tramo_red_asignada = 'SIN-ASIG' AND w_encontro_numero
        THEN
             BEGIN
                INSERT INTO fnx_inf_tv_tramites
                   ( pedido_id, subpedido_id, solicitud_id
                    ,identificador_id )
                VALUES
                   ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id,
                     w_identificador );
            EXCEPTION
                WHEN OTHERS THEN
                     NULL;

            END;

        ELSIF  v_tramo_red_asignada = 'COMPLETA' AND w_encontro_numero AND p_traslado
        THEN

            -- 2010-08-06
            -- Actualización de identificador ID en tabla FNX_INF_TV_TRAMITES para la transaccion
            -- de traslado

            -- Actualizar el identificador presupuestado en la tabla de TV_TRAMITES
            PKG_PROVISION_TELEV.pr_act_identif_tramhfc
                ( v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id
                 ,w_identificador, w_mensaje_error );

        END IF;

        -- FIN SECCION.  Definición e Inserción de identificador de TV

        -- SECCION Traslado Marcar como XLI el terminal derivador de la direccion origen
        IF v_tramo_red_asignada IN ('COMPLETA', 'SIN-ASIG') AND p_traslado  THEN

            OPEN  c_tvactiva (w_identificador);
            FETCH c_tvactiva INTO v_tvactiva;
            IF c_tvactiva%FOUND THEN
               IF v_tvactiva.terminal_derivador_id IS NOT NULL THEN
                  BEGIN
                     UPDATE fnx_terminales_derivadores
                        SET estado = 'XLI'
                      WHERE centro_distribucion_intermedia = v_tvactiva.centro_distribucion_intermedia
                        AND nodo_optico_electrico_id = v_tvactiva.nodo_optico_electrico_id
                        AND tipo_amplificador_id = v_tvactiva.tipo_amplificador_id
                        AND amplificador_id = v_tvactiva.amplificador_id
                        AND tipo_derivador_id = v_tvactiva.tipo_derivador_id
                        AND derivador_id = v_tvactiva.derivador_id
                        AND terminal_derivador_id = v_tvactiva.terminal_derivador_id;
                  EXCEPTION
                     WHEN OTHERS THEN
                        NULL;
                  END;
               END IF;
            END IF;

        END IF;
        -- FIN SECCION. Traslado Marcar como XLI el terminal derivador

        -- SECCION. Definición Estado y concepto de la solicitud
        -- 2008-03-17   Aacosta   Validación del concepto de la solicitud de INSHFC para el caso en que venga
        --                        un servicio de IPTV Mix. Se deja la solicitud en PSERV.


        -- Buscar sí viene una solicitud de INSIP en el pedido.  En tal caso, la solicitud
        -- de INSHFC se deja en ORDEN-PSERV.
        OPEN  c_solic_insip ( v_solic.subpedido_id );
        FETCH c_solic_insip INTO v_solicitud_insip;
        IF  c_solic_insip%NOTFOUND THEN
            v_solicitud_insip  := 0;
        END IF;
        CLOSE c_solic_insip;




        IF w_encontro_numero AND v_tramo_red_asignada IN ('COMPLETA', 'SIN-ASIG') AND
           v_solicitud_insip = 0  AND NOT b_global_indiv
        THEN
           w_estado_id    := 'ORDEN';
           w_concepto     := 'PORDE';
           v_estado_solic := 'DEFIN';
           w_automatica   := TRUE;

           -- 2010-08-26
           -- Se verifica sí en la variable v_mensaje_asignacion viene el literal @PAR, para
           -- determinar el tipo de tecnologìa (PAR)ABOLICA o HFC.
           IF INSTR(v_mensaje_asignacion, '@PAR') = 0 THEN
              w_tecnologia := 'HFC';
           ELSE
              w_tecnologia := 'PAR';
           END IF;

        ELSIF w_encontro_numero AND v_tramo_red_asignada IN ('COMPLETA', 'SIN-ASIG') AND
              v_solicitud_insip > 0
        THEN
           w_estado_id    := 'ORDEN';
           w_concepto      := 'PSERV';
           v_estado_solic := 'DEFIN';
           w_tecnologia   := 'HFC';
           w_automatica   := TRUE;
        -- 2010-06-09   Truizd   Se valida que solo ingrese cuando sea global Individual
        ELSIF w_encontro_numero AND v_tramo_red_asignada IN ('COMPLETA', 'SIN-ASIG') AND
              b_global_indiv
        THEN
           w_estado_id    := 'ORDEN';
           w_concepto      := 'PSERV';
           v_estado_solic := 'DEFIN';
           w_tecnologia   := 'HFC';
           w_automatica   := TRUE;
        ELSIF NOT w_encontro_numero AND v_tramo_red_asignada IN ('COMPLETA', 'SIN-ASIG')
        THEN
           w_estado_id    := 'TECNI';
           w_concepto      := '21';
           v_estado_solic := 'PENDI';
        ELSIF w_encontro_numero AND NOT v_tramo_red_asignada = 'PARCIAL'
        THEN
           w_estado_id    := 'TECNI';
           w_concepto      := 'PETEC';
           v_estado_solic := 'PENDI';
        ELSE
           w_estado_id    := 'TECNI';
           w_concepto      := 'PETEC';
           v_estado_solic := 'PENDI';
        END IF;

        IF NOT w_incons THEN
            b_act_solicitud := TRUE;
        END IF;

        -- 2006-07-24
        -- SECCION Validacion concepto PEXPQ. para determinar estado. Si
        -- la solicitud es empaquetada, se debe dejar en PEXPQ.
        -- 2009-10-10 Truizd Se agrega NVL porque para traslados de paquetes el identificador de paquete
        --                   viene en la caracteritica 4371(Identif Paquete Traslado).


        IF w_concepto = 'PORDE' THEN

            w_paquete    := NULL;
            w_paquete := NVL(Fn_valor_caract_subpedido ( v_solic.pedido_id
                                                        ,v_solic.subpedido_id
                                                        ,2879),
                                                        Fn_valor_caract_subpedido
                                                        ( v_solic.pedido_id
                                                        ,v_solic.subpedido_id
                                                        ,4371));

            -- 2006-09-09
            -- Logica para validacion de concepto PEXPQ con solicitudes de Empaquetamiento.
            IF w_paquete IS NOT NULL THEN

                -- Buscar si existe una solicitud de paquete pendiente en el pedido
                BEGIN
                   OPEN  c_pquete_pendiente;
                   FETCH c_pquete_pendiente INTO v_paquete_pendiente;
                   IF c_pquete_pendiente%NOTFOUND THEN
                       v_paquete_pendiente := 0;
                   END IF;
                   CLOSE c_pquete_pendiente;
                EXCEPTION
                   WHEN OTHERS THEN
                      v_paquete_pendiente := 0;
                END;

                IF v_paquete_pendiente > 0 THEN
                    -- Verificar si existen solicitudes anteriores, en algun concepto tecnico.
                    -- o en estado Ingre.  Si no existen, el concepto de la solicitud debe ser PORDE.
                    BEGIN
                      OPEN  c_solic_pendientes ( v_solic.subpedido_id );
                      FETCH c_solic_pendientes INTO v_solic_pendientes;
                      CLOSE c_solic_pendientes;
                    EXCEPTION
                       WHEN OTHERS THEN
                           v_solic_pendientes := 0;
                    END;

                    IF v_solic_pendientes = 0 THEN
                       w_concepto     := 'PORDE';
                    ELSE
                       w_concepto    := 'PEXPQ';
                    END IF;
                ELSE
                    w_concepto := 'PORDE';
                END IF;
            END IF;
        END IF;
        -- FIN Logica validacion concepto PEXPQ para Empaquetamiento.

        -- SECCION.  Validaciones parte comercial ( PUMED / PECOM
        IF w_concepto = 'PORDE' THEN

           w_obs_pumed :=  NULL;
           w_estado_id := 'ORDEN';
           w_concepto  := 'PORDE';
           w_respuesta_comercial := '0';

           BEGIN
               w_respuesta_comercial := fn_verificar_ciclo_correria (
                                           v_solic.pedido_id,
                                           v_solic.subpedido_id,
                                           v_solic.solicitud_id
                                        );
           EXCEPTION
               WHEN OTHERS THEN
                  w_respuesta_comercial := '101';
                  w_obs_pumed := 'Conexion hacia Open por fuera.';
           END;

           IF NVL (w_respuesta_comercial, '0') <> '0' THEN
               w_concepto  := 'PUMED';
               w_estado_id := 'INGRE';

               IF w_respuesta_comercial = '101' THEN
                  w_obs_pumed := 'Verificar Estrato';
               ELSIF w_respuesta_comercial = '102' THEN
                  w_obs_pumed := 'Verificar Pagina';
               ELSIF w_respuesta_comercial = '103' THEN
                  w_obs_pumed := 'Verificar ciclo';
               ELSIF w_respuesta_comercial = '104' THEN
                  w_obs_pumed := 'Verificar Correria';
               ELSIF w_respuesta_comercial = '105' THEN
                  w_concepto  := 'PECOM';
                  w_estado_id := 'INGRE';
                  w_obs_pumed := 'Verificar Liquidacion.';
               ELSE
                  w_obs_pumed :=    w_respuesta_comercial
                                 || ' Codigo desconocido';
               END IF;
           END IF;
        END IF; -- Fin IF concepto PORDE
        -- SECCION.  Validaciones parte comercial ( PUMED / PECOM )

 -- 2008-07-18  Aacosta    Provisión del componente servicio de configuración SERV
 --                        para Televisión. Se implementó para Cableras pero queda genérico.
 ELSIF p_nuevo_serv AND NOT b_stbox_hdd
 THEN
      OPEN  c_subped ( v_solic.subpedido_id);
      FETCH c_subped INTO v_subped;
      CLOSE c_subped;

      IF v_subped.identificador_id IS NOT NULL THEN
                 PKG_IDENTIFICADORES.pr_crear_identificador
                          (v_subped.identificador_id||'-'||v_solic.tipo_elemento_id
                           ,'PRE', NULL,
                           v_solic.servicio_id,  v_solic.producto_id,
                           v_solic.tipo_elemento_id, v_solic.tipo_elemento,
                           NULL, w_mensaje_error,
                           v_solic.municipio_id, v_solic.empresa_id);

                  IF w_mensaje_error IS NOT NULL THEN
                        w_mensaje_error    := 'Error insertando ID <'|| v_subped.identificador_id||v_solic.tipo_elemento_id||'> '||SUBSTR (SQLERRM, 1, 50);
                        w_encontro_numero  := FALSE;
                        b_act_solicitud    := FALSE;
                        w_incons           := TRUE;
               ELSE
                OPEN  c_solic_srv;
                FETCH c_solic_srv INTO v_solic_srv;
                CLOSE c_solic_srv;

                IF     NVL(v_solic_srv,'NOP') IN ('CAMBI','NOP') THEN
                    w_estado_id       := 'ORDEN';
                    w_concepto          := 'PORDE';
                    v_estado_solic    := 'DEFIN';
                    w_tecnologia      := 'HFC';
                    b_act_solicitud   := TRUE;
                    w_automatica      := TRUE;
                      w_encontro_numero := TRUE;
                      w_identificador   := v_subped.identificador_id||'-'||v_solic.tipo_elemento_id;
                    w_agrupador       := v_subped.identificador_id;
                ELSE
                    b_act_solicitud := FALSE;
                END IF;
                  END IF;
      END IF;
 ELSIF  p_nuevo  AND v_solic.tipo_elemento_id = 'SERHFC' AND
        NOT b_stbox_hdd --AND NOT b_tiene_tvdigital
 THEN


  b_pedido_tvdigital:= FN_PEDIDO_TVDIGITAL(v_solic.pedido_id,v_solic.subpedido_id,
                                           PKG_SOLICITUDES.fn_valor_caracteristica(v_solic.pedido_id,
                                                                                   v_solic.subpedido_id,
                                                                                   v_solic.solicitud_id,
                                                                                   90));

          OPEN  c_subped ( v_solic.subpedido_id);
          FETCH c_subped INTO v_subped;
          CLOSE c_subped;

          IF v_subped.identificador_id IS NOT NULL THEN
                     PKG_IDENTIFICADORES.pr_crear_identificador
                              (v_subped.identificador_id||'-'||v_solic.tipo_elemento_id
                               ,'PRE', NULL,
                               v_solic.servicio_id,  v_solic.producto_id,
                               v_solic.tipo_elemento_id, v_solic.tipo_elemento,
                               NULL, w_mensaje_error,
                               v_solic.municipio_id, v_solic.empresa_id);

                      IF w_mensaje_error IS NOT NULL THEN
                            w_mensaje_error    := 'Error insertando ID <'|| v_subped.identificador_id||v_solic.tipo_elemento_id||'> '||SUBSTR (SQLERRM, 1, 50);
                            w_encontro_numero  := FALSE;
                            b_act_solicitud    := FALSE;
                            w_incons           := TRUE;
                   ELSE
                    OPEN  c_solic_srv;
                    FETCH c_solic_srv INTO v_solic_srv;
                    CLOSE c_solic_srv;

                    -- 2010-06-16   Aacosta      Se crea el nuevo cursor c_soli_insta, con el fin de verificar si en el pedido llega una solicitud de
                    --                           INSIP o INSHFC en estado TECNI. en caso de venir no se debe aprovisionar ninguno de los demas
                    --                           componentes del pedido.
                    OPEN  c_soli_insta;
                    FETCH c_soli_insta INTO v_soli_insta;
                    CLOSE c_soli_insta;

                    IF     NVL(v_solic_srv,'NOP') IN ('CAMBI','NOP') AND
                           v_soli_insta = 0
                    THEN
                        w_estado_id       := 'ORDEN';
                        IF NOT b_pedido_tvdigital THEN
                            w_concepto          := 'PORDE';
                        ELSE
                             w_concepto          := 'POPTO';
                        END IF ;

                        v_estado_solic    := 'DEFIN';
                        w_tecnologia      := 'HFC';
                        b_act_solicitud   := TRUE;
                        w_automatica      := TRUE;
                        w_encontro_numero := TRUE;
                        w_identificador   := v_subped.identificador_id||'-'||v_solic.tipo_elemento_id;
                        w_agrupador       := v_subped.identificador_id;
                    ELSE
                        b_act_solicitud := FALSE;
                    END IF;


                      END IF;
          END IF;

 ELSIF  p_nuevo  AND v_solic.tipo_elemento_id = 'SERVIP' AND
        NOT b_stbox_hdd
 THEN
      OPEN  c_subped ( v_solic.subpedido_id);
      FETCH c_subped INTO v_subped;
      CLOSE c_subped;

      IF v_subped.identificador_id IS NOT NULL THEN
                 PKG_IDENTIFICADORES.pr_crear_identificador
                          (v_subped.identificador_id||'-'||v_solic.tipo_elemento_id
                           ,'PRE', NULL,
                           v_solic.servicio_id,  v_solic.producto_id,
                           v_solic.tipo_elemento_id, v_solic.tipo_elemento,
                           NULL, w_mensaje_error,
                           v_solic.municipio_id, v_solic.empresa_id);

                  IF w_mensaje_error IS NOT NULL THEN
                        w_mensaje_error    := 'Error insertando ID <'|| v_subped.identificador_id||v_solic.tipo_elemento_id||'> '||SUBSTR (SQLERRM, 1, 50);
                        w_encontro_numero  := FALSE;
                        b_act_solicitud    := FALSE;
                        w_incons           := TRUE;
               ELSE
                OPEN  c_solic_srv;
                FETCH c_solic_srv INTO v_solic_srv;
                CLOSE c_solic_srv;
                -- 2008-05-27   Aacosta  Validación del tipo de solicitud para las peticiones de nuevo SERVIP. Se
                --                       verifica que el componente de TELEV sea de tipo CAMBI.

                -- 2010-06-16   Aacosta      Se crea el nuevo cursor c_soli_insta, con el fin de verificar si en el pedido llega una solicitud de
                --                           INSIP o INSHFC en estado TECNI. en caso de venir no se debe aprovisionar ninguno de los demas
                --                           componentes del pedido.
                OPEN  c_soli_insta;
                FETCH c_soli_insta INTO v_soli_insta;
                CLOSE c_soli_insta;

                IF     NVL(v_solic_srv,'NOP') IN ('CAMBI','NOP') AND
                       v_soli_insta = 0
                THEN
                    w_estado_id       := 'ORDEN';
                    w_concepto        := 'PORDE';
                    v_estado_solic    := 'DEFIN';
                    w_tecnologia      := 'REDCO';
                    b_act_solicitud   := TRUE;
                    w_automatica      := TRUE;
                    w_encontro_numero := TRUE;
                    w_identificador   := v_subped.identificador_id||'-'||v_solic.tipo_elemento_id;
                    w_agrupador       := v_subped.identificador_id;
                ELSE
                    b_act_solicitud := FALSE;
                END IF;
                  END IF;
      END IF;
-- 2011-08-02 Lógica para el nuevo elemento ELSIS. Se debe colocar en PORDE
-- el componente ELSIS que viene como adición
ELSIF  p_nuevo  AND v_solic.tipo_elemento_id = 'ELSIS' AND
        NOT b_stbox_hdd --AND NOT b_existe_servicio
THEN
                  PKG_IDENTIFICADORES.pr_crear_identificador
                            (pkg_solicitudes.fn_valor_caracteristica(v_solic.pedido_id,
                                                                     v_solic.subpedido_id,
                                                                     v_solic.solicitud_id,90)||'-'||v_solic.tipo_elemento_id
                            ,'PRE', NULL,
                               v_solic.servicio_id,  v_solic.producto_id,
                               v_solic.tipo_elemento_id, v_solic.tipo_elemento,
                               NULL, w_mensaje_error,
                               v_solic.municipio_id, v_solic.empresa_id);

                  IF w_mensaje_error IS NOT NULL THEN
                        w_mensaje_error    := 'Error insertando ID <'|| v_subped.identificador_id||v_solic.tipo_elemento_id||'> '||SUBSTR (SQLERRM, 1, 50);
                        w_encontro_numero  := FALSE;
                        b_act_solicitud    := FALSE;
                        w_incons           := TRUE;

                  ELSE
                    IF b_existe_servicio AND b_existe_elsis THEN
                             w_estado_id       := 'ORDEN';
                             w_concepto        := 'PINSC';
                             v_estado_solic    := 'DEFIN';
                             w_tecnologia      := 'LOGIC';
                             b_act_solicitud   := TRUE;
                    /*ELSIF NOT b_existe_servicio AND b_existe_elsis THEN
                             w_estado_id       := 'ORDEN';
                             w_concepto        := 'PORDE';
                             v_estado_solic    := 'DEFIN';
                             w_tecnologia      := 'LOGIC';
                             b_act_solicitud   := TRUE;*/
                   END IF;
                  END IF;
                    --w_automatica      := TRUE;
                    --w_encontro_numero := TRUE;
                     w_identificador   := pkg_solicitudes.fn_valor_caracteristica(v_solic.pedido_id,v_solic.subpedido_id,v_solic.solicitud_id,90)||'-'||v_solic.tipo_elemento_id;
                   -- w_agrupador       := v_subped.identificador_id;
 ELSIF p_retiro_acceso THEN  -- R E T I R O
 
      OPEN  c_subped ( v_solic.subpedido_id);
      FETCH c_subped INTO v_subped;
      CLOSE c_subped;

      -- Verificar si en el pedido de retiro vienen solicitudes de INSIP o INSHFC.
      OPEN  c_soli_acceso ( v_solic.subpedido_id );
      FETCH c_soli_acceso INTO v_soli_acceso;
      CLOSE c_soli_acceso;

      -- 2008-10-04
      -- Buscar sí existe un componente de Nueva TELEVision en el mismo pedido y diferente al subpedido
      -- actual.  En tal caso, se evalua como un cambio de producto
      b_retiro_cambio_producto := FALSE;
      OPEN   c_nueva_solic_tv ( v_solic.subpedido_id, 'TELEV');
      FETCH  c_nueva_solic_tv INTO v_total_nuevos;
      IF  c_nueva_solic_tv%NOTFOUND THEN
          v_total_nuevos := 0;
      END IF;
      CLOSE  c_nueva_solic_tv;

     
      -- 2010-10-19 Sección de validación de demostración. Se encuentra el valor de la caracteristica
      -- 3851, en caso de venir, se activa la variable b_demostracion para determinar que se trata de una demostración.
       b_demostracion     := FALSE;
        -- 2012-08-08 Se modifica el cursor para que de ahora en adelante, acepte también el tipo elemento
        -- SERHFC.
        -- 2012-07-05 Se modifica el tipo de elemento que se manejaba en demostraciones, de 
        --            ahora en adelante, será con el SERVIP.
       --OPEN C_DEMOSTRACION (v_solic.pedido_id,'INSIP',3851);
       OPEN C_DEMOSTRACION (v_solic.pedido_id,3851);--'SERVIP',3851);
       FETCH C_DEMOSTRACION INTO v_demostracion;
       CLOSE C_DEMOSTRACION;

       IF NVL(v_demostracion.valor,'N')<>'N' THEN
              b_demostracion:=TRUE;
       END IF;

       -- 2012-08-08 Se modifica el cursor para que de ahora en adelante, acepte también el tipo elemento
       -- SERHFC.
       --2010-10-20 Para el tema de cambio de paquete, se debe realizar la validación con relación al elemento
       --SERVIP.
        b_demostracion_cambio_paquete:= FALSE;
        OPEN C_DEMOSTRACION (v_solic.pedido_id,3851);--(v_solic.pedido_id,'SERVIP',3851);
        FETCH C_DEMOSTRACION INTO v_demostracion_cambio_paquete;
        CLOSE C_DEMOSTRACION;

        IF NVL(v_demostracion_cambio_paquete.valor,'N')<>'N' THEN
           b_demostracion_cambio_paquete:=TRUE;
        END IF;

         IF v_total_nuevos > 0 THEN
            -- 2010-11-03 Se coloca en estado PCPRO solo cuando no se trata de una demostración.
            IF b_demostracion THEN
                  b_retiro_cambio_producto  := FALSE;
            ELSIF b_demostracion_cambio_paquete THEN
                  b_retiro_cambio_producto  := FALSE;
            ELSE
                 b_retiro_cambio_producto  := TRUE;

            END IF;
      END IF;

      IF v_subped.identificador_id IS NOT NULL THEN
      
           -- Cambiar el estado del identificador que se retira a XLI
           BEGIN
               UPDATE FNX_IDENTIFICADORES
               SET estado = 'XLI'
               WHERE identificador_id = v_solic.identificador_id;
           EXCEPTION
               WHEN OTHERS THEN
                   NULL;
           END;

           -- Actualizar la fecha de estudio técnico
           BEGIN
               UPDATE FNX_REPORTES_INSTALACION
               SET    fecha_est_tecnico = SYSDATE
               WHERE  pedido_id    = v_solic.pedido_id
               AND    subpedido_id = v_solic.subpedido_id
               AND    solicitud_id = v_solic.solicitud_id;
           EXCEPTION
              WHEN OTHERS THEN
                 NULL;
           END;
            
           OPEN  c_tvactiva ( v_solic.identificador_id);
           FETCH c_tvactiva INTO v_tvactiva;
           CLOSE c_tvactiva;

          -- Marcar en estado XLI el terminal derivador asociado al servicio
          IF v_tvactiva.terminal_derivador_id IS NOT NULL THEN
                   BEGIN
                      UPDATE FNX_TERMINALES_DERIVADORES
                         SET estado = 'XLI'
                       WHERE centro_distribucion_intermedia = v_tvactiva.centro_distribucion_intermedia
                       AND derivador_id = v_tvactiva.derivador_id
                       AND amplificador_id = v_tvactiva.amplificador_id
                       AND tipo_amplificador_id = v_tvactiva.tipo_amplificador_id
                       AND nodo_optico_electrico_id = v_tvactiva.nodo_optico_electrico_id
                       AND tipo_derivador_id = v_tvactiva.tipo_derivador_id
                       AND terminal_derivador_id = v_tvactiva.terminal_derivador_id;
                   EXCEPTION
                      WHEN OTHERS THEN
                         NULL;
                   END;
          END IF;
            
          -- 2008-03-14  Aacosta    Se agrega la validaciòn para retiros del servicio. IPTV
          IF    v_solic.tipo_elemento = 'SRV' THEN
             IF v_soli_acceso > 0 THEN
                
                w_estado_id    := 'ORDEN';
                w_concepto       := 'PSERV';
                v_estado_solic := 'DEFIN';
                w_tecnologia   := 'LOGIC';
             ELSE
                
                w_estado_id    := 'ORDEN';
                w_concepto       := 'PORDE';
                v_estado_solic := 'DEFIN';
                w_tecnologia   := 'HFC';
             END IF;

          ELSIF v_solic.tipo_elemento = 'CMP' THEN
            
            v_plan_comercial := PKG_IDENTIFICADORES.fn_valor_configuracion(v_subped.identificador_id, 3830);

            OPEN  c_solic_insip ( v_solic.subpedido_id );
            FETCH c_solic_insip INTO v_solicitud_insip;
            IF  c_solic_insip%NOTFOUND THEN
                v_solicitud_insip  := 0;
            END IF;
            CLOSE c_solic_insip;

            --  2009-06-23 Lquintez
            IF v_solic.tipo_elemento_id = 'INSHFC' THEN
                
                 IF NVL(fn_devuelve_descripcion_ltcar(v_plan_comercial, 3849), 'N') = 'MIXTO' THEN
                        
                        IF v_solicitud_insip > 0 THEN
                        
                            w_estado_id    := 'ORDEN';
                            w_concepto       := 'PSERV';
                            v_estado_solic := 'DEFIN';
                            w_tecnologia   := 'HFC';
                        ELSE
                        
                            w_estado_id    := 'ORDEN';
                            w_concepto       := 'PORDE';
                            v_estado_solic := 'DEFIN';
                            w_tecnologia   := 'HFC';
                        END IF;
                 ELSE
                   
                    
                   v_identificador_servicio := fn_valor_caract_identif (v_solic.identificador_id,90);
                   
                    
                   IF v_identificador_servicio IS NOT NULL THEN
                        
                       v_estado_servicio := fn_valor_caract_identif (v_identificador_servicio,132);
                       
                        
                    
                       IF NVL(v_estado_servicio,'ACTI') IN ('SXFP','SXOS') THEN
                            
                           w_estado_id     := 'ORDEN';
                           w_concepto      := 'PSERV';
                           v_estado_solic  := 'DEFIN';
                           w_tecnologia    := 'HFC';
                           b_retir_hfc     := TRUE;
                       END IF;
                    
                   END IF;
                    
                   IF NOT b_retir_hfc THEN
                    
                       w_estado_id     := 'ORDEN';
                       w_concepto      := 'PORDE';
                       v_estado_solic  := 'DEFIN';
                       w_tecnologia    := 'HFC';
                   END IF;
                 END IF;
            ELSIF v_solic.tipo_elemento_id = 'INSIP' THEN
            
                -- 2014-06-27.  Req 36900 GPON Hogares.
                -- La variable tecnología se asigna con el valor de la tecnología del identificador. 
                w_tecnologia   := PKG_IDENTIFICADORES.fn_tecnologia (v_solic.identificador_id);

                -- 2010-05-10
                -- Verificar si se deben generar puentes. Se consulta la variable CONTROL_PUENTES de rutas provisión.
                v_municipio_servicio :=  fn_valor_caract_identif (v_solic.identificador_id, 34);

                IF v_municipio_servicio IS NOT NULL THEN

                    -- Buscar el municipio de salida internet asociado al Municipio  de servicio.
                    v_munic_salida_int := PKG_PROV_INTER_UTIL.fn_munic_salida (v_municipio_servicio);

                    -- 2014-06-27.  Req 36900 GPON Hogares. 
                    -- Se agrega condición de tecnología REDCO para la generación de puentes.  No es necesario cuando la tecnología
                    -- es GPON. 

                    IF v_munic_salida_int IS NOT NULL AND w_tecnologia = 'REDCO'
                    THEN

                        v_rutasprov.control_puentes := NULL;
                        v_rutasprov.control_ordenes := NULL;
                        
                        -- Buscar la ruta de provisión de servicio asociada al proveedor, municipio y servicio
                        OPEN  c_rutasprov ( v_solic.empresa_id, v_munic_salida_int, 'TELEV',  'REDCO');
                        FETCH c_rutasprov INTO v_rutasprov;

                        IF c_rutasprov%NOTFOUND THEN
                           DBMS_OUTPUT.PUT_LINE('ET-TELEV: No se encontró registro REDCO en Rutas Provisión: '||v_munic_salida_int);
                        END IF;
                        CLOSE c_rutasprov;

                        IF NVL(v_rutasprov.control_puentes,'N') = 'S'
                           AND NVL(v_rutasprov.control_ordenes,'N') = 'S'                           
                        THEN
                            -- 2010-05-05
                            -- Uso de procedimiento para generar puentes de configuraciòn / desconfiguracion cuando
                            -- se retira un servicio de TvDig INSIP . El procedimiento verifica condiciones de
                            -- Multiservicio.
                            PR_PROV_PUENTES_ORIGEN
                            ( p_pedido              => v_solic.pedido_id
                            ,p_subpedido            => v_solic.subpedido_id
                            ,p_solicitud            => v_solic.solicitud_id
                            ,p_tipo_elemento_id     => 'DATBA'
                            ,p_tipo_transaccion     => 'RETIR'
                            ,p_identificador_actual => v_solic.identificador_id
                            );
                        END IF;

                    ELSE
                        DBMS_OUTPUT.PUT_LINE('ET-TELEV: Retiros: No se encontró un municipio de salida Internet: '||v_municipio_servicio);
                    END IF;
                ELSE
                   DBMS_OUTPUT.PUT_LINE('ET-TELEV: Retiros: No se encontró municipio de servicio: '||v_solic.identificador_id);
                END IF;
                
                --2014-01-15 lhernans
                --Se determina si se inserta o no etiqueta
                
                --2014-10-22   RVELILLR Condicional de tecnologia para consulta de etiquetas para generar eventos.
                IF NVL(w_tecnologia, 'NOT') = 'REDCO' THEN
                    v_aplicacion := 'DSLAM';            
                                            
                    --Se recupera valor de la etiqueta
                    --formado por ETBTS_[MUNICIPIO]_[PRODUCTO]_[TECNOLOGIA] (Sin corchetes)
                    v_etiqueta := PKG_ETIQUETAS_EVENTOS_BTS.FN_VALOR_PARAM_EVENTOS_BTS (
                                                    v_aplicacion,
                                                    v_solic.producto_id,
                                                    v_solic.tipo_elemento_id,                                                
                                                    'RETIR',
                                                    1
                                                    );
                ELSE
                 --Se recupera valor de la etiqueta
                   v_etiqueta := fn_dominio_values_meaning (
                                        'ETBTS'
                                           || '_'
                                           || v_munic_salida_int
                                           || '_'
                                           || v_solic.producto_id
                                           || '_'
                                           || w_tecnologia,
                                        'RETIR',
                                        1
                                        );
                END IF;                        
                --2014-10-22   RVELILLR FIN Lógica.
                
                IF NVL (v_etiqueta, 'NA') <> 'NA' THEN
                
                   --Rutina de inserción de etiquetas
                   pr_insertar_etiquetas_bts (
                        v_solic.pedido_id,
                        v_solic.subpedido_id,
                        v_solic.solicitud_id,
                        v_solic.servicio_id,
                        v_solic.producto_id,
                        v_solic.tipo_elemento,
                        v_solic.tipo_elemento_id,
                        4805,
                        v_solic.municipio_id,
                        v_solic.empresa_id,
                        v_etiqueta,
                        w_mensaje_error
                        );
                END IF;
                --2014-01-15 lhernans FIN Lógica.

                w_estado_id    := 'ORDEN';
                w_concepto     := 'PORDE';
                v_estado_solic := 'DEFIN';
                
                -- 2014-06-27.  Req 36900 GPON Hogares.
                -- La variable tecnología se asigna previamente con el valor de la tecnología del identificador.  
                -- Antes estaba REDCO y generaba problemas para accesos con tecnología GPON.                 
                -- w_tecnologia   := 'REDCO';
                
            END IF;


            -- 2014-06-27.  Req 36900 GPON Hogares.
            -- En la instrucción de UPDATE de solicitudes del cursor regcmp_equ, se cambia el valor REDCO por la variable w_tecnología 
            -- para que corresponda con la tecnología del identificador de acceso.  

             -- 2008-04-01  Aacosta   Se verifica si en la solicitud de retiro del componente vienen otras solicitudes, ya
             --                       sea de equipos o componentes adicionales.
             FOR regcmp_equ IN c_solic_cmp_equ (v_solic.pedido_id,v_solic.subpedido_id)  LOOP
                  BEGIN
                     UPDATE fnx_solicitudes
                        SET estado_soli   = 'DEFIN',
                            estado_id     = 'ORDEN',
                            concepto_id   = 'POPTO',
                            tecnologia_id = w_tecnologia,
                            observacion   = '<#TV-RES ==> POPTO '||observacion
                      WHERE pedido_id    = v_solic.pedido_id
                        AND subpedido_id = regcmp_equ.subpedido_id
                        AND solicitud_id = regcmp_equ.solicitud_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        w_incons          := TRUE;
                        w_encontro_numero := FALSE;
                        w_mensaje_error :=
                              'Error actualizando componentes/equipos. '
                           || SQLERRM;
                  END;
             END LOOP;

          END IF; -- Fin Evaluacion Elementos SRV - CMP;
            
          -- 2008-10-04
          -- Cambio de Producto.  El concepto previamente establecido se cambia a PCPRO
          IF b_retiro_cambio_producto THEN
               w_concepto     := 'PCPRO';
               
          END IF;

          -- 2008-10-04
          -- La variable identificador se pone en nulo, para que no se actualiza en el campo ID nuevo de
          -- la solicitud que se está retirando
          w_identificador   := NULL;

          IF NOT w_incons AND v_solic.tipo_elemento_id NOT IN ('SERHFC','SERVIP') THEN
          
              b_act_solicitud := TRUE;
          END IF;
    
       END IF; /* FIN Agrupador IS NULL */

  ELSIF p_retiro_cmp THEN
        

      OPEN  c_subped ( v_solic.subpedido_id);
      FETCH c_subped INTO v_subped;
      CLOSE c_subped;

      -- Cursor para verificar el estado de la solicitud del componente.
      OPEN  c_est_soli (v_solic.subpedido_id,v_solic.solicitud_id);
      FETCH c_est_soli INTO v_est_soli;
      CLOSE c_est_soli;

      IF v_subped.identificador_id IS NOT NULL AND v_est_soli = 'TECNI' THEN
        -- Cambiar el estado del identificador de SERVIP o SERHFC que se retira a XLI.
        BEGIN
            UPDATE FNX_IDENTIFICADORES
            SET estado = 'XLI'
            WHERE identificador_id = v_solic.identificador_id;
        EXCEPTION
            WHEN OTHERS THEN
                NULL;
        END;

         FOR regcmp IN c_retir_cmp LOOP
            IF regcmp.tipo_elemento <> 'EQU' THEN
               -- Cambiar el estado del identificador que se retira a XLI
               BEGIN
                   UPDATE FNX_IDENTIFICADORES
                   SET estado = 'XLI'
                   WHERE identificador_id = regcmp.identificador_id;
               EXCEPTION
                   WHEN OTHERS THEN
                       NULL;
               END;
            END IF;
            
            -- Actualizar los estados / conceptos de los componentes asociados al SERVIP o SERHFC.
            BEGIN
                 UPDATE fnx_solicitudes
                    SET estado_soli   = 'DEFIN',
                        estado_id     = 'ORDEN',
                        concepto_id   = 'PORDE',
                        observacion   = '<#TV-RES ==> PORDE '||observacion
                  WHERE pedido_id    = v_solic.pedido_id
                    AND subpedido_id = regcmp.subpedido_id
                    AND solicitud_id = regcmp.solicitud_id;

                  UPDATE fnx_reportes_instalacion
                    SET    fecha_est_tecnico = SYSDATE,
                          fecha_estado_carta = SYSDATE,
                          estado_carta    = 'APROB'
                  WHERE  pedido_id       = v_solic.pedido_id
                  AND    subpedido_id    = regcmp.subpedido_id
                  AND    solicitud_id    = regcmp.solicitud_id;
              EXCEPTION
                 WHEN OTHERS THEN
                    w_incons          := TRUE;
                    w_encontro_numero := FALSE;
                    w_mensaje_error :=
                          '<%TV-RES> Error compon /equipos. '||SUBSTR(SQLERRM, 1, 50);
             END;
         END LOOP;

         IF NOT w_incons THEN

            -- Actaulizar los estados / conceptos de las solicitudes de SERVIP o SERHFC.
             w_estado_id       := 'ORDEN';
             w_concepto        := 'PORDE';
             v_estado_solic    := 'DEFIN';

             -- 2014-06-27 Req 36900 GPON Hogares.
             -- Uso de función fn_tecnologia para buscar la tecnología que se debe asignar a la solicitud.  Antes era REDCO y podría
             -- generar ordenes erróneas para los casos de accesos con tecnología GPON. 
             IF v_solic.tipo_elemento_id = 'SERVIP' THEN
                w_tecnologia      := NVL(PKG_IDENTIFICADORES.fn_tecnologia (v_subped.identificador_id||'-IP'), 'REDCO');                                
             ELSE
                w_tecnologia      := 'HFC';
             END IF;

             b_act_solicitud   := TRUE;
             w_automatica      := TRUE;
             w_encontro_numero := TRUE;
         END IF;
      END IF;
  ELSIF p_cambio_tecno THEN
         
      OPEN  c_subped ( v_solic.subpedido_id);
      FETCH c_subped INTO v_subped;
      CLOSE c_subped;

      IF v_subped.identificador_id IS NOT NULL THEN
         IF v_solic.tipo_elemento = 'SRV' THEN
            v_valor_trab_3830 := PKG_SOLICITUDES.fn_valor_trabajo
                                    (w_pedido,v_solic.subpedido_id,v_solic.solicitud_id,3830);

            v_valor_caract_3830 := PKG_SOLICITUDES.fn_valor_caracteristica
                                    (w_pedido,v_solic.subpedido_id,v_solic.solicitud_id,3830);

            --20090623      Lquintez

            IF fn_devuelve_descripcion_ltcar(v_valor_trab_3830, 3849)=fn_devuelve_descripcion_ltcar(v_valor_caract_3830, 3849)
            THEN
               OPEN  c_cmp_servip(v_solic.subpedido_id);
               FETCH c_cmp_servip INTO v_count_servip;
               IF c_cmp_servip%NOTFOUND THEN
                  v_count_servip := 0;
               END IF;
               CLOSE c_cmp_servip;

               IF v_count_servip > 0 THEN
                  w_estado_id     := 'ORDEN';
                  w_concepto      := 'PSERV';
                  v_estado_solic  := 'DEFIN';
                  w_tecnologia    := 'LOGIC';
                  w_identificador :=  NULL;
               ELSE
                  w_estado_id     := 'ORDEN';
                  w_concepto      := 'PORDE';
                  v_estado_solic  := 'DEFIN';
                  w_tecnologia    := 'LOGIC';
                  w_identificador :=  NULL;
               END IF;
            ELSE
                w_estado_id     := 'ORDEN';
                w_concepto        := 'PSERV';
                v_estado_solic  := 'DEFIN';
                w_tecnologia    := 'LOGIC';
                w_identificador :=  NULL;
            END IF;
         END IF;
      END IF;

      IF NOT w_incons THEN
        b_act_solicitud := TRUE;
      END IF;
  -- 2008-04-23   Aacosta    Consideración de peticiones de solicitud de extensiones sobre los componentes INSIP
  --                         e INSHFC para el producto IPTV.
  ELSIF p_soli_ext AND NOT b_stbox_hdd THEN
     
    w_agrupador       := PKG_IDENTIFICADORES.fn_valor_configuracion(v_solic.identificador_id,90);
    w_tecnologia      := PKG_IDENTIFICADORES.fn_valor_configuracion(v_solic.identificador_id,207);

    -- 2008-12-30  Aacosta   Se incluye el nuevo parámetro Booleano al invocar el procedimiento, con el fin que
    --                       no se validen los equipos de tipo SET UP BOX.

    FOR regext IN c_soli_extensiones LOOP
       PR_ENRUTAR_CMP_TELEV ( regext.pedido_id, regext.subpedido_id
                             ,w_agrupador, w_tecnologia, w_mensaje_error
                             ,FALSE );

       IF w_mensaje_error IS NOT NULL THEN

          w_mensaje_error := NULL;
          w_incons        := TRUE;
       ELSE
          b_prov_stb := TRUE;
       END IF;
    END LOOP;
    
    --2014-01-15 lhernans
    --Lógica para automatización cola DLSAM
    
    --Se recupera municipio de servicio
    v_municipio_servicio := PKG_SOLICITUDES.fn_valor_caracteristica
                              (v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id, 34 );
    
    --Si es nulo, se busca en portafolio
    IF v_municipio_servicio IS NULL THEN
       v_municipio_servicio :=  fn_valor_caract_identif (v_solic.identificador_id, 34);
    END IF;
    
    --Se busca municipio de referencia
    v_munic_salida_int := PKG_PROV_INTER_UTIL.fn_munic_salida (v_municipio_servicio);
    
    --2014-01-15 lhernans
    --Se determina si se inserta o no etiqueta
    
    --2014-10-22   RVELILLR Condicional de tecnologia para consulta de etiquetas para generar eventos.  
    IF NVL(w_tecnologia, 'NOT') = 'REDCO' THEN
        v_aplicacion := 'DSLAM';            
                                            
        --Se recupera valor de la etiqueta
        v_etiqueta := PKG_ETIQUETAS_EVENTOS_BTS.FN_VALOR_PARAM_EVENTOS_BTS (
                                        v_aplicacion,
                                        v_solic.producto_id,
                                        v_solic.tipo_elemento_id,                                                
                                        'CAMBI',
                                        2909
                                        );
    ELSE
        --Se recupera valor de la etiqueta
        --formado por ETBTS_[MUNICIPIO]_[PRODUCTO]_[TECNOLOGIA] (Sin corchetes)
        v_etiqueta := fn_dominio_values_meaning (
                                        'ETBTS'
                                           || '_'
                                           || v_munic_salida_int
                                           || '_'
                                           || v_solic.producto_id
                                           || '_'
                                           || w_tecnologia,
                                        'CAMBI',
                                        2909
                                        );    
    END IF;                        
    --2014-10-22   RVELILLR FIN Lógica.
    
    IF NVL (v_etiqueta, 'NA') <> 'NA' THEN
    
       --Rutina de inserción de etiquetas
       pr_insertar_etiquetas_bts (
                        v_solic.pedido_id,
                        v_solic.subpedido_id,
                        v_solic.solicitud_id,
                        v_solic.servicio_id,
                        v_solic.producto_id,
                        v_solic.tipo_elemento,
                        v_solic.tipo_elemento_id,
                        4805,
                        v_solic.municipio_id,
                        v_solic.empresa_id,
                        v_etiqueta,
                        w_mensaje_error
                        );
    END IF;
    --2014-01-15 lhernans FIN Lógica.
    
    --2010-07-29 Validación para poder colocar en PORDE una solicitud de INSIP
    OPEN c_trabajos(v_solic.pedido_id,v_solic.subpedido_id);
    FETCH c_trabajos INTO v_tipo_trabajo_equred, v_pedido_id,v_subpedido_id, v_solicitud_id;
    IF c_trabajos%NOTFOUND THEN
         NULL;
    END IF;
    CLOSE c_trabajos;

    IF NVL (v_tipo_trabajo_equred, 'N') IN ('NUEVO', 'CAMBI', 'RETIR') THEN
       b_tipo_trabajo_equred := TRUE;
    END IF;

    OPEN c_insip(v_solic.pedido_id,v_solic.subpedido_id);
    FETCH c_insip INTO v_tipo_trabajo_insip;
    IF c_insip%NOTFOUND THEN
        NULL;
    END IF;
    CLOSE c_insip;

    IF NVL (v_tipo_trabajo_insip, 'N') = 'CAMBI' THEN
            b_tipo_trabajo_insip := TRUE;
    END IF;
    -- Fin validación.

    w_estado_id       := 'ORDEN';
    IF b_prov_stb THEN
       --2010-07-29 Solo en el caso que venga un cambio en el INSIP y que venga nuevo EQURED el INSIP se coloca en PORDE.
       IF b_tipo_trabajo_equred AND b_tipo_trabajo_insip THEN
            w_concepto := 'PORDE';
       ELSE
            w_concepto := 'POPTO';

       END IF;
    ELSE
       w_concepto := 'PORDE';

       -- 2010-08-04 Se agrega el siguiente código para poder cumplir las solicitudes de equipos
       IF b_tipo_trabajo_equred AND b_tipo_trabajo_insip THEN

            -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
            bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_solic.pedido_id);
                            
            IF bEsOfertaEspecifica THEN     
               -- Si es superplay debe verificar si tiene subpedidos pendientes                          
               p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_solic.pedido_id, v_solic.subpedido_id);
            ELSE
               p_SubPendi := FALSE; -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
            END IF;      
                    
            IF p_SubPendi and w_concepto NOT IN ('POPTO','PINSC','PECOM','PUMED','OKRED') AND bEsOfertaEspecifica THEN
                w_concepto := 'PEXPQ';
            END IF;-- En los demas casos el concepto queda con el valor que venia antes
            -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
            
            v_nueva_observacion := '<#TV (AA) ['||w_concepto||'] / '||msg_observac||'#> ';
            v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);

            BEGIN
                 UPDATE FNX_SOLICITUDES
                   SET concepto_id    = w_concepto,
                       estado_id      = 'ORDEN',
                       fecha_estado   = SYSDATE,
                       estado_soli    = 'DEFIN',
                       estado_bloqueo = 'N',
                       tecnologia_id  = w_tecnologia,
                       observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                 WHERE pedido_id      = v_pedido_id
                 AND   subpedido_id   = v_subpedido_id
                 AND   solicitud_id   = v_Solicitud_id;
            EXCEPTION
               WHEN OTHERS THEN
                  w_concepto  := 'APROB';

                  UPDATE FNX_SOLICITUDES
                     SET concepto_id    = w_concepto,
                         estado_id      = 'ORDEN',
                         fecha_estado   = SYSDATE,
                         estado_soli    = 'DEFIN',
                         estado_bloqueo = 'N',
                         tecnologia_id  = w_tecnologia,
                         observacion    = v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
                   WHERE pedido_id      = v_pedido_id
                   AND   subpedido_id   = v_subpedido_id
                   AND   solicitud_id   = v_Solicitud_id;

            END;
       END IF;

    END IF;

    v_estado_solic    := 'DEFIN';
    IF NOT w_incons THEN
       b_act_solicitud   := TRUE;
       w_automatica      := TRUE;
       w_encontro_numero := TRUE;
    END IF;
     
  END IF;  -- Fin evaluación trabajos Nuevo, Traslado, Retiro
     
  -- SECCION AA Actualización datos de subpedido y solicitud
  IF w_automatica AND  b_act_solicitud
   
  THEN
    
        BEGIN
           UPDATE fnx_reportes_instalacion
           SET    fecha_est_tecnico = SYSDATE,
                  fecha_estado_carta = SYSDATE,
                  estado_carta    = 'APROB'
           WHERE  pedido_id       = v_solic.pedido_id
           AND    subpedido_id    = v_solic.subpedido_id
           AND    solicitud_id    = v_solic.solicitud_id;
        EXCEPTION
           WHEN OTHERS THEN
                   NULL;
        END;
   END IF;  /* Encontro numero  */

   -- SECCION.  Actualización de características en la solicitud
   -- Actualización de característica 1 en solicitud
   IF (p_nuevo OR p_nuevo_serv )  AND w_encontro_numero AND
      b_act_solicitud
   THEN
     
        BEGIN
           UPDATE FNX_CARACTERISTICA_SOLICITUDES
              SET valor        = w_identificador
            WHERE pedido_id    = v_solic.pedido_id
            AND   subpedido_id = v_solic.subpedido_id
            AND   solicitud_id = v_solic.solicitud_id
            AND   caracteristica_id = 1;
        EXCEPTION
           WHEN OTHERS THEN
              NULL;
        END;

        msg_observac   := msg_observac|| ' - ['||v_tipo_acceso||'-'||v_tramo_red_asignada||']';

        --  Verificar característica de reinstalación del servicio
        OPEN  c_instal (w_pagina);
        FETCH c_instal INTO v_instal;
        IF c_instal%FOUND THEN

           msg_observac := msg_observac||' <* RECONEXION *>';

           BEGIN
              INSERT INTO fnx_caracteristica_solicitudes
                       (pedido_id, subpedido_id, solicitud_id,
                        servicio_id, producto_id,
                        tipo_elemento, tipo_elemento_id,
                        caracteristica_id, valor)
                VALUES (v_solic.pedido_id, v_solic.subpedido_id,
                        v_solic.solicitud_id,
                        v_solic.servicio_id, v_solic.producto_id,
                        v_solic.tipo_elemento, v_solic.tipo_elemento_id,
                        2102, 'S');
           EXCEPTION
              WHEN OTHERS THEN
                 BEGIN
                    UPDATE FNX_CARACTERISTICA_SOLICITUDES
                    SET    valor = 'S'
                    WHERE  pedido_id    = v_solic.pedido_id
                    AND    subpedido_id = v_solic.subpedido_id
                    AND    solicitud_id = v_solic.solicitud_id
                    AND    caracteristica_id = 2102;
                 EXCEPTION
                     WHEN OTHERS THEN
                        NULL;
                 END;
           END;
        END IF;
        CLOSE c_instal;

   END IF;
   -- FIN SECCION.  Actualización de características en la solicitud

   IF p_traslado THEN
      w_identificador := NULL;
   END IF;

  -- 2008-02-14
  -- Sí el concepto de la solicitud es PETEC, se sale del ciclo, para no hacer
  -- las instrucciones de actualización de estado y concepto.
  IF v_solic.tipo_elemento = 'CMP' AND w_concepto = 'PETEC' THEN

     -- 2009-04-07
     -- Se modifica la manera en la que se considera la condiciòn de no actualizaciòn.   Ya no se utiliza la
     -- instrucciòn EXIT para salir del ciclo, pues con el cambio de base de datos, la manera en que
     -- se realiza el ORDER BY principal es diferente.
     b_act_solicitud  := FALSE;

  END IF;



  -- 2009-10-13 Jaristz Se realiza esta validación para el tema de Constructores.
  --2013-06-13: Mmedinac: Línea Modificada Se agrega el concepto PEXPQ para que en caso de que este estado venga en un pedido de constructores lo pase a PLICO
  --IF w_concepto IN ('PORDE','PECOM','PUMED') THEN
  IF w_concepto IN ('PORDE','PECOM','PUMED','PEXPQ') THEN

        OPEN  c_pedido;
        FETCH c_pedido INTO v_constructor;
        CLOSE c_pedido;

        IF NVL(v_constructor,'N') = 'S' THEN


             w_concepto    := 'PLICO';
             w_estado_id   := 'ORDEN';

             /* SE QUITA ESTA VALIDACION PORQUE LAS SOLICITUDES DEBEN ESTAR LIQUIDADAS.
             OPEN  c_repins (v_solic.pedido_id, v_solic.subpedido_id, v_solic.solicitud_id );
             FETCH c_repins INTO w_liquido
                                 ,w_fecha_liquidacion        ;
             CLOSE c_repins;

             IF NVL(w_liquido,'N') = 'N' OR
                     w_fecha_liquidacion IS NULL THEN


                     w_concepto    := 'PLICO';
                     w_estado_id    := 'ORDEN';
             END IF;
             */
        END IF;
  END IF;

-- 2010-09-28 Se actualiza solo cuando se trata de una solicitud que no está en INGRE-PUMED
  IF b_act_solicitud  AND NOT b_ingre_pumed THEN
     

     -- SECCION. Actualización datos de la solicitud
     -- 2010-02-04   Jsierrao
     -- Se declaran las variables para que cuando las observaciones superen los 1000 caractereres, los pedidos
     -- no se queden en petec y se invoca al actualizar las solicitudes de la tv y se incluye en fnx_parametros
     -- modulo ESTUD_TEC (nuevo).

     v_nueva_observacion := '<#TV ['||w_concepto||'] / '||msg_observac||'***#> ';
     v_tamano_nueva_obs   := LENGTH(v_nueva_observacion);
      BEGIN
            UPDATE FNX_SOLICITUDES
               SET concepto_id    = w_concepto,
                   estado_id      = w_estado_id,
                   fecha_estado   = SYSDATE,
                   estado_soli    = v_estado_solic,
                   identificador_id_nuevo = w_identificador,
                   estado_bloqueo = 'N',
                   tecnologia_id  = w_tecnologia,
                   observacion    =  v_nueva_observacion||SUBSTR(observacion, 1, v_tamano_observacion - v_tamano_nueva_obs - 1)
             WHERE pedido_id      = v_solic.pedido_id
             AND   subpedido_id   = v_solic.subpedido_id
             AND   solicitud_id   = v_solic.solicitud_id;

             -- 2006-12-13 / 2006-11-20
             -- Si el concepto de la solicitud es PORDE, se habilita la variable
             -- para generacion de ordenes de paquete
             IF w_concepto = 'PORDE' AND v_paquete_pendiente > 0
             THEN
                b_generar_ordenes  := TRUE;
                v_concepto_paquete := 'PORDE';
             END IF;

             -- 2010-07-21 Si el concepto de la solicitud es PLICO, se habilita la variable
             -- para generacion de ordenes de paquete
             IF w_concepto = 'PLICO' AND v_paquete_pendiente > 0
             THEN
                b_generar_ordenes  := TRUE;
                v_concepto_paquete := 'PLICO';
             END IF;

             b_error_actualizando := FALSE;
        EXCEPTION
            WHEN OTHERS THEN
                 msg_observac := msg_observac
                                  || ' <ERR> '||SUBSTR(SQLERRM, 1, 100);
                 b_error_actualizando := TRUE;

        END;
    END IF;

    -- SECCION. Actualización datos de la solicitud en concepto APROB
    IF b_error_actualizando AND w_concepto = 'PORDE' THEN
         
       BEGIN
          UPDATE FNX_SOLICITUDES
             SET concepto_id  = 'APROB',
                 estado_id    = 'ORDEN',
                 fecha_estado = SYSDATE,
                 estado_soli  = v_estado_solic,
                 identificador_id_nuevo = w_identificador,
                 estado_bloqueo = 'N',
                 observacion   = '<#TV ['||w_concepto||'] / '||msg_observac||'#> '
                                 ||observacion,
                 tecnologia_id = 'HFC'
           WHERE pedido_id = v_solic.pedido_id
             AND subpedido_id = v_solic.subpedido_id
             AND solicitud_id = v_solic.solicitud_id;

             -- 2006-12-13
             -- Si el concepto de la solicitud es PORDE, se habilita la variable
             -- para generacion de ordenes de paquete y el concepto se deja en APROB.
             IF w_concepto = 'PORDE' AND v_paquete_pendiente > 0
             THEN
                b_generar_ordenes  := TRUE;
                v_concepto_paquete := 'APROB';
             END IF;

       EXCEPTION
           WHEN OTHERS THEN
               NULL;
       END;
    END IF;


    -- 2007-02-24
    -- Enrutamiento de componentes de Television asociados al servicio
    IF w_automatica  AND ( p_nuevo OR p_traslado ) AND
       b_act_solicitud AND NOT b_stbox_hdd
    THEN
         
       -- 2008-12-30  Aacosta   Se incluye el nuevo parámetro Booleano al invocar el procedimiento, con el fin que
       --                       no se validen los equipos de tipo SET UP BOX.
       PR_ENRUTAR_CMP_TELEV ( v_solic.pedido_id, v_solic.subpedido_id
                             ,w_agrupador, 'HFC', w_mensaje_error
                             ,TRUE);
                             
        -- 2012-07-26 DGiralL - Se incluye funcionalidad para actualizar la caract. 4094 (Red), 
        -- dependiendo de la direccionalidad del nodo.  Solo aplica para pedidos nuevos o traslados 
        BEGIN
            select decode(b.direccion,'B','BIDIR','UNIDIR')
            into   v_direccion_nodo 
            from   fnx_inf_tv_tramites a 
                  ,fnx_nodos_opticos_electrico b 
            where  a.nodo_optico_electrico_id = b.nodo_optico_electrico_id
            and    a.centro_distribucion_intermedia = b.centro_distribucion_intermedia  
            and    pedido_id    = v_solic.pedido_id
            and    subpedido_id = v_solic.subpedido_id
            and    solicitud_id = v_solic.solicitud_id;
        EXCEPTION WHEN OTHERS THEN 
            v_direccion_nodo := 'UNIDIR';
        END;
                          
        OPEN c_solicitud_telev(v_solic.pedido_id , v_solic.subpedido_id);
        FETCH c_solicitud_telev INTO v_solicitud_telev;
        CLOSE c_solicitud_telev;
        
        IF NVL(v_solicitud_telev.solicitud_id,0) = 0 THEN 
            msg_observac := msg_observac || ' <ERR> SRV-TELEV no identificado para actualizar Red Uni-Bidi';
            DBMS_OUTPUT.PUT_LINE(msg_observac);
        ELSE
            PKG_SOLCAR_UTIL.PR_INSUPD_CARACTERISTICA(v_solicitud_telev.pedido_id
                                                    ,v_solicitud_telev.subpedido_id
                                                    ,v_solicitud_telev.solicitud_id
                                                        ,v_solicitud_telev.servicio_id
                                                        ,v_solicitud_telev.producto_id
                                                         ,v_solicitud_telev.tipo_elemento
                                                          ,v_solicitud_telev.tipo_elemento_id
                                                         ,'4094'
                                                         ,v_solicitud_telev.municipio_id
                                                         ,v_solicitud_telev.empresa_id
                                                          ,v_direccion_nodo
                                                          ,w_mensaje_error  );
        END IF;
        
        IF w_mensaje_error IS NOT NULL THEN
            msg_observac := msg_observac || ' <Error Caract. Red> '||SUBSTR(SQLERRM, 1, 100);
            DBMS_OUTPUT.PUT_LINE('ET TELEV - Error actualizando direccionalidad Nodo '||msg_observac);
        END IF;
        -- 2012-07-26: Fin
        
    END IF;

  FETCH c_solicitudes INTO v_solic;

END LOOP Ciclo_Solicitudes;
CLOSE c_solicitudes;


-- 2006-11-20
-- SECCION. Paso de solicitud del paquete a ORDEN-PORDE luego de pasar
--          a PORDE la solicitud de Television.
IF b_generar_ordenes AND  NOT w_incons  AND
   v_concepto_paquete = 'PORDE' AND
   b_act_solicitud
THEN
  PKG_PROVISION_PAQUETE.pr_genorden_pexpq ( v_solic.pedido_id,v_solic.tipo_elemento_id, p_mensaje);

-- 2010-07-21  Actualizacion de las solicitudes que se ecnuentran en concepto
-- PEXPQ y PXSLN al concepto PLICO.
ELSIF b_generar_ordenes AND
    NOT w_incons        AND
    v_concepto_paquete = 'PLICO'
THEN
    PKG_PROVISION_PAQUETE.pr_genorden_plico ( v_solic.pedido_id,v_solic.tipo_elemento_id, p_mensaje);
ELSIF b_generar_ordenes AND
    NOT w_incons        AND
    v_concepto_paquete = 'APROB'
THEN
  -- 2006-12-13
  -- Logica para paso a ORDEN-APROB de solicitud de paquete, si la television
  -- se quedo en APROB.

  -- Verificacion del tipo de paquete
  v_tipo_paquete := PKG_PROVISION_PAQUETE.fn_valor_caract_paquete (w_pedido, 2878);

  -- 2008-07-17   Aacosta   Modificación de la forma como se valida si el tipo de paquete
  --                        está configurado para que el componente de paquete genere orden.
  --                        VALIDACIÓN ANTERIRO: IF NVL(v_tipo_paquete, 'NULO') IN ('OFERTA','UNETE1','UNETE2','UNETE3') THEN

  IF NVL(PKG_PROVISION_PAQUETE.fn_generar_orden(v_tipo_paquete),'SI') = 'SI' THEN
  
  
           -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
        bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_solic.pedido_id);
                            
         IF bEsOfertaEspecifica THEN     
            -- Si es superplay debe verificar si tiene subpedidos pendientes                          
            p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_solic.pedido_id, v_solic.subpedido_id);
            
            IF p_SubPendi AND bEsOfertaEspecifica THEN
                v_concepto_paquete := 'PXSLN';
            ELSIF NOT p_SubPendi AND bEsOfertaEspecifica THEN
                v_concepto_paquete := 'PORDE';
            END IF;
         END IF;-- En los demas casos el concepto queda con el valor que venia antes
         -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
  
        BEGIN
           UPDATE FNX_SOLICITUDES
           SET    estado_id    = 'ORDEN',
                  concepto_id  = v_concepto_paquete,
                  observacion  = '<#TV ['||v_concepto_paquete||'] #>'||observacion
           WHERE  pedido_id    = w_pedido
           AND    servicio_id  = 'PQUETE'
           AND    producto_id  = 'PQUETE'
           AND    tipo_elemento_id = 'PQUETE'
           AND    estado_id    = 'ORDEN'
           AND    concepto_id  = 'PXSLN';
        EXCEPTION
           WHEN OTHERS THEN
              NULL;
        END;
        
        --2013-01-24: Req20216: Mmedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
        IF v_concepto_paquete = 'PORDE' and bEsOfertaEspecifica THEN        
         pkg_provision_paquete.pr_genorden_pexpq (v_solic.pedido_id, v_solic.tipo_elemento_id, p_mensaje);                            
        END IF;
        --2013-01-24: Req20216: Mmedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico

  END IF;
END IF;

--2009-10-10 SAC Equipos Thomson - Se verifica si se inserta solicitud de EQACCP para traslado
    IF (p_traslado) THEN
      OPEN  c_subped ( v_solic.subpedido_id);
      FETCH c_subped INTO v_subped;
      CLOSE c_subped;
      v_plan_comercial_agr := PKG_IDENTIFICADORES.fn_valor_configuracion(v_subped.identificador_id, 3830);
    END IF;

  --2010-09-29 Se coloca la validación para saber si es un pedido que debe quedar en INGRE-PUMED
  --2009-09-30 SAC Equipos Thomson - Se inserta solicitud de EQACCP para traslado
  IF    not w_incons and p_traslado and NVL(v_plan_comercial_agr,'N') NOT IN ('PAR','HFC') AND NOT b_ingre_pumed
  THEN
    v_solicitud_eqaccp := null;


    PR_SOLICITUD_EQUIPO_CREAR ( v_solic.pedido_id,v_solic.subpedido_id,v_solicitud_eqaccp,v_solic.servicio_id,v_solic.producto_id,
                                'EQACCP','EQU',w_tecnologia,NULL,
                                v_solic.identificador_id,v_solic.municipio_id,v_solic.empresa_id,p_mensaje);

       --

        IF p_mensaje IS NULL THEN
            if v_solicitud_eqaccp is not null then
                  update fnx_solicitudes
                  set estado_id       = 'RUTAS',
                      concepto_id     = 'PRUTA',
                      estado_soli     = 'PENDI'
                   where pedido_id    = v_solic.pedido_id
                    and  subpedido_id = v_solic.subpedido_id
                    and solicitud_id = v_solicitud_eqaccp;
            end if;
        ELSE
             w_resp := SUBSTR (p_mensaje, 1, 50);
             w_incons := FALSE;
        END IF;
  END IF;

/*************************************************************************************************************************************************/
    --2011-03-14. DOJEDAC. Pry-Desaprovisionamiento. Se agrega la validación de que si la transacción es un retiro
    --                     para poder crear la solicitud de equipo.
    DBMS_OUTPUT.PUT_LINE('DESAPROVISIONAMIENTO');
    
    IF NOT w_incons AND p_retiro_acceso THEN
        
        v_si_equipo  := FALSE;
    
        -- Dojedac: Se valida la cantidad de equipos que tiene el servicio.
        open c_cant_equipos (v_solic.identificador_id);
        fetch c_cant_equipos into v_cant_equipos;
        close c_cant_equipos;

        BEGIN
            SELECT IDENTIFICADOR_ID
              INTO v_identificador_ip
              FROM FNX_IDENTIFICADORES
             WHERE AGRUPADOR_ID = v_solic.identificador_id
               AND TIPO_ELEMENTO_ID = 'INSIP';
        EXCEPTION
            WHEN OTHERS THEN
                v_identificador_ip := v_solic.identificador_id;
        END;

        --Dojedac: si la cantidad de equipos relacionados al servicio es mayor a cero (0)
        IF  v_cant_equipos > 0 THEN
            
            
            -- 2011-12-13 Validar para cambios de plan de mixto a basico o mixto a platino que cuando venga esta peticion solo suba los equipos
            --            del componente que se esté retirando, sea INSIP o INSHFC.
            -- 2012-04-10 - JGallg - MejorasDesaprov: Quitar validación de infraestructura compartida de la lógica de los cambios de tecnología.
            --                       Esta se maneja mediante la función interna FNI_GENERAR_SOLIC_EQUIPO que también valida el CPE por aparte.
            OPEN  c_soli_srv (v_solic.pedido_id,v_solic.subpedido_id);
            FETCH c_soli_srv INTO v_soli_srv;
            CLOSE c_soli_srv;

            IF NVL(v_soli_srv, 0) = 1 THEN
                v_cambio_plan := NVL(pkg_solicitudes.fn_tipo_trabajo (v_solic.pedido_id, v_solic.subpedido_id, v_soli_srv, 3830), 'N');

                IF v_cambio_plan <> 'N' THEN
                    b_cambio_plan_telev := TRUE;
                END IF;

                IF b_cambio_plan_telev THEN
                    v_valor_3830 := NVL(pkg_solicitudes.fn_valor_caracteristica (v_solic.pedido_id,v_solic.subpedido_id,v_soli_srv,3830),  'N');

                    IF v_valor_3830 = 'HFC' THEN -- Se retiran equipos REDCO (CPE, STBOX).
                        v_cad_equip_acceso := ' AND A.TIPO_ELEMENTO_ID IN (''STBOX'', ''CPE'') ';
                    ELSIF v_valor_3830 IN ('REDCO','BASIC') THEN  -- Se retiran equipos HFC (DECO).
                        v_cad_equip_acceso := ' AND A.TIPO_ELEMENTO_ID IN (''DECO'') ';
                    END IF;
                END IF;
            END IF;

            v_identifica_acceso := FN_VALOR_CARACT_IDENTIF(v_solic.identificador_id, 90);

            v_cade_Query := 'SELECT A.EQUIPO_ID, A.MARCA_ID, A.REFERENCIA_ID, A.TIPO_ELEMENTO, A.TIPO_ELEMENTO_ID, ' ||
                            '       DECODE(A.TIPO_ELEMENTO_ID, ''CPE'', ''EQACCP'', ''CPEWIM'', ''EQACCP'', ''CABLEM'', ''EQACCP'', ''EQURED'') TIPO_ELEMENTO_ID_EQU, ' ||
                            '       CASE WHEN B.NUM_MTA = 2 AND A.TIPO_ELEMENTO_ID IN (''CABLEM'') THEN ' ||
                            '              1 ' ||
                            '            WHEN B.NUM_MTA = 2 AND A.TIPO_ELEMENTO_ID IN (''ATA'') THEN ' ||
                            '              0 ' ||
                            '            ELSE ' ||
                            '              1 ' ||
                            '       END GENERAR ' ||
                            'FROM FNX_EQUIPOS A, (SELECT MARCA_ID, REFERENCIA_ID, COUNT(TIPO_ELEMENTO_ID) NUM_MTA ' ||
                            '                     FROM FNX_MARCAS_REFERENCIAS ' ||
                            '                     GROUP BY MARCA_ID, REFERENCIA_ID) B ' ||
                            'WHERE A.MARCA_ID = B.MARCA_ID ' ||
                            '  AND A.REFERENCIA_ID = B.REFERENCIA_ID ' ||
                            '  AND A.IDENTIFICADOR_ID IN ('''|| v_solic.identificador_id || ''', ''' || v_identifica_acceso || ''')' ||
                            '  AND A.ESTADO = ''OCU'' ' ||
                            v_cad_equip_acceso;

            v_equipo_obsoleto := 'N';
            v_si_obsoleto := 0;
            v_no_obsoleto := 0;
            
            -- Dojedac: Se ejecuta el cursor de forma dinamica y se generan las solicitudes para
            --         recuperar los equipos asociados al identificador que se va a retirar.
            OPEN c_Equipos_solic FOR v_cade_Query;
            LOOP
                FETCH c_Equipos_solic INTO reg_tipo_equipo;
                EXIT WHEN c_Equipos_solic%NOTFOUND;
                
                v_si_equipo  := TRUE;
                
                --Dojedac. 05/05/2011. Validar si el equipos es o no obsoleto para generar o no orden de carta o de recuperación del equipo.
                v_equipo_obsoleto := fn_consultar_equipo_obsoleto (reg_tipo_equipo.marca_id,         reg_tipo_equipo.referencia_id,
                                                                   reg_tipo_equipo.tipo_elemento_id, reg_tipo_equipo.tipo_elemento);

                IF v_equipo_obsoleto = 'N' THEN
                
                    -- 2014-02-24 Jrendbr Se adiciona logica para que tenga en cuenta los esato de los equipos NIR.
                    v_equipo_nir := fn_consultar_equipo_nir (reg_tipo_equipo.marca_id,         reg_tipo_equipo.referencia_id,
                                                             reg_tipo_equipo.tipo_elemento_id, reg_tipo_equipo.tipo_elemento);
                    
                    IF  v_equipo_nir = 'S' THEN
                        v_si_nir      := v_si_nir + 1;
                        v_equip_obsol := '; Equipo NIR: Marca: ' || reg_tipo_equipo.marca_id || ', Referencia: ' || reg_tipo_equipo.referencia_id || ', Serial: '||reg_tipo_equipo.equipo_id|| '';
                    ELSE
                        v_no_obsoleto := v_no_obsoleto + 1;
                        v_equip_obsol := '; Equipo a recuperar: Marca: ' || reg_tipo_equipo.marca_id || ', Referencia: ' || reg_tipo_equipo.referencia_id || ', Serial: '||reg_tipo_equipo.equipo_id|| '';
                    END IF;
                ELSE
                    
                    v_si_obsoleto := v_si_obsoleto + 1;
                    v_equip_obsol := '; EQUIPO OBSOLETO: Marca: ' || reg_tipo_equipo.marca_id || ', Referencia: ' || reg_tipo_equipo.referencia_id || ', Serial: '||reg_tipo_equipo.equipo_id|| '';
                END IF;


                IF reg_tipo_equipo.GENERAR = 1 THEN
                
                    -- JGallg - 2012-04-10 - MejorasDesaprov: Centralizar validación para generar solicitud en una sola función interna.  En ella
                    --                       se valida: infraestructura compartida para CPE y CABLEM, equipo compartido y propiedad de equipo.
                    b_genera_solic_equipo := fni_generar_solic_equipo (v_solic.identificador_id, w_tecnologia, reg_tipo_equipo, w_pedido );
                    
                    IF b_genera_solic_equipo THEN
                        v_solicitud_eqaccp := NULL;
                        
                        -- 2013-02-19   Yhernana    Se obtiene el ID asociado y se pasa como parametro en el PR_SOLICITUD_RETIR_EQUIPO.
                        v_identific_asoci := NVL(FN_VALOR_CARACT_IDENTIF(v_solic.identificador_id, 90), v_solic.identificador_id);
                        
                        -- Dojedac: Se crean las solicitudes y los trabajos para poder generar las ordenes para recuperar los equipos.
                        PR_SOLICITUD_RETIR_EQUIPO (v_solic.pedido_id,         v_solic.subpedido_id,                 v_solicitud_eqaccp,   v_solic.servicio_id,
                                                   v_solic.producto_id,       reg_tipo_equipo.tipo_elemento_id_equ, 'EQU',                w_tecnologia,
                                                   NULL,                      v_identific_asoci,             v_solic.municipio_id, v_solic.empresa_id,
                                                   reg_tipo_equipo.equipo_id, 'S',                                  p_mensaje);

                        IF p_mensaje IS NULL THEN
                            IF v_solicitud_eqaccp IS NOT NULL THEN
                                
                                -- JGallg - 2012-04-10 - MejorasDesaprov: Llamado a procedimiento interno que hace paso a ORDEN - PORDE
                                pri_paso_orden_porde (v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_eqaccp, v_equip_obsol, p_mensaje);

                                IF p_mensaje IS NOT NULL THEN
                                    w_incons := FALSE;
                                END IF;
                            END IF;
                        ELSE
                            w_incons := FALSE;
                        END IF;
                    ELSE
                       --2013-09-02    aarbob  REQ28278_Tangibilizacion
                         v_propiedadEqu := NVL(FN_VALOR_CAR_EN_CONFIG_EQUIPOS(reg_tipo_equipo.equipo_id,1093,reg_tipo_equipo.marca_id),'X');
                        
                       IF  v_propiedadEqu = 'CM' THEN
                            --se actualiza el estado del decodificador a RES con el fin de que sea a futuro recuperable
                            UPDATE fnx_equipos
                            SET ESTADO = 'RES'
                            WHERE  MARCA_ID = reg_tipo_equipo.MARCA_ID
                            AND REFERENCIA_ID = reg_tipo_equipo.REFERENCIA_ID
                            AND TIPO_ELEMENTO_ID = reg_tipo_equipo.TIPO_ELEMENTO_ID
                            AND TIPO_ELEMENTO = reg_tipo_equipo.TIPO_ELEMENTO
                            AND EQUIPO_ID = reg_tipo_equipo.EQUIPO_ID;
                           
                            ---se actualiza para dejar LOG que informe que no se genera solictud de recogida de equipo ya que el equipo es comprado por el cliente
                            UPDATE FNX_PEDIDOS
                            SET observacion =  SUBSTR ('Desap-NoGeneraSoli RecupEquip' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                            WHERE pedido_id     = w_pedido;
                            
                            
                       END IF; 
                    END IF;
                ELSE
                    
                    -- 2012-08-21    Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que no se debe generar Solicitud Recuperación equipos
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-NoGeneraSoli RecupEquip' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                        WHERE pedido_id = w_pedido;
                    EXCEPTION 
                        WHEN OTHERS THEN
                            NULL;
                    END;
                END IF;
                
                -- 2012-08-21    Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que el equipo es Obsoleto
                IF v_si_obsoleto >= 1 THEN
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-Equi Obsolet' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                        WHERE pedido_id     = w_pedido;
                    EXCEPTION 
                        WHEN OTHERS THEN
                            NULL;
                    END;
                END IF;
        
                -- 2014-02-24 Jrendbr Se adiciona logica para que tenga en cuenta los esato de los equipos NIR.
                IF v_si_nir >= 1 THEN
                    BEGIN
                        UPDATE FNX_PEDIDOS
                        SET observacion =  SUBSTR ('Desap-Equi NIR' ||reg_tipo_equipo.equipo_id||';'|| observacion,1,200)
                        WHERE pedido_id     = w_pedido;
                    EXCEPTION
                        WHEN OTHERS THEN
                            NULL;
                    END;
                END IF;
                
            END LOOP;
            CLOSE c_Equipos_solic;
            
            -- 2014-06-25.  Req 36900. GPON Hogares.  Sección Desaprovisionamiento. 
            -- Consideración de tecnologia GPON para verificación de Infraestructura compartida.

            -- 2013-10-03   Yhernana    Inicio
            --                          REQ_InfrEquiComp: Retiro equipos con Infraestructura compartida
        
            IF w_tecnologia IN ('REDCO', 'GPON') THEN
            
                v_accesp_comparte := fn_comparte_infra_activa (v_solic.identificador_id, w_tecnologia, 'INTER', 'ACCESP', NULL);
            
                v_toip_comparte := fn_comparte_infra_activa (v_solic.identificador_id, w_tecnologia, 'TO', 'TOIP', NULL); 
                
                IF v_accesp_comparte IS NOT NULL AND v_toip_comparte IS NOT NULL THEN
                    b_comparte_dos:= TRUE;
                ELSIF v_accesp_comparte IS NOT NULL AND v_toip_comparte IS NULL THEN
                    b_comparte_accesp:= TRUE;
                ELSIF v_toip_comparte IS NOT NULL AND v_accesp_comparte IS NULL THEN
                    b_comparte_toip:= TRUE;
                ELSIF  v_accesp_comparte IS NULL AND v_toip_comparte IS NULL THEN
                    b_no_comparte_infra:= TRUE;          
                END IF;    
                
                IF b_comparte_dos THEN
                -- 2014-04-11 Jrendbr   Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los 
                --                      que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.
                    SELECT COUNT (trso.pedido_id)
                      INTO v_cant_retir_proc
                      FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                     WHERE trso.pedido_id = soli.pedido_id
                       AND trso.subpedido_id = soli.subpedido_id
                       AND trso.solicitud_id = soli.solicitud_id
                       AND trso.caracteristica_id = 1
                       AND (trso.valor IN (v_accesp_comparte,v_toip_comparte) OR
                            soli.identificador_id IN (v_accesp_comparte,v_toip_comparte)
                            )
                       AND trso.tipo_trabajo = 'RETIR'
                       AND soli.estado_id = 'ORDEN';
                    
                    IF v_cant_retir_proc = 2 THEN
                        v_equipo_retiro:= FN_VERIFICAR_RETIR_EQUIPO('INTER',v_accesp_comparte, w_tecnologia);--> Busco equipo en el ACCESP
                        
                        IF v_equipo_retiro IS NOT NULL THEN
                            b_aplica_retir:= TRUE;
                        END IF;
                    END IF;
                ELSIF b_comparte_accesp THEN
                -- 2014-04-11 Jrendbr   Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los 
                --                      que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.   
                    SELECT COUNT (trso.pedido_id)
                      INTO v_cant_retir_proc
                      FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                     WHERE trso.pedido_id = soli.pedido_id
                       AND trso.subpedido_id = soli.subpedido_id
                       AND trso.solicitud_id = soli.solicitud_id
                       AND trso.caracteristica_id = 1
                       AND (trso.valor = v_accesp_comparte OR
                            soli.identificador_id = v_accesp_comparte
                            )
                       AND trso.tipo_trabajo = 'RETIR'
                       AND soli.estado_id = 'ORDEN';
                    
                    IF v_cant_retir_proc = 1 THEN
                        v_equipo_retiro:= FN_VERIFICAR_RETIR_EQUIPO('INTER',v_accesp_comparte, w_tecnologia);--> Busco equipo en el ACCESP
                    
                        IF v_equipo_retiro IS NOT NULL THEN
                            b_aplica_retir:= TRUE;
                        END IF;
                    END IF;
                
                ELSIF b_comparte_toip THEN
                -- 2014-04-11 Jrendbr   Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los 
                --                      que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.    
                    SELECT COUNT (trso.pedido_id)
                      INTO v_cant_retir_proc
                      FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                     WHERE trso.pedido_id = soli.pedido_id
                       AND trso.subpedido_id = soli.subpedido_id
                       AND trso.solicitud_id = soli.solicitud_id
                       AND trso.caracteristica_id = 1
                       AND (trso.valor = v_toip_comparte OR
                            soli.identificador_id = v_toip_comparte
                            )
                       AND trso.tipo_trabajo = 'RETIR'
                       AND soli.estado_id = 'ORDEN';
                    
                    IF v_cant_retir_proc = 1 THEN
                        v_equipo_retiro:= FN_VERIFICAR_RETIR_EQUIPO('TO',v_toip_comparte, w_tecnologia);--> Busco equipo en el TOIP
                        
                        IF v_equipo_retiro IS NOT NULL THEN
                            b_aplica_retir:= TRUE;
                        END IF;
                    END IF;
                ELSIF b_no_comparte_infra THEN
                    
                    OPEN c_datos_identif2 (v_solic.identificador_id);
                    FETCH c_datos_identif2 INTO rg_datos_identif2;
                    CLOSE c_datos_identif2;
                
                    FOR rg_equipo_comparte IN c_equipo_comparte2 (v_solic.identificador_id, rg_datos_identif2.cliente_id, rg_datos_identif2.pagina_servicio) LOOP
                    -- 2014-04-11 Jrendbr   Se modifican consultas donde validen los pedidos de retiro en proceso de los identificadores con los 
                    --                      que se comparte infraestructura, no se tengan en cuenta las solicitudes cuando se encuentren en estado TECNI.    
                        SELECT COUNT (trso.pedido_id)
                          INTO v_cant_retir_proc
                          FROM fnx_trabajos_solicitudes trso, fnx_solicitudes soli
                         WHERE trso.pedido_id = soli.pedido_id
                           AND trso.subpedido_id = soli.subpedido_id
                           AND trso.solicitud_id = soli.solicitud_id
                           AND trso.caracteristica_id = 1
                           AND (trso.valor = rg_equipo_comparte.identificador_id OR
                                soli.identificador_id = rg_equipo_comparte.identificador_id
                                )
                           AND trso.tipo_trabajo = 'RETIR'
                           AND soli.estado_id = 'ORDEN';
                           
                        IF v_cant_retir_proc = 1 THEN
                            b_aplica_retir:= TRUE;
                        ELSE
                            b_aplica_retir:= FALSE;
                            EXIT;
                        END IF;
                    END LOOP;  
                
                END IF;        
            END IF;
            
            IF b_aplica_retir THEN
                -- 2014-02-24 Jrendbr Se adiciona logica para que tenga en cuenta los esato de los equipos NIR.
                BEGIN
                    SELECT MARCA_ID, REFERENCIA_ID, TIPO_ELEMENTO_ID, TIPO_ELEMENTO, DECODE(TIPO_ELEMENTO_ID, 'CPE', 'EQACCP', 'CPEWIM', 'EQACCP', 'CABLEM', 'EQACCP', 'EQURED')
                      INTO v_marca_id, v_refer_id, v_tipele_id, v_tipo_ele, v_tipele_id_sol
                      FROM FNX_EQUIPOS
                     WHERE EQUIPO_ID = v_equipo_retiro;
                EXCEPTION
                    WHEN OTHERS THEN
                        v_marca_id  := NULL;
                        v_refer_id  := NULL;
                        v_tipo_ele  := NULL;
                        v_tipele_id := NULL;
                        v_tipele_id_sol := NULL;
                END;
                    
                v_equipo_obsoleto := fn_consultar_equipo_obsoleto (v_marca_id, v_refer_id, v_tipele_id, v_tipo_ele);
                        
                IF v_equipo_obsoleto = 'N' THEN
                
                    -- 2014-02-24 Jrendbr Se adiciona logica para que tenga en cuenta los esato de los equipos NIR.
                    v_equipo_nir := fn_consultar_equipo_nir (v_marca_id, v_refer_id, v_tipele_id, v_tipo_ele);
                    
                    IF  v_equipo_nir = 'S' THEN
                        v_equip_obsol := '; Equipo NIR: Marca: ' || v_marca_id || ', Referencia: ' || v_refer_id || ', Serial: '||v_equipo_retiro|| '';
                    ELSE 
                        v_equip_obsol := '; Equipo a recuperar: Marca: ' || v_marca_id || ', Referencia: ' || v_refer_id || ', Serial: '||v_equipo_retiro|| '';
                    END IF;
                ELSE
                    v_equip_obsol := '; EQUIPO OBSOLETO: Marca: ' || v_marca_id || ', Referencia: ' || v_refer_id || ', Serial: '||v_equipo_retiro|| '';
                END IF;
            
                v_solicitud_eqaccp := NULL;
                
                --v_identific_asoci := NVL(FN_VALOR_CARACT_IDENTIF(v_solic.identificador_id, 90), v_solic.identificador_id);
                --2014-04-21    Jrendbr Se adiciona logica para que sea enviado el identifcador con el que comparte infraestructura
                IF v_accesp_comparte IS NOT NULL THEN 
                    identificador_comparte := v_accesp_comparte;
                ELSE    
                    identificador_comparte := v_toip_comparte ;
                END IF;
                        
                -- 2014-02-24 Jrendbr Se adiciona logica para que tenga en cuenta los esato de los equipos NIR.
                IF v_marca_id IS NOT NULL THEN
                    PR_SOLICITUD_RETIR_EQUIPO (v_solic.pedido_id,   v_solic.subpedido_id,       v_solicitud_eqaccp,   v_solic.servicio_id,
                                         v_solic.producto_id,   v_tipele_id_sol,                'EQU',                w_tecnologia,
                                         NULL,                  identificador_comparte,          v_solic.municipio_id, v_solic.empresa_id,
                                         v_equipo_retiro,       'S',                        p_mensaje);
                END IF;
                        
                IF p_mensaje IS NULL THEN
                    IF v_solicitud_eqaccp IS NOT NULL THEN
                        --OJO con el manejo de inconsistencias
                        pri_paso_orden_porde (v_solic.pedido_id, v_solic.subpedido_id, v_solicitud_eqaccp, v_equip_obsol, p_mensaje);
                    END IF;
                            
                    IF p_mensaje IS NOT NULL THEN
                        w_incons := FALSE;
                    END IF;
                ELSE
                    w_incons := FALSE;
                END IF;
            END IF;
            -- 2013-10-03   Yhernana    Fin
        END IF;
        
        IF NOT v_si_equipo THEN
            
            -- 2012-08-21    Yhernana    MejorasDesaprov: Se realiza Update en FNX_PEDIDOS, adicionando en la observación de que no se encontraron equipos.
            BEGIN
                UPDATE FNX_PEDIDOS
                SET observacion =  SUBSTR ('Desap-No se encontraron Equipos;' || observacion,1,200)
                WHERE pedido_id     = w_pedido;
            EXCEPTION 
                WHEN OTHERS THEN
                    NULL;
            END;
        END IF;
        
    END IF;
    
    
    /**** dojedac 16/03/2011 DESAPROVISIONAMIENTO *******************************************/
    
    --yhernana 2012-07-03 
    --MejorasDesaprov: Se cumplen las solicitudes de RETIR de TELEV e INSHFC que se encuentran Suspendidos Por Falta de Pago y
    --                   que no tienen órdenes generadas.
    IF b_retir_hfc THEN
        OPEN  c_soli_inshfc (v_solic.pedido_id,v_solic.subpedido_id);
        FETCH c_soli_inshfc INTO v_soli_inshfc;
        CLOSE c_soli_inshfc;
        
        BEGIN
        pr_cumplir_tv (v_solic.pedido_id,
                       v_solic.subpedido_id,
                       v_soli_inshfc,
                       'DESIN',
                       SYSDATE,
                       v_mensaje
                       );

         UPDATE fnx_solicitudes
            SET observacion = 'RETIRO PSErv OK - ' || v_mensaje
          WHERE pedido_id = v_solic.pedido_id
            AND subpedido_id = v_solic.subpedido_id
            AND solicitud_id = v_soli_inshfc;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Retiros PSERV3 - ' || SQLERRM);
            v_mensaje := v_mensaje || ' - ' || SQLERRM;

            UPDATE fnx_solicitudes
               SET observacion = v_mensaje
             WHERE pedido_id = v_solic.pedido_id
               AND subpedido_id = v_solic.subpedido_id
               AND solicitud_id = v_soli_inshfc;
      END;
        
        
        IF v_mensaje IS NULL THEN
            BEGIN
               UPDATE fnx_solicitudes
                  SET estado_id = 'CUMPL',
                      concepto_id = 'CUMPL',
                   observacion =
                      SUBSTR ('Cumplido RXFP por ESTUTECN ' || observacion,
                              1,
                              2000
                             )
                WHERE pedido_id = v_solic.pedido_id
                  AND subpedido_id = v_solic.subpedido_id
                  AND solicitud_id = v_soli_inshfc;
            EXCEPTION 
                WHEN OTHERS THEN
                    NULL;
            END;
                
            IF (FN_VALOR_CARACT_SUBELEM (v_solic.pedido_id,v_solic.subpedido_id,'TELEV', 85) ='RXFP') THEN
                

                -- Registrar el evento de Cumplido del pedido RXFP en las tablas de la interfase Fenix - Biztalk
                v_motivo_retiro  := PKG_SOLICITUDES.fn_valor_caracteristica(v_solic.pedido_id,v_solic.subpedido_id,v_soli_inshfc, 85);
                v_estado_identif := fn_estado_identificador(FN_BUSCAR_IDENTIFICADOR(v_solic.pedido_id,v_solic.subpedido_id,v_soli_inshfc));
                
                -- Si el estado del identificador se encuentra OCU o XLI, no se registra el evento de cumplido
                -- del pedido RXFP en las tablas de la interfase Fenix - Biztalk
                IF NVL(v_motivo_retiro, 'NOT') = 'RXFP' AND  v_estado_identif NOT IN ('OCU','XLI')THEN
                    
                    PR_BTS_REPORTAR_EVENTO ( v_solic.pedido_id, v_solic.subpedido_id, v_soli_inshfc
                                            ,'TELEV', 'INSHFC' ,'CUMPLIDO', v_mensaje_bts_cyr
                                           );
                    
                END IF;
            END IF;   
        END IF; 
    END IF;

    IF p_mensaje IS NOT NULL THEN
       w_incons        := TRUE;
       w_mensaje_error := p_mensaje;
    END IF;

    IF NOT w_incons THEN
      w_resp := 'S';
    ELSE
      w_resp := SUBSTR (w_mensaje_error, 1, 50);
    END IF;
EXCEPTION
    WHEN OTHERS THEN
         w_resp := 'S';
END PR_ESTUDIO_TECNICO_TELEV; 