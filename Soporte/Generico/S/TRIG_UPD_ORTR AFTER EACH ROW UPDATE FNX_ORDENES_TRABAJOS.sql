DECLARE
  /*
      HISTORIA DE MODIFICACIONES
      Nro.        Requerimiento/Solucion/Oferta
                  u Orden de Cambio                 Fecha       Autor       Descripción
                                                  YYYY-MM-DD  USUARIO
      +--------------------------------------------------------------------------------------------------------------------------+
      1.          Ajustes SSMM-  2627-2-653       2011-01-18  JGLENA        Se modifica reporte de novedades a SSMM para que sólo
                                                                            se reporten los cambios de concepto diferentes a
                                                                            'PPRG', 'PPQUE', 'PROG', 'AGEN','PQUET'

      2.          Ajustes SSMM-  2627-2-653       2011-01-18  JGLENA        Se agrega variable v_concepto_sm para reportar concepto
                                                                            ENRUT a SSMM cuando se cierra un daño por enrutamiento.
                                                                            Se agrega variable v_reportar_sm para controlar
                                                                            si se debe reportar la novedad a soluciones móviles
                                                                            Se agregan instrucciones que asignan valor a
                                                                            v_reportar_sm segun la orden de trabajo sea de
                                                                            instalaciones, reparaciones o corte y reconexion
                                                                            Se condiciona la insercion en fnx_log_soluciones_moviles
                                                                            a la configuración de reporte de novedades
      3.          Ajustes SSMM-  2627-2-653       2011-01-18  JGLENA        Se agregan cursores para consultar configuración
                                                                            de reporte de novedades. Se agregan variables locales
                                                                            para almacenar la configuración e instrucciones de
                                                                            consulta.
      4.          Buzón SSMM-    2627-2-670       2011-02-03  JGLENA        Se elimina clausula rv_meaning = 'PROD' en consultas
                                                                            de colas de soluciones móviles
      5.          AjVarios SSMM- 2627-2-689       2011-03-03  JGLENA        Se agrega constante para concepto anulado y validación
                                                                            que cambia el concepto a ANULA cuando el nuevo estado
                                                                            es ANULA
      6.          AjVarios SSMM- 2627-2-738       2011-04-01  JGLENA        Se modifican las condiciones para el reporte de
                                                                            novedades a soluciones móviles de modo que
                                                                            se reporten los cambios de concepto a REAGE si
                                                                            son realizados por el usuario de SSMM y hay cambio de
                                                                            día
      7.          AjVarios SSMM- 2627-2-738       2011-04-01  JGLENA        Se modifican las condiciones para el reporte de
                                                                            novedades a soluciones móviles de modo que
                                                                            se reporten los reagendamientos si
                                                                            son realizados por el usuario de SSMM, el
                                                                            concepto anterior es REAGE y hay cambio de  día
      8.          AjVarios SSMM- 2627-2-73        2011-04-07  JGLENA        Se modifica conteo de ordenes agendadas para
                                                                            actualizar cant_ortr_ag en fnx_agenda_param_det
      9.          AjVarios SSMM- 2627-2-738        2011-04-15  JGLENA       Se agrega almacenamiento de novedad al agendar o
                                                                            programar. nuevo concepto AGEN, PROG
      10.         AjVarios SSMM- 2627-2-738        2011-04-20  JGLENA       Se modifica búsqueda de unidad de negocio SM
      11.         LTE 4G                           2011-12-05  YSOLARTE     Se modifica para reemplazar el proceso que inserta en el FNX_TAREAS_PLATAFORMAS
                                                                            por el proceso que inserta eventos
      12.         AjVarios SSMM                    2012-02-15  JGLENA       Se agregar condiciones en el trigger para que se produzca la inserción
      13.         LTE 4G                           2012-05-24  YSOLARTE     Se reemplaza la función fn_dominio_descripcion por la función fn_valor_adi_parametros_ord de órdenes
                                                                            de un registro en la tabla fnx_log_soluciones_moviles se desagende una orden
      13.         LTE 4G                           2012-05-24  YSOLARTE     Se reemplaza la función fn_dominio_descripcion por la función fn_valor_adi_parametros_ord de órdenes
      14.         Siebel Daños                     2012-10-01  ETachej      Se implementan cambios para Siebel daños. se invoca PKG_SIEBEL_FENIX.PR_CREAR_NOVEDAD_SBL
      15.         GPON                             2013-04-25  YSOLARTE     Adición de parametro aplicación para insertar en la tabla bts_eventos en el campo aplicacion_id en el llamado al paquete PKG_INTEGRA_FENIX_BTS
      16.         Mejora Comandos                  2013-05-03  ABETANB      Adición de verificación del nodo para hacer la inserción en comandos.

      17          Gestion Daños EndToEnd           2013-07-05  Jrincong     Req 25261  Cuando cambia alguno de los datos de Area Operativa / Equipo
                                                                            / Fecha PreAgenda / Hora PreAgenda o Concepto se actualiza el campo fecha_actualizacion en
                                                                            la tabla FNX_REPORTES_MANTENIMIENTO con el valor de la fecha de sistema.
      18.          REQ21213_ServFijoLTECC         2013-07-31   Jtabarc     REQ21213_ServFijoLTECC - Se incluye una validación para obtener la variable v_codigo_evento, para los cambios de equipo
                                                                           que se generen a través de un pedido de daño, es decir, a través de la pantalla de Fénix-Oracle;
                                                                           para que posteriormente se puedan generar los eventos respectivos.
      19.         REQ22579_GPON                   2013-08-23   JGLENA      Se realiza validacion para obtener v_codigo_evento en el cambio de equipo por mantenimiento para GPON
                                                                           sin sobreescribir valor hallado por LTE
      20.         REQ_SSMMSiebDañoLote            2013-10-07   Yhernana     Solo se insertará en FNX_LOG_SOLUCIONES_MOVILES si el lote es NOT NULL.
      21.         REQ34654_InstTecSMP             2014-02-05   ABETANB     Se adiciona la validacion del calculo del evento de los retiros para SMARTPLAY
      22.         REQ40792 AgendamientoNuevoE2E   2014-13-01   Ctamayol    Se adicionan los siguientes 4 campos: nombre_ferry, fecha_preagenda, hora_preagenda y tipo_capacidad
                                                                           en el llamdo del PR_CREAR_RASTRO para el Req 40792 AgendamientoNuevoE2E
      23.         REQ36900_GPONHoga               2014-06-10   JGLENA      Se adicionan variables y logica para pasar datos de la orden a pkg_tareas_plataforma
      24.         REQ_53058_MejIDAsocSMP          2014-07-24   JGIRALJ     Se adiciona condicion para que genere evento de la DESPA Para SEREQU con nueva tx de CAMBI
      +--------------------------------------------------------------------------------------------------------------------------+
  */

  v_usuario             fnx_rastros_trabajos.usuario%TYPE;
  v_estado_sol          fnx_solicitudes.estado_id%TYPE;
  v_estado_soli         fnx_solicitudes.estado_soli%TYPE;
  v_mensaje             VARCHAR2(2000);
  v_producto            fnx_solicitudes.producto_id%TYPE;
  v_empresa             fnx_servicios_productos.empresa_id%TYPE;
  v_identificador       fnx_solicitudes.identificador_id%TYPE;
  v_identificador_nuevo fnx_solicitudes.identificador_id%TYPE;
  v_cambio_prd          VARCHAR2(2) := 'NO';
  v_agrupador           fnx_subpedidos.agrupador%TYPE;
  v_ordenes             NUMBER(2, 0);
  v_subpedido           fnx_solicitudes.subpedido_id%TYPE;
  v_orden_ultma         VARCHAR(3);
  v_validar             VARCHAR(3) := 'N';
  v_concepto            fnx_solicitudes.concepto_id%TYPE;
  v_plataforma_cola     fnx_colas_trabajos.categoria2%TYPE;
  --  v_X VARCHAR2(5);
  v_procesar_cola    fnx_colas_trabajos.categoria%TYPE;
  v_parametro_xml    fnx_tareas_plataformas.xml_fenix%TYPE;
  v_tipo_trabajo     fnx_ordenes_trabajos.tipo_trabajo%TYPE;
  v_cantidad_ortr_ag fnx_agenda_param_det.cantidad_ortr_ag%TYPE;
  v_rowid            ROWID;
  v_unidad           fnx_log_soluciones_moviles.unidad%TYPE;
  v_maquina          fnx_log_soluciones_moviles.maquina%TYPE;
  v_cantidadunidad   NUMBER(3);

  /* 2011-01-18  JGLENA Ajustes SSMM-  2627-2-653        Se agrega variable v_concepto_sm para reportar concepto
                                                         ENRUT a SSMM cuando se cierra un daño por enrutamiento.
                                                         Se agrega variable v_reportar_sm para controlar
                                                         si se debe reportar la novedad a soluciones móviles
  */
  v_concepto_sm    fnx_ordenes_trabajos.concepto_id%TYPE; /* Concepto a informar en soluciones móviles en enrutamiento de daños */
  v_reportar_sm    cg_ref_codes.rv_high_value%TYPE := 'N'; /* Configuracion de reporte de novedades a soluciones móviles a usar según */
  v_reportar_insta cg_ref_codes.rv_high_value%TYPE; /* Variable que almacena si se deben reportar a SM las novedades en instalaciones */
  v_reportar_mante cg_ref_codes.rv_high_value%TYPE; /* Variable que almacena si se deben reportar a SM las novedades en reparaciones */
  v_reportar_cyr   cg_ref_codes.rv_high_value%TYPE; /* Variable que almacena si se deben reportar a SM las novedades en corte y reconexion */
  ct_concepto_anula_sm CONSTANT fnx_ordenes_trabajos.concepto_id%TYPE := 'ANULA'; /* Concepto a informar en soluciones móviles en enrutamiento de daños */
  ct_concepto_desagendar_sm CONSTANT fnx_ordenes_trabajos.concepto_id%TYPE := 'DESAG'; /* Concepto a informar en soluciones móviles al desagendar */

  /* 2011-04-15 JGLENA Ajustes Varios SSMM 2627-2-738    Variables para almacenar novedad de fecha de agendamiento o programación */
  v_detalle fnx_novedades_solicitudes.detalle%TYPE; /* Variable para almacenar información del detalle */

  /* Declaración de constantes */
  ct_novedad_fecha_agen fnx_novedades_solicitudes.novedad_id%TYPE := 63; /* Novedad de actualización de fecha de agendamiento o programación */
  /*2011-12-05 YSOLARTE LTE 4G Varible para consultar el tipo de tareas*/
  v_tipo_tarea    fnx_productos.tipo_tarea%TYPE;
  --
  -- 2014-08-06   jrojasri    -- Req ITPAM41860_INTRAWAY
                              -- SE CORRIJE EL TIPO DE DATO DE LA VARIABLE APLICACION DE VARCHAR 10 AAL TIPO FNX_PARAMETROS_ORD.VALOR_ADICIONAL%TYPE
  --
  --v_aplicacion             VARCHAR2 (10);
  v_aplicacion   FNX_PARAMETROS_ORD.VALOR_ADICIONAL%TYPE;
  v_codigo_evento fnx_caracteristica_solicitudes.valor%TYPE;

  v_error_bts NUMBER;
  v_mensaje_bts VARCHAR2(500);


  v_concepto_sol         fnx_solicitudes.concepto_id%TYPE;  --CTAMAYOL 27MAY2013 Req 20216 Superplay

  --
  --AACOSTA MVM Ing Sotware Marzo 07 de 2006: Se agregan al select de la tabla solicitudes el campo estado_soli
  --con el fin de saber cuando se esta anulando un pedido para el produicto ASP.
  CURSOR c_sol IS
    SELECT estado_id,
           estado_soli,
           producto_id,
           identificador_id,
           identificador_id_nuevo,
           concepto_id --CTAMAYOL 27MAY2013 Req 20216 Superplay
      FROM fnx_solicitudes
     WHERE pedido_id = :NEW.pedido_id
       AND subpedido_id = :NEW.subpedido_id
       AND solicitud_id = :NEW.solicitud_id;
  /* 2011-04-07 JGLENA Se modifica l+ogica del cursor para corregir los conteos de ordenes agendadas */
  /*CURSOR c_agpa IS
  SELECT cantidad_ortr_ag, ROWID
    FROM fnx_agenda_param_det
   WHERE cola_id = :OLD.cola_id
     AND equipo_id = nvl(:NEW.equipo_id, :OLD.equipo_id)
     AND fecha_entrega =
         nvl(:NEW.fecha_entrega, :OLD.fecha_entrega)
     AND to_char(hora_entrega, 'HH24:MI') =
         nvl(:NEW.hora_entrega, :OLD.hora_entrega)
     AND area_operativa_id = :OLD.area_operativa_id
     AND nvl(subzona_id, :OLD.subzona_id) = :OLD.subzona_id
     AND nvl(area_trabajo_id, :OLD.area_trabajo_id) =
         :OLD.area_trabajo_id;*/

  CURSOR c_agpa(p_cola fnx_ordenes_trabajos.cola_id%TYPE, p_area_operativa fnx_ordenes_trabajos.area_operativa_id%TYPE, p_subzona fnx_ordenes_trabajos.subzona_id%TYPE, p_area_trabajo fnx_ordenes_trabajos.area_trabajo_id%TYPE, p_equipo fnx_ordenes_trabajos.equipo_id%TYPE, p_fecha_entrega fnx_ordenes_trabajos.fecha_entrega%TYPE, p_hora_entrega fnx_ordenes_trabajos.hora_entrega%TYPE, p_estado fnx_agenda_param_det.estado%TYPE) IS
    SELECT cantidad_ortr_ag, ROWID row_id
      FROM fnx_agenda_param_det
     WHERE cola_id = p_cola
       AND area_operativa_id = p_area_operativa
       AND equipo_id = p_equipo
       AND (estado = p_estado OR p_estado IS NULL)
       AND fecha_entrega = p_fecha_entrega
       AND to_char(hora_entrega, 'HH24:MI') = p_hora_entrega
       AND (subzona_id = p_subzona OR subzona_id IS NULL OR
           p_subzona IS NULL)
       AND (area_trabajo_id = p_area_trabajo OR
           area_trabajo_id IS NULL OR p_area_trabajo IS NULL);

  /* 2011-01-18 Ajustes SM 2627-2-653 JGLENA Se agrega cursor privado c_cref02 */
  /* Cursor c_cref02
        Proposito: Traer configuración para reporte de novedades a soluciones móviles
                   en ordenes de instalaciones
  */
  CURSOR c_cref02 IS
    SELECT upper(rv_high_value)
      FROM cg_ref_codes c
     WHERE c.rv_domain = 'ORDENES'
       AND c.rv_low_value = 'REPORTAR_SM_INSTA';

  /* 2011-01-18 Ajustes SM 2627-2-653 JGLENA Se agrega cursor privado c_cref03 */
  /* Cursor c_cref03
        Proposito: Traer configuración para reporte de novedades a soluciones móviles
                   en ordenes de reparaciones
  */
  CURSOR c_cref03 IS
    SELECT upper(rv_high_value)
      FROM cg_ref_codes c
     WHERE c.rv_domain = 'ORDENES'
       AND c.rv_low_value = 'REPORTAR_SM_MANTE';

  /* 2011-01-18 Ajustes SM 2627-2-653 JGLENA Se agrega cursor privado c_cref04 */
  /* Cursor c_cref04
        Proposito: Traer configuración para reporte de novedades a soluciones móviles
                   en ordenes de corte y reconexion
  */
  CURSOR c_cref04 IS
    SELECT upper(rv_high_value)
      FROM cg_ref_codes c
     WHERE c.rv_domain = 'ORDENES'
       AND c.rv_low_value = 'REPORTAR_SM_CYR';

  /* 2011-12-05 LTE YSOLARTE Se agrega cursor para consultar el tipo de tarea de FNX_PRODUCTOS */
  /* Cursor c_tipotarea
        Proposito: Traer configuración de FNX_PRODUCTOS para determinar si inserta tareas o eventos
  */
  CURSOR c_tipotarea IS
    SELECT tipo_tarea
      FROM fnx_productos
     WHERE producto_id = v_producto;
 /* REQ36900_GPONHoga               2014-06-10   JGLENA      Se adiciona variable para pasar datos de la orden a pkg_tareas_plataforma  */

  v_orden_trab_rec PKG_TAREAS_PLATAFORMAS.ty_orden_trabajo_rec;

BEGIN
  --[Mayo-18-2010] Actualiza fecha compromiso
  /* IF :OLD.fecha_entrega <> :NEW.fecha_entrega THEN
  BEGIN
     UPDATE fnx_requerimientos_trabajos
      SET  fecha_compromiso=:NEW.fecha_entrega
      WHERE requerimiento_id = :NEW.requerimiento_id;
    EXCEPTION WHEN OTHERS THEN
       DBMS_OUTPUT.PUT_LINE('ERROR  actualizando fecha_compromiso fnx_requerimientos_trabajos' );
   END;
   END IF;*/

  /*  IF :OLD.hora_entrega <> :NEW.hora_entrega THEN
  BEGIN
     UPDATE fnx_requerimientos_trabajos
      SET  hora_compromiso=:NEW.hora_entrega
      WHERE requerimiento_id = :NEW.requerimiento_id;
    EXCEPTION WHEN OTHERS THEN
       DBMS_OUTPUT.PUT_LINE('ERROR  actualizando hora_compromiso fnx_requerimientos_trabajos' );
   END;
   END IF;*/

  ---JUNIO 28 DE 2010 Clamadrid. Cuando halla cambio de cola, fehca y concepto conusuario diferente  a SMOVIL
  --se debe ingresar en la tabla log_soluciones_móviles
  -----------------------------------------------------------------------------------
  /* 2011-04-15 JGLENA Se almacena novedad de fecha de agendamiento o programación */
  IF :OLD.estado_id = 'PENDI'
     AND :OLD.concepto_id <> :NEW.concepto_id
     AND (:NEW.concepto_id = 'AGEN' OR
     (:NEW.concepto_id = 'PROG' AND :NEW.etapa_id IN ('INSTA', 'MANTE'))) THEN
    IF :NEW.fecha_entrega IS NOT NULL THEN
      v_detalle := to_char(:NEW.fecha_entrega, 'DD/MM/YYYY');
    END IF;
    IF :NEW.fecha_entrega IS NOT NULL
       AND :NEW.hora_entrega IS NOT NULL THEN
      v_detalle := v_detalle || ' ' || :NEW.hora_entrega;
    END IF;
    pr_crear_novedad_solicitud(p_novedad      => ct_novedad_fecha_agen,
                               p_detalle      => v_detalle,
                               p_pedido       => :OLD.pedido_id,
                               p_subpedido    => :OLD.subpedido_id,
                               p_solicitud    => :OLD.solicitud_id,
                               p_concepto_ant => :OLD.concepto_id,
                               p_concepto_act => :NEW.concepto_id,
                               p_usuario      => fn_usuario);
  END IF;

  /* Comportamiento normal del trigger */
  --2012-02-04 Etachej - Siebe Daños : Reporte de novedades
  IF ((:OLD.cola_id <> :NEW.cola_id OR
     :OLD.concepto_id <> :NEW.concepto_id) AND
     fn_usuario <> 'FNX_SSMM' AND :OLD.lote IS NOT NULL)

    /* 2011-04-06 Ajustes Varios SM JGLENA Se condiciona para que reporte como novedad los pasos a AGEN cuando el
                                                                                                                                                                                                                                                                   reagendamiento se realiza desde Soluciones Móviles y existe cambio
                                                                                                                                                                                                                                                                   de día
                                                                                                                                                                                                                             */
     OR (:OLD.concepto_id <> :NEW.concepto_id AND
     :NEW.concepto_id IN ('AGEN', 'PQUET') AND
     :OLD.concepto_id = 'REAGE' AND fn_usuario = 'FNX_SSMM' AND
     :OLD.lote IS NOT NULL AND
     trunc(:NEW.fecha_entrega) <> trunc(:OLD.fecha_entrega) AND
     :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NOT NULL)

    /* GVERBELC 2012-02-15 Se inserta un registro en la tabla fnx_log_soluciones_moviles
                                                                                    cuando para que se reporte a Soluciones Móviles cuando se desagende una orden de trabajo
                                   INICIO*/

     OR (:OLD.concepto_id IN ('PROG', 'AGEN') AND :NEW.concepto_id = 'PPRG' AND
     :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NULL AND
     :OLD.lote IS NOT NULL AND fn_usuario <> 'FNX_SSMM')
     OR (:OLD.estado_id = 'PENDI' AND :NEW.estado_id = 'PENDI' AND
     :OLD.concepto_id = 'PQUET' AND :NEW.concepto_id = 'PQUET' AND
     :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NULL AND
     :OLD.etapa_id = 'INSTA' AND :OLD.actividad_id <> 'INSPA' AND
     :OLD.referencia_id = 'S' AND fn_usuario <> 'FNX_SSMM' AND
     :OLD.lote IS NOT NULL)
    /*FIN GVERBELC 2012-02-15*/
    /*2012-10-01 ETachej Siebel daños*/
    OR ((:OLD.cola_id <> :NEW.cola_id OR
        :OLD.concepto_id <> :NEW.concepto_id OR
        :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NULL OR
        :OLD.fecha_entrega IS NULL AND :NEW.fecha_entrega IS NOT NULL OR
        :OLD.hora_entrega <> :NEW.hora_entrega ) AND
        fn_usuario <> 'FNX_SIEBELD'  AND
        :OLD.ETAPA_ID='MANTE')
   THEN

    /* 2011-01-18 Ajustes SSMM-  2627-2-653        Se modifica reporte de novedades a SSMM para que sólo
                                                   se reporten los cambios de concepto diferentes a
                                                   'PPRG', 'PPQUE', 'PROG', 'AGEN','PQUET'
                                                   Se asigna valor a variable v_concepto_sm para reportar
                                                   ENRUT cuando un daño se cierra por enrutamiento
                                                   El enrutamiento se marca como 'E' en el campo referencia_id
                                                   de fnx_ordenes_trabajos
    */
    IF :NEW.concepto_id NOT IN
       ('PPRG', 'PPQUE', 'PROG', 'AGEN', 'PQUET')
      /*2011-04-06 JGLENA Se agrega condicion para reagendamiento desde SM*/
       OR (:OLD.concepto_id <> :NEW.concepto_id AND
       :NEW.concepto_id IN ('AGEN', 'PQUET') AND
       :OLD.concepto_id = 'REAGE' AND fn_usuario ='FNX_SSMM' AND
       :OLD.lote IS NOT NULL AND
       trunc(:NEW.fecha_entrega) <> trunc(:OLD.fecha_entrega) AND
       :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NOT NULL)
      /* JGLENA 2012-02-15 Se inserta un registro en la tabla fnx_log_soluciones_moviles
                                                                                                  cuando para que se reporte a Soluciones Móviles cuando se desagende una orden de trabajo
                                                 INICIO*/
       OR (:OLD.concepto_id IN ('PROG', 'AGEN') AND :NEW.concepto_id = 'PPRG' AND
       :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NULL AND
       :OLD.lote IS NOT NULL AND fn_usuario <> 'FNX_SSMM')
       OR (:OLD.estado_id = 'PENDI' AND :NEW.estado_id = 'PENDI' AND
       :OLD.concepto_id = 'PQUET' AND :NEW.concepto_id = 'PQUET' AND
       :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NULL AND
       :OLD.etapa_id = 'INSTA' AND :OLD.actividad_id <> 'INSPA' AND
       :OLD.referencia_id = 'S' AND fn_usuario <> 'FNX_SSMM' AND
       :OLD.lote IS NOT NULL)
    /* FIN JGLENA 2012-02-15 */
     THEN

      v_concepto_sm := :NEW.concepto_id;
      /* Se reporta el concepto REAGE en lugar de AGEN para que la sincronización quede OK */
      IF (:OLD.concepto_id <> :NEW.concepto_id AND
         :NEW.concepto_id IN ('AGEN', 'PQUET') AND
         :OLD.concepto_id = 'REAGE' AND fn_usuario = 'FNX_SSMM' AND
         :OLD.lote IS NOT NULL AND
         trunc(:NEW.fecha_entrega) <> trunc(:OLD.fecha_entrega) AND
         :OLD.fecha_entrega IS NOT NULL AND
         :NEW.fecha_entrega IS NOT NULL) THEN
        v_concepto_sm := :OLD.concepto_id;
        /* Se reporta el concepto PPRG */
      END IF;

      /* Se reporta el concepto DESAG en lugar de PPRG o PQUET para que la sincronización quede OK */
      IF (:OLD.concepto_id IN ('PROG', 'AGEN') AND :NEW.concepto_id = 'PPRG' AND
         :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NULL AND
         :OLD.lote IS NOT NULL AND fn_usuario <> 'FNX_SSMM')
         OR (:OLD.estado_id = 'PENDI' AND :NEW.estado_id = 'PENDI' AND
         :OLD.concepto_id = 'PQUET' AND :NEW.concepto_id = 'PQUET' AND
         :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NULL AND
         :OLD.etapa_id = 'INSTA' AND :OLD.actividad_id <> 'INSPA' AND
         :OLD.referencia_id = 'S' AND fn_usuario <> 'FNX_SSMM' AND
         :OLD.lote IS NOT NULL) THEN
        v_concepto_sm := ct_concepto_desagendar_sm;
      END IF;

      v_cantidadunidad := 0;
      v_usuario        := fn_usuario;
      v_unidad         := '';

      SELECT sys_context('USERENV', 'TERMINAL')
        INTO v_maquina
        FROM dual;

      /* 2011-03-03  JGLENA Ajustes SSMM-  2627-2-689  Se modifica concepto a reportar_a sm  si el nuevo estado es anulado */
      IF :NEW.estado_id = ct_concepto_anula_sm THEN
        v_concepto_sm := ct_concepto_anula_sm;
      END IF;

      IF :OLD.etapa_id = 'MANTE' THEN
        v_unidad := 'M';

        /* 2011-01-18 Ajustes SM 2627-2-653 JGLENA Se lee configuración para reporte de novedades en ordenes
                                                   de reparaciones
        */
        OPEN c_cref03;
        FETCH c_cref03
          INTO v_reportar_mante;
        CLOSE c_cref03;
        v_reportar_mante := nvl(v_reportar_mante, 'N');

        /* 2011-01-18  JGLENA Ajustes SSMM-  2627-2-653  Se asigna valor a v_reportar_sm según configuracion de reparaciones */
        v_reportar_sm := v_reportar_mante;
        IF :NEW.concepto_id = 'CUMPL'
           AND :NEW.referencia_id IS NOT NULL
           AND :NEW.referencia_id = 'E' THEN
          v_concepto_sm := 'ENRUT';
        END IF;
      ELSE
        SELECT COUNT(1)
          INTO v_cantidadunidad
          FROM cg_ref_codes
         WHERE rv_domain IN ('ETCOACINSTA')
           AND rv_low_value = :NEW.cola_id
           AND rv_high_value = :NEW.actividad_id;
        /*2627-2-670       2011-02-03  JGLENA        Se elimina clausula rv_meaning = 'PROD' en consultas
                                                     de colas de soluciones móviles
        AND rv_meaning = 'PROD';*/

        IF v_cantidadunidad > 0 THEN
          --Unidad de negocio de Instalaciones
          v_unidad := 'I';
          /* 2011-01-18 Ajustes SM 2627-2-653 JGLENA Se lee configuración para reporte de novedades en ordenes
                                                     de instalacion
          */
          OPEN c_cref02;
          FETCH c_cref02
            INTO v_reportar_insta;
          CLOSE c_cref02;
          v_reportar_insta := nvl(v_reportar_insta, 'N');

          /* 2011-01-18  JGLENA Ajustes SSMM-  2627-2-653  Se asigna valor a v_reportar_sm según configuracion de instalaciones */
          v_reportar_sm := v_reportar_insta;

        ELSE
          SELECT COUNT(1)
            INTO v_cantidadunidad
            FROM cg_ref_codes
           WHERE rv_domain IN ('ETCOACCYR')
             AND rv_low_value = :NEW.cola_id
             AND rv_high_value = :NEW.actividad_id;
          /*2627-2-670       2011-02-03  JGLENA        Se elimina clausula rv_meaning = 'PROD' en consultas
                                                       de colas de soluciones móviles
          AND rv_meaning = 'PROD';*/

          IF v_cantidadunidad > 0 THEN
            v_unidad := 'C';
            /* 2011-01-18 Ajustes SM 2627-2-653 JGLENA Se lee configuración para reporte de novedades en ordenes
                                                       de corte y reconexion
            */
            OPEN c_cref04;
            FETCH c_cref04
              INTO v_reportar_cyr;
            CLOSE c_cref04;
            v_reportar_cyr := nvl(v_reportar_cyr, 'N');

            /* 2011-01-18  JGLENA Ajustes SSMM-  2627-2-653  Se asigna valor a v_reportar_sm según configuracion de corte y reconexion */
            v_reportar_sm := v_reportar_cyr;
          END IF;
        END IF;
      END IF;
      /* 2011-01-18 Ajustes SSMM-  2627-2-653      Se sustituye :new.concepto_id por la variable v_concepto_sm para reportar
                                                   ENRUT cuando un daño se cierra por enrutamiento
                                                   El enrutamiento se marca como 'E' en el campo referencia_id
                                                   de fnx_ordenes_trabajos
                                                   Se condiciona la insercion en fnx_log_soluciones_moviles
                                                   a la configuración de reporte de novedades
      */
      --REQ_SSMMSiebDañoLote-   2013-10-07   Yhernana   Solo se insertará en FNX_LOG_SOLUCIONES_MOVILES si el lote es NOT NULL.
      IF v_reportar_sm = 'S' AND :OLD.lote IS NOT NULL THEN
        BEGIN

          INSERT INTO fnx_log_soluciones_moviles
            (proceso,
             pedido_id,
             subpedido_id,
             solicitud_id,
             cola_id,
             actividad_id,
             usuario,
             maquina,
             unidad,
             fecha,
             log_id,
             concepto_id)
          VALUES
            ('novedades_soluciones_moviles',
             :OLD.pedido_id,
             :OLD.subpedido_id,
             :OLD.solicitud_id,
             :OLD.cola_id,
             :OLD.actividad_id,
             v_usuario,
             v_maquina,
             v_unidad,
             SYSDATE,
             log_sm.NEXTVAL,
             v_concepto_sm --:NEW.concepto_id
             );
        EXCEPTION
          WHEN OTHERS THEN
            dbms_output.put_line('no se inserto log fnx_log_soluciones_moviles ');
        END;
      END IF;
    END IF; /* Fin If :NEW.concepto_id not in... 2011-01-18  JGLENA */

    DBMS_OUTPUT.PUT_LINE('DATOS 4: '||:OLD.cola_id||'-'||:OLD.fecha_entrega||'-'||:OLD.hora_entrega||'-'||:NEW.fecha_entrega||'-'||:NEW.hora_entrega);
      /*2012-10-01 ETachej Siebel daños -  Se inserta novedad en fnx_log_siebel*/
      IF ((:OLD.cola_id <> :NEW.cola_id OR
        :OLD.concepto_id <> :NEW.concepto_id OR
        :OLD.fecha_entrega IS NOT NULL AND :NEW.fecha_entrega IS NULL OR
        :OLD.fecha_entrega IS NULL AND :NEW.fecha_entrega IS NOT NULL OR
        :OLD.hora_entrega <> :NEW.hora_entrega ) AND
        fn_usuario <> 'FNX_SIEBELD' AND
        :OLD.ETAPA_ID='MANTE') THEN

            DBMS_OUTPUT.PUT_LINE('DATOS 5: '||:OLD.cola_id||'-'||:OLD.fecha_entrega||'-'||:OLD.hora_entrega||'-'||:NEW.fecha_entrega||'-'||:NEW.hora_entrega);

            pkg_siebel_fenix.pr_crear_novedad_sbl(:OLD.pedido_id,:OLD.subpedido_id, :OLD.solicitud_id,
                                                  :OLD.identificador_id, :NEW.actividad_id, :NEW.cola_id,
                                                  :NEW.estado_id, :NEW.concepto_id, :OLD.concepto_id,
                                                  :NEW.referencia_id,:NEW.fecha_entrega,
                                                  :NEW.hora_entrega, :OLD.fecha_entrega, :OLD.hora_entrega,
                                                  :OLD.requerimiento_id,v_mensaje);
      END IF;

  END IF;

  --FIN JUNIO 28 DE 2010 Clamadrid.
  -----------------------------------------------------------------------------------

  -- Cuando se actualice una orden de trabajo se crea un registro en fnx_rastros_trabajos
  -- con el fin de llevar una bitacora de la orden
  IF :OLD.etapa_id <> :NEW.etapa_id
     OR :OLD.actividad_id <> :NEW.actividad_id
     OR :OLD.secuencia <> :NEW.secuencia
     OR :OLD.estado_id <> :NEW.estado_id
     OR :OLD.concepto_id <> :NEW.concepto_id THEN
    v_usuario := fn_usuario;
    dbms_output.put_line('TRIG_UPD_ORTR PR_CREAR_RASTRO ');
    dbms_output.put_line('estado_id ' || :NEW.estado_id);
    dbms_output.put_line('concepto_id ' || :NEW.concepto_id);

    IF :NEW.Concepto_id = 'PAGEN' THEN
    pr_crear_rastro(:OLD.requerimiento_id,
                    :NEW.etapa_id,
                    :NEW.actividad_id,
                    :NEW.secuencia,
                    :NEW.fecha_recibo,
                    :NEW.estado_id,
                    :NEW.concepto_id,
                    SYSDATE,
                    :NEW.cola_id,
                    :NEW.equipo_id,
                    v_usuario,
                    :NEW.nombre_ferry,
                    :NEW.fecha_preagenda,
                    :NEW.hora_preagenda,
                    :NEW.tipo_capacidad
     );
     ELSE

     pr_crear_rastro(:OLD.requerimiento_id,
                    :NEW.etapa_id,
                    :NEW.actividad_id,
                    :NEW.secuencia,
                    :NEW.fecha_recibo,
                    :NEW.estado_id,
                    :NEW.concepto_id,
                    SYSDATE,
                    :NEW.cola_id,
                    :NEW.equipo_id,
                    v_usuario,
                    NULL,
                    NULL,
                    NULL,
                    NULL
    );
    END IF;

    dbms_output.put_line('SALI  TRIG_UPD_ORTR PR_CREAR_RASTRO ');
  END IF;

  IF nvl(:OLD.producto_id, 'PQUETE') <> 'PQUETE' THEN
    OPEN c_sol;

    FETCH c_sol
      INTO v_estado_sol, v_estado_soli, v_producto, v_identificador, v_identificador_nuevo, v_concepto_sol;  --CTAMAYOL 27MAY2013 Req 20216 Superplay

    CLOSE c_sol;

    --Estado y producto de la solicitud
    IF :OLD.concepto_id <> :NEW.concepto_id THEN
      BEGIN
        UPDATE fnx_requerimientos_trabajos
           SET etapa_id     = :NEW.etapa_id,
               actividad_id = :NEW.actividad_id,
               concepto_id  = :NEW.concepto_id,
               estado_id    = :NEW.estado_id,
               fecha_etapa  = :NEW.fecha_estado
         WHERE requerimiento_id = :NEW.requerimiento_id;
      EXCEPTION
        WHEN OTHERS THEN
          dbms_output.put_line('ERROR  fnx_requerimientos_trabajos');
      END;

      dbms_output.put_line('TRIGGER ORTR ' || :NEW.actividad_id || ' ' ||
                           :NEW.concepto_id);
     --abetanb Mejora Comandos ... Se agraga validacion de tipo nodo 31/05/2013

      IF :NEW.actividad_id IN ('SUSPE', 'ACTIV')
         AND :NEW.cola_id IN ('SUBAS', 'SUGRU')
         OR :OLD.actividad_id IN ('SUSPE', 'ACTIV')
         AND :OLD.cola_id IN ('SUBAS', 'SUGRU')
         AND  :OLD.tipo_nodo IN ('AXE', 'FTM', 'FTX', 'NEC') THEN
        dbms_output.put_line('se invoca PR_CREAR_COMANDO_CYR   con ' ||
                             :NEW.pedido_id || '-' ||
                             :NEW.actividad_id || '-' ||
                             :NEW.concepto_id);
        pr_crear_comando_cyr(:NEW.pedido_id,
                             :NEW.subpedido_id,
                             :NEW.solicitud_id,
                             :NEW.identificador_id,
                             v_identificador_nuevo,
                             :NEW.actividad_id,
                             :NEW.tipo_trabajo,
                             :NEW.producto_id,
                             :NEW.concepto_id);
        dbms_output.put_line('DESPUES DE  invoca PR_CREAR_COMANDO_CYR   con ' ||
                             :NEW.pedido_id || '-' ||
                             :NEW.actividad_id || '-' ||
                             :NEW.concepto_id);
      ELSIF :NEW.actividad_id IN ('HAESP', 'DEESP')
            AND  :OLD.tipo_nodo IN ('AXE', 'FTM', 'FTX', 'NEC') THEN
        IF v_identificador_nuevo IS NOT NULL
           AND v_identificador IS NOT NULL
           AND v_identificador_nuevo <> v_identificador THEN
          v_identificador_nuevo := v_identificador;
        END IF;

        --IF :NEW.concepto_id = 'PPRG'  THEN
        pr_crear_comando(:NEW.pedido_id,
                         :NEW.subpedido_id,
                         :NEW.solicitud_id,
                         :NEW.identificador_id,
                         v_identificador_nuevo,
                         :NEW.actividad_id,
                         :NEW.tipo_trabajo,
                         :NEW.producto_id,
                         :NEW.concepto_id);
        --END IF;
        --Ingresar Comando Servicios Especiales
        --LMZG 18/04/2004
      ELSIF :NEW.actividad_id IN ('HASUS', 'HACIR', 'HATOT')
            AND  :OLD.tipo_nodo IN ('AXE', 'FTM', 'FTX', 'NEC') THEN
        pr_crear_comando_descon(:NEW.pedido_id,
                                :NEW.subpedido_id,
                                :NEW.solicitud_id,
                                :NEW.identificador_id,
                                v_identificador_nuevo,
                                :NEW.actividad_id,
                                :NEW.tipo_trabajo,
                                :NEW.producto_id,
                                :NEW.concepto_id);
      END IF;

      --MARISTIM-MARZO-2006
      --Cuando el comando es cumplido manualmente el cumplido de la orden se debe  llamar desde este triger
      --Si se llama desde el triger de comandos_cyr saca triger mutante.
      /*     IF  NVL(:new.estado_id, 'nulo' ) = 'CUMPL'  THEN
                --PR_CUMPLIR_ORDEN(:new.pedido_id,
                dbms_output.put_line(' ');
            END IF;
      */
      IF :NEW.concepto_id = 'PPRG' THEN
        --Ingreso del registro a Cuenta Controlada
        IF :NEW.actividad_id IN ('HACUC', 'DECUC') THEN
          pr_crear_cuenta_controlada(:NEW.pedido_id,
                                     :NEW.subpedido_id,
                                     :NEW.solicitud_id,
                                     :NEW.identificador_id,
                                     v_identificador,
                                     :NEW.actividad_id,
                                     :NEW.tipo_trabajo);
        END IF;

        --Ingresar Plataformas
        --MARISTIM ENERO 23-2007
        -- PR_COLA_PLATAFORMA(:new.cola_id,v_tareas ,v_plataforma);
        BEGIN
          --    dbms_output.put_line('PLATAFORMA COLA ' || :new.cola_id);
          SELECT fn_procesar_cola(:NEW.cola_id),
                 fn_plataforma_cola(:NEW.cola_id)
            INTO v_procesar_cola, v_plataforma_cola
            FROM dual;
        EXCEPTION
          WHEN OTHERS THEN
            v_procesar_cola   := NULL;
            v_plataforma_cola := NULL;
        END;

        dbms_output.put_line(v_procesar_cola || '-' || v_concepto || '-' ||
                             v_procesar_cola || '-' ||
                             v_plataforma_cola);

        --    dbms_output.put_line('TRIG_UPD_ORTR--->>> PKG_TAREAS_PLATAFORMAS.pr_crear_tarea ' || v_plataforma_cola || :new.concepto_id);
        IF v_procesar_cola IN ('AUTO', 'PLATA')
           AND :NEW.concepto_id = 'PPRG' THEN
          IF :NEW.cola_id = 'ZEUS'
             AND :NEW.tipo_elemento = 'CMP' THEN
            v_tipo_trabajo := 'CAMBI';
          ELSE
            v_tipo_trabajo := :NEW.tipo_trabajo;
          END IF;

          OPEN c_tipotarea;
          FETCH c_tipotarea
            INTO v_tipo_tarea;
          CLOSE c_tipotarea;

          dbms_output.put_line('v_tipo_tarea  ' || v_tipo_tarea || '-' ||
                               :NEW.actividad_id);
          IF nvl(v_tipo_tarea, 'T') = 'B' THEN
            v_codigo_evento := fn_valor_caracteristica_sol(:NEW.pedido_id,
                                                           :NEW.subpedido_id,
                                                           :NEW.solicitud_id,
                                                           4805);
            /*2013-07-31 JTABARC Se ncluye una validación,para obtener la variable v_codigo_evento, para los cambios de equipo que se generen a través de un pedido de daño,
                                 es decir, a través de la pantalla de Fénix-Oracle*/
            IF v_codigo_evento IS NULL AND :NEW.PRODUCTO_ID = 'FIJLTE' AND :NEW.tipo_trabajo = 'REFAL' AND :NEW.TIPO_ELEMENTO_ID = 'SIMCAR' THEN
                v_codigo_evento := fn_valor_adi_parametros_ord ('ETIQ_CAMBI_EQUIP_REF',:NEW.actividad_id);
            END IF;
            --REQ34654_InstTecSMP      2014-02-05   ABETANB     Se adiciona la validacion del calculo del evento de los retiros para SMARTPLAY
            --REQ_53058_MejIDAsocSMP   2014-07-24   JGIRALJ
            IF :NEW.actividad_id = 'DESMA' AND :NEW.PRODUCTO_ID = 'SEREQU' AND (:NEW.tipo_trabajo = 'NUEVO' OR (:NEW.tipo_trabajo = 'CAMBI' AND :NEW.tipo_elemento_id='ESMPTV')) AND :NEW.TIPO_ELEMENTO = 'EQU' THEN
				v_codigo_evento := fn_valor_adi_parametros_ord ('ETIQ_CAMBI_EQUIP_REF',:NEW.actividad_id);
			END IF;
			IF :NEW.actividad_id = 'DESMA' AND :NEW.PRODUCTO_ID = 'SEREQU' AND :NEW.tipo_trabajo = 'REFAL' AND :NEW.TIPO_ELEMENTO = 'EQU' THEN
				v_codigo_evento := fn_valor_adi_parametros_ord ('ETIQ_CAMBI_EQUIP_REF',:NEW.actividad_id);
			END IF;
            --Fin ABETANB 2014-02-05
            /*2013-08-23 JGLENA Se realiza validacion para obtener v_codigo_evento en el cambio de equipo por mantenimiento sin sobreescribir LTE*/
            IF v_codigo_evento IS NULL AND :NEW.tipo_trabajo = 'REFAL' AND :NEW.TIPO_ELEMENTO = 'EQU' THEN
                v_codigo_evento := fn_valor_adi_parametros_ord ('ETIQ_CAMBI_EQUIP_REF',:NEW.actividad_id);
            		/*2014-06-24 JGLENA REQ36900_GPONHoga se asigna producto correcto para evento en cambio de equipo por mantenimiento */
                v_producto := :NEW.producto_id;
            END IF;

            /*2012-05-24 YSOLARTE Se reemplaza la función fn_dominio_descripcion por la función fn_valor_adi_parametros_ord de órdenes*/
            v_aplicacion := fn_valor_adi_parametros_ord (v_codigo_evento,:NEW.actividad_id);
          END IF;

          /*2011-12-05  YSOLARTE     LTE 4G Se modifica para reemplazar el proceso que inserta en el FNX_TAREAS_PLATAFORMAS por el proceso que inserta eventos*/
          /*Cmurillg 2013-02-11 Voz en la Nube - Se modifica condicion para consultar la plataforma en un en el parametro plataformas*/
          IF instr(FN_VALOR_PARAMETROS_ORD('LTE','PLATAFORMAS')||',',NVL(v_aplicacion,'X') || ',') > 0 THEN
          --F v_aplicacion IS NOT NULL THEN
            dbms_output.put_line('2 XXX TRIG_UPD_ORTR -->>> PKG_GESTION_EVENTOS_LTE.PR_CREAR_EVENTO ');
            pkg_apis_lte_fenix.pr_crear_evento_lte(v_aplicacion,
                                                   v_codigo_evento,
                                                   :NEW.pedido_id,
                                                   :NEW.subpedido_id,
                                                   :NEW.solicitud_id,
                                                   :NEW.identificador_id,
                                                   :NEW.tipo_trabajo,
                                                   :NEW.actividad_id);
          ELSIF instr(FN_VALOR_PARAMETROS_ORD('INTEGRABTS','PLATAFORMAS')||',',NVL(v_aplicacion,'X') || ',') > 0  THEN
              DBMS_OUTPUT.put_line ('PKG_INTEGRA_FENIX_BTS.PR_CREAR_EVENTO ');

              /*2013-04-25  YSOLARTE     Adición de parametro aplicación para insertar en la tabla bts_eventos en el campo aplicacion_id en el llamado al paquete PKG_INTEGRA_FENIX_BTS*/
              PKG_INTEGRA_FENIX_BTS.PR_CREAR_EVENTO(:NEW.pedido_id,
                                                   :NEW.subpedido_id,
                                                   :NEW.solicitud_id,
                                                    v_codigo_evento,
                                                    v_aplicacion,
                                                    v_producto,
                                                   :NEW.actividad_id,
                                                    v_error_bts,
                                                    v_mensaje_bts);
          ELSE
           /* 2014-06-10   JGLENA REQ36900_GPONHoga Se adiciona logica para pasar datos de la orden a pkg_tareas_plataforma  */
	    			IF :NEW.tecnologia_id = 'GPON' AND :NEW.tipo_trabajo = 'REFAL' THEN
	            v_orden_trab_rec.pedido_id := :new.pedido_id;
	            v_orden_trab_rec.subpedido_id := :new.subpedido_id;
	            v_orden_trab_rec.solicitud_id := :new.solicitud_id;
	            v_orden_trab_rec.requerimiento_id := :new.requerimiento_id;
	            v_orden_trab_rec.etapa_id := :new.etapa_id;
	            v_orden_trab_rec.actividad_id := :new.actividad_id;
	            v_orden_trab_rec.secuencia := :new.secuencia;
	            v_orden_trab_rec.cola_id := :new.cola_id;
	            v_orden_trab_rec.identificador_id := :new.identificador_id;
	            v_orden_trab_rec.producto_id := :new.producto_id;
	            v_orden_trab_rec.servicio_id := :new.servicio_id;
	            pkg_tareas_plataformas.pr_set_orden_trabajo_rec(v_orden_trab_rec);
            END IF;
            dbms_output.put_line('2 XXX TRIG_UPD_ORTR -->>> PKG_TAREAS_PLATAFORMAS.pr_crear_tarea ');
            pkg_tareas_plataformas.pr_crear_tarea(:NEW.pedido_id,
                                                  :NEW.subpedido_id,
                                                  :NEW.solicitud_id,
                                                  :NEW.requerimiento_id,
                                                  :NEW.identificador_id,
                                                  v_plataforma_cola,
                                                  :NEW.actividad_id,
                                                  :NEW.tipo_trabajo
                                                  --,v_mensaje
                                                  );
					   IF :NEW.tecnologia_id = 'GPON' AND :NEW.tipo_trabajo = 'REFAL' THEN
				     		v_orden_trab_rec := NULL;
				      	pkg_tareas_plataformas.pr_set_orden_trabajo_rec(v_orden_trab_rec);
					   END IF;
           /* Fin 2014-06-10   JGLENA REQ36900_GPONHoga Se adiciona logica para pasar datos de la orden a pkg_tareas_plataforma  */
          END IF;


          IF v_mensaje IS NOT NULL THEN
            dbms_output.put_line('Error al Insertar PKG_TAREAS_PLATAFORMAS.pr_crear_tarea ' ||
                                 v_mensaje);
          END IF;
        END IF;
      END IF; --IF :NEW.concepto_id = 'PPRG' THEN

      IF :NEW.estado_id = 'PENDI'
         AND :NEW.concepto_id IN ('PROG', 'AGEN', 'PQUET') --NOT IN ('PENDI','PPRG','ERCER')
         AND v_estado_sol = 'ORDEN' THEN
        BEGIN
          v_concepto := 'ORDEN'; --MARISTIM AGOSTO 01-2006

          IF :NEW.tipo_trabajo IN ('SUSPE', 'ACTIV')
             AND :NEW.cola_id IN ('CMSUS') THEN
            v_concepto := NULL;
          ELSE
            v_concepto := 'ORDEN';
          END IF;

         --2013-05-24 CTAMAYOL Inicio Req 20216 Superplay
         IF v_concepto IS NOT NULL THEN
            IF PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(:NEW.pedido_id) AND v_concepto_sol IN ('PSERV','PXSLN') THEN
                NULL;
            ELSE

                BEGIN
                  UPDATE fnx_solicitudes
                     SET concepto_id = v_concepto
                   WHERE pedido_id = :NEW.pedido_id
                     AND subpedido_id = :NEW.subpedido_id
                     AND solicitud_id = :NEW.solicitud_id;
                EXCEPTION
                  WHEN OTHERS THEN
                    dbms_output.put_line('ERROR  fnx_solicitudes');
                END;
           END IF;
          END IF;
        --2013-05-24 CTAMAYOL Fin Req 20216 Superplay
        END;
      END IF;
    END IF; --IF :OLD.concepto_id <> :NEW.concepto_id THEN
  END IF; --IF :old.cola_id='HOGAR' OR :new.cola_id='HOGAR' THEN

  /*2011-04-07 JGLENA Se modifica la lógica de los conteos de acuerdo a regla del negocio */

  /*IF :NEW.concepto_id <> :OLD.concepto_id
   AND (:NEW.concepto_id = 'AGEN' OR :OLD.concepto_id = 'AGEN') THEN
  dbms_output.put_line('Entra Nuevo: ' || :NEW.concepto_id ||
                       ' Viejo: ' || :OLD.concepto_id || ' ' ||
                       :OLD.cola_id || ' ' || :OLD.equipo_id || ' ' ||
                       :OLD.fecha_entrega || ' ' ||
                       :OLD.hora_entrega || ' ' ||
                       :OLD.area_operativa_id);

  OPEN c_agpa;

  FETCH c_agpa
    INTO v_cantidad_ortr_ag, v_rowid;

  IF c_agpa%NOTFOUND THEN
    v_cantidad_ortr_ag := -1;
  END IF;

  CLOSE c_agpa;

  dbms_output.put_line('Cantidad de v_cantidad_ortr_ag ' ||
                       v_cantidad_ortr_ag);

  IF :NEW.concepto_id = 'AGEN' THEN
    IF v_cantidad_ortr_ag >= 0 THEN
      v_cantidad_ortr_ag := v_cantidad_ortr_ag + 1;
    ELSIF v_cantidad_ortr_ag IS NULL THEN
      v_cantidad_ortr_ag := 1;
    END IF;
  ELSIF :OLD.concepto_id = 'AGEN' THEN
    IF v_cantidad_ortr_ag >= 1 THEN
      v_cantidad_ortr_ag := v_cantidad_ortr_ag - 1;
    ELSIF v_cantidad_ortr_ag IS NULL THEN
      v_cantidad_ortr_ag := 0;
    END IF;
  END IF;

  dbms_output.put_line('Cantidad Real de v_cantidad_ortr_ag ' ||
                       v_cantidad_ortr_ag);

  IF v_cantidad_ortr_ag >= 0 THEN
    BEGIN
      UPDATE fnx_agenda_param_det
         SET cantidad_ortr_ag = v_cantidad_ortr_ag
       WHERE ROWID = v_rowid;
    EXCEPTION
      WHEN OTHERS THEN
        v_cantidad_ortr_ag := 0;
    END;
  END IF;*/
  /* En el cambio de concepto se se anula la orden o continúa en estado pendiente y alguno de los conceptos
     conceptos es AGEN se debe realizar el conteo.
  */
  IF :NEW.concepto_id <> :OLD.concepto_id
     AND (:NEW.concepto_id = 'AGEN' OR :OLD.concepto_id = 'AGEN')
     AND :NEW.estado_id IN ('PENDI', 'ANULA') THEN
    dbms_output.put_line('Datos Orden OLD ' || :OLD.cola_id || ' ' ||
                         :OLD.area_operativa_id || ' ' ||
                         :OLD.subzona_id || ' ' ||
                         :OLD.area_trabajo_id || ' ' ||
                         :OLD.equipo_id || ' ' ||
                         to_char(:OLD.fecha_entrega,
                                 'DD/MM/YYYY HH24:MI') || ' ' ||
                         :OLD.hora_entrega);
    dbms_output.put_line('Datos Orden NEW ' || :NEW.cola_id || ' ' ||
                         :NEW.area_operativa_id || ' ' ||
                         :NEW.subzona_id || ' ' ||
                         :NEW.area_trabajo_id || ' ' ||
                         :NEW.equipo_id || ' ' ||
                         to_char(:NEW.fecha_entrega,
                                 'DD/MM/YYYY HH24:MI') || ' ' ||
                         :NEW.hora_entrega);

    IF :OLD.concepto_id = 'AGEN' THEN
      OPEN c_agpa(:OLD.cola_id,
                  :OLD.area_operativa_id,
                  :OLD.subzona_id,
                  :OLD.area_trabajo_id,
                  :OLD.equipo_id,
                  :OLD.fecha_entrega,
                  :OLD.hora_entrega,
                  NULL);
      FETCH c_agpa
        INTO v_cantidad_ortr_ag, v_rowid;
      IF c_agpa%FOUND THEN
        /* Descontar ordenes según la siguiente regla del negocio:
           Sólo se descuentan cupos cuando la fecha de entrega antigua sea posterior al día actual
        */
        IF trunc(:OLD.fecha_entrega) > trunc(SYSDATE) THEN
          v_cantidad_ortr_ag := v_cantidad_ortr_ag - 1;
          IF v_cantidad_ortr_ag < 0 THEN
            v_cantidad_ortr_ag := 0;
          END IF;
          BEGIN
            UPDATE fnx_agenda_param_det
               SET cantidad_ortr_ag = v_cantidad_ortr_ag
             WHERE ROWID = v_rowid;
          EXCEPTION
            WHEN OTHERS THEN
              dbms_output.put_line('No se pudo disminuir el numero de cupos usados v_cantidad_ortr_ag = ' ||
                                   v_cantidad_ortr_ag);
          END;
        END IF;
      END IF;
      CLOSE c_agpa;
      dbms_output.put_line('Se disminuyó el número de cupos. v_cantidad_ortr_ag ' ||
                           v_cantidad_ortr_ag);

    ELSIF :NEW.concepto_id = 'AGEN' THEN
      OPEN c_agpa(:NEW.cola_id,
                  :NEW.area_operativa_id,
                  :NEW.subzona_id,
                  :NEW.area_trabajo_id,
                  :NEW.equipo_id,
                  :NEW.fecha_entrega,
                  :NEW.hora_entrega,
                  'A');
      FETCH c_agpa
        INTO v_cantidad_ortr_ag, v_rowid;
      --Incrementar ordenes agendadas.
      IF c_agpa%FOUND THEN
        v_cantidad_ortr_ag := v_cantidad_ortr_ag + 1;
        IF v_cantidad_ortr_ag < 0 THEN
          v_cantidad_ortr_ag := 0;
        END IF;
        BEGIN
          UPDATE fnx_agenda_param_det
             SET cantidad_ortr_ag = v_cantidad_ortr_ag
           WHERE ROWID = v_rowid;
        EXCEPTION
          WHEN OTHERS THEN
            dbms_output.put_line('No se pudo aumentar el numero de cupos usados v_cantidad_ortr_ag = ' ||
                                 v_cantidad_ortr_ag);
        END;
      END IF;
      CLOSE c_agpa;
      dbms_output.put_line('Se incrementó el número de cupos. v_cantidad_ortr_ag ' ||
                           v_cantidad_ortr_ag);

    END IF;

  END IF; /*Fin IF cambio conceptos en estado PENDI */


  -- 2013-07-05 Jrincong
  -- Req 25261 - Gestion Daños EndToEnd. Cuando cambia alguno de los datos de Area Operativa / Equipo / Fecha PreAgenda / Hora PreAgenda
  -- o Concepto, se actualiza el campo fecha_actualizacion en la tabla FNX_REPORTES_MANTENIMIENTO
  IF NVL(:NEW.area_operativa_id, 'SIN')    <>  NVL(:OLD.area_operativa_id, 'SIN')     OR
     NVL(:NEW.equipo_id, 'SIN')            <>  NVL(:OLD.equipo_id, 'SIN')             OR
     NVL(:NEW.fecha_preagenda, SYSDATE-1)  <>  NVL(:OLD.fecha_preagenda, SYSDATE -2 ) OR
     NVL(:NEW.hora_preagenda, 'SIN')       <>  NVL(:OLD.hora_preagenda, 'SIN')        OR
     NVL(:NEW.concepto_id, 'SIN')          <>  NVL(:OLD.concepto_id, 'SIN')
  THEN
     BEGIN

        UPDATE FNX_REPORTES_MANTENIMIENTO
        SET    fecha_actualizacion  = SYSDATE
        WHERE  pedido_id    = :NEW.pedido_id
        AND    subpedido_id = :NEW.subpedido_id
        AND    solicitud_id = :NEW.solicitud_id
        ;

        DBMS_OUTPUT.PUT_LINE('<Actualizacion Fecha_Actualizacion');
     EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('<ORD> - TRIG_UPD_ORTR '||SUBSTR(SQLERRM, 1, 150));
     END;

  END IF;

END trig_upd_ortr;