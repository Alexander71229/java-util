import java.util.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.*;

public class Bloque extends Estructura{
	public Declaraciones declaraciones;
	public Sentencias sentencias;
	public Excepciones excepciones;
	public String codigo;
	public String tokenComprobacion;
	public static void comprobar(String token,Tokenizer fuente,Programa programa){
		Bloque bloque=null;
		if(token.toUpperCase().equals("DECLARE")||token.toUpperCase().equals("BEGIN")){
			bloque=new Bloque(token,fuente);
			programa.add(bloque);
		}
	}
	public Bloque(Tokenizer fuente){
		declaraciones=new Declaraciones();
		sentencias=new Sentencias();
		excepciones=new Excepciones();
		String token=fuente.next();
		analisis(token,fuente);
	}
	public Bloque(String token,Tokenizer fuente){
		this.codigo="";
		declaraciones=new Declaraciones();
		sentencias=new Sentencias();
		excepciones=new Excepciones();
		analisis(token,fuente);
	}
	public void analisis(String token,Tokenizer fuente){
		this.tokenComprobacion=token;
		token=Declaraciones.comprobar(token,fuente,declaraciones);
		token=Sentencias.comprobar(token,fuente,sentencias);
		token=Excepciones.comprobar(token,fuente,excepciones);
		if(!token.toUpperCase().equals("END")){
			System.out.println("Token no esperado en bloque:"+token+"--->"+this.tokenComprobacion);
			System.out.println(excepciones.codigo+"");
			System.exit(0);
		}
		token+=fuente.next()+"\n";
		if(!declaraciones.codigo.equals("")){
			this.codigo+=declaraciones.codigo+"\n";
		}
		this.codigo=this.codigo+sentencias.codigo+"\n"+excepciones.codigo+"\n"+token;
	}
	public String getCodigo(int nivel){
		String resultado="";
		resultado+=declaraciones.getCodigo(nivel);
		resultado+=sentencias.getCodigo(nivel);
		resultado+=excepciones.getCodigo(nivel);
		resultado+=Util.tab(nivel)+"END;\n";
		return resultado;
	}
	public String getTipo(){
		return "BLOQUE";
	}
	public String desc(int nivel){
		String resultado=Util.tab(nivel);
		resultado+=declaraciones.desc(nivel)+sentencias.desc(nivel)+excepciones.desc(nivel);
		return resultado;
	}
	public String getHTML()throws Exception{
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		contenido=contenido.replace("Declaraciones",declaraciones.getHTML());
		contenido=contenido.replace("Sentencias",sentencias.getHTML());
		contenido=contenido.replace("Excepciones",excepciones.getHTML());
		return contenido;
	}
}