FUNCTION       FN_TELEV_PAQUETE_CONFIGURADO (
    p_pedido_id           IN   fnx_solicitudes.pedido_id%TYPE
   ,p_identificador_id    IN   fnx_identificadores.identificador_id%TYPE
   ,p_tecnologia          IN   fnx_caracteristica_solicitudes.valor%TYPE
   ,p_canal               IN   fnx_configuraciones_identif.caracteristica_id%TYPE
   ,p_transaccion         IN   fnx_trabajos_solicitudes.tipo_trabajo%TYPE DEFAULT 'NUEVO'

)
   RETURN VARCHAR2
IS
-- Historia de Modificaciones

-- Objeto de la función: Se realiza la construcción de la función con el fin que extracción e integración pueda determinar
-- si un canal está configurado tanto para un SERHFC, como un SERVIP.

-- Fecha       Autor        Observacion
-- ----------  -----------  ----------------------------------------------------------
-- 2011-11-03  Wmendoza     Construcción Inicial de la función.
-- 2011-12-23  Aacosta      Modificación del cursor c_existe_canal_otr_cmp con el fin de cruzar con la tabla FNX_IDENTIFICADORES.
--                          esto debido que cuando se valida si el canal esta configurado en otro componente, se debe validar que
--                          ese identificador esta OCUpado.
-- 2012-08-30  Jrincong     Revisar sí el canal corresponde a un canal configurado como de Solo Demostración. En tal caso, la función debe
--                          retornar el valor SI.

   CURSOR c_solicitudes_canal (p_pedido FNX_SOLICITUDES.pedido_id%TYPE
                              ,p_caracteristica_canal fnx_trabajos_solicitudes.caracteristica_id%TYPE
   )
   IS
   SELECT A.pedido_id, a.subpedido_id, a.solicitud_id,a.tipo_elemento, a.tipo_elemento_id
     FROM FNX_TRABAJOS_SOLICITUDES A
    WHERE PEDIDO_ID          = p_pedido
      AND CARACTERISTICA_ID  = p_caracteristica_canal
      AND TIPO_TRABAJO       = 'NUEVO';

   CURSOR c_existe_canal (
       p_identificador_id    fnx_identificadores.identificador_id%TYPE
      ,p_caracteristica_id   fnx_caracteristica_solicitudes.caracteristica_id%TYPE
   )
   IS
      SELECT 'EXISTE'
        FROM fnx_configuraciones_identif
       WHERE identificador_id  = p_identificador_id
         AND caracteristica_id = p_caracteristica_id
         AND valor             = 'S';

   CURSOR c_existe_canal_otr_cmp (
       p_identificador_id    fnx_identificadores.identificador_id%TYPE
      ,p_caracteristica_id   fnx_caracteristica_solicitudes.caracteristica_id%TYPE
   )
   IS
      SELECT 'EXISTE'
        FROM fnx_configuraciones_identif a, fnx_identificadores b
       WHERE a.identificador_id = b.identificador_id
         AND a.identificador_id  = p_identificador_id
         AND a.caracteristica_id = p_caracteristica_id
         AND a.valor             = 'S'
         AND b.estado            = 'OCU';

   v_existe_canal         c_existe_canal%ROWTYPE;
   v_existe_canal_otr_cmp c_existe_canal_otr_cmp%ROWTYPE;
   v_valor_retorno       VARCHAR2 (4);
   v_identificador_srv   fnx_identificadores.identificador_id%TYPE;

   v_tipo_elemento_id    fnx_identificadores.tipo_elemento_id%TYPE;

   c_servip         NUMBER(5):=0;
   c_serhfc         NUMBER(5):=0;

   b_canal_ambos_componentes        BOOLEAN:=FALSE;

   v_modulo_parametrizacion         FNX_PARAMETROS.modulo_id%TYPE;
   e_canal_solo_demostracion        EXCEPTION;

BEGIN

    -- 2012-08-30
    -- Revisar sí el canal corresponde a un canal configurado como de Solo Demostración y que no se cobra en Open.
    -- En tal caso, la función debe retornar el valor SI para que el módulo de Integración Facturación, interprete que
    -- el canal NO se debe cobrar en Open (cuando es Nuevo) o Desconfigurar de Open en transacciones de Retiro.
    v_modulo_parametrizacion  := 'DEM_'||TO_CHAR(p_canal);
    IF NVL(fn_valor_parametros (v_modulo_parametrizacion, SUBSTR(p_canal, 1, 3)), 'N') = 'S'
    THEN
       -- Sí el canal está configurado como solo Demostración, se genera la excepción para retornar el valor SI
       RAISE e_canal_solo_demostracion;
    END IF;

    c_servip:=0;
    c_serhfc:=0;
    -- Verificar si el canal que llega por parametro, existe tanto en el SERVIP.
    -- como el SERHFC.
    FOR reg IN c_solicitudes_canal(p_pedido_id,p_canal) LOOP

        IF reg.tipo_elemento_id='SERVIP' THEN
            c_servip:=c_servip+1;
        END IF;

        IF reg.tipo_elemento_id='SERHFC' THEN
            c_serhfc:=c_serhfc+1;
        END IF;

    END LOOP;

    v_tipo_elemento_id:=PKG_IDENTIFICADORES.fn_elemento_id(p_identificador_id);
    v_valor_retorno     := 'NO';
    b_canal_ambos_componentes:=FALSE;

    -- Si ambos contadores de SERHFC y SERVIP, son mayores a cero,
    -- quiere decir que el canal existe en ambos componentes.
    IF c_servip>0  AND c_serhfc>0 THEN
        -- En este caso, el canal fue solicitado en ambos componentes para un servicio de Telefonía MIXTA
        b_canal_ambos_componentes:=TRUE;
    ELSE
        b_canal_ambos_componentes:=FALSE;
    END IF;

    -- Si existe el canal en ambos componentes y el elemento del identificador que llega como parámetro es un SERHFC,
    -- la función devuelve NO.
    IF b_canal_ambos_componentes AND v_tipo_elemento_id='SERHFC' THEN
          v_valor_retorno:='NO';
    ELSIF b_canal_ambos_componentes AND v_tipo_elemento_id='SERVIP' THEN
       --  Si existe el canal en ambos componentes y el elemento del identificador que llega como parámetro es un SERVIP,
       -- la función devuelve SI, para indicar que el componente ya se encuentra configurado para el servicio.
          v_valor_retorno:='SI';
    -- En el caso que el canal no esté en ambos componentes
    ELSIF NOT b_canal_ambos_componentes THEN
           -- Se busca el identificador de servicio de Television asociado al identificador
           v_identificador_srv := pkg_identificadores.fn_valor_configuracion (p_identificador_id, 90);
           OPEN c_existe_canal (p_identificador_id,
                                p_canal);
           FETCH c_existe_canal INTO v_existe_canal;
           IF c_existe_canal%FOUND THEN
                  v_valor_retorno := 'SI';
                  -- Buscar si el canal existe también para el otro identificador
                  IF p_tecnologia='IP' THEN
                      v_identificador_srv := pkg_identificadores.fn_valor_configuracion (p_identificador_id, 90)||'-SERVIP';
                  ELSIF p_tecnologia='HFC' THEN
                      v_identificador_srv := pkg_identificadores.fn_valor_configuracion (p_identificador_id, 90)||'-SERHFC';
                  END IF;

                  -- Se busca el canal en el otro componente.
                  OPEN c_existe_canal_otr_cmp (v_identificador_srv,
                                               p_canal);
                  FETCH c_existe_canal_otr_cmp INTO v_existe_canal_otr_cmp;
                  IF c_existe_canal_otr_cmp%FOUND THEN
                     v_valor_retorno := 'SI';
                  ELSE -- No existe en el portafolio.
                     v_valor_retorno := 'NO';
                  END IF;
                  CLOSE c_existe_canal_otr_cmp;
           ELSE
                v_valor_retorno := 'NO';
           END IF;

           CLOSE c_existe_canal;

    END IF;

    RETURN (v_valor_retorno);

EXCEPTION
    WHEN e_canal_solo_demostracion THEN
         -- Para el caso del canal sólo Demostración, se retorna el valor SI.
         v_valor_retorno := 'SI';
         RETURN (v_valor_retorno);
    WHEN NO_DATA_FOUND THEN
         v_valor_retorno := 'NO';
         RETURN (v_valor_retorno);
    WHEN OTHERS THEN
         v_valor_retorno := 'NO';
         RETURN (v_valor_retorno);
END;