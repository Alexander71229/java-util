import java.util.*;
public class Funcion extends Estructura{
	public String nombre;
	public String codigo;
	public Parametros parametros;
	public Bloque bloque;
	public String tipo;
	public static void comprobar(String token,Tokenizer fuente,Programa programa){
		Funcion funcion=null;
		if(token.toUpperCase().equals("FUNCTION")){
			funcion=new Funcion(fuente);
			programa.add(funcion);
		}
	}
	public Funcion(Tokenizer fuente){
		analisis(fuente);
	}
	public void analisis(Tokenizer fuente){
		nombre=fuente.next();
		codigo="FUNCTION "+nombre;
		parametros=new Parametros(fuente);
		codigo+=parametros.codigo;
		String token="";
		tipo="";
		while(fuente.hasNext()){
			token=fuente.next();
			if(token.toUpperCase().equals("IS")){
				break;
			}
			this.tipo+=" "+token;
			this.codigo+=" "+token;
		}
		bloque=new Bloque(token,fuente);
		this.codigo+="\n"+bloque.codigo;
	}
	public String getCodigo(int nivel){
		String resultado=Util.tab(nivel);
		resultado+="FUNCTION "+this.nombre.toUpperCase()+parametros.getCodigo(0)+tipo+"\n";
		resultado+=this.bloque.getCodigo(nivel);
		return resultado;
	}
	public String getTipo(){
		return "FUNCIÓN";
	}
	public String desc(int nivel){
		String resultado=Util.tab(nivel);
		resultado+=this.getTipo()+" "+this.nombre+"\n";
		resultado+=bloque.desc(nivel);
		return resultado;
	}
	public String getHTML()throws Exception{
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		contenido=contenido.replace("Descripcion","FUNCTION "+this.nombre+" "+tipo);
		contenido=contenido.replace("Bloque",bloque.getHTML());
		return contenido;
	}
}