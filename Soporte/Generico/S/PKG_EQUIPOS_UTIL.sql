PACKAGE BODY PKG_EQUIPOS_UTIL IS
    -- ----------------------------------------------------
    -- Fecha       Autor       Observacion
    -- --------    --------    -------------------------------------------
    -- 2005-02-04  Jrincong    Función Fn_valor_configuracion_mr. Función para obtener el valor
    --                         de configuración de equipo, Marca, Referencia, Serial y Car.
    -- 2005-09-21  Pmendoza    Procedimiento para cambiar la configuración de un equipo a Otro.
    -- 2005-11-15  Jrincong    Proc. Pr_Equipos_Transferir.  Adición de parámetros p_servicio, p_producto.
    -- 2005-11-22  Jrincong    Proc. Pr_Config_Trabajos. Para el caso de los canales de televisión,
    --                         la característica se actualiza a N. Si no es canal, la carac. se retira.
    -- 2006-07-15  Jrincong    Procs. pr_configurar_equipo_mr y pr_configurar_equipo. No se
    --                         consideran las características 2613 y 2614 para configurar el equipo
    -- 2006-07-15  Jrincong    Proc. Pr_Borrar_Config. No se borran de la configuración, las características
    --                         347 ( Tipo Equipo ), 959 ( MAC Equipo ), 2608 ( Tipo Equipo Movil )
    -- 2006-08-31  Jrincong    Nuevas funciones para obtener identificador y agrupador asociado a
    --                         un equipo
    -- 2008-01-17  Jrincong    Proc. Pr_Borrar_Config. Inclusión de característica 3765, para no
    --                         borrarla de la configuración.
    -- 2008-07-14   Jrincong   Proc. Pr_Borrar_Config. Inclusión de característica 3766, para no
    --                         borrarla de la configuración.
    -- 2009-05-05   Jrincong   Proc. Pr_Borrar_Config. Inclusión de característica 3767, para no
    --                         borrarla de la configuración.
    -- 2010-03-01   Jaristz    Modificacion del procedimiento PR_EQUIPO_ACT_TERMINAL para permitir insertan registros
    --                         en la tabla FNX_TERMINALES_EQUIPOS.
    -- 2013-11-22   Aarbob     Se detecta que la función  FN_estado_equipo, devuelve una descripción mayor a tres caracteres en caso de no encontrar el equipo, 
    --                         esto causa que el proceso que llame a esta función genere error, por en el caso del pr_enrutar_telev, define la variable
    --                         que recibe el valor de la función es de tres caracteres , del tipo del campo estado de la tabla fnx_equipos. 

    -- PR_EQUIPO_TIPO_ELEM
    -- Procedimiento para obtener el tipo elemento y tipo de elementoID de un equipo.
    PROCEDURE pr_equipo_tipoelem(p_equipo_id     IN VARCHAR2,
                                          p_marca_id      IN VARCHAR2,
                                          p_referencia_id IN VARCHAR2,
                                          p_tipo_elem     OUT VARCHAR2,
                                          p_tipo_elemid   OUT VARCHAR2,
                                          p_mensaje       OUT VARCHAR2) IS
        -- Cursor para obtener el tipo_elemento_id y el tipo de elemento asociado a un equipo
        CURSOR c_tipoelem_equ IS
            SELECT tipo_elemento_id,
                     tipo_elemento
              FROM fnx_equipos
             WHERE equipo_id = p_equipo_id
                AND marca_id = p_marca_id
                AND referencia_id = p_referencia_id;
    BEGIN
        -- Obtener el tipo de elemento y el tipo elementoID del equipo asociado
        OPEN c_tipoelem_equ;

        FETCH c_tipoelem_equ
            INTO p_tipo_elemid,
                  p_tipo_elem;

        IF c_tipoelem_equ%NOTFOUND THEN
            p_mensaje := '<PKG_EQU>. No se encontrò el equipo. Verificar serial, marca y referencia';
        END IF;

        CLOSE c_tipoelem_equ;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := '<PKG_EQU> Error determinando tipo elemento. ' || substr(SQLERRM,
                                                                                                         1,
                                                                                                         10);
    END; -- FIN PR_EQUIPO_TIPO_ELEM

    -- PR_EQUIPO_MARCA_REFER
    -- Procedimiento para obtener la marca y la referencia del equipo pasado como parámetro.
    PROCEDURE pr_equipo_marca_refer(p_equipo_id        IN VARCHAR2,
                                              p_tipo_elemento    IN VARCHAR2,
                                              p_tipo_elemento_id IN VARCHAR2,
                                              pr_marca           OUT VARCHAR2,
                                              pr_referencia      OUT VARCHAR2) IS
        CURSOR c_equip_marcaref IS
            SELECT marca_id,
                     referencia_id
              FROM fnx_equipos
             WHERE equipo_id = p_equipo_id
                AND tipo_elemento = p_tipo_elemento
                AND tipo_elemento_id = p_tipo_elemento_id;
    BEGIN
        OPEN c_equip_marcaref;

        FETCH c_equip_marcaref
            INTO pr_marca,
                  pr_referencia;

        IF c_equip_marcaref%NOTFOUND THEN
            pr_marca      := NULL;
            pr_referencia := NULL;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            pr_marca      := NULL;
            pr_referencia := NULL;
    END; -- Fin PR_EQUIPO_MARCA_REFER

    -- PR_INSERTAR_CONF_EQU
    -- Procedimiento para insertar un registro en la tabla FNX_CONFIGURACIONES_EQUIPOS
    PROCEDURE pr_insertar_conf_equ(p_marca             IN fnx_configuraciones_equipos.marca_id%TYPE,
                                             p_referencia        IN fnx_configuraciones_equipos.referencia_id%TYPE,
                                             p_tipo_elemento_id  IN fnx_configuraciones_equipos.tipo_elemento_id%TYPE,
                                             p_tipo_elemento     IN fnx_configuraciones_equipos.tipo_elemento%TYPE,
                                             p_equipo_id         IN fnx_configuraciones_equipos.equipo_id%TYPE,
                                             p_caracteristica_id IN fnx_configuraciones_equipos.caracteristica_id%TYPE,
                                             p_valor             IN fnx_configuraciones_equipos.valor%TYPE,
                                             p_mensaje           OUT VARCHAR2) IS
    BEGIN
        INSERT INTO fnx_configuraciones_equipos
            (marca_id,
             referencia_id,
             tipo_elemento_id,
             tipo_elemento,
             equipo_id,
             caracteristica_id,
             valor)
        VALUES
            (p_marca,
             p_referencia,
             p_tipo_elemento_id,
             p_tipo_elemento,
             p_equipo_id,
             p_caracteristica_id,
             p_valor);
    EXCEPTION
        WHEN dup_val_on_index THEN
            UPDATE fnx_configuraciones_equipos
                SET valor = p_valor
             WHERE tipo_elemento_id = p_tipo_elemento_id
                AND tipo_elemento = p_tipo_elemento
                AND equipo_id = p_equipo_id
                AND caracteristica_id = p_caracteristica_id;
        WHEN OTHERS THEN
            dbms_output.put_line(p_caracteristica_id||' ERROR ' || substr(SQLERRM,
                                                                 1,
                                                                 200));
            p_mensaje := '<PKG_EQU> Error configurando equipo - ' || p_tipo_elemento_id || '-' || p_equipo_id || '-' || to_char(p_caracteristica_id)|| substr(SQLERRM,
                                                                 1,
                                                                 200);
    END; -- Fin PR_INSERTAR_CONF_EQU

    -- PR_CONFIGURAR_EQUIPO
    -- Procedimiento para configurar un equipo con base en las características
    -- de una solicitud.
    PROCEDURE pr_configurar_equipo(p_pedido      IN fnx_pedidos.pedido_id%TYPE,
                                             p_subpedido   IN fnx_subpedidos.subpedido_id%TYPE,
                                             p_solicitud   IN fnx_solicitudes.solicitud_id%TYPE,
                                             p_equipo      IN fnx_equipos.equipo_id%TYPE,
                                             p_tipo_elemid IN fnx_equipos.tipo_elemento_id%TYPE,
                                             p_tipo_elem   IN fnx_equipos.tipo_elemento%TYPE,
                                             p_mensaje     OUT VARCHAR2) IS
        CURSOR c_carac IS
            SELECT caracteristica_id,
                     valor
              FROM fnx_caracteristica_solicitudes
             WHERE pedido_id = p_pedido
                AND subpedido_id = p_subpedido
                AND solicitud_id = p_solicitud
                AND caracteristica_id NOT IN (2613,
                                                        2614);

        v_carac      c_carac%ROWTYPE;
        v_marca      fnx_equipos.marca_id%TYPE;
        v_referencia fnx_equipos.referencia_id%TYPE;
    BEGIN
        -- Determinar marca y referencia del equipo
        pkg_equipos_util.pr_equipo_marca_refer(p_equipo,
                                                            p_tipo_elem,
                                                            p_tipo_elemid,
                                                            v_marca,
                                                            v_referencia);

        IF v_marca IS NULL THEN
            p_mensaje := '<PKG_EQU> No se pudo determinar la marca del equipo. ' || p_equipo;
        END IF;

        OPEN c_carac;

        LOOP
            FETCH c_carac
                INTO v_carac;

            EXIT WHEN c_carac%NOTFOUND OR p_mensaje IS NOT NULL;

            UPDATE fnx_configuraciones_equipos
                SET valor = v_carac.valor
             WHERE tipo_elemento_id = p_tipo_elemid
                AND tipo_elemento = p_tipo_elem
                AND equipo_id = p_equipo
                AND caracteristica_id = v_carac.caracteristica_id;

            IF SQL%ROWCOUNT = 0 THEN
                pkg_equipos_util.pr_insertar_conf_equ(v_marca,
                                                                  v_referencia,
                                                                  p_tipo_elemid,
                                                                  p_tipo_elem,
                                                                  p_equipo,
                                                                  v_carac.caracteristica_id,
                                                                  v_carac.valor,
                                                                  p_mensaje);
            END IF;
        END LOOP;

        CLOSE c_carac;
    END; -- Fin PR_CONFIGURAR_EQUIPO

    -- PR_CONFIGURAR_EQUIPO_MR
    -- Procedimiento para configurar un equipo con base en las características
    -- de una solicitud.
    PROCEDURE pr_configurar_equipo_mr(p_pedido      IN fnx_pedidos.pedido_id%TYPE,
                                                 p_subpedido   IN fnx_subpedidos.subpedido_id%TYPE,
                                                 p_solicitud   IN fnx_solicitudes.solicitud_id%TYPE,
                                                 p_equipo      IN fnx_equipos.equipo_id%TYPE,
                                                 p_tipo_elemid IN fnx_equipos.tipo_elemento_id%TYPE,
                                                 p_tipo_elem   IN fnx_equipos.tipo_elemento%TYPE,
                                                 p_marca       IN fnx_equipos.marca_id%TYPE,
                                                 p_referencia  IN fnx_equipos.referencia_id%TYPE,
                                                 p_mensaje     OUT VARCHAR2) IS
        CURSOR c_carac IS
            SELECT caracteristica_id,
                     valor
              FROM fnx_caracteristica_solicitudes
             WHERE pedido_id = p_pedido
                AND subpedido_id = p_subpedido
                AND solicitud_id = p_solicitud
                AND caracteristica_id NOT IN (2613,
                                                        2614);

        v_carac      c_carac%ROWTYPE;
        v_marca      fnx_equipos.marca_id%TYPE;
        v_referencia fnx_equipos.referencia_id%TYPE;
    BEGIN
        -- Determinar marca y referencia del equipo
        pkg_equipos_util.pr_equipo_marca_refer(p_equipo,
                                                            p_tipo_elem,
                                                            p_tipo_elemid,
                                                            v_marca,
                                                            v_referencia);

        -- Comparar marca y referencia encontradas con la marca y referencia pasadas
        -- como parámetros.
        IF nvl(v_marca,
                 'NOMARCA') <> nvl(p_marca,
                                         'SINMARCA') AND
            nvl(v_referencia,
                 'NOREFER') <> nvl(p_referencia,
                                         'SINREFER') THEN
            p_mensaje := '<PKG-EQU> Equipo: ' || p_equipo || ' Marca o referencia no corresponden';
        END IF;

        IF v_marca IS NULL THEN
            p_mensaje := '<PKG_EQU> No se pudo determinar la marca del equipo. ' || p_equipo;
        END IF;

        IF p_mensaje IS NULL THEN
            OPEN c_carac;

            LOOP
                FETCH c_carac
                    INTO v_carac;

                EXIT WHEN c_carac%NOTFOUND OR p_mensaje IS NOT NULL;

                UPDATE fnx_configuraciones_equipos
                    SET valor = v_carac.valor
                 WHERE tipo_elemento_id = p_tipo_elemid
                    AND tipo_elemento = p_tipo_elem
                    AND equipo_id = p_equipo
                    AND caracteristica_id = v_carac.caracteristica_id;

                IF SQL%ROWCOUNT = 0 THEN
                    pkg_equipos_util.pr_insertar_conf_equ(v_marca,
                                                                      v_referencia,
                                                                      p_tipo_elemid,
                                                                      p_tipo_elem,
                                                                      p_equipo,
                                                                      v_carac.caracteristica_id,
                                                                      v_carac.valor,
                                                                      p_mensaje);
                END IF;
            END LOOP;

            CLOSE c_carac;
        END IF;
    END; -- Fin PR_CONFIGURAR_EQUIPO

    -- PR_EQUIPO_ACT_ESTADO
    -- Procedimiento para actualizar el estado de un equipo, el identificador asociado
    -- el agrupador y la fecha de estado.
    PROCEDURE pr_equipo_act_estado(p_tipo_elemento_id IN fnx_equipos.tipo_elemento_id%TYPE,
                                             p_tipo_elemento    IN fnx_equipos.tipo_elemento%TYPE,
                                             p_equipo           IN fnx_equipos.equipo_id%TYPE,
                                             p_estado           IN fnx_equipos.estado%TYPE,
                                             p_identificador    IN fnx_equipos.identificador_id%TYPE,
                                             p_agrupador        IN fnx_equipos.agrupador_id%TYPE,
                                             p_mensaje          OUT VARCHAR2,
                                             p_municipio        IN fnx_solicitudes.municipio_id%TYPE DEFAULT 'MEDANTCOL',
                                             p_empresa          IN fnx_solicitudes.empresa_id%TYPE DEFAULT 'UNE',
                                             p_marca_id         IN VARCHAR2 DEFAULT NULL,
                                             p_referencia_id    IN VARCHAR2 DEFAULT NULL) IS
        CURSOR c_equipo_mr IS
            SELECT 'S'
              FROM fnx_equipos
             WHERE tipo_elemento_id = p_tipo_elemento_id
                AND tipo_elemento = p_tipo_elemento
                AND marca_id = p_marca_id
                AND referencia_id = p_referencia_id
                AND equipo_id = p_equipo
                FOR UPDATE;

        CURSOR c_equipo IS
            SELECT 'S'
              FROM fnx_equipos
             WHERE tipo_elemento_id = p_tipo_elemento_id
                AND tipo_elemento = p_tipo_elemento
                AND equipo_id = p_equipo
                FOR UPDATE;

        v_encontrado VARCHAR2(1);
    BEGIN
        IF p_marca_id IS NULL OR p_referencia_id IS NULL THEN
            OPEN c_equipo;

            FETCH c_equipo
                INTO v_encontrado;

            IF c_equipo%FOUND THEN
                UPDATE fnx_equipos
                    SET estado           = p_estado,
                         fecha_estado     = SYSDATE,
                         identificador_id = p_identificador,
                         agrupador_id     = p_agrupador,
                         municipio_id     = p_municipio,
                         empresa_id       = p_empresa
                 WHERE CURRENT OF c_equipo;
            ELSE
                p_mensaje := '<PKG_EQU>.  Equipo no encontrado: ' || p_tipo_elemento_id || '-' || p_tipo_elemento || '-' || p_equipo;
            END IF;

            CLOSE c_equipo;
        ELSE
            OPEN c_equipo_mr;

            FETCH c_equipo_mr
                INTO v_encontrado;

            IF c_equipo_mr%FOUND THEN
                UPDATE fnx_equipos
                    SET estado           = p_estado,
                         fecha_estado     = SYSDATE,
                         identificador_id = p_identificador,
                         agrupador_id     = p_agrupador,
                         municipio_id     = p_municipio,
                         empresa_id       = p_empresa
                 WHERE CURRENT OF c_equipo_mr;
            ELSE
                p_mensaje := '<PKG_EQU>.  Equipo no encontrado: ' || p_tipo_elemento_id || '-' || p_tipo_elemento || '-' || p_equipo;
            END IF;

            CLOSE c_equipo_mr;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := '<PKG_EQU>.  Error actualizando estado equipo: ' || p_tipo_elemento || '-' || p_tipo_elemento || '-' || p_equipo;
    END; -- Fin PR_EQUIPO_ACT_ESTADO

    -- PR_EQUIPO_ACT_TERMINAL
    -- Procedimiento para actualizar el estado de una terminal asociada a un equipo, el identificador asociado
    -- el agrupador y la fecha de estado.
    -- HISTORIA DE MODIFICACIONES
    -- Fecha        Autor       Observacion
    -- -------------------------------------------------------------------------
    -- 2010-03-01   Jaristz    Modificacion del procedimiento PR_EQUIPO_ACT_TERMINAL para permitir insertan registros
    --                         en la tabla FNX_TERMINALES_EQUIPOS.
    PROCEDURE pr_equipo_act_terminal(p_tipo_elemento_id IN fnx_equipos.tipo_elemento_id%TYPE,
                                                p_tipo_elemento    IN fnx_equipos.tipo_elemento%TYPE,
                                                p_marca            IN fnx_marcas_referencias.marca_id%TYPE,
                                                p_referencia       IN fnx_marcas_referencias.referencia_id%TYPE,
                                                p_equipo           IN fnx_equipos.equipo_id%TYPE,
                                                p_terminal         IN fnx_terminales_equipos.terminal_id%TYPE,
                                                p_estado           IN fnx_equipos.estado%TYPE,
                                                p_identificador    IN fnx_equipos.identificador_id%TYPE,
                                                p_agrupador        IN fnx_equipos.agrupador_id%TYPE,
                                                p_mensaje          OUT VARCHAR2) IS
        CURSOR c_terminales_equipos IS
            SELECT 'S'
              FROM fnx_terminales_equipos
             WHERE tipo_elemento_id = p_tipo_elemento_id
                AND tipo_elemento = p_tipo_elemento
                AND equipo_id = p_equipo
                AND marca_id = p_marca
                AND referencia_id = p_referencia
                AND terminal_id = p_terminal
                AND estado = 'LIB'
                FOR UPDATE;

        v_encontrado VARCHAR2(1);
    BEGIN
        OPEN c_terminales_equipos;

        FETCH c_terminales_equipos
            INTO v_encontrado;

        IF c_terminales_equipos%FOUND THEN
            UPDATE fnx_terminales_equipos
                SET estado           = p_estado,
                     identificador_id = p_identificador,
                     agrupador_id     = p_agrupador
             WHERE CURRENT OF c_terminales_equipos;
        ELSE
            BEGIN
                -- Copiar configuración del identificador anterior al nuevo identificador
                INSERT INTO fnx_terminales_equipos
                    (marca_id,
                     referencia_id,
                     tipo_elemento_id,
                     tipo_elemento,
                     equipo_id,
                     terminal_id,
                     estado,
                     identificador_id,
                     agrupador_id,
                     tipo_terminal_id)
                VALUES
                    (p_marca,
                     p_referencia,
                     p_tipo_elemento_id,
                     p_tipo_elemento,
                     p_equipo,
                     p_terminal,
                     p_estado,
                     p_identificador,
                     p_agrupador,
                     NULL);
            EXCEPTION
                WHEN OTHERS THEN
                    p_mensaje := '<PKG_EQU>. Error insertando registro FNX_TERMINALES_EQUIPOS: ' || p_tipo_elemento_id || '-' || p_tipo_elemento || '-' ||
                                     p_equipo || '-' || p_terminal;
            END;
        END IF;

        CLOSE c_terminales_equipos;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := '<PKG_EQU>.  Error actualizando estado de la terminal asociada al equipo: ' || p_tipo_elemento || '-' || p_tipo_elemento || '-' ||
                             p_equipo || '-' || p_terminal;
    END; -- Fin PR_EQUIPO_ACT_TERMINAL

    -- PR_EQUIPO_SERIAL_SOL
    -- Procedimiento para determinar el valor del serial de un equipo, suministrado
    -- como característica de una solicitud
    PROCEDURE pr_equipo_serial_sol(p_pedido         IN fnx_solicitudes.pedido_id%TYPE,
                                             p_subpedido      IN fnx_solicitudes.subpedido_id%TYPE,
                                             p_solicitud      IN fnx_solicitudes.solicitud_id%TYPE,
                                             p_caracteristica IN fnx_caracteristica_solicitudes.caracteristica_id%TYPE,
                                             p_equipo         IN OUT fnx_solicitudes.equipo_id%TYPE,
                                             p_mensaje        OUT VARCHAR2) IS
    BEGIN
        -- Se utiliza la instruccion Replace, debido a que algunos seriales son suministrados
        -- con el caracter @ en la solicitud.  ( Ejm. Decodificadores de television )
        SELECT REPLACE(valor,
                            '@',
                            '')
          INTO p_equipo
          FROM fnx_caracteristica_solicitudes
         WHERE pedido_id = p_pedido
            AND subpedido_id = p_subpedido
            AND solicitud_id = p_solicitud
            AND caracteristica_id = p_caracteristica;

        IF p_equipo IS NULL THEN
            p_mensaje := '<PKG_EQU>. Serial de equipo nulo. Caracteristica ' || to_char(p_caracteristica);
        END IF;
    EXCEPTION
        WHEN no_data_found THEN
            p_mensaje := '<PKG_EQU> No se hizo provisión de Equipo. Falta caracterìstica ' || to_char(p_caracteristica);
            p_equipo  := NULL;
        WHEN OTHERS THEN
            p_mensaje := '<PKG_EQU> WO. No pudo determinarse el serial del Equipo';
            p_equipo  := NULL;
    END; -- Fin PR_EQUIPO_SERIAL_SOL

    -- PR_EQUIPOS_TRANSFERIR
    -- Procedimiento utilizado para transferir los equipos asociados desde un
    -- identificador hacia otro identificador
    PROCEDURE pr_equipos_transferir(p_identificador_anterior IN VARCHAR2,
                                              p_identificador_nuevo    IN VARCHAR2,
                                              p_servicio               IN fnx_equipos.servicio_id%TYPE,
                                              p_producto               IN fnx_equipos.producto_id%TYPE,
                                              pr_mensaje               OUT VARCHAR2) IS
        -- Cursor para determinar los equipos asociados al identificador anterior
        CURSOR c_equipos IS
            SELECT equipo_id,
                     tipo_elemento,
                     tipo_elemento_id
              FROM fnx_equipos
             WHERE identificador_id = p_identificador_anterior;
    BEGIN
        FOR reg IN c_equipos LOOP
            -- Actualizar la característica de identificador asociado
            UPDATE fnx_configuraciones_equipos
                SET valor = p_identificador_nuevo
             WHERE tipo_elemento_id = reg.tipo_elemento_id
                AND tipo_elemento = reg.tipo_elemento
                AND equipo_id = reg.equipo_id
                AND caracteristica_id = 90;

            -- Actualizar el equipo con el nuevo identificador
            UPDATE fnx_equipos
                SET identificador_id = p_identificador_nuevo,
                     servicio_id      = p_servicio,
                     producto_id      = p_producto
             WHERE tipo_elemento_id = reg.tipo_elemento_id
                AND tipo_elemento = reg.tipo_elemento
                AND equipo_id = reg.equipo_id;
        END LOOP;
    EXCEPTION
        WHEN OTHERS THEN
            pr_mensaje := '<PKG_EQU> WO. Error transfiriendo equipos';
    END; -- Fin PI_TRANSFERIR_EQUIPOS

    -- PR_BORRAR_CARACT_CONFIG
    -- Procedimiento para borrar una característica de la configuración del equipo
    PROCEDURE pr_borrar_caract_config(p_equipo         IN fnx_equipos.equipo_id%TYPE,
                                                 p_tipo_elemid    IN fnx_equipos.tipo_elemento_id%TYPE,
                                                 p_tipo_elem      IN fnx_equipos.tipo_elemento%TYPE,
                                                 p_marca          IN fnx_equipos.marca_id%TYPE,
                                                 p_referencia     IN fnx_equipos.referencia_id%TYPE,
                                                 p_caracteristica IN fnx_configuraciones_equipos.caracteristica_id%TYPE,
                                                 p_mensaje        OUT VARCHAR2) IS
    BEGIN
        IF p_marca IS NOT NULL THEN
            -- Borrar la característica teniendo en cuenta la marca y la referencia.
            DELETE FROM fnx_configuraciones_equipos
             WHERE marca_id = p_marca
                AND referencia_id = p_referencia
                AND tipo_elemento_id = p_tipo_elemid
                AND tipo_elemento = p_tipo_elem
                AND equipo_id = p_equipo
                AND caracteristica_id = p_caracteristica;
        ELSE
            -- Borrar la característica, haciendo uso del serial del equipo, el tipo de elemento
            -- y el tpo de elemento ID.
            DELETE FROM fnx_configuraciones_equipos
             WHERE tipo_elemento_id = p_tipo_elemid
                AND tipo_elemento = p_tipo_elem
                AND equipo_id = p_equipo
                AND caracteristica_id = p_caracteristica;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := '<PKG_EQU> WO. Error borrando característica de equipo';
    END; -- FIN PR_BORRAR_CARACT_CONFIG

    -- PR_BORRAR_CONFIG
    -- Procedimiento para borrar la configuración de un equipo
    PROCEDURE pr_borrar_config(p_equipo      IN fnx_equipos.equipo_id%TYPE,
                                        p_tipo_elemid IN fnx_equipos.tipo_elemento_id%TYPE,
                                        p_tipo_elem   IN fnx_equipos.tipo_elemento%TYPE,
                                        p_marca       IN fnx_equipos.marca_id%TYPE,
                                        p_referencia  IN fnx_equipos.referencia_id%TYPE,
                                        p_mensaje     OUT VARCHAR2) IS
    BEGIN
        IF p_marca IS NOT NULL THEN
            -- Borrar las características de configuración del equipo, teniendo en cuenta
            -- la marca y la referencia.
            DELETE FROM fnx_configuraciones_equipos
             WHERE marca_id = p_marca
                AND referencia_id = p_referencia
                AND tipo_elemento_id = p_tipo_elemid
                AND tipo_elemento = p_tipo_elem
                AND equipo_id = p_equipo
                AND caracteristica_id NOT IN (200,
                                              347,
                                                        959,
                                                        2608,
                                                        3765,
                                                        3766,
                                                        3767,
                                                        1067,
                                                        1093,
                                                        4814);
        ELSE
            -- Borrar las características de configuración del equipo, haciendo uso del serial
            -- del equipo, el tipo de elemento y el tpo de elemento ID.
            DELETE FROM fnx_configuraciones_equipos
             WHERE tipo_elemento_id = p_tipo_elemid
                AND tipo_elemento = p_tipo_elem
                AND equipo_id = p_equipo
                AND caracteristica_id NOT IN (200,
                                              347,
                                                        959,
                                                        2608,
                                                        3765,
                                                        3766,
                                                        3767,
                                                        1067,
                                                        1093,
                                                        4814);
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := '<PKG_EQU> WO. Error borrando configuración de equipo';
    END; -- FIN PR_BORRAR_CONFIG 

    -- FN_VALOR_CONFIGURACION
    -- Función para obtener el valor de la configuración de una característica
    -- asociada a un equipo.
    FUNCTION fn_valor_configuracion(p_equipo           fnx_equipos.equipo_id%TYPE,
                                              p_tipo_elemento_id fnx_equipos.tipo_elemento_id%TYPE,
                                              p_tipo_elemento    fnx_equipos.tipo_elemento%TYPE,
                                              p_caracteristica   fnx_configuraciones_equipos.caracteristica_id%TYPE) RETURN VARCHAR2 IS
        v_valor fnx_configuraciones_equipos.valor%TYPE;
    BEGIN
        SELECT valor
          INTO v_valor
          FROM fnx_configuraciones_equipos
         WHERE tipo_elemento_id = p_tipo_elemento_id
            AND tipo_elemento = p_tipo_elemento
            AND equipo_id = p_equipo
            AND caracteristica_id = p_caracteristica;

        RETURN(v_valor);
    EXCEPTION
        WHEN no_data_found THEN
            RETURN NULL;
        WHEN OTHERS THEN
            RETURN NULL;
    END; -- FIN FN_VALOR_CONFIGURACION

    -- PR_CONFIG_TRABAJOS
    -- Procedimiento para configurar los trabajos que vienen en una solicitud
    -- de equipo.  Sí el tipo de trabajo es NUEVO, se inserta la característica en la
    -- configuración de equipo
    -- Sí el tipo de trabajo es RETIR, se borra la característica de la configuración
    -- de equipo
    PROCEDURE pr_config_trabajos(p_pedido        IN fnx_solicitudes.pedido_id%TYPE,
                                          p_subpedido     IN fnx_solicitudes.subpedido_id%TYPE,
                                          p_solicitud     IN fnx_solicitudes.solicitud_id%TYPE,
                                          p_equipo        IN fnx_equipos.equipo_id%TYPE,
                                          p_tipo_elemid   IN fnx_equipos.tipo_elemento_id%TYPE,
                                          p_tipo_elemento IN fnx_equipos.tipo_elemento%TYPE,
                                          p_mensaje       OUT VARCHAR2) IS
        -- Cursor para determinar los trabajos de la solicitud, de acuerdo al parametro tipo_trabajo
        CURSOR c_trabajos_solic(p_tipo_trabajo fnx_trabajos_solicitudes.tipo_trabajo%TYPE) IS
            SELECT tipo_trabajo,
                     caracteristica_id,
                     valor
              FROM fnx_trabajos_solicitudes
             WHERE pedido_id = p_pedido
                AND subpedido_id = p_subpedido
                AND solicitud_id = p_solicitud
                AND tipo_trabajo = p_tipo_trabajo
                AND caracteristica_id <> 1;

        v_equipo_marca      fnx_equipos.marca_id%TYPE;
        v_equipo_referencia fnx_equipos.referencia_id%TYPE;
        v_valor_caract      fnx_caracteristica_solicitudes.valor%TYPE;
    BEGIN
        -- Obtener la marca y la referencia del equipo a configurar
        pkg_equipos_util.pr_equipo_marca_refer(p_equipo,
                                                            p_tipo_elemento,
                                                            p_tipo_elemid,
                                                            v_equipo_marca,
                                                            v_equipo_referencia);

        IF v_equipo_marca IS NULL THEN
            p_mensaje := '<PKG_EQU> Error determinando marca y referencia de equipo';
        END IF;

        IF p_mensaje IS NULL THEN
            -- Configurar trabajos Nuevos de la solicitud de equipo
            FOR reg IN c_trabajos_solic('NUEVO') LOOP
                -- 2004-08-12
                -- Búsqueda de valor de característica en solicitudes
                v_valor_caract := nvl(pkg_solicitudes.fn_valor_caracteristica(p_pedido,
                                                                                                  p_subpedido,
                                                                                                  p_solicitud,
                                                                                                  reg.caracteristica_id),
                                             pkg_solicitudes.fn_valor_trabajo(p_pedido,
                                                                                         p_subpedido,
                                                                                         p_solicitud,
                                                                                         reg.caracteristica_id));

                BEGIN
                    -- Insertar el valor de la característica como configuración del equipo.
                    pkg_equipos_util.pr_insertar_conf_equ(v_equipo_marca,
                                                                      v_equipo_referencia,
                                                                      p_tipo_elemid,
                                                                      p_tipo_elemento,
                                                                      p_equipo,
                                                                      reg.caracteristica_id,
                                                                      v_valor_caract,
                                                                      p_mensaje);
                EXCEPTION
                    WHEN OTHERS THEN
                        p_mensaje := '<PKG_EQU>. Error configurando trabajos Nuevos de equipo';
                END;
            END LOOP;
        END IF;

        IF p_mensaje IS NULL THEN
            -- Configurar trabajos de Retiro de la solicitud de equipo
            FOR reg IN c_trabajos_solic('RETIR') LOOP
                IF reg.caracteristica_id NOT IN (218,
                                                            219,
                                                            220,
                                                            221,
                                                            222) THEN
                    -- Borrar la característica de configuración para el equipo
                    DELETE FROM fnx_configuraciones_equipos
                     WHERE marca_id = v_equipo_marca
                        AND referencia_id = v_equipo_referencia
                        AND tipo_elemento_id = p_tipo_elemid
                        AND tipo_elemento = p_tipo_elemento
                        AND equipo_id = p_equipo
                        AND caracteristica_id = reg.caracteristica_id;
                ELSE
                    -- Para el caso de los canales de televisión, la característica se actualiza a N.
                    UPDATE fnx_configuraciones_equipos
                        SET valor = 'N'
                     WHERE marca_id = v_equipo_marca
                        AND referencia_id = v_equipo_referencia
                        AND tipo_elemento_id = p_tipo_elemid
                        AND tipo_elemento = p_tipo_elemento
                        AND equipo_id = p_equipo
                        AND caracteristica_id = reg.caracteristica_id;
                END IF;
            END LOOP;
        END IF;
    END; -- Fin PR_CONFIG_TRABAJOS

    -- FN_ESTADO_EQUIPO ( 2003-12-16)
    -- Función para determinar el estado de un equipo, pasando como parámetros
    -- el serial, la marca y la referencia.
    -- 2013-11-22   Aarbob 
    FUNCTION fn_estado_equipo(p_equipo_id        IN VARCHAR2,
                                      p_marca            IN VARCHAR2,
                                      p_referencia       IN VARCHAR2,
                                      p_tipo_elemento_id IN VARCHAR2,
                                      p_tipo_elemento    IN VARCHAR2) RETURN VARCHAR2 IS
        v_estado_equipo fnx_equipos.estado%TYPE;
    BEGIN
        SELECT estado
          INTO v_estado_equipo
          FROM fnx_equipos
         WHERE marca_id = p_marca
            AND referencia_id = p_referencia
            AND tipo_elemento_id = p_tipo_elemento_id
            AND tipo_elemento = p_tipo_elemento
            AND equipo_id = p_equipo_id;

        RETURN v_estado_equipo;
    EXCEPTION
        WHEN no_data_found THEN
            RETURN 'NOK';
        WHEN OTHERS THEN
            RETURN 'NOK';
    END;

    -- PR_EQUIPO_SOLICITUD ( 2004-08-17 )
    -- Procedimiento para obtener los valores de serial, marca y referencia,
    -- asociados en una solicitud.
    PROCEDURE pr_equipo_solicitud(p_pedido     IN VARCHAR2,
                                            p_subpedido  IN NUMBER,
                                            p_solicitud  IN NUMBER,
                                            p_equipo     OUT fnx_solicitudes.equipo_id%TYPE,
                                            p_marca      OUT fnx_solicitudes.marca_id%TYPE,
                                            p_referencia OUT fnx_solicitudes.referencia_id%TYPE) IS
        CURSOR c_equipo_solicitud IS
            SELECT equipo_id,
                     marca_id,
                     referencia_id
              FROM fnx_solicitudes
             WHERE pedido_id = p_pedido
                AND subpedido_id = p_subpedido
                AND solicitud_id = p_solicitud;
    BEGIN
        OPEN c_equipo_solicitud;

        FETCH c_equipo_solicitud
            INTO p_equipo,
                  p_marca,
                  p_referencia;

        IF c_equipo_solicitud%NOTFOUND THEN
            p_equipo     := NULL;
            p_marca      := NULL;
            p_referencia := NULL;
        END IF;

        CLOSE c_equipo_solicitud;
    EXCEPTION
        WHEN OTHERS THEN
            CLOSE c_equipo_solicitud;
    END; -- Fin PR_Equipo_Solicitud

    -- FN_VALOR_CONFIGURACION SobreCarga
    -- Función para obtener el valor de la configuración de una característica
    -- asociada a un equipo, con los parámetros Marca, Referencia, Serial y Car.
    FUNCTION fn_valor_configuracion_mr(p_marca          fnx_equipos.marca_id%TYPE,
                                                  p_referencia     fnx_equipos.referencia_id%TYPE,
                                                  p_equipo         fnx_equipos.equipo_id%TYPE,
                                                  p_caracteristica fnx_configuraciones_equipos.caracteristica_id%TYPE) RETURN VARCHAR2 IS
        v_valor fnx_configuraciones_equipos.valor%TYPE;
    BEGIN
        SELECT valor
          INTO v_valor
          FROM fnx_configuraciones_equipos
         WHERE marca_id = p_marca
            AND referencia_id = p_referencia
            AND equipo_id = p_equipo
            AND caracteristica_id = p_caracteristica;

        RETURN(v_valor);
    EXCEPTION
        WHEN no_data_found THEN
            RETURN NULL;
        WHEN OTHERS THEN
            RETURN NULL;
    END; -- FIN FN_VALOR_CONFIGURACION

    -- FN_IDENTIFICADOR
    -- Función para obtener el identificador asociado a un equipo
    FUNCTION fn_identificador(p_marca      fnx_equipos.marca_id%TYPE,
                                      p_referencia fnx_equipos.referencia_id%TYPE,
                                      p_equipo     fnx_equipos.equipo_id%TYPE) RETURN VARCHAR2 IS
        v_identificador fnx_equipos.identificador_id%TYPE;
    BEGIN
        SELECT identificador_id
          INTO v_identificador
          FROM fnx_equipos
         WHERE marca_id = p_marca
            AND referencia_id = p_referencia
            AND equipo_id = p_equipo;

        RETURN(v_identificador);
    EXCEPTION
        WHEN no_data_found THEN
            RETURN NULL;
        WHEN OTHERS THEN
            RETURN NULL;
    END; -- FIN FN_IDENTIFICADOR

    -- FN_AGRUPADOR
    -- Función para obtener el identificador asociado a un equipo
    FUNCTION fn_agrupador(p_marca      fnx_equipos.marca_id%TYPE,
                                 p_referencia fnx_equipos.referencia_id%TYPE,
                                 p_equipo     fnx_equipos.equipo_id%TYPE) RETURN VARCHAR2 IS
        v_agrupador fnx_equipos.agrupador_id%TYPE;
    BEGIN
        SELECT agrupador_id
          INTO v_agrupador
          FROM fnx_equipos
         WHERE marca_id = p_marca
            AND referencia_id = p_referencia
            AND equipo_id = p_equipo;

        RETURN(v_agrupador);
    EXCEPTION
        WHEN no_data_found THEN
            RETURN NULL;
        WHEN OTHERS THEN
            RETURN NULL;
    END; -- FIN FN_AGRUPADOR

    -- PR_EQUIPO_LIB_TERMINALES  <SobreCargado I>
    -- Procedimiento para liberar los terminales ocupados de un equipo y que se encuentran asociados a un
    -- identificador y un equipo determinado, con paràmetros de tipo de elemento y tipo de elemento ID
    PROCEDURE pr_equipo_lib_terminales(p_equipo           IN fnx_equipos.equipo_id%TYPE,
                                                  p_tipo_elemento_id IN fnx_equipos.tipo_elemento_id%TYPE,
                                                  p_tipo_elemento    IN fnx_equipos.tipo_elemento%TYPE,
                                                  p_identificador    IN fnx_identificadores.identificador_id%TYPE,
                                                  p_mensaje          OUT VARCHAR2) IS
    BEGIN
        UPDATE fnx_terminales_equipos
            SET estado           = 'LIB',
                 identificador_id = NULL,
                 agrupador_id     = NULL
         WHERE equipo_id = p_equipo
            AND tipo_elemento_id = p_tipo_elemento_id
            AND tipo_elemento = p_tipo_elemento
            AND identificador_id = p_identificador;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := '<PKG_EQU> Error liberando terminales ocupadas: ' || p_tipo_elemento_id || '-' || p_tipo_elemento || '-' || p_equipo;
    END pr_equipo_lib_terminales;

    -- PR_EQUIPO_LIB_TERMINALES  <SobreCargado II>
    -- Procedimiento para liberar los terminales ocupados de un equipo y que se encuentran asociados a un
    -- identificador y un equipo determinado, con paràmetros de marca y referencia
    PROCEDURE pr_equipo_lib_terminales(p_marca         IN fnx_equipos.marca_id%TYPE,
                                                  p_referencia    IN fnx_equipos.referencia_id%TYPE,
                                                  p_equipo        IN fnx_equipos.equipo_id%TYPE,
                                                  p_identificador IN fnx_identificadores.identificador_id%TYPE,
                                                  p_mensaje       OUT VARCHAR2) IS
    BEGIN
        UPDATE fnx_terminales_equipos
            SET estado           = 'LIB',
                 identificador_id = NULL,
                 agrupador_id     = NULL
         WHERE marca_id = p_marca
            AND referencia_id = p_referencia
            AND equipo_id = p_equipo
            AND identificador_id = p_identificador;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := '<PKG_EQU> Error liberando terminales ocupadas';
    END pr_equipo_lib_terminales;

    -- PR_EQUIPO_TERMINAL_ESTADO
    -- Procedimiento para actualizar el estado de una terminal asociada a un equipo, el identificador asociado
    -- el agrupador
    PROCEDURE pr_equipo_terminal_estado(p_marca         IN fnx_marcas_referencias.marca_id%TYPE,
                                                    p_referencia    IN fnx_marcas_referencias.referencia_id%TYPE,
                                                    p_equipo        IN fnx_equipos.equipo_id%TYPE,
                                                    p_terminal      IN fnx_terminales_equipos.terminal_id%TYPE,
                                                    p_estado        IN fnx_equipos.estado%TYPE,
                                                    p_identificador IN fnx_equipos.identificador_id%TYPE,
                                                    p_agrupador     IN fnx_equipos.agrupador_id%TYPE,
                                                    p_mensaje       OUT VARCHAR2) IS
    BEGIN
        UPDATE fnx_terminales_equipos
            SET estado           = p_estado,
                 identificador_id = p_identificador,
                 agrupador_id     = p_agrupador
         WHERE equipo_id = p_equipo
            AND marca_id = p_marca
            AND referencia_id = p_referencia
            AND terminal_id = p_terminal;
    EXCEPTION
        WHEN OTHERS THEN
            p_mensaje := NULL;
    END; -- Fin PR_EQUIPO_TERMINAL_ESTADO
END; -- Fin Body Paquete Pkg_Telev_Util