PROCEDURE       PR_ENRUTAR_TELEV (
   w_pedido                 IN       fnx_pedidos.pedido_id%TYPE,
   w_resp                   IN OUT   VARCHAR2
) IS

-- HISTORIA DE MODIFICACIONES
-- Fecha        Autor       Observaciones
-- ----------   ----------- -----------------------------------------------------------------
-- 2006-07-28   Jrincong    Actualizacion de campo identificador_id_nuevo en solicitudes de
--                          Equipo.
-- 2006-07-31   Jrincong    Parametrizacion de cursores.
-- 2006-07-31   Jrincong    Insercion de caracteristicas 1 y 90 en solicitudes de componente.
-- 2006-08-02   Jrincong    Consideracion de tipo elemento ID PayPerView PPV, en
--                          cursores para no realizar enrutamiento.
-- 2006-08-23   Jrincong    Cambio en cursor c_solic_srv, para verificar que la solicitud de
--                          servicio que viene en el pedido es de tipo NUEVO.  Esto es para
--                          que se puedan enrutar las solicitudes de DECO y Canales Empaquetados.
-- 2006-08-23   Jrincong    Actualizacion de campo identificador_id_nuevo, estado y concepto en las
--                          otras solicitudes de Equipos del subpedido.
-- 2006-08-23   Jrincong    Determinar si en el subpedido viene un identificador de paquete y asi
--                          establecer el concepto de la solicitud que genera las ordenes
-- 2006-09-11   Jrincong    Consideracion de concepto PRACC en cursores de instrucciones para
--                          actualizacion de decodificadores y componentes.
-- 2006-11-19   Jrincong    Consideracion de tipo elemento ID PayPerView PPV, en
--                          cursores para realizar enrutamiento.
-- 2006-11-19   Jrincong    Se quita condicion AND tipo_elemento_id <> 'PPV' de los cursores
--                          c_solic_cmp_equ y c_solic_cmp
-- 2006-11-19   Jrincong    Seccion Construir identificador de componente.  Para el caso
--                          de PPV se debe hace un conteo de PayPerViews existentes.
-- 2006-11-19   Jrincong    Seccion Determinar concepto de solicitud. Se pasa para dentro
--                          del ciclo del cursor c_solic_cmp. Es necesario evaluar cada
--                          componente para determinar si queda en PORDE, POPTO o PINSC.
-- 2006-11-19   Jrincong    Se considera el concepto PETEC en cursores de componentes, debido
--                          a que el PPV ingrese en tal concepto.
-- 2007-01-16   Jrincong    Verificacion del tipo de subpedido, para asignar variable
--                          v_identificador_nuevo.
-- 2007-01-16   Jrincong    Consideracion de tipo de subpedido RETIR para no insertar
--                          caracteristicas 1 y 90, ni crear identificador de componente.
-- 2007-01-16   Jrincong    Creacion identificador cmp.  Manejo excepcion WDVOnIndex,
--                          para presupuestar el identificador.
-- 2007-01-16   Jrincong    Concepto PPV. Se verifica si existen equipos en el subpedido.
--                          Si no existen, el concepto de la solicitud es PORDE.
-- 2007-04-30   Jrincong    TV ADSL Consideración elemento EQURED ( STBOX ) en instrucciones de
--                          Equipo, para el enrutamiento de solicitudes.
-- 2007-05-07   Jrincong    Búsqueda de característica de tecnología 207 en el identificador
--                          del subpedido, en caso de que sea nula.
-- 2007-05-11   Jrincong    Inclusión de tipo elemento STBOX para enrutamiento de solicitudes
--                          de retiro de equipo Set Top Box.
-- 2007-05-11   Jrincong    Actualización Equipos.  Se actualiza campo identificador_id en
--                          lugar de Identificador_Id_Nuevo.
-- 2007-05-31   Jrincong    Verificar sí en el subpedido viene una solicitud de Televisión donde
--                          se solicita un cambio de extensiones.
-- 2007-09-10   Jrincong    Proc. pkg_pedidos_util.pr_insupd_caracteristica. Adición campos
--                          municipio_id, empresa_id
-- 2007-09-10   Jrincong    Condición para insertar o actualizar característica 90 en transacción
--                          CAMBIo
-- 2007-09-12   Jrincong    Inserción / actualización característica 90 en solicitudes de EQU
-- 2008-03-22   Aacosta     Consideraciòn de la tecnología asociada al componente INSHFC para IPTV.
-- 2008-03-22   Aacosta     Creación del cursor c_cmp_acceso, con el fin de determinar si el agrupador del servicio
--                          tiene asociados componentes de IPTV.
-- 2008-03-26   Aacosta     Modificación de cursor c_soli_cmp con el fin de agregar la nueva condición en el where
--                          tipo_elemento_id NOT IN ('INSHFC','INSIP'), con el fin que no se estudien estos componentes
--                          al momento de el ingreso de peticiones de retiro.
-- 2008-03-27   Aacosta     Modificación de la condición para que se provisionen componentes y equipos
--                          para peticiones.
-- 2008-07-16   Aacosta     Se comentarea la linea AND    b.tipo_solicitud NOT IN ('NUEVO','RETIR') del cursor c_solic_cmp_equ
--                          Esto debido que no se están enrutando correctamente los equipos asociados al pedido.
-- 2008-07-26   Aacosta     Validación asociada a equipos de televisión que vengan en la solicitud. Si viene uno
--                          y en el mismo pedido viene una solicitud de componente INSIP o INSHFC no se debe actualizar
--                          el estado-concepto de la solicitud del equipo.
-- 2008-07-26   Aacosta     Creación del cursor c_cmp_nuevo_telev con el fin de verificar antes de actualizar la solicitud de
--                          equipos Nuevos, si viene un componente ya sea INSHFC o INSIP NUEVO. caso de venir no se debe
--                          actualizar la solicitud del equipo.
-- 2008-07-30   Aacosta     Modificación del cursro c_cmp_nuevo_telev, con el fin de validar no solo si viene algún componente
--                          INSHFC o INSIP NUEVO, sino tambien algún RETIR. Se modifica la linea AND    b.tipo_solicitud = 'NUEVO'
--                          por AND    b.tipo_solicitud IN ('NUEVO','RETIR').
-- 2008-09-09   Aacosta     Se incluye la validación del tipo de solicitud en reportes instalación
--                          para los casos de PPV en los cuales en fnx_subpedidos el tipo de subpedido
--                          viene como CAMBI, y no permite la provisión de este componente.
-- 2008-12-24   Aacosta     Validar para el componente EQURED la característica 1067. Para el caso que sea un
--                          equipo SET UP BOX de alta definición la solicitud debe quedar PETEC, caso de no serlo
--                          se debe comportar igual como se hace actualmente.
-- 2009-02-13   Aacosta     Creación del cursor c_tipo_elemen_id_equ, con el fin de poder obtener el tipo elemento_id del equipos.
--                          se hace la validación que para equipos de tipo DECO la tecnología sea HFC.
--2009-06-19    Lquintez    se coloca la condición de que si hay cambio de tipo de televisión no
--                          pase el INSIP o el INSHFC a PORDE
--2009-11-24    Jsierrao    Se modifica el tipo de variable para v_cambio_plan_telev ya que se encuentra como tipo_trabajo y debe ser como tipo valor
--2010-01-13    Wmendoza    Se modifica la instrucción v_concepto_ordenes := 'POPTO';
--                          para el tema de STBOX de Alta definición. Se coloca en PINSC el SERVIP
-- 2010-02-04   Jaristz     Cuando el tipo de solicitus sea CAMBI el concepto del cmp SERVIP debe ser POPTO.
-- 2010-02-04   Jaristz     Se tiene en cuenta que cuando el tipo de solicitud sea un CAMBI los STBOX y EQURED pasen a PORDE.
-- 2010-02-08   Aacosta     Para obtener la tecnología que se debe asociar al componente o al equipo, se debe
--                          consultar la tecnología del componente INSIP o INSHFC en la característica 207 de
--                          estos componentes.
-- 2010-02-11   Jaristz     Se Obtiene en la caracteristica 1067 si el tipo equipo es un SWITCH el concepto de esa soliictud pase a POPTO.
-- 2010-02-11   Jaristz    Si el componente SERVIP viene solo en el pedido este debe pasar a PORDE, en caso contrario POPTO.
-- 2010-04-29   Wmendoza    Cursor c_solic_acceso_cmp para realizar el ajuste de cambio de hfc a MIXTO
-- 2010-04-29   Wmendoza    Se abre el cursor para realizar ajuste al tema de cambio de HFC a mixto.
-- 2010-04-29   Wmendoza    Si el cursor devuelve 0, entonces se garantiza que no es un cambio HFC A MIXTO BÁSICO
--                          para tal fin, se asigna la condicion AND w_acceso_cmp_nuevo = 0
-- 2010-07-22   Wmendoza    Se verifica si el equipo que viene, corresponde a un set up box de alta definición.
-- 2010-07-22   Wmendoza    Se quita la condición
--                          IF NVL(v_tipo_equipo, 'NING') = 'STBAD' THEN  EXIT; END IF;
--                          Para poder validar con la variable  v_tiene_stboxhd.
-- 2010-08-13   Aacosta     Se comenta lógica correspondiente al enrutamiento de tema de set up box.
--             /Wmendoza    Para poder realizarlo de manera genérica.
-- 2010-08-13   Aacosta     Se crea lógica a Fin de poder colocar genérica la manera en la que se deben enrutar.
--             /Wmendoza    los STBOX HD, teniendo cuenta las peticiones que se puedan presentar.
-- 2010-08-31   Wmendoza    Se cambia la condición IF reg.tipo_solicitud IN ('CAMBI','NUEVO') THEN
--                          por la condición IF reg.tipo_solicitud ='NUEVO' THEN de acuerdo al requerimiento 1371892
--
-- 2010-08-31   Wmendoza    Se cambia el concepto a POPTO de acuerdo a requerimiento 1371892.
-- 2010-09-01   Wmendoza    Se debe agregar una validación considerando el tipo de cambio de equipo que se va a realizar.
-- 2011-05-25   ETachej     Integridad Fenix OPEN - La solicitud de retiro de DECo debe quedar PORDE.
-- 2011-08-04   Wmendoza    Para el tipo de elemento ELSIS, la tecnología que se debe
--                          trabajar es LOGIC.
-- 2011-09-20   Wmendoza    En caso de no ser un pedido de tv digital, se hace lo que actualmente,
--                          realiza el procedimiento, en caso contrario, se colocan en PORDE todos los equipos.
-- 2011-09-26   Wmendoza    Solo se pasan a POPTO los demas equipos, cuando se trate
--                          de un pedido que no sea televisión digital HFC.
-- 2011-10-11   Wmendoza    Si es retiro de un EQURED debe quedar en PORDE
-- 2011-10-11   Wmendoza    Si además se está retirando un equipo
--                          se debe colocar en POPTO.
-- 2011-10-24   Wmendoza    Verificación de la solicitud de equipos de TV digital
--                          se colocan en PORDE aquellos equipos de tv digital. Los demás quedan en PETEC.
--                          para el reenvío de componentes.
-- 2011-11-24   Wmendoza    Se crea condición para el tipo de elemento que venga en
--                          la solicitud.
-- 2011-11-28   Wmendoza    Se verifica el valor del trabajo para ver si se trata de una migración.
-- 2011-11-28   Wmendoza    Si es una migración con el EQURED, la tecnología debe ser HFC.
-- 2011-11-28   Wmendoza    En caso de ser un SERHFC la tecnología es HFC, en caso de
--                          ser un SERVIP, la tecnología deber ser REDCO.
-- 2011-12-22   Aacosta     Se valida que si no viene equipo y viene solo el componente SERVIP o SERVHFC
--                             la solicitud debe quedar PORDE.
-- 2012-01-16   Wmendoza    Solo pasa a PORDE, si no vienen equipos.
-- 2012-04-26   Wmendoza    Se verifica los trabajos para PPV, SERHFC y EQU
--                          en caso de estar retirándolos, se debe colocar en PORDE el SERHFC.
-- 2012-04-26   Wmendoza    Cursor para retiros de DECOS. Con este cursor se pretende poder
--                          colocar en PORDE el componente de canales cuando viene un PPV.
-- 2012-05-02   Wmendoza    Se verifica los trabajos para PPV, SERHFC y EQU
--                          en caso de estar retirándolos, se debe colocar en PORDE el SERHFC.
--                          y los equipos que traiga en PORDE.
-- 2012-05-14   Wmendoza    Se agrega la condición para colocar la tecnología
--                          de la solicitud de equipo.
-- 2012-06-29   Aacosta     Se incluye el tipo de trabajo NUEVO para peticion de cambio o adicion de canales HD.
-- 2012-07-25   Wmendoza    Se agrega condición para el subpedido que se está verificando.
--                          Se modifica para el requerimiento de demostraciones.
-- 2012-07-30   Wmendoza    Además de ser una migración, debería tratarse de un pedido de TV digital.
-- 2012-08-23   Wmendoza    Se crea cursor para verificar el tipo de tecnología, esto pasa
--                          cuando es un pedido mixto que tienen televisión hfc digital.
-- 2012-08-28   Wmendoza    Se actualiza la tecnología de la solicitud para tener
--                          en cuenta los equipos que no son HFC.
-- 2012-09-13   DGiralL     No actualizar a PORDE las solicitudes de cambio de equipo de TV de convencional a digital
--                          cuando el pedido es de tecnologia Red de Cobre y los equipos son de alta definicion
-- 2012-10-11   Wmendoza    Cursores para el tema de siembra digital.
-- 2012-10-11   Wmendoza    Se realiza el ajuste relacionado con el tema de siembra digital.
-- 2012-10-16   Wmendoza    Ajuste para el tema de siembra digital.
-- 2012-10-16   Wmendoza    En caso de tratarse un pedido de siembra digital, sin equipos
--                          el SERHFC o el SERVIP pasan a PORDE.
-- 2012-11-08   Yhernana    EstabilizDesaprov: generación procedimiento interno encargado de insertar caracteristicas necesarias
--                          para el Desaprovisionamiento de Equipos.Aplica para Retiro de Canales que tiene asociado el retiro de Decodificadores.
-- 2012-11-20   Yhernana    EstabilizDesaprov: Se invoca procedimiento PKG_PEDIDOS_UTIL.PR_INSUPD_CARACTERISTICA,
--                          en vez de PKG_PEDIDOS_UTIL.PR_CREAR_CARACTERISTICA
-- 2012-11-20   Yhernana    EstabilizDesaprov: Creación procedimiento interno encargado de insertar registros en FNX_TRABAJOS_SOLICITUDES.
--                          En caso de presentar error en la inserción por registro duplicado se realiza un Update sobre la misma tabla.
-- 2012-11-20   Yhernana    EstabilizDesaprov: Se invoca procedimiento interno encargado de insertar registros
-- 2013-01-24   Mmedinac    Req20216: Mejora Superplay Adicion función fn_Verificar_PendET y procedimiento pr_act_est_ETpquete para identificar
--                          si existen subpedidos pendientes de estudio técnico en el paquete y evitar que los otros subpedidos se vayan a PORDE
-- 2013-02-19   Yhernana    Antes de actualizar la solicitud a un concepto de órdenes, se hace necesario validar que las características  
--                          de Desaprovisionamiento de Equipos se encuentren insertadas en los pedidos de Retiros.  
-- 2013-02-22   Yhernana    Se comenta la linea del estado, ya que en ocasiones no viene con valor OCU.
-- 2013-04-02   Sbuitrab    Las solicitudes de retiros de equipos ANALOGOS deben pasar a PORDE
-- 2013-04-25   Jrincong    Req 24541 - Autoinstalación canales Adicionales
--                          En caso que no haya un valor para la variable v_tipo_elemen_id_sw.identificador_id se asigna el valor v_identificador_nuevo
--                          que contiene el identificador del subpedido.  Cuando el identificador viene nulo en la solicitud, 
--                          la consulta fallaba y se sobreescribían los datos de marca y referencia.
-- 2013-05-08   eaguirrg    REQ24324_MejoDemoTvAtc. Se modifica el procedimiento para que cuando un cliente realice una aceptación parcila de canales
--                          de demostracion la solicitud de adicion de los canales pase a estado ORDEN/PINSC y de esta manera quede en esspera esta
--                          solicitud hasta que se cumpla la solicitud de retiro de canales.
-- 2013-05-23   Yhernana    Modificación pri_insertar_carac_desaprov: Se insertan las características 90 (Identificador Asociado), 
--                          347 (Tipo Equipo), 1067 (Tipo de Equipo) y 1093 (Propiedad Equipo) en FNX_CARACTERISTICA_SOLICITUDES 
-- 2013-05-31   eaguirrg    REQ24324_MejoDemoTvAtc. En las pruebas se detecta un error en el cursor c_equipos_solicitud, se incluye para que lo evalue
--                          tambien con el subpedido_id.
-- 2013-06-25    Jrincong   Cursores C_SIEMDIG / C_EQUIPOSC_SIEM.  En estos cursores se cambia la variable para la búsqueda por pedidos. Se utiliza
--                          w_pedido que hace referencia específica al pedido para el cual se está realizando el Estudio Técnico. Antes
--                          se utiliza el valor pedido_id = pedido_id y esto hace que la búsqueda se haga en toda la tabla.
--                          Esto generaba tiempos muy largos en el proceso de Estudio Técnico, causando bloqueos frecuentes.
--
-- 2013-09-02    aarbob     REQ28278_Tangibilizacion. Búsqueda de la propiedad del equipo para determinar estado RES en caso que sea CM. 
-- 2013-09-19   Wmendoza    Si es una adición del ELSIS para TELEV entonces, la solicitud debe quedar PORDE.

--2013-09-20    aarbob      Se realiza modificaciones para garantizar que cuando el equipo sea comprado 1093-propieda equipo "CM" el equipo quede en 
--                          estado - concepto CUMPL-CUMPL, no se debe generar ordenes , se actualiza el estado del equipos a RES, la Solictud del componente debe quedar en estado ORDEN_PORDE
--                          cuando sea un equipo comprado 
--                          REQ28278_Tangibilizacion
--- 2014-01-15  lhernans    Automatización cola DSLAM. Lógica para inserción de característica para generar evento.
-- 2014-01-27   mcastaa     INC2166560_ProvCanaHDIPTV  Validar si el pedido tiene la caracteristica_id 200 y el tipotrabajo es RETIR 
--                          entre a validar que las característicasde Desaprovisionamiento de Equipos se encuentren insertadas en los pedidos de Retiros.
-- 2014-02-04   aarbob      REQ34242_MejoTang. Búsqueda de la propiedad del equipo para determinar estado RES  ( Retiro ) 
-- 2014-04-11   Jrincong    REQ 36900. GPON Hogares
--                          Cursor c_carac.  Se utiliza parámetro p_caracteristica.  Antes estaba quemada como 216 dentro de la consulta.
-- 2014-04-11               [COMMENT] Revisar vigencia de cursor c_equ.  El cursor hace referencia a valor IP en característica 33.                            
-- 2014-04-11   Jrincong    REQ 36900 GPON Hogares
--                          Se cambia búsqueda de característica 216 por caracteristica 33 con cursor c_carac.  La característica 216 ya 
--                          no se utiliza para definir la tecnología y no está configurada para elementos EQU / SERHFC / SERVIP 
--                          Buscar el valor de la caracteristica tipo Tecnologia TV.
--                          C_CARAC.  Se cambia referencias a variable v_solic_cmp variable v_subped.            
-- 2014-04-11   Jrincong    REQ 36900 GPON Hogares
--                          [Condicion: v_tipo_equ_sw <> 'SWITCH' OR b_retir_deco] Se agrega la tecnología GPON para considerar Enrutamiento de Equipos.
-- 2014-04-11   Jrincong    REQ 36900 GPON Hogares
--                          [Ciclo: FOR v_tipo_elemen_id_sw IN c_tipo_elemen_id_sw] Se agrega la tecnología GPON para considerar Enrutamiento de Equipos.
-- 2014-04-11   Jrincong    REQ 36900 GPON Hogares
--                            [IF NOT b_pedido_tvdigital] Se agrega la tecnología GPON para considerar el Enrutamiento de Equipos.
-- 2014-10-15   MGATTIS     Automatización Cola DSLAM ITPAM25626_AutoColaDSLA. Actualización Lógica para inserción de etiquetas para generar eventos.
-- 2014-10-22   RVELILLR    Automatización Cola DSLAM ITPAM25626_AutoColaDSLA. Condicional de tecnologia para consulta de etiquetas 4805 para generar eventos.
                                                     
   w_mensaje_error              VARCHAR2 (100);
   w_incons                     BOOLEAN;
   w_tipo_elemento              FNX_SOLICITUDES.tipo_elemento%TYPE;
   p_mensaje                    VARCHAR2(50);
   v_concepto_solicitud         FNX_SOLICITUDES.concepto_id%TYPE;
   v_concepto_ordenes           FNX_SOLICITUDES.concepto_id%TYPE;
   v_identificador_pack         FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;
   b_orden_generada             BOOLEAN := FALSE;
   v_identificador_cmp          FNX_SOLICITUDES.identificador_id%TYPE;
   v_identificador_nuevo        FNX_SOLICITUDES.identificador_id%TYPE;
   v_total_payperviews          NUMBER(2);
   --2013-01-28: MMedinac: Req20216: Línea adicionada, variable para determinar si hay subpedidos del paquete trio superplay pendientes 
   p_SubPendi                   BOOLEAN := TRUE;
   --2013-01-28: MMedinac: Req20216: Línea adicionada, variable que guardará si el paquete que se esta trabajando es Trio Superplay 
   bEsOfertaEspecifica                 BOOLEAN := FALSE;    
   --2013-01-28: MMedinac: Req20216: Línea adicionada, variable para guardar el tipo elemento id de la solicitud actual
   vTipoElementoID              FNX_SOLICITUDES.tipo_elemento_id%TYPE;
   --2013-09-20    aarbob REQ28278_Tangibilizacion
   v_propiedadEqu  FNX_CONFIGURACIONES_EQUIPOS.VALOR%TYPE;
   v_tipo_trabajo FNX_TRABAJOS_SOLICITUDES.TIPO_TRABAJO%TYPE;
   v_men          varchar2(1000);  
   
   CURSOR c_equipo_carac (
            p_identificador_asociado IN FNX_IDENTIFICADORES.identificador_id%TYPE,
            pp_equipo_id             IN FNX_EQUIPOS.EQUIPO_ID%TYPE
        ) IS
            SELECT marca_id, referencia_id, tipo_elemento_id, tipo_elemento
              FROM fnx_equipos
             WHERE identificador_id = p_identificador_asociado
               AND equipo_id        = pp_equipo_id;
               --AND estado           = 'OCU';
   v_carac_equipo         c_equipo_carac%ROWTYPE;  
   
   CURSOR c_subped IS
   SELECT pedido_id, subpedido_id, identificador_id, tipo_subpedido
   FROM   FNX_SUBPEDIDOS
   WHERE  pedido_id   = w_pedido
   AND    servicio_id = 'ENTRE'
   AND    producto_id = 'TELEV';

   v_subped          c_subped%ROWTYPE;

   w_cuenta_srv_nuevo      NUMBER(2);

   CURSOR c_solic_srv ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                        p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                      ) IS
   SELECT COUNT (*)
   FROM   FNX_SOLICITUDES A,
          FNX_REPORTES_INSTALACION B
   WHERE  a.pedido_id     = p_pedido
   AND    a.subpedido_id  = p_subpedido
   AND    a.pedido_id     = b.pedido_id
   AND    a.subpedido_id  = b.subpedido_id
   AND    a.solicitud_id  = b.solicitud_id
   AND    a.tipo_elemento = 'SRV'
   AND    a.servicio_id = 'ENTRE'
   AND    a.producto_id = 'TELEV'
   AND    b.tipo_solicitud = 'NUEVO';

--Cursor c_solic_acceso_cmp para realizar el ajuste de cambio de hfc a MIXTO

   w_acceso_cmp_nuevo      NUMBER(2);
   CURSOR c_acceso_cmp_nuevo    ( p_pedido           IN  FNX_SOLICITUDES.pedido_id%TYPE,
                                 p_subpedido        IN  FNX_SOLICITUDES.subpedido_id%TYPE)
   IS
   SELECT COUNT (*)
   FROM   FNX_SOLICITUDES A,
          FNX_REPORTES_INSTALACION B
   WHERE  a.pedido_id     = p_pedido
   AND    a.subpedido_id  = p_subpedido
   AND    a.pedido_id     = b.pedido_id
   AND    a.subpedido_id  = b.subpedido_id
   AND    a.solicitud_id  = b.solicitud_id
   AND    a.tipo_elemento = 'CMP'
   AND    a.servicio_id   = 'ENTRE'
   AND    a.producto_id   = 'TELEV'
   AND    b.tipo_solicitud = 'NUEVO'
   AND    a.tipo_elemento_id IN ('INSHFC','INSIP');



   CURSOR c_solic_cambi_srv ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                              p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                            ) IS
   SELECT A.solicitud_id
   FROM   FNX_SOLICITUDES A,
          FNX_REPORTES_INSTALACION B
   WHERE  a.pedido_id     = p_pedido
   AND    a.subpedido_id  = p_subpedido
   AND    a.pedido_id     = b.pedido_id
   AND    a.subpedido_id  = b.subpedido_id
   AND    a.solicitud_id  = b.solicitud_id
   AND    a.tipo_elemento = 'SRV'
   AND    a.servicio_id   = 'ENTRE'
   AND    a.producto_id   = 'TELEV'
   AND    a.estado_id     = 'TECNI'
   AND    a.concepto_id   = 'PETEC'
   AND    b.tipo_solicitud = 'CAMBI';

   v_solicitud_cambio_srv      NUMBER(2);

   v_cuenta_cmp_equ    NUMBER(2);

   CURSOR c_solic_cmp_equ ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                            p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                        ) IS
   SELECT COUNT(*)
   FROM   FNX_SOLICITUDES A,
          FNX_REPORTES_INSTALACION B
   WHERE  a.pedido_id    = p_pedido
   AND    a.subpedido_id = p_subpedido
   AND    a.pedido_id    = b.pedido_id
   AND    a.subpedido_id = b.subpedido_id
   AND    a.solicitud_id = b.solicitud_id
   AND    a.producto_id  = 'TELEV'
   AND    a.tipo_elemento_id NOT IN ('INSHFC','INSIP')
   AND    a.tipo_elemento IN ('CMP', 'EQU')
   AND    a.estado_id IN ('TECNI', 'RUTAS');

   CURSOR c_solic_cmp ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                        p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                        ) IS
   SELECT a.pedido_id, a.subpedido_id, a.solicitud_id
          ,a.servicio_id, a.producto_id, a.tipo_elemento_id, a.tipo_elemento
          ,a.municipio_id, a.empresa_id, b.tipo_solicitud,a.identificador_id --2013-05-08 Inicio Cambios
   FROM   FNX_SOLICITUDES A,
          FNX_REPORTES_INSTALACION B
   WHERE  a.pedido_id     = p_pedido
   AND    a.subpedido_id  = p_subpedido
   AND    a.pedido_id     = b.pedido_id
   AND    a.subpedido_id  = b.subpedido_id
   AND    a.solicitud_id  = b.solicitud_id
   AND    a.producto_id   = 'TELEV'
   AND    a.tipo_elemento = 'CMP'
   AND    a.tipo_elemento_id NOT IN ('INSHFC','INSIP')
   AND    a.concepto_id   IN ('PRUTA', 'PRACC', 'PETEC');

   v_solic_cmp       c_solic_cmp%ROWTYPE;

   CURSOR c_total_cmp ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                        p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                      ) IS
   SELECT COUNT (*)
   FROM   FNX_SOLICITUDES
   WHERE  pedido_id     = p_pedido
   AND    subpedido_id  = p_subpedido
   AND    producto_id   = 'TELEV'
   AND    tipo_elemento = 'CMP'
   AND    concepto_id   IN ('PRUTA', 'PRACC', 'PETEC');

   w_cuenta_cmp      NUMBER(2);

   CURSOR c_total_equ ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                        p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                      ) IS
   SELECT COUNT (*)
   FROM   FNX_SOLICITUDES
   WHERE  pedido_id     = p_pedido
   AND    subpedido_id  = p_subpedido
   AND    producto_id   = 'TELEV'
   AND    tipo_elemento = 'EQU'
   AND    concepto_id   IN ('PETEC', 'PRACC');

   w_cuenta_equ      NUMBER(2);

   CURSOR c_ident  (p_identificador  IN FNX_SUBPEDIDOS.identificador_id%TYPE)
   IS
   SELECT identificador_id, serie_id
         ,servicio_id, producto_id, tipo_elemento_id, tipo_elemento
         ,tecnologia_id
   FROM   FNX_IDENTIFICADORES
   WHERE  identificador_id = p_identificador;

   v_ident          c_ident%ROWTYPE;
   v_tecnologia     FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE;

   -- 2014-04-11. REQ 36900.
   -- Cursor c_carac.  Se utiliza parámetro p_caracteristica.  Antes estaba quemada como 216 dentro de la consulta.

   CURSOR c_carac ( p_pedido         IN  FNX_SOLICITUDES.pedido_id%TYPE,
                    p_subpedido      IN  FNX_SOLICITUDES.subpedido_id%TYPE,
                    p_solicitud      IN  FNX_SOLICITUDES.solicitud_id%TYPE,
                    p_caracteristica IN  FNX_CARACTERISTICA_SOLICITUDES.caracteristica_id%TYPE
                   ) IS
   SELECT valor
   FROM   FNX_CARACTERISTICA_SOLICITUDES
   WHERE  pedido_id    = p_pedido
   AND    subpedido_id = p_subpedido
   AND    solicitud_id = p_solicitud
   AND    caracteristica_id = p_caracteristica;

   -- Cursor para contar el numero de identificadores PayPerView asociados al SeRVicio
   CURSOR c_conteo_payperviews  ( p_agrupador  IN FNX_IDENTIFICADORES.agrupador_id%TYPE)
   IS
   SELECT COUNT(*)
   FROM   FNX_IDENTIFICADORES
   WHERE  servicio_id = 'ENTRE'
   AND    producto_id = 'TELEV'
   AND    tipo_elemento_id = 'PPV'
   AND    tipo_elemento = 'CMP'
   AND    agrupador_id  = p_agrupador
   AND    estado        IN ('OCU', 'PRE', 'XLI');

   CURSOR c_cmp_acceso (p_agrupador IN FNX_IDENTIFICADORES.agrupador_id%TYPE)
   IS
   SELECT COUNT(*)
   FROM fnx_identificadores
   WHERE agrupador_id     = p_agrupador
   AND   servicio_id      = 'ENTRE'
   AND   producto_id      = 'TELEV'
   AND   tipo_elemento_id IN ('INSHFC','INSIP')
   AND   estado           = 'OCU';

   CURSOR c_cmp_nuevo_telev ( p_subpedido      IN FNX_SUBPEDIDOS.subpedido_id%TYPE)
   IS
   SELECT COUNT(*)
   FROM   FNX_SOLICITUDES A,
          FNX_REPORTES_INSTALACION B
   WHERE  a.pedido_id    = w_pedido
   AND    a.subpedido_id = p_subpedido
   AND    a.pedido_id    = b.pedido_id
   AND    a.subpedido_id = b.subpedido_id
   AND    a.solicitud_id = b.solicitud_id
   AND    a.producto_id  = 'TELEV'
   AND    a.tipo_elemento_id IN ('INSHFC','INSIP')
   AND    b.tipo_solicitud IN ('NUEVO','RETIR')
   AND    a.tipo_elemento = 'CMP'
   AND    a.estado_id = 'TECNI';

   CURSOR c_tipo_elemen_id_equ ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                                 p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                                )
   IS
   SELECT tipo_elemento_id
   FROM   FNX_SOLICITUDES
   WHERE  pedido_id     = p_pedido
   AND    subpedido_id  = p_subpedido
   AND    producto_id   = 'TELEV'
   AND    tipo_elemento = 'EQU'
   AND    concepto_id   IN ('PETEC', 'PRACC');

   -- Se buscan las solicitudes de equipos para determinar cual de estos
   -- equipos es de tipo SWITCH.
   -- 2012-11-08   Yhernana    EstabilizDesaprov: adición campos empresa_id y municipio_id.
   CURSOR c_tipo_elemen_id_sw ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                                 p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                                )
   IS
   SELECT pedido_id,subpedido_id,solicitud_id, servicio_id, producto_id, tipo_elemento,tipo_elemento_id,identificador_id,empresa_id, municipio_id
   FROM   FNX_SOLICITUDES
   WHERE  pedido_id     = p_pedido
   AND    subpedido_id  = p_subpedido
   AND    producto_id   = 'TELEV'
   AND    tipo_elemento = 'EQU'
   --AND    tipo_elemento_id = 'EQURED'
   AND    concepto_id   IN ('PETEC', 'PRACC')
   ORDER BY tipo_elemento_id DESC;

   CURSOR c_sol_servip ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                         p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                       )
   IS
   SELECT COUNT(1)
   FROM   FNX_SOLICITUDES
   WHERE  pedido_id     = p_pedido
   AND    subpedido_id  = p_subpedido
   AND    servicio_id   = 'ENTRE'
   AND    producto_id   = 'TELEV'
   AND    tipo_elemento_id <> 'SERVIP';


   CURSOR c_equ ( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                  p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                )
   IS
    SELECT a.subpedido_id,a.solicitud_id,b.tipo_solicitud,
           FN_VALOR_CARACTERISTICA_SOL(A.pedido_id,A.subpedido_id,A.solicitud_id,1067) tipo_equipo_carct
    FROM fnx_solicitudes a, fnx_reportes_instalacion b
    WHERE a.pedido_id       = b.pedido_id
    AND a.subpedido_id      = b.subpedido_id
    AND a.solicitud_id      = b.solicitud_id
    AND a.tipo_elemento     = 'EQU'
    AND A.estado_soli       <> 'ANULA'
    AND a.pedido_id         = p_pedido
    AND a.subpedido_id      = p_subpedido
    AND FN_VALOR_CARACTERISTICA_SOL(A.pedido_id,A.subpedido_id,A.solicitud_id,33)='IP';

    v_sol_servip                NUMBER(2):=0;

    CURSOR C_TIPO_TRABAJO (p_pedido_id FNX_SOLICITUDES.pedido_id%TYPE, p_subpedido_id FNX_SOLICITUDES.subpedido_id%TYPE)
    IS
    SELECT A.pedido_id, A.subpedido_id, A.solicitud_id, A.tipo_trabajo
     FROM FNX_TRABAJOS_solicitudes A
    WHERE PEDIDO_ID         = p_pedido_id
    AND SUBPEDIDO_ID        = p_subpedido_id
    AND servicio_id         = 'ENTRE'
    AND producto_id         = 'TELEV'
    AND tipo_elemento_id    = 'EQURED'
    AND caracteristica_id   = 200
    AND ROWNUM              = 1;

    V_TIPO_TRABAJO_DG    C_TIPO_TRABAJO%ROWTYPE;

    CURSOR C_TIPO_TRABAJO_MIG (p_pedido_id FNX_SOLICITUDES.pedido_id%TYPE, p_subpedido_id FNX_SOLICITUDES.subpedido_id%TYPE)
    IS
    SELECT COUNT(*)
     FROM FNX_TRABAJOS_solicitudes A
    WHERE PEDIDO_ID         = p_pedido_id
    AND SUBPEDIDO_ID        = p_subpedido_id
    AND servicio_id         = 'ENTRE'
    AND producto_id         = 'TELEV'
    AND tipo_elemento_id    ='EQURED'
    AND caracteristica_id   = 1067
    AND VALOR               = 'ANALOGO'
    AND ROWNUM              = 1;

    V_TIPO_TRABAJO_MIG NUMBER(5);

    CURSOR c_equipos_tv_dig( p_pedido    IN  FNX_SOLICITUDES.pedido_id%TYPE,
                             p_subpedido IN  FNX_SOLICITUDES.subpedido_id%TYPE
                            )
    IS
    select A.pedido_id,
           A.subpedido_id,
           A.solicitud_id,
           A.tipo_elemento_id,
           A.tipo_elemento,
           A.caracteristica_id,
           A.valor
     from fnx_caracteristica_solicitudes A
     where pedido_id            = p_pedido
       and subpedido_id         = p_subpedido
       and caracteristica_id    = 33
       and valor                = 'HFC';

    -- 2012-07-25 Se agrega condición para el subpedido que se está verificando.
    -- Se modifica para el requerimiento de demostraciones.
    CURSOR c_equipos
    IS
    SELECT COUNT(*) FROM FNX_SOLICITUDES
    WHERE PEDIDO_ID =  w_pedido
      AND SUBPEDIDO_ID = v_subped.SUBPEDIDO_ID
      AND SERVICIO_ID = 'ENTRE'
      AND PRODUCTO_ID = 'TELEV'
      AND TIPO_eLEMENTO_ID IN ('DECO','EQURED');

    -- 2012-04-26 Cursor para retiros de DECOS. Con este cursor se pretende poder
    -- colocar en PORDE el componente de canales cuando viene un PPV.
    CURSOR C_TRABAJOS_RET_DECO (p_pedido_id           FNX_SOLICITUDES.pedido_id%TYPE
                               ,p_subpedido_id        FNX_SOLICITUDES.subpedido_id%TYPE
                               ,p_tipo_elemento_id    FNX_SOLICITUDES.tipo_elemento_id%TYPE
                               ,p_caracteristica_id   FNX_TRABAJOS_SOLICITUDES.caracteristica_id%TYPE)
    IS
    SELECT COUNT(*)
      FROM FNX_TRABAJOS_SOLICITUDES
     WHERE PEDIDO_ID         = p_pedido_id
       AND SUBPEDIDO_ID      = p_subpedido_id
       AND TIPO_ELEMENTO_ID  = p_tipo_elemento_id
       AND CARACTERISTICA_ID = p_caracteristica_id
       AND TIPO_TRABAJO      = 'RETIR';

    -- 2012-08-23 Se crea cursor para verificar el tipo de tecnología, esto pasa
    -- cuando es un pedido mixto que tienen televisión hfc digital.
    CURSOR C_EQUIPOS_TECNOLOGIA (p_pedido_id            FNX_SOLICITUDES.pedido_id%TYPE
                                ,p_subpedido_id         FNX_SOLICITUDES.subpedido_id%TYPE
                                ,p_caracteristica_id   FNX_CARACTERISTICA_SOLICITUDES.caracteristica_id%TYPE)
        IS
    SELECT PEDIDO_ID, SUBPEDIDO_ID,SOLICITUD_ID, VALOR
      FROM FNX_CARACTERISTICA_SOLICITUDES
     WHERE PEDIDO_ID           = p_pedido_id
       AND SUBPEDIDO_ID        = p_subpedido_id
       AND CARACTERISTICA_ID   = p_caracteristica_id;

    CURSOR c_equipo_config (p_equipo_id FNX_EQUIPOS.equipo_id%TYPE,
                            p_caracteristica_id FNX_CONFIGURACIONES_EQUIPOS.caracteristica_id%TYPE)
    IS
    SELECT VALOR
      FROM FNX_configuraciones_EQUIPOS
     WHERE EQUIPO_ID         = p_equipo_id
       AND caracteristica_id = p_caracteristica_id;

    b_tiene_retiro_ppv      BOOLEAN:=FALSE;
    b_tiene_retiro_serhfc   BOOLEAN:=FALSE;
    b_tiene_retiro_equ      BOOLEAN:=FALSE;

    V_TRABAJOS_SERHFC       NUMBER:=0;
    V_TRABAJOS_PPV          NUMBER:=0;
    V_TRABAJOS_EQU          NUMBER:=0;

    v_concepto_equipos      FNX_SOLICITUDES.concepto_id%TYPE;

    b_existen_equipos           BOOLEAN:=FALSE;
    v_equipos                   NUMBER(5);

    v_tipo_elemen_id_equ        FNX_SOLICITUDES.tipo_elemento_id%TYPE:= NULL;
    v_tipo_elemen_id_sw         FNX_SOLICITUDES.tipo_elemento_id%TYPE:= NULL;

    v_cmp_nuevo_telev           NUMBER(1):=0;
    v_cmp_iptv                  NUMBER(5):=0;
    v_cambio_extensiones        FNX_TRABAJOS_SOLICITUDES.tipo_trabajo%TYPE;
    v_traslado                  FNX_TRABAJOS_SOLICITUDES.tipo_trabajo%TYPE;
    v_tipo_equipo               FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE := NULL;
    v_tipo_equipo_trabajos      FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE := NULL;
    v_tipo_equ_sw               FNX_CARACTERISTICA_SOLICITUDES.valor%TYPE := NULL;
    v_tipo_solicitud            FNX_REPORTES_INSTALACION.tipo_solicitud%TYPE:=NULL;
    --2009-11-24   Jsierrao  Se modifica el tipo de variable v_cambio_plan_telev pasando de tipo_trabajo a valor para cuando se generen cambios de plan de televisión
    --             y en la caracteristica 3830 venga el valor MIXBAS y no quede en PETEC
    v_cambio_plan_telev         FNX_TRABAJOS_SOLICITUDES.valor%TYPE;
    v_tiene_stboxhd             VARCHAR2(10);
    b_salir                     BOOLEAN:=FALSE;

    --2011-05-25 ETachej Integridad Fenix OPEN - La solicitud de retiro de DECo debe quedar PORDE.
    v_tipo_trabajo_equ          FNX_TRABAJOS_SOLICITUDES.tipo_trabajo%TYPE;
    b_retir_deco                BOOLEAN:=FALSE;
    b_pedido_tvdigital          BOOLEAN:=FALSE;
    b_tiene_decos_hfc           BOOLEAN:=FALSE;
    b_migracion_hfc             BOOLEAN:=FALSE;
    
    --2014-01-15 lhernans Variables para automatización cola DSLAM
    v_etiqueta                      fnx_caracteristica_solicitudes.valor%TYPE;
    v_municipio_porta               fnx_caracteristica_solicitudes.valor%TYPE;
    v_mun_salida                    fnx_municipios.mun_salida_internet%TYPE;
    --2014-10-15   MGATTIS       
    v_aplicacion                FNX_PARAM_EVENTOS_BTS.aplicacion%TYPE;   
    -- 2014-10-15   MGATTIS Fin declaración de variables
    
    v_concepto_tv_digital       FNX_SOLICITUDES.concepto_id%TYPE;
    v_equipos_hdd       BOOLEAN := FALSE;  --2012-09-13
    CURSOR c_equipos_solicitud (p_subpedido_id FNX_SOLICITUDES.subpedido_id%TYPE) IS          --2013-05-31   eaguirrg
     SELECT pedido_id, subpedido_id, solicitud_id,
            fn_valor_caracteristica_sol (pedido_id, subpedido_id, solicitud_id, 1067) tipo_equipo
     FROM   fnx_solicitudes
     WHERE  pedido_id = w_pedido
     AND    subpedido_id  = p_subpedido_id --2013-05-31   eaguirrg
     AND    servicio_id   = 'ENTRE'
     AND    producto_id   = 'TELEV'
     AND    tipo_elemento = 'EQU';

    -- 2012-10-11 Cursores para el tema de siembra digital.
    CURSOR C_SIEMDIG IS
    SELECT COUNT(*)
      FROM fnx_CARACTERISTICA_solicitudes
     WHERE pedido_id         = w_pedido
       AND SERVICIO_ID       = 'ENTRE'
       AND PRODUCTO_ID       = 'TELEV'
       AND TIPO_ELEMENTO_ID  = 'SERVIP'
       AND TIPO_ELEMENTO     = 'CMP'
       AND CARACTERISTICA_ID = '4897' ;

    CURSOR C_EQUIPOSC_SIEM
        IS
    SELECT COUNT(*)
      FROM FNX_SOLICITUDES
     WHERE PEDIDO_ID     = w_pedido
       AND SERVICIO_ID   = 'TELEV'
       AND TIPO_ELEMENTO = 'EQU';

    V_SIEMDIG      NUMBER;
    v_equipos_siem NUMBER;

    -- 2012-11-08 Yhernana    EstabilizDesaprov:Cursor para determinar si el equipo a actualizarse tiene asociado una solicitud de Rec de Equipo
    CURSOR c_validar_retir_equipo(pc_equipo     IN FNX_EQUIPOS.equipo_id%TYPE)
    IS
    SELECT caso.pedido_id, caso.subpedido_id, caso.solicitud_id, soli.producto_id,
           soli.tipo_elemento_id, soli.estado_id, soli.concepto_id,
           soli.municipio_id
      FROM fnx_caracteristica_solicitudes caso,
           fnx_solicitudes soli,
           fnx_reportes_instalacion rein
     WHERE caso.pedido_id = soli.pedido_id
       AND caso.subpedido_id = soli.subpedido_id
       AND caso.solicitud_id = soli.solicitud_id
       AND soli.pedido_id = rein.pedido_id
       AND soli.subpedido_id = rein.subpedido_id
       AND soli.solicitud_id = rein.solicitud_id
       AND caso.caracteristica_id = 200
       AND caso.valor = pc_equipo
       AND soli.tipo_elemento_id IN ('EQURED', 'EQACCP')
       AND rein.tipo_solicitud = 'RETIR';

    -- 2012-11-08   Yhernana    EstabilizDesaprov: variables necesarias para proceso Desaprovisionamiento Equipos
    v_equipoAsoc           FNX_EQUIPOS.equipo_id%TYPE;
    v_validar_retir_equipo      c_validar_retir_equipo%ROWTYPE;
    
    --2014-01-27   Mcastaa   INC2166560_ProvCanaHDIPTV Validar que el tipo trabajo sea RETIR y la caracteristica sea 200 
      CURSOR c_validar_retir_trab(pc_pedido IN fnx_solicitudes.pedido_id%TYPE, 
                                  pc_subpedido IN fnx_solicitudes.subpedido_id%TYPE,
                                  pc_solicitud IN fnx_solicitudes.solicitud_id%TYPE)
      IS
      SELECT valor
        FROM fnx_trabajos_solicitudes 
       WHERE pedido_id    = pc_pedido
         AND subpedido_id = pc_subpedido
         AND solicitud_id = pc_solicitud
         AND caracteristica_id = '200'
         AND tipo_trabajo = 'RETIR';    
    
    -- 2013-05-08 - Inicio Cambios
       CURSOR C_TIPO_TRABAJO_DEMO (p_pedido_id FNX_SOLICITUDES.pedido_id%TYPE, p_subpedido_id FNX_SOLICITUDES.subpedido_id%TYPE)
       IS
       SELECT A.pedido_id, A.subpedido_id, A.solicitud_id, A.tipo_trabajo
       FROM FNX_TRABAJOS_solicitudes A
       WHERE PEDIDO_ID         = p_pedido_id
       AND SUBPEDIDO_ID        = p_subpedido_id
       AND servicio_id         =  'ENTRE'
       AND producto_id         =  'TELEV'
       AND tipo_elemento_id    IN ('SERHFC','SERVIP')
       AND tipo_elemento       = 'CMP'
       AND caracteristica_id   IN 
                                  (
                                   --Caracteristicas correspondintes a paquetes de TV.
                                   SELECT distinct CE.caracteristica_id
                                   FROM FNX_CARACTERISTICAS_ELEMENTOS CE
                                   WHERE CE.SERVICIO_ID = 'ENTRE'
                                   AND   CE.PRODUCTO_ID = 'TELEV'
                                   AND   CE.TIPO_CARACTERISTICA = 'SERV'
                                   AND   CE.tipo_elemento_id IN ('SERHFC','SERVIP')
                                  )
       AND ROWNUM              = 1;
       --
       v_tipo_trabajo_demo C_TIPO_TRABAJO_DEMO%ROWTYPE;
       --
       CURSOR c_tiene_solici_demo (p_pedido_id        FNX_SOLICITUDES.pedido_id%TYPE, 
                                   p_subpedido_id     FNX_SOLICITUDES.subpedido_id%TYPE,
                                   p_identificador_id FNX_SOLICITUDES.identificador_id%TYPE)
       IS
       SELECT count(*)
       FROM FNX_SOLICITUDES SO,
            FNX_CARACTERISTICA_SOLICITUDES CS
       WHERE (SO.IDENTIFICADOR_ID_NUEVO = p_identificador_id OR SO.IDENTIFICADOR_ID = p_identificador_id)  
       AND   SO.ESTADO_ID     = 'FACTU'       
       AND   SO.SERVICIO_ID   ='ENTRE'
       AND   SO.PRODUCTO_ID   = 'TELEV'
       AND   SO.TIPO_ELEMENTO = 'CMP'
       AND   SO.TIPO_ELEMENTO_ID IN ('SERHFC','SERVIP')
       AND   SO.PEDIDO_ID    = CS.PEDIDO_ID
       AND   SO.SUBPEDIDO_ID = CS.SUBPEDIDO_ID
       AND   SO.SOLICITUD_ID = CS.SOLICITUD_ID
       AND   CS.CARACTERISTICA_ID = 3852 --Acep
       AND   CS.VALOR = 'P'
       AND   CS.SUBPEDIDO_ID < p_subpedido_id;
       --
       v_tiene_solici_demo NUMBER(5) := 0;
       --   
       b_es_nuevo_trabajo BOOLEAN := FALSE;
       --                                
    -- 2013-05-08 - Fin Cambios

    -- 2012-11-08   Yhernana    EstabilizDesaprov: procedimiento interno encargado de insertar caracteristicas necesarias para el Desaprovisionamiento de Equipos
    --                          Aplica para Retiro de Canales que tiene asociado el retiro de Decodificadores.
    PROCEDURE pri_insertar_carac_desaprov (
                                            p_pedido          IN     FNX_PEDIDOS.pedido_id%TYPE,
                                            p_subpedido       IN     FNX_SUBPEDIDOS.subpedido_id%TYPE,
                                            p_solicitud       IN     FNX_SOLICITUDES.solicitud_id%TYPE,
                                            p_servicio        IN     FNX_SOLICITUDES.servicio_id%TYPE,
                                            p_producto        IN     FNX_SOLICITUDES.producto_id%TYPE,
                                            p_tipo_elementoID IN     FNX_SOLICITUDES.tipo_elemento_id%TYPE,
                                            p_tipo_elemento   IN     FNX_SOLICITUDES.tipo_elemento%TYPE,
                                            p_identificador   IN     FNX_SOLICITUDES.identificador_id%TYPE,
                                            p_municipio       IN     FNX_SOLICITUDES.municipio_id%TYPE DEFAULT 'MEDANTCOL',
                                            p_empresa         IN     FNX_SOLICITUDES.empresa_id%TYPE DEFAULT 'UNE',
                                            p_equipo_id       IN     FNX_EQUIPOS.EQUIPO_ID%TYPE,
                                            p_mensaje         OUT    VARCHAR2
                                            ) IS

        v_obsoleto                 fnx_marcas_referencias.obsoleto%TYPE;
        v_caracteristica           fnx_tipos_caracteristicas.caracteristica_id%TYPE;
        v_valor_caracteristica     fnx_caracteristica_solicitudes.valor%TYPE;
        v_config_equipo            fnx_caracteristica_solicitudes.valor%TYPE;--2013-05-23   Yhernana
        
        -- 2013-02-22   Yhernana    Se comenta la linea del estado, ya que en ocasiones no viene con valor OCU.
        CURSOR c_equipo_carac (
            p_identificador_asociado IN FNX_IDENTIFICADORES.identificador_id%TYPE,
            pp_equipo_id             IN FNX_EQUIPOS.EQUIPO_ID%TYPE
        ) IS
            SELECT marca_id, referencia_id, tipo_elemento_id, tipo_elemento
              FROM fnx_equipos
             WHERE identificador_id = p_identificador_asociado
               AND equipo_id        = pp_equipo_id;
               --AND estado           = 'OCU';
        v_carac_equipo         c_equipo_carac%ROWTYPE;


         CURSOR c_carac_equipos (
            p_marca_id     IN FNX_CARACTERISTICAS_EQUIPOS.MARCA_ID%TYPE,
            p_tipo_elem_id IN FNX_CARACTERISTICAS_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
            p_refe_id      IN FNX_CARACTERISTICAS_EQUIPOS.REFERENCIA_ID%TYPE,
            p_tipo_elem    IN FNX_CARACTERISTICAS_EQUIPOS.TIPO_ELEMENTO%TYPE
        ) IS
            SELECT caracteristica_id
              FROM fnx_caracteristicas_equipos
             WHERE marca_id         = p_marca_id
               AND referencia_id    = p_refe_id
               AND tipo_elemento_id = p_tipo_elem_id
               AND tipo_elemento    = p_tipo_elem;
        
        -- 2013-05-23   Yhernana
        CURSOR c_config_equipo (
            pc_marca_id     IN FNX_EQUIPOS.MARCA_ID%TYPE,
            pc_tipo_elem_id IN FNX_EQUIPOS.TIPO_ELEMENTO_ID%TYPE,
            pc_refe_id      IN FNX_EQUIPOS.REFERENCIA_ID%TYPE,
            pc_tipo_elem    IN FNX_EQUIPOS.TIPO_ELEMENTO%TYPE,
            pc_equipo       IN FNX_EQUIPOS.EQUIPO_ID%TYPE
        ) IS
            SELECT caracteristica_id, valor
              FROM fnx_configuraciones_equipos
             WHERE marca_id = pc_marca_id
               AND referencia_id = pc_refe_id
               AND tipo_elemento_id = pc_tipo_elem_id
               AND tipo_elemento = pc_tipo_elem
               AND equipo_id = pc_equipo
               AND caracteristica_id IN (90, 347, 1067, 1093);              

        -- 2012-11-20   Yhernana    EstabilizDesaprov: Creación procedimiento interno encargado de insertar registros en FNX_TRABAJOS_SOLICITUDES.
        --                          En caso de presentar error en la inserción por registro duplicado se realiza un Update sobre la misma tabla.     
        PROCEDURE pri_crear_trabajo
              ( p_pedido         IN FNX_SOLICITUDES.pedido_id%TYPE,
                p_subpedido      IN FNX_SOLICITUDES.subpedido_id%TYPE,
                p_solicitud      IN FNX_SOLICITUDES.solicitud_id%TYPE,
                p_servicio       IN FNX_SOLICITUDES.servicio_id%TYPE,
                p_producto       IN FNX_SOLICITUDES.producto_id%TYPE,
                p_tipo_elemento  IN FNX_SOLICITUDES.tipo_elemento%TYPE,
                p_tipo_elemID    IN FNX_SOLICITUDES.tipo_elemento_id%TYPE,
                p_caracteristica IN FNX_TRABAJOS_SOLICITUDES.caracteristica_id%TYPE,
                p_valor          IN FNX_TRABAJOS_SOLICITUDES.valor%TYPE,
                p_tipo_trabajo   IN FNX_TRABAJOS_SOLICITUDES.tipo_trabajo%TYPE,
                p_municipio      IN FNX_CARACTERISTICA_SOLICITUDES.municipio_id%TYPE,
                p_empresa        IN FNX_CARACTERISTICA_SOLICITUDES.empresa_id%TYPE,
                p_mensaje        OUT VARCHAR2 )
        IS
        BEGIN
           INSERT INTO FNX_TRABAJOS_SOLICITUDES
             ( pedido_id, subpedido_id, solicitud_id,
               servicio_id, producto_id, tipo_elemento, tipo_elemento_id,
               caracteristica_id, valor, tipo_trabajo,
               municipio_id, empresa_id
             )
           VALUES
             ( p_pedido, p_subpedido, p_solicitud,
               p_servicio, p_producto, p_tipo_elemento, p_tipo_elemID,
               p_caracteristica, p_valor, p_tipo_trabajo,
               p_municipio, p_empresa
             );
        EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE  FNX_TRABAJOS_SOLICITUDES
                SET     valor   = p_valor
                WHERE   pedido_id               =   p_pedido
                        AND subpedido_id        =   p_subpedido
                        AND solicitud_id        =   p_solicitud
                        AND servicio_id         =   p_servicio
                        AND producto_id         =   p_producto
                        AND tipo_elemento       =   p_tipo_elemento
                        AND tipo_elemento_id    =   p_tipo_elemID
                        AND caracteristica_id   =   p_caracteristica ;
            
           WHEN OTHERS THEN
                 p_mensaje := '<PPU-005> Error creando trabajos. '||SUBSTR(SQLERRM, 1, 30);

        END;        

    BEGIN

        -- Obtener los datos del equipo terminal actual asociado al identificador.
        OPEN  c_equipo_carac (p_identificador, p_equipo_id);
        FETCH c_equipo_carac INTO v_carac_equipo;
        CLOSE c_equipo_carac;

        --Determinar de una vez si el equipo es obsoleto.
        v_obsoleto := nvl(fn_consultar_equipo_obsoleto (v_carac_equipo.marca_id, v_carac_equipo.referencia_id, v_carac_equipo.tipo_elemento_id, v_carac_equipo.tipo_elemento), 'N');
        
         --2013-09-02    aarbob  REQ28278_Tangibilizacion
        v_propiedadEqu := NVL(FN_VALOR_CAR_EN_CONFIG_EQUIPOS(p_equipo_id,1093,v_carac_equipo.marca_id),'X');
                        
        IF  v_propiedadEqu = 'CM' THEN
                            --se actualiza el estado del decodificador a RES con el fin de que sea a futuro recuperable
            UPDATE fnx_equipos
               SET ESTADO = 'RES'
             WHERE  MARCA_ID = v_carac_equipo.marca_id
               AND REFERENCIA_ID = v_carac_equipo.referencia_id
               AND TIPO_ELEMENTO_ID = v_carac_equipo.tipo_elemento_id
               AND TIPO_ELEMENTO = v_carac_equipo.tipo_elemento
               AND EQUIPO_ID = p_equipo_id;
                           
                            ---se actualiza para dejar LOG que informe que no se genera solictud de recogida de equipo ya que el equipo es comprado por el cliente
            UPDATE FNX_PEDIDOS
               SET observacion =  SUBSTR ('Desap-NoGeneraSoli RecupEquip' ||p_equipo_id||';'|| observacion,1,200)
             WHERE pedido_id     = p_pedido;
        
        ELSE
             
            -- Actualizar equipo como XRE
            IF v_obsoleto = 'S' THEN
                UPDATE fnx_equipos
                   SET estado = 'XRE'
                 WHERE marca_id = v_carac_equipo.marca_id
                   AND referencia_id = v_carac_equipo.referencia_id
                   AND tipo_elemento_id = v_carac_equipo.tipo_elemento_id
                   AND tipo_elemento = v_carac_equipo.tipo_elemento
                   AND equipo_id = p_equipo_id;
            END IF;
                       
                            
        END IF; 
        
        
        --Inserción de características 4421, 4422, 4423, 4424, 4427, 4428 en FNX_CARACTERISTICA_SOLICITUDES basado en la
        --configuración de la  tabla FNX_CARACTERISTICAS_EQUIPOS según la Marca y Referencia del equipo a retirar.
        FOR reg_carac_equipos IN c_carac_equipos(v_carac_equipo.marca_id,      v_carac_equipo.tipo_elemento_id,
                                                 v_carac_equipo.referencia_id, v_carac_equipo.tipo_elemento) LOOP
            v_caracteristica       := reg_carac_equipos.CARACTERISTICA_ID;
            v_valor_caracteristica := NULL;

            -- Creacion de la caracteristica para la solicitud
            PKG_PEDIDOS_UTIL.PR_INSUPD_CARACTERISTICA (p_pedido,          p_subpedido,      p_solicitud,
                                                       p_servicio,        p_producto,       p_tipo_elemento,
                                                       p_tipo_elementoID, v_caracteristica, NULL,
                                                       p_municipio,       p_empresa,        p_mensaje);
        END LOOP;

        -- Insertar características 960 y 982 en FNX_CARACTERISTICA_SOLICITUDES.
        -- 2012-11-20   Yhernana    EstabilizDesaprov: Se invoca procedimiento PKG_PEDIDOS_UTIL.PR_INSUPD_CARACTERISTICA,
        --                          en vez de PKG_PEDIDOS_UTIL.PR_CREAR_CARACTERISTICA
        PKG_PEDIDOS_UTIL.PR_INSUPD_CARACTERISTICA (p_pedido,          p_subpedido, p_solicitud,
                                                   p_servicio,        p_producto,  p_tipo_elemento,
                                                   p_tipo_elementoID, 960,         v_carac_equipo.marca_id,
                                                   p_municipio,       p_empresa,   p_mensaje);

        PKG_PEDIDOS_UTIL.PR_INSUPD_CARACTERISTICA (p_pedido,          p_subpedido, p_solicitud,
                                                   p_servicio,        p_producto,  p_tipo_elemento,
                                                   p_tipo_elementoID, 982,         v_carac_equipo.referencia_id,
                                                   p_municipio,       p_empresa,   p_mensaje);
        
        -- 2013-05-23   Yhernana    Se insertan las características 90 (Identificador Asociado), 347 (Tipo Equipo), 1067 (Tipo de Equipo)
        --                          y 1093 (Propiedad Equipo) en FNX_CARACTERISTICA_SOLICITUDES                                     
        FOR rg_config_equipo IN c_config_equipo (v_carac_equipo.marca_id,       v_carac_equipo.tipo_elemento_id,
                                                 v_carac_equipo.referencia_id,  v_carac_equipo.tipo_elemento,   p_equipo_id) LOOP

            v_config_equipo := NULL;
            v_config_equipo := fn_valor_caracteristica_sol (p_pedido, p_subpedido, p_solicitud, rg_config_equipo.caracteristica_id);
    
            IF v_config_equipo IS NULL THEN
                PKG_PEDIDOS_UTIL.PR_INSUPD_CARACTERISTICA (p_pedido,    p_subpedido,    p_solicitud,
                                                           p_servicio,  p_producto,     p_tipo_elemento,    p_tipo_elementoID,
                                                           rg_config_equipo.caracteristica_id,  rg_config_equipo.valor,
                                                           p_municipio, p_empresa,      p_mensaje);
            END IF;                                                 
        END LOOP; 


        
        -- Insertar característica 960 en FNX_TRABAJOS_SOLICITUDES.
        -- 2012-11-20   Yhernana    EstabilizDesaprov: Se invoca procedimiento interno encargado de insertar registros
        PRI_CREAR_TRABAJO (p_pedido,        p_subpedido,       p_solicitud,      p_servicio,  p_producto,
                           p_tipo_elemento, p_tipo_elementoid, 960, v_carac_equipo.marca_id, 'RETIR',
                           p_municipio,     p_empresa,         p_mensaje);
        
        -- Insertar característica 982 en FNX_TRABAJOS_SOLICITUDES.
        -- 2012-11-20   Yhernana    EstabilizDesaprov: Se invoca procedimiento interno encargado de insertar registros
        PRI_CREAR_TRABAJO (p_pedido,        p_subpedido,       p_solicitud,      p_servicio,  p_producto,
                           p_tipo_elemento, p_tipo_elementoid, 982, v_carac_equipo.referencia_id, 'RETIR',
                           p_municipio,     p_empresa,         p_mensaje);

    EXCEPTION
        WHEN OTHERS THEN
          p_mensaje := '<ENRUTAR_TELEV) Error insertando caract desaprov. '||SUBSTR(SQLERRM, 1, 50);
    END;

BEGIN
   w_mensaje_error  := NULL;
   w_incons         := FALSE;
   v_cambio_plan_telev  :=NULL;

   OPEN  c_subped;
   FETCH c_subped INTO v_subped;

   LOOP
      EXIT WHEN c_subped%NOTFOUND OR w_incons;
      -- Cursor para determinar si el agrupador del pedido tiene componentes para IPTV.
      OPEN  c_cmp_acceso (v_subped.identificador_id);
      FETCH c_cmp_acceso INTO v_cmp_iptv;
      CLOSE c_cmp_acceso;

      -- 2010-04-29 Se abre el cursor para realizar ajuste al tema de cambio de HFC a mixto
      OPEN  c_acceso_cmp_nuevo (v_subped.pedido_id, v_subped.subpedido_id);
      FETCH c_acceso_cmp_nuevo INTO w_acceso_cmp_nuevo;
      CLOSE c_acceso_cmp_nuevo;


      OPEN  c_solic_srv (v_subped.pedido_id, v_subped.subpedido_id );
      FETCH c_solic_srv INTO w_cuenta_srv_nuevo;
      CLOSE c_solic_srv;


      v_cambio_plan_telev:=fn_valor_trabaj_subelem(v_subped.pedido_id, v_subped.subpedido_id,'TELEV',3830);

      -- 2011-11-28 Se verifica el valor del trabajo para ver si se trata de una migración
      OPEN C_TIPO_TRABAJO_MIG (v_subped.pedido_id, v_subped.subpedido_id);
      FETCH C_TIPO_TRABAJO_MIG INTO V_TIPO_TRABAJO_MIG;
      CLOSE C_TIPO_TRABAJO_MIG;

      IF V_TIPO_TRABAJO_MIG >0 THEN
            b_migracion_hfc:=TRUE;
      END IF;


      -- 2008-03-27   Aacosta   Modificación de la condición para que se provisionen componentes y equipos
      --                        para peticiones.
      -- 2010-04-29 Si el cursor devuelve 0, entonces se garantiza que no es un cambio HFC A MIXTO BÁSICO
      -- para tal fin, se asigna la condicion AND w_acceso_cmp_nuevo = 0
      IF w_cuenta_srv_nuevo = 0 AND w_acceso_cmp_nuevo = 0 THEN

         OPEN  c_solic_cmp_equ (v_subped.pedido_id, v_subped.subpedido_id);
         FETCH c_solic_cmp_equ INTO v_cuenta_cmp_equ;
         CLOSE c_solic_cmp_equ;

         IF v_cuenta_cmp_equ > 0 THEN

            -- Determinar si en el subpedido viene un identificador de paquete y asi
            -- establecer el concepto de la solicitud que genera las ordenes
            v_identificador_pack :=
                     FN_valor_caract_subpedido ( v_subped.pedido_id, v_subped.subpedido_id, 2879 );

            -- 2008-12-24  Aacosta   Validar para el componente EQURED la característica 1067. Para el caso que sea un
            --                       equipo SET UP BOX de alta definición la solicitud debe quedar PETEC, caso de no serlo
            --                       se debe comportar igual como se hace actualmente.

            -- 2010-08-13 Se comenta lógica correspondiente al enrutamiento de tema de set up box.
            --            Para poder realizarlo de manera genérica.
            /*v_tipo_equipo  := FN_VALOR_CARACT_SUBPEDIDO ( v_subped.pedido_id, v_subped.subpedido_id, 1067 );

            -- 2010-07-22 Se verifica si el equipo que viene, corresponde a un set up box de alta definición.

            v_tiene_stboxhd := NVL(FN_DOMINIO_DESCRIPCION('EQU_TELEV',v_tipo_equipo),'NO');

            -- 2010-07-22 Se quita la condición
            --IF NVL(v_tipo_equipo, 'NING') = 'STBAD' THEN  EXIT; END IF;
            -- Para poder validar con la variable  v_tiene_stboxhd


            IF v_tiene_stboxhd = 'SI' THEN
               EXIT;
            END IF;*/

            --Sección Nuevo enrutamiento
            --2010-08-13 Se crea lógica a Fin de poder colocar genérica la manera en la que se deben enrutar.
            --los STBOX HD, teniendo cuenta las peticiones que se puedan presentar.

            -- 2014-04-11. [COMMENT] Revisar vigencia de cursor c_equ.  El cursor hace referencia a valor IP en característica 33. 
            
            FOR reg IN c_equ (v_subped.pedido_id, v_subped.subpedido_id) LOOP
            --2010-08-31 Se cambia la condición IF reg.tipo_solicitud IN ('CAMBI','NUEVO') THEN
            -- por la condición IF reg.tipo_solicitud ='NUEVO' THEN de acuerdo al requerimiento 1371892
             IF reg.tipo_solicitud ='NUEVO' THEN
                v_tipo_equipo := NVL(FN_DOMINIO_DESCRIPCION('EQU_TELEV',reg.tipo_equipo_carct),'NO');

                IF v_tipo_equipo = 'SI' THEN
                  b_salir := TRUE;
                END IF;
              END IF;
              --2010-09-01 Se debe agregar una validación considerando el tipo de cambio de equipo que se va a realizar.

              IF reg.tipo_solicitud IN('CAMBI') THEN

                    v_tipo_equipo:= NVL(FN_DOMINIO_DESCRIPCION('EQU_TELEV',reg.tipo_equipo_carct),'NO');
                    v_tipo_equipo_trabajos:= NVL(FN_DOMINIO_DESCRIPCION('EQU_TELEV',PKG_SOLICITUDES.fn_valor_trabajo(v_subped.pedido_id,v_subped.subpedido_id,reg.solicitud_id,1067)),'NO');

                   IF v_tipo_equipo = 'SI' AND v_tipo_equipo_trabajos='NO' THEN
                       b_salir := TRUE;
                   END IF;

               END IF;

            END LOOP;

            IF b_salir THEN

                -- 2011-10-24 Verificación de la solicitud de equipos de TV digital
                -- se colocan en PORDE aquellos equipos de tv digital. Los demás quedan en PETEC.
                -- para el reenvío de componentes.
                FOR reg in c_equipos_tv_dig (v_subped.pedido_id, v_subped.subpedido_id) LOOP
                        v_identificador_nuevo := v_subped.identificador_id;

                        -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                        bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_subped.pedido_id);
                            
                        IF bEsOfertaEspecifica THEN
                            p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_subped.pedido_id, v_subped.subpedido_id);
                        ELSE
                            p_SubPendi := FALSE; -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
                        END IF;
                    
                        IF p_SubPendi AND bEsOfertaEspecifica THEN
                           
                              UPDATE FNX_SOLICITUDES
                                 SET estado_id     = 'ORDEN',
                                     estado_soli   = 'DEFIN',
                                     concepto_id   = 'PEXPQ',
                                     tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                     identificador_id = v_identificador_nuevo,
                                     observacion   = ' <*TV1* ['||'PEXPQ'||']> '||observacion
                               WHERE pedido_id     = reg.pedido_id
                               AND   subpedido_id  = reg.subpedido_id
                               AND   solicitud_id  = reg.solicitud_id
                               AND   servicio_id   = 'ENTRE'
                               AND   producto_id   = 'TELEV'
                               AND   tipo_elemento = 'EQU'
                               --AND   tipo_elemento_id = 'STBOX'
                               AND   concepto_id   IN ('PETEC', 'PRACC');
                                                                     
                        ELSE
                            -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                           
                              UPDATE FNX_SOLICITUDES
                                 SET estado_id     = 'ORDEN',
                                     estado_soli   = 'DEFIN',
                                     concepto_id   = 'PORDE',
                                     tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                     identificador_id = v_identificador_nuevo,
                                     observacion   = ' <*TV1* ['||'PORDE'||']> '||observacion
                               WHERE pedido_id     = reg.pedido_id
                               AND   subpedido_id  = reg.subpedido_id
                               AND   solicitud_id  = reg.solicitud_id
                               AND   servicio_id   = 'ENTRE'
                               AND   producto_id   = 'TELEV'
                               AND   tipo_elemento = 'EQU'
                               --AND   tipo_elemento_id = 'STBOX'
                               AND   concepto_id   IN ('PETEC', 'PRACC');                                              
                        -- 2013-01-28: Mmedinac: Req20216: INICIO MODIFICACIÒN llamado a la funcion para actualiza los demas subpedidos a porde
                                   
                            -- 2013-01-24: Req20216: Mmedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico 
                            IF bEsOfertaEspecifica AND NOT p_SubPendi THEN
                                pkg_provision_paquete.pr_genorden_pexpq (reg.pedido_id, reg.tipo_elemento_id, p_mensaje);
                            END IF;                       
                            -- 2013-01-24: Req20216: Mmedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico

                        END IF;
                        -- 2013-01-28: Mmedinac: Req20216: FIN MODIFICACIÒN llamado a la funcion para actualiza los demas subpedidos a porde

                END LOOP;
                EXIT;
            END IF;
            -- Fin Sección Nuevo enrutamiento
            IF v_identificador_pack IS NULL THEN
               v_concepto_ordenes := 'PORDE';
               dbms_output.put_line('VAL 1 ');
            ELSE
               v_concepto_ordenes := 'PEXPQ';
            END IF;

            --   Si hay solicitudes de EPM-TV (equipos y/o canales)
            OPEN  c_total_cmp(v_subped.pedido_id, v_subped.subpedido_id);
            FETCH c_total_cmp INTO w_cuenta_cmp;
            CLOSE c_total_cmp;

            OPEN  c_total_equ (v_subped.pedido_id, v_subped.subpedido_id);
            FETCH c_total_equ INTO w_cuenta_equ;
            CLOSE c_total_equ;

            -- 2014-04-11 REQ 36900 GPON Hogares
            -- Se cambia búsqueda de característica 216 por caracteristica 33 con cursor c_carac.  La característica 216 ya no se utiliza
            -- para definir la tecnología y no está configurada para elementos EQU / SERHFC / SERVIP 
            -- Buscar el valor de la caracteristica tipo Tecnologia TV.
            -- C_CARAC.  Se cambia referencias a variable v_solic_cmp variable v_subped. pues la variable v_solic_cmp aún no tiene ningun valor            
            OPEN  c_carac( v_subped.pedido_id, v_subped.subpedido_id, 1, 33 );
            FETCH c_carac INTO v_tecnologia;
            CLOSE c_carac;

            IF v_tecnologia IS NULL THEN
            
               -- Buscar sí viene una solicitud de Equipo en el subpedido y determinar el tipo de elemento
               OPEN  c_tipo_elemen_id_equ (v_subped.pedido_id, v_subped.subpedido_id);
               FETCH c_tipo_elemen_id_equ INTO v_tipo_elemen_id_equ;
               CLOSE c_tipo_elemen_id_equ;

               -- 2009-02-13  Aacosta   Se valida para el caso de los DECOS que la tecnología que se maneje en la
               --                       solicitud sea HFC.

               IF v_tipo_elemen_id_equ = 'DECO' THEN
                  v_tecnologia := 'HFC';
               ELSE
                 -- 2010-02-08  Aacosta  Para obtener la tecnología que se debe asociar al componente o al equipo, se debe
                 --                      consultar la tecnología del componente INSIP o INSHFC en la característica 207 de
                 --                      estos componentes.
                 v_tecnologia := NVL(PKG_IDENTIFICADORES.fn_valor_configuracion
                                        ( v_subped.identificador_id||'-IP', 207),
                                     PKG_IDENTIFICADORES.fn_valor_configuracion
                                        ( v_subped.identificador_id||'-HFC', 207));
               END IF;

               IF v_tecnologia IS NULL THEN
                  v_tecnologia := PKG_IDENTIFICADORES.fn_tecnologia ( v_subped.identificador_id );
               END IF;
            END IF;

            DBMS_OUTPUT.PUT_LINE('<Tecnología:  ['||v_tecnologia||']');

            -- 2008-07-26   Aacosta   Validación asociada a equipos de televisión que vengan en la solicitud. Si viene uno
            --                        y en el mismo pedido viene una solicitud de componente INSIP o INSHFC no se debe actualizar
            --                        el estado-concepto de la solicitud del equipo.
            OPEN  c_cmp_nuevo_telev (v_subped.subpedido_id);
            FETCH c_cmp_nuevo_telev INTO v_cmp_nuevo_telev;
            CLOSE c_cmp_nuevo_telev;

            -- SECCION.  Actualizacion de estado concepto de solicitudes de DECOS y
            --           Equipos de Red
            IF w_cuenta_equ > 0 AND v_cmp_nuevo_telev = 0 THEN
            
               BEGIN

                  dbms_output.put_line('<ENRUTAR_TELEV) <TRACK 20> Equipos');
                  -- 2007-01-16
                  -- Verificacion del tipo de subpedido, para asignar variable
                  -- v_identificador_nuevo.

                  IF v_subped.tipo_subpedido = 'NUEVO' THEN
                     v_identificador_nuevo := v_subped.identificador_id;
                  ELSE
                     v_identificador_nuevo := v_subped.identificador_id;
                  END IF;
                  
                  --2013-02-19   Yhernana   Antes de actualizar la solicitud a un concepto de órdenes, se hace necesario validar que las características  
                  --                        de Desaprovisionamiento de Equipos se encuentren insertadas en los pedidos de Retiros.  
                  FOR v_tipo_elemen_id_sw IN c_tipo_elemen_id_sw (w_pedido, v_subped.subpedido_id) LOOP
                  
                   --2014-01-27   Mcastaa   se borrá la forma como se estaba obteniendo v_equipoAsoc - fn_valor_caracteristica_trab
                   --2014-01-27   Mcastaa   Obtenemos v_equipoAsoc por medio del nuevo cursor c_validar_retir_trab,
                   --                       donde tambien se valida que sea RETIR.  
                 
                        OPEN c_validar_retir_trab(v_tipo_elemen_id_sw.pedido_id,
                                                  v_tipo_elemen_id_sw.subpedido_id,
                                                  v_tipo_elemen_id_sw.solicitud_id);
                        FETCH c_validar_retir_trab INTO v_equipoAsoc;
                        CLOSE  c_validar_retir_trab;                                       
                                                                    
                      dbms_output.put_line('v_equipoAsoc: '|| v_equipoAsoc);
                      
                      -- 2013-04-25
                      -- Req 24541 - Autoinstalación canales Adicionales
                      -- En caso que no haya un valor para la variable v_tipo_elemen_id_sw.identificador_id se asigna el valor v_identificador_nuevo
                      -- que contiene el identificador del subpedido.  Cuando el identificador viene nulo en la solicitud, la consulta fallaba y se sobre-
                      -- escribían los datos de marca y referencia.
                      
                      IF v_tipo_elemen_id_sw.identificador_id IS NULL THEN
                         v_tipo_elemen_id_sw.identificador_id  := v_identificador_nuevo;                       
                      END IF;
                      
                      IF v_equipoAsoc IS NOT NULL THEN
                         OPEN c_validar_retir_equipo(v_equipoAsoc);
                         FETCH c_validar_retir_equipo INTO v_validar_retir_equipo;
                         IF c_validar_retir_equipo%FOUND THEN

                            dbms_output.put_line('ENTRA pri_insertar_carac_desaprov: '|| v_tipo_elemen_id_sw.pedido_id||'-'||v_tipo_elemen_id_sw.subpedido_id||'-'||v_tipo_elemen_id_sw.solicitud_id||'-'||v_tipo_elemen_id_sw.tipo_elemento_id||'-'||v_tipo_elemen_id_sw.tipo_elemento||'-'||v_tipo_elemen_id_sw.identificador_id);
                            --Procedimiento encargado de insertar características
                            pri_insertar_carac_desaprov(v_tipo_elemen_id_sw.pedido_id,v_tipo_elemen_id_sw.subpedido_id,v_tipo_elemen_id_sw.solicitud_id,
                                                        v_tipo_elemen_id_sw.servicio_id,v_tipo_elemen_id_sw.producto_id,v_tipo_elemen_id_sw.tipo_elemento_id,
                                                        v_tipo_elemen_id_sw.tipo_elemento,v_tipo_elemen_id_sw.identificador_id,v_tipo_elemen_id_sw.municipio_id,
                                                        v_tipo_elemen_id_sw.empresa_id,v_equipoAsoc, w_mensaje_error );

                            IF w_mensaje_error IS NOT NULL THEN
                                w_incons := TRUE;
                            END IF;
                         END IF;
                         CLOSE c_validar_retir_equipo;
                      END IF;
                  END LOOP;
                  
                  
                  -- 2010-02-04 Jaristz Se tiene en cuenta que cuando el tipo de solicitud sea un
                  -- CAMBI los STBOX y EQURED pasen a PORDE.
                  IF v_subped.tipo_subpedido = 'CAMBI' THEN
                      FOR v_tipo_elemen_id_sw IN c_tipo_elemen_id_sw (w_pedido, v_subped.subpedido_id) LOOP

                          v_tipo_equ_sw:= NULL;
                          v_tipo_equ_sw := fn_valor_caracteristica_sol (v_tipo_elemen_id_sw.pedido_id,
                                                                        v_tipo_elemen_id_sw.subpedido_id,
                                                                        v_tipo_elemen_id_sw.solicitud_id,
                                                                        1067
                                                                       );

                         --2011-05-25 ETachej Integridad Fenix OPEN - La solicitud de retiro de DECo debe quedar PORDE.
                         v_tipo_trabajo_equ := fn_tipo_trabajo (v_tipo_elemen_id_sw.pedido_id,
                                                                        v_tipo_elemen_id_sw.subpedido_id,
                                                                        v_tipo_elemen_id_sw.solicitud_id
                                                                       );
                           -- 2011-10-11 Si es retiro de un EQURED debe quedar en PORDE
                          IF (v_tipo_trabajo_equ ='RETIR' AND v_tipo_elemen_id_sw.tipo_elemento_id IN('DECO','EQURED' ))THEN
                             b_retir_deco := TRUE;
                          END IF;

                          b_pedido_tvdigital:=FN_PEDIDO_TVDIGITAL(v_subped.pedido_id,v_subped.subpedido_id,v_subped.identificador_id);

                          -- 2011-11-28 Si es una migración con el EQURED, la tecnología debe ser HFC.
                          -- 2012-07-30 Además de ser una migración, debería tratarse de un pedido de TV digital.
                          IF (b_migracion_hfc AND v_tipo_elemen_id_sw.tipo_elemento_id='EQURED') OR b_pedido_tvdigital THEN
                                    v_tecnologia:='HFC';
                          END IF;

                          IF v_tipo_equ_sw <> 'SWITCH' OR b_retir_deco THEN

                            --2012-09-13  DGiralL - Solo realizar esta actualizacion si la tecnologia es HFC, o si
                            --  siendo la tecnologia REDCO los equipos no son de alta definicion.
                            
                            -- 2014-04-11. REQ 36900 GPON Hogares
                            -- Se agrega la tecnología GPON para considerar el enrutamiento de Equipos.

                            IF v_tecnologia IN ('HFC', 'GPON') OR
                              (v_tecnologia = 'REDCO' AND v_tipo_equ_sw NOT IN ('STBAS','STBAD')) THEN

                                IF NOT w_incons THEN

                                       -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                       bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_tipo_elemen_id_sw.pedido_id);
                            
                                       IF bEsOfertaEspecifica THEN
                                          -- Si es superplay debe verificar si tiene subpedidos pendientes
                                          p_SubPendi := pkg_provision_paquete.fn_Verificar_PendET(v_tipo_elemen_id_sw.pedido_id, v_tipo_elemen_id_sw.subpedido_id);
                                          IF p_SubPendi THEN
                                            v_concepto_ordenes := 'PEXPQ';
                                          ELSE
                                            v_concepto_ordenes := 'PORDE'; 
                                          END IF;
                                       ELSE
                                          p_SubPendi := FALSE;  -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
                                       END IF;
                                       -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico                           
                                       --2013-09-20    aarbob REQ28278_Tangibilizacion
                                       
                                       --2014-01-15 lhernans
                                       --Lógica para inserción de característica que genera eventos DSLAM
                                       
                                       --Se busca municipio de servicio
                                       v_municipio_porta := pkg_solicitudes.fn_valor_caracteristica(
                                                                                v_tipo_elemen_id_sw.pedido_id,
                                                                                v_tipo_elemen_id_sw.subpedido_id,
                                                                                v_tipo_elemen_id_sw.solicitud_id,
                                                                                34
                                                                                );
                                       
                                       --Validación de valor nulo, para búsqueda en portafolio
                                       IF v_municipio_porta IS NULL THEN
                                          --Búsqueda de municipio en portafolio
                                          v_municipio_porta := pkg_identificadores.fn_valor_configuracion(
                                                    v_tipo_elemen_id_sw.identificador_id,
                                                    34
                                                    );
                                       END IF;
                                                                                                                                                 
                                       --Se busca el municipio de referencia
                                       v_mun_salida := pkg_prov_inter_util.fn_munic_salida(
                                                                    NVL (v_municipio_porta, 
                                                                       v_tipo_elemen_id_sw.municipio_id
                                                                    ));
                                                                    
                                       --Validación si debe insertar o no etiqueta
                                       --2014-10-22   RVELILLR Condicional de tecnologia para consulta de etiquetas para generar eventos.
                                       IF NVL(v_tecnologia, 'NOT') = 'REDCO' THEN
                                            --2014-10-15   MGATTIS
                                            v_aplicacion := 'DSLAM';            
                                        
                                            --Se recupera valor de la etiqueta
                                            v_etiqueta := PKG_ETIQUETAS_EVENTOS_BTS.FN_VALOR_PARAM_EVENTOS_BTS (
                                                                            v_aplicacion,
                                                                            v_tipo_elemen_id_sw.producto_id,
                                                                            v_tipo_elemen_id_sw.tipo_elemento_id,                                                
                                                                            'CAMBI',
                                                                            200
                                                                            );
                                            --2014-10-15   MGATTIS fin lógica
                                       ELSE
                                           --Se recupera valor de la etiqueta
                                           -- El dominio se forma con: ETBTS_[MUNICIPIO]_[PRODUCTO]_[TECNOLOGIA]
                                           -- (Sin corchetes)
                                           v_etiqueta := fn_dominio_values_meaning (
                                                                'ETBTS'
                                                                   || '_'
                                                                   || v_mun_salida
                                                                   || '_'
                                                                   || v_tipo_elemen_id_sw.producto_id
                                                                   || '_'
                                                                   || v_tecnologia,
                                                                'CAMBI',
                                                                200
                                                                );
                                       END IF;                        
                                       --2014-10-22   RVELILLR FIN Lógica.
                                       
                                       --Validación de la etiqueta retornada
                                       IF NVL (v_etiqueta, 'NA') <> 'NA' THEN
                                          --Rutina de inserción de etiquetas
                                          pr_insertar_etiquetas_bts (
                                                            v_tipo_elemen_id_sw.pedido_id,
                                                            v_tipo_elemen_id_sw.subpedido_id,
                                                            v_tipo_elemen_id_sw.solicitud_id,
                                                            v_tipo_elemen_id_sw.servicio_id,
                                                            v_tipo_elemen_id_sw.producto_id,
                                                            v_tipo_elemen_id_sw.tipo_elemento,
                                                            v_tipo_elemen_id_sw.tipo_elemento_id,
                                                            4805,
                                                            v_tipo_elemen_id_sw.municipio_id,
                                                            v_tipo_elemen_id_sw.empresa_id,
                                                            v_etiqueta,
                                                            p_mensaje
                                                            );
                                       END IF;
                                       --2014-01-15 lhernans FIN Lógica.
                                       
                                       -- Obtener los datos del equipo terminal actual asociado al identificador.
                                        OPEN  c_equipo_carac (v_identificador_nuevo, v_equipoAsoc);
                                        FETCH c_equipo_carac INTO v_carac_equipo;
                                        CLOSE c_equipo_carac;
                                                                       
                                       v_propiedadEqu := NVL(FN_VALOR_CAR_EN_CONFIG_EQUIPOS(v_equipoAsoc,1093,v_carac_equipo.marca_id),'X');       
                                       
                                       
                                          
                                            
                                       v_tipo_trabajo:= nvl(pkg_solicitudes.fn_tipo_trabajo (v_tipo_elemen_id_sw.pedido_id,
                                                                    v_tipo_elemen_id_sw.subpedido_id,
                                                                    v_tipo_elemen_id_sw.solicitud_id,
                                                                    200),'X');
                                       --2014-02-04    aarbob      REQ34242_MejoTang
                                           
                                       IF v_propiedadEqu = 'CM' AND v_tipo_trabajo = 'RETIR' THEN   
                                           
                                           --se actualiza el estado del decodificador a RES con el fin de que sea a futuro recuperable
                                          UPDATE fnx_equipos
                                             SET ESTADO = 'RES'
                                           WHERE MARCA_ID = v_carac_equipo.marca_id
                                             AND REFERENCIA_ID = v_carac_equipo.referencia_id
                                             AND TIPO_ELEMENTO_ID = v_carac_equipo.tipo_elemento_id
                                             AND TIPO_ELEMENTO = v_carac_equipo.tipo_elemento
                                             AND EQUIPO_ID = v_equipoAsoc;
                                           
                                            ---se actualiza para dejar LOG que informe que no se genera solictud de recogida de equipo ya que el equipo es comprado por el cliente
                                            UPDATE FNX_PEDIDOS
                                            SET observacion =  SUBSTR ('Desap-NoGeneraSoli RecupEquip' ||v_equipoAsoc||';'|| observacion,1,200)
                                            WHERE pedido_id     = v_tipo_elemen_id_sw.pedido_id; 
                                        
                                            /*     
                                            UPDATE FNX_SOLICITUDES
                                                 SET estado_id     = 'CUMPL',
                                                     estado_soli   = 'DEFIN',
                                                     concepto_id   = 'CUMPL',
                                                     tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                                     identificador_id = v_identificador_nuevo,
                                                     observacion   = ' <*TV1 [1.'||'CUMPL'||']> '||observacion
                                               WHERE pedido_id     = v_tipo_elemen_id_sw.pedido_id
                                               AND   subpedido_id  = v_tipo_elemen_id_sw.subpedido_id
                                               AND   solicitud_id  = v_tipo_elemen_id_sw.solicitud_id
                                               AND   servicio_id   = 'ENTRE'
                                               AND   producto_id   = 'TELEV'
                                               AND   tipo_elemento = 'EQU'
                                               --AND   tipo_elemento_id = 'STBOX'
                                               AND   concepto_id   IN ('PETEC', 'PRACC');*/
                                        END IF;    
                                             
                                        --ELSE 
                                                                                                                                                                                
                                              UPDATE FNX_SOLICITUDES
                                                 SET estado_id     = 'ORDEN',
                                                     estado_soli   = 'DEFIN',
                                                     concepto_id   = v_concepto_ordenes,
                                                     tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                                     identificador_id = v_identificador_nuevo,
                                                     observacion   = ' <*TV1 [1.'||v_concepto_ordenes||']> '||observacion
                                               WHERE pedido_id     = v_tipo_elemen_id_sw.pedido_id
                                               AND   subpedido_id  = v_tipo_elemen_id_sw.subpedido_id
                                               AND   solicitud_id  = v_tipo_elemen_id_sw.solicitud_id
                                               AND   servicio_id   = 'ENTRE'
                                               AND   producto_id   = 'TELEV'
                                               AND   tipo_elemento = 'EQU'
                                               --AND   tipo_elemento_id = 'STBOX'
                                               AND   concepto_id   IN ('PETEC', 'PRACC');
                                               
                                       --END IF;      

                                       -- 2013-01-24: Req20216: Mmedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico 
                                        IF bEsOfertaEspecifica AND NOT p_SubPendi THEN
                                            pkg_provision_paquete.pr_genorden_pexpq (v_tipo_elemen_id_sw.pedido_id, v_tipo_elemen_id_sw.tipo_elemento_id, p_mensaje);
                                        END IF;                       
                                        -- 2013-01-24: Req20216: Mmedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico  
                                END IF;

                            END IF;

                          ELSE

                              UPDATE FNX_SOLICITUDES
                                 SET estado_id     = 'ORDEN',
                                     estado_soli   = 'DEFIN',
                                     concepto_id   = 'POPTO',
                                     tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                     identificador_id = v_identificador_nuevo,
                                     observacion   = ' <*TV1 [2.'||v_concepto_ordenes||']> '||observacion
                               WHERE pedido_id     = v_tipo_elemen_id_sw.pedido_id
                               AND   subpedido_id  = v_tipo_elemen_id_sw.subpedido_id
                               AND   solicitud_id  = v_tipo_elemen_id_sw.solicitud_id
                               AND   servicio_id   = 'ENTRE'
                               AND   producto_id   = 'TELEV'
                               AND   tipo_elemento = 'EQU'
                               --AND   tipo_elemento_id = 'STBOX'
                               AND   concepto_id   IN ('PETEC', 'PRACC');

                          END IF;
                      END LOOP;

                  ELSE
                        -- 2011-09-20 En caso de no ser un pedido de tv digital, se hace lo que actualmente,
                        -- realiza el procedimiento, en caso contrario, se colocan en PORDE todos los equipos.
                           b_pedido_tvdigital  := FN_PEDIDO_TVDIGITAL(v_subped.pedido_id,v_subped.subpedido_id,v_subped.identificador_id);
                        
                        FOR v_tipo_elemen_id_sw IN c_tipo_elemen_id_sw (v_subped.pedido_id,v_subped.subpedido_id) LOOP
                            
                              v_tipo_equ_sw:= NULL;
                              v_tipo_equ_sw := fn_valor_caracteristica_sol (v_subped.pedido_id, v_subped.subpedido_id,v_tipo_elemen_id_sw.solicitud_id,1067);    
                                                
                                                                                               
                            IF NOT b_pedido_tvdigital THEN
                                    
                                --2012-09-13  DGiralL - Solo realizar esta actualizacion si la tecnologia es HFC, o si
                                --  siendo la tecnologia REDCO los equipos no son de alta definicion    ---- Ultimo ajuste
                                -- 2013-04-02    Sbuitrab    Las solicitudes de retiros de equipos ANALOGOS deben pasar a PORDE 

                                -- 2014-04-11. REQ 36900 GPON Hogares
                                -- Se agrega la tecnología GPON para considerar el enrutamiento de Equipos.

                                IF v_tecnologia IN ('HFC', 'GPON') OR 
                                  (v_tecnologia = 'REDCO' AND v_tipo_equ_sw NOT IN ('STBAS','STBAD')) OR
                                   v_subped.tipo_subpedido = 'RETIR'
                                THEN

                                   -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                   bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_subped.pedido_id);
                                
                                   IF bEsOfertaEspecifica THEN     
                                       -- Si es superplay debe verificar si tiene subpedidos pendientes                          
                                       p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_subped.pedido_id, v_subped.subpedido_id);
                                       IF p_SubPendi THEN
                                            v_concepto_ordenes := 'PEXPQ';
                                       ELSE
                                            v_concepto_ordenes := 'PORDE';
                                       END IF;
                                   ELSE
                                       p_SubPendi := FALSE; -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
                                   END IF;
                                  -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico

                                  -- 2006-07-28
                                  -- Actualizacion de campo identificador_id_nuevo, estado y concepto en la solicitud
                                  -- del primer Equipo.
                                  UPDATE FNX_SOLICITUDES
                                     SET estado_id     = 'ORDEN',
                                         estado_soli   = 'DEFIN',
                                         concepto_id   = v_concepto_ordenes,
                                         tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                         identificador_id = v_identificador_nuevo,
                                         observacion   = ' <*TV1 [3.'||v_concepto_ordenes||']> '||observacion
                                   WHERE pedido_id     = v_subped.pedido_id
                                   AND   subpedido_id  = v_subped.subpedido_id
                                   AND   solicitud_id  = v_tipo_elemen_id_sw.solicitud_id        
                                   AND   servicio_id   = 'ENTRE'
                                   AND   producto_id   = 'TELEV'
                                   AND   tipo_elemento = 'EQU'
                                   AND   tipo_elemento_id IN ('DECO', 'EQURED', 'STBOX')
                                   AND   concepto_id   IN ('PETEC', 'PRACC');
                                    
                                   -- 2013-01-24: Req20216: Mmedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico 
                                    IF bEsOfertaEspecifica AND NOT p_SubPendi THEN
                                        pkg_provision_paquete.pr_genorden_pexpq (v_subped.pedido_id, v_solic_cmp.tipo_elemento_id, p_mensaje);
                                    END IF;                       
                                    -- 2013-01-24: Req20216: Mmedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico

                                END IF;

                            ELSIF b_pedido_tvdigital THEN
                            -- 2012-08-23 Se crea cursor para verificar el tipo de tecnología, esto pasa
                            -- cuando es un pedido mixto que tienen televisión hfc digital.
                                    -- 2012-05-14 Se agrega la condición para colocar la tecnología
                                    -- de la solicitud de equipo.
                                    v_tecnologia:=pkg_solicitudes.fn_valor_caracteristica(v_subped.pedido_id,v_subped.subpedido_id,v_tipo_elemen_id_sw.solicitud_id,33);
                                    
                                   -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                   bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_subped.pedido_id);
                                
                                   IF bEsOfertaEspecifica THEN     
                                       -- Si es superplay debe verificar si tiene subpedidos pendientes                          
                                       p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_subped.pedido_id, v_subped.subpedido_id);
                                       IF p_SubPendi THEN
                                            v_concepto_ordenes := 'PEXPQ';
                                       ELSE
                                            v_concepto_ordenes := 'PORDE';         
                                       END IF;
                                   ELSE
                                       p_SubPendi := FALSE; -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
                                   END IF;
                                   -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                    
                                      UPDATE FNX_SOLICITUDES
                                         SET estado_id     = 'ORDEN',
                                             estado_soli   = 'DEFIN',
                                             concepto_id   = v_concepto_ordenes,
                                             tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                             identificador_id = v_identificador_nuevo,
                                             observacion   = ' <*TVD1 ['||v_concepto_ordenes||']> '||observacion
                                       WHERE pedido_id     = v_subped.pedido_id
                                       AND   subpedido_id  = v_subped.subpedido_id
                                       AND   servicio_id   = 'ENTRE'
                                       AND   producto_id   = 'TELEV'
                                       AND   tipo_elemento = 'EQU'
                                       AND   tipo_elemento_id IN ('DECO', 'EQURED')
                                       AND   concepto_id   IN ('PETEC', 'PRACC');

                                       -- 2013-01-28: Mmedinac: Req20216: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                       IF bEsOfertaEspecifica AND NOT p_SubPendi THEN
                                        pkg_provision_paquete.pr_genorden_pexpq (v_subped.pedido_id, v_solic_cmp.tipo_elemento_id, p_mensaje);                             
                                       END IF;
                                       -- 2013-01-28: Mmedinac: Req20216:FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico 

                                       -- 2012-08-28 Se actualiza la tecnología de la solicitud para tener
                                       -- en cuenta los equipos que no son HFC.
                                       FOR reg in C_EQUIPOS_TECNOLOGIA(v_subped.pedido_id,v_subped.subpedido_id,200) LOOP

                                            IF NVL(reg.valor,'N')='N' THEN
                                                  v_tecnologia:=pkg_solicitudes.fn_valor_caracteristica(v_subped.pedido_id,v_subped.subpedido_id,v_solic_cmp.solicitud_id,33);
                                            ELSIF NVL(reg.valor,'N')<>'N' THEN
                                                  OPEN c_equipo_config(reg.valor,33);
                                                  FETCH c_equipo_config INTO v_tecnologia;
                                                  CLOSE c_equipo_config;
                                            END IF;

                                              UPDATE FNX_SOLICITUDES
                                                  SET tecnologia_id = NVL (v_tecnologia, 'HFC')
                                                WHERE pedido_id     = v_subped.pedido_id
                                                  AND subpedido_id  = v_subped.subpedido_id
                                                  AND solicitud_id  = reg.solicitud_id;
                                       END LOOP;

                              -- AND   ROWNUM        = 1;
                            END IF;
                            
                        END LOOP;    

                  END IF;

                   b_orden_generada    := TRUE;

               EXCEPTION
                  WHEN OTHERS THEN
                      BEGIN
                         DBMS_OUTPUT.Put_Line('<ERROR> '||SUBSTR(SQLERRM, 1, 200));
                         v_men:= SUBSTR(SQLERRM, 1, 900);

                         IF v_subped.tipo_subpedido = 'CAMBI' THEN

                             FOR v_tipo_elemen_id_sw IN c_tipo_elemen_id_sw (w_pedido, v_subped.subpedido_id) LOOP

                                 v_tipo_equ_sw:= NULL;
                                 v_tipo_equ_sw := fn_valor_caracteristica_sol (v_tipo_elemen_id_sw.pedido_id,
                                                                               v_tipo_elemen_id_sw.subpedido_id,
                                                                               v_tipo_elemen_id_sw.solicitud_id,
                                                                               1067
                                                                              );
                                 IF v_tipo_equ_sw <> 'SWITCH' THEN

                                     UPDATE FNX_SOLICITUDES
                                     SET    estado_id   = 'ORDEN',
                                            estado_soli = 'DEFIN',
                                            concepto_id = 'APROB',
                                            tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                            identificador_id = v_identificador_nuevo,
                                            observacion = '<* TV1 [APROB]>'||observacion
                                     WHERE  pedido_id     = v_tipo_elemen_id_sw.pedido_id
                                     AND    subpedido_id  = v_tipo_elemen_id_sw.subpedido_id
                                     AND    solicitud_id  = v_tipo_elemen_id_sw.solicitud_id
                                     AND    servicio_id   = 'ENTRE'
                                     AND    producto_id   = 'TELEV'
                                     AND    tipo_elemento = 'EQU'
                                     --AND    tipo_elemento_id = 'STBOX'
                                     AND    concepto_id   IN ('PETEC', 'PRACC');

                                 ELSE

                                     UPDATE FNX_SOLICITUDES
                                     SET    estado_id   = 'ORDEN',
                                            estado_soli = 'DEFIN',
                                            concepto_id = 'APROB',
                                            tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                            identificador_id = v_identificador_nuevo,
                                            observacion = '<* TV1 [APROB]>'||observacion
                                     WHERE  pedido_id     = v_tipo_elemen_id_sw.pedido_id
                                     AND    subpedido_id  = v_tipo_elemen_id_sw.subpedido_id
                                     AND    solicitud_id  = v_tipo_elemen_id_sw.solicitud_id
                                     AND    servicio_id   = 'ENTRE'
                                     AND    producto_id   = 'TELEV'
                                     AND    tipo_elemento = 'EQU'
                                     --AND    tipo_elemento_id IN ('DECO', 'EQURED')
                                     AND    concepto_id   IN ('PETEC', 'PRACC');

                                 END IF;

                             END LOOP;

                         ELSE
                             UPDATE FNX_SOLICITUDES
                             SET    estado_id   = 'ORDEN',
                                    estado_soli = 'DEFIN',
                                    concepto_id = 'APROB',
                                    tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                    identificador_id = v_identificador_nuevo,
                                    observacion = '<* TV1 [APROB]>'||observacion
                             WHERE  pedido_id   = v_subped.pedido_id
                             AND    subpedido_id  = v_subped.subpedido_id
                             AND    servicio_id   = 'ENTRE'
                             AND    producto_id   = 'TELEV'
                             AND    tipo_elemento = 'EQU'
                             AND    tipo_elemento_id IN ('DECO', 'EQURED', 'STBOX')
                             AND    concepto_id   IN ('PETEC', 'PRACC')
                             AND    ROWNUM        = 1;

                         END IF;

                         b_orden_generada    := TRUE;

                      EXCEPTION
                         WHEN OTHERS THEN
                            w_incons := TRUE;
                            w_mensaje_error := '<ENRUTAR_TELEV) Error actualizando solic APROB '||SUBSTR(SQLERRM, 1, 15);
                            v_men := SQLERRM;
                      END;
               END;

               -- 2007-09-12
               -- Inserción / actualización característica 90 en solicitudes de EQU
               IF NOT w_incons AND v_subped.tipo_subpedido IN ('NUEVO', 'CAMBI')
               THEN
                 BEGIN
                     -- Actualización caracteristica 90 Identificador asociado
                     UPDATE FNX_CARACTERISTICA_SOLICITUDES
                     SET    valor        = v_subped.identificador_id
                     WHERE  pedido_id    = v_subped.pedido_id
                     AND    subpedido_id = v_subped.subpedido_id
                     AND    caracteristica_id = 90
                     AND    servicio_id   = 'ENTRE'
                     AND    producto_id   = 'TELEV'
                     AND    tipo_elemento = 'EQU';

                     DBMS_OUTPUT.put_line('<ENRUTAR_TELEV) <TRACK 30>'||TO_CHAR(SQL%ROWCOUNT));
                 EXCEPTION
                     WHEN OTHERS THEN
                         NULL;
                 END;
               END IF;

               -- 2006-08-23
               -- Actualizacion de campo identificador_id_nuevo, estado y concepto en las
               -- otras solicitudes de Equipos del subpedido.
               IF NOT w_incons THEN

                  -- 2011-09-26 Solo se pasan a POPTO los demas equipos, cuando se trate
                  -- de un pedido que no sea televisión digital HFC.
                  b_pedido_tvdigital  := FN_PEDIDO_TVDIGITAL(v_subped.pedido_id,v_subped.subpedido_id,
                                                               v_subped.identificador_id);
                  IF v_subped.tipo_subpedido <> 'CAMBI' THEN
                        IF NOT b_pedido_tvdigital THEN
                                -- 2012-05-02 Se verifica los trabajos para PPV, SERHFC y EQU
                                -- en caso de estar retirándolos, se debe colocar en PORDE el SERHFC.
                                -- y los equipos que traiga en PORDE.
                                OPEN C_TRABAJOS_RET_DECO (w_pedido,v_subped.subpedido_id,'SERHFC',1);
                                FETCH C_TRABAJOS_RET_DECO INTO V_TRABAJOS_SERHFC;
                                CLOSE C_TRABAJOS_RET_DECO;

                                IF V_TRABAJOS_SERHFC>0 THEN
                                    b_tiene_retiro_serhfc:=TRUE;
                                END IF;

                                OPEN C_TRABAJOS_RET_DECO (w_pedido,v_subped.subpedido_id,'PPV',1);
                                FETCH C_TRABAJOS_RET_DECO INTO V_TRABAJOS_PPV;
                                CLOSE C_TRABAJOS_RET_DECO;

                                IF V_TRABAJOS_PPV>0 THEN
                                    b_tiene_retiro_ppv:=TRUE;
                                END IF;

                                OPEN C_TRABAJOS_RET_DECO (w_pedido,v_subped.subpedido_id,'EQURED',200);
                                FETCH C_TRABAJOS_RET_DECO INTO V_TRABAJOS_EQU;
                                CLOSE C_TRABAJOS_RET_DECO;

                                IF V_TRABAJOS_EQU>0 THEN
                                    b_tiene_retiro_equ:=TRUE;
                                END IF;

                                IF b_tiene_retiro_ppv
                                AND b_tiene_retiro_serhfc
                                AND b_tiene_retiro_equ THEN
                               -- AND v_solic_cmp.TIPO_ELEMENTO_ID='SERHFC' THEN
                                    
                                    v_concepto_equipos := 'PORDE';

                                    -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                   bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_subped.pedido_id);
                            
                                   IF bEsOfertaEspecifica THEN     
                                       -- Si es superplay debe verificar si tiene subpedidos pendientes                          
                                       p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_subped.pedido_id, v_subped.subpedido_id);
                                       IF p_SubPendi AND v_concepto_equipos NOT IN ('PORDE','POPTO','PECOM','PUMED') THEN
                                          v_concepto_equipos := 'PEXPQ';
                                       ELSE
                                          IF v_concepto_equipos NOT IN ('PORDE','POPTO','PECOM','PUMED') THEN
                                            v_concepto_equipos := 'PORDE';
                                          END IF;
                                       END IF;
                                   ELSE
                                       p_SubPendi := FALSE; -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
                                   END IF;                                    
                                   -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                ELSE
                                dbms_output.put_line('POPTO 1 ');
                                      v_concepto_equipos := 'POPTO';
                                END IF;

                                --2012-09-13  DGiralL - Solo realizar esta actualizacion si la tecnologia es HFC, o si
                                --  siendo la tecnologia REDCO los equipos no son de alta definicion

                                -- 2014-04-11. REQ 36900 GPON Hogares
                                -- Se agrega la tecnología GPON para considerar el enrutamiento de Equipos.
                            
                                IF v_tecnologia IN ('HFC', 'GPON') OR
                                  (v_tecnologia = 'REDCO' AND v_tipo_equ_sw NOT IN ('STBAS','STBAD')) THEN

                                  UPDATE FNX_SOLICITUDES
                                     SET estado_id     = 'ORDEN',
                                     estado_soli   = 'DEFIN',
                                         concepto_id   = v_concepto_equipos,
                                         tecnologia_id = NVL (v_tecnologia, 'HFC'),
                                         identificador_id = v_identificador_nuevo,
                                         observacion   = '<*TV1 '||v_concepto_equipos||'> '||observacion
                                   WHERE pedido_id     = v_subped.pedido_id
                                   AND   subpedido_id  = v_subped.subpedido_id
                                   AND   servicio_id   = 'ENTRE'
                                   AND   producto_id   = 'TELEV'
                                   AND   tipo_elemento = 'EQU'
                                   AND   tipo_elemento_id IN ('DECO', 'EQURED', 'STBOX')
                                   AND   concepto_id   IN ('PETEC', 'PRUTA', 'PRACC');

                                   -- 2013-01-28: Req20216: Mmedinac INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                   IF bEsOfertaEspecifica AND NOT p_SubPendi THEN
                                     pkg_provision_paquete.pr_genorden_pexpq (v_subped.pedido_id, v_solic_cmp.tipo_elemento_id, p_mensaje);
                                   END IF;
                                   -- 2013-01-28: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico

                                END IF;

                          END IF;

                  END IF;
               END IF;

            END IF;
            -- FIN SECCION.  Actualizacion de estado concepto solicitudes DECOS y EQURED

            -- SECCION.  Actualizacion de estado concepto solicitudes CANALES
            IF w_cuenta_cmp > 0 AND NOT w_incons
            THEN

               OPEN  c_solic_cmp(v_subped.pedido_id, v_subped.subpedido_id);
               FETCH c_solic_cmp INTO v_solic_cmp;

               LOOP
                  EXIT WHEN c_solic_cmp%NOTFOUND;

                  OPEN  c_ident ( v_subped.identificador_id );
                  FETCH c_ident INTO v_ident;
                  CLOSE c_ident;

                  IF v_ident.identificador_id IS NULL THEN
                     w_incons := TRUE;
                     w_mensaje_error := '<ENRUTAR_TELEV) El identificador del subpedido es nulo';
                  END IF;

                   -- 2006-11-19
                   -- SECCION.  Construccion identificador de componente.
                   -- 2008-09-09  Aacosta  Se incluye la validación del tipo de solicitud en reportes instalación
                   --                      para los casos de PPV en los cuales en fnx_subpedidos el tipo de subpedido
                   --                      viene como CAMBI, y no permite la provisión de este componente.
                  IF NOT w_incons AND (v_subped.tipo_subpedido = 'NUEVO' OR v_solic_cmp.tipo_solicitud = 'NUEVO')
                  THEN
                     IF v_solic_cmp.tipo_elemento_id <> 'PPV' THEN
                        v_identificador_cmp := v_subped.identificador_id
                                               || '-'|| v_solic_cmp.tipo_elemento_id;
                                               dbms_output.put_line('v_identificador_cmp '||v_identificador_cmp);
                     ELSE
                          BEGIN
                             -- Conteo de identificadores PayPerViews existentes.
                             OPEN  c_conteo_payperviews (v_subped.identificador_id);
                             FETCH c_conteo_payperviews INTO v_total_payperviews;
                             IF c_conteo_payperviews%NOTFOUND THEN
                                v_total_payperviews := 0;
                             END IF;
                             CLOSE c_conteo_payperviews;

                             -- Armar el identificador de PayPerView.
                             v_identificador_cmp := v_subped.identificador_id||'-PPV'||
                                                 LPAD(TO_CHAR(v_total_payperviews + 1), 2, '0');
                          END;
                     END IF;

                     -- Creacion del identificador de Componente.
                     BEGIN
                           INSERT INTO FNX_IDENTIFICADORES
                                    ( identificador_id, agrupador_id,
                                      estado, fecha_estado, serie_id,
                                      servicio_id, producto_id,
                                      tipo_elemento_id, tipo_elemento,
                                      empresa_id,municipio_id)
                           VALUES   ( v_identificador_cmp, v_subped.identificador_id,
                                      'PRE', SYSDATE, v_ident.serie_id,
                                      v_ident.servicio_id, v_ident.producto_id,
                                      v_solic_cmp.tipo_elemento_id, 'CMP',
                                      v_solic_cmp.empresa_id, v_solic_cmp.municipio_id);
                     EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                           UPDATE FNX_IDENTIFICADORES
                           SET    estado = 'PRE'
                           WHERE  identificador_id = v_identificador_cmp;
                        WHEN OTHERS THEN
                           NULL;
                     END;
                  ELSE
                     v_identificador_cmp := NULL;
                  END IF;

                  IF NOT w_incons THEN
                     -- SECCION.  Determinar concepto de solicitud
                     -- Si la orden no fue generada, el subpedido no tiene Decodificador y
                     -- la orden se debe generar con la primera solicitud de canal premium
                     IF NOT b_orden_generada AND v_solic_cmp.tipo_elemento_id <> 'PPV'
                     THEN
                          --Lquintez 2009-06-19 se coloca la condición de que si hay cambio de tipo de televisión no
                          --                    pase el INSIP o el INSHFC a PORDE
                          IF v_cambio_plan_telev IS NOT NULL AND v_solic_cmp.tipo_elemento_id IN ('SERVIP','SERHFC')  THEN
                            v_concepto_ordenes := 'POPTO';
                            dbms_output.put_line('POPTO 2 ');
                          ELSIF v_identificador_pack IS NULL THEN

                            v_sol_servip:= 0;
                            OPEN c_sol_servip (v_solic_cmp.pedido_id, v_solic_cmp.subpedido_id);
                            FETCH c_sol_servip INTO v_sol_servip;
                            CLOSE c_sol_servip;

                             -- 2010-02-04 Jaristz Cuando el tipo de solicitus sea CAMBI el concepto
                             -- del cmp SERVIP debe ser POPTO.
                             IF v_solic_cmp.tipo_solicitud = 'CAMBI' AND v_solic_cmp.tipo_elemento_id = 'SERVIP' THEN
                                IF v_sol_servip > 0 THEN
                                   -- 2012-10-11 Se realiza el ajuste relacionado con el tema de siembra digital
                                   OPEN C_SIEMDIG;
                                   FETCH C_SIEMDIG INTO V_SIEMDIG;
                                   CLOSE C_SIEMDIG;

                                   OPEN C_EQUIPOSC_SIEM;
                                   FETCH C_EQUIPOSC_SIEM INTO V_EQUIPOS_SIEM;
                                   CLOSE C_EQUIPOSC_SIEM;

                                   IF NVL(V_SIEMDIG,0) > 0 AND NVL(V_EQUIPOS,0) = 0 THEN
                                        v_concepto_ordenes := 'PORDE';
                                   ELSE
                                        v_concepto_ordenes := 'POPTO';
                                                                       dbms_output.put_line('POPTO 3 ');
                                   END IF;
                                ELSE
                                    v_concepto_ordenes := 'PORDE';
                                END IF;
                             ELSE
                                v_concepto_ordenes := 'PORDE';
                                    -- 2011-08-04 Para el tipo de elemento ELSIS, la tecnología que se debe
                                    -- trabajar es LOGIC.
                                    IF v_solic_cmp.tipo_elemento_id = 'ELSIS' THEN
                                        v_tecnologia            :='LOGIC';
                                         v_identificador_cmp := v_subped.identificador_id
                                               || '-'|| v_solic_cmp.tipo_elemento_id;
                                    END IF;

                             END IF;

                          ELSE
                             v_concepto_ordenes := 'PEXPQ';
                          END IF;
                          b_orden_generada := TRUE;
                     ELSIF b_orden_generada AND v_solic_cmp.tipo_elemento_id <> 'PPV'
                     THEN

                          v_sol_servip:= 0;
                          OPEN c_sol_servip (v_solic_cmp.pedido_id, v_solic_cmp.subpedido_id);
                          FETCH c_sol_servip INTO v_sol_servip;
                          CLOSE c_sol_servip;

                          -- 2010-01-13 Se modifica la instrucción v_concepto_ordenes := 'POPTO';
                          -- para el tema de STBOX de Alta definición. Se coloca en PINSC el SERVIP
                          IF v_solic_cmp.tipo_elemento_id = 'SERVIP' THEN
                                -- 2010-02-04 Jaristz Cuando el tipo de solicitus sea CAMBI el concepto
                                -- del cmp SERVIP debe ser POPTO.
                                IF v_solic_cmp.tipo_solicitud = 'CAMBI' THEN

                                    IF v_sol_servip > 0 THEN
                                        v_concepto_ordenes := 'POPTO';
                                                                        dbms_output.put_line('POPTO 4 ');
                                    ELSE
                                        v_concepto_ordenes := 'PORDE';
                                         dbms_output.put_line('VAL 4 ');
                                    END IF;
                                ELSE
                                    --2010-08-31 Se cambia el concepto a POPTO de acuerdo a requerimiento 1371892.
                                    IF v_solic_cmp.tipo_solicitud = 'NUEVO' THEN
                                        v_concepto_ordenes := 'POPTO';
                                                                        dbms_output.put_line('POPTO 5 ');
                                    ELSE
                                        v_concepto_ordenes := 'PINSC';
                                    END IF;

                                END IF;
                          ELSE
                                -- 2012-04-26 Se verifica los trabajos para PPV, SERHFC y EQU
                                -- en caso de estar retirándolos, se debe colocar en PORDE el SERHFC.
                                OPEN C_TRABAJOS_RET_DECO (w_pedido,v_subped.subpedido_id,'SERHFC',1);
                                FETCH C_TRABAJOS_RET_DECO INTO V_TRABAJOS_SERHFC;
                                CLOSE C_TRABAJOS_RET_DECO;

                                IF V_TRABAJOS_SERHFC>0 THEN
                                    b_tiene_retiro_serhfc:=TRUE;
                                END IF;

                                OPEN C_TRABAJOS_RET_DECO (w_pedido,v_subped.subpedido_id,'PPV',1);
                                FETCH C_TRABAJOS_RET_DECO INTO V_TRABAJOS_PPV;
                                CLOSE C_TRABAJOS_RET_DECO;

                                IF V_TRABAJOS_PPV>0 THEN
                                    b_tiene_retiro_ppv:=TRUE;
                                END IF;

                                OPEN C_TRABAJOS_RET_DECO (w_pedido,v_subped.subpedido_id,'EQURED',200);
                                FETCH C_TRABAJOS_RET_DECO INTO V_TRABAJOS_EQU;
                                CLOSE C_TRABAJOS_RET_DECO;

                                IF V_TRABAJOS_EQU>0 THEN
                                    b_tiene_retiro_equ:=TRUE;
                                END IF;

                                IF b_tiene_retiro_ppv
                                AND b_tiene_retiro_serhfc
                                AND b_tiene_retiro_equ
                                AND v_solic_cmp.TIPO_ELEMENTO_ID='SERHFC' THEN
                                     v_concepto_ordenes := 'PORDE';
                                ELSE
                                    
                                 --2013-09-20    aarbob REQ28278_Tangibilizacion
                                 --Si el equipo es comprado se debe dejar la solictud del equipo en CUMPL - CUMPL
                                 --y el estado de los componentes en ORDEN - PORDE
                                   IF v_propiedadEqu = 'CM' THEN
                                   
                                       v_concepto_ordenes:='PORDE';
                                   ELSE       
                                    
                                       v_concepto_ordenes := 'POPTO';
                                                                       dbms_output.put_line('POPTO 6 ');
                                
                                END IF;
                                
                                END IF;
                          END IF;

                     ELSIF v_solic_cmp.tipo_elemento_id = 'PPV'
                     THEN
                         IF w_cuenta_equ > 0 THEN
                           v_concepto_ordenes := 'PINSC';
                         ELSE
                           v_concepto_ordenes := 'PORDE';
                            dbms_output.put_line('VAL 5 ');
                         END IF;
                     END IF;

                     IF (v_subped.tipo_subpedido = 'NUEVO' OR v_solic_cmp.tipo_solicitud = 'NUEVO') THEN
                         -- Insercion caracteristica 1 Identificador
                         PKG_PEDIDOS_UTIL.pr_insupd_caracteristica
                           ( v_subped.pedido_id, v_subped.subpedido_id, v_solic_cmp.solicitud_id
                            ,v_solic_cmp.servicio_id, v_solic_cmp.producto_id, v_solic_cmp.tipo_elemento
                            ,v_solic_cmp.tipo_elemento_id, 1
                            ,v_identificador_cmp
                            ,v_solic_cmp.municipio_id, v_solic_cmp.empresa_id
                            ,p_mensaje );
                     END IF;

                     IF v_subped.tipo_subpedido IN ('NUEVO', 'CAMBI') THEN
                         -- Insercion caracteristica 90 Identificador asociado
                         PKG_PEDIDOS_UTIL.pr_insupd_caracteristica
                           ( v_subped.pedido_id, v_subped.subpedido_id, v_solic_cmp.solicitud_id
                            ,v_solic_cmp.servicio_id, v_solic_cmp.producto_id, v_solic_cmp.tipo_elemento
                            ,v_solic_cmp.tipo_elemento_id, 90, v_subped.identificador_id
                            ,v_solic_cmp.municipio_id, v_solic_cmp.empresa_id
                            ,p_mensaje );
                     END IF;

                     b_pedido_tvdigital  := FN_PEDIDO_TVDIGITAL(v_subped.pedido_id,v_subped.subpedido_id,
                                                                v_subped.identificador_id);
                     b_tiene_decos_hfc:= FN_TV_DECOS_ASOCIADOS(v_subped.identificador_id,'HFC');

                     -- 2011-11-28 En caso de ser un SERHFC la tecnología es HFC, en caso de
                     -- ser un SERVIP, la tecnología deber ser REDCO.

                     IF b_migracion_hfc AND v_solic_cmp.tipo_elemento_id='SERHFC' THEN
                        v_tecnologia:='HFC';
                     END IF;

                     IF b_migracion_hfc AND v_solic_cmp.tipo_elemento_id='SERVIP' THEN
                        v_tecnologia:='REDCO';
                     END IF;

                     --2012-09-13  DGiralL - Hallar solicitudes de equipos y terminar si estos son de Alta definicion
                     FOR c_equipos_hd IN c_equipos_solicitud(v_solic_cmp.subpedido_id) LOOP --2013-05-31   eaguirrg
                        IF NVL(c_equipos_hd.tipo_equipo,'x') IN ('STBAS','STBAD') THEN
                            v_equipos_hdd := TRUE;
                        END IF;
                     END LOOP;
                    -- 2013-05-08 - Inicio Cambios
                      --
                      b_es_nuevo_trabajo:=FALSE;
                      v_tiene_solici_demo:=0;
                      --
                      OPEN  c_tipo_trabajo_demo(v_solic_cmp.pedido_id,v_solic_cmp.subpedido_id);
                      FETCH c_tipo_trabajo_demo INTO v_tipo_trabajo_demo;
                      CLOSE c_tipo_trabajo_demo;                      
                      --
                      --Evaluamos si se trata de un tipo de trabajo NUEVO   Y  si la evaluación del concepto hasta este momento da PORDE
                      --Si se tiene un estado diferento como POPTO asi quedara la solicitud esto para no afectar la logica actual.
                      IF(v_tipo_trabajo_demo.tipo_trabajo='NUEVO') THEN
                       --
                       b_es_nuevo_trabajo:=TRUE;
                       --
                       v_tiene_solici_demo:=0;                                                                                           
                       --
                       OPEN  c_tiene_solici_demo(v_solic_cmp.pedido_id,v_solic_cmp.subpedido_id,v_solic_cmp.identificador_id);
                       FETCH c_tiene_solici_demo INTO v_tiene_solici_demo;
                       CLOSE c_tiene_solici_demo;                                                                     
                       --                       
                      END IF;
                     -- 2013-05-08 - Fin Cambios
                     
                     IF NOT b_pedido_tvdigital THEN
                             --2012-09-13  DGiralL - Solo realizar esta actualizacion si la tecnologia es HFC, o si
                             -- siendo la tecnologia REDCO los equipos no son de alta definicion
                             
                             -- 2014-04-11. REQ 36900 GPON Hogares
                             -- Se agrega la tecnología GPON para considerar el enrutamiento de Equipos.
                             IF v_tecnologia IN ('HFC', 'GPON') OR
                               (v_tecnologia = 'REDCO' AND NOT v_equipos_hdd ) THEN
                               
                                    -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                   bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_subped.pedido_id);
                            
                                   IF bEsOfertaEspecifica THEN     
                                       -- Si es superplay debe verificar si tiene subpedidos pendientes                          
                                       p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_subped.pedido_id, v_subped.subpedido_id);
                                       IF p_SubPendi AND v_concepto_ordenes NOT IN ('POPTO','PINSC','PECOM','PUMED') THEN
                                            v_concepto_ordenes := 'PEXPQ';
                                       ELSE
                                            IF v_concepto_ordenes NOT IN ('POPTO','PINSC','PECOM','PUMED') THEN
                                                v_concepto_ordenes := 'PORDE';    
                                            END IF;
                                       END IF;
                                   ELSE
                                       p_SubPendi := FALSE; -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
                                   END IF;   
                                    -- 2013-05-08 - Inicio Cambios                                 
                                    --Si tiene una solicitud anterior de demostracion se manejara el estado PINSC en vez de PORDE para la solicitud
                                    IF(v_tiene_solici_demo>0 AND b_es_nuevo_trabajo AND v_concepto_ordenes='PORDE')THEN 
                                     --
                                     v_concepto_ordenes:='PINSC';
                                     --                                    
                                    END IF;
                                    -- 2013-05-08 - Fin Cambios
                                    -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico


                                    BEGIN
                                        UPDATE FNX_SOLICITUDES
                                        SET    estado_id              = 'ORDEN',
                                               estado_soli            = 'DEFIN',
                                               concepto_id            = v_concepto_ordenes,
                                               tecnologia_id          = v_tecnologia,
                                               identificador_id_nuevo = v_identificador_cmp,
                                               observacion            = ' <*TV2 ['||v_concepto_ordenes||'] >'||observacion
                                        WHERE pedido_id              = v_solic_cmp.pedido_id
                                        AND   subpedido_id           = v_solic_cmp.subpedido_id
                                        AND   solicitud_id           = v_solic_cmp.solicitud_id;

                                    EXCEPTION
                                        WHEN OTHERS THEN
                                            w_mensaje_error := '<ENRUTAR_TELEV) Error actualizando solicitud. '||SUBSTR(SQLERRM, 1, 50);
                                            w_incons := TRUE;
                                    END;

                                   --2013-01-24: Req20216: Mmedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                                   IF bEsOfertaEspecifica AND NOT p_SubPendi THEN                                    
                                      pkg_provision_paquete.pr_genorden_pexpq (v_subped.pedido_id, v_solic_cmp.tipo_elemento_id, p_mensaje);
                                   END IF;
                                   --2013-01-24: Req20216: Mmedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                               -- 2013-03-19 Si es una adición del ELSIS para TELEV entonces, la solicitud debe quedar PORDE
                             ELSIF NVL(v_tecnologia,'S')='LOGIC' AND v_solic_cmp.tipo_ELEMENTO_ID='ELSIS' THEN
                                 IF nvl(v_concepto_ordenes,'S')<>'S'
                                AND nvl(v_tecnologia,'S')<>'S'
                                AND nvl(v_identificador_cmp,'S')<>'S' THEN
                                    BEGIN
                                        UPDATE FNX_SOLICITUDES
                                        SET    estado_id              = 'ORDEN',
                                               estado_soli            = 'DEFIN',
                                               concepto_id            = v_concepto_ordenes,
                                               tecnologia_id          = v_tecnologia,
                                               identificador_id_nuevo = v_identificador_cmp,
                                               observacion            = ' <*TV2EL ['||v_concepto_ordenes||'] >'||observacion
                                        WHERE pedido_id              = v_solic_cmp.pedido_id
                                        AND   subpedido_id           = v_solic_cmp.subpedido_id
                                        AND   solicitud_id           = v_solic_cmp.solicitud_id;

                                    EXCEPTION
                                        WHEN OTHERS THEN
                                            w_mensaje_error := '<ENRUTAR_TELEV) Error actualizando solicitud. '||SUBSTR(SQLERRM, 1, 50);
                                            w_incons := TRUE;
                                    END;
                                   END IF;
                             END IF;

                     ELSIF b_pedido_tvdigital THEN

                            IF b_tiene_decos_hfc THEN
                                -- 2011-10-11 Si además se está retirando un equipo
                                -- se debe colocar en POPTO.
                                OPEN C_TIPO_TRABAJO (v_solic_cmp.pedido_id,v_solic_cmp.subpedido_id);
                                FETCH C_TIPO_TRABAJO INTO V_TIPO_TRABAJO_DG;
                                IF C_TIPO_TRABAJO%FOUND THEN
                                    -- 2012-06-29  Aacosta  Se incluye el tipo de trabajo NUEVO para peticion de cambio o adicion de canales HD.
                                    IF V_TIPO_TRABAJO_DG.tipo_trabajo IN ('RETIR','NUEVO') THEN
                                        v_concepto_tv_digital:='POPTO';
                                                    dbms_output.put_line('POPTO 7 ');
                                    ELSE
                                         v_concepto_tv_digital:='PORDE';
                                    END IF;
                                ELSE

                                   b_existen_equipos:=FALSE;
                                   OPEN c_equipos;
                                   FETCH c_equipos INTO v_equipos;
                                   CLOSE c_equipos;

                                   IF v_equipos > 0 THEN
                                        b_existen_equipos:=TRUE;
                                   END IF;

                                   -- 2012-10-16 Ajuste para el tema de siembra digital.
                                   OPEN C_SIEMDIG;
                                   FETCH C_SIEMDIG INTO V_SIEMDIG;
                                   CLOSE C_SIEMDIG;

                                   OPEN C_EQUIPOSC_SIEM;
                                   FETCH C_EQUIPOSC_SIEM INTO V_EQUIPOS_SIEM;
                                   CLOSE C_EQUIPOSC_SIEM;

                                   -- 2011-12-22  Se valida que si no viene equipo y viene solo el componente SERVIP o SERVHFC
                                   --             la solicitud debe quedar PORDE.
                                   -- 2012-01-16 Solo pasa a PORDE, si no vienen equipos
                                   IF v_solic_cmp.tipo_elemento_id IN ('SERVIP','SERHFC') AND v_solic_cmp.tipo_solicitud = 'CAMBI' AND NOT b_existen_equipos THEN
                                      v_concepto_tv_digital:='PORDE';
                                   ELSE
                                       -- 2012-10-16 En caso de tratarse un pedido de siembra digital, sin equipos
                                       -- el SERHFC o el SERVIP pasan a PORDE.
                                       IF v_solic_cmp.tipo_elemento_id IN ('SERVIP','SERHFC')AND NVL(V_SIEMDIG,0) > 0 AND NVL(V_EQUIPOS,0) = 0 THEN
                                            v_concepto_tv_digital := 'PORDE';
                                       ELSE
                                            v_concepto_tv_digital := 'POPTO';
                                            dbms_output.put_line('POPTO 8 ');
                                       END IF;
                                   END IF;
                                END IF;
                                CLOSE C_TIPO_TRABAJO;
                            ELSE
                            --2013-09-20    aarbob REQ28278_Tangibilizacion
                            --Si el equipo es comprado se debe dejar la solictud del equipo en CUMPL - CUMPL
                            --y el estado de los componentes en ORDEN - PORDE
                               IF v_propiedadEqu = 'CM' THEN
                               
                                   v_concepto_tv_digital:='PORDE';
                               ELSE       
                               
                                   v_concepto_tv_digital:='POPTO';
                                                                   dbms_output.put_line('POPTO 9 ');
                             
                               END IF;
                            END IF;

                           -- 2014-04-11. REQ 36900 GPON Hogares
                           -- [COMMENT ] Revisar enrutamiento de SERVIP para GPON

                            -- 2011-11-24 Se crea condición para el tipo de elemento que venga en
                            -- la solicitud.
                            IF v_solic_cmp.tipo_elemento_id = 'SERVIP' THEN
                                v_tecnologia:='REDCO';
                            ELSIF v_solic_cmp.tipo_elemento_id = 'SERHFC' THEN
                                v_tecnologia:='HFC';
                            END IF;
                            
                            
                            -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                           bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_subped.pedido_id);
                            
                           IF bEsOfertaEspecifica THEN     
                               -- Si es superplay debe verificar si tiene subpedidos pendientes                          
                               p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_subped.pedido_id, v_subped.subpedido_id);
                               IF p_SubPendi AND v_concepto_tv_digital NOT IN ('POPTO','PINSC','PECOM','PUMED') THEN
                                v_concepto_tv_digital := 'PEXPQ';
                               ELSE
                                    IF v_concepto_tv_digital NOT IN ('POPTO','PINSC','PECOM','PUMED') THEN
                                        v_concepto_tv_digital := 'PORDE';  
                                    END IF;
                               END IF;
                           ELSE
                               p_SubPendi := FALSE; -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
                           END IF;  
                           -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                           -- 2013-05-08 - Inicio Cambios                                 
                            --Si tiene una solicitud anterior de demostracion se manejara el estado PINSC en vez de PORDE para la solicitud
                            IF(v_tiene_solici_demo>0 AND b_es_nuevo_trabajo AND v_concepto_tv_digital='PORDE')THEN 
                             --
                             v_concepto_tv_digital:='PINSC';
                             --                                    
                            END IF;
                            -- 2013-05-08 - Fin Cambios                                                          
                                BEGIN
                                    UPDATE FNX_SOLICITUDES
                                    SET    estado_id              = 'ORDEN',
                                           estado_soli            = 'DEFIN',
                                           concepto_id            = v_concepto_tv_digital,
                                           tecnologia_id          = v_tecnologia,
                                           identificador_id_nuevo = v_identificador_cmp,
                                           observacion            = ' <*TVD2 ['||v_concepto_tv_digital||'] >'||observacion
                                     WHERE pedido_id              = v_solic_cmp.pedido_id
                                     AND   subpedido_id           = v_solic_cmp.subpedido_id
                                     AND   solicitud_id           = v_solic_cmp.solicitud_id;

                                 EXCEPTION
                                    WHEN OTHERS THEN
                                       w_mensaje_error := '<ENRUTAR_TELEV) Error actualizando solicitud. '||SUBSTR(SQLERRM, 1, 50);
                                       w_incons := TRUE;
                                 END;
                                 
                                --2013-01-24: Req20216: Mmedinac: INICIO MODIFICACION: Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico  
                                IF bEsOfertaEspecifica AND NOT p_SubPendi THEN   
                                    pkg_provision_paquete.pr_genorden_pexpq (v_subped.pedido_id, v_solic_cmp.tipo_elemento_id, p_mensaje);
                                END IF;
                                --2013-01-24: Req20216: Mmedinac: FIN MODIFICACION: Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico                                

                     END IF;
                     -- Manejo de errores en cambio de estado y concepto de solicitud
                     IF w_incons AND v_concepto_ordenes IN ('PORDE', 'PEXPQ')
                     THEN
                        BEGIN
                          UPDATE FNX_SOLICITUDES
                          SET    estado_id   = 'ORDEN',
                                 estado_soli = 'DEFIN',
                                 concepto_id = 'APROB',
                                 tecnologia_id  = v_tecnologia,
                                 identificador_id_nuevo = v_identificador_cmp,
                                 observacion   = '<* TV2 [APROB]>'||observacion
                          WHERE  pedido_id     = v_subped.pedido_id
                          AND    subpedido_id  = v_subped.subpedido_id
                          AND    servicio_id   = 'ENTRE'
                          AND    producto_id   = 'TELEV'
                          AND    tipo_elemento = 'EQU'
                          AND    tipo_elemento_id IN ('DECO', 'EQURED', 'STBOX')
                          AND    concepto_id   IN ('PETEC', 'PRACC')
                          AND    ROWNUM        = 1;

                          b_orden_generada    := TRUE;
                          -- La variable de control de error se vuelve FALSE, dado que la
                          -- solicitud se pudo poner en concepto APROB.
                          w_incons            := FALSE;
                        EXCEPTION
                           WHEN OTHERS THEN
                              w_incons := TRUE;
                              w_mensaje_error := '<ENRUTAR_TELEV) Error actualizando solicitud APROB '||SUBSTR(SQLERRM, 1, 15);
                        END;
                     ELSIF w_incons THEN
                         w_mensaje_error := '<ENRUTAR_TELEV) Actualizando solicitud <'||v_concepto_ordenes||'>'
                                             || v_identificador_cmp;
                     END IF;

                  END IF;

                  FETCH c_solic_cmp INTO v_solic_cmp;
               END LOOP;
               CLOSE c_solic_cmp;

            END IF;
            -- FIN SECCION.  Actualizacion de estado concepto solicitudes CANALES

         END IF; -- FIN IF v_cuenta_cmp_equ > 0

      END IF;

      -- 2007-05-31
      -- Verificar sí en el subpedido viene una solicitud de Televisión donde se solicita
      -- un cambio de extensiones.
      IF NOT w_incons THEN

         OPEN  c_solic_cambi_srv (v_subped.pedido_id, v_subped.subpedido_id );
         FETCH c_solic_cambi_srv INTO v_solicitud_cambio_srv;
         CLOSE c_solic_cambi_srv;

         IF v_solicitud_cambio_srv > 0 THEN
            -- Verificar sí existe un trabajo de cambio de extensiones y no existe un
            -- trabajo de traslado.

            v_cambio_extensiones :=  PKG_SOLICITUDES.fn_tipo_trabajo
                   ( v_subped.pedido_id, v_subped.subpedido_id, v_solicitud_cambio_srv, 2909 );

            IF v_cambio_extensiones = 'CAMBI' THEN
               -- Verificar sí hay un trabajo para el cambio de página ( traslado )
               v_traslado        := PKG_SOLICITUDES.fn_tipo_trabajo
                   ( v_subped.pedido_id, v_subped.subpedido_id, v_solicitud_cambio_srv, 38 );

               IF NVL(v_traslado, 'SOLO_EXT') = 'SOLO_EXT' THEN

                  -- Establecer como debe quedar la solicitud de servicio.  Sí trae
                  -- equipos, debe

                  v_tecnologia := PKG_IDENTIFICADORES.fn_tecnologia ( v_subped.identificador_id );

                  IF w_cuenta_equ > 0 THEN
                     v_concepto_ordenes := 'POPTO';
                                                     dbms_output.put_line('POPTO 10 ');
                  ELSE
                     v_concepto_ordenes := 'PORDE';
                      dbms_output.put_line('VAL 6 ');
                  END IF;
                  
                    -- 2013-01-24: Req20216: MMedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico
                   bEsOfertaEspecifica := PKG_PROVISION_PAQUETE.fn_EsOfertaEspecifica(v_subped.pedido_id);
                            
                   IF bEsOfertaEspecifica THEN     
                       -- Si es superplay debe verificar si tiene subpedidos pendientes                          
                       p_SubPendi := PKG_PROVISION_PAQUETE.fn_Verificar_PendET(v_subped.pedido_id, v_subped.subpedido_id);
                       IF p_SubPendi AND v_concepto_ordenes NOT IN ('POPTO','PINSC','PECOM','PUMED') THEN
                            v_concepto_ordenes := 'PEXPQ';
                       ELSE
                            IF v_concepto_ordenes NOT IN ('POPTO','PINSC','PECOM','PUMED') THEN
                                v_concepto_ordenes := 'PORDE';
                            END IF;
                       END IF;
                   ELSE
                       p_SubPendi := FALSE; -- Si no es un paquete trio superplay debe seguir funcionando como lo hace actualmente
                   END IF;  
                    -- 2013-01-24: Req20216: MMedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico

                      UPDATE FNX_SOLICITUDES
                         SET estado_id     = 'ORDEN',
                             estado_soli   = 'DEFIN',
                             concepto_id   = v_concepto_ordenes,
                             tecnologia_id = NVL (v_tecnologia, 'HFC'),
                             identificador_id = v_subped.identificador_id,
                             observacion   = ' <*TV1 [4.'||v_concepto_ordenes||']> '||observacion
                       WHERE pedido_id     = v_subped.pedido_id
                       AND   subpedido_id  = v_subped.subpedido_id
                       AND   solicitud_id  = v_solicitud_cambio_srv;
                       
                    -- 2013-01-24: Req20216: Mmedinac: INICIO MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico 
                    IF bEsOfertaEspecifica AND NOT p_SubPendi THEN
                       SELECT tipo_elemento_id
                         INTO vTipoElementoID 
                         FROM fnx_solicitudes 
                        WHERE pedido_id = v_subped.pedido_id
                          AND subpedido_id = v_subped.subpedido_id
                          AND solicitud_id = v_solicitud_cambio_srv;  
                          
                        pkg_provision_paquete.pr_genorden_pexpq (v_subped.pedido_id, vTipoElementoID, p_mensaje);
                    END IF;                       
                    -- 2013-01-24: Req20216: Mmedinac: FIN MODIFICACION Se adiciona el llamado a la función para verificar si existen pedidos pendientes por Estudio Tecnico                       

               END IF; -- FIN NVL(v_traslado, 'SOLO_EXT')
            END IF; -- IF v_cambio_extensiones
         END IF; --- IF v_cuenta_srv_cambio

      END IF;
      -- FIN SECCION Verificación de solicitud de SRV en el subpedido;

      FETCH c_subped INTO v_subped;
   END LOOP;
   CLOSE c_subped;


   IF NOT w_incons THEN
      w_resp := 'S';
   ELSE
      w_resp := w_mensaje_error;
   END IF;
END PR_ENRUTAR_TELEV; 