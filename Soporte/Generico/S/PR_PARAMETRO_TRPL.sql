PROCEDURE       PR_PARAMETRO_TRPL (
  p_pedido          IN       fnx_ordenes_trabajos.pedido_id%TYPE
, p_subpedido       IN       fnx_ordenes_trabajos.subpedido_id%TYPE
, p_solicitud       IN       fnx_ordenes_trabajos.solicitud_id%TYPE
, p_requerimiento   IN       fnx_tareas_plataformas.requerimiento_id%TYPE
, p_producto        IN       fnx_ordenes_trabajos.pedido_id%TYPE
, p_identificador   IN       fnx_ordenes_trabajos.identificador_id%TYPE
, p_tecnologia      IN       fnx_ordenes_trabajos.tecnologia_id%TYPE
, p_cola            IN       fnx_ordenes_trabajos.cola_id%TYPE
, p_tareaplataf     IN       fnx_ord_det_impresiones.tareaplataf%TYPE
, p_rpedido         IN       fnx_ord_det_impresiones.rpedido%TYPE
, p_actividad       IN       fnx_tareas_plataformas.actividad_id%TYPE Default NULL--02/12/2010 Ldonadop
, p_xml_fenix       OUT      VARCHAR2
)
IS

--MARISTIM mayo 15/2008 se verifico con PRD y Dllo

/*
Para tener en cuenta
-El bloque1 no se utiliza
-En los registrs tipo FUNCI, los campos a reemplazar deben se mayuscula fija ej: FN_VALOR_CARACT_SUBELEM(V_PEDISOLI,V_SUBPEDISOLI,'SRVWBH',925)
*/
/*
    2010/12/22 JGLENA Se agrega cláusula ORDER BY a cursor c_impbloque para garantizar el correcto ordenamiento de los
                      datos en todos los ambientes
*/


  --cgctrl2_V_PARAMETRO VARCHAR2(100);

  -----------------------------------------------------------
--Variables Cursor c_ImpColaOrd
  v_cara_detimpre       fnx_ord_det_impresiones.caracteristica_id%TYPE;
  v_titulo              fnx_ord_det_impresiones.titulo%TYPE;
  v_pos_pantalla        fnx_ord_det_impresiones.pos_pantalla%TYPE;
  v_pos_ortr_impr       fnx_ord_det_impresiones.pos_ortr_impr%TYPE;
  v_campo_bloque        fnx_ord_det_impresiones.campo_bloque%TYPE;
  v_rpedido_detimpre    fnx_ord_det_impresiones.rpedido%TYPE;
  v_imparchexcel        fnx_ord_det_impresiones.imprime%TYPE;
  v_tipo_campo          fnx_ord_det_impresiones.tipo_campo%TYPE;
  v_fvalor_actual       fnx_ord_det_impresiones.fvalor_actual%TYPE;
  v_fvalor_anterior     fnx_ord_det_impresiones.fvalor_anterior%TYPE;
  v_tecno_detimpre      fnx_ord_det_impresiones.tecnologia_id%TYPE;
-----------------------------------------------------------
--Variables cursor c_caso
  v_pedisoli            fnx_solicitudes.pedido_id%TYPE;
  v_subpedisoli         fnx_solicitudes.subpedido_id%TYPE;
  v_solisoli            fnx_solicitudes.solicitud_id%TYPE;
  v_servsoli            fnx_solicitudes.servicio_id%TYPE;
  v_prodsoli            fnx_solicitudes.producto_id%TYPE;
  v_compsoli            fnx_solicitudes.tipo_elemento_id%TYPE;
  v_identsoli           fnx_solicitudes.identificador_id%TYPE;
  v_identsoli_nuevo     fnx_solicitudes.identificador_id%TYPE;
  v_tecnosoli           fnx_solicitudes.tecnologia_id%TYPE;
  v_actisoli            fnx_actividades_ordenes.actividad_id%TYPE;--Ldonadop
-----------------------------------------------------------
-----------------------------------------------------------
--Variables cursor c_traso
  v_caratraso           fnx_trabajos_solicitudes.caracteristica_id%TYPE;
-----------------------------------------------------------
-----------------------------------------------------------
--Variables
  v_pos_impr_real       NUMBER (5, 0)                                    := 0;
  v_pos_impr_real_tr    NUMBER (5, 0)                                    := 0;
  v_new_pos_impr_real   NUMBER (5, 0)                                    := 0;
  v_dato                VARCHAR2 (32000);
  v_dato_ant            VARCHAR2 (32000);
  v_indicasoli          VARCHAR2 (50);
  v_tipo_subpedi        fnx_subpedidos.tipo_subpedido%TYPE;
  v_error               VARCHAR2 (2000);
  v_conttut             NUMBER (5, 0)                                    := 0;
  v_grupobloque         fnx_ord_det_impresiones.grupo%TYPE;
  v_bloquebloque        fnx_ord_det_impresiones.bloque%TYPE;
  v_rpedidobloque       fnx_ord_det_impresiones.rpedido%TYPE;
  v_indicador_pet       fnx_ord_det_impresiones.indicador_pet%TYPE;
  v_tipo_tag            fnx_ord_det_impresiones.tipo_tag%TYPE;
-----------------------------------------------------------
  v_funciony            VARCHAR2 (3500);
  v_funcionx            VARCHAR2 (3500);
  v_sql_ordenes         VARCHAR2 (3500);
  v_funcionpeti         VARCHAR2 (3500);
-----------------------------------------------------------
  bimprimeini           NUMBER (3, 0);
  bimprimefin           NUMBER (3, 0);
  v_tituloant_impre     VARCHAR2 (500);
  v_lineaxml            VARCHAR2 (10000);
  v_xml_fenix           VARCHAR2 (8000);
  v_etiqbl1             NUMBER (3, 0);
  v_etiqbl2             NUMBER (3, 0);
  v_etiqbl9             NUMBER (3, 0);
  v_par_impar           NUMBER (3, 0);
  v_titulobloque        VARCHAR2 (500);
  v_tipo_requerimiento  fnx_requerimientos_trabajos.tipo_requerimiento%TYPE;

-----------------------------------------------------------
    /*    2010/12/22 JGLENA Se agrega cláusula ORDER BY a cursor c_impbloque para garantizar el correcto
                            ordenamiento de los XML en todos los ambientes */
  --Cursor de Bloque que indica el orden de la impresion
  CURSOR c_impbloque
  IS
    SELECT   grupo, bloque, DECODE (rpedido, 'GRAL', p_producto, rpedido)
        FROM fnx_ord_det_impresiones
       WHERE cola_id = p_cola
         AND tareaplataf = p_tareaplataf
         AND estado = 'ACT'
    GROUP BY grupo, bloque, rpedido
    ORDER BY grupo, bloque, rpedido;

-----------------------------------------------------------

  --Cursor de solicitudes
--siempre consulto todas las solicitudes y hago un filtro despues para poder extraer los datos de imrpesion
  CURSOR c_caso
  IS
    SELECT   pedido_id, subpedido_id, solicitud_id, servicio_id, producto_id
           , tipo_elemento_id, p_identificador ident
           , fn_valor_caracteristica_trab (pedido_id
                                         , subpedido_id
                                         , solicitud_id
                                         , 1
                                          ) ident_nuevo
           , p_tecnologia
        FROM fnx_caracteristica_solicitudes
       WHERE pedido_id = p_pedido
         AND subpedido_id = p_subpedido
         AND solicitud_id = p_solicitud
         AND producto_id = p_producto
         AND ROWNUM = 1
    ORDER BY caracteristica_id
--AND tipo_elemento_id = v_rpedidobloque
  ;

  CURSOR c_rqtr
  IS
    SELECT pedido_id, NVL(subpedido_id,0), NVL(solicitud_id,0), servicio_id, producto_id
         , fn_identificador_tipoelem (identificador_id)
         , p_identificador ident
         , fn_valor_caracteristica_trab (pedido_id
                                       , NVL(subpedido_id,0)
                                       , NVL(solicitud_id,0)
                                       , 1
                                        ) ident_nuevo
         , p_tecnologia
      FROM fnx_requerimientos_trabajos
     WHERE requerimiento_id = p_requerimiento
       AND producto_id = p_producto
       AND ROWNUM = 1;

-----------------------------------------------------------
 --Cursor de Detalle de la impresion
  CURSOR c_impcolaord
  IS
    SELECT   caracteristica_id, titulo, pos_pantalla, pos_ortr_impr
           , campo_bloque, tipo_campo
           , DECODE (rpedido, 'GRAL', p_producto, rpedido), imprime
           , indicador_pet, fvalor_actual, fvalor_anterior
           , DECODE (tecnologia_id, 'GRAL', p_tecnologia, tecnologia_id)
           , tipo_tag
        FROM fnx_ord_det_impresiones
       WHERE cola_id = p_cola
         AND estado = 'ACT'
         AND grupo = v_grupobloque
         AND bloque = v_bloquebloque
         AND tareaplataf = p_tareaplataf
    ORDER BY grupo, bloque, cola_id, rpedido, tecnologia_id, pos_ortr_impr;

-----------------------------------------------------------
--Cursor de trabajos
  CURSOR c_traso
  IS
    SELECT caracteristica_id
      FROM fnx_trabajos_solicitudes
     WHERE pedido_id = v_pedisoli
       AND subpedido_id = v_subpedisoli
       AND solicitud_id = v_solisoli
       AND caracteristica_id = v_cara_detimpre
       AND ROWNUM = 1;
BEGIN
  /* DBMS_OUTPUT.put_line (' BEGIN PR_PARAMETRO_TRPL');
   DBMS_OUTPUT.put_line ('cola_id ' || p_cola || ' tareaplataf='|| p_tareaplataf);
   DBMS_OUTPUT.put_line (   p_pedido|| ','|| p_subpedido|| ','|| p_solicitud|| ','|| p_producto|| ','|| p_identificador|| ','|| p_tecnologia|| ','|| p_cola|| ','|| p_tareaplataf|| ','|| p_rpedido );

 */
-----------------------------------------------------------
--Aumento el contador para llevar los datos al bloque
--en el orden que se va a imprimir
  v_pos_impr_real := 0;
  v_etiqbl1 := 0;
  v_etiqbl2 := 0;
  v_etiqbl9 := 0;
  v_par_impar := 1;                                           --Par =2 impar2
  v_xml_fenix := '<?xml version="1.0" encoding="ISO-8859-1"?>' || '<msg>';

-----------------------------------------------------------
--Abrir cursor Bloque
  OPEN c_impbloque;

  LOOP
    FETCH c_impbloque
     INTO v_grupobloque, v_bloquebloque, v_rpedidobloque;

    EXIT WHEN c_impbloque%NOTFOUND;
    v_conttut := 0;

    ---1. Con el Cursor de impresion X bloque, por cada producto configurado recorro las solicitudes para extraer los datos
    BEGIN
      SELECT tipo_requerimiento
        INTO v_tipo_requerimiento
        FROM fnx_requerimientos_trabajos
       WHERE requerimiento_id = p_requerimiento
         AND ROWNUM = 1;
    EXCEPTION
      WHEN OTHERS
      THEN
        v_tipo_requerimiento := NULL;
    END;

    IF v_tipo_requerimiento = 'INSTA'
    THEN
      OPEN c_caso;

      FETCH c_caso
       INTO v_pedisoli, v_subpedisoli, v_solisoli, v_servsoli, v_prodsoli
          , v_compsoli, v_identsoli, v_identsoli_nuevo, v_tecnosoli;

      CLOSE c_caso;
    ELSE
      OPEN c_rqtr;

      FETCH c_rqtr
       INTO v_pedisoli, v_subpedisoli, v_solisoli, v_servsoli, v_prodsoli
          , v_compsoli, v_identsoli, v_identsoli_nuevo, v_tecnosoli;

      --EXIT WHEN c_rqtr%NOTFOUND;
      CLOSE c_rqtr;
    END IF;

    IF p_rpedido = 'ELEM'
    THEN
      v_indicasoli := v_compsoli;
    ELSIF p_rpedido = 'PROD'
    THEN
      v_indicasoli := v_prodsoli;
    END IF;

    v_actisoli := p_actividad; --02/12/2010 Ldonadop

    OPEN c_impcolaord;

    LOOP
      FETCH c_impcolaord
       INTO v_cara_detimpre, v_titulo, v_pos_pantalla, v_pos_ortr_impr
          , v_campo_bloque, v_tipo_campo, v_rpedido_detimpre, v_imparchexcel
          , v_indicador_pet, v_fvalor_actual, v_fvalor_anterior
          , v_tecno_detimpre, v_tipo_tag;

      EXIT WHEN c_impcolaord%NOTFOUND;

     --YSOLARTE 2011/07/12 Se adiciona esta condición para cambiar el tag cuando sea un producto de TV empresarial
     --Por solicitd de UNE con el fin de no crear otro intercambio para solamente cambiar el titulo del tag
     IF p_producto = 'TV' AND v_titulo = 'numeroSERVIP' THEN
        v_titulo := 'numeroPUNTIP';
      END IF;

--1
--        DBMS_OUTPUT.put_line (   v_rpedido_detimpre||'='||v_rpedidobloque||' and '||v_rpedido_detimpre||'='||v_indicasoli||' and '||v_tecno_detimpre||'='||v_tecnosoli );
      IF v_tipo_requerimiento = 'INSTA'
      THEN
        v_tipo_subpedi :=
                    fn_tipo_solicitud (v_pedisoli, v_subpedisoli, v_solisoli);
      ELSE
        v_tipo_subpedi := 'CAMBI';
      -- Si es un pedido de Mantenimiento, puede irse por NUEVO o por CAMBI
      END IF;

      --dbms_output.put_line('TIPO_REQUERIMIENTO ' || v_tipo_requerimiento);
      --dbms_output.put_line('v_tipo_subpedi ' || v_tipo_subpedi);
      --dbms_output.put_line('TIPO_SUBP ->'||V_TIPO_SUBPEDI);

-------------------------------------------------------------
--NUEVO
-------------------------------------------------------------
      IF v_tipo_subpedi = 'NUEVO' AND v_pos_impr_real < 100
      THEN

        v_dato := '';

---------------------------------
--v_tipo_campo = 'CARA'
        IF v_tipo_campo = 'CARA'
        THEN
          v_dato :=
            NVL (fn_valor_caracteristica_sol (v_pedisoli
                                            , v_subpedisoli
                                            , v_solisoli
                                            , v_cara_detimpre
                                             )
               , fn_valor_caract_identif (p_identificador, v_cara_detimpre)
                );

---------------------------------
--v_tipo_campo   ='RQTR'
        ELSIF v_tipo_campo = 'RQTR'
        THEN
          v_funcionx := v_campo_bloque;
          IF v_funcionx IS NOT NULL
          THEN
            pr_reempl_campos_rqtr (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , p_requerimiento
                                 , p_cola
                                 , v_funcionx
                                 , v_funciony
                                  );
            v_funciony := REPLACE (v_funciony, CHR (39), '');--Los parametros le asigna los valores actuales
            v_dato := v_funciony;
          ELSE
            v_dato := NULL;
          END IF;

---------------------------------
--v_tipo_campo   ='FUNCI'
        ELSIF v_tipo_campo = 'FUNCI'
        THEN

          v_funcionx := v_fvalor_actual;

          IF v_funcionx IS NOT NULL
          THEN
            pr_reempl_campos_rqtr (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , p_requerimiento
                                 , p_cola
                                 , v_funcionx
                                 , v_funciony
                                  );
            --Los parametros le asigna los valores actuales
            v_funcionx := v_funciony;
            --No se puede utilizar funcion esta PR_REEMPL_CAMPOS_CARASOLI(v_funcionX, v_funcionY);  --Los parametros le asigna los valores actuales
            pr_reempl_campos_soli (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , v_servsoli
                                 , v_prodsoli
                                 , v_compsoli
                                 , v_identsoli
                                 , v_identsoli_nuevo
                                 , v_tecnosoli
                                 , v_tipo_subpedi
                                 , v_actisoli --02/12/2010 Ldonadop
                                 , v_funcionx
                                 , v_funciony
                                  );
            v_sql_ordenes :=
                 ' SELECT '
              || NVL (v_funciony, ' ')
              || ' FROM DUAL WHERE ROWNUM=1 ';

            /*  DBMS_OUTPUT.put_line (   ' FUNCI -v_sql_ordenes '
                                    || SUBSTR (v_sql_ordenes, 1, 200)
                                   );
              DBMS_OUTPUT.put_line (   ' FUNCI -v_sql_ordenes '
                                    || SUBSTR (v_sql_ordenes, 200, 200)
                                   );
                                   */
            v_dato := fn_valor_cursor (v_sql_ordenes, 1);
/*            DBMS_OUTPUT.put_line (   'NUEVO  FUNCION '
                                  || SUBSTR (v_sql_ordenes, 1, 200)
                                 );*/
          ELSE
            v_dato := NULL;
          END IF;
          v_sql_ordenes := NULL;

---------------------------------
--v_tipo_campo   ='CONS1'
        ELSIF v_tipo_campo = 'CONS1'
        THEN
          v_funcionx := v_fvalor_actual;

          IF v_funcionx IS NOT NULL
          THEN
            pr_reempl_campos_soli (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , v_servsoli
                                 , v_prodsoli
                                 , v_compsoli
                                 , v_identsoli
                                 , v_identsoli_nuevo
                                 , v_tecnosoli
                                 , v_tipo_subpedi
                                 , v_actisoli --02/12/2010 Ldonadop
                                 , v_funcionx
                                 , v_funciony
                                  );
            v_sql_ordenes := v_funciony;

            /* DBMS_OUTPUT.put_line (   '***CONS1 **v_sql_ ordenes '
                                    || SUBSTR ('CONS1 ' || v_sql_ordenes,
                                               0,
                                               200
                                              )
                                   );*/
            v_dato := fn_valor_cursor (v_sql_ordenes, 1);
          --  || ' FROM DUAL WHERE ROWNUM=1 ';
            v_sql_ordenes := NULL;
          ELSE
            v_dato := NULL;
          END IF;

---------------------------------
--v_tipo_campo   ='CONS2'
        ELSIF v_tipo_campo = 'CONS2'
        THEN
          v_funcionx := v_fvalor_actual;

          IF v_funcionx IS NOT NULL
          THEN
            pr_reempl_campos_ortr (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , p_cola
                                 , v_funcionx
                                 , v_funciony
                                  );
            --Los parametros le asigna los valores actuales
            v_funcionx := v_funciony;
            --PR_REEMPL_CAMPOS_CARASOLI(v_funcionX, v_funcionY);  --Los parametros le asigna los valores actuales
            pr_reempl_campos_soli (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , v_servsoli
                                 , v_prodsoli
                                 , v_compsoli
                                 , v_identsoli
                                 , v_identsoli_nuevo
                                 , v_tecnosoli
                                 , v_tipo_subpedi
                                 , v_actisoli --02/12/2010 Ldonadop
                                 , v_funcionx
                                 , v_funciony
                                  );
            v_sql_ordenes := v_funciony;
            /* DBMS_OUTPUT.put_line (   'v_sql_ ordenes '
                                   || SUBSTR ('CONS2 ' || v_sql_ordenes,
                                              0,
                                              200
                                             )
                                  );*/
--            v_dato := fn_valores_cursor (v_sql_ordenes);
            v_sql_ordenes := NULL;
          ELSE
            v_dato := NULL;
          END IF;
---------------------------------
-- v_tipo_campo = 'VALOR'
        ELSIF v_tipo_campo = 'VALOR'
        THEN
          v_dato := v_fvalor_actual;
        END IF;                                        --ELSIF    v_tipo_campo
---------------------------------
--FIN   v_tipo_campo

        ------------------------------------------------------------------------------------
        --Copiar dato en cadena
        ------------------------------------------------------------------------------------
        IF v_grupobloque IN (2, 4, 6, 8, 10, 12, 14, 16, 18, 20)
        THEN
          IF v_tipo_tag = 'E'
          THEN
            v_lineaxml := v_titulo;
          --   dbms_output.put_line( v_lineaXML);
          ELSIF v_tipo_tag = 'A'
          THEN
            ---dbms_output.put_line('<' || v_titulo ||'>' || v_dato || '</' ||  v_titulo ||'>');
            v_lineaxml :=
                  '<' || v_titulo || '>' || v_dato || '</' || v_titulo || '>';
--            dbms_output.put_line( v_lineaXML);
          END IF;
        END IF;

        v_xml_fenix := v_xml_fenix || v_lineaxml;


-------------------------------------------------------------
--PETICIONES
-------------------------------------------------------------
      ELSE


        v_dato := '';
---------------------------------
-- v_tipo_campo = 'CARA'
        --dbms_output.put_line('v_titulo ' || v_titulo || '-'|| v_tipo_campo  || '-'||  v_indicador_pet );
        IF v_tipo_campo = 'CARA'
        THEN
          IF v_indicador_pet = 'TRSO'
          THEN
            v_dato :=
              fn_valor_caracteristica_trab (v_pedisoli
                                          , v_subpedisoli
                                          , v_solisoli
                                          , v_cara_detimpre
                                           );
          ELSIF v_indicador_pet = 'CONF'
          THEN
            -- DBMS_OUTPUT.put_line ('v_identsoli ' || v_identsoli);
            v_dato := fn_valor_caract_identif (v_identsoli, v_cara_detimpre);
          ELSIF v_indicador_pet = 'CASO'
          THEN
            v_dato :=
              fn_valor_caracteristica_sol (v_pedisoli
                                         , v_subpedisoli
                                         , v_solisoli
                                         , v_cara_detimpre
                                          );
          END IF;

---------------------------------
--v_tipo_campo   ='RQTR'
        ELSIF v_tipo_campo = 'RQTR'
        THEN

          v_funcionx := v_campo_bloque;

          IF v_funcionx IS NOT NULL
          THEN
            pr_reempl_campos_rqtr (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , p_requerimiento
                                 , p_cola
                                 , v_funcionx
                                 , v_funciony
                                  );
            --Los parametros le asigna los valores actuales
            v_funciony := REPLACE (v_funciony, CHR (39), '');
            v_dato := v_funciony;
          -- dbms_output.put_line('RQTR -> ' || v_funcionx || ' dato -> ' || v_dato );
          ELSE
            v_dato := NULL;
          END IF;

---------------------------------
--v_tipo_campo   ='FUNCI'
        ELSIF v_tipo_campo = 'FUNCI'
        THEN

          v_funcionx := v_fvalor_anterior;

--           dbms_output.put_line('CAMBI v_tipo_campo = FUNCI v_funcionX  ' || v_funcionX);
          IF v_funcionx IS NOT NULL
          THEN
            pr_reempl_campos_rqtr (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , p_requerimiento
                                 , p_cola
                                 , v_funcionx
                                 , v_funciony
                                  );
            --Los parametros le asigna los valores actuales
            v_funcionx := v_funciony;
            --No se puede utilizar funcion esta PR_REEMPL_CAMPOS_CARASOLI(v_funcionX, v_funcionY);  --Los parametros le asigna los valores actuales
            pr_reempl_campos_soli (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , v_servsoli
                                 , v_prodsoli
                                 , v_compsoli
                                 , v_identsoli
                                 , v_identsoli_nuevo
                                 , v_tecnosoli
                                 , v_tipo_subpedi
                                 , v_actisoli --02/12/2010 Ldonadop
                                 , v_funcionx
                                 , v_funciony
                                  );
            v_sql_ordenes :=
                 ' SELECT '
              || NVL (v_funciony, ' ')
              || ' FROM DUAL WHERE ROWNUM=1 ';
            DBMS_OUTPUT.put_line
              (   ' XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX FUNCI v_sql_ordenes '
               || v_sql_ordenes
              );
            v_dato := fn_valor_cursor (v_sql_ordenes, 1);
          ELSE
            v_dato := NULL;
          END IF;

          v_sql_ordenes := NULL;

---------------------------------
--v_tipo_campo   ='CONS1'
        ELSIF v_tipo_campo = 'CONS1'
        THEN
          v_funcionx := v_fvalor_anterior;

          IF v_funcionx IS NOT NULL
          THEN
            pr_reempl_campos_rqtr (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , p_requerimiento
                                 , p_cola
                                 , v_funcionx
                                 , v_funciony
                                  );
            --Los parametros le asigna los valores actuales
            v_funcionx := v_funciony;
            pr_reempl_campos_soli (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , v_servsoli
                                 , v_prodsoli
                                 , v_compsoli
                                 , v_identsoli
                                 , v_identsoli_nuevo
                                 , v_tecnosoli
                                 , v_tipo_subpedi
                                 , v_actisoli --02/12/2010 Ldonadop
                                 , v_funcionx
                                 , v_funciony
                                  );
            v_sql_ordenes := v_funciony;


            v_dato := fn_valor_cursor (v_sql_ordenes, 1);
            /*   DBMS_OUTPUT.put_line (   'v_sql_ ordenes '
                                      || SUBSTR ('CONS1 ' || v_sql_ordenes,
                                                 0,
                                                 200
                                                )
                                     );*/
            v_sql_ordenes := NULL;
          ELSE
            v_dato := NULL;
          END IF;
        --v_tipo_campo   ='CONS2'
        ELSIF v_tipo_campo = 'CONS2'
        THEN
          v_funcionx := v_fvalor_anterior;

          IF v_funcionx IS NOT NULL
          THEN
            pr_reempl_campos_rqtr (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , p_requerimiento
                                 , p_cola
                                 , v_funcionx
                                 , v_funciony
                                  );
            --Los parametros le asigna los valores actuales
            v_funcionx := v_funciony;
            pr_reempl_campos_soli (v_pedisoli
                                 , v_subpedisoli
                                 , v_solisoli
                                 , v_servsoli
                                 , v_prodsoli
                                 , v_compsoli
                                 , v_identsoli
                                 , v_identsoli_nuevo
                                 , v_tecnosoli
                                 , v_tipo_subpedi
                                 , v_actisoli --02/12/2010 Ldonadop
                                 , v_funcionx
                                 , v_funciony
                                  );
            v_sql_ordenes := v_funciony;
            DBMS_OUTPUT.put_line (   'v_sql_ ordenes '
                                  || SUBSTR ('CONS2 ' || v_sql_ordenes, 0
                                           , 200)
                                 );
--            v_dato := fn_valores_cursor (v_sql_ordenes);
            v_sql_ordenes := NULL;
          ELSE
            v_dato := NULL;
          END IF;

---------------------------------
--v_tipo_campo = 'VALOR'
        ELSIF v_tipo_campo = 'VALOR'
        THEN
          v_dato := v_fvalor_actual;
        END IF;                                        --ELSIF    v_tipo_campo
---------------------------------
--FIN v_tipo_campo
------------------------------------------------------------------------------------
--Copiar dato en cadena
------------------------------------------------------------------------------------
        IF v_grupobloque = 2
        THEN
          IF v_tipo_tag = 'E'
          THEN
            v_lineaxml := v_titulo;
          ELSIF v_tipo_tag = 'A'
          THEN
            v_lineaxml :=
                  '<' || v_titulo || '>' || v_dato || '</' || v_titulo || '>';
            DBMS_OUTPUT.put_line (v_lineaxml);
          END IF;
        END IF;

        v_xml_fenix := v_xml_fenix || v_lineaxml;
      END IF;               --ELSE --PETICIONES (CAMBIO, RETIR, TRASL, SUSPE),
    --dbms_output.put_line('v_lineaXML ' || v_lineaXML);

    --END IF;
    END LOOP;

    CLOSE c_impcolaord;
  END LOOP;

  CLOSE c_impbloque;

  v_xml_fenix := v_xml_fenix || '</msg>';
  p_xml_fenix := v_xml_fenix;
  DBMS_OUTPUT.put_line
                   ('******************************** SALI PR_PARAMETRO_TRPL ');

  IF p_xml_fenix IS NULL
  THEN
    DBMS_OUTPUT.put_line (' Es nulo ');
  END IF;
END;