import java.util.*;
public class Declaraciones extends Estructura{
	public String codigo;
	public String token;
	public ArrayList<Declaracion>listaDeclaraciones;
	public Declaraciones(){
		this.codigo="";
		listaDeclaraciones=new ArrayList<Declaracion>();
	}
	public static String comprobar(String token,Tokenizer fuente,Declaraciones declaraciones){
		if(token.toUpperCase().equals("IS")||token.toUpperCase().equals("DECLARE")){
			declaraciones.codigo=token;
			declaraciones.token=token.toUpperCase();
			while(fuente.hasNext()){
				token=fuente.next();
				if(token.toUpperCase().equals("BEGIN")){
					break;
				}
				Declaracion declaracion=new Declaracion(token,fuente);
				declaraciones.listaDeclaraciones.add(declaracion);
				declaraciones.codigo+="\n"+declaracion.codigo;
			}
		}
		return token;
	}
	public String getCodigo(int nivel){
		if(token==null){
			return "";
		}
		String resultado=Util.tab(nivel)+token+"\n";
		for(int i=0;i<listaDeclaraciones.size();i++){
			resultado+=listaDeclaraciones.get(i).getCodigo(nivel);
		}
		return resultado;
	}
	public String getTipo(){
		return "DECLARACIONES";
	}
	public String desc(int nivel){
		String resultado="";
		if(!this.getCodigo(nivel).equals("")){
			resultado=Util.tab(nivel)+this.getTipo()+"\n";
		}
		return resultado;
	}
	public String getHTML()throws Exception{
		String resultado="";
		String contenido=Util.leer("HTML//"+this.getClass().getName()+".html");
		for(int i=0;i<listaDeclaraciones.size();i++){
			resultado+=contenido.replace("Declaracion",listaDeclaraciones.get(i).getHTML());
		}
		return resultado;
	}
}