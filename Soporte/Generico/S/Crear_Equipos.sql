PROCEDURE Crear_Equipos IS
-- 03-Julio-2003
-- Procedimiento para cargar equipos
-- Nestor Javier Palacio Roldan - MVM Ingenieria de software
-- HISTORIA DE MODIFICACIONES
-- Fecha			Autor				Observacion
-- ---------	--------		----------------------------------------------
-- 2008-01-15	Jrincong	 / Alopezr
-- 2008-01-15	Jrincong		Cursor c_cargar_equipos.  Adicion de campo agrupador_id para ser 
--												considerado como MAC del Equipo
-- 2008-01-15	Jrincong		Determinar el valor que se guarda en el campo EQUIPO_ID para poder
-- 												almacenar correctamente el serial o la MAC				    
-- 2008-01-15	Jrincong		Inserción de característica MAC Equipo ( 3765 )
-- 2008-01-31	Jrincong		Inserción de característica Serial Real Equipo ( 3766 )
-- 2008-01-31	Jrincong		Uso de funcion UPPER en campo equipo_id
-- 2008-02-12 Aacosta     Modificación del cursor c_cargar_equipos con el fin de darle un alias a los 
--				     				    campo marca_id_n y referencia_id_n como PIN y PUK respectivamente. esto es para 
--												las cargas de equipos de movilidad (SIMCARD'S).
-- 2008-02-12 Aacosta     Validar si se está cargando equipos de Movilidad, se seleccione la oficina a la cual
--										    se cargarán los equipos.
-- 2008-03-18	Jrincong		Seleccion de campos MUNICIPIO y EMPRESA para inserción en tabla
--												FNX_EQUIPOS
-- 2008-03-25	Jrincong		Uso de campos :BLQ_DATOS.municipio_carga y :BLQ_DATOS.empresa_carga
--												en caso de nulidad de campos en tabla FNX_CAMBIOS_EQUIPOS
-- 2008-03-25	Jrincong		Cursor c_existe_equipo.  Se quitan condiciones de marca, referencia
--												tipo_equipo para garantizar que no se carguen seriales repetidos.
-- 2008-07-29 Aacosta     Adición del campo Identificador_id_n en el cursor c_cargar_equipos, con el fin
--										    de incluir la nueva característica 2131 ( esta no es la definitiva) para los Decos del
--									      proyecto de Cableras. 
-- 2009-08-31 Aacosta     Inserción de la característica 4362 (Teléfono móvil) en la configuracion
--	                      de las simcard's para el producto Internet Móvil.
-- 2009-08-31 Aacosta     Validación asociada a la característica 4362 (Teléfono móvil), para que sea
--					              cargada en el portafolio del equipo.
-- 2009-08-31 Aacosta     Creación de la numeración asociada al equipo SIMCARD, según la característica 4362.
-- 2009-09-16 Aacosta     Manejo de valores nulos para el producto.
-- 2009-11-24 Aacosta     Modificación del cursor c_cargar_equipos con el fin de incluir el campo agrupador_id_n
--												para efectos de cargar en el el dato de serial real de simcard para internet móvil.
-- 2009-11-24 Aacosta     Se incluye validación para adicionar en la configuracion del equipo simcard
--									      para internet móvil la 3766 con el serial real de la simcard.
-- 2010-03-09 Aobregon    Se pone en comentario la intruccion para decodificadores.					
						              --IF NVL(v_cambio.caract_2131,'N') = 'N' THEN
						              --	w_inconsistente := TRUE;
						              --END IF;
-- 2010-03-09 Aobregon    se utiliza la variable :blq_datos.inconsistencias para la visialiazacion de 
--                        inconsistencias.
-- 2010-05-19 Jsierrao    Se agrega la validación para que solo se puedan matricular las referencias que se encuentran activas en la tabla
--                        fnx_marcas_referencias
-- 2010-06-07 Jsierrao    Se agrega la condición para que los datos de mac y serial se inserten en la tabla fnx_equipos
-- 2010-08-17 Aacosta     Se comentarea la validación asociada a PIN donde se decia que se debe insertar esta característica
--											  si el producto era diferente a INTOMOV. Ya aplica tambien para este servicio.
-- 2010-08-17 Aacosta     Se comentarea la validación asociada a PUK donde se decia que se debe insertar esta característica
--											  si el producto era diferente a INTOMOV. Ya aplica tambien para este servicio.
-- 2010-08-31 Jsierrao    Se incluye la función trim para la inserción de datos
-- 2011-04-11 Wmendoza	  Si se está creando una SIMCAR, se debe verificar si el campo de ESTADO, viene 
-- 												NULL, en caso de no venir nulo, se le coloca el estado que tenga, caso de venir nulo, 
-- 												se coloca LIB.
-- 2011-04-11 Wmendoza		Se comenta la línea a fin de poder colocar el estado
-- 												que venga desde la tabla de carga inicial (v_estado_equ).
--												,ltrim(rtrim(v_equipo_ppal)), 'LIB', SYSDATE
-- 2011-04-11 Wmendoza		Se agrega el campo ESTADO que se carga inicialmente en el campo tipo_elemento_id_n
-- 2011-04-19 Wmendoza	  Se debe verificar si se está creando una USB ó una SIMCAR, en ese caso, se debe 
-- 												colocar la fecha nula.
-- 2011-04-19 Wmendoza		Se comenta la línea para colocar la fecha de estado de acuerdo a si viene
-- 												acompañanada o no de una USB.
-- 2011-04-20 Wmendoza		Solamente actualizo en FNX_EQUIPOS si los tipo_elemento_id son diferentes de SIMCAR o USBMOV
-- 2011-05-30 Wmendoza		De ahora en adelante se verifica que si el check está seleccionado, se trata de una carga de una SIM , genérica.
-- 												en ese caso, el estado que se le coloca es RSM.
-- 2011-05-30 Wmendoza	  De ahora en adelante se verifica que si el check está seleccionado, la fecha de estado es 19641204,
-- 												en caso contrario es el sysdate
-- 2012-04-05	Jrincong	  Ciclo para realizar un chequeo de las marcas y referencias de la tabla de Carga, para evitar cargar equipos
-- 											  de una marca - referencia que no existe,  que no está habilitada o que pertenece a una marca tipificada
-- 												como del producto 4G LTE.
-- 2013-09-04 Aarbob			REQ28278_Tangibilizacion. Se realiza modificación para que en los campos tipo_elemento_n y servicio_id_n se utilicen para poblar
--												las caracteristicas 1093-propiedad equipo y 5045 tipo oferta, se implementa logica para que se respete el valor del campo
--												tipo_elemento_id_n AS ESTADO, y este sea el estado que se ponga en el equipo REQ28278_Tangibilizacion
-- 2014-01-09 Aarbob			REQ28278_Tangibilizacion. Se Modifica la carga masiva de equipos, para que cuando sean equipos COMPRADOS 'CM', Sean cargados tres identificadores en la tabla de EQUIPOS, los cuales son:
--MAC(FNX_EQUIPOS.MAC_REAL),Identificador del cliente (FNX_EQUIPOS.EQUIPO_ID),Serial(FNX_EQUIPOS.SERIAL_REAL)


CURSOR c_cargar_equipos IS
SELECT 	marca_id, referencia_id, tipo_elemento_id, tipo_elemento
       ,UPPER(equipo_id) AS equipo_id
       ,identificador_id, servicio_id, producto_id
       ,marca_id_n AS pin, referencia_id_n AS puk
       ,UPPER(agrupador_id) AS Mac_Equipo, identificador_id_n AS caract_2131
       ,equipo_id_n AS numero_movil
       ,municipio_id,  empresa_id,agrupador_id_n AS serial_real_simcard ,-- 2014-01-09 Aarbob	REQ28278_Tangibilizacion, SERIAL del equipo, se utlizara para el serial real del equipo para equipos de tangibilización y para INTMOV.
       tipo_elemento_id_n AS ESTADO, --2011-04-11 Se agrega el campo ESTADO que se carga inicialmente en el campo tipo_elemento_id_n
       tipo_elemento_n AS caract_1093,  -- 2013-09-04 Aarbob
       servicio_id_n AS caract_5045 -- 2013-09-04 Aarbob
FROM   FNX_CAMBIOS_EQUIPOS;

v_cambio          					 c_cargar_equipos%ROWTYPE;
v_estado_referencia          FNX_MARCAS_REFERENCIAS.estado%TYPE;
v_tipo_equipo								 FNX_MARCAS_REFERENCIAS.tipo_equipo%TYPE;	

-- 2009-09-16 Aobregon    Se adicionan en el cursor c_existe_equipo de la tabla fnx_equipos los parametros
--                        p_marca_id y la p_referencia_id
CURSOR c_existe_equipo 
 ( p_equipo_id 		 IN FNX_EQUIPOS.equipo_id%TYPE)
IS
SELECT 'SI'
FROM   FNX_EQUIPOS
WHERE  equipo_id 				= p_equipo_id;

-- 2010-05-19 Jsierrao    Se agrega la validación para que solo se puedan matricular las referencias que se encuentran activas en la tabla
--                        fnx_marcas_referencias   

CURSOR c_referencia_equipo 
 ( p_referencia_id 		 IN FNX_MARCAS_REFERENCIAS.referencia_id%TYPE)
IS
SELECT estado
FROM   FNX_MARCAS_REFERENCIAS
WHERE  referencia_id 				= p_referencia_id;

w_existe_equipo   VARCHAR2(2) := 'NO';        
w_total_registros	NUMBER(5);
w_total_incon			NUMBER(5);
w_inconsistente		BOOLEAN;
w_contador	      NUMBER (5);
v_mensaje         VARCHAR2(1000);

v_cargar_como			VARCHAR2(15):=null;
v_serial_equipo		FNX_CAMBIOS_EQUIPOS.equipo_id%TYPE;
v_mac_equipo			FNX_CAMBIOS_EQUIPOS.agrupador_id%TYPE;
v_estado_equ			FNX_EQUIPOS.estado%type;

v_equipo_ppal     FNX_CAMBIOS_EQUIPOS.equipo_id%TYPE;

v_servicio				FNX_EQUIPOS.servicio_id%TYPE;
v_producto				FNX_EQUIPOS.producto_id%TYPE;
v_incon_valor			FNX_CAMBIOS_EQUIPOS.inconsistente%TYPE;
v_reg_insert			NUMBER(5);
v_fecha_estado		DATE;
v_mensaje_error		VARCHAR2(250);
e_error_marca			EXCEPTION;
						              	
-- Cursor para realizar un chequeo previo de las marcas y referencias de los equipos que se van a cargar
CURSOR  c_referencias_carga IS
SELECT 	DISTINCT marca_id, referencia_id, tipo_elemento_id
FROM    FNX_CAMBIOS_EQUIPOS
;

-- Cursor para determinar sí la marca y referencia del equipo se encuentra activa en la tabla de fnx_marcas_referencias   
-- y en tal caso, poder realizar la carga.
CURSOR c_datos_referencia
( p_marca        			IN FNX_MARCAS_REFERENCIAS.marca_id%TYPE,
  p_referencia 	 			IN FNX_MARCAS_REFERENCIAS.referencia_id%TYPE,
  p_tipo_elemento_id	IN FNX_TIPOS_ELEMENTOS.tipo_elemento_id%TYPE
)
IS
SELECT estado, tipo_equipo
FROM   FNX_MARCAS_REFERENCIAS
WHERE  marca_id         = p_marca 
AND    referencia_id 	  = p_referencia
AND    tipo_elemento_id = p_tipo_elemento_id
;

BEGIN

-- 2012-04-05 /12
-- Ciclo para realizar un chequeo de las marcas y referencias de la tabla de Carga, para evitar cargar equipos
-- de una marca - referencia que no existe,  que no está habilitada o que pertenece a una marca tipificada
-- como del producto 4G LTE.
FOR reg IN c_referencias_carga LOOP
    -- Buscar los datos del estado de la referencia y el tipo de Equipo de carga
    OPEN  c_datos_referencia ( reg.marca_id, reg.referencia_id, reg.tipo_elemento_id);
    FETCH c_datos_referencia INTO v_estado_referencia, v_tipo_equipo;        
    IF  c_datos_referencia%NOTFOUND THEN
    	  v_estado_referencia := 'NE';
    END IF;	      	
    CLOSE c_datos_referencia;        
    
    IF v_estado_referencia = 'INA' THEN        	
       w_inconsistente := TRUE;	
       v_mensaje_error  := 'Error: La marca y la referencia <'||reg.marca_id||' * '||reg.referencia_id||'> no se encuentran habilitadas '||
                           'para el tipo de elemento <'||reg.tipo_elemento_id||'>';
    ELSIF v_estado_referencia = 'NE' THEN 
    	 w_inconsistente := TRUE;
       v_mensaje_error  := 'Error: La marca y la referencia <'||reg.marca_id||' * '||reg.referencia_id||'> no existen  '||
                           'para el tipo de elemento <'||reg.tipo_elemento_id||'>';
    ELSIF NVL(v_estado_referencia, 'ACT') = 'ACT' THEN
    	 -- Verificar el tipo de Equipo para no permitir cargar equipos de las marcas tipificadas como LTE.  Esto es
    	 -- para indicar al usuario que la carga se debe hacer por la opción respectiva.
    	 IF NVL(v_tipo_equipo, 'OTR') = 'LTE' THEN
    	    w_inconsistente := TRUE;
          v_mensaje_error  := 'Error: La marca y la referencia <'||reg.marca_id||' * '||reg.referencia_id||'> pertenece a Equipos 4G LTE. '||
                              'Debe realizar la carga a través de la opción <Crear equipos 4G LTE>';    	 	
    	 END IF;	
    END IF; 
    
    IF w_inconsistente THEN
    	 EXIT;
    END IF;
END LOOP;

-- Sí se presentó un error revisando las marcas y referencias, se genera la excepcion e_error_marca.
IF w_inconsistente THEN
   RAISE e_error_marca;
END IF;

v_reg_insert      := 0;
w_total_registros	:= 0;
w_total_incon			:= 0;
w_inconsistente		:= FALSE;
:blq_datos.inconsistencias := 'N';

-- Cursor principal para realizar la carga de Equipos		
OPEN c_cargar_equipos;
LOOP

		FETCH c_cargar_equipos INTO v_cambio;
			   EXIT WHEN c_cargar_equipos%NOTFOUND;
			
		w_total_registros := w_total_registros + 1;
		w_inconsistente		:= FALSE;
				        
		-- 2008-02-12  Aacosta   
		-- Validar si se está cargando equipos de Movilidad, se seleccione la oficina a la cual
    -- se cargarán los equipos.		    
		IF v_cambio.tipo_elemento_id IN ('MOVIL', 'SIMCAR')AND NVL(v_cambio.producto_id,'N') <> 'INTMOV' THEN

		   v_servicio  := 'TELMOV';
			 v_producto  := 'MOVIL';

			-- 2008-02-12  Aacosta 
			-- Validar que para los tipos elemento id SIMCARD venga el valor del PIN y PUK.
			IF  v_cambio.tipo_elemento_id = 'SIMCAR' THEN
					IF (NVL(v_cambio.pin,'N') = 'N' OR NVL(v_cambio.puk,'N') = 'N') THEN
						w_inconsistente := TRUE;
					END IF;
			END IF;

		  -- 2009-08-31  Aacosta  Validación asociada a la característica 4362 (Teléfono móvil), para que sea
		  --					            cargada en el portafolio del equipo.
		  -- 2009-09-16  Aacosta  manejo de valores nulos para el producto.
		ELSIF NVL(v_cambio.numero_movil,'N') = 'N' AND NVL(v_cambio.producto_id,'N') = 'INTMOV' AND
				  v_cambio.tipo_elemento_id	= 'SIMCAR'
		THEN
				w_inconsistente := TRUE;
				Msg_Alert ('El valor de la característica 4362 (Teléfono móvil) del archivo es nula. Verifique  <'||v_cargar_como||'>' ,'I',FALSE);
		ELSIF NVL(v_cambio.producto_id,'N') = 'INTMOV' AND
				  v_cambio.tipo_elemento_id	= 'USBMOV'
		THEN
			  v_servicio  := 'DATOS';
			  v_producto  := 'INTMOV';						
		ELSIF v_cambio.tipo_elemento_id = 'DECO' THEN
			  v_servicio  := 'ENTRE';
			  v_producto  := 'TELEV';

			 	-- 2010-03-09 Aobregon    Se pone en comentario la instruccion.					
				--IF NVL(v_cambio.caract_2131,'N') = 'N' THEN
				--	w_inconsistente := TRUE;
				--END IF;
		ELSE
			 v_servicio  := NULL;
			 v_producto  := NULL;					
		END IF;
		
		-- En caso que no se hayan presentado inconsistencias, se procesa a la carga del Equipo
    IF NOT w_inconsistente THEN

			 -- Verificar sí el equipo que se va a cargar ya existe en la tabla FNX_EQUIPOS
			 OPEN  c_existe_equipo ( v_cambio.equipo_id );
			 FETCH c_existe_equipo INTO w_existe_equipo;

		   IF  c_existe_equipo%NOTFOUND THEN 

			    -- 2008-01-15
			    -- Determinar el valor que se guarda en el campo EQUIPO_ID para poder
			    -- almacenar correctamente el serial o la MAC	
			    IF v_cargar_como IS NULL THEN	
			       v_cargar_como := FN_dominio_descripcion ('EQU_TIPOELEM_SERMAC', v_cambio.tipo_elemento_id);
			    END IF;
			    
			    IF v_cargar_como    = 'EQUIPO_ID' THEN
			    	  v_equipo_ppal  := v_cambio.equipo_id;
			    ELSIF v_cargar_como = 'MAC_ID' THEN
			    	  v_equipo_ppal  := NVL(v_cambio.mac_equipo, v_cambio.equipo_id); 					    	  
			    ELSE
			        v_equipo_ppal  := v_cambio.equipo_id;
			    END IF;
				    
			    -- 2011-04-11 Si se está creando una SIMCAR, se debe verificar si el campo de ESTADO, viene 
			    -- NULL, en caso de no venir nulo, se le coloca el estado que tenga, caso de venir nulo, 
			    -- se coloca LIB.
			    IF NVL(v_cambio.producto_id,'N') = 'INTMOV' AND v_cambio.tipo_elemento_id = 'SIMCAR' THEN
			   	   -- IF v_cambio.ESTADO IS NOT NULL THEN
			   	 	 -- 2011-05-30 De ahora en adelante se verifica que si el check está seleccionado, se trata de una carga de una SIM , genérica.
			   	 	 -- en ese caso, el estado que se le coloca es RSM
			   	 	 IF CHECKBOX_CHECKED('BLQ_DATOS.CHECK_GENERICA')	THEN
			    		  -- v_estado_equ:=	v_cambio.ESTADO;
			    			v_estado_equ:=	'RSM';
			    	 ELSE	
			    		  v_estado_equ:='LIB';
			    	 END IF;
			    -- 2013-09-04 Aarbob, si el campo estado se encuentra lleno se debe respetar el valor, para el caso del desarrollo especifico este
			    --estado es RES	 
			   	ELSIF NVL(v_cambio.estado,'X') <> 'X' THEN
			   	 	 	
			   	 		v_estado_equ:= 	v_cambio.estado;
			   	 	 	
			   	ELSE
			   	    v_estado_equ := 'LIB';
			   	END IF; 

			    BEGIN             

					    -- 2011-04-19 Se debe verificar si se está creando una USB ó una SIMCAR, en ese caso, se debe 
					    -- colocar la fecha nula
					    IF v_cambio.tipo_elemento_id IN ('SIMCAR','USBMOV') THEN
					     	 -- 2011-05-30 De ahora en adelante se verifica que si el check está seleccionado, la fecha de estado es 19641204,
								 -- en caso contrario es el sysdate
					    	 IF v_cambio.tipo_elemento_id	= 'SIMCAR'-- AND v_cambio.estado='RSM' THEN
					    	    AND CHECKBOX_CHECKED('BLQ_DATOS.CHECK_GENERICA')
					    	 THEN
					    			v_fecha_estado:=to_date('19641204','yyyymmdd');
					    	 ELSIF	v_cambio.tipo_elemento_id	= 'SIMCAR'AND NOT CHECKBOX_CHECKED('BLQ_DATOS.CHECK_GENERICA')
					    	 THEN
					    			--AND v_cambio.estado<>'RSM' THEN
					    			v_fecha_estado := SYSDATE;
					    	 ELSE
					    			v_fecha_estado :=SYSDATE;
					    	 END IF;
					    ELSE
					    	 v_fecha_estado  := SYSDATE;
					    END IF;		

							-- 2011-04-11 Se comenta la línea a fin de poder colocar el estado
							-- que venga desde la tabla de carga inicial (v_estado_equ).
							--,ltrim(rtrim(v_equipo_ppal)), 'LIB', SYSDATE
							-- 2011-04-19 Se comenta la línea para colocar la fecha de estado de acuerdo a si viene
							-- acompañanada o no de una USB
							--,ltrim(rtrim(v_equipo_ppal)), v_estado_equ, SYSDATE

		          -- 2011-04-20 
		          -- Los datos de Equipo_ID y Mac_Equipo se actualizan en FNX_EQUIPOS si los tipo_elemento_id son 
		          -- diferentes de SIMCAR o USBMOV
		          IF v_cambio.tipo_elemento_id IN ('SIMCAR','USBMOV') THEN
								 v_cambio.equipo_id   := NULL;
								 v_cambio.mac_equipo	:= NULL;
		          END IF;
				    	-- 2014-01-09 Aarbob
				    	IF 	v_cambio.caract_1093 = 'CM' THEN
				    	
				    			INSERT INTO fnx_equipos
										 (  marca_id, referencia_id
								  		,tipo_elemento_id, tipo_elemento
					  					,equipo_id, estado, fecha_estado
								  		,identificador_id, servicio_id, producto_id
								  		,ubicacion_id
								  		,municipio_id,  empresa_id
								  		,serial_real,   mac_real)
								  VALUES
										( ltrim(rtrim(v_cambio.marca_id)), ltrim(rtrim(v_cambio.referencia_id))
			  						 ,ltrim(rtrim(v_cambio.tipo_elemento_id)), ltrim(rtrim(v_cambio.tipo_elemento))
										 ,ltrim(rtrim(v_cambio.equipo_id)), v_estado_equ, v_fecha_estado
										 ,ltrim(rtrim(v_cambio.identificador_id)) 
										 ,ltrim(rtrim(NVL(v_cambio.servicio_id, v_servicio))), ltrim(rtrim(NVL(v_cambio.producto_id, v_producto)))
										 ,ltrim(rtrim(:BLQ_CONTROLES.oficina_id))
										 ,ltrim(rtrim(NVL(v_cambio.municipio_id, :BLQ_DATOS.municipio_carga)))
										 ,ltrim(rtrim(NVL(v_cambio.empresa_id,   :BLQ_DATOS.empresa_carga)))
										 ,v_cambio.serial_real_simcard, v_cambio.mac_equipo);	
				    	
				    	ELSE			
				    		
								INSERT INTO fnx_equipos
										 (  marca_id, referencia_id
								  		,tipo_elemento_id, tipo_elemento
					  					,equipo_id, estado, fecha_estado
								  		,identificador_id, servicio_id, producto_id
								  		,ubicacion_id
								  		,municipio_id,  empresa_id
								  		,serial_real,   mac_real						
										 )
								VALUES
										( ltrim(rtrim(v_cambio.marca_id)), ltrim(rtrim(v_cambio.referencia_id))
			  						 ,ltrim(rtrim(v_cambio.tipo_elemento_id)), ltrim(rtrim(v_cambio.tipo_elemento))
										 ,ltrim(rtrim(v_equipo_ppal)), v_estado_equ, v_fecha_estado
										 ,ltrim(rtrim(v_cambio.identificador_id)) 
										 ,ltrim(rtrim(NVL(v_cambio.servicio_id, v_servicio))), ltrim(rtrim(NVL(v_cambio.producto_id, v_producto)))
										 ,ltrim(rtrim(:BLQ_CONTROLES.oficina_id))
										 ,ltrim(rtrim(NVL(v_cambio.municipio_id, :BLQ_DATOS.municipio_carga)))
										 ,ltrim(rtrim(NVL(v_cambio.empresa_id,   :BLQ_DATOS.empresa_carga)))
										 ,v_cambio.equipo_id, v_cambio.mac_equipo
										);
							END IF;			
								
							-- 2009-08-31  Aacosta  Creación de la numeración asociada al equipo SIMCARD, según la característica 4362.
							IF NVL(v_cambio.producto_id,'N') = 'INTMOV' AND v_cambio.tipo_elemento_id = 'SIMCAR' 
							THEN		
							   PKG_IDENTIFICADORES.pr_crear_identificador
								  	( v_cambio.numero_movil,'LIB',NULL,'DATOS','INTMOV','INTMOV','SRV','INALA',
								  	  v_mensaje,NVL(v_cambio.municipio_id, :BLQ_DATOS.municipio_carga),
								  	  NVL(v_cambio.empresa_id, :BLQ_DATOS.empresa_carga));
							   
							   IF v_mensaje IS NOT NULL THEN
							   	  Msg_Alert ('El identificador '||v_cambio.numero_movil||' para el producto Internet Móvil ya existe. verifique.' ,'I',FALSE);
							   	  RAISE FORM_TRIGGER_FAILURE;
							   END IF;
							END IF;
								
		          -- 2008-01-15
		          -- Inserción de característica MAC Equipo ( 3765 )
		          IF v_cambio.mac_equipo IS NOT NULL and NVL(v_cambio.producto_id,'N') <> 'INTMOV' THEN
		          	  PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
							                              ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
							                              ,v_equipo_ppal, 3765, ltrim(rtrim(v_cambio.mac_equipo)), v_mensaje );
		          END IF;
		          
		          -- 2008-01-31
		          -- Inserción de característica Serial Real Equipo ( 3766 )
		          -- 2014-01-09 Aarbob
		          IF v_cambio.equipo_id IS NOT NULL AND NVL(v_cambio.producto_id,'N') <> 'INTMOV' AND v_cambio.caract_1093 <> 'CM'
		          THEN
		          	  PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
							                              ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
							                              ,v_equipo_ppal, 3766, ltrim(rtrim(v_cambio.equipo_id)), v_mensaje );
		
							-- 2009-11-24  Aacosta  Se incluye validación para adicionar en la configuracion del equipo simcard
							--									    para internet móvil la 3766 con el serial real de la simcard.
		          ELSE
		          	  IF v_cambio.equipo_id IS NOT NULL AND NVL(v_cambio.serial_real_simcard,'N') <> 'N' 
		          	  THEN
		            	   PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
								                               ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
								                               ,v_equipo_ppal, 3766, ltrim(rtrim(v_cambio.serial_real_simcard)), v_mensaje );      
							    END IF;              	
		          END IF;

		          -- 2008-02-12   Aacosta  Inserción de la característica PIN (1475).
		          -- 2010-08-17   Aacosta  Se comentarea la validación asociada a PIN donde se decia que se debe insertar esta característica
		          --											 si el producto era diferente a INTOMOV. Ya aplica tambien para este servicio.
		          IF v_cambio.pin IS NOT NULL THEN --and NVL(v_cambio.producto_id,'N') <> 'INTMOV' THEN
		          	  PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
							                              ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
							                              ,v_equipo_ppal, 1475, ltrim(rtrim(v_cambio.pin)), v_mensaje );
		          END IF;
                
		          -- 2008-02-12   Aacosta  Inserción de la característica PUK (1476).
		          -- 2010-08-17   Aacosta  Se comentarea la validación asociada a PUK donde se decia que se debe insertar esta característica
		          --											 si el producto era diferente a INTOMOV. Ya aplica tambien para este servicio.                    
		          IF v_cambio.puk IS NOT NULL THEN -- and NVL(v_cambio.producto_id,'N') <> 'INTMOV' THEN
		          	  PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
							                              ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
							                              ,v_equipo_ppal, 1476, ltrim(rtrim(v_cambio.puk)), v_mensaje );
		          END IF;			                    

					    -- 2008-07-29   Aacosta  Inserción de la característica 2131 para Decos de cableras.
		          IF v_cambio.caract_2131 IS NOT NULL and NVL(v_cambio.producto_id,'N') <> 'INTMOV' THEN
		          	  PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
							                              ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
							                              ,v_equipo_ppal, '2131', ltrim(rtrim(v_cambio.caract_2131)), v_mensaje );
		          END IF;	
              
		          -- 2009-08-31  Aacosta  Inserción de la característica 4362 (Teléfono móvil) en la configuracion
		          --	                    de las simcard's para el producto Internet Móvil.
		          IF v_cambio.numero_movil IS NOT NULL and NVL(v_cambio.producto_id,'N') = 'INTMOV' AND
		          	 v_cambio.tipo_elemento_id = 'SIMCAR'
		          THEN
		          	  PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
							                              ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
							                              ,v_equipo_ppal, 4362, ltrim(rtrim(v_cambio.numero_movil)), v_mensaje );
		          END IF;                    
							
							-- 2013-09-04 Aarbob inserción de la caracteristica 1093 - Propiedad Equipo
							IF v_cambio.caract_1093 IS NOT NULL  THEN
		          	  PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
							                              ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
							                              ,v_equipo_ppal, 1093, ltrim(rtrim(v_cambio.caract_1093)), v_mensaje );
		          END IF; 
							
							-- 2013-09-04 Aarbob inserción de la caracteristica 5045 - Tipo Oferta
							IF v_cambio.caract_5045 IS NOT NULL  THEN
		          	  PKG_EQUIPOS_UTIL.pr_insertar_conf_equ ( v_cambio.marca_id, v_cambio.referencia_id
							                              ,v_cambio.tipo_elemento_id, v_cambio.tipo_elemento
							                              ,v_equipo_ppal, 5045, ltrim(rtrim(v_cambio.caract_5045)), v_mensaje );
		          END IF; 
							
							
							v_reg_insert  := v_reg_insert + 1;

							IF MOD ( v_reg_insert, 50 ) = 0 THEN
								 COMMIT;								
							END IF;
								
				 EXCEPTION 
				 		WHEN OTHERS THEN
	 					   w_inconsistente	:= TRUE;
	 					   v_incon_valor   := 'I';
	 					   w_total_incon		:= w_total_incon + 1;				 					  
				 END;
               
			ELSE
				 		w_inconsistente := TRUE;
				 		v_incon_valor   := 'E'; 					 	  
				 		w_total_incon		:= w_total_incon + 1;
			END IF;
			CLOSE c_existe_equipo; /* Fin Consulta de verificación de existencia de equipo*/
   		   
   ELSE
		  w_inconsistente 	:= TRUE;	
		  v_incon_valor    := 'M';
		  w_total_incon 		:= w_total_incon + 1; 					 
   END IF; /* Fin condición antes de abrir cursor c_existe_equipo */
			
	 IF w_inconsistente THEN
		  UPDATE FNX_CAMBIOS_EQUIPOS 
		  SET    inconsistente    = v_incon_valor
		  WHERE  marca_id         = v_cambio.marca_id
		  AND    referencia_id    = v_cambio.referencia_id
		  AND    tipo_elemento_id = v_cambio.tipo_elemento_id
		  AND    tipo_elemento    = v_cambio.tipo_elemento  
		  AND    equipo_id        = v_cambio.equipo_id ;
		  :blq_datos.inconsistencias := 'S';
	END IF;		

END LOOP;
CLOSE c_cargar_equipos;
		
COMMIT; 

Msg_Alert ('El proceso de creación masiva de equipos ha finalizado. Tipo Equipo <'||v_cargar_como||'>' ,'I',FALSE);

Msg_Alert ('Número total de registros procesados < '||to_char(w_total_registros)||' >' || CHR(13)
            ||' Registros cargados        < '||to_char(v_reg_insert)||'  >' || CHR(13)
            ||' Registros inconsistentes  < '||to_char(w_total_incon)||' >','I', FALSE);


EXCEPTION
	 WHEN e_error_marca THEN
	      Msg_Alert(v_mensaje_error, 'I', FALSE);
	      Msg_Alert('No se ha realizado el proceso de carga. Por favor verifique los datos de marca y referencia de los equipos a cargar.',
                  'I', FALSE );	                
	 WHEN OTHERS THEN
	      COMMIT;
        Msg_Alert('Se ha presentado un error en la carga de registros de Equipos. Por favor tome nota del error.', 'I', FALSE);
        Msg_Alert(SUBSTR(SQLERRM, 1, 150), 'I', FALSE);
        		
END;