import java.io.*;
import java.util.*;
import java.util.stream.*;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
interface Conversor{
	public String c(Object o);
}
public class Main{
	public static List<String>gids(File f)throws Exception{
		List<String>r=new ArrayList<>();
		Scanner s=new Scanner(f);
		while(s.hasNext()){
			r.add(s.next());
		}
		return r;
	}
	public static HashMap<String,Conversor>g(){
		HashMap<String,Conversor>r=new HashMap<>();
		r.put("java.lang.String",new Conversor(){
			public String c(Object o){
				if(o==null){
					return "''";
				}
				return "'"+o+"'";
			}
		});
		r.put("java.math.BigDecimal",new Conversor(){
			public String c(Object o){
				if(o==null){
					return "''";
				}
				return o+"";
			}
		});
		r.put("oracle.jdbc.OracleClob",new Conversor(){
			public String c(Object o){
				if(o==null){
					return "''";
				}
				try{
					java.sql.Clob x=(java.sql.Clob)o;
					ArrayList<String>ps=new ArrayList<>();
					for(int i=0;i<x.length();i+=4000){
						ps.add("TO_CLOB(q'["+x.getSubString(1,4000)+"]')");
					}
					return ps.stream().collect(Collectors.joining("||"));
				}catch(Exception e){c.U.imp(e);
					return "";
				}
			}
		});
		return r;
	}
	public static void main(String[]argumentos){
		try{
			c.U.imp("INICIO");
			String tabla="ts_def_rep_contenidos";
			String consulta="select referencia,orden,dobjeto_id,tipo_registro,texto from "+tabla+" order by id";
			int[]pk=new int[]{1,2,3,4};
			long tini=new Date().getTime();
			HashMap<String,Conversor>h=g();
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection x=DriverManager.getConnection("jdbc:oracle:thin:@10.18.203.19:1521/MARTE.COTRAFA","sic","cot202103");
			x.setAutoCommit(false);
			List<String>r=new ArrayList<>();
			c.U.ruta="salida\\ts_def_rep_contenidos(DATOS).sql";
			c.U.ps=null;
			c.U.imp("DECLARE");
			c.U.imp("\tc NUMBER;");
			c.U.imp("BEGIN");
			{
				Statement statement=x.createStatement();
				ResultSet rs=statement.executeQuery(consulta);
				/*for(int i=1;i<=rs.getMetaData().getColumnCount();i++){
					c.U.imp(rs.getMetaData().getColumnName(i)+":"+rs.getMetaData().getColumnClassName(i));
				}*/
				while(rs.next()){
					{
						StringBuilder sb=new StringBuilder("\tSELECT COUNT(1)INTO c FROM "+tabla+" WHERE");
						for(int j=0;j<pk.length;j++){
							if(j>0){
								sb.append(" AND");
							}
							sb.append(" "+rs.getMetaData().getColumnName(pk[j])+"="+h.get(rs.getMetaData().getColumnClassName(pk[j])).c(rs.getObject(pk[j])));
						}
						sb.append(";");
						c.U.imp(sb+"");
					}
					{
						c.U.imp("\tIF c=0 THEN");
						StringBuilder sb=new StringBuilder("\t\tINSERT INTO "+tabla+"(");
						for(int i=1;i<=rs.getMetaData().getColumnCount();i++){
							if(i>1){
								sb.append(",");
							}
							sb.append(rs.getMetaData().getColumnName(i));
						}
						sb.append(",ID)VALUES(");
						for(int i=1;i<=rs.getMetaData().getColumnCount();i++){
							if(i>1){
								sb.append(",");
							}
							sb.append(h.get(rs.getMetaData().getColumnClassName(i)).c(rs.getObject(i)));
						}
						sb.append(",m");
						c.U.imp(sb+");");
						c.U.imp("\t\tm:=m+1;");
						c.U.imp("\tELSE");
						sb=new StringBuilder("\t\tUPDATE "+tabla+" SET ");
						for(int i=1;i<=rs.getMetaData().getColumnCount();i++){
							if(i>1){
								sb.append(",");
							}
							sb.append(rs.getMetaData().getColumnName(i)+"="+h.get(rs.getMetaData().getColumnClassName(i)).c(rs.getObject(i)));
						}
						sb.append(" WHERE");
						for(int j=0;j<pk.length;j++){
							if(j>0){
								sb.append(" AND");
							}
							sb.append(" "+rs.getMetaData().getColumnName(pk[j])+"="+h.get(rs.getMetaData().getColumnClassName(pk[j])).c(rs.getObject(pk[j])));
						}
						c.U.imp(sb+";");
						c.U.imp("\tEND IF;");
					}
				}
				statement.close();
			}
			c.U.imp("\tCOMMIT;");
			c.U.imp("END;");
			c.U.ruta="log.txt";
			c.U.ps=null;
			c.U.imp("t:"+(new Date().getTime()-tini));
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}