	public List<String>asignarBaseValidar(Integer cuadrePrincipal,String codigoCaja,Integer valor)throws Exception{
		ArrayList<Exception>excepciones=new ArrayList<>();
		ArrayList<String>resultado=new ArrayList<>();
		((org.hibernate.internal.SessionImpl)entityManager.getDelegate()).doWork(x->{
			try{
				java.sql.CallableStatement s=x.prepareCall(Util.leerRecurso("scripts/AsignarBase.sql"));
				s.setBigDecimal(2,new BigDecimal(cuadrePrincipal));
				s.setString(3,codigoCaja);
				s.setBigDecimal(4,new BigDecimal(valor));
				s.registerOutParameter(1,java.sql.Types.VARCHAR);
				s.registerOutParameter(5,java.sql.Types.VARCHAR);
				s.executeUpdate();
				resultado.add((String)s.getObject(1));
				resultado.add((String)s.getObject(5));
			}catch(Exception ex){
				excepciones.add(ex);
			}
		});
		if(excepciones.size()>0){
			throw excepciones.get(0);
		}
		return resultado;
	}
