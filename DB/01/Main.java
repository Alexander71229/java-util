import java.io.*;
import java.util.*;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
public class Main{
	public static List<String>gids(File f)throws Exception{
		List<String>r=new ArrayList<>();
		Scanner s=new Scanner(f);
		while(s.hasNext()){
			r.add(s.next());
		}
		return r;
	}
	public static void main(String[]argumentos){
		try{
			c.U.imp("INICIO");
			long tini=new Date().getTime();
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection x=DriverManager.getConnection("jdbc:oracle:thin:@10.18.203.24:1521/JUPITER.COTRAFA","sic","cot202006");
			x.setAutoCommit(false);
			List<String>r=new ArrayList<>();
			String k="";
			String nk="";
			String tipo=null;
			{
				Statement statement=x.createStatement();
				ResultSet rs=statement.executeQuery("select type,name,text from all_source where owner='SIC' order by type,name,line");
				while(rs.next()){
					nk=rs.getString("type")+":"+rs.getString("name");
					if(!nk.equals(k)){
						k=nk;
						tipo="";
						if(rs.getString("type").equals("PACKAGE BODY")){
							tipo="(body)";
						}
						c.U.ruta="salida\\"+rs.getString("name")+tipo+".sql";
						c.U.ps=null;
					}
					c.U.inp(rs.getString("text"));
				}
				statement.close();
			}
			c.U.ruta="log.txt";
			c.U.ps=null;
			c.U.imp("t:"+(new Date().getTime()-tini));
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}