import java.io.*;
import java.util.*;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
public class Main{
	public static List<String>gids(File f)throws Exception{
		List<String>r=new ArrayList<>();
		Scanner s=new Scanner(f);
		while(s.hasNext()){
			r.add(s.next());
		}
		return r;
	}
	public static void main(String[]argumentos){
		try{
			c.U.imp("INICIO");
			long tini=new Date().getTime();
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//Connection x=DriverManager.getConnection("jdbc:oracle:thin:@10.18.203.24:1521/JUPITER.COTRAFA","sic","cot202006");
			Connection x=DriverManager.getConnection("jdbc:oracle:thin:@10.5.1.6:1521/PROD","amolina","aeclun2109");
			x.setAutoCommit(false);
			List<String>r=new ArrayList<>();
			String k="";
			String tipo=null;
			HashMap<String,ArrayList<String>>codigo=new HashMap<>();
			{
				Statement statement=x.createStatement();
				ResultSet rs=statement.executeQuery("select type,name,text from all_source where owner='SIC' order by type,name,line");
				while(rs.next()){
					tipo="";
					if(rs.getString("type").equals("PACKAGE BODY")){
						tipo="(body)";
					}
					k=rs.getString("name")+tipo;
					if(codigo.get(k)==null){
						codigo.put(k,new ArrayList<>());
					}
					codigo.get(k).add(rs.getString("text"));
				}
				statement.close();
			}
			long t2=new Date().getTime()-tini;
			for(String n:codigo.keySet()){
				c.U.ruta="log.txt";
				c.U.ps=null;
				c.U.imp(n+"");
				c.U.ruta="salida\\"+n+".sql";
				c.U.ps=null;
				for(int i=0;i<codigo.get(n).size();i++){
					c.U.inp(codigo.get(n).get(i));
				}
			}
			c.U.ruta="log.txt";
			c.U.ps=null;
			c.U.imp("t2:"+t2);
			c.U.imp("t:"+(new Date().getTime()-tini));
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}