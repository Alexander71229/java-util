from sqlalchemy.orm import Session
from db import models,schemas
def create_producto(db:Session,producto:schemas.ProductoCreate):
	db_producto=models.Producto(name=producto.name)
	db.add(db_producto)
	db.commit()
	db.refresh(db_producto)
	return db_producto
def get_producto(db:Session,id:int):
	return db.query(models.Producto).filter(models.Producto.id==id).first()