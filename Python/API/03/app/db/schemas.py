from pydantic import BaseModel
class ProductoBase(BaseModel):
	name:str
class ProductoCreate(ProductoBase):
	pass
class Producto(ProductoBase):
	id:int
	class Config:
		orm_mode=True
