package com.ejemplo;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.util.Properties;
public class Main {
	private static Properties properties=new Properties();
	public static String getResponse(String url)throws Exception{
		CloseableHttpClient httpClient=HttpClients.createDefault();
		HttpGet httpGet=new HttpGet(url);
		CloseableHttpResponse response=httpClient.execute(httpGet);
		HttpEntity entity=response.getEntity();
		return EntityUtils.toString(entity);
	}
	public static void main(String[]args){
		try{
			properties.load(Main.class.getClassLoader().getResourceAsStream("config.properties"));
			System.out.println(getResponse(properties.getProperty("url.consulta")));
		}catch (Throwable t){
			t.printStackTrace();
		}
	}
}
