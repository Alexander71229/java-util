package com.example;
public class Habilidad{
	private String cdHabilidad;
	private String dsHabilidad;
	public Habilidad(){
	}
	public String getCdHabilidad(){
		return cdHabilidad;
	}
	public void setCdHabilidad(String cdHabilidad){
		this.cdHabilidad=cdHabilidad;
	}
	public String getDsHabilidad(){
		return dsHabilidad;
	}
	public void setDsHabilidad(String dsHabilidad){
		this.dsHabilidad=dsHabilidad;
	}
}