package com.example;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;
import com.google.gson.Gson;
import java.util.*;
@Path("myresource")
public class MyResource{
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt(){
		return"Got it!";
	}
	@GET
	@Path("habilidades/HabilidadesExpediente/{nmAsistencia}")
	@Produces("application/json")
	public Response consultarHabilidadesPorExpediente(@PathParam("nmAsistencia")String nmAsistencia){
		List<Habilidad>r=new ArrayList<>();
		r.add(new Habilidad());
		r.get(r.size()-1).setCdHabilidad("cd01");
		r.get(r.size()-1).setDsHabilidad("ds01");
		r.add(new Habilidad());
		r.get(r.size()-1).setCdHabilidad("cd02");
		r.get(r.size()-1).setDsHabilidad("ds02");
		return Response.ok().entity(new Gson().toJson(r)).build();
	}
}
