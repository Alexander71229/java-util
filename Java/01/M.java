import java.util.*;
import java.time.*;
import java.text.SimpleDateFormat;
public class M{
	public static LocalDate addDaysSkippingWeekends(LocalDate date,int days){
		LocalDate result=date;
		int addedDays=0;
		while(addedDays<days){
			result=result.plusDays(1);
			if (!(result.getDayOfWeek()==DayOfWeek.SATURDAY||result.getDayOfWeek()==DayOfWeek.SUNDAY)){
				++addedDays;
			}
		}
		return result;
	}
	public static Date agregarDias(Date fecha,int cantidad){
		long dia=86400000L;
		long semana=7*dia;
		int cantidadFinesSemana=(int)(cantidad+7-(fecha.getTime()%semana)/dia)/7; //Para evitar usar un loop, se usa una fórmula.
		return new Date(fecha.getTime()+dia*(cantidad+cantidadFinesSemana*2)); //Cada fin de semana tiene dos días.
	}
	public static Date agregarDiasSaltandoFinesSemana(Date fecha,int cantidad){
		Calendar calendario=Calendar.getInstance();
		calendario.setTime(fecha);
		int agregados=0;
		while(agregados<cantidad){
			calendario.add(Calendar.DATE,1);
			int diaSemana=calendario.get(Calendar.DAY_OF_WEEK);
			if(diaSemana!=7&&diaSemana!=1){
				agregados++;
			}
		}
		return calendario.getTime();
	}
	public static void main(String[]argumentos){
		Date od=new Date();
		try{
			for(int i=0;i<10;i++){
				for(int j=0;j<10;j++){
					Date d=new Date(od.getTime()+i*86400000L);
					LocalDate l=d.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					String a=new SimpleDateFormat("yyyy-MM-dd").format(agregarDiasSaltandoFinesSemana(d,j));
					String b=addDaysSkippingWeekends(l,j)+"";
					if(!a.equals(b)){
						c.U.imp(d+":"+i+","+j+"-->"+a+"<->"+b);
					}
				}
			}
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
	static{
		c.U.ruta="log.txt";
		c.U.imp("Inicio");
	}
}