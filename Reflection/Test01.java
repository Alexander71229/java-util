import java.lang.reflect.*;
import java.util.Date;
import java.util.Random;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.math.BigDecimal;
import c.U;
class A{
	private int a=3;
	private String b;
	private BigDecimal c;
	private Date d;
	public void setA(int a){
		this.a=a;
	}
	public void setB(String b){
		this.b=b;
	}
	public void setC(BigDecimal c){
		this.c=c;
	}
	public void setD(Date d){
		this.d=d;
	}
	public String toString(){
		return this.a+":"+this.b+":"+this.c+":"+this.d;
	}
}
public class Test01{
	public static String primeraMayuscula(String cadena){
		return cadena.substring(0,1).toUpperCase()+cadena.substring(1);
	}
	public static String cadenaAleatoria(int inicio,int rango,int tamano){
		return IntStream.rangeClosed(1,tamano).mapToObj(x->Character.toString((char)(new Random().nextInt(rango)+inicio))).collect(Collectors.joining());
	}
	public static BigDecimal decimalAleatorio(int tamano){
		return new BigDecimal(cadenaAleatoria(49,9,tamano));
	}
	public static<T>T generarValorAleatorio(Class<T>clase,int tamano){
		try{
			if(clase==Date.class){
				return clase.getDeclaredConstructor(long.class).newInstance(new Date().getTime()-new Random().nextInt(tamano)*86400000);
			}
			if(clase==String.class){
				return clase.getDeclaredConstructor(clase).newInstance(cadenaAleatoria(65,26,10));
			}
			if(clase==BigDecimal.class){
				return clase.cast(decimalAleatorio(tamano));
			}
			try{
				return clase.getDeclaredConstructor(String.class).newInstance(cadenaAleatoria(49,9,tamano));
			}catch(Exception e){
				c.U.imp(e);
			}
		}catch(Throwable t){
			c.U.imp(t);
			return null;
		}
		return null;
	}
	public static<T>T generarInstanciaAleatoria(Class<T>clase,int tamano){
		try{
			T instancia=clase.newInstance();
			Field[]camposDeclarados=clase.getDeclaredFields();
			Arrays.stream(camposDeclarados).forEach(campo->{
				try{
					Method metodo=clase.getMethod("set"+primeraMayuscula(campo.getName()),campo.getType());
					if(campo.getType()==int.class){
						metodo.invoke(instancia,generarValorAleatorio(Integer.class,tamano));
						return;
					}
					if(campo.getType()==long.class){
						metodo.invoke(instancia,generarValorAleatorio(Long.class,tamano));
						return;
					}
					if(campo.getType()==float.class){
						metodo.invoke(instancia,generarValorAleatorio(Float.class,tamano));
						return;
					}
					if(campo.getType()==double.class){
						metodo.invoke(instancia,generarValorAleatorio(Double.class,tamano));
						return;
					}
					metodo.invoke(instancia,generarValorAleatorio(campo.getType(),tamano));
				}catch(Exception e){
				}
			});
			return instancia;
		}catch(Throwable t){
			return null;
		}
	}
	public static void main(String[]argumentos){
		try{
			c.U.imp(generarInstanciaAleatoria(A.class,5)+"");
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}