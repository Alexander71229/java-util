import java.util.*;
import java.util.stream.*;
import java.util.function.Function;
import java.text.SimpleDateFormat;
public class UtilidadPresentacion{
	private static HashMap<Class,Function<Object,String>>conversores;
	private static Object ejecutarMetodo(Object objeto,String nombre){
		try{
			return objeto.getClass().getMethod(nombre).invoke(objeto);
		}catch(Throwable t){
			return null;
		}
	}
	private static String mayusculaInicial(String palabra){
		if(palabra.length()==0){
			return palabra;
		}
		return palabra.substring(0,1).toUpperCase()+palabra.substring(1);
	}
	public static String representacionCadena(Object objeto,String separadorValores,String separadorCampos,String inicio,String fin){
		return Arrays.stream(objeto.getClass().getDeclaredFields()).filter(campo->ejecutarMetodo(objeto,"get"+mayusculaInicial(campo.getName()))!=null).map(campo->{
			Object valor=ejecutarMetodo(objeto,"get"+mayusculaInicial(campo.getName()));
			if(conversores.get(valor.getClass())!=null){
				valor=conversores.get(valor.getClass()).apply(valor);
			}
			return(campo.getAnnotation(NombreColumna.class)==null?campo.getName():campo.getAnnotation(NombreColumna.class).value())+separadorValores+valor;
		}).collect(Collectors.joining(separadorCampos,inicio,fin));
	}
	public static void agregarConversor(Class clase,Function<Object,String>funcion){
		if(conversores==null){
			conversores=new HashMap<>();
		}
		conversores.put(clase,funcion);
	} 
	public static void main(String[]argumentos){
		try{
			//A a=new A(new Date(),"G4","D6","B1","C8");
			A a=new A(new Date(),"G4",null,"B1","C8");
			agregarConversor(Date.class,x->new SimpleDateFormat("ddMMyyyy").format(x));
			c.U.imp(representacionCadena(a,":",". ","","."));
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}
