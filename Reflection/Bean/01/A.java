import java.util.*;
public class A{
	@NombreColumna("CH")
	private Date campo01;
	@NombreColumna("UO")
	private String campo02;
	@NombreColumna("XO")
	private String campo03;
	@NombreColumna("OU")
	private String campo04;
	@NombreColumna("BI")
	private String campo05;
	public A(Date campo01,String campo02,String campo03,String campo04,String campo05){
		this.campo01=campo01;
		this.campo02=campo02;
		this.campo03=campo03;
		this.campo04=campo04;
		this.campo05=campo05;
	}
	public void setCampo01(Date campo01){
		this.campo01=campo01;
	}
	public Date getCampo01(){
		return this.campo01;
	}
	public void setCampo02(String campo02){
		this.campo02=campo02;
	}
	public String getCampo02(){
		return this.campo02;
	}
	public void setCampo03(String campo03){
		this.campo03=campo03;
	}
	public String getCampo03(){
		return this.campo03;
	}
	public void setCampo04(String campo04){
		this.campo04=campo04;
	}
	public String getCampo04(){
		return this.campo04;
	}
	public void setCampo05(String campo05){
		this.campo05=campo05;
	}
	public String getCampo05(){
		return this.campo05;
	}
}
