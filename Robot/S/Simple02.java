import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.MouseInfo;
public class Simple02{
	public static void main(String[]argumentos){
		Robot r=null;
		int k=1;
		try{
			r=new Robot();
			while(true){
				int x=(int)MouseInfo.getPointerInfo().getLocation().getX();
				int y=(int)MouseInfo.getPointerInfo().getLocation().getY();
				if(x>=k){
					x-=k;
				}else{
					x+=k;
				}
				r.mouseMove(x,y);
				r.delay(2000);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}