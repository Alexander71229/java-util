import org.jnativehook.*;
import org.jnativehook.keyboard.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
public class Mouse extends JFrame implements NativeKeyListener{
	public HashMap<Integer,Character>ks;
	public HashMap<Integer,int[]>ms;
	public JLabel etiqueta;
	public Robot r;
	public Mouse()throws Exception{
		ks=new HashMap<Integer,Character>();
		ms=new HashMap<Integer,int[]>();
		r=new Robot();
		int v=1;
		ms.put(57416,new int[]{0,-v});
		ms.put(57424,new int[]{0,v});
		ms.put(57419,new int[]{-v,0});
		ms.put(57421,new int[]{v,0});
		this.setSize(310, 210);
		this.setLocationRelativeTo(null);
		this.setLayout(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		etiqueta=new JLabel();
		etiqueta.setBounds(0,0,200,200);
		etiqueta.setText("Entradas");
		this.add(etiqueta);
	}
	public void mostrar(){
		String texto="";
		for(Map.Entry<Integer,Character>e:ks.entrySet()){
			texto+=e.getValue();
		}
		etiqueta.setText(texto);
	}
	public void nativeKeyPressed(NativeKeyEvent e){
		c.U.imp("P:"+e.getKeyCode()+":"+NativeKeyEvent.getKeyText(e.getKeyCode()));
		ks.put(e.getKeyCode(),'X');
		int x=(int)MouseInfo.getPointerInfo().getLocation().getX();
		int y=(int)MouseInfo.getPointerInfo().getLocation().getY();
		if(e.getKeyCode()==35){
			r.mouseMove(0,0);
		}
		try{
			r.mouseMove(x+ms.get(e.getKeyCode())[0],y+ms.get(e.getKeyCode())[1]);
		}catch(Exception ex){
		}
	}
	public void nativeKeyReleased(NativeKeyEvent e){
		c.U.imp("R:"+e.getKeyCode()+":"+NativeKeyEvent.getKeyText(e.getKeyCode()));
		ks.remove(e.getKeyCode());
	}
	public void nativeKeyTyped(NativeKeyEvent e){
		c.U.imp("T:"+e.getKeyCode()+":"+NativeKeyEvent.getKeyText(e.getKeyCode()));
	}
	public static void main(String[]argumentos){
		try{
			Mouse m=new Mouse();
			GlobalScreen.registerNativeHook();
			GlobalScreen.addNativeKeyListener(m);
			m.setVisible(true);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}