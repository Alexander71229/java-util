import java.awt.*;
import java.util.*;
import java.awt.event.*;
import java.util.*;
public class Prog{
	public static Robot r;
	public static int delay=50;
	public static void ejecutarComando(String comando,int veces)throws Exception{
		for(int i=0;i<veces;i++){
			ejecutarComando(comando);
		}
	}
	public static void ejecutarComando(String comando)throws Exception{
		char[]cr=comando.toCharArray();
		for(int i=0;i<cr.length;i++){
			if(cr[i]=='C'){
				r.keyPress(KeyEvent.VK_CONTROL);
				continue;
			}
			if(cr[i]=='S'){
				r.keyPress(KeyEvent.VK_SHIFT);
				continue;
			}
			if(cr[i]=='E'){
				r.keyPress(KeyEvent.VK_ENTER);
				continue;
			}
			if(cr[i]=='I'){
				r.mousePress(MouseEvent.BUTTON1_DOWN_MASK);
				continue;
			}
			if(cr[i]=='D'){
				r.mousePress(MouseEvent.BUTTON3_DOWN_MASK);
				continue;
			}
			r.keyPress(cr[i]-32);
		}
		r.delay(delay);
		for(int i=0;i<cr.length;i++){
			if(cr[i]=='C'){
				r.keyRelease(KeyEvent.VK_CONTROL);
				continue;
			}
			if(cr[i]=='S'){
				r.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			}
			if(cr[i]=='E'){
				r.keyRelease(KeyEvent.VK_ENTER);
				continue;
			}
			if(cr[i]=='I'){
				r.mouseRelease(MouseEvent.BUTTON1_DOWN_MASK);
				continue;
			}
			if(cr[i]=='D'){
				r.mouseRelease(MouseEvent.BUTTON3_DOWN_MASK);
				continue;
			}
			r.keyRelease(cr[i]-32);
		}
		r.delay(delay);
	}
	public static void ejecutarComandoTiempo(String comando,long tiempo)throws Exception{
		long ini=(new Date()).getTime();
		while((new Date()).getTime()-ini<tiempo){
			ejecutarComando(comando,1);
		}
	}
	public static void rect(int x1,int y1,int x2,int y2,long tiempo)throws Exception{
		int x=(int)MouseInfo.getPointerInfo().getLocation().getX();
		int y=(int)MouseInfo.getPointerInfo().getLocation().getY();
		long inicial=0;
		while(true){
			x=(int)MouseInfo.getPointerInfo().getLocation().getX();
			y=(int)MouseInfo.getPointerInfo().getLocation().getY();
			System.out.println(x+":"+y);
			if(x1<=x&&x<=x2&&y1<=y&&y<=y2){
				if(inicial==0){
					inicial=(new Date()).getTime();
				}else{
					if((new Date()).getTime()-inicial>tiempo){
						return;
					}
				}
			}else{
				inicial=0;
			}
		}
	}
	public static void main(String[]args){
		try{//Top
			r=new Robot();
			//r.delay(1000);
			rect(590,772,828,796,100);
			while(true){
				System.out.println(MouseInfo.getPointerInfo().getLocation().getX()+":"+MouseInfo.getPointerInfo().getLocation().getY());
				ejecutarComando("I");
				ejecutarComando("Sj");
				ejecutarComando("g");
				ejecutarComando("E");
				rect(590,772,828,796,100);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}