package f;
public abstract class Actor implements Oyente{
	protected double at;
	public void accion(int tipo,Actor destino)throws Exception{
	}
	public void pasiva(double t)throws Exception{
	}
	public boolean solicitar(Actor origen,String accion,Object[]parametros)throws Exception{
		return true;
	}
	public void realizada(Actor origen,String accion,Object[]parametros)throws Exception{
	}
	public void registrarse(String clave,String nombreMetodo){
		Control.registrar(clave,nombreMetodo,this);
	}
	public void registrarSolicitud(String clave,String nombreMetodo){
		this.registrarse(clave+":solicitar",nombreMetodo);
	}
	public void registrarRealizada(String clave,String nombreMetodo){
		this.registrarse(clave+":realizada",nombreMetodo);
	}
}