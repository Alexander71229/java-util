package f;
public interface Oyente{
	public boolean solicitar(Actor origen,String accion,Object[]parametros)throws Exception;
	public void realizada(Actor origen,String accion,Object[]parametros)throws Exception;
	public void registrarse(String clave,String nombreMetodo);
	public void registrarSolicitud(String clave,String nombreMetodo);
	public void registrarRealizada(String clave,String nombreMetodo);
}