package f;
import java.util.*;
public class Control{
	private static HashMap<String,ArrayList<Registro>>hashListaRegistros;
	public static void registrar(String clave,String nombreMetodo,Oyente oyente){
		Registro registro=new Registro(oyente,nombreMetodo);
		if(hashListaRegistros==null){
			hashListaRegistros=new HashMap<String,ArrayList<Registro>>();
		}
		if(hashListaRegistros.get(clave)==null){
			hashListaRegistros.put(clave,new ArrayList<Registro>());
		}
		hashListaRegistros.get(clave).add(registro);
	}
	public static boolean solicitar(Actor origen,String accion,Object[]parametros){
		return solicitar(origen,accion,parametros,"solicitar");
	}
	public static void realizada(Actor origen,String accion,Object[]parametros){
		solicitar(origen,accion,parametros,"realizada");
	}
	public static boolean solicitar(Actor origen,String accion,Object[]parametros,String tipo){
		if(hashListaRegistros==null){
			return true;
		}
		{
			String clave="Actor."+accion+":"+tipo;
			ArrayList<Registro>listaRegistros=new ArrayList<Registro>();
			if(hashListaRegistros.get(clave)!=null){
				listaRegistros.addAll(hashListaRegistros.get(clave));
			}
			if(listaRegistros.size()>0){
				for(int i=0;i<listaRegistros.size();i++){
					try{
						if(!listaRegistros.get(i).solicitarGeneral(origen,accion,parametros)){
							return false;
						}
					}catch(Exception e){
					}
				}
			}
		}
		{
			String clave=origen.getClass().getSimpleName()+"."+accion+":"+tipo;
			ArrayList<Registro>listaRegistros=new ArrayList<Registro>();
			if(hashListaRegistros.get(clave)!=null){
				listaRegistros.addAll(hashListaRegistros.get(clave));
			}
			clave=origen+"."+accion+":"+tipo;
			if(hashListaRegistros.get(clave)!=null){
				listaRegistros.addAll(hashListaRegistros.get(clave));
			}
			if(listaRegistros.size()>0){
				for(int i=0;i<listaRegistros.size();i++){
					try{
						if(!listaRegistros.get(i).solicitar(origen,accion,parametros)){
							return false;
						}
					}catch(Exception e){
					}
				}
			}
		}
		return true;
	}
	public static void fin(){
		hashListaRegistros=null;
	}
}