package f;
import java.lang.reflect.*;
public class Registro{
	private Oyente oyente;
	private String nombreMetodo;
	public Registro(Oyente oyente,String nombreMetodo){
		this.oyente=oyente;
		this.nombreMetodo=nombreMetodo;
	}
	public boolean solicitarGeneral(Actor origen,String accion,Object[]parametros)throws Exception{
		Class[]tipos=new Class[]{Actor.class,String.class,Object[].class};
		Method metodo=oyente.getClass().getMethod(nombreMetodo,tipos);
		Boolean resultado=(Boolean)metodo.invoke(oyente,new Object[]{origen,accion,parametros});
		return resultado;
	}
	public boolean solicitar(Actor origen,String accion,Object[]parametros)throws Exception{
		Class[]tipos=new Class[]{origen.getClass(),String.class,Object[].class};
		Method metodo=oyente.getClass().getMethod(nombreMetodo,tipos);
		Boolean resultado=(Boolean)metodo.invoke(oyente,new Object[]{origen,accion,parametros});
		return resultado;
	}
	public void realizadaGeneral(Actor origen,String accion,Object[]parametros)throws Exception{
		Class[]tipos=new Class[]{Actor.class,String.class,Object[].class};
		Method metodo=oyente.getClass().getMethod(nombreMetodo,tipos);
		metodo.invoke(oyente,new Object[]{origen,accion,parametros});
	}
	public void realizada(Actor origen,String accion,Object[]parametros)throws Exception{
		Class[]tipos=new Class[]{origen.getClass(),String.class,Object[].class};
		Method metodo=oyente.getClass().getMethod(nombreMetodo,tipos);
		metodo.invoke(oyente,new Object[]{origen,accion,parametros});
	}
}