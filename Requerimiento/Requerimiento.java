import java.util.*;
import f.*;
public class Requerimiento extends Actor{
	public int id;
	public String nombre;
	public String descripcion;
	public Plan plan;
	private HashMap<String,Dato>datos;
	public Requerimiento(int id,String nombre)throws Exception{
		if(nombre.trim().equals("")){
			throw new Exception("Nombre de requerimiento no válido");
		}
		this.id=id;
		this.nombre=nombre;
		this.datos=new HashMap<String,Dato>();
		Util.log("Nuevo requerimiento:"+this.nombre);
		plan=new Plan(this);
	}
	public String toString(){
		return this.nombre+":"+this.hashCode();
	}
	public String mostrarHTML()throws Exception{
		String resultado=Util.leer("HTML//requerimiento.html");
		resultado=resultado.replace("xnombrex",this.nombre);
		String codigoActividades="";
		ArrayList<Actividad>listaActividades=this.plan.listaActividades(1);
		for(int i=0;i<listaActividades.size();i++){
			codigoActividades+=listaActividades.get(i).mostrarHTML();
		}
		resultado=resultado.replace("xlistaActividadesx",codigoActividades);
		ArrayList<Dato>listaDatos=listaDatos();
		String codigoDatos="";
		for(int i=0;i<listaDatos.size();i++){
			codigoDatos+=listaDatos.get(i).mostrarHTML();
		}
		resultado=resultado.replace("xlistaDatosx",codigoDatos);
		return resultado;
	}
	public void agregarDato(Dato dato){
		if(Control.solicitar(this,"agregarDato",new Object[]{dato})){
			datos.put(dato.nombre,dato);
			Control.realizada(this,"agregarDato",new Object[]{dato});
		}
	}
	public ArrayList<Dato>listaDatos(){
		ArrayList<Dato>resultado=new ArrayList<Dato>();
		Iterator<Map.Entry<String,Dato>>iterador=datos.entrySet().iterator();
		while(iterador.hasNext()){
			Map.Entry<String,Dato>entrada=iterador.next();
			resultado.add(entrada.getValue());
		}
		return resultado;
	}
}