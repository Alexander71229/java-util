import java.util.*;
import java.io.*;
import f.*;
public class Maestro extends Actor{
	private HashMap<String,Requerimiento>hashRequerimientos;
	private HashMap<Integer,Requerimiento>idRequerimientos;
	private int maximo;
	private long tiempoCambio;
	public Maestro(){
		hashRequerimientos=new HashMap<String,Requerimiento>();
		idRequerimientos=new HashMap<Integer,Requerimiento>();
		registrarRealizada("Actor.actualizar","detectarCambios");
		registrarRealizada("Actor.crearRequerimiento","detectarCambios");
	}
	public ArrayList<Requerimiento>listaRequerimientos(){
		ArrayList<Requerimiento>resultado=new ArrayList<Requerimiento>();
		Iterator<Map.Entry<String,Requerimiento>>iterador=hashRequerimientos.entrySet().iterator();
		while(iterador.hasNext()){
			Map.Entry<String,Requerimiento>entrada=iterador.next();
			resultado.add(entrada.getValue());
		}
		return resultado;
	}
	public Requerimiento obtenerRequerimiento(String nombre){
		return hashRequerimientos.get(nombre);
	}
	public Requerimiento obtenerRequerimiento(int id){
		return idRequerimientos.get(id);
	}
	public void agregarRequerimiento(Requerimiento requerimiento){
		//if(hashRequerimientos.get(requerimiento.nombre)==null){
		{
			hashRequerimientos.put(requerimiento.nombre,requerimiento);
			idRequerimientos.put(requerimiento.id,requerimiento);
			if(requerimiento.id!=maximo){
				Util.log("El id del requerimiento no es igual al máximo");
				System.exit(0);
			}
			maximo++;
		}
	}
	public void cargar()throws Exception{
		cargarRequerimientos();
		cargarDatos();
	}
	private void cargarRequerimientos()throws Exception{
		if(Control.solicitar(this,"cargarRequerimientos",null)){
			Scanner s=new Scanner(new File("Asignaciones.txt"));
			while(s.hasNextLine()){
				String nombreRequerimiento=s.nextLine();
				if(nombreRequerimiento.equals("")){
					continue;
				}
				Requerimiento requerimiento=new Requerimiento(maximo,nombreRequerimiento);
				requerimiento.plan.cargar();
				requerimiento.plan.cargarDatosActividades();
				while(s.hasNextInt()){
					int tipo=s.nextInt();
					Actividad actividad=requerimiento.plan.obtenerActividad(tipo);
					actividad.setEstado(s.nextInt());
				}
				agregarRequerimiento(requerimiento);
			}
			Control.realizada(this,"cargarRequerimientos",null);
		}
	}
	public void cargarDatos()throws Exception{
		if(Control.solicitar(this,"cargarDatos",null)){
			Scanner s=new Scanner(new File("DatosRequerimientos.txt"),"UTF-8");
			while(s.hasNext()){
				String nombreRequerimiento=s.next();
				Requerimiento requerimiento=obtenerRequerimiento(nombreRequerimiento);
				while(s.hasNext()){
					String token=s.next();
					if(token.equals("%")){
						break;
					}
					String nombre=token;
					while(s.hasNext()){
						token=s.next();
						if(token.equals(":")){
							break;
						}
						nombre+=" "+token;
					}
					String valor="";
					while(s.hasNext()){
						token=s.next();
						if(token.equals("$")){
							break;
						}
						valor+=" "+token;
					}
					Dato dato=new Dato(nombre,valor);
					if(requerimiento!=null){
						requerimiento.agregarDato(dato);
					}
				}
			}
			Control.realizada(this,"cargarDatos",null);
		}
	}
	public void guardar()throws Exception{
		if(Control.solicitar(this,"guardar",null)){
			Util.respaldar("Asignaciones","txt");
			Util.respaldar("DatosRequerimientos","txt");
			PrintStream ps=new PrintStream(new FileOutputStream(new File("Asignaciones.txt"),false));
			PrintStream ps2=new PrintStream(new FileOutputStream(new File("DatosRequerimientos.txt"),false));
			ArrayList<Requerimiento>listaRequerimientos=listaRequerimientos();
			for(int i=0;i<listaRequerimientos.size();i++){
				Requerimiento requerimiento=listaRequerimientos.get(i);
Util.log("Guadardando:"+requerimiento.nombre);
				ps.println(requerimiento.nombre);
				ArrayList<Actividad>listaActividades=requerimiento.plan.listaActividades();
Util.log("Cantidad de actividades:"+listaActividades.size());
				for(int j=0;j<listaActividades.size();j++){
					Actividad actividad=listaActividades.get(j);
					ps.println(actividad.tipo+" "+actividad.getEstado());
				}
				ps2.println(requerimiento.nombre);
				ArrayList<Dato>listaDatos=requerimiento.listaDatos();
				for(int j=0;j<listaDatos.size();j++){
					Dato dato=listaDatos.get(j);
					ps2.println(dato.nombre+" : "+dato.valor+" $");
				}
				ps2.println("%");
			}
			Control.realizada(this,"guardar",null);
		}
	}
	public void detectarCambios(Actor origen,String accion,Object[]parametros)throws Exception{
		guardar();
	}
	public void actualizar(){
		if(Control.solicitar(this,"actualizar",null)){
			ArrayList<Requerimiento>listaRequerimientos=listaRequerimientos();
			for(int i=0;i<listaRequerimientos.size();i++){
				listaRequerimientos.get(i).plan.actualizar();
			}
			Control.realizada(this,"actualizar",null);
		}
	}
	public String mostrarHTML()throws Exception{
		String resultado="";
		String codigoListaRequerimientos="";
		ArrayList<Requerimiento>listaRequerimientos=listaRequerimientos();
		for(int i=0;i<listaRequerimientos.size();i++){
			codigoListaRequerimientos+=listaRequerimientos.get(i).mostrarHTML();
		}
		resultado=Util.leer("HTML//maestro.html");
		resultado=resultado.replace("xlistaRequerimientosx",codigoListaRequerimientos);
		return resultado;
	}
	public Actividad actividad(int irequerimiento,int tipo)throws Exception{
		return obtenerRequerimiento(irequerimiento).plan.obtenerActividad(tipo);
	}
	public void crearRequerimiento(String nombre)throws Exception{
		if(Control.solicitar(this,"crearRequerimiento",new Object[]{nombre})){
			String nombreRequerimiento=nombre;
			Requerimiento requerimiento=new Requerimiento(maximo,nombreRequerimiento);
			//requerimiento.plan.cargar();
			agregarRequerimiento(requerimiento);
			//requerimiento.plan.actualizar();
			guardar();
			cargar();
			actualizar();
			Control.realizada(this,"crearRequerimiento",new Object[]{nombre});
		}
	}
	public static void main(String[]args){
		try{
			Maestro maestro=new Maestro();
			maestro.cargarRequerimientos();
			maestro.actualizar();
			maestro.guardar();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}