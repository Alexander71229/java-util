document.ajaxSubmit=ajaxSubmit;
function agregarAsignacion(){
	try{
		var nombreAsignacion=document.getElementById('nombreAsignacion').value;
		document.action="/&agregarAsignacion="+nombreAsignacion+"&";
		document.ajaxSubmit();
	}catch(e){
		alert(e.stack);
	}
}
function completarTarea(elemento,requerimiento,tipo){
	try{
		var tabla=buscarContenedor(elemento,{tipo:"listaActividades"});
		document.action="/&completarTarea="+requerimiento+","+tipo;
		var maestro=buscarContenedor(elemento,{tipo:"maestroActividad"});
		var datos=tabla.rows[maestro.rowIndex+1];
		var nombres=obtenerElementosFiltro(datos,{tipo:"nombreDatoActividad"});
		var valores=obtenerElementosFiltro(datos,{tipo:"valorDatoActividad"});
		for(var i=0;i<nombres.length;i++){
			var nombre=nombres[i].innerHTML.trim();
			var valor=valores[i].value.trim();
			document.action+=","+nombre+","+valor;
		}
		document.action+="&";
		document.ajaxSubmit();
	}catch(e){
		alert(e.stack);
	}
}
function refrescar(){
	try{
		document.action="/&refrescar=1&";
		document.ajaxSubmit();
	}catch(e){
		alert(e.stack);
	}
}
function recargar(){
	try{
		document.action="/&recargar=1&";
		document.ajaxSubmit();
	}catch(e){
		alert(e.stack);
	}
}