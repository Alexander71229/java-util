	//------------------------------------------------//
	//Alexander Molina Grisales 20100928
	//------------------------------------------------//

	//---------------------------------------//
	//Petici�n por medio de XMLHttpRequest.  //
	//---------------------------------------//

		function solicitud(metodo,url,asin,antes,despues){
			asin=asin||true;
			metodo=metodo||'GET';
			if(url.indexOf('?')>=0){
				url=url+'&rnd='+Math.random();
			}
			else{
				url=url+'?rnd='+Math.random();
			}
			if(window.XMLHttpRequest){
				var req=new XMLHttpRequest();
			}else{
				var req=new ActiveXObject("Msxml2.XMLHTTP");
			}
			if(asin){
				req.onreadystatechange=function(){
					if(this.readyState==4||this.readyState=="complete"){
						var destino=this.responseText;
						if(typeof despues=='function')despues(destino,this);
						return;
					}
				}
			}
			if(typeof antes=='function')antes();
			req.open(metodo,url,asin);
			req.send(null);
			if(asin)return req;
			if(typeof despues=='function')despues();
			return req.responseText;
		}

	/***************************************
	Alexander Molina: 20100709
	Serializar objeto de javascript.
	***************************************/
		function serializar(elemento,parametro,nombre){
			if(nombre){
				nombre=nombre+".";
			}else{
				nombre="";
			}
			if(!parametro){
				parametro="";
			}else{
				if(parametro.indexOf("=")==-1){
					parametro="";
				}
			}
			if(elemento){
				esto=elemento;
			}else{
				esto=this;
			}
			var res="";
			var sep="";
			var ins=[];
			ins[0]=esto.getElementsByTagName('input');
			ins[1]=esto.getElementsByTagName('select');
			ins[2]=esto.getElementsByTagName('textarea');
			for (var i=0;i<ins.length;i++) {
				for (var j=0;j<ins[i].length;j++){
					if(ins[i][j].name&&!ins[i][j].disabled){
						var tipo=ins[i][j].type.toLowerCase();
						if(tipo!='checkbox'&&tipo!='submit'&&tipo!='button'&&tipo!='file'){
							res+=sep+nombre+ins[i][j].name+'='+ins[i][j].value;
							sep='&';
						}
						if(tipo=='checkbox'&&ins[i][j].checked){
							res+=sep+nombre+ins[i][j].name+'='+ins[i][j].value;
							sep='&';
						}
					}
				}
			}
			return res+parametro;
		}

	/***************************************
	Alexander Molina: 20100701
	Funci�n para darle valores autom�ticamente 
	a las entradas que se encuentran dentro de
	un elemento HTML dado(destino) a partir de
	un objeto de javascript(Origen).
	Adicionalmente si a continuaci�n de un
	input tipo hidden se encuentra un span que
	solo contiene un texto, el valor del hidden
	es mostrado en ese span.
	Tambi�n est� la opci�n de activar el onchange
	de un objeto al que se le ha cambiado el valor
	***************************************/
		function establecerElemento(nombre,origen,destino,activarEvento){
			var ins=[];
			var valor=null;
			var valido=false;
			var avance={};
			var oncs=[];
			var objeto=false;
			try{
				eval(nombre+"=origen;");
				objeto=true;
			}catch(e){
				;
			}
			ins[0]=destino.getElementsByTagName('input');
			ins[1]=destino.getElementsByTagName('select');
			ins[2]=destino.getElementsByTagName('textarea');
			for(var i=0;i<ins.length;i++){
				for(var j=0;j<ins[i].length;j++){
					if(ins[i][j].name){
						valido=false;
						try{
							if(objeto){
								eval("valor="+ins[i][j].name);
								valido=true;
							}
						}catch(e){
							valido=false;
						}
						if(!valido||valor==undefined){
							try{
								valor=origen[ins[i][j].name];
								valido=true;
							}catch(e){
								valido=false;
							}
						}
						if(!valido||valor==undefined){
							try{
								eval("valor=origen."+ins[i][j].name);
								valido=true;
							}catch(e){
								valido=false;
							}
						}
						if(valido&&valor==undefined){
							valido=false;
						}
						if(valido){
							var tipo=ins[i][j].type.toLowerCase();
							if(tipo!='checkbox'&&tipo!='submit'&&tipo!='button'&&tipo!='file'&&tipo!='radio'){
								try{
									if(valor instanceof Array){
										if(avance[ins[i][j].name]){
											valor=valor[avance[ins[i][j].name]];
											avance[ins[i][j].name]++;
										}else{
											valor=valor[0];
											avance[ins[i][j].name]=1;
										}
									}
								}catch(e){
								}
								var valorAnterior=ins[i][j].value;
								ins[i][j].value=valor+"";
								try{
									if(tipo=='select-one'&&(ins[i][j].selectedIndex==-1||ins[i][j].value!=valor)){
										ins[i][j].options[0].selected=true;
									}
								}catch(e){
								}
								if(ins[i][j].value!=valorAnterior&&activarEvento){
									oncs.push(ins[i][j]);
								}
								try{
									if(ins[i][j].type.toLowerCase()=='hidden'&&ins[i][j].nextSibling.tagName.toLowerCase()=='span'){
										var span=ins[i][j].nextSibling;
										if(span.childNodes.length<=1&&(!span.firstChild||!span.firstChild.tagName)){
											span.innerHTML=ins[i][j].value;
										}
									}
								}catch(e){
								}
							}
							if(tipo=='checkbox'||tipo=='radio'){
								ins[i][j].checked=false;
								try{
									if(!(valor instanceof Array)){
										valor=[valor+""];
									}
									for(var k=0;k<valor.length;k++){
										if(valor[k]==ins[i][j].value){
											if(!ins[i][j].checked){
												oncs.push(ins[i][j].onchange);
											}
											ins[i][j].checked=true;
											break;
										}
									}
								}catch(e){
								}
							}
						}
					}
				}
			}
			for(var i=0;i<oncs.length;i++){
				try{
					oncs[i].onchange();
				}catch(e){
					;
				}
			}
		}

	/***************************************
	Alexander Molina: 20100701
	Retorna un objeto de javascript creado a
	partir de un objeto HTML dado (origen) de
	acuerdo a los valores y nombres de las
	entradas que est�n dentro del mismo.
	***************************************/
		function crearObjeto(origen,raiz){
			try{
				var res={};
				var ins=[];
				raiz=raiz||"";
				if(raiz)raiz=raiz+".";
				ins[0]=origen.getElementsByTagName('input');
				ins[1]=origen.getElementsByTagName('select');
				ins[2]=origen.getElementsByTagName('textarea');
				for (var i=0;i<ins.length;i++) {
					for (var j=0;j<ins[i].length;j++) {
						if(ins[i][j].name&&!ins[i][j].disabled){
							if(!res[raiz+ins[i][j].name+""]){
								res[raiz+ins[i][j].name+""]=[];
							}
							var tipo=ins[i][j].type.toLowerCase();
							if(tipo!='checkbox'&&tipo!='submit'&&tipo!='button'&&tipo!='file'){
								res[raiz+ins[i][j].name+""].push(ins[i][j].value);
							}
							if(tipo=='checkbox'&&ins[i][j].checked){
								res[raiz+ins[i][j].name+""].push(ins[i][j].value);
							}
						}
					}
				}
			}catch(e){
			}
			return res;
		}

	/***************************************
	Alexander Molina: 20100708
	Deja la opci�n indicada por index o en
	su defecto la primera.
	***************************************/
	function dejarUnaOpcion(idSelect,index){
		try{
			var select=document.getElementById(idSelect);
			select=select||idSelect;
			index=index||0;
			var option=select.options[index];
			while(select.length>1){
				if(select.options[0]==option){
					select.remove(1);
				}else{
					select.remove(0);
				}
			}
			option.selected=true;
		}catch(e){
		}
	}
	/***************************************
	Alexander Molina: 20100916
	Obtener un objeto con el c�digo de un objeto JSON
	y con una respuesta en texto
	***************************************/
		function obtenerCodigoJSON(respuesta,cadenaInicial,cadenaFinal){
			cadenaInicial=cadenaInicial||"InicioObjetoJSON---";
			cadenaFinal=cadenaFinal||"---FinalObjetoJSON";
			respuesta=respuesta||"";
			var objetoRespuesta={};
			if(respuesta.indexOf(cadenaInicial)>=0&&respuesta.indexOf(cadenaFinal)>=0){
				var sp=respuesta.split(cadenaInicial);
				sp=sp[1].split(cadenaFinal);
				objetoRespuesta.objetoJSON=obtenerObjetoJSON(sp[0]);
				objetoRespuesta.textoRespuesta=sp[1];
			}else{
				objetoRespuesta.objetoJSON=obtenerObjetoJSON(respuesta);
				objetoRespuesta.textoRespuesta=respuesta;
			}
			return objetoRespuesta;
		}
	/***************************************
	Alexander Molina: 20100910
	De un codigo fuente obtener un objeto.
	***************************************/
		function obtenerObjetoJSON(codigo){
			var obj=null;
			try{
				obj=eval("("+codigo+")");
			}catch(e){
				//alert(e+":"+respuesta);
			}
			return obj;
		}
	/***************************************
	Alexander Molina: 20100910
	Retorna un texto con el c�digo HTML para objeto error.
	***************************************/
		function obtenerCodigoHTMLErrores(objetoJSON){
			var res="";
			try{
				if(objetoJSON.tipoClase=="AjaxErrores"&&objetoJSON.errores.length>=0){
					for(var i=0;i<objetoJSON.errores.length;i++){
						res+="<li>"+objetoJSON.errores[i].mensaje;
					}
				}
			}catch(e){}
			return res;
		}
	/***************************************
	Alexander Molina: 20100913
	Obtiene las entradas que se encuentran dentro de un elemento de acuerdo al filtro dado.
	***************************************/
		function obtenerEntradas(elemento,filtro1,filtro2){
			var res=[];
			try{
				elemento=document.getElementById(elemento)||elemento;
				elemento=elemento||[document.body];
				if(!(elemento instanceof Array)){
					elemento=[elemento];
				}
				filtro1=filtro1||[];
				filtro2=filtro2||[];
				if(!(filtro1 instanceof Array)){
					filtro1=[filtro1];
				}
				if(!(filtro2 instanceof Array)){
					filtro2=[filtro2];
				}
				var ins=[];
				for(var ils=0;ils<elemento.length;ils++){
					ins.push(elemento[ils].getElementsByTagName('input'));
					ins.push(elemento[ils].getElementsByTagName('select'));
					ins.push(elemento[ils].getElementsByTagName('textarea'));
				}
				for(var i=0;i<ins.length;i++){
					for(var j=0;j<ins[i].length;j++){
						var seleccionado=false;
						for(var fil=0;fil<filtro2.length&&!seleccionado;fil++){
							var sol=0;
							var col=0;
							for(var cm in filtro2[fil]){
								sol++;
								if((filtro2[fil][cm]==ins[i][j].getAttribute(cm)&&filtro2[fil][cm]!=undefined)||(filtro2[fil][cm]==undefined&&ins[i][j].getAttribute(cm)!=undefined)||(filtro2[fil][cm]==ins[i][j][cm]&&filtro2[fil][cm]!=undefined)){
									col++;
								}
							}
							if(col==sol){
								seleccionado=true;
							}
						}
						for(var fil=0;fil<filtro1.length&&!seleccionado;fil++){
							var sol=0;
							var col=0;
							for(var cm in filtro1[fil]){
								sol++;
								if((filtro1[fil][cm]==ins[i][j].getAttribute(cm)&&filtro1[fil][cm]!=undefined)||(filtro1[fil][cm]==undefined&&ins[i][j].getAttribute(cm)!=undefined)||(filtro1[fil][cm]==ins[i][j][cm]&&filtro1[fil][cm]!=undefined)){
									col++;
								}
							}
							if(col==sol){
								res.push(ins[i][j]);
								seleccionado=true;
							}
						}
					}
				}
			}catch(e){}
			return res;
		}
	/***************************************
	Alexander Molina: 20110307
	Obtiene elementos HTML a partir de un array elementos padre y en concordancia con un un array de filtros.
	elemento: elemento o array de Elementos a partir del cual se realiza la b�squeda.
	filtro1: Condiciones que el objeto debe cumplir.
	filtro2: Condiciones excluyentes.
	***************************************/
		function obtenerElementosFiltro(elemento,filtro1,filtro2,tagName){
			var res=[];
			var tagName=tagName||"*";
			elemento=document.getElementById(elemento)||elemento;
			elemento=elemento||[document.body];
			if(!(elemento instanceof Array)){
				elemento=[elemento];
			}
			filtro1=filtro1||[];
			filtro2=filtro2||[];
			if(!(filtro1 instanceof Array)){
				filtro1=[filtro1];
			}
			if(!(filtro2 instanceof Array)){
				filtro2=[filtro2];
			}
			for(var iel=0;iel<elemento.length;iel++){
				var els=elemento[iel].getElementsByTagName(tagName);
				for(var ils=0;ils<els.length;ils++){
					var seleccionado=false;
					for(var fil=0;fil<filtro2.length&&!seleccionado;fil++){
						var sol=0;
						var col=0;
						for(var cm in filtro2[fil]){
							sol++;
							if((filtro2[fil][cm]==els[ils].getAttribute(cm)&&filtro2[fil][cm]!=undefined)||(filtro2[fil][cm]==undefined&&els[ils].getAttribute(cm)!=undefined)||(filtro2[fil][cm]==els[ils][cm]&&filtro2[fil][cm]!=undefined)){
								col++;
							}
						}
						if(col==sol){
							seleccionado=true;
						}
					}
					for(var fil=0;fil<filtro1.length&&!seleccionado;fil++){
						var sol=0;
						var col=0;
						for(var cm in filtro1[fil]){
							sol++;
							if((filtro1[fil][cm]==els[ils].getAttribute(cm)&&filtro1[fil][cm]!=undefined)||(filtro1[fil][cm]==undefined&&els[ils].getAttribute(cm)!=undefined)||(filtro1[fil][cm]==els[ils][cm]&&filtro1[fil][cm]!=undefined)){
								col++;
							}
						}
						if(col==sol){
							res.push(els[ils]);
							seleccionado=true;
						}
					}
				}
			}
			return res;
		}
	/***************************************
	Alexander Molina: 20100913
	Insertar un elemento de HTML despu�s de otro.
	elemento1: Es el elemento despu�s del cual se desea hacer la inserci�n.
	elemento2: Elemento a insertar.
	***************************************/
		function insertarDespues(elemento2,elemento1){
			try{
			elemento1=document.getElementById(elemento1)||elemento1;
			elemento2=document.getElementById(elemento2)||elemento2;
				if(elemento1.parentNode){
					if(elemento1.nextSibling){
						elemento1.parentNode.insertBefore(elemento2,elemento1.nextSibling);
					}else{
						elemento1.parentNode.appendChild(elemento2)
					}
				}
			}catch(e){}
		}
	/***************************************
	Alexander Molina: 20100927
	Activa el modo de edici�n para un campo cuyos valores provienen de un select.
	***************************************/
		function activarLovs(elemento,visible,soloLectura,deshabilitado,noValidar){
			try{
				deshabilitado=deshabilitado||false;
				soloLectura=soloLectura||false;
				noValidar=noValidar||false;
				if(visible==undefined)visible=true;
				var res=obtenerEntradas(elemento,{lov:undefined});
				for(var i=0;i<res.length;i++){
					if(!res[i].lovx){
						if(visible)res[i].style.display="none";
						try{
							if(res[i].nextSibling&&res[i].nextSibling.getAttribute&&res[i].nextSibling.getAttribute('tipo')=='etiqueta'){
								res[i].etiqueta=res[i].nextSibling;
							}
						}catch(e){}
						if(!res[i].etiqueta){
							res[i].etiqueta=document.createElement('span');
							insertarDespues(res[i].etiqueta,res[i]);
						}
						try{
							if(visible)res[i].etiqueta.style.display='none';
						}catch(e){}
						try{
							var selo=document.getElementById(res[i].getAttribute('lov'));
							var selc=selo.cloneNode(true);
							selc.id="";
							selc.style.display="none";
							selc.value=selo.value;
							selc.setDisabled=function(deshabilitado){
								this.disabled=deshabilitado;
							}
							selc.setValue=function(valor){
								var avalor=this.value;
								this.value=valor;
								if(this.selectedIndex==-1){
									this.value=avalor;
								}
								this.newValue=this.value;
							}
							if(noValidar){
								var estaEnLasOpciones=false;
								for(var iop=0;iop<selc.options.length;iop++){
									if(res[i].value==selc.options[iop].value){
										estaEnLasOpciones=true;
										break;
									}
								}
								if(!estaEnLasOpciones){
									var nuevaOpcion=new Option(res[i].etiqueta.innerHTML,res[i].value);
									selc.options.add(nuevaOpcion);
								}
							}
							if(res[i].getAttribute('tipo')=='radio'){
								selc.aRadio=aRadio;
								selc=selc.aRadio();
							}
							selc.setValue(res[i].value);
							selc.destino=res[i];
							selc.alcambiar=selc.onchange;
							selc.onchange=function(){
								selc.haSidoCambiado=true;
								this.text=this.options[this.selectedIndex].text;
								if(!this.readOnly){
									try{
										if(this.actualizarSiempre)this.actualizar();
									}catch(e){}
									try{
										this.alcambiar();
									}catch(e){}
								}else{
									if(this.newValue){
										this.value=this.newValue;
									}
								}
							}
							selc.actualizar=function(){
								this.destino.value=this.value;
								this.destino.etiqueta.innerHTML=this.options[this.selectedIndex].text;
								this.text=this.options[this.selectedIndex].text;
								this.newValue=this.value;
							}
							insertarDespues(selc,res[i]);
							selc.setValue(selc.value);
							selc.readOnly=soloLectura;
							res[i].lovx=selc;
							try{
								eval("selc.onactivarlov=function(){"+selc.getAttribute('onactivarlov')+"};");
								selc.onactivarlov();
							}catch(e){}
							selc.setDisabled(deshabilitado);
							selc.haSidoCambiado=false;
							if(visible)selc.style.display="";
						}catch(e){}
					}else{
						var selc=res[i].lovx;
						selc.setValue(res[i].value);
						selc.disabled=deshabilitado;
						for(var j=0;j<selc.options.length;j++){
							selc.options[j].disabled=selc.disabled;
						}
						res[i].lovx.readOnly=soloLectura;
						if(visible)res[i].style.display="none";
						if(visible)res[i].etiqueta.style.display="none";
						res[i].lovx.onactivarlov();
						if(visible)res[i].lovx.style.display="";
						try{
							if(res[i].lovx.focus)res[i].lovx.focus();
						}catch(e){}
					}
				}
			}catch(e){}
		}
	/***************************************
	Alexander Molina: 20100913
	Desactiva el modo de edici�n para un campo cuyos valores provienen de un select.
	***************************************/
		function desactivarLovs(elemento){
			try{
				var res=obtenerEntradas(elemento,{lov:undefined});
				for(var i=0;i<res.length;i++){
					if(res[i].lovx){
						if(res[i].getAttribute('lovVisible')=='true'){
							res[i].style.display="none";
							res[i].etiqueta.style.display="none";
							res[i].lovx.setValue(res[i].value);
							res[i].lovx.setDisabled(true);
							res[i].lovx.style.display="";
						}else{
							res[i].lovx.style.display="none";
							res[i].style.display="";
							res[i].etiqueta.style.display="";
						}
						try{
							eval("res[i].lovx.ondesactivarlov=function(){"+res[i].lovx.getAttribute('ondesactivarlov')+"};");
							res[i].lovx.ondesactivarlov();
						}catch(e){}
					}
				}
			}catch(e){}
		}
	/***************************************
	Alexander Molina: 20100915
	Actualizar el valor de los campos seg�n lo seleccionado en la lista de valores correspodiente.
	***************************************/
		function actualizarLovs(elemento){
			var res=obtenerEntradas(elemento,{lov:undefined});
			for(var i=0;i<res.length;i++){
				if(res[i].lovx){
					res[i].lovx.actualizar();
				}
			}
		}
	/***************************************
	Alexander Molina: 20110309
	Crea una tabla con una fila y varias columnas con radios a partir de un select
	***************************************/
		function aRadio(){
			var tabla=null;
			try{
				var tabla=document.createElement('table');
				this.tabla=tabla;
				tabla.select=this;
				tabla.select.disabled=false;
				tabla.select.style.display="none";
				tabla.style.display="none";
				tabla.options=[];
				var fila=tabla.insertRow(0);
				fila.style.display="none";
				var celda=fila.insertCell(0);
				celda.appendChild(this);
				var fila=tabla.insertRow(1);
				tabla.radios=[];
				tabla.onchange=this.onchange;
				tabla.onclick=this.onclick;
				tabla.onmousemove=this.onmousemove;
				tabla.onmouseout=this.onmouseout;
				tabla.iniciando=true;
				tabla.readOnly=this.readOnly;
				tabla.setValue=function(valor){
					for(var i=0;i<this.radios.length;i++){
						if(this.radios[i].value==valor){
							this.radios[i].onclick();
						}
					}
				}
				tabla.setDisabled=function(deshabilitado){
					this.disabled=deshabilitado;
					for(var j=0;j<this.options.length;j++){
						this.options[j].disabled=this.disabled;
					}
				}
				for(var i=0;i<this.options.length;i++){
					var celda=fila.insertCell(i);
					celda.innerHTML='<input type="radio" value="'+this.options[i].value+'"/>'+this.options[i].text;
					tabla.radios.push(celda.getElementsByTagName('input')[0]);
					tabla.options[i]=tabla.radios[i];
					tabla.radios[i].tabla=tabla;
					tabla.radios[i].text=this.options[i].text;
					tabla.radios[i].index=i;
					tabla.radios[i].onclick=function(){
						var haCambiado=false;
						for(var j=0;j<this.tabla.radios.length;j++){
							this.tabla.radios[j].checked=false;
						}
						if(!this.tabla.iniciando&&this.tabla.readOnly){
							this.tabla.radios[this.tabla.selectedIndex].checked=true;
							return;
						}
						this.checked=true;
						if(this.tabla.value!=this.value&&!this.tabla.iniciando){
							haCambiado=true;
						}
						this.tabla.value=this.value;
						try{
							this.tabla.select.value=this.value;
						}catch(e){}
						this.tabla.text=this.text;
						this.tabla.selectedIndex=this.index;
						if(haCambiado){
							try{
								if(this.tabla.onchange&&window.event)this.tabla.onchange();
							}catch(e){}
						}
					}
					if(this.options[i].selected){
						tabla.radios[i].onclick();
					}
				}
				tabla.iniciando=false;
			}catch(e){}
			return tabla;
		}
	/***************************************
	Alexander Molina: 20100924
	Para una entrada editable crear una forma de edici�n
	***************************************/
		function crearEditor(elemento,estilo,propiedades){
			try{
				elemento=document.getElementById(elemento)||elemento;
				if(!elemento.editor){
					var editor=document.createElement('input');
					try{
						if(estilo){
							editor.cssText=estilo;
						}
					}catch(e){}
					try{
						if(propiedades){
							for(var i in propiedades){
								editor.setAttribute(i,propiedades[i]);
								editor[i]=propiedades[i];
							}
						}
					}catch(e){}
					editor.actualizar=function(){
						try{
							this.elemento.value=this.value;
							this.elemento.etiqueta.innerHTML=this.value;
						}catch(e){}
					}
					editor.onchange=function(){
						try{
							if(this.actualizarSiempre)this.actualizar();
						}catch(e){}
					}
					if(elemento.getAttribute('tipo')=='calendario'){
						editor.readOnly=true;
					}
					editor.value=elemento.value;
					editor.style.display="none";
					insertarDespues(editor,elemento);
					elemento.editor=editor;
					editor.elemento=elemento;
				}
			}catch(e){}
		}
	/***************************************
	Alexander Molina: 20100927
	Mostrar etiquetas para elementos editables u ocultos.
	***************************************/
		function mostrarEtiquetas(elemento,actualizar,noValidar){
			if(actualizar==undefined){
				actualizar=true;
			}
			var els=obtenerEntradas(elemento,[{type:'hidden'},{editable:'true'}],[{editable:'false'},{lov:undefined}]);
			for(var i=0;i<els.length;i++){
				if(!els[i].etiqueta){
					var span=document.createElement('span');
					span.innerHTML=els[i].value;
					insertarDespues(span,els[i]);
					els[i].etiqueta=span;
					span.elemento=els[i];
				}
			}
			activarLovs(elemento,false,false,false,noValidar);
			if(actualizar)actualizarLovs(elemento);
			desactivarLovs(elemento);
		}
	/***************************************
	Alexander Molina: 20100924
	***************************************/
		function activarEdicion(elemento,mostrarEditor,estilo,propiedades){
			if(mostrarEditor==undefined){
				mostrarEditor=true;
			}
			var els=obtenerEntradas(elemento,[{type:'hidden'},{editable:'true'}],[{editable:'false'},{lov:undefined}]);
			for(var i=0;i<els.length;i++){
				if(!els[i].editor){
					crearEditor(els[i],estilo,propiedades);
				}
				if(els[i].editor&&mostrarEditor){
					try{
						els[i].editor.value=els[i].value;
						els[i].style.display="none";
						if(els[i].etiqueta)els[i].etiqueta.style.display="none";
						els[i].editor.style.display="";
					}catch(e){}
				}
			}
			var els=obtenerElementosFiltro(elemento,{tagName:'A',tipo:'calendario'});
			for(var i=0;i<els.length;i++){
				els[i].style.display='';
			}
		}
	/***************************************
	Alexander Molina: 20100915
	***************************************/
		function desactivarEdicion(elemento){
			var els=obtenerEntradas(elemento,[{type:'hidden'},{editable:'true'}],[{editable:'false'},{lov:undefined}]);
			for(var i=0;i<els.length;i++){
				if(els[i].editor){
					try{
						els[i].style.display="";
						if(els[i].etiqueta)els[i].etiqueta.style.display="";
						els[i].editor.style.display="none";
					}catch(e){}
				}
			}
		}
	/***************************************
	Alexander Molina: 20100915
	***************************************/
		function actualizarEdicion(elemento){
			var els=obtenerEntradas(elemento,[{type:'hidden'},{editable:'true'}],[{editable:'false'},{lov:undefined}]);
			for(var i=0;i<els.length;i++){
				if(els[i].editor){
					try{
						els[i].editor.actualizar();
					}catch(e){}
				}
			}
		}
	/***************************************
	Alexander Molina: 20100915
	***************************************/
		function aislarPlantilla(elemento,padre){
			try{
				elemento=document.getElementById(elemento)||elemento;
				if(!elemento)return;
				if(!(elemento instanceof Array)){
					elemento=[elemento];
				}
				padre=padre||elemento[0].offsetParent;
				if(!padre)return;
				if(elemento.length==1){
					padre.plantilla=elemento[0];
				}else{
					padre.plantilla=elemento;
				}
				for(var i=0;i<elemento.length;i++){
					try{
						elemento[i].parentNode.removeChild(elemento[i]);
					}catch(e){}
				}
				return;
			}catch(e){}
		}
	/***************************************
	Alexander Molina: 20100915
	***************************************/
		function iniciarBotones(elemento,tipos){
			var els=obtenerElementosFiltro(elemento,{tagName:'A',tipo:undefined});
			for(var i=0;i<els.length;i++){
				for(var j=0;j<tipos.length;j++){
					if(els[i].getAttribute('tipo')==tipos[j]){
						els[i].style.display="";
						break;
					}else{
						els[i].style.display="none";
					}
				}
			}
		}
	/***************************************
	Alexander Molina: 20100927
	Diferenciar las filas pares de las impares por medio del color de fondo.
	***************************************/
		function stripedTable(tabla,colorPar,colorImpar,filaInicio,respetarClase){
			tabla=document.getElementById(tabla)||tabla;
			if(filaInicio!=0){
				filaInicio=filaInicio||1;
			}
			if(!tabla)return;
			for(var i=filaInicio;i<tabla.rows.length;i++){
				if(!(respetarClase&&(tabla.rows[i].className||tabla.rows[i].style.backgroundColor))){
					if(i%2==0){
						tabla.rows[i].style.backgroundColor=colorPar;
					}else{
						tabla.rows[i].style.backgroundColor=colorImpar;
					}
				}
			}
		}
	/***************************************
	Alexander Molina: 20101105
	Crear una tabla HTML a partir de una tablar formada por medio de Array de Array.
	***************************************/
		function crearTabla(tabla,visible,c1,c2,ct,cb){
			if(visible==undefined){
				visible=true;
			}
			c1=c1||'#D7EBFF';
			c2=c2||'#CCCCCC';
			ct=ct||'#FCFFAC';
			cb=cb||'#FCFFAC';
			var t=document.createElement('table');
			t.datos=tabla;
			var tb=document.createElement('tbody');
			t.appendChild(tb);
			t.createTHead();
			t.style.backgroundColor=cb;
			for(var i=0;i<tabla.length;i++){
				if(i==0){
					var f=t.tHead.insertRow(i);
					f.style.backgroundColor=ct;
				}else{
					var f=t.tBodies[0].insertRow(i-1);
					if(i%2==0){
						f.style.backgroundColor=c1;
					}else{
						f.style.backgroundColor=c2;
					}
					f.onclick=function(){
						this.aonc=this.tabla.aonc;
						this.bonc=this.tabla.bonc;
						try{
							this.bonc();
						}catch(e){}
						try{
							for(var i=0;i<this.valor.length;i++){
								try{
									if(this.tabla.destino[i].value!=undefined){
										this.tabla.destino[i].value=this.valor[i];
									}
									if(this.tabla.destino[i].innerHTML!=undefined){
										this.tabla.destino[i].innerHTML='<a>'+this.valor[i]+'</a>';
									}
								}catch(e){}
							}
							this.aonc();
						}catch(e){}
					}
				}
				f.valor=tabla[i];
				f.tabla=t;
				for(var j=0;j<tabla[i].length;j++){
					var c=f.insertCell(j);
					c.innerHTML=tabla[i][j];
					c.valor=tabla[i][j];
					if(j==0){
						if(!visible){
							c.style.display='none';
						}
					}
				}
			}
			return t;
		}
	/***************************************
	Alexander Molina: 20110309
	Retorna verdadero si el elemento dado es un 'input' tipo 'text'
	***************************************/
		function esInputText(elemento){
			try{
				return (elemento.tagName.toLowerCase()=='input')&&(elemento.type.toLowerCase()=='text');
			}catch(e){
				return false;
			}
		}
	/***************************************
	Alexander Molina: 20110310
	Framework en desarrollo para la edici�n en l�nea.
	***************************************/
		function seleccionarTabla(tabla){
			try{
				tablaSeleccionada=tabla;
				elementoSeleccionado.tabla=tablaSeleccionada;
			}catch(e){
			}
		}
		function seleccionar(elemento){
			try{desactivarSeleccion(elementoSeleccionado);}catch(e){}
			elementoSeleccionado=elemento;
			try{activarSeleccion(elemento);}catch(e){}
		}
		function clickNuevo(tabla){
			nuevo(tabla);
			try{
				despuesNuevo(tabla,elementoSeleccionado);
			}catch(e){
			}
		}
		function nuevo(tabla){
			tabla=tabla||tablaSeleccionada;
			tabla=document.getElementById(tabla)||tabla;
			var tr=tabla.plantilla.cloneNode(true);
			tabla.tBodies[0].appendChild(tr);
			seleccionar(tr);
			iniciarBotones(tr,['Guardar','Eliminar']);
			tr.nuevo=true;
			editar(elementoSeleccionado);
			tr.style.display="";
		}
		function clickEditar(){
			var continuar=true;
			if(!elementoSeleccionado.esperando){
				try{
					continuar=antesEditar(elementoSeleccionado);
				}catch(e){}
				if(continuar==undefined||continuar){
					editar(elementoSeleccionado);
				}
			}
		}
		function editar(elemento){
			iniciarBotones(elemento,['Guardar','Cancelar']);
			actualizarValores(elemento);
			mostrarEditores(elemento);
		}
		function despuesGuardar(respuesta,elementoEdicion){
			var obj=obtenerObjetoJSON(respuesta);
			try{
				if(obj){
					var html=obtenerCodigoHTMLErrores(obj);
					try{
						mostrarHTML(html);
					}catch(e){}
          try{
            if(obj.tipoClase=="AjaxErrores"&&obj.exito==0){
              return false;
            }
          }catch(e){
          }
					var objeto=obj.objeto||obj;
					establecerElemento("",objeto,elementoEdicion,true);
					return true;
				}else{
					mostrarHTML(respuesta);
					return false;
				}
			}catch(e){return false}
		}
		function clickGuardar(){
			var continuar=true;
			if(!elementoSeleccionado.esperando){
				try{
					continuar=antesGuardar(elementoSeleccionado);
				}catch(e){}
				if(continuar==undefined||continuar){
					guardar(elementoSeleccionado);
				}
			}
		}
		function clickGuardarRapido(){
			var continuar=true;
			if(!elementoSeleccionado.esperando){
				try{
					continuar=antesGuardar(elementoSeleccionado);
				}catch(e){}
				if(continuar==undefined||continuar){
					guardarRapido(elementoSeleccionado);
				}
			}
		}
		function guardar(elemento){
			try{
				var elementoEdicion=elemento;
				if(elementoEdicion.nuevo){
					var comando="guardar";
				}else{
					var comando="editar";
				}
				var url=getUrl(comando,elementoEdicion);
				url=url+serializar(elementoEdicion,'',getBean(comando,elementoEdicion));
				function f(){
					try{
						antesPeticion(elementoEdicion,comando,url);
					}catch(e){}
				}
				try{
					var els=obtenerElementosFiltro(elemento,[{editable:'true'},{lov:undefined},{name:undefined}],null,"span");
					for(var i=0;i<els.length;i++){
						var e=els[i];
						if(e.s){
							bloquear(e.s);
						}
					}
				}catch(e){}
				elementoEdicion.esperando=true;
				solicitud('GET',url,true,f,function(respuesta){
					elementoEdicion.esperando=false;
					try{
						finPeticion(elementoEdicion,comando);
					}catch(e){}
					try{
						var exito=despuesGuardar(respuesta,elementoEdicion);
					}catch(e){
						var exito=false;
					}
					try{
						despuesOperacion(respuesta,exito,comando,elementoEdicion);
					}catch(e){}
					try{
						var els=obtenerElementosFiltro(elemento,[{editable:'true'},{lov:undefined},{name:undefined}],null,"span");
						for(var i=0;i<els.length;i++){
							var e=els[i];
							if(e.s){
								desBloquear(e.s);
							}
						}
					}catch(e){}
					if(exito){
						actualizarEtiquetas(elementoEdicion);
						ocultarEditores(elementoEdicion);
						iniciarBotones(elementoEdicion,['Editar','Eliminar']);
						elementoEdicion.nuevo=false;
					}
					try{
						finMetodo(respuesta,exito,comando,elementoEdicion);
					}catch(e){}
				});
			}catch(e){}
		}
		function clickEjecutarAccion(comando,funcion){
			var continuar=true;
			if(!elementoSeleccionado.esperando){
				try{
					continuar=antesEjecutarAccion(comando,elementoSeleccionado);
				}catch(e){}
				if(continuar==undefined||continuar){
					ejecutarAccion(comando,elementoSeleccionado,funcion);
				}
			}
		}
		function ejecutarAccion(comando,elemento,funcion){
			try{
				var elementoEdicion=elemento;
				var url=getUrl(comando,elementoEdicion);
				url=url+serializar(elementoEdicion,'',getBean(comando,elementoEdicion));
				function f(){
					try{
						antesPeticion(elementoEdicion,comando,url);
					}catch(e){}
				}
				try{
					var els=obtenerElementosFiltro(elemento,[{editable:'true'},{lov:undefined},{name:undefined}],null,"span");
					for(var i=0;i<els.length;i++){
						var e=els[i];
						if(e.s){
							bloquear(e.s);
						}
					}
				}catch(e){}
				elementoEdicion.esperando=true;
				solicitud('GET',url,true,f,function(respuesta){
					elementoEdicion.esperando=false;
					try{
						finPeticion(elementoEdicion,comando);
					}catch(e){}
					try{
						var exito=funcion(respuesta,elementoEdicion);
					}catch(e){
						var exito=false;
					}
					try{
						despuesOperacion(respuesta,exito,comando,elementoEdicion);
					}catch(e){}
					try{
						var els=obtenerElementosFiltro(elemento,[{editable:'true'},{lov:undefined},{name:undefined}],null,"span");
						for(var i=0;i<els.length;i++){
							var e=els[i];
							if(e.s){
								desBloquear(e.s);
							}
						}
					}catch(e){}
					if(exito){
					}
					try{
						finMetodo(respuesta,exito,comando,elementoEdicion);
					}catch(e){}
				});
			}catch(e){}
		}
		function guardarRapido(elemento){
			try{
				var elementoEdicion=elemento;
				if(elementoEdicion.nuevo){
					var comando="guardar";
				}else{
					var comando="editar";
				}
				var respuesta="";
				var exito=true;
				try{
					exito=despuesGuardarRapido(respuesta,elementoEdicion);
				}catch(e){
					exito=true;
				}
				try{
					despuesOperacion(respuesta,exito,comando,elementoEdicion);
				}catch(e){}
				if(exito){
					actualizarEtiquetas(elementoEdicion);
					ocultarEditores(elementoEdicion);
					iniciarBotones(elementoEdicion,['Editar','Eliminar']);
					elementoEdicion.nuevo=false;
				}
				try{
					finMetodo(respuesta,exito,comando,elementoEdicion);
				}catch(e){}
			}catch(e){}
		}
		function clickEliminar(){
			var continuar=true;
			if(!elementoSeleccionado.esperando){
				try{
					continuar=antesEliminar(elementoSeleccionado);
				}catch(e){}
				if(continuar==undefined||continuar){
					actualizarValores(elementoSeleccionado);
					eliminar(elementoSeleccionado);
				}
			}
		}
		function despuesEliminar(respuesta,elementoEdicion){
			var obj=obtenerObjetoJSON(respuesta);
			try{
				if(obj){
					var html=obtenerCodigoHTMLErrores(obj);
					try{
						mostrarHTML(html);
					}catch(e){}
          try{
            if(obj.tipoClase=="AjaxErrores"&&obj.exito==0){
              return false;
            }
          }catch(e){
          }
					if(obj.exito*1||obj.objeto||(obj.tipoClase!='AjaxErrores')){
						return true;
					}
					return false;
				}else{
					mostrarHTML(respuesta);
					return false;
				}
			}catch(e){return false}
		}
		function eliminar(elemento){
			if(elemento.nuevo){
				cancelar(elemento);
				return;
			}
			var elementoEdicion=elemento;
			var comando="eliminar";
			var url=getUrl(comando,elementoEdicion);
			url=url+serializar(elementoEdicion,'',getBean(comando,elementoEdicion));
			function f(){
				try{
					antesPeticion(elementoEdicion,comando,url);
				}catch(e){}
			}
			elementoEdicion.esperando=true;
			solicitud('GET',url,true,f,function(respuesta){
				elementoEdicion.esperando=false;
				try{
					finPeticion(elementoEdicion,comando);
				}catch(e){}
				try{
					var exito=despuesEliminar(respuesta,elementoEdicion);
				}catch(e){
					var exito=false;
				}
				try{
					despuesOperacion(respuesta,exito,comando,elementoEdicion);
				}catch(e){}
				if(exito){
					try{
						elemento.parentNode.removeChild(elemento);
					}catch(e){}
				}
				try{
					finMetodo(respuesta,exito,comando,elementoEdicion);
				}catch(e){}
			});
		}
		function clickCancelar(){
			var continuar=true;
			if(!elementoSeleccionado.esperando){
				try{
					continuar=antesCancelar(elementoSeleccionado);
				}catch(e){}
				if(continuar==undefined||continuar){
					cancelar(elementoSeleccionado);
				}
			}
		}
		function clickRemover(){
			var continuar=true;
			if(!elementoSeleccionado.esperando){
				try{
					continuar=antesRemover(elementoSeleccionado);
				}catch(e){}
				if(continuar==undefined||continuar){
					elementoSeleccionado.nuevo=true;
					clickCancelar();
				}
			}
		}
		function cancelar(elemento){
			try{
				if(!elemento.nuevo){
					ocultarEditores(elemento);
					iniciarBotones(elemento,['Editar','Eliminar']);
					actualizarValores(elemento);
				}else{
					elemento.parentNode.removeChild(elemento);
				}
			}catch(e){}
		}
		function ocultarEditores(elemento){
			elementosEnEdicion[elemento.momentoEdicion]=undefined;
			elemento.enEdicion=false;
			var els=obtenerElementosFiltro(elemento,[{verdadero:undefined,falso:undefined}],null,"input");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				e.disabled=true;
				try{
					if(e.s.value==e.getAttribute('verdadero')){
						e.checked=true;
					}else{
						e.checked=false;
					}
				}catch(e){
				}
			}
			var els=obtenerElementosFiltro(elemento,[{editable:'true'},{lov:undefined},{name:undefined}],null,"span");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(e.s){
					e.s.style.display="none";
				}
				e.style.display="";
			}
		}
		function mostrarEditores(elemento){
			elemento.momentoEdicion=elementosEnEdicionContador++;
			elementosEnEdicion[elemento.momentoEdicion]=elemento;
			elemento.enEdicion=true;
			var els=obtenerElementosFiltro(elemento,[{verdadero:undefined,falso:undefined}],null,"input");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(e.s){
					try{
						antesMostrarEdicion(e);
					}catch(e){}
					e.disabled=false;
					try{
						despuesMostrarEdicion(e);
					}catch(e){}
				}
			}
			var els=obtenerElementosFiltro(elemento,[{editable:'true'},{lov:undefined},{name:undefined}],null,"span");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(e.s){
					try{
						antesMostrarEdicion(e);
					}catch(e){}
					e.style.display="none";
					e.s.style.display="";
					try{
						despuesMostrarEdicion(e);
					}catch(e){}
				}
			}
		}
		function actualizarEtiquetas(elemento){
			var els=obtenerElementosFiltro(elemento,[{verdadero:undefined,falso:undefined}],null,"input");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(e.s){
					if(e.checked){
						e.s.value=e.getAttribute('verdadero');
					}else{
						e.s.value=e.getAttribute('falso');
					}
				}
			}
			var els=obtenerElementosFiltro(elemento,[{editable:'true'},{name:undefined}],[{lov:undefined}],"span");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(e.s){
					e.innerHTML=e.s.value;
				}
			}
			var els=obtenerElementosFiltro(elemento,[{lov:undefined}],null,"span");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(e.s){
					e.se.innerHTML=e.s.value;
					if(e.s.selectedIndex>=0){
						e.innerHTML=e.s.options[e.s.selectedIndex].text;
					}else{
						e.innerHTML="";
					}
				}
			}
		}
		function actualizarValores(elemento){
			var els=obtenerElementosFiltro(elemento,[{verdadero:undefined,falso:undefined}],null,"input");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(!e.s){
					var s=e.nextSibling;
					if(s){
						try{
							s.e=e;
						}catch(e){
						}
						e.s=s;
					}
				}
				if(e.s){
					if(e.checked){
						var valorAnterior=e.s.value;
						e.s.value=e.getAttribute('verdadero');
						if(valorAnterior!=e.s.value&&e.s.onchange){
							try{
								e.s.onchange();
							}catch(e){}
						}
					}else{
						try{
							e.s.value=e.getAttribute('falso');
						}catch(e){
						}
					}
				}
			}
			var els=obtenerElementosFiltro(elemento,[{editable:'true'}],null,"span");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(!e.s){
					var s=e.nextSibling;
					s.e=e;
					e.s=s;
				}
				if(e.s){
					var valorAnterior=e.s.value;
					e.s.value=e.innerHTML;
					if(valorAnterior!=e.s.value&&e.s.onchange()){
						try{
							e.s.onchange();
						}catch(e){}
					}
				}
			}
			var els=obtenerElementosFiltro(elemento,[{lov:undefined}],null,"span");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(!e.s){
					var n=e.getAttribute('name');
					var se=e.nextSibling;
					var s=e.getAttribute('lov');
					var tr=e.getAttribute('tipo');
					s=document.getElementById(s);
					if(s&&se){
						if(!n||!s.outerHTML){
							var ns=s.cloneNode(true);
							if(n){
								ns.name=n;
							}
						}else{
							var d=document.createElement('div');
							d.innerHTML=s.outerHTML.replace("name="+s.name,"name="+n);
							var ns=d.firstChild;
						}
						ns.id="";
						ns.disabled=false;
						if(tr=='radio'){
							ns.aRadio=aRadio;
							ns=ns.aRadio();
						}
						insertarDespues(ns,se);
						e.se=se;
						se.e=e;
						e.s=ns;
						ns.e=e;
					}
				}
				if(e.s){
					var valorAnterior=e.s.value;
					if(e.s.setValue){
						e.s.setValue(e.se.innerHTML);
					}else{
						e.s.value=e.se.innerHTML;
					}
					if(e.s.value!=valorAnterior&&e.s.onchange){
						try{
							e.s.onchange();
						}catch(e){}
					}
				}
			}
			var els=obtenerElementosFiltro(elemento,[{name:undefined}],[{editable:undefined},{lov:undefined}],"span");
			for(var i=0;i<els.length;i++){
				var e=els[i];
				if(!e.s){
					var d=document.createElement('div');
					d.appendChild(e.cloneNode(true));
					var s=d.innerHTML;
					s=(s.replace(/>.*/,"")).replace(/span/i,"input")+"/>";
					d.innerHTML=s;
					s=d.firstChild;
					s.e=e;
					e.s=s;
					e.s.style.display="none";
					insertarDespues(e.s,e);
				}
				if(e.s){
					var valorAnterior=e.s.value;
					e.s.value=e.innerHTML;
					if(e.s.value!=valorAnterior&&e.s.onchange){
						try{
							e.s.onchange();
						}catch(e){}
					}
				}
			}
		}
		function bloquear(elemento){
			elemento.disabled=true;
			try{
				if(elemento&&elemento.setDisabled){
					elemento.setDisabled(true);
				}
			}catch(e){}
		}
		function desBloquear(elemento){
			elemento.disabled=false;
			try{
				if(elemento&&elemento.setDisabled){
					elemento.setDisabled(false);
				}
			}catch(e){}
		}
	/***************************************
	Alexander Molina: 20110613
	Agregar la funci�n trim para las cadenas
	***************************************/
		function trim(s){
			s=s||this;
			if(!s){
				return "";
			}
			return s.replace(/^\s+/g,'').replace(/\s+$/g,'');
		}
		if(!String.prototype.trim){
			String.prototype.trim=trim;
		}
	/***************************************
	Alexander Molina: 20110609
	Retorna verdadero si la cadena s es una fecha v�lida de la forma YYYYMMDD,
	de lo contrario retornar false.
	***************************************/
		function validarFecha(s){
			try{
				s=s.trim();
				if(s.length!=8){
					return false;
				}
				y=s.substr(0,4)*1;
				m=s.substr(4,2)*1;
				d=s.substr(6,2)*1;
				var f=new Date(y,m-1,d);
				if(f.getFullYear()!=y){
					return false;
				}
				if(f.getMonth()!=m-1){
					return false;
				}
				if(f.getDate()!=d){
					return false;
				}
				return true;
			}catch(e){
				return false;
			}
		}
	/***************************************
	Alexander Molina: 20110922
	elemento: elemento a partir del cual se realiza la b�squeda.
	filtro1: Condiciones que el objeto debe cumplir.
	filtro2: Condiciones excluyentes.
	***************************************/
		function buscarContenedor(elemento,filtro1,filtro2){
			var res=[];
			elemento=document.getElementById(elemento)||elemento;
			elemento=elemento||document.body;
			filtro1=filtro1||[];
			filtro2=filtro2||[];
			if(!(filtro1 instanceof Array)){
				filtro1=[filtro1];
			}
			if(!(filtro2 instanceof Array)){
				filtro2=[filtro2];
			}
			var contenedor=elemento.parentNode;
			while(contenedor!=null){
				var seleccionado=false;
				for(var fil=0;fil<filtro2.length&&!seleccionado;fil++){
					var sol=0;
					var col=0;
					for(var cm in filtro2[fil]){
						sol++;
						if((filtro2[fil][cm]==contenedor.getAttribute(cm)&&filtro2[fil][cm]!=undefined)||(filtro2[fil][cm]==undefined&&contenedor.getAttribute(cm)!=undefined)||(filtro2[fil][cm]==contenedor[cm]&&filtro2[fil][cm]!=undefined)){
							col++;
						}
					}
					if(col==sol){
						seleccionado=true;
					}
				}
				for(var fil=0;fil<filtro1.length&&!seleccionado;fil++){
					var sol=0;
					var col=0;
					for(var cm in filtro1[fil]){
						sol++;
						if((filtro1[fil][cm]==contenedor.getAttribute(cm)&&filtro1[fil][cm]!=undefined)||(filtro1[fil][cm]==undefined&&contenedor.getAttribute(cm)!=undefined)||(filtro1[fil][cm]==contenedor[cm]&&filtro1[fil][cm]!=undefined)){
							col++;
						}
					}
					if(col==sol){
						return contenedor;
					}
				}
				contenedor=contenedor.parentNode;
			}
		}
		function tieneBody(respuesta){
			return respuesta.toLowerCase().indexOf("body")==1&&respuesta.toLowerCase().indexOf("body")-1!=respuesta.toLowerCase().indexOf("tbody");
		}
		function validarHora(s){
			try{
				s=s.trim();
				if(s.length!=4){
					return false;
				}
				var n=s*1;
				if(2459<n||n<0){
					return false;
				}
				if(n%100>59){
					return false;
				}
			}catch(e){
				return false;
			}
			return true;
		}
		function cancelarElementosEnEdicion(){
			try{
				for(i in elementosEnEdicion){
					if(elementosEnEdicion[i]){
						elementoSeleccionado=elementosEnEdicion[i];
						clickCancelar();
					}
				}
			}catch(e){
			}
		}
		//Esta funci�n debe retornar la url seg�n el comando y el elemento que est� editando. Los comandos son: "guardar","editar" y "eliminar"
		function getUrl(comando,elemento){
		}
		//Esta funci�n debe retornar una cadena con el nombre del bean o una cadena vac�a si el bean no es necesario.
		function getBean(comando,elemento){
		}
		//Eventos:
		function desactivarSeleccion(elemento){} //Se ejecuta cuando un elemento deja de estar seleccionado. "elemento" es el que deja de estar seleccionado.
		function activarSeleccion(elemento){} //Se ejecuta cuando un elemento pasa a estar seleccionado. "elemento" es el que pasa a estar seleccionado.
		function antesPeticion(elementoEdicion,comando,url){} //Se ejecuta antes de realizar una petici�n por ajax. "elementoEdicion" es sobre el que se est� haciendo la edici�n. "comando" es "guardar","editar" o "eliminar"
		function finPeticion(elementoEdicion,comando){} //Se ejecuta despu�s de terminar una petici�n por ajax. "elementoEdicion" es sobre el que se est� haciendo la edici�n. "comando" es "guardar","editar" o "eliminar"
		function antesGuardar(elementoSeleccionado){} //Se ejecuta antes de ejecutar el c�digo de guardar despu�s de dar clic. �til para validaciones.
		function antesEditar(elementoSeleccionado){} //Se ejecuta antes de ejecutar el c�digo de editar despu�s de dar clic. �til para validaciones.
		function antesEliminar(elementoSeleccionado){} //Se ejecuta antes de ejecutar el c�digo de eliminar despu�s de dar clic. �til para colocar mensajes de confirmaci�n.
		function antesRemover(elementoSeleccionado){} //Se ejecuta antes de ejecutar el c�digo de remover elemento. �til para colocar mensajes de confirmaci�n.
		function antesCancelar(elementoSeleccionado){} //Se ejecuta antes de ejecutar el c�digo de cancelar despu�s de dar clic. �til para colocar mensajes de confirmaci�n.
		function antesMostrarEdicion(elemento){} //Se ejecuta antes de que el editor de "elemento" est� visible. El tratamiento de los calendarios se puede hacer aqu�.
		function despuesMostrarEdicion(elemento){} //Se ejecuta despu�s de que el editor de "elemento" est� visible. El tratamiento de los calendarios se puede hacer aqu�.
		function despuesOperacion(respuesta,exito,comando,elementoEdicion){} //Despu�s de realizar la operaci�n, pero antes de desactivar los editores.
		function finMetodo(respuesta,exito,comando,elementoEdicion){} //Se ejecuta cuando el m�todo ha realizado todas las operaciones, despu�s de haber desactivado los editores
		function mostrarHTML(html){} //Tratar el c�digo HTML de la respuesta. El argumento tambi�n puede ser una respuesta que no se logra interpretar.
		elementosEnEdicion={};
		elementosEnEdicionContador=1;

	/***************************************
	Alexander Molina: 20120220
	Funci�n que reemplaza el submit por una petici�n ajax
	Ejemplo:
		algunFormularioForm.submit=ajaxSubmit;
	***************************************/
		function ajaxSubmit(){
			try{
				if(window.enPeticion){
					return;
				}
				var url=this.action+"?";
				url+=serializar(this);
				//enPeticion=true;
				try{
					mostrarCarga();
				}catch(e){
				}
				solicitud('GET',url,true,null,function(respuesta){
					try{
						enPeticion=false;
						document.body.innerHTML=respuesta;
						var els=document.body.getElementsByTagName('script');
						for(var i=0;i<els.length;i++){
							try{
								eval(els[i].innerHTML);
							}catch(e){
							}
						}
					}catch(e){
						alert(e.stack);
					}
				});
			}catch(e){
				alert(e.stack);
			}
		};
