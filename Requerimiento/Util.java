import java.io.*;
import java.util.*;
import f.*;
public class Util extends Actor{
	public static HashMap<String,Long>hash;
	public static void log(String mensaje){
		System.out.println(mensaje);
	}
	public static void log(Exception e){
	}
	public static String leer(File file)throws Exception{
		String resultado="";
		Scanner s=new Scanner(file);
		while(s.hasNext()){
			resultado+=s.nextLine()+"\n";
		}
		return resultado;
	}
	public static String leer(String nombre)throws Exception{
		return leer(new File(nombre));
	}
	public static void guardar(String nombre,String valor)throws Exception{
		PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre),false));
		ps.print(valor);
	}
	public static void respaldar(String nombre,String extension)throws Exception{
		/*String clave=nombre+"."+extension;
		String valor=leer(clave);
		if(valor.equals("")){
			return;
		}
		if(hash==null){
			hash=new HashMap<String,Long>();
		}
		if(hash.get(clave)!=null){
			long anterior=hash.get(clave);
			String nombreArchivo=nombre+anterior+"."+extension;
			String valorAnterior="";
			try{
				valorAnterior=leer(nombreArchivo);
			}catch(Exception e){
				Util.log("Respaldo error"+e);
			}
			if(valorAnterior.equals(valor)){
				return;
			}
		}
		long tiempo=(new Date()).getTime();
		String nombreArchivo=nombre+tiempo+"."+extension;
		guardar(nombreArchivo,valor);
		hash.put(clave,tiempo);*/
	}
	public Util(){
		/*registrarSolicitud("Maestro.cargarRequerimientos","logMaestro");
		registrarSolicitud("Maestro.cargarDatos","logMaestro");
		registrarSolicitud("Maestro.guardar","logMaestro");
		registrarSolicitud("Maestro.actualizar","logMaestro");
		registrarSolicitud("Maestro.crearRequerimiento","logMaestro");
		registrarSolicitud("Plan.cargar","logPlan");
		registrarSolicitud("Plan.cargarDatosActividades","logPlan");
		registrarRealizada("Maestro.cargarRequerimientos","logMaestroRealizada");
		registrarRealizada("Maestro.cargarDatos","logMaestroRealizada");
		registrarRealizada("Maestro.guardar","logMaestroRealizada");
		registrarRealizada("Maestro.actualizar","logMaestroRealizada");
		registrarRealizada("Maestro.crearRequerimiento","logMaestroRealizada");
		registrarRealizada("Plan.cargar","logPlanRealizada");
		registrarRealizada("Plan.cargarDatosActividades","logPlanRealizada");*/
	}
	public void logMaestro(Maestro maestro,String accion,Object[]parametros)throws Exception{
		Util.log("Maestro inicial:"+accion);
		mostrarEstado(maestro);
	}
	public void logPlan(Plan plan,String accion,Object[]parametros)throws Exception{
		Util.log("Plan inicial:"+accion+"-->"+plan);
	}
	public void logMaestroRealizada(Maestro maestro,String accion,Object[]parametros)throws Exception{
		Util.log("Maestro final:"+accion);
		mostrarEstado(maestro);
	}
	public void logPlanRealizada(Plan plan,String accion,Object[]parametros)throws Exception{
		Util.log("Plan final:"+accion+"-->"+plan);
	}
	public void mostrarEstado(Maestro maestro){
		ArrayList<Requerimiento>listaRequerimientos=maestro.listaRequerimientos();
		for(int i=0;i<listaRequerimientos.size();i++){
			Util.log(listaRequerimientos.get(i)+"-->"+listaRequerimientos.get(i).plan);
		}
	}
}