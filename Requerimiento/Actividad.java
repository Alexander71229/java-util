import java.util.*;
import f.*;
public class Actividad extends Actor{
	public Plan plan;
	public int tipo;
	public String descripcion;
	private int estado;
	public Requerimiento requerimiento;
	ArrayList<Actividad>dependencias;
	private ArrayList<String>listaDatos;
	public Actividad(Plan plan,int tipo){
		this.plan=plan;
		this.tipo=tipo;
		this.descripcion="";
		dependencias=new ArrayList<Actividad>();
		listaDatos=new ArrayList<String>();
	}
	public void agregarDependencia(Actividad actividad){
		if(actividad==null){
			Util.log("Actividad de dependencia nula");
		}
		dependencias.add(actividad);
	}
	public void setEstado(int estado){
		this.estado=estado;
	}
	public void completarTarea(HashMap<String,Dato>datos){
		if(Control.solicitar(this,"completarTarea",null)){
			for(int i=0;i<listaDatos.size();i++){
				if(datos.get(listaDatos.get(i))==null){
					return;
				}
				if(datos.get(listaDatos.get(i)).valor.trim().equals("")){
					return;
				}
			}
			cambiarEstado(2);
			Control.realizada(this,"completarTarea",null);
		}
	}
	public void completarTarea(ArrayList<Dato>datos){
		if(Control.solicitar(this,"completarTarea",null)){
			int existentes=0;
			for(int i=0;i<datos.size();i++){
				if(datos.get(i).valor.trim().equals("")){
					return;
				}
				plan.requerimiento.agregarDato(datos.get(i));
			}
			cambiarEstado(2);
			Control.realizada(this,"completarTarea",null);
		}
	}
	public void cambiarEstado(int estado){
		if(estado!=this.estado){
			if(Control.solicitar(this,"cambiarEstado",new Object[]{estado})){
				this.estado=estado;
				Control.realizada(this,"cambiarEstado",new Object[]{estado});
			}
		}
	}
	public int getEstado(){
		return this.estado;
	}
	public String mostrarHTML()throws Exception{
		String resultado=Util.leer("HTML//actividad.html");
		resultado=resultado.replace("xrequerimientox",this.plan.requerimiento.id+"");
		resultado=resultado.replace("xtipox",this.tipo+"");
		resultado=resultado.replace("xdescripcionx",this.descripcion);
		resultado=resultado.replace("xestadox",this.estado+"");
		String codigoDatos="";
		String datoActividad=Util.leer("HTML//datoActividad.html");
		for(int i=0;i<listaDatos.size();i++){
			codigoDatos+=datoActividad.replace("xnombrex",listaDatos.get(i));
		}
		resultado=resultado.replace("xlistaDatosActividadx",codigoDatos);
		return resultado;
	}
	public void agregarDato(String dato){
		listaDatos.add(dato);
	}
	public String toString(){
		return tipo+" "+descripcion+" "+estado+"\n";
	}
}