import java.util.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import javax.script.*;
public class Servidor extends Thread{
	public int puerto;
	BufferedReader entrada;
	PrintStream salida;
	public Maestro maestro;
	public static PrintStream ps;
	public Servidor(int puerto){
		this.puerto=puerto;
		this.maestro=new Maestro();
		try{
			Servidor.ps=new PrintStream(new FileOutputStream(new File("log.txt")));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void run(){
		try{
			maestro.cargar();
			maestro.actualizar();
		}catch(Exception e){
			e.printStackTrace();
			Util.log("Error al cargar maestro");
			Util.log(e);
			System.exit(0);
		}
		try{
			ServerSocket serverSocket=new ServerSocket(puerto);
			while(true){
				System.out.println("Esperando");
				Socket socket=serverSocket.accept();
				System.out.println("Aceptado");
				entrada=new BufferedReader(new InputStreamReader(socket.getInputStream()));
				salida=new PrintStream(socket.getOutputStream());
				String leer=".";
				String texto="";
				while(leer.length()>0){
					leer=entrada.readLine();
					texto+=leer;
				}
				Servidor.ps.println("Leído:"+texto);
				{
					int i=texto.indexOf(" HTTP/");
					texto=texto.substring(5,i);
				}
				//System.out.println("TEXTO:"+texto);
				if(texto.indexOf(".js")>=0){
					try{
						salida.println(Util.leer(unescape(texto)));
						salida.flush();
						socket.close();
						continue;
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				salida.println("HTTP/1.0 200 OK");
				salida.println("Content-Type: text/html");
				salida.println("Server: Alex");
				salida.println("");
				String[]instrucciones=texto.split("&");
				for(int i=0;i<instrucciones.length;i++){
					String[]c=instrucciones[i].split("=");
					if(c.length>1){
						analizarComando(c[0],c[1]);
					}
				}
				salida.println(maestro.mostrarHTML());
				salida.flush();
				socket.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void analizarComando(String comando,String valor){
		try{
			//System.out.println(comando+":"+valor);
			Servidor.ps.println("s.h.put(\""+comando+"\",\""+unescape(valor)+"\");");
			if(Math.sin(1000)<100){
				return;
			}
			if(comando.equals("completarTarea")){
				Util.log("completarTarea--->"+valor);
				String[]datos=valor.split(",");
				int idRequerimiento=Integer.parseInt(datos[0].trim());
				int tipoActividad=Integer.parseInt(datos[1].trim());
				//maestro.actividad(idRequerimiento,tipoActividad).cambiarEstado(2);
				/*HashMap<String,Dato>hash=new HashMap<String,Dato>();
				for(int i=2;i<datos.length;i=i+2){
					hash.put(datos[i].trim(),new Dato(datos[i].trim(),datos[i+1].trim()));
				}*/
				ArrayList<Dato>listaDatos=new ArrayList<Dato>();
				for(int i=2;i<datos.length;i=i+2){
					listaDatos.add(new Dato(unescape(datos[i].trim()),unescape(datos[i+1].trim())));
				}
				maestro.actividad(idRequerimiento,tipoActividad).completarTarea(listaDatos);
			}
			if(comando.equals("agregarAsignacion")){
				Util.log("agregarAsignacion--->("+valor+")");
				maestro.crearRequerimiento(unescape(valor));
			}
			if(comando.equals("refrescar")){
				Util.log("refrescar--->("+valor+")");
				maestro.actualizar();
			}
			if(comando.equals("recargar")){
				Util.log("recargar--->("+valor+")");
				maestro.cargar();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void main(String[]args){
		try{
			new Util();
			Servidor servidor=new Servidor(9999);
			servidor.start();
		}catch(Exception e){
			e.printStackTrace();
			Util.log(e);
		}
	}
	public String unescape(String entrada)throws Exception{
		String resultado="";
		ScriptEngineManager factory=new ScriptEngineManager();
		ScriptEngine engine=factory.getEngineByName("JavaScript");
		engine.put("entrada",entrada);
		engine.eval("var salida=unescape(entrada);");
		resultado=(String)engine.get("salida");
		return resultado;
	}
}