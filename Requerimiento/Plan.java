import java.util.*;
import java.io.*;
import f.*;
public class Plan extends Actor{
	public Requerimiento requerimiento;
	private HashMap<Integer,Actividad>hashActividades;
	public void cargar()throws Exception{
		if(Control.solicitar(this,"cargar",null)){
			Scanner s=new Scanner(new File("Plan01.txt"));
			while(s.hasNextInt()){
				int tipo=s.nextInt();
				Actividad actividad=new Actividad(this,tipo);
				while(s.hasNext()){
					String valor=s.next();
					if(valor.equals("&")){
						while(s.hasNextInt()){
							actividad.agregarDependencia(hashActividades.get(s.nextInt()));
						}
						if(!s.next().equals("$")){
							Util.log("Se esperaba $ en el archivo de actividades");
						}
						break;
					}
					if(valor.equals("$")){
						break;
					}
					actividad.descripcion+=valor+" ";
				}
				agregarActividad(actividad);
			}
			Control.realizada(this,"cargar",null);
		}
	}
	public void agregarActividad(Actividad actividad)throws Exception{
		hashActividades.put(actividad.tipo,actividad);
	}
	public void cargarDatosActividades()throws Exception{
		if(Control.solicitar(this,"cargarDatosActividades",null)){
			Scanner s=new Scanner(new File("DatosActividad.txt"));
			while(s.hasNextInt()){
				int tipo=s.nextInt();
				Actividad actividad=obtenerActividad(tipo);
				while(s.hasNext()){
					String token=s.next();
					if(token.equals("%")){
						break;
					}
					String nombre=token;
					while(s.hasNext()){
						token=s.next();
						if(token.equals("$")){
							break;
						}
						nombre+=" "+token;
					}
					actividad.agregarDato(nombre);
				}
			}
			Control.realizada(this,"cargarDatosActividades",null);
		}
	}
	public Plan(Requerimiento requerimiento){
		this.requerimiento=requerimiento;
		hashActividades=new HashMap<Integer,Actividad>();
		registrarRealizada("Actividad.cambiarEstado","detectarCambios");
	}
	public Actividad obtenerActividad(int tipo)throws Exception{
		if(hashActividades.get(tipo)==null){
			throw new Exception("No existe el tipo de actividad:"+tipo);
		}
		return hashActividades.get(tipo);
	}
	public void detectarCambios(Actividad origen,String accion,Object[]parametros)throws Exception{
		actualizar();
	}
	public void actualizar(){
		if(Control.solicitar(this,"actualizar",null)){
			ArrayList<Actividad>listaActividades=this.listaActividades();
			for(int i=0;i<listaActividades.size();i++){
				Actividad actividad=listaActividades.get(i);
				if(actividad.getEstado()==0){
					int dependencias=0;
					for(int j=0;j<actividad.dependencias.size();j++){
						if(actividad.dependencias.get(j).getEstado()!=2){
							dependencias++;
						}
					}
					if(dependencias==0){
						actividad.setEstado(1);
					}
				}
			}
			Control.realizada(this,"actualizar",null);
		}
	}
	public ArrayList<Actividad>listaActividades(int estado){
		ArrayList<Actividad>resultado=new ArrayList<Actividad>();
		Iterator<Map.Entry<Integer,Actividad>>iterador=hashActividades.entrySet().iterator();
		while(iterador.hasNext()){
			Map.Entry<Integer,Actividad>entrada=iterador.next();
			Actividad actividad=entrada.getValue();
			if(actividad.getEstado()==estado){
				resultado.add(actividad);
			}
		}
		return resultado;
	}
	public ArrayList<Actividad>listaActividades(){
		ArrayList<Actividad>resultado=new ArrayList<Actividad>();
		Iterator<Map.Entry<Integer,Actividad>>iterador=hashActividades.entrySet().iterator();
		while(iterador.hasNext()){
			Map.Entry<Integer,Actividad>entrada=iterador.next();
			resultado.add(entrada.getValue());
		}
		return resultado;
	}
	public String toString(){
		return requerimiento+"|"+this.hashCode()+":"+hashActividades.size();
	}
}