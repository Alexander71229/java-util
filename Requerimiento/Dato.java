public class Dato{
	public String nombre;
	public String valor;
	public Dato(String nombre,String valor){
		this.nombre=nombre.trim();
		this.valor=valor.trim();
	}
	public String mostrarHTML()throws Exception{
		String resultado=Util.leer("HTML//dato.html");
		resultado=resultado.replace("xnombrex",this.nombre+"");
		resultado=resultado.replace("xvalorx",this.valor+"");
		return resultado;
	}
}